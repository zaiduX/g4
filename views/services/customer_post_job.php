<style type="text/css">
.container {
  width: 60%;
  margin:0 auto;
  padding: 20px 0
}
.drop-shadow {
    position:relative;
    width:100%;
    padding:3em 1em;
    margin:2em 10px 4em;
    background:#fff;
    -webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
       -moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
      box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
}

.drop-shadow:before,
.drop-shadow:after {
    content:"";
    position:absolute;
    z-index:-2;
}

.drop-shadow p {
    font-size:1.5em;
    font-weight:300;
    text-align:center;
}

.lifted {
    -moz-border-radius:4px;
   border-radius:4px;
}

.lifted:before,
.lifted:after {
    bottom:15px;
    left:10px;
    width:50%;
    height:20%;
    max-width:300px;
    max-height:100px;
    -webkit-box-shadow:0 15px 10px rgba(0, 0, 0, 0.7);
       -moz-box-shadow:0 15px 10px rgba(0, 0, 0, 0.7);
      box-shadow:0 15px 10px rgba(0, 0, 0, 0.7);
    -webkit-transform:rotate(-3deg);
       -moz-transform:rotate(-3deg);
  -ms-transform:rotate(-3deg);
   -o-transform:rotate(-3deg);
      transform:rotate(-3deg);
}

.lifted:after {
    right:10px;
    left:auto;
    -webkit-transform:rotate(3deg);
       -moz-transform:rotate(3deg);
  -ms-transform:rotate(3deg);
   -o-transform:rotate(3deg);
      transform:rotate(3deg);
}


#attachment, .attachment { 
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px; 
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0); 
  }
  .entry:not(:first-of-type) {
    margin-top: 10px;
  }

</style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Post A Job'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-wpforms fa-2x text-muted"></i> <?= $this->lang->line('Get your Project Done!'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Post a project for Free'); ?></small>    
    </div>
  </div>
</div>
 
<div class="content">
  <div class="row">
    <?php
    // echo json_encode($res);

    $sub_cat=$this->api->get_sub_category_by_id($res['sub_category']);
    // echo json_encode($sub_cat->cat_name);

    ?>
    <div class="hpanel hblue">
      <form action="<?= base_url('user-panel-services/register-customer-job'); ?>" method="post" class="form-horizontal" id="cutomer_job_post_form" enctype="multipart/form-data">
        <div class="panel-body">
          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <label class=""><?= $this->lang->line('WHAT DO YOU NEED TO GET DONE?'); ?> </label>
              <input id="job_title" name="job_title" type="text" class="form-control" placeholder="<?= $this->lang->line('e.g I need a professional website design'); ?>" autocomplete="none" value="<?=$res['job_title']?ucfirst($res['job_title']):"";?>" > <br/>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
              <label class=""><?= strtoupper($this->lang->line('Category')); ?> </label>
              <select class="select2 form-control" name="category" id="category">
                  <option value=""><?= $this->lang->line('select'); ?></option>
                  <?php foreach ($category_types as $c ): ?>
                    <option value="<?= $c['cat_type_id']?>" <?=($c['cat_type_id']==$res['category'])?"selected":"";?>><?= $c['cat_type']?></option>
                  <?php endforeach; ?>
              </select><br/>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
              <label class=""><?= strtoupper($this->lang->line('Sub Category')); ?> </label>
              <select class="select2 form-control" name="sub_category" id="sub_category"<?=($res['sub_category'])?"":"disabled";?> >
                <?php if($res['sub_category']): ?>
                  <option value="<?=$res['sub_category'];?>"><?= $sub_cat->cat_name; ?></option>
                <?php endif; ?>
                  <option value=""><?= $this->lang->line('select'); ?></option>
              </select><br/>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <label class=""><?= strtoupper($this->lang->line('desc')); ?> </label>
              <textarea style="resize:none"  rows="5" id="description" name="description" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Provide a more detailed description to help you get better proposal'); ?>" autocomplete="none" ><?=$res['description']?ucfirst($res['description']):"";?></textarea> <br/>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-right:0px; padding-left: 0px;">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px; padding-left: 0px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <label class=""><?= strtoupper($this->lang->line('Currency')); ?> </label>
                  <input type="text" id="currency_signz" name="currency_signz" value="<?=$currency_details['currency_sign']?>" class="form-control" disabled="true" autocomplete="none"> <br/>
                  <input type="hidden" id="currency_sign" name="currency_sign" value="<?=$currency_details['currency_sign']?>" class="form-control">
                  <input type="hidden" id="currency_id" name="currency_id" value="<?=$currency_details['currency_id']?>" class="form-control">
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <label class=""><?= strtoupper($this->lang->line('work type')); ?> </label>
                  <select class="select2 form-control" name="work_type" id="work_type">
                      <option value="fixed price" <?=($res['work_type']=='fixed price')?"selected":"";?>><?= $this->lang->line('Fixed Price'); ?></option>
                      <option value="per hour" <?=($res['work_type']=='per hour')?"selected":"";?>><?= $this->lang->line('Per Hour'); ?></option>
                      <option value="Daily" <?=($res['work_type']=='Daily')?"selected":"";?>><?= $this->lang->line('Daily'); ?></option>
                      <option value="Weekly" <?=($res['work_type']=='Weekly')?"selected":"";?>><?= $this->lang->line('Weekly'); ?></option>
                      <option value="Monthly" <?=($res['work_type']=='Monthly')?"selected":"";?>><?= $this->lang->line('Monthly'); ?></option>
                  </select><br/>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <label class="bgtfxd"><?= strtoupper($this->lang->line('budget')); ?> </label>
                  <label class="bgtpr hidden"><?= strtoupper($this->lang->line('budget per hour')); ?> </label>
                  <input id="budget" name="budget" type="text" class="form-control" autocomplete="none" value="<?=($res['budget'])?$res['budget']:"";?>"> <br/>
                </div>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px; padding-left: 0px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <label class=""><?= strtoupper($this->lang->line('Upload samples'))." (".$this->lang->line('Optional').")"; ?> </label>
                  <div class="control-group" id="upload_sample">
                    <div class="controls">
                      <div class="entry input-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4"style="width:unset;">
                        <input class="btn btn-primary image_url" name="image_url[]" type="file" accept="image/png,image/gif,image/jpeg" style="font-size: 12px;">
                        <span class="input-group-btn">
                          <button class="btn btn-success btn-add" type="button" style="margin-left: 2px !important;">
                            <span class="fa fa-plus"></span>
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="exprdt col-xl-12 col-lg-12 col-md-12 col-sm-12 <?=($res['work_type']=='per hour')?"":"hidden";?>" style="padding-right:0px; padding-left: 0px;">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px; padding-left: 0px;">
                <div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-bottom: 18px;">
                  <label><?= strtoupper($this->lang->line('expiry_date'))." (".$this->lang->line('Optional').")";?> </label>
                  <div class="input-group date">
                    <input type="text" class="form-control" id="expiry_date" name="expiry_date" autocomplete="none" value="<?=($res['expiry_date'])?$res['expiry_date']:"";?>" />
                    <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                    </div>
                  </div>                      
                </div>
              </div>
              <!-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px; padding-left: 0px;">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-right:0px ">
                  <label><?= strtoupper($this->lang->line('Billing period'))?> </label>
                  <select class="select2 form-control" name="billing_period" id="billing_period">
                    <option value=""><?= $this->lang->line('select'); ?></option>
                    <option value="Daily" <?=($res['billing_period']=='Daily')?"selected":"";?>><?= $this->lang->line('Daily'); ?></option>
                    <option value="Weekly" <?=($res['billing_period']=='Weekly')?"selected":"";?>><?= $this->lang->line('Weekly'); ?></option>
                    <option value="Monthly" <?=($res['billing_period']=='Monthly')?"selected":"";?>><?= $this->lang->line('Monthly'); ?></option>
                    <option value="Other" <?=($res['billing_period']=='Other')?"selected":"";?>><?= $this->lang->line('other'); ?></option>
                  </select><br/>
                </div>
              </div> -->
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <a class="btn btn-link btn_show" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Show advanced options (location, visibility, duration, interview questions)")?></a>
              <br/>
              
              <div class="row" id="advance_option" style="display:none; padding-bottom: 0px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <label class=""><?=$this->lang->line('WHERE DOES THE FREELANCER NEED TO WORK FROM?')?></label>
                  <select class="select2 form-control" name="work_from" id="work_from">
                      <option value="Remotely (anywhere)"><?= $this->lang->line('Remotely (anywhere)'); ?></option>
                      <option value="Remotely (Preferred Country)"><?= $this->lang->line('Remotely (Preferred Country)'); ?></option>
                      <option value="On-site (specific location)"><?= $this->lang->line('On-site (specific location)'); ?></option>
                  </select><br/>
                </div>                
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 " id="preffered_country" style="display:none;">
                  <label class=""><?= strtoupper($this->lang->line('country')); ?> </label>
                  <select class="select2 form-control" name="country_id" id="country_id">
                      <option value=""><?= $this->lang->line('select'); ?></option>
                      <?php foreach ($countries as $country ): ?>
                        <option value="<?= $country['country_id']?>"><?= $country['country_name']?></option>
                      <?php endforeach; ?>
                  </select><br/>
                </div>
                <div id="on_site" style="display:none;padding-left: 0px; padding-right: 0px;">
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <label><?= strtoupper($this->lang->line('city'))?> </label>
                    <input type="text" name="city_name" class="form-control" id="city_name" placeholder="<?=$this->lang->line('Enter Location Name')?>">    
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 18px;">
                    <label><?= strtoupper($this->lang->line('Starting date'))." (".$this->lang->line('Optional').")"; ?> </label>
                    <div class="input-group date">
                      <input type="text" class="form-control" id="starting_date" name="starting_date" autocomplete="none" value="" />
                      <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                      </div>
                    </div>                      
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <label><?= strtoupper($this->lang->line('Starting time'))." (".$this->lang->line('Optional').")"; ?> </label>
                    <select class="select2 form-control" name="starting_time" id="starting_time">
                      <option value=""><?= $this->lang->line('select'); ?></option>
                      <option value="7:00 AM">7:00 AM</option>
                      <option value="7:30 AM">7:30 AM</option>
                      <option value="8:00 AM">8:00 AM</option>
                      <option value="8:30 AM">8:30 AM</option>
                      <option value="9:00 AM">9:00 AM</option>
                      <option value="9:30 AM">9:30 AM</option>
                      <option value="10:00 AM">10:00 AM</option>
                      <option value="10:30 AM">10:30 AM</option>
                      <option value="11:00 AM">11:00 AM</option>
                      <option value="11:30 AM">11:30 AM</option>
                      <option value="12:00 PM">12:00 PM</option>
                      <option value="12:30 PM">12:30 PM</option>
                      <option value="1:00 PM">1:00 PM</option>
                      <option value="1:30 PM">1:30 PM</option>
                      <option value="2:00 PM">2:00 PM</option>
                      <option value="2:30 PM">2:30 PM</option>
                      <option value="3:00 PM">3:00 PM</option>
                      <option value="3:30 PM">3:30 PM</option>
                      <option value="4:00 PM">4:00 PM</option>
                      <option value="4:30 PM">4:30 PM</option>
                      <option value="5:00 PM">5:00 PM</option>
                      <option value="5:30 PM">5:30 PM</option>
                      <option value="6:00 PM">6:00 PM</option>
                      <option value="6:30 PM">6:30 PM</option>
                      <option value="7:00 PM">7:00 PM</option>
                      <option value="7:30 PM">7:30 PM</option>
                      <option value="8:00 PM">8:00 PM</option>
                      <option value="8:30 PM">8:30 PM</option>
                      <option value="9:00 PM">9:00 PM</option>
                      <option value="9:30 PM">9:30 PM</option>
                      <option value="10:00 PM">10:00 PM</option>
                      <option value="10:30 PM">10:30 PM</option>
                      <option value="11:00 PM">11:00 PM</option>
                      <option value="11:30 PM">11:30 PM</option>
                      <option value="12:00 AM">12:00 AM</option>
                    </select><br/>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <label><?= strtoupper($this->lang->line('Duration On-site'))." (".$this->lang->line('Optional').")"; ?> </label>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-left: 0px;">
                      <input type="number" min="0" placeholder="<?= $this->lang->line('hours');?>" class="form-control" id="hours" name="hours">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px ">
                      <select class="select2 form-control" name="hours_per" id="hours_per">
                        <option value="in 1 Day"><?= $this->lang->line('In 1 day'); ?></option>
                        <option value="per day"><?= $this->lang->line('Per day'); ?></option>
                        <option value="per week"><?= $this->lang->line('Per week'); ?></option>
                        <option value="per month"><?= $this->lang->line('Per month'); ?></option>
                      </select><br/>
                    </div>
                  </div>  
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                 <strong><?=$this->lang->line('VISIBILITY OF PROJECT POST')?></strong>
                 <br/>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <div class='radio radio-success'>
                    <input type='radio' id='public' name="visibility" value='public' checked="checked">
                    <label><i class="fa fa-users"></i><strong> <?=strtoupper($this->lang->line('Public'))?></strong> (<?=$this->lang->line('All Freelancers can view the project post and send proposals')?>)</label>
                  </div>
                  <div class='radio radio-success'>
                    <input type='radio' id='private' name="visibility" value='private'>
                    <label><i class="fa fa-lock"></i><strong> <?=strtoupper($this->lang->line('Private'))?></strong> (<?=$this->lang->line('Only Freelancers that you specifically invite can view the project post and send proposals')?>)</label>
                  </div>
                  <div class='radio radio-success'>
                    <input type='radio' id='p2p' name="visibility" value='p2p'>
                    <label><i class="fa fa-user"></i><strong> <?=strtoupper($this->lang->line('Peer to peer'))?></strong> (<?=$this->lang->line('Only single Freelancer will get invitation and post a proposal')?>)</label>
                  </div>
                  <br/>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                  <label class=""><?= strtoupper($this->lang->line('Get to known your freelancer'))." (".$this->lang->line('Optional').")"; ?> </label>
                  <div id="q1">
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9" style="padding-left: 0px">
                      <select class="select2 form-control" name="question1" id="question1" disabled>
                        <option value=""><?= $this->lang->line('select'); ?></option>
                      </select>
                      <div id="q1_other" style="display:none;">
                        <br/>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="question1_other" name="question1_other" autocomplete="none" value="" />
                      </div>
                      <a class="btn btn-link btn-add-q1" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new question")?></a>  
                    <br/>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                      <!-- <button class="btn btn-danger btn-delete1" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                      </button> -->
                    </div>
                  </div>
                  <div id="q2" style="display:none;">
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9" style="padding-left: 0px">
                      <select class="select2 form-control" name="question2" id="question2" disabled>
                        <option value=""><?= $this->lang->line('select'); ?></option>
                      </select>
                      <div id="q2_other" style="display:none;">
                        <br/>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="question2_other" name="question2_other" autocomplete="none" value="" />
                      </div>
                      <a class="btn btn-link btn-add-q2" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new question")?></a>  
                      <br/>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                      <button class="btn btn-danger btn-delete2" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                      </button>
                    </div>
                  </div>
                  <div id="q3" style="display:none;">
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9" style="padding-left: 0px">
                      <select class="select2 form-control" name="question3" id="question3" disabled>
                        <option value=""><?= $this->lang->line('select'); ?></option>
                      </select>
                      <div id="q3_other" style="display:none;">
                        <br/>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="question3_other" name="question3_other" autocomplete="none" value="" />
                      </div>
                      <a class="btn btn-link btn-add-q3" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new question")?></a>  
                      <br/>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                      <button class="btn btn-danger btn-delete3" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                      </button>
                    </div>
                  </div>
                  <div id="q4" style="display:none;">
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9" style="padding-left: 0px">
                      <select class="select2 form-control" name="question4" id="question4" disabled>
                        <option value=""><?= $this->lang->line('select'); ?></option>
                      </select>
                      <div id="q4_other" style="display:none;">
                        <br/>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="question4_other" name="question4_other" autocomplete="none" value="" />
                      </div>
                      <a class="btn btn-link btn-add-q4" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new question")?></a>  
                      <br/>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                      <button class="btn btn-danger btn-delete4" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                      </button>
                    </div>
                  </div>
                  <div id="q5" style="display:none;">
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9" style="padding-left: 0px">
                      <select class="select2 form-control" name="question5" id="question5" disabled>
                        <option value=""><?= $this->lang->line('select'); ?></option>
                      </select>
                      <div id="q5_other" style="display:none;">
                        <br/>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="question5_other" name="question5_other" autocomplete="none" value="" />
                      </div>
                      <!-- <a class="btn btn-link btn-add-q5" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new question")?></a> -->  
                      <br/>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                      <button class="btn btn-danger btn-delete5" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 " style="padding-left: 0px;">
                    <label class=""><?= strtoupper($this->lang->line('Estimated project duration'))." (".$this->lang->line('Optional').")"; ?> </label>
                    <select class="select2 form-control" name="project_duration" id="project_duration">
                        <option value=""><?= $this->lang->line('select'); ?></option>
                        <option value="1 day or less"><?= $this->lang->line('1 day or less'); ?></option>
                        <option value="1 - 2 weeks"><?= $this->lang->line('1 - 2 weeks'); ?></option>
                        <option value="3 - 4 weeks"><?= $this->lang->line('3 - 4 weeks'); ?></option>
                        <option value="1 - 6 months"><?= $this->lang->line('1 - 6 months'); ?></option>
                        <option value="More than 6 months"><?= $this->lang->line('More than 6 months')?></option>
                        <option value="Ongoing"><?= $this->lang->line('Ongoing'); ?></option>
                        <option value="Not sure"><?= $this->lang->line('Not sure'); ?></option>
                        <option value="other"><?= $this->lang->line('other'); ?></option>
                    </select><br/>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 project_duration_other" style="display: none;padding-left: 0px;">
                    <label class="">&nbsp;</label>
                    <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="project_duration_other" name="project_duration_other" autocomplete="none" value="" />
                  </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <a class="btn btn-link btn_hide" style="padding-left:0px;color: #225595"><i class="fa fa-minus"></i> <?=$this->lang->line("Hide advanced options")?></a>
                </div>
              </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
             <br/> <input type="submit" id="post_project" value="<?=strtoupper($this->lang->line('Post project'))?>" class="btn btn-success">
            </div>
          </div>
          <div class="map_canvas_location hidden" style="width: 100%; height: 200px;"></div>
          <input type="hidden" data-geo-location="lat" name="latitude_location" id="latitude_location" />
          <input type="hidden" data-geo-location="lng" name="longitude_location" id="longitude_location" />
          <input type="hidden" data-geo-location="country" name="country_location" id="country_location" />
          <input type="hidden" data-geo-location="administrative_area_level_1" name="state_location" id="state_location" />
          <input type="hidden" data-geo-location="locality" name="city_location" id="city_location" />
          <input type="hidden" data-geo-location="formatted_address" name="street_location" id="street_location" />
          <input type="hidden" data-geo-location="postal_code" name="zipcode_location" id="zipcode_location" />
          <input type="hidden" name="no_of_points" id="no_of_points" value="1" />
          <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
            <div class="drop-shadow lifted">
              <h3 class="text-center" style="margin-top:-20px;"><?=$this->lang->line('USEFUL TIPS')?></h3>
              <ul> 
                <li><?=$this->lang->line('useful tips 1')?></li><br>
                <li><?=$this->lang->line('useful tips 2')?></li><br>
                <li><?=$this->lang->line('useful tips 3')?></li><br>
              </ul>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>


              

<script>
  $(function() {
    $(document).on('click', '.btn-add', function(e) {
      e.preventDefault();
      var numItems = $('.entry').length;
      if(numItems <= 5) {
        var controlForm = $('.controls:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
        .removeClass('btn-add').addClass('btn-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="fa fa-minus"></span>');
      } else { swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('You can upload upto 6 files only.')?>","warning"); }
      }).on('click', '.btn-remove', function(e) {
        $(this).parents('.entry:first').remove();
      e.preventDefault();
      return false;
    });
  });
</script>
<script>
  $(".btn_show").click(function(){
    $("#advance_option").slideToggle();
    $(".btn_show").addClass("hidden");
    $(".btn_hide").removeClass("hidden");
  });
  $(".btn_hide").click(function(){
    $("#advance_option").slideToggle();
    $(".btn_show").removeClass("hidden");
    $(".btn_hide").addClass("hidden");
  });
</script>
<script>
  $("#category").on('change', function(event) { event.preventDefault();
    $('#sub_category').empty();
    $('#question1,#question2,#question3,#question4,#question5').empty();
    var d = <?= json_encode($this->lang->line("no sub category found"))?>;
    var cat_type_id = $(this).val();

    $.post('get-sub-category', {cat_type_id: cat_type_id}, function(sub_category) {
      //console.log(sub_category);
      sub_category = $.parseJSON(sub_category);
      if(sub_category.length > 0 ){
        $('#sub_category').attr('disabled', false);
          $('#sub_category').empty(); 
          $('#sub_category').append('<option value=""><?= $this->lang->line("select")?></option>');
          $('#sub_category').append('<option value=""><?= $this->lang->line("select")?></option>');
          $.each(sub_category, function(){    
            $('#sub_category').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');              
          });
          $('#sub_category').focus();
      }else{
          $('#sub_category').attr('disabled', true);
          $('#sub_category').empty();
          $('#sub_category').append('<option value="0">'+d+'</option>');
        }
    });

    $.post('get-questions-know-freelancer', {cat_type_id: cat_type_id}, function(questions) {
      //console.log(questions);
      questions = $.parseJSON(questions);
      if(questions.length > 0 ){
        $('#question1,#question2,#question3,#question4,#question5').attr('disabled', false);
          $('#question1,#question2,#question3,#question4,#question5').empty(); 
          $('#question1,#question2,#question3,#question4,#question5').append('<option value=""><?= $this->lang->line("select")?></option>');
          $.each(questions, function(){    
              $('#question1,#question2,#question3,#question4,#question5').append('<option value="'+$(this).attr('question_title')+'">'+$(this).attr('question_title')+'</option>');              
          });
          $('#question1,#question2,#question3,#question4,#question5').append('<option value="Create your own question"><?= $this->lang->line("Create your own question"); ?></option>');
          //$('#question1').focus();
      }else{
          $('#question1,#question2,#question3,#question4,#question5').attr('disabled', true);
          $('#question1,#question2,#question3,#question4,#question5').empty();
          $('#question1,#question2,#question3,#question4,#question5').append('<option value="0">'+d+'</option>');
        }
    });
  });
</script>
<script>
  $("#work_from").on('change', function(event) {
    event.preventDefault();
    var w_f = $(this).val();
    if(w_f == "Remotely (Preferred Country)"){
      $('#preffered_country').css('display', 'block');
      $('#on_site').css('display', 'none');
    }
    if(w_f == "On-site (specific location)"){
      $('#preffered_country').css('display', 'none');
      $('#on_site').css('display', 'block');
    }
    if(w_f == "Remotely (anywhere)"){
      $('#preffered_country').css('display', 'none');
      $('#on_site').css('display', 'none');
    }
  }); 
</script>

<script>
  $(".btn-add-q1").click(function(){
    $('#q2').css('display', 'block');
    $('.btn-add-q1').hide();
    $('.btn-delete1').hide();
  });

  $(".btn-delete2").click(function(){
    $('#q2,#q2_other').css('display', 'none');
    $('.btn-add-q1').show();
    $('.btn-delete1').show();
  });

  $(".btn-add-q2").click(function(){
    $('#q3').css('display', 'block');
    $('.btn-add-q2').hide();
    $('.btn-delete2').hide();
  });

  $(".btn-delete3").click(function(){
    $('#q3,#q3_other').css('display', 'none');
    $('.btn-add-q2').show();
    $('.btn-delete2').show();
  });

  $(".btn-add-q3").click(function(){
    $('#q4').css('display', 'block');
    $('.btn-add-q3').hide();
    $('.btn-delete3').hide();
  });

  $(".btn-delete4").click(function(){
    $('#q4,#q4_other').css('display', 'none');
    $('.btn-add-q3').show();
    $('.btn-delete3').show();
  });

  $(".btn-add-q4").click(function(){
    $('#q5').css('display', 'block');
    $('.btn-add-q4').hide();
    $('.btn-delete4').hide();
  });

  $(".btn-delete5").click(function(){
    $('#q5,#q5_other').css('display', 'none');
    $('.btn-add-q4').show();
    $('.btn-delete4').show();
  });

  $("#project_duration").on('change', function(event) {
    event.preventDefault();
    //alert($(this).val());
    if($(this).val() == "other" ){
      $('.project_duration_other').show();
    }else{
      $('.project_duration_other').hide();
      $('#project_duration_other').val("");
    }
  });

  $("#question1").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q1_other').show();
    }else{
      $('#q1_other').hide();
      $('#question1_other').val("");
    }
  });

  $("#question2").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q2_other').show();
    }else{
      $('#q2_other').hide();
      $('#question2_other').val("");
    }
  });

  $("#question3").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q3_other').show();
    }else{
      $('#q3_other').hide();
      $('#question3_other').val("");
    }
  });

  $("#question4").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q4_other').show();
    }else{
      $('#q4_other').hide();
      $('#question4_other').val("");
    }
  });

  $("#question5").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q5_other').show();
    }else{
      $('#q5_other').hide();
      $('#question5_other').val("");
    }
  });

  $("#city_name").geocomplete({
    map:".map_canvas_location",
    location: "",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: false, },
    details: "form",
    detailsAttribute: "data-geo-location", 
    types: ["geocode", "establishment"],
  });

  $("#post_project").on('click', function(event) { event.preventDefault();
    var job_title = $("#job_title").val();
    var category = $("#category").val();
    var sub_category = $("#sub_category").val();
    var description = $("#description").val();
    var work_type = $("#work_type").val();
    var billing_period = $("#billing_period").val();
    var budget = $("#budget").val();
    var work_from = $("#work_from").val();
    var country_id = $("#country_id").val();
    var city_name = $("#city_name").val();
    var starting_date = $("#starting_date").val();
    var starting_time = $("#starting_time").val();
    var hours = $("#hours").val();
    var hours_per = $("#hours_per").val();
    var visibility = $("#visibility").is(":checked");
    var question1 = $("#question1").val();
    var question2 = $("#question2").val();
    var question3 = $("#question3").val();
    var question4 = $("#question4").val();
    var question5 = $("#question5").val();
    var question1_other = $("#question1_other").val();
    var question2_other = $("#question2_other").val();
    var question3_other = $("#question3_other").val();
    var question4_other = $("#question4_other").val();
    var question5_other = $("#question5_other").val();
    var project_duration = $("#project_duration").val();
    var project_duration_other = $("#project_duration_other").val();
    


    if(job_title == "") { $("#job_title").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Job Title"); ?>', 'warning', false, "#DD6B55");}
    else if(category == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("sel_category"); ?>', 'warning', false, "#DD6B55"); }
    else if(sub_category == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("sel_subcategory"); ?>', 'warning', false, "#DD6B55"); }
    else if(description == "") {$("#description").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("please_enter_description"); ?>', 'warning', false, "#DD6B55"); }
    else if(work_type == "") { $("#work_type").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Please select work type"); ?>', 'warning', false, "#DD6B55"); }
    else if(budget == "") { $("#budget").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter budget"); ?>', 'warning', false, "#DD6B55"); }
    else if( parseInt(budget) <= 0) { $("#budget").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("budget should be greater than 0"); ?>', 'warning', false, "#DD6B55"); }
    else if(work_from == "Remotely (Preferred Country)" && country_id == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("sel_count"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(work_type == "per hour" && billing_period == ""){ $("#billing_period").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Select billing period"); ?>', 'warning', false, "#DD6B55");}
    else if(work_from == "On-site (specific location)" && city_name == ""){ $("#city_name").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Location Name"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question1 == "Create your own question" && question1_other == "" && $("#q1").is(":visible")){ $("#question1_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question2 == "Create your own question" && question2_other == "" && $("#q2").is(":visible")){ $("#question2_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question3 == "Create your own question" && question3_other == "" && $("#q3").is(":visible")){ $("#question3_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question4 == "Create your own question" && question4_other == "" && $("#q4").is(":visible")){ $("#question4_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question5 == "Create your own question" && question5_other == "" && $("#q5").is(":visible")){ $("#question5_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(project_duration == "other" && project_duration_other == ""){ $("#project_duration_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter project duration"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else{
      //alert("submit");
      $("#cutomer_job_post_form").submit(); 
    }
  });

  $("#work_type").on('change', function(event) {
    event.preventDefault();
    if($(this).val() != "fixed price" ){
      $(".bgtpr").removeClass("hidden");
      $(".exprdt").removeClass("hidden");
      $('.bgtfxd').hide();
    }else{
      $('.bgtfxd').show();
      $(".bgtpr").addClass("hidden");
      $(".exprdt").addClass("hidden");
    }
  });

  $("#budget").keypress(function(event) {
    return /\d/.test(String.fromCharCode(event.keyCode));
  });

  $("#budget").keyup(function(){
    var value = $(this).val();
    value = value.replace(/^(0*)/,"");
    $(this).val(value);
  });

</script>
<script>
  if("<?=$res['category']?>" > 0){
     // $("#category").on('change', function(event) { event.preventDefault();
    $('#sub_category').empty();
    $('#question1,#question2,#question3,#question4,#question5').empty();
    var d = <?= json_encode($this->lang->line("no sub category found"))?>;
    var cat_type_id = <?=$res['category']?>;

    $.post('get-sub-category', {cat_type_id: cat_type_id}, function(sub_category) {

      //console.log(sub_category);
      sub_category = $.parseJSON(sub_category);
      if(sub_category.length > 0 ){
        $('#sub_category').attr('disabled', false);
          $('#sub_category').empty(); 
          $('#sub_category').append('<option value=""><?= $this->lang->line("select")?></option>');
          $('#sub_category').append('<option value=""><?= $this->lang->line("select")?></option>');
          $.each(sub_category, function(){    
                   var cat_id =$(this).attr('cat_id');
       var cat_name=$(this).attr('cat_name');
       var set=(cat_id=="<?=$res['sub_category']?>")?"selected":"";
            $('#sub_category').append('<option value="'+cat_id+'" '+set+'>'+cat_name+'</option>');              
          });
          $('#sub_category').focus();
      }else{
          $('#sub_category').attr('disabled', true);
          $('#sub_category').empty();
          $('#sub_category').append('<option value="0">'+d+'</option>');
        }
    });

    $.post('get-questions-know-freelancer', {cat_type_id: cat_type_id}, function(questions) {
      //console.log(questions);
      questions = $.parseJSON(questions);
      if(questions.length > 0 ){
        $('#question1,#question2,#question3,#question4,#question5').attr('disabled', false);
          $('#question1,#question2,#question3,#question4,#question5').empty(); 
          $('#question1,#question2,#question3,#question4,#question5').append('<option value=""><?= $this->lang->line("select")?></option>');
          $.each(questions, function(){    
              $('#question1,#question2,#question3,#question4,#question5').append('<option value="'+$(this).attr('question_title')+'">'+$(this).attr('question_title')+'</option>');              
          });
          $('#question1,#question2,#question3,#question4,#question5').append('<option value="Create your own question"><?= $this->lang->line("Create your own question"); ?></option>');
          //$('#question1').focus();
      }else{
          $('#question1,#question2,#question3,#question4,#question5').attr('disabled', true);
          $('#question1,#question2,#question3,#question4,#question5').empty();
          $('#question1,#question2,#question3,#question4,#question5').append('<option value="0">'+d+'</option>');
        }
    });
  // }); 
  }

</script>