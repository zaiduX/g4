<style>
.vl {
  border-left: 1px solid grey;
  height: 200px;
  position: absolute;
  left: 65%;
  margin-left: -3px;
  top: 0;
}
</style>
<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel-services/my-inprogress-jobs'; ?>"><?= $this->lang->line('in_progress'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('Milestone payment request'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
               <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Milestone payment request'); ?>
            </h2>
            <!-- <small class="m-t-md"><?= $this->lang->line('Send request to customer to pay milestone price'); ?>.</small> -->
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <?php if($direct_payment == "yes"){ ?>
                    <form method="post" class="form-horizontal" action="<?=base_url('user-panel-services/accept-job-milestone-payment-request')?>" id="MilestoneRequest">
                    <?php }else{ ?>
                    <form method="post" class="form-horizontal" action="<?=base_url('user-panel-services/accept-job-milestone-payment-view')?>" id="MilestoneRequest">
                    <?php } ?>

                        <input type="hidden" name="job_id" value="<?= $job_details['job_id'] ?>">
                        <input type="hidden" name="milestone_id" value="<?= $proposal_milestone['milestone_id'] ?>">
                        <input type="hidden" name="milestone_price" value="<?= $proposal_milestone['milestone_price'] ?>">
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                                <?php endif; ?>
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-xs-12">
                                <div class="col-xl-8 col-lg-8 col-md-8 col-xs-12">
                                    <div class="form-group"><label class="col-md-3"><?= $this->lang->line('Job details'); ?>: </label>
                                        <div class="col-md-8"> 
                                            <label style="color:#59bdd7"><?= $job_details['job_title']?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-3"><?= $this->lang->line('Milestone details'); ?>: </label>
                                        <div class="col-md-8"> 
                                            <label style="color:#59bdd7"><?= $proposal_milestone['milestone_details'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-3"><?= $this->lang->line('Milestone price'); ?>: </label>
                                        <div class="col-md-8"> 
                                            <label style="color:#59bdd7"><?= $proposal_milestone['milestone_price']." ".$proposal_milestone['currency_code']?></label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-3"><?= $this->lang->line('desc'); ?> </label>
                                        <div class="col-md-8"> 
                                            <label style="color:#59bdd7"><?= $milestone_payment_request['request_description']?></label>
                                        </div>
                                    </div>
                                    <?php if($direct_payment == "no"){ ?>
                                    <div class="form-group"><label class="col-md-3"><?= $this->lang->line('Add payment'); ?> </label>
                                        <div class="col-md-4"> 

                                            <div class="input-group">
                                                <input type="number" class="form-control" id="total" name="total" value="<?=$min?>" required min="<?=$min?>" max="<?=$max?>" >
                                                <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button class="btn btn-success btn-outline" type="submit"><?=($direct_payment == "no")?$this->lang->line('Make a payment'):$this->lang->line('Accept And Pay')?></button>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <button type="button" class="btn btn-danger btn-outline" id="btnCancel" data-toggle="modal" data-target="#myModal_<?=$job_details['job_id']?>" style="text-align: -webkit-left;"><i class="fa fa-times"></i> <?= $this->lang->line('Cancel And Dispute'); ?></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="vl">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-xs-12">
                                    <div class="form-group"><label class="col-md-6"><?= $this->lang->line('Job price'); ?>: </label>
                                        <div class="col-md-6"> 
                                            <label style="color:#59bdd7"><?= $job_details['budget']." ".$job_details['currency_code']?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-6"><?= $this->lang->line('Job scrow amount'); ?>: </label>
                                        <div class="col-md-6">
                                            <?php if($milestone_price < $max){ ?> 
                                            <label style="color:#59bdd7"><?= $remain." ".$job_details['currency_code']?>
                                            </label>
                                            <?php }else{ ?>
                                            <label style="color:#fb0101"><?= $remain." ".$job_details['currency_code']?>
                                            </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-6"><?= $this->lang->line('Release amount'); ?>: </label>
                                        <div class="col-md-6"> 
                                            <label style="color:#59bdd7"><?= $paid_amount_history." ".$job_details['currency_code']?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-6"><?= $this->lang->line('Remaining amount'); ?>: </label>
                                        <div class="col-md-6">  
                                            <label style="color:#59bdd7"><?= $max." ".$job_details['currency_code']?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-6"><?= $this->lang->line('Total paid amount'); ?>: </label>
                                        <div class="col-md-6"> 
                                            <label style="color:#59bdd7"><?= $job_details['paid_amount']." ".$job_details['currency_code']?>
                                            </label>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal_<?=$job_details['job_id']?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="frmDispute_<?=$job_details['job_id']?>" action="<?=base_url('user-panel-services/customer-create-dispute-for-job')?>" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?=$this->lang->line('Raise a dispute')?></h4>
          <small><?=$this->lang->line('Post a dispute to sittle your issues')?></small>
        </div>
        <div class="modal-body">
          <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>" />
          <input type="hidden" name="redirect_url" value="my-inprogress-jobs" />
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <label class="label-control"><?=$this->lang->line('Dispute Title')?></label>
              <input type="text" name="dispute_title" id="dispute_title" class="form-control" />
            </div>
          </div>
          <div class="row form-group">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
              <label class="label-control"><?=$this->lang->line('Select dispute category')?></label>
              <select name="dispute_cat_id" id="dispute_cat_id" class="form-control dispute_cat_id">
                <option value=""><?=$this->lang->line('Select dispute category')?></option>
                <?php for ($i=0; $i < sizeof($dispute_master); $i++) { ?>
                  <option value="<?=$dispute_master[$i]['dispute_cat_id']?>"><?=$dispute_master[$i]['dispute_cat_title']?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="row form-group">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
              <label class="label-control"><?=$this->lang->line('sel_subcategory')?></label>
              <select name="dispute_sub_cat_id" id="dispute_sub_cat_id" class="form-control dispute_sub_cat_id">
                <option value=""><?=$this->lang->line('sel_subcategory')?></option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <label class="label-control"><?=$this->lang->line('description_optional')?></label>
              <textarea name="dispute_desc" id="dispute_desc" class="form-control" style="resize: none;"></textarea>
            </div>
          </div>
          <div class="row form-group">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <label class="label-control"><?=$this->lang->line('Request money back')?></label>
              <br />
              <label class=""> <div class="icheckbox_square-green" style="position: relative;"><input type="checkbox" name="request_money_back" id="request_money_back" class="i-checks" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <?=$this->lang->line('yes')?> </label>

            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default"><?=$this->lang->line('btn_submit')?></button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('close')?></button>
        </div>
      </form>
      <script>
        $("#frmDispute_<?=$job_details['job_id']?>").validate({
          ignore: [], 
          rules: { 
            dispute_title: { required : true },
            dispute_cat_id: { required : true },
            dispute_sub_cat_id: { required : true },
          },
          messages: { 
            dispute_title: { required : <?=json_encode($this->lang->line('Enter dispute title!'))?> },
            dispute_cat_id: { required : <?=json_encode($this->lang->line('Select dispute category!'))?> },
            dispute_sub_cat_id: { required : <?=json_encode($this->lang->line('Select dispute sub category!'))?> },
          },
        });
      </script>
    </div>
  </div>
</div>
<script>
  $(".dispute_cat_id").on('change', function(event) {  event.preventDefault();
    var dispute_cat_id = $(this).val();
    if(dispute_cat_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "<?=base_url('user-panel-services/get-dispute-sub-category')?>", 
        data: { dispute_cat_id: dispute_cat_id },
        dataType: "json",
        success: function(res) { 
          $('.dispute_sub_cat_id').empty();
          $('.dispute_sub_cat_id').append("<option value=''><?=$this->lang->line('sel_subcategory');?></option>");
          $.each( res, function(){$('.dispute_sub_cat_id').append('<option value="'+$(this).attr('dispute_sub_cat_id')+'">'+$(this).attr('dispute_sub_cat_title')+'</option>');});
          $('.dispute_sub_cat_id').focus();
        }
      });
    }
  });
</script>