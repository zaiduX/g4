<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel-services/dashboard-services'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
          <li><a href="<?= $this->config->item('base_url') . 'user-panel-services/services-wallet'; ?>"><span><?= $this->lang->line('e_wallet'); ?></span></a></li>
          <li class="active"><span><?= $this->lang->line('Available milestones'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"> <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Available milestones'); ?>  &nbsp;&nbsp;&nbsp;
      <small class="m-t-md"><?= $this->lang->line('User available milestone details'); ?></small> 
    </div>
  </div>
</div>
    
<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel">
        <div class="panel-body">
          <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
            <thead>
              <tr>
                <th><?= $this->lang->line('Job ID'); ?></th>
                <th><?= $this->lang->line('job_title'); ?></th>
                <th><?= $this->lang->line('Number of milestone'); ?></th>
                <th><?= $this->lang->line('withdraw'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($job_list as $job) { ?>
              <tr>
                <td><?= $job['job_id'] ?></td>
                <td><?= $job['job_title']?></td>
                <td><?php if($job['completed_milestone'] != "offer"){ echo $job['completed_milestone']; }?></td>
                <td>
                  <?php if($job['completed_milestone'] != "offer"){ ?>
                  <form action="<?= $this->config->item('base_url') . 'user-panel-services/job-milestone-for-withdraw'; ?>" method="post">
                    <input type="hidden" name="account_id" value="<?=$account_id?>" />
                    <input type="hidden" name="currency_code" value="<?=$currency_code?>" />
                    <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                    <button type="submit" class="btn btn-outline btn-info" ><i class="fa fa-retweet"></i> <?= $this->lang->line('Milestones'); ?> </button>
                  </form>
                  <?php }elseif($job['completed_milestone'] == "offer" && $job['job_status'] == "completed" && $job['complete_paid'] == 1 && $job['withdraw_status'] == "NULL"){ ?>
                  <form action="<?= $this->config->item('base_url') . 'user-panel-services/create-services-withdraw-request'; ?>" method="post">
                    <input type="hidden" name="account_id" value="<?=$account_id?>" />
                    <input type="hidden" name="currency_code" value="<?=$currency_code?>" />
                    <input type="hidden" name="job_id" value="<?=$job['job_id']?>"/>
                    <input type="hidden" name="milestone_id" value="NULL"/>
                    <button type="submit" class="btn btn-outline btn-info" ><i class="fa fa-retweet"></i> <?= $this->lang->line('withdraw'); ?> </button>
                  </form>
                  <?php }elseif($job['withdraw_status'] != "NULL"){ ?>
                    <button type="button" class="btn btn-outline btn-warning" ><i class="fa fa-check"></i> <?= strtoupper($job['withdraw_status']); ?> </button>
                  <?php } ?>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>