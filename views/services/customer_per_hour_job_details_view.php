<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= $_SERVER['HTTP_REFERER'] ?>"><?= $this->lang->line('Jobs'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Job details'); ?></span></li>
        </ol>
      </div>
      <h3 class="font-light m-b-xs"><i class="fa fa-briefcase text-muted"></i> <?= $this->lang->line('Job details'); ?> </h3> 
    </div>
  </div>
</div>

<div class="content">
  <div class="hpanel hblue" style="margin-top: 0px;">
    <div class="panel-body">
      <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-xs-12" style="padding: 0px 15px 0px 15px">
        
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 0px; margin-bottom: 5px;" class="font-uppercase"><i class="fa fa-tag"></i> Ref #GS-<?=$job_details['job_id']?>&nbsp;&nbsp;&nbsp;<?=$job_details['job_title']?></h4>
          <p>
            <i class="fa fa-clock-o"></i> <?= $this->lang->line('posted on'); ?>: <?=date('D, d M y', strtotime($job_details['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <i class="fa fa-map-marker"></i>&nbsp;<?=$job_details['location_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </p>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h3><?= $this->lang->line('Milestone details'); ?></h3><br>

          <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
            <thead>
              <tr>
                <th><?= $this->lang->line('Milestone id'); ?></th>
                <th><?= $this->lang->line('From date'); ?></th>
                <th><?= $this->lang->line('To date'); ?></th>
                <th><?= $this->lang->line('Work hours'); ?></th>
                <th><?= $this->lang->line('amount'); ?></th>
                <th><?= $this->lang->line('status'); ?></th>
              </tr>
            </thead>
            <tbody class="panel-body">
              <?php foreach ($all_milestone_details as $milestone) { ?>
                <tr>
                  <td><?=$milestone['milestone_id']?></td>
                  <td><?=$milestone['from_date']?></td>
                  <td><?=$milestone['to_date']?></td>
                  <td><?=$milestone['work_hours']?></td>
                  <td><?=$milestone['work_hours']*$job_details['budget']?></td>
                  <td><?=$milestone['status']?></td>    
                  <td>
                    <?php
                     if($milestone['status']=="open"){
                       echo "<span class='label label-warning'>".$milestone['status']."</span>"; 
                      }elseif($milestone['status']=="reject"){
                       echo "<span class='label label-danger'>".$milestone['status']."</span>"; 
                      }else{
                       echo "<span class='label label-success'>".$milestone['status']."</span>"; 
                      }
                    ?>
                  </td>    
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>       
      <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #f1f3f6; padding-bottom: 15px;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; display: inline-flex; border-bottom: 1px dotted gray">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
              <?php if($job_details['job_type']==0){ ?>
              <h2 style="color: #3498db"><?=$job_details['budget']?> <?=$job_details['currency_code']?> <small> <?=($job_details['work_type'] == "per hour")?$this->lang->line('Per Hour'):$this->lang->line('Fixed Price')?> </small></strong></h2>
              <?php }else{ ?>
                <h2 style="color: #3498db"><strong><?=(!$job_details['immediate'])?$this->lang->line('Immediate'):$this->lang->line('On Date')?><small> <?=($job_details['immediate'])?date('d M Y' , strtotime($job_details['complete_date'])):'';?> </small></strong></h2>           
              <?php } ?>
            </div>
          </div>

          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; padding-top: 15px;">
            
            <?php if($cust_id == $job_details['cust_id']){ ?>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($provider_details['avatar_url'])?>">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
              <h3 style="color: #3498db"><strong><?= $provider_details['firstname'] .' '. $provider_details['lastname']; ?></strong></h3>
              <h4><i class="fa fa-map-marker"></i> <?= $this->api->get_country_name_by_id($provider_details['country_id']); ?></h4>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding: 0px">
              (<?=$this->lang->line('Your Job provider')?>)
            </div>
            <?php }else{ ?>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($customer_details['avatar_url'])?>">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
              <h3 style="color: #3498db"><strong><?= $customer_details['firstname'] .' '. $customer_details['lastname']; ?></strong></h3>
              <h4><i class="fa fa-map-marker"></i> <?= $this->api->get_country_name_by_id($customer_details['country_id']); ?></h4>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding: 0px">
              (<?=$this->lang->line('Your Customer')?>)
            </div> 
            <?php } ?>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; display: inline-flex; border-bottom: 1px dotted gray">
              </div>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-top: 10px;font-size: 20px;">
                <label><?=$this->lang->line('Payment details')?></label>
              </div>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;font-size: 16px;padding-left: 0px;">
                <div class="col-md-8" style="padding-left: 0px;">
                  <lable  class="lable lable-primary"></i> <?=$this->lang->line('No. of Milestone')?> : </lable> <br> 
                  <lable  class="lable lable-primary"></i> <?=$this->lang->line('No. of submitted hours')?> : </lable> <br> 
                  <?php if($cust_id == $job_details['cust_id']){ ?> 
                  <lable  class="lable lable-primary"></i> <?=$this->lang->line('Total amount paid')?> : </lable>  <br>
                  <?php }else{ ?> 
                  <lable  class="lable lable-primary"></i> <?=$this->lang->line('Total amount received')?> : </lable>  <br>
                  <?php } ?> 
                </div>
                <div class="col-md-4">
                 <lable style="font-size: 16px"> <?=sizeof($accepted_milestone)?> </lable> <br>
                 <lable style="font-size: 16px"> <?=$submitted_hours['total_hours']." ".$this->lang->line('hours')?> </lable><br>
                 <lable style="font-size: 16px"> <?=$job_payment_paid_history['paid_amount']." ".$job_details['currency_code'] ?> </lable><br>
                </div>
            </div>
             </div>
          </div>
      </div>
    </div>
    <div class="panel-footer">
      <div class="row">
        <div class="col-md-12">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <a href="<?= base_url('user-panel-services/'.$redirect_url); ?>" class="btn btn-info btn-outline"><?= $this->lang->line('back'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  //Scroll chat to down before page display.
  $(window).load(function() {
    var elem = document.getElementById('feedDiv');
    elem.scrollTop = elem.scrollHeight;

  });
</script>
<script>
  $(function () {
  $("#toMapID").geocomplete({
    map:".map_canvas",
    location: "<?=$job_details['address']?>",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: false, },
    details: "form",
    detailsAttribute: "to-data-geo", 
    types: ["geocode", "establishment"],
  });
  });
</script>