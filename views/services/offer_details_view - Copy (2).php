<style>
  .img-slider { width:100%; }
  /*******************
  SWIPPER 
  ********************/
  .swiper-container {
    width: 100%;
    height: 200px;
    margin-left: auto;
    margin-right: auto;
  }
  .swiper-slide {
    background-size: cover;
    background-position: center;
  }
  .gallery-top {
    height: 80%;
    width: 100%;
  }
  .gallery-thumbs {
    height: 20%;
    box-sizing: border-box;
    padding: 10px 0;
  }
  .gallery-thumbs .swiper-slide {
    width: 25%;
    height: 100%;
    opacity: 0.4;
  }
  .gallery-thumbs .swiper-slide-active {
    opacity: 1;
  }

  .sticky {
    position: fixed;
  }

  div.tip span {
    display: none
  }
  div.tip:hover span {
    border: #c0c0c0 1px dotted;
    padding: 5px 5px 5px 5px;
    display: block;
    z-index: 100;
    /*background: url(../images/status-info.png) #f0f0f0 no-repeat 100% 5%;*/
    background-color: #FFF;
    left: 0px;
    margin: 10px 10px 10px -60px;
    width: 250px;
    position: absolute;
    top: -80px;
    text-decoration: none
  }
  div.tip {
    /*border-bottom: 1px dashed;*/
    text-decoration: none;
    text-align: center;
  }
  div.tip:hover {
    cursor: help;
    position: inherit;
  }

  div.tip-fav span {
    display: none
  }
  div.tip-fav:hover span {
    border: #c0c0c0 1px dotted;
    padding: 5px 5px 5px 5px;
    display: block;
    z-index: 100;
    /*background: url(../images/status-info.png) #f0f0f0 no-repeat 100% 5%;*/
    background-color: #FFF;
    left: 0px;
    margin: 10px 10px 10px -60px;
    width: 200px;
    position: absolute;
    top: -40px;
    text-decoration: none
  }
  div.tip-fav {
    /*border-bottom: 1px dashed;*/
    text-decoration: none;
    text-align: center;
  }
  div.tip-fav:hover {
    cursor: help;
    position: inherit;
  }
</style>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/available-offers-list-view'); ?>"><?= $this->lang->line('Offers'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Offer details'); ?></span></li>
        </ol>
      </div>
      <h3 class="font-light m-b-xs"><i class="fa fa-gift text-muted"></i> <?= $offer_details['offer_title']; ?> </h3>  
    </div>
  </div>
</div>

<div class="content">
  <div class="hpanel hblue" style="margin-top: -10px; margin-left: -15px; margin-right: -15px;">
    <div class="panel-body" style="">
      <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-xs-12" style="padding: 0px 15px 0px 15px; margin-top: -10px;" >

        <div class="cars-gallery">
          <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="swiper-zoom-container">
                  <img class="img-slider" src="<?=($offer_details['default_image_url'] != 'NULL') ? base_url($offer_details['default_image_url']) : base_url('resources/no-image.jpg'); ?>">
                </div>
              </div>
              <?php if($offer_details['image_url_1'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_1']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_2'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_2']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_3'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_3']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_4'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_4']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_5'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_5']); ?>">
                  </div>
                </div>
              <?php endif; ?>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
          </div>
          <div class="swiper-container gallery-thumbs" style="padding-top: 5px !important;">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="swiper-zoom-container">
                  <img class="img-slider" src="<?=($offer_details['default_image_url'] != 'NULL') ? base_url($offer_details['default_image_url']) : base_url('resources/no-image.jpg'); ?>">
                </div>
              </div>
              <?php if($offer_details['image_url_1'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_1']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_2'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_2']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_3'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_3']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_4'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_4']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_5'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_5']); ?>">
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('What you get with this Offer'); ?></h4>
        </div>  
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <p><?=$offer_details['offer_desc']?></p>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('What the Provider needs to start the work'); ?></h4>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <p><?=$offer_details['offer_req']?></p>
        </div>

        <?php if($offer_details['add_on_status'] == 1): ?>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
            <h4 style="margin-top: 15px; margin-bottom: 0px;" class="font-uppercase"><?= $this->lang->line('Get more with Offer Add-ons'); ?></h4>
          </div>

          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 15px 15px 10px 15px; background-color: #f1f3f6; margin-top: 15px; border-radius: 5px;">
            <?php
              $add_ons = $this->api->get_offer_add_ons($offer_details['offer_id']); $addon_ids='';
              foreach ($add_ons as $add_on) { $addon_ids .= $add_on['addon_id'].',';  ?>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #FFF; margin-bottom: 5px; border-radius: 5px;">
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-8 col-xs-8 forum-heading" style="padding-left: 0px; padding-bottom: 5px;">
                  <div class="checkbox checkbox-default checkbox-inline" style="margin-left: 0px; margin-right: auto; margin-top: 5px;">
                    <input type="checkbox" id="chk_add_on_<?=$add_on['addon_id']?>" name="chk_add_on_<?=$add_on['addon_id']?>" value="yes" />
                    <label for="chk_add_on_<?=$add_on['addon_id']?>"><strong><?=$add_on['addon_title']?></strong></label>
                  </div><br/>
                  <small class="text-muted"><?= $this->lang->line('Additional'); ?> <?=$add_on['work_day']?> <?= $this->lang->line('working day/s'); ?></small>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-4 text-center forum-info" style="padding-left: 0px; padding-right: 0px;">
                  <h4> <?=$add_on['currency_code']?> <span id="add_on_<?=$add_on['addon_id']?>"><?=$add_on['addon_price']?></span></h4>
                  <input type="hidden" name="add_on_hidden_<?=$add_on['addon_id']?>" id="add_on_hidden_<?=$add_on['addon_id']?>" value="<?=$add_on['addon_id']?>" />
                  <input type="hidden" name="add_on_base_<?=$add_on['addon_id']?>" id="add_on_base_<?=$add_on['addon_id']?>" value="<?=$add_on['addon_price']?>" />
                </div>
              </div>
              <script>
                $("#chk_add_on_<?=$add_on['addon_id']?>").change(function() {
                    if($(this).is(":checked")) {
                      var selected_add_on_ids = $('#selected_add_on_ids').val();
                      if(selected_add_on_ids != '') {
                        add_on_id = $("#add_on_hidden_<?=$add_on['addon_id']?>").val();
                        selected_add_on_ids = selected_add_on_ids + ',' + add_on_id;
                        $('#selected_add_on_ids').val(selected_add_on_ids);
                      } else { $('#selected_add_on_ids').val("<?=$add_on['addon_id']?>"); }

                      var quantity = parseInt($('#quantity').val());
                      var new_offer_price = parseFloat($('#new_offer_price').text());
                      var add_on_base_price = $("#add_on_base_<?=$add_on['addon_id']?>").val();

                      offer_price = ((new_offer_price) + ((parseFloat(add_on_base_price) * quantity)));
                      $('#new_offer_price').text(offer_price);
                    } else { 
                      var add_on_id = parseInt($("#add_on_hidden_<?=$add_on['addon_id']?>").val());
                      var selected_add_on_ids = $('#selected_add_on_ids').val();
                      var selected_add_on_ids = selected_add_on_ids.split(',');

                      selected_add_on_ids = jQuery.grep(selected_add_on_ids, function(value) {
                        return value != add_on_id;
                      });

                      var selected_add_on_ids = selected_add_on_ids.toString();
                      $('#selected_add_on_ids').val(selected_add_on_ids);

                      var quantity = parseInt($('#quantity').val());
                      var new_offer_price = parseFloat($('#new_offer_price').text());
                      var add_on_base_price = $("#add_on_base_<?=$add_on['addon_id']?>").val();

                      offer_price = ((new_offer_price) - ((parseFloat(add_on_base_price) * quantity)));
                      $('#new_offer_price').text(offer_price);
                    }     
                });
              </script>
            <?php } $addon_ids = substr($addon_ids,0,-1); ?>
            <input type="hidden" name="addon_ids" value="<?=$addon_ids?>" id="addon_ids" />
            <input type="hidden" name="no_of_ad_on_selected" value="0" id="no_of_ad_on_selected" />
            <input type="hidden" name="selected_add_on_ids" value="" id="selected_add_on_ids" />
            <?php if($offer_details['quick_delivery'] == 1): ?>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #FFF; margin-bottom: 5px; border-radius: 5px; padding-top: 5px;">
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-8 col-xs-8" style="padding-right: 0px; padding-left: 0;">
                  <div class="checkbox checkbox-default checkbox-inline" style="margin-left: 0px; margin-right: auto;">
                    <input type="checkbox" id="quick_delivery" name="quick_delivery" value="yes" />
                    <label for="quick_delivery"><img style="width: 36px;" src="<?=base_url('resources/icon_delivery.png')?>" />&nbsp;&nbsp;<strong><?= $this->lang->line('I can deliver all work in'); ?>&nbsp;&nbsp;<font color="#59bdd7" style="font-style: italic;"><?=$offer_details['quick_delivery_day']?></font>&nbsp;&nbsp;<?= $this->lang->line('day/s'); ?></strong></label>
                  </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-4 text-center forum-info" style="padding-left: 0px; padding-right: 0px;">
                  <h4 style="margin-top: 5px;"> <?=$offer_details['currency_code']?> <span id="quick_delivery_price"><?=$offer_details['quick_delivery_price']?></span></h4>
                  <input type="hidden" name="quick_delivery_selected_price" id="quick_delivery_selected_price" value="" />
                </div>
              </div>
              <script>
                $("#quick_delivery").change(function() {
                  if($(this).is(":checked")) {
                    var new_offer_price = parseFloat($('#new_offer_price').text());
                    var quick_delivery_price = parseFloat($('#quick_delivery_price').text());
                    new_offer_price = (new_offer_price + quick_delivery_price);
                    $('#new_offer_price').text(new_offer_price);
                    $('#quick_delivery_selected_price').val(quick_delivery_price);
                  } else { 
                    var new_offer_price = parseFloat($('#new_offer_price').text());
                    var quick_delivery_price = parseFloat($('#quick_delivery_price').text());
                    new_offer_price = (new_offer_price - quick_delivery_price);
                    $('#new_offer_price').text(new_offer_price);
                    $('#quick_delivery_selected_price').val(0);
                  }
                });
              </script>
            <?php endif; ?>
          </div>
        <?php endif; ?>

        <?php if($offer_details['review_count']>0): ?>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
            <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('reviews'); ?> (<?=$offer_details['review_count']?>)</h4>
          </div>
        <?php endif; ?>

        <?php if($offer_details['review_count']>0): ?>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
              <div class="v-timeline vertical-container animate-panel" data-child="vertical-timeline-block" data-delay="1" style="    margin-top: 0em !important; margin-bottom: 0em !important;">
                <table id="offerTableData" style="width: 100% !important">
                  <thead><tr class="hidden"><th></th></tr></thead>
                  <tbody>
                    <?php foreach ($offer_reviews as $review) { ?>
                      <tr>
                        <td>
                          <div class="vertical-timeline-block" style="margin-bottom: 25px !important; margin-right: 0px !important">
                            <div class="vertical-timeline-icon navy-bg" style="width: 50px !important; height: 50px !important; left: -5px !important;">
                              <img src="<?=base_url($review['cust_avatar_url'])?>" class="img-responsive img-circle" />
                              <small class="text-muted" style="font-size: small !important; background-color: #e8ebf0 !important; color: #505050"><?=date_format(date_create($review['review_datetime']), 'd M Y')?></small>
                            </div>
                            <div class="vertical-timeline-content" style="box-shadow: 0px 0px 1px 0px #888888; border-radius: 5px;">
                              <div style="padding: 10px 15px 15px 15px !important;">
                                <span class="vertical-date pull-right"><small>
                                  <?php for($i=0; $i<(int)$review['review_rating']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                                  <?php for($i=0; $i<(5-$review['review_rating']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?></small></span>
                                <h5><font><?=$review['cust_fname'] .' '. $review['cust_lname']?></font>&nbsp;&nbsp;&nbsp;<font><i class="fa fa-map-marker"></i> <?= $this->api->get_country_name_by_id($review['cust_country_id']); ?></font></h5>
                                <p><?=$review['review_comment']?></p>
                              </div>
                              <?php if($review['review_reply'] != ''): ?>
                                <div class="panel-footer" style="margin-left: 30px; margin-right: 10px; margin-bottom: -15px; border-radius: 5px; border: 1px solid #eaeaea; box-shadow: 1px 1px 1px 1px #888888;">
                                  <div class="row">
                                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                      <img src="<?=base_url($review['free_avatar_url'])?>" class="img-responsive img-circle" />
                                    </div>
                                    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                      <h5><font><?= ($review['free_company_name'] != 'NULL') ? $review['free_company_name'] : $review['free_fname'] .' '. $review['free_lname']?></font>
                                      <p><?=$review['review_reply']?></p>
                                    </div>
                                  </div>
                                </div>
                              <?php endif; ?>
                            </div>
                          </div>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
        <?php endif; ?>

      </div>

      <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #f1f3f6; padding-bottom: 15px;">

          <div class="row" style="position: fixed; top: 129px; z-index: 3; background-color: #f1f3f6; margin-right: 61px;">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 15px; padding-bottom: 15px; display: inline-flex; padding-left: 0px; padding-right: 0px;">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 style="color: #3498db"><strong><?=$offer_details['currency_code']?> <span id="new_offer_price"><?=$offer_details['offer_price']?></span></strong></h2>
                <input type="hidden" name="offer_price" id="offer_price" value="<?=$offer_details['offer_price']?>" />
                <input type="hidden" name="offer_base_price" id="offer_base_price" value="<?=$offer_details['offer_price']?>" />
              </div>
              <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-4 text-right" style="padding: 15px 15px 0px 0px;">
                <a class="a2a_dd" href="https://www.addtoany.com/share"><i class="fa fa-share-alt"></i> <?= $this->lang->line('Share'); ?></a>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
              </div> -->
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="padding-bottom: 15px;">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                <input type="number" class="form-control" name="quantity" id="quantity" min="1" max="<?=($advance_payment['max_quantity_to_buy_offer'] != null || $advance_payment['max_quantity_to_buy_offer'] > 0)?$advance_payment['max_quantity_to_buy_offer']:10?>" value="1" />
              </div>
              <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12" style="">
                <button type="button" class="btn btn-info form-control"><?= $this->lang->line('Buy Now'); ?></button>
              </div>
            </div>
            
          </div>

          <div class="" style="margin-top: 120px;">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="border-bottom: 1px dotted gray">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 tip" style="padding-left: 0px; padding-right: 0px;">
                <span><?= $this->lang->line('Amount of days required to complete work for this offer as set by the provider'); ?></span>
                <label><i class="fa fa-paper-plane-o fa-2x"></i></label><br />
                <h5><?= $this->lang->line('Delivery in'); ?></h5>
                <label style="color: #3498db"><?= ($offer_details['delivered_in'] == 1) ? $offer_details['delivered_in'].' '.$this->lang->line('Day') : $offer_details['delivered_in'].' '.$this->lang->line('Days') ?></label>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 tip" style="padding-left: 0px; padding-right: 0px;">
                <span><?= $this->lang->line('Rating of the offer as calculated from the buyers review'); ?></span>
                <label><i class="fa fa-thumbs-o-up fa-2x"></i></label><br />
                <h5><?= $this->lang->line('rating'); ?></h5>
                <label style="color: #3498db"><?=$offer_details['offer_rating'].'/5 <br />( '.$offer_details['review_count'].' '.$this->lang->line('reviews') ?>)</label>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 tip" style="padding-left: 0px; padding-right: 0px;">
                <span><?= $this->lang->line('Average time for the provider to first reply on the workstream after purchase of this offer'); ?></span>
                <label><i class="fa fa-clock-o fa-2x"></i></label><br />
                <h5><?= $this->lang->line('Response Time'); ?></h5>
                <label style="color: #3498db"><?= ($offer_details['free_response_time'] <= 6) ? $this->lang->line('Within a few hours') : $offer_details['free_response_time'].' '.$this->lang->line('hours') ?></label>
              </div>
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; padding-top: 10px;">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                <label class="text-muted"><?= $this->lang->line('views'); ?></label>
                <label style="color: #404040"><?=$offer_details['view_count']?></label>
              </div>
              <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12" style="">
                <label class="text-muted"><?= $this->lang->line('Sales'); ?></label>
                <label style="color: #404040"><?=$offer_details['sales_count']?></label>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 tip-fav" style="">
                <span><?=(in_array($offer_details['offer_id'], $favorite_offer_ids)?$this->lang->line('Remove from favorite'):$this->lang->line('Add to favorite'))?></span>
                <a style="padding: 0px;" title="<?=(in_array($offer_details['offer_id'], $favorite_offer_ids)?$this->lang->line('Remove from favorite'):$this->lang->line('Add to favorite'))?>" class="btn-link <?=(in_array($offer_details['offer_id'], $favorite_offer_ids)?'remove_favorite':'add_favorite')?>" data-id="<?=$offer_details['offer_id'];?>"><i size="5px" style="color: red"><i class="fa fa-heart<?=(in_array($offer_details['offer_id'], $favorite_offer_ids)?'':'-o')?>"></i></i></a>&nbsp;&nbsp;&nbsp;
                <label><?= $offer_details['favorite_counts']; ?></label>
              </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; padding-top: 15px;">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($offer_details['free_avatar_url'])?>">
              </div>
              <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12" style="">
                <h3 style="color: #3498db"><strong><?= ($offer_details['free_company_name']!='NULL')?$offer_details['free_company_name']:$offer_details['free_fname']; ?></strong></h3>
                <h4><i class="fa fa-map-marker"></i> <?= $this->api->get_country_name_by_id($offer_details['free_country_id']); ?></h4>
                <h5 style="color: #404040"><?= strtoupper(str_replace(',', '| ', $offer_details['tags'])); ?></h5>
              </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; margin-top: 5px;">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 1px solid #909090; background-color: #FFF">
                <p style="padding-top: 5px;"><?=$offer_details['free_introduction']?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 15px 0px;">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-1"><?= $this->lang->line('Buyer Tips'); ?></a></li>
            <li class=""><a data-toggle="tab" href="#tab-2"><?= $this->lang->line('How it works'); ?></a></li>
          </ul>
          <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
              <div class="panel-body">
                <ul style="padding-left: 15px;">
                  <li><?= $this->lang->line('The Offer price is fixed - you never pay a penny more'); ?></li>
                  <li><?= $this->lang->line('Your money is safe until you agree to release funds to the Provider'); ?></li>
                  <li><?= $this->lang->line('After purchase, you should contact the Provider and let them know about your requirements'); ?></li>
                </ul>
              </div>
            </div>
            <div id="tab-2" class="tab-pane">
              <div class="panel-body">
                <ul style="padding-left: 15px;">
                  <li><?= $this->lang->line('You buy an Offer and your payment is held in escrow'); ?></li>
                  <li><?= $this->lang->line('You contact the Provider and specify your requirements'); ?></li>
                  <li><?= $this->lang->line('Work is delivered'); ?></li>
                  <li><?= $this->lang->line('If you are happy you release the money to the Provider'); ?></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
          <a href="<?= $_SERVER['HTTP_REFERER'] ?>" class="btn btn-info btn-outline"><?= $this->lang->line('back'); ?></a>
        </div>
      </div>
    </div>
  </div>
</div>

<script>

  $("#quantity").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false; }
  });

  $('#quantity').on('input',function(e){
    var quantity = $('#quantity').val();
    var addon_ids = $('#addon_ids').val();
    var addon_ids_array = addon_ids.split(',');
    for (var i = 0; i < addon_ids_array.length; i++) {
      var add_on_price = $('#add_on_base_'+addon_ids_array[i]).val();
      add_on_price = (parseFloat(add_on_price) * parseInt(quantity));
      $('#add_on_'+addon_ids_array[i]).text(add_on_price);
    }

    var offer_base_price = $('#offer_base_price').val();
    var add_price = 0;
    var selected_add_on_ids = $('#selected_add_on_ids').val();
    if(selected_add_on_ids != '') {
      var selected_add_on_ids = $('#selected_add_on_ids').val();
      var selected_add_on_ids = selected_add_on_ids.split(',');
      for (var i = 0; i < selected_add_on_ids.length; i++) {
        add_price = (add_price + parseFloat($('#add_on_'+selected_add_on_ids[i]).text()));
      }
      add_on_price = add_price;
    } else { add_on_price = 0; }

    offer_price = (parseFloat(offer_base_price) * parseInt(quantity));
    offer_price = (offer_price + parseFloat(add_on_price));
    $('#new_offer_price').text(offer_price);
  });
</script>
<script>
  $('#offerTableData').dataTable( {
    "pageLength": 5,
    "bLengthChange": false,
    "bFilter": false,
    "ordering": false,
  } );
</script>
<script>
   var galleryTop = new Swiper('.gallery-top', {
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 10,
      centeredSlides: true,
      slidesPerView: 'auto',
      touchRatio: 0.2,
      slideToClickedSlide: true,
    });
    galleryTop.controller.control = galleryThumbs;
    galleryThumbs.controller.control = galleryTop;
</script>
<script>
  $('.add_favorite').click(function () { 
    var id = $(this).attr("data-id")
    $.post('offer-add-to-favorite', {id: id}, 
    function(res) { 
      //console.log(res);
      if($.trim(res) == "success") { 
        swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Offer added to your favorite list.')); ?>, "success");
        setTimeout(function() { window.location.reload(); }, 2000); 
      } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to add. Try again...')); ?>, "error");  } 
    });
  });
  $('.remove_favorite').click(function () { 
    var id = $(this).attr("data-id")
    $.post('offer-remove-from-favorite', {id: id}, 
    function(res) { 
      //console.log(res);
      if($.trim(res) == "success") { 
        swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Offer removed from favorite list.')); ?>, "success");
        setTimeout(function() { window.location.reload(); }, 2000); 
      } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to remove. Try again...')); ?>, "error");  } 
    });
  });
</script>