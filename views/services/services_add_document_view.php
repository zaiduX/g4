<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/user-profile'); ?>"><?= $this->lang->line('profile'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/documents'); ?>"><?= $this->lang->line('document'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Add'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-file-text-o fa-2x text-muted"></i> <?= $this->lang->line('document'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('upload_important_documents'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="hpanel hblue">
      <form action="<?= base_url('user-panel-services/add-document'); ?>" method="post" class="form-horizontal" id="addDoc" enctype="multipart/form-data">        
        <div class="panel-body">              
          <div class="col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

            <?php if($this->session->flashdata('error')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                </div>
              </div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                </div>
              </div>
            <?php endif; ?>
          
            <div class="row">                
              <div class="form-group">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-6">
                  <label class=""><?= $this->lang->line('title'); ?></label>
                  <input name="title" type="text" class="form-control" placeholder="<?= $this->lang->line('title'); ?>" required />
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6">                                      
                  <label class=""><?= $this->lang->line('document_image'); ?></label>
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button id="document_image" class="btn btn-green col-lg-12"><i class="fa fa-user"></i>&nbsp; <?= $this->lang->line('select_document_image'); ?></button> 
                    </span> 
                  </div>
                  <span id="document_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('document_image_attached'); ?></span>
                  <input type="file" id="document" name="document" class="upload attachment" accept="image/*" onchange="document_name(event)"/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label class=""><?= $this->lang->line('description_optional'); ?></label>
                  <textarea name="description"  class="form-control" id="description" rows="5" placeholder="<?= $this->lang->line('description_optional'); ?>" style="resize: none;"></textarea>
                </div> 
              </div> 
            </div> 
          </div>
        </div>
        <div class="panel-footer"> 
          <div class="row">
             <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
                <a href="<?= base_url('user-panel-services/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>
             </div>
             <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('submit_detail'); ?></button>               
             </div>
           </div>         
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(function() {  
    $("#addDoc").validate({
      ignore: [],
      rules: {
        title: { required: true, },      
        document: { required: true, accept:"jpg,png,jpeg,gif" },
      }, 
      messages: {
        title: { required: "<?= $this->lang->line('title'); ?>",   },
        document: { required: "<?= $this->lang->line('select_document_image'); ?>", accept: "<?= $this->lang->line('only_image_allowed'); ?>"   },
      }
    });
    $("#document_image").on('click', function(e) { e.preventDefault(); $("#document").trigger('click'); });
  });
  function document_name(e){ if(e.target.files[0].name !="") { $("#document_name").removeClass('hidden'); }}
</script>