<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= $_SERVER['HTTP_REFERER'] ?>"><?= $this->lang->line('Jobs'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Job details'); ?></span></li>
        </ol>
      </div>
      <h3 class="font-light m-b-xs"><i class="fa fa-briefcase text-muted"></i> <?= $this->lang->line('Job details'); ?> </h3> 
    </div>
  </div>
</div>

<div class="content">
  <div class="hpanel hblue" style="margin-top: 0px;">
    <div class="panel-body">
      <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-xs-12" style="padding: 0px 15px 0px 15px">

         <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 0px; margin-bottom: 5px;" class="font-uppercase"><i class="fa fa-tag"></i> Ref #GS-<?=$job_details['job_id']?>&nbsp;&nbsp;&nbsp;<?=$job_details['job_title']?></h4>
          <p>
            <i class="fa fa-calendar"></i> <?= $this->lang->line('Purchased on'); ?>: <?=date('D, d M y', strtotime($job_details['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <i class="fa fa-sort-numeric-asc"></i> <?= $this->lang->line('quantity'); ?>: <?=$job_details['offer_qty']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <strong style="color: #3498db;"><i class="fa fa-gears"></i> <?= $this->lang->line('status'); ?>: <?= strtoupper(str_replace('_', ' ', $job_details['job_status']))?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </p>
        </div>  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('What you get with this Offer'); ?></h4>
        </div> 
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <p><?=$job_details['job_desc']?></p>
        </div>

        <?php
          if($job_details['service_type'] == 1):
            $add_ons = $this->api->get_job_purchased_add_ons($job_details['job_id']);
            if($add_ons || $job_details['offer_quick_delivery_charges'] > 0) : ?>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                <h4 style="margin-top: 15px; margin-bottom: 0px;" class="font-uppercase"><?= $this->lang->line('Additional services with this job'); ?></h4>
              </div>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 15px 15px 10px 15px; background-color: #f1f3f6; margin-top: 15px; border-radius: 5px;">
                <?php if($add_ons):
                  foreach ($add_ons as $add_on) { ?>
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #FFF; margin-bottom: 5px; border-radius: 5px;">
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9 forum-heading" style="padding-left: 0px; padding-bottom: 5px;">
                      <h5><?=$add_on['addon_title']?></h5>
                      <small class="text-muted"><?= $this->lang->line('Additional'); ?> <?=$add_on['work_day']?> <?= $this->lang->line('day/s'); ?></small>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center forum-info" style="padding-left: 0px; padding-right: 0px;">
                      <h4> <?=$add_on['currency_code']?> <?=$add_on['total_price']?></h4>
                    </div>
                  </div>
                <?php } endif; ?>
                <?php if($job_details['offer_quick_delivery_charges'] > 0): ?>
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #FFF; margin-bottom: 5px; border-radius: 5px; padding-top: 5px;">
                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1" style="padding-right: 0px; padding-left: 0;">
                      <img style="width: 54px;" src="<?=base_url('resources/icon_delivery.png')?>" />
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-8 forum-heading" style="">
                      <h5 for="quick_delivery" style="margin-top: 10px;"><strong><?= $this->lang->line('I can deliver all work in'); ?>&nbsp;&nbsp;<font color="#59bdd7" style="font-style: italic;"><?=$job_details['offer_quick_delivery_days']?></font>&nbsp;&nbsp;<?= $this->lang->line('day/s'); ?></strong></h5>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center forum-info" style="padding-left: 0px; padding-right: 0px;">
                      <h4> <?=$job_details['currency_code']?> <?=$job_details['offer_quick_delivery_charges']?></h4>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
          <?php endif; ?>
        <?php endif; ?>
         <?php  
         $res=$this->db->query("SELECT * FROM `tbl_smp_job_proposals` WHERE job_id=".$job_details['job_id']." AND provider_id=".$cust_id)->row();
        if($res):  ?>
          <div class="col-md-10 col-md-offset-1" style="background-color: #ccc">
            <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('Your Proposal Description'); ?></h4>
            <p><?=$res->proposal_details?></p>
          </div>

         <?php endif; ?>
           <?php if($job_details['job_type']):
         $this->db->where("job_id" , $job_details['job_id']);
         $ques=$this->db->get("tbl_question_answer")->result();
        ?>

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;font-size: 16px;">
             <label><?= $this->lang->line('location'); ?></label>
              <input type="text" name="toMapID" class="form-control" id="toMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>" value="<?=$job_details['address']?>" disabled>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;font-size: 16px;">
          <div id="map" class="map_canvas" style="width: 100%; height: 200px;">
            
          </div>
        </div>




        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;font-size: 16px">
          <br>
          <label style="font-size: 22px;">FAQ</label><br>

          <?php $i=1; foreach ($ques as $q) { ?>

          <label> &nbsp; Question:- <?=$i;?></label><br>
          <lable class="form-control" style="font-size: 16px;background-color: #f2f2f2;color: #0c0b0b">  <?=$q->question?></lable>
          <label> &nbsp; Answer:-</label>
          <textarea class="form-control" style="resize: none;" disabled>  <?=$q->answer?></textarea> <br>
        
        <?php $i++; } ?> </div><?php endif;?>




      </div>


      <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #f1f3f6; padding-bottom: 15px;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; display: inline-flex; border-bottom: 1px dotted gray">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
              <h2 style="color: #3498db"><strong><?=$job_details['currency_code']?> <?=($job_details['proposal_id'])?$job_details['proposal_price']:$job_details['offer_total_price'];?></strong></h2>
            </div>
          </div>

          <?php if($job_details['provider_id'] == $cust_id) : ?>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; padding-top: 15px;">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
                <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($job_customer_details['avatar_url'])?>">
              </div>
              <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
                <h3 style="color: #3498db"><strong><?= $job_customer_details['firstname'] .' '. $job_customer_details['lastname']; ?></strong></h3>
                <h4><i class="fa fa-map-marker"></i> <?= $this->api->get_country_name_by_id($job_customer_details['country_id']); ?></h4>
              </div>
            </div>
          <?php else: ?>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; padding-top: 15px;">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
                <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($job_provider_profile['avatar_url'])?>">
              </div>
              <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
                <h3 style="color: #3498db"><strong><?= ($job_provider_profile['company_name']!='NULL')?$job_provider_profile['company_name']:$job_provider_profile['firstname']; ?></strong></h3>
                <h4><i class="fa fa-map-marker"></i> <?= $this->api->get_country_name_by_id($job_provider_profile['country_id']); ?></h4>
                <h5 style="color: #404040"><?= $job_provider_profile['introduction']; ?></h5>
              </div>
            </div>
          <?php endif; ?>

          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; display: inline-flex; border-bottom: 1px dotted gray;margin-top: 10px;">
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-top: 10px;font-size: 20px;">
            <label><?=$this->lang->line('Payment Detail')?></label>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;font-size: 15px;">
              <div class="col-md-6 text-right">
                
              <lable></i> <?=$this->lang->line('Job scrow amount')?> : </lable> <br> 
              <lable  ></i> <?=$this->lang->line('Release amount')?> : </lable> <br> 
              <lable></i> <?=$this->lang->line('Remaining amount')?> : </lable>  <br>
              <lable></i> <?=$this->lang->line('Total paid amount')?> : </lable>  
              </div>
              <div class="col-md-6">
                
             <lable> <?= $remain." ".$job_details['currency_code']?></lable><br>
             <lable><?= $paid_amount_history." ".$job_details['currency_code']?></lable><br>
             <lable><?= $max." ".$job_details['currency_code']?></lable><br>
             <lable><?= $job_details['paid_amount']." ".$job_details['currency_code']?></lable>
              </div>
          </div>
        

        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5px 0px">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
            <h4><?= $this->lang->line('Workroom feeds'); ?></h4>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5px 0px;">
            <div class="panel-body" style="overflow: auto; height: 280px;" id="feedDiv">
              <ul style="padding-left: 15px;">
                <?php foreach($workroom as $v => $w): if($w['type']=='job_started' || $w['type']=='payment' || $w['type']=='cancel_request' || $w['type']=='cancel_withdraw' || $w['type']=='job_cancelled'):
                if($w['text_msg'] != '' && $w['text_msg'] != 'NULL') { echo '<li><h5>'. str_replace('_', ' ', strtoupper($w["type"])).'</h5><p>'.$w["text_msg"].'<br /><small style="color:gray">'.$this->lang->line('Posted on').': '.date('l, M / d / Y  h:i a', strtotime($w["cre_datetime"])).'</small></p></li>'; }
                endif; endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <div class="row">
        <div class="col-md-12">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <a href="<?= base_url('user-panel-services/'.$redirect_url); ?>" class="btn btn-info btn-outline"><?= $this->lang->line('back'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  //Scroll chat to down before page display.
  $(window).load(function() {
    var elem = document.getElementById('feedDiv');
    elem.scrollTop = elem.scrollHeight;
  });
</script>
<script>
  $(function () {
  $("#toMapID").geocomplete({
    map:".map_canvas",
    location: "<?=$job_details['address']?>",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: false, },
    details: "form",
    detailsAttribute: "to-data-geo", 
    types: ["geocode", "establishment"],
  });
  });
</script>