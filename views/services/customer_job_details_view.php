<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>
<?php 
 $res=$this->db->query("SELECT * FROM `tbl_smp_job_proposals` WHERE job_id=".$job_details['job_id']." AND provider_id=".$cust_id)->row();
?>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= $_SERVER['HTTP_REFERER'] ?>"><?= $this->lang->line('Jobs'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Job details'); ?></span></li>
        </ol>
      </div>
      <h3 class="font-light m-b-xs"><i class="fa fa-briefcase text-muted"></i> <?= $this->lang->line('Job details'); ?> </h3> 
    </div>
  </div>
</div>

<div class="content">
  <div class="hpanel hblue" style="margin-top: 0px;">
    <div class="panel-body">
      <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-xs-12" style="padding: 0px 15px 0px 15px">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 0px; margin-bottom: 5px;" class="font-uppercase"><i class="fa fa-tag"></i> Ref #GS-<?=$job_details['job_id']?>&nbsp;&nbsp;&nbsp;<?=$job_details['job_title']?></h4>
          <p>
            <i class="fa fa-clock-o"></i> <?= $this->lang->line('posted on'); ?>: <?=date('D, d M y', strtotime($job_details['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <i class="fa fa-map-marker"></i>&nbsp;<?=$job_details['location_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <i class="fa fa-dot-circle-o"></i> <?=$this->lang->line('Proposals').": ".$job_details['proposal_counts']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </p>
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('desc'); ?></h4>
        </div>  
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <p><?=$job_details['job_desc']?></p>
          <?php if($res){ ?>
          <div class="col-md-10 col-md-offset-1" style="background-color: #ccc">
            <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('Your Proposal Description'); ?></h4>
            <p><?=$res->proposal_details?></p>
           </div>

         <?php } ?>
        </div>
        <?php if($job_details['job_type']):
         $this->db->where("job_id" , $job_details['job_id']);
         $ques=$this->db->get("tbl_question_answer")->result();
        ?>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;font-size: 16px">
          <br>
          <label style="font-size: 22px;">FAQ</label><br>

          <?php $i=1; foreach ($ques as $q) { ?>

          <label> &nbsp; Question:- <?=$i;?></label><br>
          <lable class="form-control" style="font-size: 16px;background-color: #f2f2f2;color: #0c0b0b">  <?=$q->question?></lable>
          <label> &nbsp; Answer:-</label>
          <textarea class="form-control" style="resize: none;" disabled>  <?=$q->answer?></textarea> <br>
        <?php $i++; } ?> </div><?php endif;?>

       
      </div>

      <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #f1f3f6; padding-bottom: 15px;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; display: inline-flex; border-bottom: 1px dotted gray">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
              <?php if($job_details['job_type']==0){ ?>
              <h2 style="color: #3498db"><?=$job_details['budget']?> <?=$job_details['currency_code']?> <small> <?=($job_details['work_type'] == "per hour")?$this->lang->line('Per Hour'):$this->lang->line('Fixed Price')?> </small></strong></h2>
              <?php }else{ ?>
                <h2 style="color: #3498db"><strong><?=(!$job_details['immediate'])?$this->lang->line('Immediate'):$this->lang->line('On Date')?><small> <?=($job_details['immediate'])?date('d M Y' , strtotime($job_details['complete_date'])):'';?> </small></strong></h2>           


              <?php } ?>
            </div>
          </div>

          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; padding-top: 15px;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($customer_details['avatar_url'])?>">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
              <h3 style="color: #3498db"><strong><?= $customer_details['firstname'] .' '. $customer_details['lastname']; ?></strong></h3>
              <h4><i class="fa fa-map-marker"></i> <?= $this->api->get_country_name_by_id($customer_details['country_id']); ?></h4>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              &nbsp;
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
              <?php
             
            
              if(!$res){

               if($redirect_url == "job-invitations"){ ?>
                <?php if($job_details['work_type'] == "per hour"){ ?>
                  <form action="<?=base_url('user-panel-services/send-proposal-view-per-hour')?>" method="post">
                <?php }else{ ?>  
                  <form action="<?=base_url('user-panel-services/send-proposal-view')?>" method="post">
                <?php } ?>
                  <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>">
                  <input type="hidden" name="redirect_url" value="customer-job-details-view">
                  <button type="submit" class="btn btn-sm btn-outline btn-success btn-block" id="btnWorkroom"><i class="fa fa-paper-plane"></i> <?=$this->lang->line('Send Proposal')?></button>
                </form>


              <?php } }else{ ?>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; display: inline-flex; border-bottom: 1px dotted gray">
              </div>

              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-top: 10px;font-size: 20px;">
                <label><?=$this->lang->line('Proposal Detail')?></label>
              </div>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;font-size: 16px;">
                <div class="col-md-6 text-right">
                    
                  <lable  class="lable lable-primary"></i> <?=$this->lang->line('amount')?> : </lable> <br> 
                  <lable  class="lable lable-primary"></i> <?=$this->lang->line('deposite')?> : </lable>  
                </div>
                <div class="col-md-6">
                  
                 <lable style="font-size: 16px"> <?=$res->total_price." ".$res->currency_code?></lable><br>
                 <lable style="font-size: 16px"> <?=$res->deposit_price." ".$res->currency_code?></lable>
                </div>
             </div>

            <?php } ?>
            </div>
             </div>
          </div>
          <?php if($job_details['job_type']): ?>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;font-size: 16px;">
             <label><?= $this->lang->line('location'); ?></label>
              <input type="text" name="toMapID" class="form-control" id="toMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>" value="<?=$job_details['address']?>" disabled>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;font-size: 16px;">
            <div id="map" class="map_canvas" style="width: 100%; height: 200px;"></div>
          </div>
        <?php endif; ?>
        <!-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5px 0px">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
            <h4><?= $this->lang->line('Workroom feeds'); ?></h4>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5px 0px;">
            <div class="panel-body" style="overflow: auto; height: 280px;" id="feedDiv">
              <ul style="padding-left: 15px;">
                
              </ul>
            </div>
          </div>
        </div> -->
      </div>
    </div>
    <div class="panel-footer">
      <div class="row">
        <div class="col-md-12">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <a href="<?= base_url('user-panel-services/'.$redirect_url); ?>" class="btn btn-info btn-outline"><?= $this->lang->line('back'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  //Scroll chat to down before page display.
  $(window).load(function() {
    var elem = document.getElementById('feedDiv');
    elem.scrollTop = elem.scrollHeight;

  });
</script>
<script>
  $(function () {
  $("#toMapID").geocomplete({
    map:".map_canvas",
    location: "<?=$job_details['address']?>",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: false, },
    details: "form",
    detailsAttribute: "to-data-geo", 
    types: ["geocode", "establishment"],
  });
  });
</script>