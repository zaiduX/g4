<?php  $email_cut = explode('@', $cust['email1']);  $name = $email_cut[0];  ?>
<style> .avatar { width: 85%; } </style>
<!-- Navigation -->
<aside id="menu">
  <div id="navigation">
    <?php $url = parse_url($cust['avatar_url']);?>
    <div class="profile-picture">
      <a>
        <?php if($cust['avatar_url'] == "NULL"): ?>
          <a href="<?= base_url('user-panel-services/user-profile'); ?>"><img src="<?= base_url('resources/default-profile.jpg'); ?>" class="img-thumbnail m-b avatar" alt="avatar" /></a>
        <?php else: ?>
          <a href="<?= base_url('user-panel-services/user-profile'); ?>"><img src="<?= ( isset($url['scheme']) AND ($url['scheme'] == "https" || $url['scheme'] == "http" )) ? $cust['avatar_url'] : base_url($cust['avatar_url']); ?>" class="img-thumbnail m-b avatar" alt="avatar" /></a>
        <?php endif; ?>
      </a>
      <div class="stats-label text-color">
        <?php if( $cust['firstname'] != "NULL" || $cust['lastname'] != "NULL") : ?>
          <span class="font-extra-bold font-uppercase"><?= $cust['firstname'] . ' ' .$cust['lastname']; ?></span>
        <?php else: ?>
          <span class="font-extra-bold font-uppercase"><?= $name; ?></span>
        <?php endif; ?>
        <div class="dropdown">
          <a class="dropdown-toggle" href="#" data-toggle="dropdown">
            <small class="text-muted"><?= $this->lang->line('profile'); ?><b class="caret"></b></small>
          </a>
          <ul class="dropdown-menu animated flipInX m-t-xs">
            <li><a href="<?= base_url('user-panel-services/user-profile'); ?>"><?= $this->lang->line('edit_profile'); ?></a></li>
            <li><a href="<?= base_url('user-panel-services/change-password'); ?>"><?= $this->lang->line('change_password'); ?></a></li>
            <li class="divider"></li>
            <li><a href="<?= base_url('log-out'); ?>"><?= $this->lang->line('logout'); ?></a></li>
          </ul>
        </div>
      </div>
    </div>
    <ul class="nav" id="side-menu">
      <?php 
        $method_name = $this->router->fetch_method(); 

        $dashboard = array('dashboard_services');
        $basic_info = array('provider_profile','edit_provider_profile','edit_provider_profile_settings','proposal_credit_payment','edit_provider_document');
        $business_photo = array('business_photos');
        $service_provider_offers = array('service_provider_offers_list','add_service_provider_offers_view','edit_service_provider_offers_view');

        // my jobs
        $my_job = array('customer_job_workroom','customer_job_offer_workroom','accept_job_proposal_payment_view');
        $my_jobs = array('my_jobs','job_workstream_list');
        $my_inprogress_jobs = array('my_inprogress_jobs');
        $my_completed_jobs = array('my_completed_jobs');
        $my_cancelled_jobs = array('my_cancelled_jobs');

        //Fav section
        $my_favorite_offers = array('my_favorite_offers','customer_offer_details');
        $my_favorite_job = array('my_favorite_job');
        $my_favorite_freelancers = array('my_favorite_freelancers','provider_profile_view');

        //provider offers
        $provider_offers_list_view = array('provider_offers_list_view','provider_offer_details');

        //provider projects
        $provider_job = array('provider_job_workroom','customer_job_details_view','send_proposal_view','customer_job_details');
        $job_invitations = array('job_invitations');
        $pending_job_proposals = array('pending_job_proposals');
        $provider_jobs = array('provider_jobs');
        $provider_inprogress_jobs = array('provider_inprogress_jobs','provider_submit_hours_for_payment','job_milestone_payment_request_view');
        $provider_completed_jobs = array('provider_completed_jobs');
        $provider_cancelled_jobs = array('provider_cancelled_jobs');

        //services Accounts
        $account_login = array('account_login');
        $services_wallet = array('services_wallet','services_account_history','services_invoices_history','avalaible_milestone_for_withdraw','job_milestone_for_withdraw','create_services_withdraw_request');
        $scrow = array('my_scrow','user_scrow_history');
        $services_withdrawals_request = array('services_withdrawals_request','services_withdrawals_accepted','services_withdrawals_rejected','services_withdrawal_request_details','withdraw_request_sent');
        $support_request = array('get_support_list','get_support','view_support_reply_details');
      ?>
      
      <?php 
        $orderCount = 0;
        if(isset($_SESSION['last_login_date_time'])) {
          $orderCount = $this->api->requested_order_count($_SESSION['cust_id'], $_SESSION['last_login_date_time']); } 
      ?>

      <li class="<?php if(in_array($method_name, $dashboard)) { echo 'active'; } ?>">
        <a href="<?= base_url('user-panel-services/dashboard-services'); ?>"> <i class="fa fa-tachometer" aria-hidden="true"></i> <span class="nav-label"><?= $this->lang->line('dash'); ?></span></a>
      </li>

      <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
        <li class="<?php if(in_array($method_name, $basic_info) || in_array($method_name, $business_photo) ) { echo 'active'; } ?>">
            <a href="#"><i class="fa fa-user" aria-hidden="true"></i> <span class="nav-label"><?= $this->lang->line('Provider Profile'); ?> </span><span class="fa arrow"></span> </a>
          <ul class="nav nav-second-level">
            <li class="<?php if(in_array($method_name, $basic_info)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/provider-profile'); ?>"><?= $this->lang->line('basic_info'); ?></a></li>
            <li class="<?php if(in_array($method_name, $business_photo)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/business-photos'); ?>"><?= $this->lang->line('business_photos'); ?></a></li>            
          </ul>
        </li>
      <?php } ?>

      <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'buyer') { ?>
        <li class="<?php if(in_array($method_name, $my_jobs) || in_array($method_name, $my_inprogress_jobs)|| in_array($method_name, $my_job)|| in_array($method_name, $my_completed_jobs)|| in_array($method_name, $my_cancelled_jobs)  )  { echo 'active'; } ?>">
          <a href="#"><i class="fa fa-folder-open" aria-hidden="true"></i> <span class="nav-label"><?= $this->lang->line('My Jobs'); ?></span><span class="fa arrow"></span> </a>
          <ul class="nav nav-second-level">
            <li class="<?php if(in_array($method_name, $my_jobs)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/my-jobs'); ?>"><?= $this->lang->line('Open'); ?></a></li>
            <li class="<?php if(in_array($method_name, $my_inprogress_jobs)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/my-inprogress-jobs'); ?>"><?= $this->lang->line('in_progress'); ?></a></li>
            <li class="<?php if(in_array($method_name, $my_completed_jobs)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/my-completed-jobs'); ?>"><?= $this->lang->line('Completed'); ?></a></li>
            <li class="<?php if(in_array($method_name, $my_cancelled_jobs)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/my-cancelled-jobs'); ?>"><?= $this->lang->line('cancelled'); ?></a></li>
          </ul>
        </li>
      <?php } ?>

        <li class="<?php if(in_array($method_name, $my_favorite_offers) || in_array($method_name, $my_favorite_job) || in_array($method_name, $my_favorite_freelancers)) { echo 'active'; } ?>">
          <a href="#"><i class="fa fa-star" aria-hidden="true"></i> <span class="nav-label"><?= $this->lang->line('Favorite'); ?></span><span class="fa arrow"></span> </a>
          <ul class="nav nav-second-level">
            <li class="<?php if(in_array($method_name, $my_favorite_offers)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/my-favorite-offers'); ?>"><?= $this->lang->line('Offers'); ?></a></li>

            <?php if($cust['acc_type'] == 'seller' || $cust['acc_type'] == 'both') { ?>  
            <li class="<?php if(in_array($method_name, $my_favorite_job)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/my-favorite-jobs'); ?>"><?= $this->lang->line('Projects'); ?></a></li>
            <?php } ?>
            <?php if($cust['acc_type'] == 'buyer' || $cust['acc_type'] == 'both') { ?>  
            <li class="<?php if(in_array($method_name, $my_favorite_freelancers)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/my-favorite-freelancers'); ?>"><?= $this->lang->line('Providers'); ?></a></li>
            <?php } ?>
          </ul>
        </li>

      <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
        <li class="<?php if(in_array($method_name, $provider_offers_list_view)){ echo 'active'; } ?>">
          <a href="<?= base_url('user-panel-services/provider-offers'); ?>"><i class="fa fa-gift" aria-hidden="true"></i> <span class="nav-label"><?= $this->lang->line('My Offers'); ?></span></a>
        </li>
      <?php } ?>

      <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
        <li class="<?php if(in_array($method_name, $job_invitations) || in_array($method_name, $pending_job_proposals) || in_array($method_name, $provider_jobs) || in_array($method_name, $provider_inprogress_jobs) || in_array($method_name, $provider_completed_jobs) || in_array($method_name, $provider_cancelled_jobs) || in_array($method_name, $provider_job) ){ echo 'active'; } ?>">
          <a href="#"><i class="fa fa-folder-open-o" aria-hidden="true"></i><span class="nav-label"><?= $this->lang->line('projects'); ?></span> <span class="fa arrow"></span> </a>
          <ul class="nav nav-second-level">
            <li class="<?php if(in_array($method_name, $job_invitations)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/job-invitations')?>"><?= $this->lang->line('job Invitation'); ?></a></li>
            <li class="<?php if(in_array($method_name, $pending_job_proposals)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/pending-job-proposals')?>"><?= $this->lang->line('Send Proposals'); ?></a></li>
            <li class="<?php if(in_array($method_name, $provider_jobs)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/provider-jobs')?>"><?= $this->lang->line('Open'); ?></a></li>
            <li class="<?php if(in_array($method_name, $provider_inprogress_jobs)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/provider-inprogress-jobs')?>"><?= $this->lang->line('in_progress'); ?></a></li>
            <li class="<?php if(in_array($method_name, $provider_completed_jobs)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/provider-completed-jobs')?>"><?= $this->lang->line('Completed'); ?></a></li>
            <li class="<?php if(in_array($method_name, $provider_cancelled_jobs)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/provider-cancelled-jobs')?>"><?= $this->lang->line('cancelled'); ?></a></li>
          </ul>
        </li>
      <?php } ?>

      <li class="<?php if(in_array($method_name, $account_login) || in_array($method_name, $services_wallet) || in_array($method_name, $scrow) || in_array($method_name, $services_withdrawals_request)) { echo 'active'; } ?>">
        <a href="#"><i class="fa fa-money" aria-hidden="true"></i> <span class="nav-label"><?= $this->lang->line('my_accounts'); ?></span><span class="fa arrow"></span> </a>
        <ul class="nav nav-second-level">
          <li class="<?php if(in_array($method_name, $services_wallet)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/account-login/1'); ?>"><?= $this->lang->line('e_wallet'); ?></a></li>
          <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?><li class="<?php if(in_array($method_name, $scrow)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/account-login/2'); ?>"><?= $this->lang->line('scrow_account'); ?></a></li><?php } ?>
          <li class="<?php if(in_array($method_name, $services_withdrawals_request)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-services/account-login/3'); ?>"><?= $this->lang->line('withdrawals'); ?></a></li>
        </ul>
      </li>

      <li class="<?php if(in_array($method_name, $support_request)) { echo 'active'; } ?>"><a href="#"><i class="fa fa-question-circle"></i> <?= $this->lang->line('get_support'); ?></a>
      </li>
    </ul>
  </div>
</aside>
<!-- Main Wrapper -->
<?php
$method_name = $this->router->fetch_method(); 
if($method_name == 'dashboard_services') { ?>
  <div id="wrapper" style="background: url(<?=base_url('resources/services_services.jpg')?>); background-repeat: no-repeat; background-size: 100%;" >
<?php } else { ?>
  <div id="wrapper" style="" >
<?php } ?>