<style type="text/css">
.container {
  width: 60%;
  margin:0 auto;
  padding: 20px 0
}
.drop-shadow {
    position:relative;
    width:100%;
    padding:3em 1em;
    margin:2em 10px 4em;
    background:#fff;
    -webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
       -moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
      box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
}

.drop-shadow:before,
.drop-shadow:after {
    content:"";
    position:absolute;
    z-index:-2;
}

.drop-shadow p {
    font-size:1.5em;
    font-weight:300;
    text-align:center;
}

.lifted {
    -moz-border-radius:4px;
   border-radius:4px;
}

.lifted:before,
.lifted:after {
    bottom:15px;
    left:10px;
    width:50%;
    height:20%;
    max-width:300px;
    max-height:100px;
    -webkit-box-shadow:0 15px 10px rgba(0, 0, 0, 0.7);
       -moz-box-shadow:0 15px 10px rgba(0, 0, 0, 0.7);
      box-shadow:0 15px 10px rgba(0, 0, 0, 0.7);
    -webkit-transform:rotate(-3deg);
       -moz-transform:rotate(-3deg);
  -ms-transform:rotate(-3deg);
   -o-transform:rotate(-3deg);
      transform:rotate(-3deg);
}

.lifted:after {
    right:10px;
    left:auto;
    -webkit-transform:rotate(3deg);
       -moz-transform:rotate(3deg);
  -ms-transform:rotate(3deg);
   -o-transform:rotate(3deg);
      transform:rotate(3deg);
}


#attachment, .attachment { 
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px; 
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0); 
  }
  .entry:not(:first-of-type) {
    margin-top: 10px;
  }
</style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?=$this->lang->line('Troubleshoot a Job')?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-wpforms fa-2x text-muted"></i> <span style="font-size: 30px;"><?=$this->lang->line('Post Troubleshoot your job')?> </span></h2>
    </div>
  </div>
</div>
 
<div class="content">
  <div class="row">
    <div class="hpanel hblue">
      <form action="<?= base_url('user-panel-services/register-troubleshoot-job'); ?>" method="post" class="form-horizontal" id="customer_job_troubleshoot" enctype="multipart/form-data">
        <div class="panel-body">
          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <label class=""><?= $this->lang->line('WHAT DO YOU NEED TO GET DONE?'); ?> </label>
              <input id="job_title" name="job_title" type="text" class="form-control" placeholder="<?= $this->lang->line('e.g I want to bug fixing of my website'); ?>" autocomplete="none"> <br/>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
              <label class=""><?= strtoupper($this->lang->line('Category')); ?> </label>
              <select class="select2 form-control" name="category" id="category">
                  <option value=""><?= $this->lang->line('select'); ?></option>
                  <?php foreach ($category_types as $c ): ?>
                    <option value="<?= $c['cat_type_id']?>"><?= $c['cat_type']?></option>
                  <?php endforeach; ?>
              </select><br/>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
              <label class=""><?= strtoupper($this->lang->line('Sub Category')); ?> </label>
              <select class="select2 form-control" name="sub_category" id="sub_category" disabled>
                  <option value=""><?= $this->lang->line('select'); ?></option>
              </select><br/>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <label class=""><?= strtoupper($this->lang->line('desc')); ?> </label>
              <textarea style="resize:none"  rows="5" id="description" name="description" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Provide a more detailed description to help you get better proposal'); ?>" autocomplete="none"></textarea> <br/>
            </div>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                 <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="font-size: 16px;">
                  <div class='col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 radio radio-success'>
                    <input type='radio' id='immdediate' name="immdediate" value='0' class="imdi" checked="checked">
                    <label for="immdediate"><strong> <?=$this->lang->line('Immediate')?></strong></label>
                  </div>
                  <div class='col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 radio radio-success'>
                    <input type='radio' id='on_date' class="imdi" name="immdediate" value='1'>
                    <label for="on_date"><strong> <?=$this->lang->line('On Date')?></strong></label>
                  </div>
               </div>
               <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5  col-xs-12 text-rihgt">                
                <div class="input-group date" data-provide="datepicker" id="datepickers" data-date-start-date="<?=date("m/d/Y")?>" data-date-end-date="" style="">
                  <input type="text" class="form-control" id="start_date" name="start_date" value="" autocomplete="off"  disabled />
                  <div class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </div>
                </div>
              </div>
              </div>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-top: 30px;font-size: 15px;">
            <label><?= $this->lang->line('search_address_in_map_booking'); ?></label>
            <input type="text" name="toMapID" class="form-control" id="toMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>">
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
            <div id="map" class="map_canvas" style="width: 100%; height: 200px;"></div>
          </div>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  
                  <h2 class="text-center"><b><?= $this->lang->line('Questions');?></b></h2>
                  <div id="ques">
                
                  </div>                  
                </div>
            <!--  -->

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
             <br/> <input type="submit" id="post_project" value="<?=strtoupper($this->lang->line('Post project'))?>" class="btn btn-success">
            </div>
          </div>
         

          <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
            <div class="drop-shadow lifted">
              <h3 class="text-center" style="margin-top:-20px;"><?=$this->lang->line('USEFUL TIPS')?></h3>
              <ul> 
                <li><?=$this->lang->line('useful tips 1')?></li><br>
                <li><?=$this->lang->line('useful tips 2')?></li><br>
                <li><?=$this->lang->line('useful tips 3')?></li><br><br>
                <li><?=$this->lang->line('useful tips 4')?> <a href="#" style="color: blue">"<?=$this->lang->line('Hiring a freelancer')?>"</a></li><br>
              </ul>
            </div>
          </div>
        </div>
        <input type="hidden" to-data-geo="lat" name="latitude" id="latitude" value="" />
            <input type="hidden" to-data-geo="lng" name="longitude" id="longitude" value="" />
            <input type="hidden" to-data-geo="formatted_address" id="street" name="street" class="form-control" placeholder="<?= $this->lang->line('street_name'); ?>" />
            <input type="hidden" to-data-geo="postal_code" id="zipcode" name="zipcode" class="form-control" placeholder="<?= $this->lang->line('zip_code'); ?>"  />
            <input type="hidden" to-data-geo="country" id="country" name="country" class="form-control" placeholder="<?= $this->lang->line('country'); ?>" readonly/>
            <input type="hidden" to-data-geo="administrative_area_level_1" id="state" name="state" class="form-control" id="state" placeholder=" <?= $this->lang->line('state'); ?>" readonly/>
            <input type="hidden" to-data-geo="locality" id="city" name="city" class="form-control" placeholder="<?= $this->lang->line('city'); ?>" readonly/>
            <input type="hidden" name="size" id="size">
      </form>
    </div>
  </div>
</div>
<script>
  $(function () {
    var on_date=0;
    $(document).on('change' , '.imdi' , function () {
      on_date=this.value;
      // console.log(this.value);
      if(this.value==0){

        $("#start_date").val('');
        $("#start_date").attr("disabled" ,true);
       }else{
        $("#start_date").removeAttr("disabled");

      }
    });


  $("#toMapID").geocomplete({
    map:".map_canvas",
    location: "",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: false, },
    details: "form",
    detailsAttribute: "to-data-geo", 
    types: ["geocode", "establishment"],
  });
  $("#category").on('change', function(event) { event.preventDefault();
    $('#sub_category').empty();
    $('#ques').empty();
    var d = <?= json_encode($this->lang->line("no sub category found"))?>;
    var cat_type_id = $(this).val();

    $.post('get-sub-category', {cat_type_id: cat_type_id}, function(sub_category) {
      // console.log(sub_category);
      sub_category = $.parseJSON(sub_category);
      if(sub_category.length > 0 ){
        $('#sub_category').attr('disabled', false);
          $('#sub_category').empty(); 
          $('#sub_category').append('<option value=""><?= $this->lang->line("select")?></option>');
          $.each(sub_category, function(){    
            $('#sub_category').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');              
          });
          $('#sub_category').focus();
      }else{
          $('#sub_category').attr('disabled', true);
          $('#sub_category').empty();
          $('#sub_category').append('<option value="0">'+d+'</option>');
        }
    });

    $.post('get-questions-know-freelancer', {cat_type_id: cat_type_id}, function(questions) {
      // console.log(questions.length);
      questions = $.parseJSON(questions);
      var i=0;
      if(questions.length > 0 ){

          i=1;
          $.each(questions, function(){    
              $('#ques').append('<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; margin-top: 10px;"><div class="row"><div class="col-xl-1 col-lg-1 col-md-1 col-sm-2"><label class="form-control" style="font-size: 16px;border: none"> Q.'+i+'-</label></div><div class="col-xl-11 col-lg-11 col-md-11 col-sm-10"><lable class="form-control" style="font-size: 16px;background-color: #f2f2f2;color: #0c0b0b">'+$(this).attr('question_title')+'</lable></div>               </div><input type="hidden" name="ques_'+i+'" value="'+$(this).attr('question_title')+'"><div class="row">                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-2" ><label class="form-control" style="font-size: 16px;border: none"> Ans. </label></div><div class="col-xl-11 col-lg-11 col-md-11 col-sm-10"><textarea style="resize:none"  rows="3" id="ans_'+i+'" name="ans_'+i+'" type="textarea" class="form-control"  autocomplete="none"></textarea></div></div>   </div>');  
              ++i;            
          });

      }
      $("#size").val(i-1);
      // console.log(i-1);
    });
  });

  $("#post_project").on('click', function(event) { event.preventDefault();
    var job_title = $("#job_title").val();
    var category = $("#category").val();
    var sub_category = $("#sub_category").val();
    var description = $("#description").val();
    var toMapID = $("#toMapID").val();
    var start_date = $("#start_date").val();

    if(job_title == "") { $("#job_title").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Job Title"); ?>', 'warning', false, "#DD6B55");}
    else if(category == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("sel_category"); ?>', 'warning', false, "#DD6B55"); }
    else if(sub_category == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("sel_subcategory"); ?>', 'warning', false, "#DD6B55"); }
    else if(description == "") {$("#description").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("please_enter_description"); ?>', 'warning', false, "#DD6B55"); }
    else if (!start_date==true && on_date=="1") {$("#start_date").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("please_select_date"); ?>', 'warning', false, "#DD6B55"); }
    else if(!toMapID == true) { $("#toMapID").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Please select address"); ?>', 'warning', false, "#DD6B55"); }
    
    else{
      // alert("submit");
      $("#customer_job_troubleshoot").submit(); 
    }
  });
});
</script> 
