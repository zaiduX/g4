<style>
    * { box-sizing: border-box }
    body { font-family: "Lato", sans-serif; }
    /* Style the tab */
    .tab {
        float: left;
        border: 1px solid #225595;
        background-color: #3498db;
        width: 30%;
        height: 400px;
    }
    /* Style the buttons inside the tab */
    .tab button {
        display: block;
        background-color: #3498db;
        color: white;
        padding: 22px 16px;
        width: 100%;
        border: none;
        outline: none;
        text-align: left;
        cursor: pointer;
        transition: 0.3s;
        font-size: 17px;
    }
    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #225595;
    }
    /* Create an active/current "tab button" class */
    .tab button.active {
        background-color: #225595;
    }
    /* Style the tab content */
    .tabcontent {
        float: left;
        padding: 0px 12px;
        border: 1px solid #225595;
        width: 70%;
        border-left: none;
        height: 400px;
    }
</style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Offer Payment'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Offer Payment'); ?></h2>
      <small class="m-t-md"><?= $this->lang->line('Choose your payment option and confirm your offer purchase.'); ?></small>
    </div>
  </div>
</div>
        
<div class="content" style="padding-top: 65px;">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">

      <?php if($this->session->flashdata('error_promo')):  ?>
        <div class="row">
          <div class="form-group"> 
            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error_promo'); ?></div>
          </div>
        </div>
      <?php endif; ?>
      <?php if($this->session->flashdata('success_promo')):  ?>
        <div class="row">
          <div class="form-group"> 
            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
          </div>
        </div>
      <?php endif; ?>

      <div class="hpanel">
        <div class="panel-body">
          <?php $offer_total_price = $details['offer_total_price']; ?>

          <div class="tab" style="width:20%">
            <button class="tablinks" onclick="openTab(event, 'Payment')" id="defaultOpen"><?= $this->lang->line('complete_payment'); ?></button>
          </div>

          <div id="Payment" class="tabcontent" style="width:80%">
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                <h2><?= $this->lang->line('Offer Payment'); ?></h2>
              </div>
            </div>

            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                <div class="hpanel stats">
                  <div class="panel-body" style="padding-top:0px; padding-bottom:0px">
                    <div class="row">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                        <i class="pe-7s-cash fa-5x"></i>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                        <h1 class="text-success">
                          <strong><?= $this->lang->line('Pay') .' '. $details['currency_code'] . ' '. $offer_total_price ?></strong>
                        </h1>
                        <?php if($details['promo_code'] != 'NULL') { ?>
                          <span class="text-muted" style="font-size: large; background-color: gray; border-radius: 15px; color: #FFF; padding: 5px 15px; box-shadow: 0px 0px 1px 3px #8888882b"><s><?= $details['currency_code'] . ' '. $details['actual_price'] ?></s>&nbsp;&nbsp;&nbsp;<?= $details['discount_percent'] . '% '. $this->lang->line('Promotional discount'); ?></span>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                        <p style="margin: 15px 0px 5px 0px;">
                          <?= $this->lang->line('str_c_pay'); ?>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <?php if($details['promo_code'] == 'NULL') { ?>
              <form action="<?=base_url('user-panel-services/offer-promocode')?>" method="post" id="submit_promo_form">
                <div class="row" style="padding:10px">
                  <div class="col-xl-8 col-xl-offset-2 col-xl-8 col-xl-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                    <div class="col-md-10 text-center">
                    <input type="hidden" name="job_id" id="job_id" value="<?= $details['job_id']; ?>">
                    <input type="text" class="form-control" name="promo_code" id="promo_code" placeholder="<?= $this->lang->line('Enter Promocode'); ?>" style="text-transform:uppercase;" autocomplete="off"/>
                    </div>
                    <div class="col-md-2" style="padding-left:0px; padding-right:0px; ">
                      <button type="submit" class="btn btn-info" id="btn_submit_promo"><?= $this->lang->line('Apply'); ?></button>
                    </div>
                  </div>
                </div>
              </form>
            <?php } ?>

            <?php
              //Get User Payment Methods from profile
              function searchForId($id, $array) {
                foreach ($array as $key => $val) { if ($val['brand_name'] === $id) { return $key; }
                } return null;
              }
              $payment_mode = $this->user->get_customer_payment_methods(trim($details['cust_id']));
              //echo json_encode($payment_mode);
              $posMTN = searchForId("MTN",$payment_mode);
              //Get user details
              $user_details = $this->api->get_user_details(trim($details['cust_id']));

              $sender_reference = trim($details['cust_id']);
              $currency = trim($details['currency_code']);
              $lang = substr($_SESSION['language'], 0, 2);
            ?>
            <div class="row text-center">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                <div style="display: inline-flex;">
                  <?php if($currency == 'XAF' || $currency == 'XOF') { ?>
                    <a style="cursor: pointer;" data-toggle="modal" data-target="#paymentMTN" class="btn btn-warning btn-outline"><img style="padding:0px;  margin: 0px;" src="<?=base_url('resources/mtn-logo.png')?>">  &nbsp;&nbsp;&nbsp;<?= $this->lang->line('Pay By MTN'); ?></a>&nbsp;&nbsp;&nbsp;
                    <div class="modal fade" id="paymentMTN" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><?= $this->lang->line('Enter MTN Mobile Number'); ?></h4>
                          </div>
                          <form id="formmomo" method="POST" action="<?=base_url('user-panel-services/offer-payment-mtn')?>" target="_top">
                            <input type="hidden" name="job_id" value="<?=$details['job_id']?>">
                            <input type="hidden" name="offer_total_price" value="<?= $offer_total_price; ?>" />
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-md-12 form-group">
                                  <div class="col-md-12">
                                    <label class="form-label"><?= $this->lang->line('MTN Mobile Number'); ?></label>
                                    <input type="text" name="phone_no" id="phone_no" class="form-control" placeholder="<?= $this->lang->line('Enter MTN Mobile Number'); ?>" value="<?=(isset($posMTN))?$payment_mode[$posMTN]['card_number']:'';?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
                              <button type="submit" class="btn btn-info" id="btn_submit"><?= $this->lang->line('Pay Now'); ?></button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  <?php } ?>

                  <form  class="pay" action="<?= base_url('user-panel-services/offer-payment-stripe');?>" method="POST">
                    <input type="hidden" name="job_id" value="<?= $details['job_id']; ?>" />
                    <script
                      src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                      data-key="pk_test_zbkyUBSvx5wMSon5cemiBCqO"
                      data-amount="<?= $details['offer_total_price']; ?>"
                      data-name="Gonagoo"
                      data-description="<?= $this->lang->line('Offer Payment'); ?>"
                      data-image="<?=base_url('resources/images/72x72-payment.png')?>"
                      data-locale="auto"
                      data-zip-code="false"
                      data-allow-remember-me=false
                      data-label="<?= $this->lang->line('pay_with_card'); ?>"
                      data-currency="<?= $details['currency_code']; ?>"
                      data-email="<?=$user_details['email1']?>"
                      >
                    </script>
                    <a style="cursor:pointer;" id="btn_mtn_pay" class="btn btn-info btn-outline"><img src="<?=base_url('resources/card_icon.png')?>">&nbsp;&nbsp;&nbsp;<?= $this->lang->line('pay_with_card'); ?></a>
                  </form>&nbsp;&nbsp;&nbsp;

                  <form  class="pay" action="<?= base_url('user-panel-services/offer-payment-orange');?>" method="POST" id="pay_orange">
                    <input type="hidden" name="job_id" value="<?= $details['job_id']; ?>" />
                    <a style="cursor:pointer;" id="btn_orange_pay" class="btn btn-danger btn-outline"><img src="<?=base_url('resources/logo-orange.png')?>">&nbsp;&nbsp;&nbsp;<?= $this->lang->line('Pay with Orange Money'); ?></a>
                  </form>
                  
                  <?php if($customer_details['is_dedicated'] == 1) { ?>
                    &nbsp;&nbsp;&nbsp;
                    <form action="<?= base_url('user-panel-services/offer-payment-bank');?>" method="POST">
                      <input type="hidden" name="job_id" value="<?=$details['job_id']?>" />
                      <button type="submit" class="btn btn-warning btn-outline btn-lg"><i class="fa fa-university"></i> <?= $this->lang->line('By Bank'); ?></button>
                    </form>
                  <?php } ?>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function openTab(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  document.getElementById("defaultOpen").click();
</script>
<script>
  $("#formmomo").validate({
    ignore: [],
    rules:{ phone_no: { required:true }, },
    messages:{ phone_no: { required:"Please enter MTN mobile number!" }, }
  });
</script>
<script>
  $(".stripe-button-el").addClass("hidden");
  $('#btn_orange_pay').click(function(){
    $('#pay_orange').submit();
  });
  $('#btn_mtn_pay').click(function(){
    $('.stripe-button-el').click();
  });
</script>
<script>
  $("#submit_promo_form").validate({
    ignore: [],
    rules: { promo_code: {  required:true, }, },
    messages: { promo_code: { required:"<?=$this->lang->line('Please enter promocode!')?>"}, }
  });
</script>
<script>
  $('#btn_submit_promo').click(function(e){
    e.preventDefault();
    var promocode = $('#promo_code').val();
    var job_id = $('#job_id').val();
    $.ajax({
      type: "POST", 
      url: "<?=base_url('user-panel-services/offer-promocode-ajax'); ?>", 
      data: { job_id : job_id , promocode : promocode },
      success: function(res) {
        var respp   = res.split('@');
        if(respp[0] == "success"){
          var lines = respp[1].split('$');
          swal( {
            title:lines[0] ,
            text: lines[1],
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?= $this->lang->line('Yes Use this promocode');?>",
            cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
            closeOnConfirm: false,
            closeOnCancel: false 
          },
          function (isConfirm) {
            if (isConfirm) {
              $('#submit_promo_form').submit();
              //swal("<?= $this->lang->line('success'); ?>", "Promocode code applied", "success");
            } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Promocode not used'); ?>", "error"); }
          });
        } else { swal ( "<?= $this->lang->line('error'); ?>" ,  respp[1] ,  "error" ) }
      }
    });
  });
</script>