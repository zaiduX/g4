<style>
  .img-slider { width:100%; }
  /*******************
  SWIPPER 
  ********************/
  .swiper-container {
    width: 100%;
    height: 200px;
    margin-left: auto;
    margin-right: auto;
  }
  .swiper-slide {
    background-size: cover;
    background-position: center;
  }
  .gallery-top {
    height: 80%;
    width: 100%;
  }
  .gallery-thumbs {
    height: 20%;
    box-sizing: border-box;
    padding: 10px 0;
  }
  .gallery-thumbs .swiper-slide {
    width: 25%;
    height: 100%;
    opacity: 0.4;
  }
  .gallery-thumbs .swiper-slide-active {
    opacity: 1;
  }

  .sticky {
    position: fixed;
  }

  div.tip span {
    display: none
  }
  div.tip:hover span {
    border: #c0c0c0 1px dotted;
    padding: 5px 5px 5px 5px;
    display: block;
    z-index: 100;
    /*background: url(../images/status-info.png) #f0f0f0 no-repeat 100% 5%;*/
    background-color: #FFF;
    left: 0px;
    margin: 10px 10px 10px -60px;
    width: 250px;
    position: absolute;
    top: -80px;
    text-decoration: none
  }
  div.tip {
    /*border-bottom: 1px dashed;*/
    text-decoration: none;
    text-align: center;
  }
  div.tip:hover {
    cursor: help;
    position: inherit;
  }

  div.tip-fav span {
    display: none
  }
  div.tip-fav:hover span {
    border: #c0c0c0 1px dotted;
    padding: 5px 5px 5px 5px;
    display: block;
    z-index: 100;
    /*background: url(../images/status-info.png) #f0f0f0 no-repeat 100% 5%;*/
    background-color: #FFF;
    left: 0px;
    margin: 10px 10px 10px -60px;
    width: 200px;
    position: absolute;
    top: -40px;
    text-decoration: none
  }
  div.tip-fav {
    /*border-bottom: 1px dashed;*/
    text-decoration: none;
    text-align: center;
  }
  div.tip-fav:hover {
    cursor: help;
    position: inherit;
  }
</style>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/provider-offers'); ?>"><?= $this->lang->line('Offers'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Offer details'); ?></span></li>
        </ol>
      </div>
      <h3 class="font-light m-b-xs"><i class="fa fa-gift text-muted"></i> <?= $offer_details['offer_title']; ?> </h3> 
    </div>
  </div>
</div>

<div class="content">
  <div class="hpanel hblue" style="margin-top: -20px;">
    <div class="panel-body">
      <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-xs-12" style="padding: 0px 15px 0px 15px">

        <div class="cars-gallery">
          <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="swiper-zoom-container">
                  <img class="img-slider" src="<?=($offer_details['default_image_url'] != 'NULL') ? base_url($offer_details['default_image_url']) : base_url('resources/no-image.jpg'); ?>">
                </div>
              </div>
              <?php if($offer_details['image_url_1'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_1']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_2'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_2']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_3'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_3']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_4'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_4']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_5'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_5']); ?>">
                  </div>
                </div>
              <?php endif; ?>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
          </div>
          <div class="swiper-container gallery-thumbs" style="padding-top: 5px !important;">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="swiper-zoom-container">
                  <img class="img-slider" src="<?=($offer_details['default_image_url'] != 'NULL') ? base_url($offer_details['default_image_url']) : base_url('resources/no-image.jpg'); ?>">
                </div>
              </div>
              <?php if($offer_details['image_url_1'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_1']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_2'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_2']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_3'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_3']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_4'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_4']); ?>">
                  </div>
                </div>
              <?php endif; ?>
              <?php if($offer_details['image_url_5'] != 'NULL'): ?>
                <div class="swiper-slide">
                  <div class="swiper-zoom-container">
                    <img class="img-slider" src="<?= base_url($offer_details['image_url_5']); ?>">
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('What you get with this Offer'); ?></h4>
        </div>  
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <p><?=$offer_details['offer_desc']?></p>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('What the Provider needs to start the work'); ?></h4>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <p><?=$offer_details['offer_req']?></p>
        </div>

        <?php if($offer_details['add_on_status'] == 1): ?>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 15px 15px 10px 15px; background-color: #f1f3f6; margin-top: 15px; border-radius: 5px;">
            <?php
              $add_ons = $this->api->get_offer_add_ons($offer_details['offer_id']);
              foreach ($add_ons as $add_on) { ?>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #FFF; margin-bottom: 5px; border-radius: 5px;">
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9 forum-heading" style="padding-left: 0px; padding-bottom: 5px;">
                  <h5><?=$add_on['addon_title']?></h5>
                  <small class="text-muted"><?= $this->lang->line('Additional'); ?> <?=$add_on['work_day']?> <?= $this->lang->line('day/s'); ?></small>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center forum-info" style="padding-left: 0px; padding-right: 0px;">
                  <h4> <?=$add_on['currency_code']?> <?=$add_on['addon_price']?></h4>
                </div>
              </div>
            <?php } ?>
            <?php if($offer_details['quick_delivery'] == 1): ?>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #FFF; margin-bottom: 5px; border-radius: 5px; padding-top: 5px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1" style="padding-right: 0px; padding-left: 0;">
                  <img style="width: 54px;" src="<?=base_url('resources/icon_delivery.png')?>" />
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-8 forum-heading" style="">
                  <h5 for="quick_delivery" style="margin-top: 10px;"><strong><?= $this->lang->line('I can deliver all work in'); ?>&nbsp;&nbsp;<font color="#59bdd7" style="font-style: italic;"><?=$offer_details['quick_delivery_day']?></font>&nbsp;&nbsp;<?= $this->lang->line('day/s'); ?></strong></h5>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center forum-info" style="padding-left: 0px; padding-right: 0px;">
                  <h4> <?=$offer_details['currency_code']?> <?=$offer_details['quick_delivery_price']?></h4>
                </div>
              </div>
            <?php endif; ?>
          </div>
        <?php endif; ?>

        <?php if($offer_details['review_count']>0): ?>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
            <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><?= $this->lang->line('reviews'); ?> (<?=$offer_details['review_count']?>)</h4>
          </div>
        <?php endif; ?>

        <?php if($offer_details['review_count']>0): ?>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
              <div class="v-timeline vertical-container animate-panel" data-child="vertical-timeline-block" data-delay="1" style="    margin-top: 0em !important; margin-bottom: 0em !important;">
                <table id="offerTableData" style="width: 100% !important">
                  <thead><tr class="hidden"><th></th></tr></thead>
                  <tbody>
                    <?php foreach ($offer_reviews as $review) { ?>
                      <tr>
                        <td>
                          <div class="vertical-timeline-block" style="margin-bottom: 25px !important; margin-right: 0px !important">
                            <div class="vertical-timeline-icon navy-bg" style="width: 50px !important; height: 50px !important; left: -5px !important;">
                              <img src="<?=base_url($review['cust_avatar_url'])?>" class="img-responsive img-circle" />
                              <small class="text-muted" style="font-size: small !important; background-color: #e8ebf0 !important; color: #505050"><?=date_format(date_create($review['review_datetime']), 'd M Y')?></small>
                            </div>
                            <div class="vertical-timeline-content" style="box-shadow: 0px 0px 1px 0px #888888; border-radius: 5px;">
                              <div style="padding: 10px 15px 15px 15px !important;">
                                <span class="vertical-date pull-right"><small>
                                  <?php for($i=0; $i<(int)$review['review_rating']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                                  <?php for($i=0; $i<(5-$review['review_rating']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?></small></span>
                                <h5><font><?=$review['cust_fname'] .' '. $review['cust_lname']?></font>&nbsp;&nbsp;&nbsp;<font><i class="fa fa-map-marker"></i> <?= $this->api->get_country_name_by_id($review['cust_country_id']); ?></font></h5>
                                <p><?=$review['review_comment']?></p>
                              </div>
                              <?php if($review['review_reply'] != 'NULL'): ?>
                                <div class="panel-footer" style="margin-left: 30px; margin-right: 10px; margin-bottom: -15px; border-radius: 5px; border: 1px solid #eaeaea; box-shadow: 1px 1px 1px 1px #888888;">
                                  <div class="row">
                                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                      <img src="<?=base_url($review['free_avatar_url'])?>" class="img-responsive img-circle" />
                                    </div>
                                    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                      <h5><font><?= ($review['free_company_name'] != 'NULL') ? $review['free_company_name'] : $review['free_fname'] .' '. $review['free_lname']?></font>
                                      <p><?=$review['review_reply']?></p>
                                    </div>
                                  </div>
                                </div>
                              <?php else: ?>
                                <div class="panel-footer" style="margin-left: 30px; margin-right: 10px; margin-bottom: -15px; border-radius: 5px; border: 1px solid #eaeaea; box-shadow: 1px 1px 1px 1px #888888;">
                                  <form action="<?=base_url('user-panel-services/provider-reply-to-review')?>" method="post">
                                    <input type="hidden" name="review_id" value="<?=$review['review_id']?>" />
                                    <input type="hidden" name="offer_code" value="<?=$offer_details['offer_code']?>" />
                                    <div class="row">
                                      <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <img src="<?=base_url($review['free_avatar_url'])?>" class="img-responsive img-circle" />
                                      </div>
                                      <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <textarea class="form-control" style="width: 100%; resize: none;" name="review_reply" id="review_reply"></textarea>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-xl-9 col-lg-9 col-md-9 col-sm-8 col-xs-12" style="margin-top: 10px;">
                                        <h5 style="display: inline-flex;"><?= $this->lang->line('Rate to customer'); ?> &nbsp;&nbsp;&nbsp;
                                        <span style="font-size: 8px;"><input id="rating" name="rating_customer" class="rating" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" required /></span></h5>
                                      </div>
                                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-top: 10px;">
                                        <button class="btn btn-info btn-sm"><?= $this->lang->line('Post reply'); ?></button>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              <?php endif; ?>

                            </div>
                          </div>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
        <?php endif; ?>

      </div>

      <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #f1f3f6; padding-bottom: 15px;">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 15px; padding-bottom: 15px; display: inline-flex; padding-left: 0px; padding-right: 0px;">
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-8">
              <h2 style="color: #3498db"><strong><?=$offer_details['currency_code']?> <?=$offer_details['offer_price']?></strong></h2>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-4 text-right" style="padding: 15px 15px 0px 0px;">
              <!-- AddToAny BEGIN -->
                <a class="a2a_dd" href="https://www.addtoany.com/share"><i class="fa fa-share-alt"></i> <?= $this->lang->line('Share'); ?></a>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
              <!-- AddToAny END -->
            </div>
          </div>

          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center hidden" style="padding-bottom: 15px;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left: 0px;">
              <input type="number" class="form-control" name="quantity" id="quantity" min="1" max="<?=($advance_payment['max_quantity_to_buy_offer'] != null || $advance_payment['max_quantity_to_buy_offer'] > 0)?$advance_payment['max_quantity_to_buy_offer']:10?>" value="1" />
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding-right: 0px;">
              <button type="button" class="btn btn-info form-control"><?= $this->lang->line('Buy Now'); ?></button>
            </div>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="padding-left: 0; padding-right: 0px; border-bottom: 1px dotted gray">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 tip" style="padding-left: 0px; padding-right: 0px;">
              <span><?= $this->lang->line('Amount of days required to complete work for this offer as set by the provider'); ?></span>
              <label><i class="fa fa-paper-plane-o fa-2x"></i></label><br />
              <h5><?= $this->lang->line('Delivery in'); ?></h5>
              <label style="color: #3498db"><?= ($offer_details['delivered_in'] == 1) ? $offer_details['delivered_in'].' '.$this->lang->line('Day') : $offer_details['delivered_in'].' '.$this->lang->line('Days') ?></label>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 tip" style="padding-left: 0px; padding-right: 0px;">
              <span><?= $this->lang->line('Rating of the offer as calculated from the buyers review'); ?></span>
              <label><i class="fa fa-thumbs-o-up fa-2x"></i></label><br />
              <h5><?= $this->lang->line('rating'); ?></h5>
              <label style="color: #3498db"><?=($offer_details['rating_count']!=0)?round(($offer_details['offer_rating']/$offer_details['rating_count']),1).'/5 <br />( '.$offer_details['review_count'].' '.$this->lang->line('reviews'):'0'.'/5 <br />( '.$offer_details['review_count'].' '.$this->lang->line('reviews') ?>)</label>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 tip" style="padding-left: 0px; padding-right: 0px;">
              <span><?= $this->lang->line('Average time for the provider to first reply on the workstream after purchase of this offer'); ?></span>
              <label><i class="fa fa-clock-o fa-2x"></i></label><br />
              <h5><?= $this->lang->line('Response Time'); ?></h5>
              <label style="color: #3498db"><?= ($operator['response_time'] <= 6) ? $this->lang->line('Within a few hours') : $operator['response_time'].' '.$this->lang->line('hours') ?></label>
            </div>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; padding-top: 10px;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <label class="text-muted"><?= $this->lang->line('views'); ?></label>
              <label style="color: #404040"><?=$offer_details['view_count']?></label>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <label class="text-muted"><?= $this->lang->line('Sales'); ?></label>
              <label style="color: #404040"><?=$offer_details['sales_count']?></label>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <!-- <span><?= $this->lang->line('Add to your favorite offers'); ?></span> -->
              <label class="text-muted"><?= $this->lang->line('Favorite'); ?></label>
              <label><?= $offer_details['favorite_counts']; ?></label>
            </div>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; padding-top: 15px;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($operator['avatar_url'])?>">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
              <h3 style="color: #3498db"><strong><?= ($operator['company_name']!='NULL')?$operator['company_name']:$operator['firstname']; ?></strong></h3>
              <h4><i class="fa fa-map-marker"></i> <?= $this->api->get_country_name_by_id($operator['country_id']); ?></h4>
              <h5 style="color: #404040"><?= strtoupper(str_replace(',', '| ', $offer_details['tags'])); ?></h5>
            </div>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; margin-top: 5px;">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 1px solid #909090; background-color: #FFF">
              <p style="padding-top: 5px;"><?=$operator['introduction']?></p>
            </div>
          </div>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 15px 0px;">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-1"><?= $this->lang->line('Buyer Tips'); ?></a></li>
            <li class=""><a data-toggle="tab" href="#tab-2"><?= $this->lang->line('How it works'); ?></a></li>
          </ul>
          <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
              <div class="panel-body">
                <ul style="padding-left: 15px;">
                  <li><?= $this->lang->line('The Offer price is fixed - you never pay a penny more'); ?></li>
                  <li><?= $this->lang->line('Your money is safe until you agree to release funds to the Provider'); ?></li>
                  <li><?= $this->lang->line('After purchase, you should contact the Provider and let them know about your requirements'); ?></li>
                </ul>
              </div>
            </div>
            <div id="tab-2" class="tab-pane">
              <div class="panel-body">
                <ul style="padding-left: 15px;">
                  <li><?= $this->lang->line('You buy an Offer and your payment is held in escrow'); ?></li>
                  <li><?= $this->lang->line('You contact the Provider and specify your requirements'); ?></li>
                  <li><?= $this->lang->line('Work is delivered'); ?></li>
                  <li><?= $this->lang->line('If you are happy you release the money to the Provider'); ?></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <div class="row">
        <div class="col-md-12">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <a href="<?= base_url('user-panel-services/provider-offers'); ?>" class="btn btn-info btn-outline"><?= $this->lang->line('back'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $('#offerTableData').dataTable( {
    "pageLength": 5,
    "bLengthChange": false,
    "bFilter": false,
    "ordering": false,
  } );
</script>

<script>
   var galleryTop = new Swiper('.gallery-top', {
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 10,
      centeredSlides: true,
      slidesPerView: 'auto',
      touchRatio: 0.2,
      slideToClickedSlide: true,
    });
    galleryTop.controller.control = galleryThumbs;
    galleryThumbs.controller.control = galleryTop;
</script>

<script>
  $('.cancelorder').click(function () {
    var id = this.id;

    swal({
      title: <?= json_encode($this->lang->line('are_you_sure'))?>,
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: <?= json_encode($this->lang->line('yes'))?>,
      cancelButtonText: <?= json_encode($this->lang->line('no'))?>,
      closeOnConfirm: false,
      closeOnCancel: false,
    },
    function (isConfirm) {
      if (isConfirm) {
        $.post('cancel-order', {id: id},
        function(res){
          if(res == "success") {
            swal(<?= json_encode($this->lang->line('deleted'))?>, "", "success");
            setTimeout(function() { window.location.reload(); }, 2000);
          } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, "", "error");  }
        });
      } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, "", "error");  }
    });
  });
</script>