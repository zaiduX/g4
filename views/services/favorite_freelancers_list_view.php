<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
   display: none;
  }
  .hoverme {
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
  }
  .btnMenu:hover {
    text-decoration: none;
    background-color: #ffb606;
    color: #FFF;
  }

  .price_tag {
    display: inline-block;
    
    width: auto;
    height: 25px;
    
    background-color: #aac3ca;
    -webkit-border-radius: 3px 4px 4px 3px;
    -moz-border-radius: 3px 4px 4px 3px;
    border-radius: 3px 4px 4px 3px;
    
    border-right: 1px solid #aac3ca;

    /* This makes room for the triangle */
    margin-right: 0px;
    
    position: relative;
    
    color: white;
    font-weight: 300;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 15px;
    line-height: 25px;

    padding: 0 10px 0 10px;
  }

  .text_limit {
    display: block;
    width: 500px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  .custom_checkbox {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 0px;
    padding-top: 7px;
    font-size: 13px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }

  /* Hide the browser's default checkbox */
  .custom_checkbox input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  /* Create a custom checkbox */
  .checkmark {
      position: absolute;
      top: 8px;
      left: 0;
      height: 18px;
      width: 18px;
      background-color: #eee;
  }

  /* On mouse-over, add a grey background color */
  .custom_checkbox:hover input ~ .checkmark {
    background-color: #ccc;
  }

  /* When the checkbox is checked, add a blue background */
  .custom_checkbox input:checked ~ .checkmark {
    background-color: #2196F3;
  }

  /* Create the checkmark/indicator (hidden when not checked) */
  .checkmark:after {
    content: "";
    position: absolute;
    display: none;
  }

  /* Show the checkmark when checked */
  .custom_checkbox input:checked ~ .checkmark:after {
    display: block;
  }

  /* Style the checkmark/indicator */
  .custom_checkbox .checkmark:after {
      left: 7px;
      top: 3px;
      width: 5px;
      height: 12px;
      border: solid white;
      border-width: 0 3px 3px 0;
      -webkit-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      transform: rotate(45deg);
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Favorite freelancers'); ?></span></li>
        </ol>
      </div>
      <h3 class="font-light m-b-xs"><strong><i class="fa fa-users text-muted"></i> <?= $this->lang->line('Favorite freelancers'); ?></strong>
        <a class="btn btn-outline btn-info" href="<?=base_url('user-panel-services/browse-provider')?>"><i class="fa fa-heart"></i> More Freelancers</a>
      </h3>
      <small class="m-t-md"><?= $this->lang->line(''); ?></small>
    </div>
  </div>
</div>
<div class="content" style="padding-top: 45px;">
  <div class="row">  
  <?php if($this->session->flashdata('error')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:red; color:#FFF; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('error'); ?></strong></div>
    </div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:green; color:white; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('success'); ?></strong></div>
    </div>
  <?php endif; ?>
  <?php
   // echo json_encode(sizeof($freelancers)); 
   if(sizeof($freelancers) > 0) { ?>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px;">
        <table id="addressTableData" class="table">
          <thead><tr class="hidden"><th><?=$profile['cust_id'];?></th><th></th></tr></thead>
          <tbody>
            <?php foreach ($freelancers as $profile) {  ?>
            <?php if($profile['project_5'] == "yes"){ ?>
              <tr>
                <td class="hidden"><?=$profile['cust_id'];?></td>
                <td style="width: inherit">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: -5px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 5px 5px 5px 5px;">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1" style="padding-left:0px;padding-right:0px;">
                          <a href="<?=$profile['profile_url_login']?>" target="_blank">
                            <img alt="logo" style="height: 62px;width: 62px;margin-top: 14px;" class="img-circle m-b-xs" src="<?=base_url($profile['avatar_url'])?>"> </a>
                             <span class="pull-right" style="display: inline-flex;">
                             <button id="btn_add_<?=$profile['cust_id']?>" style="padding: 0px;display:<?=(in_array($profile["cust_id"], $fav_freelancer)?"inline-block":"none")?>" title="<?=$this->lang->line('Remove from favorite')?>" class="btn-link remove_favorite_<?=$profile['cust_id']?>" data-id="<?=$profile['cust_id']?>"><font size="5px" style="color: red"><i id="add_favorite_<?=$profile['cust_id']?>" class="fa fa-heart"></i></font></button>
                             <button id="btn_remove_<?=$profile['cust_id']?>" style="padding: 0px;display:<?=(in_array($profile["cust_id"], $fav_freelancer)?"none":"inline-block")?> " title="<?=$this->lang->line('Add to favorite')?>" class="btn-link add_favorite_<?=$profile['cust_id']?>" data-id="<?=$profile['cust_id']?>"><font size="5px" style="color: red"><i id="add_favorite_<?=$profile['cust_id']?>" class="fa fa-heart-o"></i></font></button>
                             </span>
                            <script>
                              $('.add_favorite_<?=$profile['cust_id']?>').click(function () { 
                                var id = $(this).attr("data-id");
                                $.post('<?=base_url("user-panel-services/make-freelancer-favourite")?>', {id: id}, 
                                function(res) { 
                                  console.log(res);
                                  if($.trim(res) == "success") { 
                                    swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Freelancer added to your favorite list.')); ?>, "success");
                                    $("#btn_remove_<?=$profile['cust_id']?>").css("display", "none");
                                    $("#btn_add_<?=$profile['cust_id']?>").css("display", "inline-block");
                                  } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to add. Try again...')); ?>, "error");  } 
                                });
                              });
                              $('.remove_favorite_<?=$profile['cust_id']?>').click(function () { 
                                var id = $(this).attr("data-id");
                                $.post('<?=base_url("user-panel-services/freelancer-remove-from-favorite")?>', {id: id}, 
                                function(res) { 
                                  //console.log(res);
                                  if($.trim(res) == "success") { 
                                    swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Freelancer removed from favorite list.')); ?>, "success"); 
                                    $("#btn_remove_<?=$profile['cust_id']?>").css("display", "inline-block");
                                    $("#btn_add_<?=$profile['cust_id']?>").css("display", "none");
                                  } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to remove. Try again...')); ?>, "error");  } 
                                });
                              });
                            </script>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left: 8px;">
                          <a href="<?=$profile['profile_url_login']?>" target="_blank">
                          <strong style="font-size:20px"><?=$profile['firstname']." ".$profile['lastname']?>&nbsp;</strong> &nbsp; <?=($profile['total_job'] > 0)?$profile['job_percent']."% (".$profile['total_job'].")":""?></a><br/>
                          <span class="text_limit"><?=$profile['introduction']?></span>
                          <i class="fa fa-map-marker"></i>
                          <span style='padding-top: 4px;padding-bottom: 4px;'><?=$profile['city_name'].", ".$profile['country_name']?> &nbsp; <?=($profile['portfolio'] > 0)?" <a href='".$profile['profile_url_login']."' target='_blank' style='padding-top: 4px;padding-bottom: 4px; color: #3498db; font-size:12px' class='btn btn-link portfolio_".$profile['cust_id']."'><i class='fa fa-briefcase' aria-hidden='true'></i>&nbsp;".strtoupper($this->lang->line('view portfolio'))."</a> (".$profile['portfolio']." ".strtoupper($this->lang->line('items')).")":""?> </span><br/>
                            <?php 
                              $skill_name = explode(",",$profile['skills_name']);
                              for($i=0; $i<sizeof($skill_name); $i++){
                                //echo "<span class='price_tag'>".$skill_name[$i]."</span>&nbsp;";
                                echo "<code style='margin: 5px; padding: 0px 5px; font-size: 100%;'>".$skill_name[$i]."</code>&nbsp;";
                                if ($i == 4) {break;}
                              }
                            ?>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
                          &nbsp;
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center" style="padding-left:0px;">
                          <br/>
                          <?php if($profile['rate_per_hour'] != "NULL" && $profile['currency_code'] != "NULL"){ ?>
                          <strong style="font-size:20px;color: #3498db"><?=$profile['rate_per_hour']?></strong> &nbsp;<?=$profile['currency_code']?><br/>
                          <?=strtoupper($this->lang->line('Per Hour'))?>
                          <?php }else{ ?>
                            <strong style="font-size:15px;color: #3498db"><?=$this->lang->line('not_provided')?></strong>
                          <?php } ?>
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 text-right">
                         
                          <a href="<?=$profile['profile_url_login']?>" target="_blank" class="btn btn-outline btn-warning" style="margin-top: 10px;" id="invite_<?=$profile['cust_id']?>"><?=strtoupper($this->lang->line('view'))?></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div> 
    </div>
  <?php } else { ?>
    <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: 0px; margin-top: 20px; margin-left: -7px; margin-right: -7px;">
      <div class="panel-body" style="border-radius: 10px; padding: 15px;">
        <h5><?=$this->lang->line('No favourite freelancer.')?></h5>
      </div>
    </div>
  <?php } ?>
</div>
</div>

<script>
  $('#advanceBtn').click(function() {
    if($('#advanceSearch').css('display') == 'none') {
      $('#basicSearch').hide("slow");
      $('#advanceSearch').show("slow");
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Hide Filters'))?>);
      //$('#searchbox').hide("slow");
      //$('#searchlabel').hide("slow");
    } else {
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Show Filters'))?>);
      $('#advanceSearch').hide("slow");
      $('#basicSearch').show("slow");
      //$('#searchbox').show("slow");
      //$('#searchlabel').show("slow");
    }
    return false;
  });
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-categories-by-type", 
      data: { type_id: cat_type_id },
      dataType: "json",
      success: function(res) {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('select_category')?></option>");
        $.each( res, function() {
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function() {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('loading')?></option>");
      },
      error: function() {
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('no_options')?></option>");
      }
    })
  });

  $('.add_favorite').click(function () { 
    var id = $(this).attr("data-id");
    $.post('<?=base_url("user-panel-services/make-freelancer-favourite")?>', {id: id}, 
    function(res) { 
      console.log(res);
      if($.trim(res) == "success") { 
        swal(<?= json_encode($this->lang->line('success')); ?>, <?=json_encode($this->lang->line('Freelancer added to your favorite list.')); ?>, "success");
        // setTimeout(function() { window.location.reload(); }, 2000);
          $(this).removeClass('fa fa-heart-o').addClass('fa fa-heart');
      } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to add. Try again...')); ?>, "error");  } 
    });
  });
  $('.remove_favorite').click(function () { 
    var id = $(this).attr("data-id");
    $.post('<?=base_url("user-panel-services/freelancer-remove-from-favorite")?>', {id: id}, 
    function(res) { 
      //console.log(res);
      if($.trim(res) == "success") { 
        swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Freelancer removed from favorite list.')); ?>, "success");
        setTimeout(function() { window.location.reload(); }, 2000); 
      } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to remove. Try again...')); ?>, "error");  } 
    });
  });

  $("#country_id").on('change', function(event) { event.preventDefault();
    $('#state_id').empty();
    $('#city_id').empty().append("<option value=''><?= $this->lang->line('Select City'); ?></option>");

    var country_id = $(this).val();

    $.post('<?=base_url("user-panel-services/get-state-by-country-id")?>', {country_id: country_id}, function(data) {
      data = $.parseJSON(data);
      if(data.length > 0 ){
        $('#state_id').attr('disabled', false);
        $('#state_id').empty(); 
        $('#state_id').append("<option value=''><?= $this->lang->line('Select State'); ?></option>");
        $.each( data, function(){    
          $('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');              
        });
        $('#state_id').focus();
      } else {
        $('#state_id').attr('disabled', true);
        $('#state_id').empty();
        $('#state_id').append("<option value=''><?= $this->lang->line('No state found!'); ?></option>");
      }
    });
  });

  $("#state_id").on('change', function(event) { event.preventDefault();
    $('#city_id').empty();

    var state_id = $(this).val();

    $.post('<?=base_url("user-panel-services/get-cities-by-state-id")?>', {state_id: state_id}, function(cities) {
      cities = $.parseJSON(cities);
      if(cities.length > 0 ){
        $('#city_id').attr('disabled', false);
          $('#city_id').empty(); 
          $('#city_id').append("<option value=''><?= $this->lang->line('Select City'); ?></option>");
          $.each(cities, function(){    
              $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>');              
          });
          $('#city_id').focus();
      }
      else{
          $('#city_id').attr('disabled', true);
          $('#city_id').empty();
          $('#city_id').append("<option value=''><?= $this->lang->line('No city found!'); ?></option>");
        }
    });
  });

  $('#project_50').click(function () { 
    if($(this).is(':checked')){if($('#project_5').is(':checked')){
    $('#project_5').prop('checked', false);}}
  });
  $('#project_5').click(function () { 
    if($(this).is(':checked')){if($('#project_50').is(':checked')){
    $('#project_50').prop('checked', false);}}
  });

  $('.filter_submit').click(function (event) { 
    event.preventDefault();
    var rate_from = $('#rate_from').val();
    var rate_to = $('#rate_to').val();
    if(rate_to != "" && rate_from != ""){
      if(parseInt(rate_from) < parseInt(rate_to)){
        $('#freelancerslistFilter').submit();
      }else{swal("<?=$this->lang->line('error')?>", "<?=$this->lang->line('Start price is greater than end price')?>", "error");}
    }else{
      $('#freelancerslistFilter').submit();      
    }
  });
//   $('.table').dataTable({
//   "pageLength": 2
// });
</script>

