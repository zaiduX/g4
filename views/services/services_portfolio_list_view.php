<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/user-profile'); ?>"><?= $this->lang->line('profile'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('portfolio'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-photo fa-2x text-muted"></i> <?= $this->lang->line('portfolio'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('list_of_added_portfolios'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="hpanel hblue">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <h4>
              <strong> <?= $this->lang->line('portfolio'); ?> </strong> &nbsp;
              <a href="<?= base_url('user-panel-services/portfolio/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
            </h4>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
            <a href="<?= base_url('user-panel-services/user-profile'); ?>" class="btn btn-primary"><i class="fa fa-left-arrow"></i> <?= $this->lang->line('go_to_profile'); ?></a> 
          </div>
        </div>
      </div>
      
      <?php foreach($portfolios as $portfolio ): ?>
        <div class="panel-body" style="padding-bottom: 0px; padding-top: 10px; margin-bottom: 5px;">
          <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">
            <p style="word-wrap: break-word;">
              <?php if($portfolio['attachement_url'] != "NULL"): ?>
                <img src="<?= base_url($portfolio['attachement_url']); ?>" class="img-responsive" style="height: 100px; width: auto;">
              <?php else: ?>
                 <?= $this->lang->line('portfolio'); ?>;
              <?php endif; ?>
              </p>
          </div>
          <div class="col-xl-9 col-lg-9 col-md-9 col-sm-6 col-xs-6">
            <div class="row">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6"> <p><strong><?= $this->lang->line('title'); ?></strong></p> </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4 col-xs-4"> <p style="word-wrap: break-word;"><?= $portfolio['title']; ?></p> </div>  
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 text-right">
                <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-services/portfolios/').$portfolio['portfolio_id'];?>">
                  <i class="fa fa-pencil"></i>
                </a>
                <a class="btn btn-danger btn-circle btn-delete" id="<?=$portfolio['portfolio_id'];?>">
                  <i class="fa fa-trash"></i>
                </a>
              </div>
            </div>
            <div class="row">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12"> <p><strong><?= $this->lang->line('category'); ?></strong></p> </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12"> <p style="word-wrap: break-word;"><?= $this->user->get_category_name_by_id($portfolio['cat_id']); ?></p> </div>  
            </div>
            <div class="row">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12"> <p><strong><?= $this->lang->line('sub_categories'); ?></strong></p> </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12"> <p style="word-wrap: break-word;">
                <?php if($portfolio['subcat_ids'] != "NULL") { 
                  $subcat_ids = explode(',', $portfolio['subcat_ids']); 
                    foreach($subcat_ids as $subid){ $subcats [] = $this->user->get_category_name_by_id($subid); }
                    echo implode(', ', $subcats);
                  } else { echo  $this->lang->line('not_selected'); }
                ?>
                </p> 
              </div>  
            </div>
            <div class="row">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12"> <p><strong><?= $this->lang->line('desc'); ?></strong></p> </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12"> <p style="word-wrap: break-word;">
                <?= ($portfolio['description'] != "NULL") ? nl2br($portfolio['description']) : $this->lang->line('not_defined'); ?>
                </p></div>  
            </div>
          </div>
        </div>
      <?php endforeach; ?> 
    </div>
  </div>
</div>

<script>
  $('.btn-delete').click(function () {            
    var id = this.id;            
    swal({                
      title: <?= json_encode($this->lang->line('are_you_sure'))?>,                
      text: <?= json_encode($this->lang->line('you_will_not_be_able_to_recover_this_record'))?>,                
      type: "warning",                
      showCancelButton: true,                
      confirmButtonColor: "#DD6B55",                
      confirmButtonText:<?= json_encode($this->lang->line('yes'))?>,                
      cancelButtonText: <?= json_encode($this->lang->line('no'))?>,                
      closeOnConfirm: false,                
      closeOnCancel: false, 
    },                
    function (isConfirm) {                    
      if (isConfirm) {                        
        $.post("<?=base_url('user-panel-services/delete-portfolio')?>", {id: id}, 
        function(res){
          if($.trim(res) == "success") {
            swal("<?=$this->lang->line('deleted')?>", "<?=$this->lang->line('Deleted successfully.')?>", "success");
            setTimeout(function() { window.location.reload(); }, 2000);
          } else {  swal("<?=$this->lang->line('error')?>", "<?=$this->lang->line('While deleting record!')?>", "error"); }
        }); 
      } else {  swal("<?=$this->lang->line('canceled')?>", "<?=$this->lang->line('Record is safe!')?>", "error");  }  
    });            
  });
</script>