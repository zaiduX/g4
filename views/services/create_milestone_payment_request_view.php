</style>
    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-services/provider-inprogress-jobs'; ?>"><?= $this->lang->line('in_progress'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('Milestone payment request'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Milestone payment request'); ?>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('Send request to customer to pay milestone price'); ?>.</small>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <form method="post" class="form-horizontal" action="<?=base_url('user-panel-services/job-milestone-payment-request')?>" id="MilestoneRequest">
                            <input type="hidden" name="job_id" value="<?= $job_details['job_id'] ?>">
                            <input type="hidden" name="milestone_id" value="<?= $proposal_milestone['milestone_id'] ?>">
                            <input type="hidden" name="milestone_price" value="<?= $proposal_milestone['milestone_price'] ?>">
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <?php if($this->session->flashdata('error')):  ?>
                                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                                    <?php endif; ?>
                                    <?php if($this->session->flashdata('success')):  ?>
                                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-group"><label class="col-md-3 text-right"><?= $this->lang->line('Job details'); ?>: </label>
                                        <div class="col-md-8"> 
                                            <label style="color:#59bdd7"><?= $job_details['job_title'] ?></label>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group"><label class="col-md-3 text-right"><?= $this->lang->line('Job budget'); ?>: </label>
                                        <div class="col-md-8"> 
                                            <label style="color:#59bdd7"><?= $job_details['budget']." ".$job_details['currency_code'] ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-3 text-right"><?= $this->lang->line('Paid Amount'); ?>: </label>
                                        <div class="col-md-8"> 
                                            <label style="color:#59bdd7"><?= $job_details['paid_amount']." ".$job_details['currency_code'] ?></label>
                                        </div>
                                    </div> -->
                                    <div class="form-group"><label class="col-md-3 text-right"><?= $this->lang->line('Milestone details'); ?>: </label>
                                        <div class="col-md-8"> 
                                            <label style="color:#59bdd7">
                                                <?=(!$job_details['job_type'])?$proposal_milestone['milestone_details']: $this->lang->line('Pay remaining balance amount'); ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-3 text-right"><?= $this->lang->line('Milestone price'); ?>: </label>
                                        <div class="col-md-8"> 
                                            <label style="color:#59bdd7"><?=$proposal_milestone['milestone_price']." ".$proposal_milestone['currency_code']?></label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-3 text-right"><?= $this->lang->line('desc'); ?> </label>
                                        <div class="col-md-8"> 
                                            <textarea style="resize:none"  rows="4" id="description" name="description" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Describe Milestone details.'); ?>" autocomplete="none"></textarea> <br/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-md-5 col-md-offset-6 text-right">
                                            <button class="btn btn-info" type="submit"><?= $this->lang->line('send_request'); ?></button>
                                            <a href="<?= $this->config->item('base_url') . 'user-panel-services/provider-inprogress-jobs'; ?>" class="btn btn-primary" type="submit"><?= $this->lang->line('cancel');?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>