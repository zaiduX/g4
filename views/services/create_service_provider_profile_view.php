<style>
  #attachment, .attachment { 
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px; 
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0); 
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('create'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-building fa-2x text-muted"></i> <?= $this->lang->line('Create Service Provider Profile'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('fill_details'); ?></small>     
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="hpanel hblue">
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>

        <form action="<?= base_url('user-panel-services/create-service-provider-profile'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="updateProfile">

          <input type="hidden" name="cust_id" value="<?=$cust_id?>">
          <div class="row form-group">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <label class=""><?= $this->lang->line('f_name'); ?></label>
              <input type="text" class="form-control" id="firstname" placeholder="<?= $this->lang->line('f_name'); ?>" name="firstname" >
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <label class=""><?= $this->lang->line('l_name'); ?></label>
              <input type="text" class="form-control" id="lastname" placeholder="<?= $this->lang->line('l_name'); ?>" name="lastname" >
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <label class="control-label"><?= $this->lang->line('gender'); ?></label><br />
              <div class="radio radio-success radio-inline">
                <input type="radio" id="genderMale" value="m" name="gender" aria-label="male" />
                <label for="male"> <?= $this->lang->line('male'); ?> </label>
              </div>
              <div class="radio radio-success radio-inline">
                <input type="radio" id="genderFemale" value="f" name="gender" aria-label="female" />
                <label for="female"> <?= $this->lang->line('female'); ?> </label>
              </div>
            </div>
          </div>
          <div class="row form-group">
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-xs-6">
              <label class=""><?= $this->lang->line('pro_img'); ?></label>
              <div class="input-group">
                <span class="input-group-btn">
                  <button id="upload_avatar" class="btn btn-green"><i class="fa fa-user"></i>&nbsp; <?= $this->lang->line('btn_avatar'); ?></button>
                </span>
              </div>
              <span id="avatar_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('avatar'); ?></span>
              <input type="file" id="avatar" name="avatar" class="upload attachment" accept="image/*" onchange="avatar_name(event)" />
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-xs-6">
              <label class=""><?= $this->lang->line('c_img'); ?></label>
              <div class="input-group" style="margin-bottom: 10px;">
                <span class="input-group-btn">
                  <button id="upload_cover" class="btn btn-green"><i class="fa fa-photo"></i>&nbsp; <?= $this->lang->line('select_cover'); ?></button>
                </span>                      
              </div>
              <span id="cover_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('cover'); ?></span>
              <input type="file" id="cover" name="cover" class="upload attachment" accept="image/*" onchange="cover_name(event)" />
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-6">
              <label class=""><?= $this->lang->line('c_name'); ?></label>
              <input type="text" class="form-control" id="company_name" placeholder="<?= $this->lang->line('c_name'); ?>" name="company_name" >
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-6">
              <label class=""><?= $this->lang->line('m_number'); ?></label>
              <input type="text" class="form-control" id="contact_no" placeholder="<?= $this->lang->line('m_number'); ?>" name="contact_no" >
            </div>
          </div>
          <div class="row form-group">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <label class=""><?= $this->lang->line('email'); ?></label>
              <input type="text" class="form-control" id="email_id" placeholder="<?= $this->lang->line('email'); ?>" name="email_id">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12">
              <label class=""><?= $this->lang->line('intro'); ?></label>
              <textarea placeholder="<?= $this->lang->line('intro'); ?>" class="form-control" id="introduction" name="introduction" rows=1></textarea>
            </div>
          </div>
          <div class="row form-group">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <label class="control-label"><?= $this->lang->line('country'); ?></label>
              <select id="country_id" name="country_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('sel_count'); ?>">
                <option value=""><?= $this->lang->line('sel_count'); ?></option>
                <?php foreach ($countries as $country): ?>
                  <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <label class="control-label"><?= $this->lang->line('state'); ?></label>
              <select id="state_id" name="state_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('sel_state'); ?>" disabled>
                <option value=""><?= $this->lang->line('sel_state'); ?></option>
              </select>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <label class="control-label"><?= $this->lang->line('city'); ?></label>
              <select id="city_id" name="city_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('sel_city'); ?>" disabled>
                <option value=""><?= $this->lang->line('sel_city'); ?></option>
              </select>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <label class=""><?= $this->lang->line('zip'); ?></label>
              <input type="text" class="form-control" id="zipcode" placeholder="<?= $this->lang->line('zip'); ?>" name="zipcode">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <label><?= $this->lang->line('select_address_from_map'); ?></label>
              <input type="text" class="form-control" id="us3-address" name="address" placeholder="<?= $this->lang->line('select_address_from_map'); ?>" required="required" />
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <label><?= $this->lang->line('street_name'); ?></label>
              <input type="text" class="form-control" id="street_name" name="street_name" placeholder="<?= $this->lang->line('street_name'); ?>" required="required" />
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <div id="us3" style="width: 100%; height: 250px;"></div>
            </div>
          </div>
          <div class="row form-group hidden">
            <label class="col-sm-2 control-label"><?= $this->lang->line('radius'); ?></label>
            <input type="text" class="form-control" id="us3-radius" />
          </div>
          <div class="row col-md-6 hidden">
            <div class="form-group">
              <label class="control-label"><?= $this->lang->line('latitude'); ?> </label>
              <input type="text" class="form-control" id="us3-lat" name="latitude" />
            </div>
          </div>
          <div class="row col-md-6 hidden">
            <div class="form-group">
              <label class="control-label"><?= $this->lang->line('longitude'); ?> </label>
              <input type="text" class="form-control" id="us3-lon" name="longitude" />
            </div>
          </div>
        </div>
        <div class="panel-footer"> 
          <div class="row form-group">
             <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
              <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('c_del_profile'); ?></button>
              <a href="<?= base_url('user-panel-services/dashboard-laundry'); ?>" class="btn btn-primary"><?= $this->lang->line('back'); ?></a>                            
             </div>
           </div>         
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $(this).val();
    $('#city_id').attr('disabled', true);

    $.ajax({
      url: "<?= base_url('user-panel-services/get-state-by-country-id')?>",
      type: "POST",
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        //console.log(res);
        $('#state_id').attr('disabled', false);
        $('#state_id').empty();
        $('#state_id').append('<option value="0"><?= json_encode($this->lang->line('select_state'));?></option>');
        $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
        $('#state_id').focus();
      },
      beforeSend: function(){
        $('#state_id').empty();
        $('#state_id').append('<option value="0"><?= json_encode($this->lang->line('loading'));?></option>');
      },
      error: function(){
        $('#state_id').attr('disabled', true);
        $('#state_id').empty();
        $('#state_id').append('<option value="0"><?= json_encode($this->lang->line('no_option'));?></option>');
      }
    })
  });

  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    $.ajax({
      type: "POST",
      url: "<?= base_url('user-panel-services/get-cities-by-state-id')?>",
      data: { state_id: state_id },
      dataType: "json",
      success: function(res){
        $('#city_id').attr('disabled', false);
        $('#city_id').empty();
        $('#city_id').append('<option value="0"><?= json_encode($this->lang->line('select_city'));?></option>');
        $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
        $('#city_id').focus();
      },
      beforeSend: function(){
        $('#city_id').empty();
        $('#city_id').append('<option value="0"><?= json_encode($this->lang->line('loading'));?></option>');
      },
      error: function(){
        $('#city_id').attr('disabled', true);
        $('#city_id').empty();
        $('#city_id').append('<option value="0"><?= json_encode($this->lang->line('no_option'));?></option>');
      }
    })
  });
</script>
<script>
  $(function(){
    $("#updateProfile").validate({
      ignore: [],
      rules: {
        avatar: {  accept:"jpg,png,jpeg,gif" },
        cover: {  accept:"jpg,png,jpeg,gif" },
        firstname: { required: true, },
        lastname: { required: true, },
        company_name: { required: true, },
        contact_no: { required: true, },
        country_id: { required: true, },
        state_id: { required: true, },
        city_id: { required: true, },
        zipcode: { required: true, },
        address: { required: true, },
        email_id: { required: true, },
        introduction: { required: true, },
        street_name: { required: true, },
      }, 
      messages: {
        avatar: {  accept: <?= json_encode($this->lang->line('only_image_allowed'));?> },
        cover: {  accept: <?= json_encode($this->lang->line('only_image_allowed'));?>   },
        firstname: { required: <?= json_encode($this->lang->line('enter_first_name'));?>,   },
        lastname: { required: <?= json_encode($this->lang->line('enter_last_name'));?>,  },
        company_name: { required: <?= json_encode($this->lang->line('enter_company_name'));?>,  },
        contact_no: { required: <?= json_encode($this->lang->line('enter_mobile_number'));?>, },
        country_id: { required: <?= json_encode($this->lang->line('select_country'));?>,  },
        state_id: { required: <?= json_encode($this->lang->line('select_state'));?>,  },
        city_id: { required: <?= json_encode($this->lang->line('select_city'));?>,  },
        zipcode: { required: <?= json_encode($this->lang->line('enter_zipcode'));?>,  },
        address: { required: <?= json_encode($this->lang->line('enter_address'));?>,  },
        email_id: { required: <?= json_encode($this->lang->line('enter_email_address'));?>,  },
        introduction: { required: <?= json_encode($this->lang->line('enter_some_introduction'));?>, },
        street_name: { required: <?= json_encode($this->lang->line('street_name'));?>, },
      }
    });

    $("#upload_avatar").on('click', function(e) { e.preventDefault(); $("#avatar").trigger('click'); });
    $("#upload_cover").on('click', function(e) { e.preventDefault(); $("#cover").trigger('click'); });
  });

  function avatar_name(e){ if(e.target.files[0].name !="") { $("#avatar_name").removeClass('hidden'); }}
  function cover_name(e){ if(e.target.files[0].name !="") { $("#cover_name").removeClass('hidden'); }}
</script>
<script>
  $('#us3').locationpicker({
    location: {
      latitude: 0.00,
      longitude: 0.00,
    },
    radius: 300,
    inputBinding: {
      latitudeInput: $('#us3-lat'),
      longitudeInput: $('#us3-lon'),
      radiusInput: $('#us3-radius'),
      locationNameInput: $('#us3-address')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
      //Uncomment line below to show alert on each Location Changed event
      //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
  });
</script>