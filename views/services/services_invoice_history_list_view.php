    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>
          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-services/dashboard-services'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('invoices'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs"> <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('invoices'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel-services/services-wallet'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-money"></i> <?= $this->lang->line('my_wallet'); ?> </a> </h2>
          <small class="m-t-md"><?= $this->lang->line('view_and_download_order_invoices'); ?></small> 
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= $this->lang->line('Milestone id'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('Job ID'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('From date'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('To date'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('Work hours'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('amount'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('invoices'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($details as $dtls) { ?>
                                <tr>
                                    <td class="text-center"><?=$dtls['milestone_id']?></td>
                                    <td class="text-center"><?=$dtls['job_id']?></td>
                                    <td class="text-center"><?=($dtls['from_date']=="NULL")?"-":$dtls['from_date']?></td>
                                    <td class="text-center"><?=($dtls['to_date']=="NULL")?"-":$dtls['to_date']?></td>
                                    <td class="text-center"><?=($dtls['work_hours']=="0")?"-":$dtls['work_hours']?></td>
                                    <td class="text-center"><?=$dtls['milestone_price']." ".$dtls['currency_code']?></td>
                                    <td class="text-center"><a href="<?= base_url($dtls['invoice_url']) ?>" target="_blank"><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>