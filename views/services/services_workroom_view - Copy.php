<?php
  $provider_id = $keys["provider_id"];
  $cust_id = $keys["cust_id"];
  $job_id = $keys["job_id"];
?> 
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 15px;">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/my-jobs'); ?>"><?= $this->lang->line('My Jobs'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('workroom'); ?></span></li>
        </ol>
      </div>
      <h4 class="font-light m-b-xs"><i class="fa fa-comments"></i> <?= $this->lang->line('Workroom with').' '; ?><font color="#3498db"><strong><?=($job_details['provider_id'] == $cust_id)?$job_details['cust_name']:$job_details['provider_company_name']?></strong></font></h4> 
    </div>
  </div>
</div>

<div class="content" style="padding-top: 50px;">
  <div class="hpanel hblue">
    <div class="panel-body" style="padding: 0px;">
      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h4 style="margin-top: 15px; margin-bottom: 15px;" class="font-uppercase"><i class="fa fa-tag"></i> Ref #GS-<?=$job_details['job_id']?>&nbsp;&nbsp;&nbsp;<?=$job_details['job_title']?></h4>
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <div class="hpanel horange" style="margin-bottom: 15px;">
            <div class="panel-body no-padding">
              <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <ul class="chat-discussion" style="height: 300px;" id="chatDiv">
                    <input type="hidden" name="chat_count" value="<?=sizeof($workroom)?>" id="chat_count" />
                    <?php $total = COUNT($workroom); foreach($workroom as $v => $w): ?>
                      <?php date_default_timezone_set($this->session->userdata("default_timezone")); $ud = strtotime($w['cre_datetime']); ?>
                      <?php date_default_timezone_set($this->session->userdata("user_timezone")); ?>
                      <li class="chat-message <?= ($w['sender_id'] == $cust_id)?'right':'left'; ?>" <?=($total==($v+1))?"tabindex='1'":'';?>>
                        <?php if($w['sender_id'] == $cust_id): ?>
                          <img class="message-avatar" style="margin-right: 0px;" src="<?=base_url($login_customer_details['avatar_url'])?>" alt="" >
                          <div class="message" style="margin-left: 0px;">
                            <a class="message-author"><?=$login_customer_details['firstname'].' '.$login_customer_details['lastname']?></a>
                            <span class="message-date">  <?= date('l, M / d / Y  h:i a', $ud); ?></span>
                            <div class="row">
                              <?php if($w['type'] == "chat" || $w['type'] == "attachment"): ?>
                                <span class="message-content">
                                  <?php if($w['attachment_url'] != 'NULL'): ?>
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">
                                      <p>&nbsp;</p>
                                      <p><a href="<?= base_url($w['attachment_url']);?>" target="_blank"> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a></p>
                                    </div>
                                  <?php endif; ?>
                                  <div class="col-xl-<?=($w['attachment_url'] == 'NULL')?12:9?> col-lg-<?=($w['attachment_url'] == 'NULL')?12:9?> col-md-<?=($w['attachment_url'] == 'NULL')?12:9?> col-sm-<?=($w['attachment_url'] == 'NULL')?12:12?> col-xs-<?=($w['attachment_url'] == 'NULL')?12:12?>">
                                    <?php if(trim($w["text_msg"]) != "NULL" && trim($w["text_msg"]) != "" ): ?>
                                      <h5 style="text-align: left;"><?= $this->lang->line('message'); ?></h5>
                                    <?php endif; ?>
                                    <p style="text-align: left;"><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </div>
                                </span>
                              <?php endif; ?>
                              <?php if($w['type'] == "job_started"): ?>
                                <span class="message-content">
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h5 style="text-align: left;"><?= $this->lang->line('Job started'); ?></h5>
                                    <p style="text-align: left;"><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </div>
                                </span>
                              <?php endif; ?>
                              <?php if($w['type'] == "payment"): ?>
                                <span class="message-content">
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <h5 style="text-align: left;"><?= $this->lang->line('Payment completed'); ?></h5>
                                    <p style="text-align: left;"><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </div>
                                </span>
                              <?php endif; ?>




                              <?php if($w['type'] == "raise_invoice"): ?>
                                <div class="col-md-3 text-left">
                                  <p style="margin-top:10px;"><a href="<?= base_url().'resources/'.$w['attachment_url'];?>" target="_blank">
                                    <i class="fa fa-file-pdf-o fa-2x"></i> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a>
                                  </p>
                                </div>
                                <div class="col-md-9">
                                  <span class="message-content">
                                    <h5><?= $this->lang->line('invoice'); ?></h5>
                                    <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </span>
                                </div>
                              <?php endif; ?>
                              <?php if($w['type'] == "review_rating"): ?>
                                <div class="col-md-3 text-left">
                                  <?php for($i=0; $i < (int) $w['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                                  <?php for($i=0; $i < ( 5-(int) $w['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                                  ( <?= $w['ratings'] ?> / 5 )
                                </div>
                                <div class="col-md-9">
                                  <span class="message-content">
                                    <h5>Ratings &amp; Review</h5>
                                    <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </span>
                                </div>
                              <?php endif; ?>
                              <?php if($w['type'] == "status"): ?>
                                <div class="col-md-12">
                                  <span class="message-content">
                                    <h5><?= $this->lang->line('order_status'); ?></h5>
                                    <p style="text-align: justify;"><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </span>
                                </div>
                              <?php endif; ?>
                            </div>
                          </div>
                        <?php else: ?>
                          <img class="message-avatar" src="<?=base_url(($w['sender_id']==$job_details['cust_id'])?$job_customer_details['avatar_url']:$job_provider_details['avatar_url'])?>" alt="" >
                          <div class="message">
                            <a class="message-author text-left"><?=($w['sender_id']==$job_details['cust_id'])?$job_customer_details['firstname'].' '.$job_customer_details['lastname']:$job_provider_details['firstname'].' '.$job_provider_details['lastname']?></a>
                            <span class="message-date"><?php $ud = strtotime($w['cre_datetime']); echo date('l, M / d / Y  h:i a', $ud); ?></span>
                            <div class="row">
                              <?php if($w['type'] == "chat" || $w['type'] == "attachment"): ?>
                                <span class="message-content">
                                  <div class="col-xl-<?=($w['attachment_url'] == 'NULL')?12:9?> col-lg-<?=($w['attachment_url'] == 'NULL')?12:9?> col-md-<?=($w['attachment_url'] == 'NULL')?12:9?> col-sm-<?=($w['attachment_url'] == 'NULL')?12:12?> col-xs-<?=($w['attachment_url'] == 'NULL')?12:12?>">
                                    <?php if(trim($w["text_msg"]) != "NULL" && trim($w["text_msg"]) != "" ): ?>
                                      <h5 style="text-align: left;"><?= $this->lang->line('message'); ?></h5>
                                    <?php endif; ?>
                                    <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </div>
                                  <?php if($w['attachment_url'] != 'NULL'): ?>
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">
                                      <p>&nbsp;</p>
                                      <p><a href="<?= base_url($w['attachment_url']);?>" target="_blank"> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a></p>
                                    </div>
                                  <?php endif; ?>
                                </span>
                              <?php endif; ?>
                              <?php if($w['type'] == "job_started"): ?>
                                <span class="message-content">
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h5 style="text-align: left;"><?= $this->lang->line('Job started'); ?></h5>
                                    <p style="text-align: left;"><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </div>
                                </span>
                              <?php endif; ?>
                              <?php if($w['type'] == "payment"): ?>
                                <span class="message-content">
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <h5 style="text-align: left;"><?= $this->lang->line('Payment completed'); ?></h5>
                                    <p style="text-align: left;"><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </div>
                                </span>
                              <?php endif; ?>




                              <?php if($w['type'] == "raise_invoice"): ?>
                                <div class="col-md-9">
                                  <span class="message-content">
                                    <h5><?= $this->lang->line('order_invoice'); ?></h5>
                                    <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </span>
                                </div>
                                <div class="col-md-3 text-left">
                                  <p style="margin-top:10px;"><a href="<?= base_url().'resources/'.$w['attachment_url'];?>" target="_blank"><i class="fa fa-file-pdf-o fa-2x"></i> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a></p>
                                </div>
                              <?php endif; ?>
                              <?php if($w['type'] == "review_rating"): ?>
                                <div class="col-md-9">
                                  <span class="message-content">
                                    <h5><?= $this->lang->line('ratings_review'); ?></h5>
                                    <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </span>
                                </div>
                                <div class="col-md-3 text-left">
                                  <?php for($i=0; $i < (int) $w['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                                  <?php for($i=0; $i < ( 5-(int) $w['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                                  ( <?= $w['ratings'] ?> / 5 )
                                </div>
                              <?php endif; ?>
                              <?php if($w['type'] == "order_status"): ?>
                                <div class="col-md-9">
                                  <span class="message-content">
                                    <h5><?= $this->lang->line('order_status'); ?></h5>
                                    <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                                  </span>
                                </div>
                                <div class="col-md-3 text-left"></div>
                              <?php endif; ?>
                            </div>
                          </div>
                        <?php endif; ?>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            </div>
            <div class="panel-footer borders" style="padding:15px 15px 0px 15px">
              <div class="row form-group">
                <!-- <?=$_SERVER['HTTP_REFERER']?> -->
                <?php $ref_page = explode('/', $_SERVER['HTTP_REFERER'])[sizeof(explode('/', $_SERVER['HTTP_REFERER']))-1] ?>

                <?php if($job_details['provider_id'] != $cust_id): ?>
                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <select id="send_option" class="select2 form-group">
                      <option value="chat"><?= $this->lang->line('chat'); ?></option>
                      <?php if($ref_page == 'my-completed-jobs' || $ref_page == 'my-cancelled-jobs'):?><option value="rating"><?= $this->lang->line('rating'); ?></option><?php endif ?>
                    </select>
                  </div>
                <?php else: ?>
                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <select id="send_option" class="select2 form-group">
                      <option value="chat"><?= $this->lang->line('chat'); ?></option>
                      <?php if($ref_page == 'provider-completed-jobs' || $ref_page == 'provider-cancelled-jobs'):?><option value="rating"><?= $this->lang->line('rating'); ?></option><?php endif ?>
                    </select>
                  </div>
                <?php endif; ?>
                <form action="<?=base_url('user-panel-services/add-services-workroom-chat');?>" id="chatForm" method="post" enctype="multipart/form-data">
                  <div id="chat_div" class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-8">
                      <input type="hidden" name="job_id" value="<?=$job_details['job_id']; ?>" />
                      <input type="text" name="message_text" class="form-control message_text" id="message_text" placeholder="<?= $this->lang->line('type_your_message'); ?>" />
                      <input type="file" id="file_attachment" name="attachment" class="upload form-control" accept="image/*" onchange="attachement_name(event)" />
                  </div>
                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 chat_div">
                    <button type="submit" class="btn btn-success btn-block" style="line-height:1.9em"><i class="fa fa-send"></i><br /><?=$this->lang->line('send');?></button>
                  </div>
                </form>
                <div id="rating_div" class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10 hidden">
                  <form action="<?=base_url('user-panel-laundry/add-laundry-workroom-rating');?>" id="ratingForm" method="post">
                    <input type="hidden" name="job_id" value="<?= $job_details['job_id']; ?>" />
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left: 0;">
                      <input id="rating" name="rating" class="rating" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" required />
                      <span id="error_rating" class="text-danger"></span>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9" style="padding-right: 0;">
                      <div class="input-group">
                        <input type="text" id="review_text" name="review_text" class="form-control" placeholder="Type your review here...">
                        <span class="input-group-btn">
                          <button type="submit" class="btn btn-success send_message"><?= $this->lang->line('send'); ?></button>
                        </span>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 15px;">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; padding-right: 0px; padding-top: 15px;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=($job_details['provider_id'] == $cust_id)?base_url($job_customer_details['avatar_url']):base_url($job_provider_profile['avatar_url'])?>">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
              <h4 style="color: #3498db"><strong><?=($job_details['provider_id'] == $cust_id)?$job_customer_details['firstname'].' '.$job_customer_details['lastname']:$job_provider_profile['firstname'].' '.$job_provider_profile['lastname']; ?></strong></h4>
              <h5><i class="fa fa-map-marker"></i> <?= ($job_details['provider_id'] == $cust_id)?$this->api->get_country_name_by_id($job_customer_details['country_id']):$this->api->get_country_name_by_id($job_provider_profile['country_id']); ?></h5>
              <h5>
                <?php
                  if($job_details['provider_id'] == $cust_id) {
                    $to_time = strtotime($job_customer_details['last_login_datetime']);
                    date_default_timezone_set($_SESSION['default_timezone']);
                    $from_time = strtotime(date('Y-m-d h:i:s'));
                    //echo date('Y-m-d H:i:s');
                    //echo $job_customer_details['last_login_datetime'];
                  } else {
                    $to_time = strtotime($job_provider_details['last_login_datetime']);
                    date_default_timezone_set($_SESSION['default_timezone']);
                    $from_time = strtotime(date('Y-m-d h:i:s'));
                    //echo $job_provider_details['last_login_datetime'];
                    //echo date('Y-m-d h:i:s');
                  }
                  $min = round(((abs($to_time - $from_time) / 60) - 270),2);
                  if( $min > 10 ) { echo '<span class="text-danger"><i class="fa fa-circle"></i> '.$this->lang->line('Offline').'</span>'; }
                  else { echo '<span class="text-success"><i class="fa fa-circle"></i> '.$this->lang->line('Online').'</span>'; }
                  //echo '<br />'.$min;
                ?>
              </h5>
            </div>
          </div>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 15px 0px;">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-1"><?= $this->lang->line('Workroom feeds'); ?></a></li>
            <li class=""><a data-toggle="tab" href="#tab-2"><?= $this->lang->line('Attachments'); ?></a></li>
          </ul>
          <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
              <div class="panel-body" style="overflow: auto; height: 330px;">
                <ul style="padding-left: 15px;">
                  <?php foreach($workroom as $v => $w): if($w['type']=='job_started' || $w['type']=='payment'):
                  if($w['text_msg'] != '' && $w['text_msg'] != 'NULL') { echo '<li><h5>'. str_replace('_', ' ', strtoupper($w["type"])).'</h5><p>'.$w["text_msg"].'<br /><small style="color:gray">'.$this->lang->line('Posted on').': '.date('l, M / d / Y  h:i a', strtotime($w["cre_datetime"])).'</small></p></li>'; }
                  endif; endforeach; ?>
                </ul>
              </div>
            </div>
            <div id="tab-2" class="tab-pane">
              <div class="panel-body" style="overflow: auto; height: 330px;">
                <ul style="padding-left: 15px;">
                  <?php array_reverse($workroom); foreach($workroom as $v => $w): if($w['type']=='attachment') : ?>
                    <li><p><a href="<?= base_url($w['attachment_url']);?>" target="_blank"> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a> <?=($w['text_msg'] != '' && $w['text_msg'] != 'NULL')?$w['text_msg']:'';?><br /><small style="color:gray"><?=$this->lang->line('Posted on').': '.date('l, M / d / Y  h:i a', strtotime($w["cre_datetime"]))?></small></p></li>
                  <?php endif; endforeach; ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <div class="row">
        <div class="col-md-12">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <a href="<?= $_SERVER['HTTP_REFERER'] ?>" class="btn btn-info btn-outline"><?= $this->lang->line('back'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(function(){
    $("#send_option").on('change', function(event) {  event.preventDefault();
      var option = $(this).val();
      if(option =="rating") { $("#rating_div").removeClass('hidden'); $("#chat_div").addClass('hidden'); $(".chat_div").addClass('hidden'); }
      else { $("#chat_div").removeClass('hidden'); $(".chat_div").removeClass('hidden'); $("#rating_div").addClass('hidden'); }
    });
    $("#ratingForm").submit(function(e) { e.preventDefault();
      var form = $(this).get(0);
      var rating = $("#rating").val();
      if(rating == 0 ) {
        $("#error_rating").removeClass('hidden').html('Give some rating.');
      } else { form.submit(); }
    });
  });
</script>
<script>
  //Scroll chat to down
   $(window).load(function() {
    var elem = document.getElementById('chatDiv');
    elem.scrollTop = elem.scrollHeight;
  });
  //press enter for submit chat
  $('.input').keypress(function (e) {
    if (e.which == 13) {
      $('#chatForm').submit();
      return false;
    }
  });
</script>

<script>
  var display_time = 3000;

  setInterval(function() {
    get_messages();
  }, display_time);

  var get_messages = function() {
    var chat_count = $("#chat_count").val();
    $.ajax({
      url: '<?=base_url("user-panel-services/customer-job-offer-workroom-ajax");?>',
      type: 'POST',
      dataType: 'json',
      data: { job_id: "<?=$job_details['job_id']?>", chat_count: chat_count },
      success: function(res){
        console.log(res);
        if(res != false) {
          $("#chatDiv").empty();
          $('#chatDiv').html(res);
          return $('#chatDiv').animate({ scrollTop: $('#chatDiv').prop('scrollHeight') }, 300);
          $("#chat_count").val();
        }
      }
    });
  };

  $("#file_attachment").on('change', function(e) { e.preventDefault();
    var $file_input = $('#file_attachment').prop('files')[0];
    if($file_input.size > 1048576 ) { 
      swal('Error','Attachment not more than 1MB','error'); 
      $("#file_attachment").val('');
      $("#file_name").html('');
    }
  });

  /*postMessage = function($message_input="") {
    if($message_input=="") { return; }
    var $file_input = $('#attachment').prop('files')[0];
    var form_data = new FormData();
    
    form_data.append("message_text",$message_input);
    form_data.append("attachment",$file_input);
    form_data.append("appointment_id",appointment_id);
    form_data.append("csrf_stream_token",csrf_stream_token);

    $.ajax({
      url: '<?=base_url("dashboard_doctor/appointment/appointment/register_chat");?>',
      type: 'POST',
      // dataType: 'json',
      processData:false,
      cache:false,
      contentType:false,
      data: form_data,
      success: function(res){ get_messages(); }
    });
  };


  $('.send_message').click(function(e) { 
    var $message_input = $('.message_input').val();
    $(".message_input").val('');
    return postMessage($message_input);  
  });
  $('.message_input').keyup(function(e){ if (e.which === 13) {  
    var $message_input = $('.message_input').val();
    $(".message_input").val('');
    return postMessage($message_input); } 
  });*/
</script>