<style>
    * { box-sizing: border-box }
    body { font-family: "Lato", sans-serif; }
    /* Style the tab */
    .tab {
        float: left;
        border: 1px solid #225595;
        background-color: #3498db;
        width: 30%;
        height: 400px;
    }
    /* Style the buttons inside the tab */
    .tab button {
        display: block;
        background-color: #3498db;
        color: white;
        padding: 22px 16px;
        width: 100%;
        border: none;
        outline: none;
        text-align: left;
        cursor: pointer;
        transition: 0.3s;
        font-size: 17px;
    }
    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #225595;
    }
    /* Create an active/current "tab button" class */
    .tab button.active {
        background-color: #225595;
    }
    /* Style the tab content */
    .tabcontent {
        float: left;
        padding: 0px 12px;
        border: 1px solid #225595;
        width: 70%;
        border-left: none;
        height: 400px;
    }
</style>
<?php $proposal_deposit_price=$milestone_price; ?>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Job Payment'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Job Payment'); ?></h2>
      <small class="m-t-md"><?= $this->lang->line('Choose your payment option and pay for job milestone.'); ?></small>
    </div>
  </div>
</div>
        
<div class="content" style="padding-top: 65px;">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
      <div class="hpanel">
        <div class="panel-body">
      <!-- proposal_details -->
          <div class="tab" style="width:20%">
            <button class="tablinks" onclick="openTab(event, 'Payment')" id="defaultOpen"><?= $this->lang->line('complete_payment'); ?></button>
          </div>

          <div id="Payment" class="tabcontent" style="width:80%">
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                <h2><?= $this->lang->line('Job milestone payment')?></h2>
              </div>
            </div>

            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                <div class="hpanel stats">
                  <div class="panel-body" style="padding-top:0px; padding-bottom:0px">
                    <div class="row">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                        <i class="pe-7s-cash fa-5x"></i>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                        <h1 class="text-success">
                          <strong><?= $this->lang->line('Pay') .' '.$milestone_price.' '. $job_details['currency_code']?></strong>
                        </h1>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                        <p style="margin: 15px 0px 5px 0px;">
                          <?= $this->lang->line('str_c_pay'); ?>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php
              //Get User Payment Methods from profile
              function searchForId($id, $array) {
                foreach ($array as $key => $val) { if ($val['brand_name'] === $id) { return $key; }
                } return null;
              }
              $payment_mode = $this->user->get_customer_payment_methods(trim($job_details['cust_id']));
              //echo json_encode($payment_mode);
              $posMTN = searchForId("MTN",$payment_mode);
              //Get user details
              $user_details = $this->api->get_user_details(trim($job_details['cust_id']));

              $sender_reference = trim($job_details['cust_id']);
              $currency = trim($job_details['currency_code']);
              $lang = substr($_SESSION['language'], 0, 2);
            ?>
            <div class="row text-center">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                <div style="display: inline-flex;">
                  <?php if($currency == 'XAF' || $currency == 'XOF') { ?>
                    <a style="cursor: pointer;" data-toggle="modal" data-target="#paymentMTN" class="btn btn-warning btn-outline"><img style="padding:0px;  margin: 0px;" src="<?=base_url('resources/mtn-logo.png')?>">  &nbsp;&nbsp;&nbsp;<?= $this->lang->line('Pay By MTN'); ?></a>&nbsp;&nbsp;&nbsp;
                    <div class="modal fade" id="paymentMTN" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><?= $this->lang->line('Enter MTN Mobile Number'); ?></h4>
                          </div>
                          <form id="formmomo" method="POST" action="<?=base_url('user-panel-services/job-payment-mtn')?>" target="_top">
                            <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>">
                            <input type="hidden" name="proposal_deposit_price" value="<?= $proposal_deposit_price; ?>" />
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-md-12 form-group">
                                  <div class="col-md-12">
                                    <label class="form-label"><?= $this->lang->line('MTN Mobile Number'); ?></label>
                                    <input type="text" name="phone_no" id="phone_no" class="form-control" placeholder="<?= $this->lang->line('Enter MTN Mobile Number'); ?>" value="<?=(isset($posMTN))?$payment_mode[$posMTN]['card_number']:'';?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
                              <button type="submit" class="btn btn-info" id="btn_submit"><?= $this->lang->line('Pay Now'); ?></button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  <?php } ?>

                  <form  class="pay" action="<?= base_url('user-panel-services/job-per-hour-milestone-payment-stripe');?>" method="POST">
                    <input type="hidden" name="job_id" value="<?= $job_details['job_id']; ?>" />
                    <input type="hidden" name="provider_id" value="<?= $provider_id; ?>" />
                    <input type="hidden" name="milestone_id" value="<?= $milestone_id; ?>" />
                    <input type="hidden" name="milestone_price" value="<?= $milestone_price; ?>" />
                    <script
                      src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                      data-key="pk_test_zbkyUBSvx5wMSon5cemiBCqO"
                      data-amount="<?= $milestone_price;?>"
                      data-name="Gonagoo"
                      data-description="<?= $this->lang->line('job Payment'); ?>"
                      data-image="<?=base_url('resources/images/72x72-payment.png')?>"
                      data-locale="auto"
                      data-zip-code="false"
                      data-allow-remember-me=false
                      data-label="<?= $this->lang->line('pay_with_card'); ?>"
                      data-currency="<?= $job_details['currency_code']; ?>"
                      data-email="<?=$user_details['email1']?>"
                      >
                    </script>
                    <a style="cursor:pointer;" id="btn_mtn_pay" class="btn btn-info btn-outline"><img src="<?=base_url('resources/card_icon.png')?>">&nbsp;&nbsp;&nbsp;<?= $this->lang->line('pay_with_card'); ?></a>
                  </form>&nbsp;&nbsp;&nbsp;

                  <form  class="pay" action="<?= base_url('user-panel-services/job-payment-orange');?>" method="POST" id="pay_orange">
                    <input type="hidden" name="job_id" value="<?= $job_details['job_id']; ?>" />
                    <a style="cursor:pointer;" id="btn_orange_pay" class="btn btn-danger btn-outline"><img src="<?=base_url('resources/logo-orange.png')?>">&nbsp;&nbsp;&nbsp;<?= $this->lang->line('Pay with Orange Money'); ?></a>
                  </form>
                  
                  <?php if($customer_details['is_dedicated'] == 1) { ?>
                    &nbsp;&nbsp;&nbsp;
                    <form action="<?= base_url('user-panel-services/job-payment-bank');?>" method="POST">
                      <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>" />
                      <button type="submit" class="btn btn-warning btn-outline btn-lg"><i class="fa fa-university"></i> <?= $this->lang->line('By Bank'); ?></button>
                    </form>
                  <?php } ?>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function openTab(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  document.getElementById("defaultOpen").click();
</script>
<script>
  $("#formmomo").validate({
    ignore: [],
    rules:{ phone_no: { required:true }, },
    messages:{ phone_no: { required:"Please enter MTN mobile number!" }, }
  });
</script>
<script>
  $(".stripe-button-el").addClass("hidden");
  $('#btn_orange_pay').click(function(){
    $('#pay_orange').submit();
  });
  $('#btn_mtn_pay').click(function(){
    $('.stripe-button-el').click();
  });
</script>
<script>
  $("#submit_promo_form").validate({
    ignore: [],
    rules: { promo_code: {  required:true, }, },
    messages: { promo_code: { required:"<?=$this->lang->line('Please enter promocode!')?>"}, }
  });
</script>
<script>
  $('#btn_submit_promo').click(function(e){
    e.preventDefault();
    var promocode = $('#promo_code').val();
    var job_id = $('#job_id').val();
    $.ajax({
      type: "POST", 
      url: "<?=base_url('user-panel-services/job-promocode-ajax'); ?>", 
      data: { job_id : job_id , promocode : promocode },
      success: function(res) {
        var respp   = res.split('@');
        if(respp[0] == "success"){
          var lines = respp[1].split('$');
          swal( {
            title:lines[0] ,
            text: lines[1],
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?= $this->lang->line('Yes Use this promocode');?>",
            cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
            closeOnConfirm: false,
            closeOnCancel: false 
          },
          function (isConfirm) {
            if (isConfirm) {
              $('#submit_promo_form').submit();
              //swal("<?= $this->lang->line('success'); ?>", "Promocode code applied", "success");
            } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Promocode not used'); ?>", "error"); }
          });
        } else { swal ( "<?= $this->lang->line('error'); ?>" ,  respp[1] ,  "error" ) }
      }
    });
  });
</script>