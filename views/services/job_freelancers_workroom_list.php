<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
   display: none;
  }
  .hoverme {
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
  }
  .btnMenu:hover {
    text-decoration: none;
    background-color: #ffb606;
    color: #FFF;
  }

  .price_tag {
    display: inline-block;
    
    width: auto;
    height: 25px;
    
    background-color: #aac3ca;
    -webkit-border-radius: 3px 4px 4px 3px;
    -moz-border-radius: 3px 4px 4px 3px;
    border-radius: 3px 4px 4px 3px;
    
    border-right: 1px solid #aac3ca;

    /* This makes room for the triangle */
    margin-right: 0px;
    
    position: relative;
    
    color: white;
    font-weight: 300;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 15px;
    line-height: 25px;

    padding: 0 10px 0 10px;
  }


  .li-dropdown-menu {
    position: absolute;
    top: 100%;
    right: 0;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 160px;
    padding: 5px 0;
    margin: 2px 0 0;
    font-size: 14px;
    text-align: left;
    list-style: none;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, .15);
    border-radius: 4px;
    -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
    box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
}
ul, ol {
    margin-top: 0;
    margin-bottom: 10px;
}

  /* Makes the triangle */
  /*.price_tag:before {
    content: "";
    position: absolute;
    display: block;
    right: -19px;
    width: 0;
    height: 0;
    border-top: 13px solid transparent;
    border-bottom: 12px solid transparent;
    border-left: 20px solid #aac3ca;
  }
*/
  /* Makes the circle */
  /*.price_tag:after {
    content: "";
    background-color: white;
    border-radius: 50%;
    width: 6px;
    height: 6px;
    display: block;
    position: absolute;
    right: -9px;
    top: 9px;
  }*/

  .text_limit {
    display: block;
    width: 300px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }


  .custom_checkbox {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 0px;
    padding-top: 7px;
    font-size: 13px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.custom_checkbox input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 8px;
    left: 0;
    height: 18px;
    width: 18px;
    background-color: #eee;
}

/* On mouse-over, add a grey background color */
.custom_checkbox:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.custom_checkbox input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.custom_checkbox input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.custom_checkbox .checkmark:after {
    left: 7px;
    top: 3px;
    width: 5px;
    height: 12px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}

  div.ex3 {
  width: 100%;
  height: 215px;
  overflow: auto;
}



</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/my-jobs'); ?>"><?= $this->lang->line('My Jobs'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Job Workstreams'); ?></span></li>
        </ol>
      </div>

      <!-- <h2 class=""><i class="fa fa-briefcase text-muted"></i> <?=$job_details['job_title']?></h2>
      <small class="m-t-md"></small> -->

      <h3 class="">
        <i class="fa fa-briefcase text-muted"></i> <strong><?=$job_details['job_title']?></strong>&nbsp;&nbsp;&nbsp;
      </h3>

    </div>
  </div>
</div>
<div class="content" style="padding-top: 45px;">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 15px;padding-right: 15px;">
      <div class="hpanel hoverme" style="margin: 25px -7px 0px -7px; background-color:white; border-radius: 10px;">
        <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
          <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding-right: 0px;">
              <input type="text" id="searchbox" class="form-control" placeholder="<?= $this->lang->line('Type here to search for quick search...'); ?>" />
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-8" style="padding-right: 0px;padding-left:0px">  
              <a class="btn btn-link btn-block advanceBtn" id="advanceBtn"><i class="fa fa-filter"></i> <?= $filter == 'advance' ? $this->lang->line('Hide Filters') : $this->lang->line('Show Filters') ?></a>
            </div>
          </div>
          <div class="row" id="advanceSearch" style="margin-top: 10px;">
            <form action="<?= base_url('user-panel-services/job-workstream-list') ?>" method="post">
              <input type="hidden" name="filter" value="advance"/>
              <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>" />
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><?= $this->lang->line('Proposal Status'); ?></label>
                <select id="proposal_status" name="proposal_status" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select'); ?>">
                  <option value="All"><?= $this->lang->line('All'); ?></option>
                  <option value="pending" <?=(isset($proposal_status) && $proposal_status == "pending")?"selected":""?>><?= $this->lang->line('Pending'); ?></option>
                  <option value="decline" <?=(isset($proposal_status) && $proposal_status == "decline")?"selected":""?>><?= $this->lang->line('decline'); ?></option>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><?= $this->lang->line('ratings'); ?></label>
                <select id="ratings" name="ratings" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('ratings'); ?>">
                  <option value="0"><?= $this->lang->line('select'); ?></option>
                  <option value="5" <?=(isset($ratings) && $ratings == "5")?"selected":""?>><?= $this->lang->line("Atleas 5 Star"); ?></option>
                  <option value="4" <?=(isset($ratings) && $ratings == "4")?"selected":""?><?=(isset($ratings) && $ratings == "5")?"selected":""?>><?= $this->lang->line("Atleas 4 Star"); ?></option>
                  <option value="3" <?=(isset($ratings) && $ratings == "3")?"selected":""?>><?= $this->lang->line("Atleas 3 Star"); ?></option>
                  <option value="2" <?=(isset($ratings) && $ratings == "2")?"selected":""?>><?= $this->lang->line("Atleas 2 Star"); ?></option>
                  <option value="1" <?=(isset($ratings) && $ratings == "1")?"selected":""?>><?= $this->lang->line("Atleas 1 Star"); ?></option>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><?= $this->lang->line('city'); ?></label>
                <select id="country_id" name="country_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('location'); ?>">
                  <option value="0"><?= $this->lang->line('select_country'); ?></option>
                  <?php  foreach ($freelancer_country as $c): ?>
                    <option value="<?= $c['country_id'] ?>" <?php if(isset($country_id) && $country_id == $c['country_id']) { echo 'selected'; } ?>><?= $c['country_name']; ?></option>
                  <?php endforeach;?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right" style="margin-top: 23px;padding-left: 0px;">
                <button class="btn btn-success btn-sm filter_submit" type="submit" style="height: 34px;"><i class="fa fa-filter"></i> <?= $this->lang->line('apply.'); ?></button>&nbsp;
                <a id="reset_id" class="btn btn-warning btn-sm" title="<?= $this->lang->line('reset'); ?>" href="<?= base_url()."user-panel-services/job-workstream-list/".$job_details['job_id'] ?>" style="height: 34px;"><i class="fa fa-refresh" style="padding-top: 5px;"></i></a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  
  
  <!-- <?php if($this->session->flashdata('error')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:red; color:#FFF; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('error')?></strong></div>
    </div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:green; color:white; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('success'); ?></strong></div>
    </div>
  <?php endif; ?> -->
 
  <?php if(sizeof($freelancers) > 0) { ?>

      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
        <table id="addressTableData" class="table">
          <thead><tr class="hidden"><th><?=$job['job_id']?></th><th></th></tr></thead>
          <tbody>
            <?php foreach ($freelancers as $profile) { if($profile['profile_status']== "safe") {  ?>
              <tr>
                <td class="hidden"><?=$profile['cust_id'];?></td>
                <td style="width: inherit;">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: -5px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="padding-left: 0px;width: 115px;"> 
                          <a href="<?=$profile['profile_url_login']?>" target="_blank">
                            <img alt="logo" style="height:65px; width: 65px;" class="img-circle m-b-xs" src="<?=base_url($profile['avatar_url'])?>"> </a>
                             <span class="pull-right" style="display: inline-flex;">
                             <button id="btn_add_<?=$profile['cust_id']?>" style="padding: 0px;display:<?=(in_array($profile["cust_id"], $fav_freelancer)?"inline-block":"none")?>" title="<?=$this->lang->line('Remove from favorite')?>" class="btn-link remove_favorite_<?=$profile['cust_id']?>" data-id="<?=$profile['cust_id']?>"><font size="5px" style="color: red"><i id="add_favorite_<?=$profile['cust_id']?>" class="fa fa-heart"></i></font></button>
                             <button id="btn_remove_<?=$profile['cust_id']?>" style="padding: 0px;display:<?=(in_array($profile["cust_id"], $fav_freelancer)?"none":"inline-block")?> " title="<?=$this->lang->line('Add to favorite')?>" class="btn-link add_favorite_<?=$profile['cust_id']?>" data-id="<?=$profile['cust_id']?>"><font size="5px" style="color: red"><i id="add_favorite_<?=$profile['cust_id']?>" class="fa fa-heart-o"></i></font></button>
                             </span>
                              <script>
                                $('.add_favorite_<?=$profile['cust_id']?>').click(function () { 
                                  var id = $(this).attr("data-id");
                                  $.post('<?=base_url("user-panel-services/make-freelancer-favourite")?>', {id: id}, 
                                  function(res) { 
                                    console.log(res);
                                    if($.trim(res) == "success") { 
                                      swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Freelancer added to your favorite list.')); ?>, "success");
                                      $("#btn_remove_<?=$profile['cust_id']?>").css("display", "none");
                                      $("#btn_add_<?=$profile['cust_id']?>").css("display", "inline-block");
                                    } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to add. Try again...')); ?>, "error");  } 
                                  });
                                });
                                $('.remove_favorite_<?=$profile['cust_id']?>').click(function () { 
                                  var id = $(this).attr("data-id");
                                  $.post('<?=base_url("user-panel-services/freelancer-remove-from-favorite")?>', {id: id}, 
                                  function(res) { 
                                    //console.log(res);
                                    if($.trim(res) == "success") { 
                                      swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Freelancer removed from favorite list.')); ?>, "success"); 
                                      $("#btn_remove_<?=$profile['cust_id']?>").css("display", "inline-block");
                                      $("#btn_add_<?=$profile['cust_id']?>").css("display", "none");
                                    } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to remove. Try again...')); ?>, "error");  } 
                                  });
                                });
                              </script>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                          <a href="<?=$profile['profile_url_login']?>" target="_blank">
                          <strong style="font-size:20px"><?=$profile['firstname']." ".$profile['lastname']?>&nbsp;</strong><i class="fa fa-thumbs-o-up"></i> &nbsp; <zz title="<?=$this->lang->line('completed_jobs')?>"> <?=($profile['total_job'] > 0)?$profile['job_percent']."% (".$profile['total_job'].")":""?></zz></a> <?=(isset($profile['proposal_details']) && $profile['proposal_details']['proposal_status'] == "reject")?"<span class='label label-danger'>".$this->lang->line('DECLINED')."</span>":""?>
                            <?=(isset($profile['proposal_details']) && $profile['proposal_details']['proposal_status'] != "reject")?"<span class='label label-success'>".strtoupper($this->lang->line('New Proposal'))."</span>":""?><?=($profile['proposal'] != "yes")?"<span class='label label-warning'>".strtoupper($this->lang->line('Invited'))."</span>":""?>
                           <br/>
                          <span class="text_limit"><?=$profile['introduction']?></span>
                          <i class="fa fa-map-marker"></i>
                          <span style='padding-top: 4px;padding-bottom: 4px;'><?=$profile['city_name'].", ".$profile['country_name']?></span><!-- &nbsp; <?=($profile['portfolio'] > 0)?" <a href='' target='_blank' style='padding-top: 4px;padding-bottom: 4px;' class='btn btn-link portfolio_".$profile['cust_id']."'><strong style='color: #3498db'><i class='fa fa-briefcase' aria-hidden='true'></i>&nbsp;".strtoupper($this->lang->line('view portfolio'))."</strong></a> (".$profile['portfolio']." ".strtoupper($this->lang->line('items')).")":""?> <br/> -->    
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4" style="width: 405px;">
                          <br/>
                          <?php if($profile['proposal'] == 'yes'){ ?>
                            <h5><span class="text_limit" data-toggle="modal" data-target="#my_model<?=$profile['cust_id']?>">
                              <?=$job_details['job_title']?>
                            </span></h6>
                          <?php }else{?>
                            <h5><span class="text_limit">
                              <?=$job_details['job_title']?>
                            </span></h6>
                          <?php }?>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 text-right" style="padding-right:0px">
                          <br/>
                          <div style="display: inline-flex;">
                            <button type="button" class="btn btn-sm btn-outline btn-warning" data-toggle="modal" data-target="#my_model<?=$profile['cust_id']?>" style="border-radius: 0px;border-top-left-radius:3px;border-bottom-left-radius:3px;"><?=$this->lang->line('view')?></button>
                            <div class="btn-group">
                              <button type="button" class="btn btn-sm btn-outline btn-warning dropdown-toggle btn-block" data-toggle="dropdown" style="border-radius: 0px;border-top-right-radius:3px;border-bottom-right-radius:3px;">
                               <span class="caret"></span> 
                            </button>
                              <ul class="dropdown-menu" role="menu">
                                <li>
                                  <form action="<?=base_url('user-panel-services/customer-job-workroom')?>" method="post">
                                    <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>" />
                                    <input type="hidden" name="provider_id" value="<?=$profile['cust_id']?>"/>
                                    <input type="hidden" name="redirect_url" value="job-workstream-list/<?=$job_details['job_id']?>" />
                                    <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align: -webkit-left;"><i class="fa fa-briefcase"></i> <?= $this->lang->line('work_room'); ?></button>
                                  </form>
                                </li>
                                <?php if(isset($profile['proposal_details']) && $profile['proposal_details']['proposal_status'] != "reject" && $profile['proposal'] == 'yes') : ?>
                                  <li>
                                    <?php if($job_details['work_type'] == "per hour" || $job_details['work_type'] == "Daily" || $job_details['work_type'] == "Weekly" || $job_details['work_type'] == "Monthly"){ ?>
                                      <form action="<?=base_url('user-panel-services/accept-per-hour-job-proposal')?>" method="post">
                                    <?php }elseif($job_details['work_type'] == "fixed price"){ ?>  
                                      <form action="<?=base_url('user-panel-services/accept-job-proposal-payment-view')?>" method="post">
                                    <?php } ?>
                                      <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="job-workstream-list" />
                                      <input type="hidden" name="proposal_id" value="<?=$profile['proposal_details']['proposal_id']?>" />
                                      <input type="hidden" name="provider_id" value="<?=$profile['cust_id']?>" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnWithdrawCancel" style="text-align: -webkit-left;"><i class="fa fa-check"></i> <?= $this->lang->line('accept'); ?></button>
                                    </form>
                                  </li>
                                  <li>
                                    <button type="button" class="btn btn-link btn-block btnMenu btn-sm" data-toggle="modal" data-target="#decline_model<?=$profile['cust_id']?>" style="text-align: -webkit-left;"><i class="fa fa-ban"></i> <?= $this->lang->line('decline'); ?></button>
                                  </li>
                                <?php endif; ?>
                              </ul>
                            </div>
                          </div>
                          <div class="modal fade hmodal-warning" id="my_model<?=$profile['cust_id']?>" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header">
                                  <h4 class="modal-title"><?=($profile['proposal']=='yes')?$this->lang->line('Proposal Details'):$this->lang->line('details')?></h4>
                                  <small class="font-bold"></small>
                                </div>
                                <div class="modal-body">
                                  <?php if(isset($profile['proposal_details'])){ ?>
                                  <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                      <?php if(isset($profile['proposal_details']) && $profile['proposal_details']['proposal_status'] == "reject"){ ?>
                                        <h3 style="color: red;"> <?=$this->lang->line('DECLINED')?></h3>
                                      <?php } ?> 
                                    </div>
                                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-xs-7" style="padding-left: 0px;padding-right: 0px">
                                      <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <img alt="logo" style="height:65px; width: 65px;" class="img-circle m-b-xs" src="<?=base_url($profile['avatar_url'])?>">  </a>
                                      </div>
                                      <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <?=$profile['proposal_details']['proposal_date']?> <br/>
                                        <h4><strong> <?=$profile['firstname']." ".$profile['lastname']?></strong> <?=" (".$profile['company_name'].")"?></h4>
                                      </div><br/>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <br/><h4><strong><?=$this->lang->line('New Proposal')?></strong></h4>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="ex3"> 
                                          <p><?=$profile['proposal_details']['proposal_details']?></p>
                                        </div> 
                                      </div>
                                    </div>
                                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                                        <h3><strong><?=$profile['proposal_details']['total_price']." ".$profile['proposal_details']['currency_code']?></strong></h3>
                                        <?=(!$job_details['job_type'])?$job_details['work_type']:"";?> <br/>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <br/>
                                        <strong><?=$this->lang->line('details')?></strong><hr style="margin-bottom: 5px;margin-top: 5px;">
                                      </div>
                                      <?php if($job_details['work_type'] == "per hour" || $job_details['work_type'] == "Daily" || $job_details['work_type'] == "Weekly" || $job_details['work_type'] == "Monthly"){ ?>
                                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <?=$this->lang->line('Billing period')?>
                                      </div>
                                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                                        <?=$job_details['billing_period']?>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <hr style="margin-bottom: 5px;margin-top: 5px;">
                                      </div>
                                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <?=$this->lang->line('Hours as per billing period')?>
                                      </div>
                                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right"> 
                                        <?=$profile['proposal_details']['hours_billing_type']." ".$this->lang->line('hours')?>
                                      </div>
                                        
                                      <?php }elseif($job_details['work_type'] == "fixed price"){ ?>
                                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <?=$this->lang->line('Deposit')?>
                                      </div>
                                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                                        <?=$profile['proposal_details']['deposit_price']." ".$profile['proposal_details']['currency_code']?>
                                      </div>
                                      <?php } ?>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <hr style="margin-bottom: 5px;margin-top: 5px;">
                                      </div>
                                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <?=$this->lang->line('Proposal Id')?>
                                      </div>
                                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                                        <?="#REF-".$profile['proposal_details']['proposal_id']?> <br/>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                                        <br/>
                                        <div class="btn-group">
                                          <button type="button" class="btn btn-sm btn-outline btn-warning dropdown-toggle btn-block" data-toggle="dropdown">
                                         <i class="fa fa-wrench" aria-hidden="true"></i> <?=$this->lang->line('action')?>&nbsp;&nbsp;&nbsp; <span class="caret"></span> 
                                          </button>
                                          <ul class="dropdown-menu" role="menu">
                                            <li>

                                              <form action="<?=base_url('user-panel-services/customer-job-workroom')?>" method="post">
                                                <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>" />
                                                <input type="hidden" name="provider_id" value="<?=$profile['cust_id']?>"/>
                                                <input type="hidden" name="redirect_url" value="job-workstream-list" />
                                                <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" style="text-align: -webkit-left;"><i class="fa fa-briefcase"></i> <?= $this->lang->line('work_room');  ?></button>
                                              </form>
                                            </li>
                                            <?php if(isset($profile['proposal_details']) && $profile['proposal_details']['proposal_status'] != "reject" && $profile['proposal'] == 'yes') : ?>
                                              <li>
                                                <?php if($job_details['work_type'] == "per hour" || $job_details['work_type'] == "Daily" || $job_details['work_type'] == "Weekly" || $job_details['work_type'] == "Monthly"){ ?>
                                                  <form action="<?=base_url('user-panel-services/accept-per-hour-job-proposal')?>" method="post">
                                                <?php }elseif($job_details['work_type'] == "fixed price"){ ?>  
                                                  <form action="<?=base_url('user-panel-services/accept-job-proposal-payment-view')?>" method="post">
                                                <?php } ?>
                                                  <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>" />
                                                  <input type="hidden" name="redirect_url" value="job-workstream-list" />
                                                  <input type="hidden" name="proposal_id" value="<?=$profile['proposal_details']['proposal_id']?>" />
                                                  <input type="hidden" name="provider_id" value="<?=$profile['cust_id']?>" />
                                                  <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" style="text-align: -webkit-left;"><i class="fa fa-check"></i> <?= $this->lang->line('accept'); ?></button>
                                                </form>
                                              </li>
                                              <li>
                                                <button type="button" class="btn btn-link btn-block btnMenu btn-sm" data-toggle="modal" data-target="#decline_model<?=$profile['cust_id']?>" style="text-align: -webkit-left;"><i class="fa fa-ban"></i> <?= $this->lang->line('decline'); ?></button>
                                              </li>
                                            <?php endif; ?>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <br/>
                                        <strong><?=$this->lang->line('Attachments')?></strong><hr style="margin-bottom: 5px;margin-top: 5px;">
                                      </div>
                                      <?php if($profile['proposal_details']['attachment_1'] != "NULL"): ?>
                                      <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-xs-4">

                                        <a href="<?=base_url($profile['proposal_details']['attachment_1'])?>" download><i class="fa fa-download"></i> </a>

                                      </div> 

                                      <?php endif; ?>
                                     <?php if($profile['proposal_details']['attachment_2'] != "NULL"): ?>
                                      <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-xs-4">
                                        <a href="<?=base_url($profile['proposal_details']['attachment_2'])?>" download><i class="fa fa-download"></i> </a>

                                      </div>

                                      <?php endif; ?>
                                     <?php if($profile['proposal_details']['attachment_3'] != "NULL"): ?>
                                      <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-xs-4">
                                        <a href="<?=base_url($profile['proposal_details']['attachment_3'])?>" download><i class="fa fa-download"></i> </a>

                                      </div>
                                      <?php endif; ?>
                                     <?php if($profile['proposal_details']['attachment_4'] != "NULL"): ?>

                                      <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-xs-4">
                                        <a href="<?=base_url($profile['proposal_details']['attachment_4'])?>" download><i class="fa fa-download"></i> </a>

                                      </div>

                                      <?php endif; ?>
                                     <?php  if($profile['proposal_details']['attachment_5'] != "NULL"): ?>
                                      <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-xs-4">
                                        <a href="<?=base_url($profile['proposal_details']['attachment_5'])?>" download><i class="fa fa-download"></i> </a>

                                      </div>
                                      <?php endif; ?>
                                 
                                    </div>
                                  </div>
                                  <?php }else{ ?>
                                  <div class="row">
                                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-xs-7" style="padding-left: 0px;padding-right: 0px">
                                      <h4><strong><?=$this->lang->line('Invited')?></strong></h4>
                                    </div>
                                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    </div>
                                  </div>
                                  <?php } ?>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal fade hmodal-danger" id="decline_model<?=$profile['cust_id']?>" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header">
                                    <h4 class="modal-title"><?=$this->lang->line('What is the reason for declining?')?></h4>
                                    <small class="font-bold"></small>
                                </div>
                                <div class="modal-body">
                                  <div class="row">
                                    <form action="<?=base_url('user-panel-services/job-proposal-decline')?>" method="post">
                                      <input type="hidden" name="proposal_id" value="<?=$profile['proposal_details']['proposal_id']?>" />
                                      <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>" />
                                      <input type="hidden" name="provider_id" value="<?=$profile['cust_id']?>" />
                                      <input type="hidden" name="redirect_url" value="job-workstream-list" />
                                      
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <?php foreach($decline_reason as $reason){ ?>
                                        <div class='radio radio-success'>
                                          <input type="radio"  name="reason" value="<?=$reason['title']?>" required>
                                          <label><strong><?=$reason['title']?></strong></label>
                                        </div>
                                      <?php } ?>
                                        <div class='radio radio-success'>
                                          <input type="radio" name="reason" value="other" required>
                                          <label><strong><?=$this->lang->line('other')?></strong></label>
                                        </div>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 other_<?=$profile['cust_id']?>" style="display: none;">
                                        <textarea style="resize:none"  rows="3" id="other_desc" name="other_desc" type="textarea" class="form-control" placeholder="<?= $this->lang->line('other'); ?>" autocomplete="none"></textarea><br/>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                                        <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-ban"></i> <?= $this->lang->line('decline'); ?></button>
                                      </div>
                                      <script>
                                        $('input[type=radio][name=reason]').change(function() {
                                          if (this.value == 'other') {
                                            //$('.other_<?=$profile['cust_id']?>').show();
                                            $('.other_<?=$profile['cust_id']?>').css('display', 'block');
                                          }
                                          else {
                                            //$('.other_<?=$profile['cust_id']?>').hide();
                                            $('.other_<?=$profile['cust_id']?>').css('display', 'none');
                                          }
                                        });
                                      </script>
                                    </form>
                                  </div>
                                </div>      
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php }} ?>
          </tbody>
        </table>
      </div>
      
  <?php } else { ?>
    <div class="hpanel hoverme" style="background-color:white;border-radius: 10px;margin-bottom: 0px;margin-top: 157px;margin-left: 7px;margin-right: 7px;">
      <div class="panel-body" style="border-radius: 10px; padding: 15px;">
        <h5><?=($is_fav == "no")?$this->lang->line('No freelancer found.'):$this->lang->line('No favourite freelancer.')?></h5>
      </div>
    </div>
  <?php } ?>
</div>
</div>

<script>
  $('#advanceBtn').click(function() {
    if($('#advanceSearch').css('display') == 'none') {
      $('#basicSearch').hide("slow");
      $('#advanceSearch').show("slow");
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Hide Filters'))?>);
      //$('#searchbox').hide("slow");
      //$('#searchlabel').hide("slow");
    } else {
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Show Filters'))?>);
      $('#advanceSearch').hide("slow");
      $('#basicSearch').show("slow");
      //$('#searchbox').show("slow");
      //$('#searchlabel').show("slow");
    }
    return false;
  });
</script>
