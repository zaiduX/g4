<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
   display: none;
  }
  .hoverme {
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
  }
  .btnMenu:hover {
    text-decoration: none;
    background-color: #ffb606;
    color: #FFF;
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Pending Proposals'); ?></span></li>
        </ol>
      </div>
      <h3 class="">
        <i class="fa fa-briefcase text-muted"></i> <strong><?= $this->lang->line('Pending job proposals list'); ?></strong>
      </h3>
    </div>
  </div>
</div>
<div class="content" style="padding-top: 45px;">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="hpanel hoverme" style="margin: 25px -7px 0px -7px; background-color:white; border-radius: 10px;">
        <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <input type="text" id="searchbox" class="form-control" placeholder="<?= $this->lang->line('Type here to search for quick search...'); ?>" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php if($this->session->flashdata('error')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:red; color:#FFF; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('error'); ?></strong></div>
    </div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:green; color:white; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('success'); ?></strong></div>
    </div>
  <?php endif; ?>

  <?php if(sizeof($job_list) > 0) { ?>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
        <table id="addressTableData" class="table">
          <thead><tr class="hidden"><th><?=$job['job_id']?></th><th></th></tr></thead>
          <tbody>
            <?php foreach ($job_list as $job) { if($job['service_type'] == 1) { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: -5px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding-left: 0px;">
                            <p style="margin: 0px;"><img align="middle" src="<?=base_url($job['icon_url'])?>" /> <?=$job['cat_name']?></p>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
                            <h6><i class="fa fa-tag"></i> Ref #GS-<?=$job['job_id']?></h6>
                          </div>
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12 text-right" style="padding-right: 0px;">
                            <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=$job['actual_price']?> <?=$job['currency_code']?></strong></h4>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <h4 style="margin-top: 5px;"><?=$job['job_title']?>&nbsp;&nbsp;<span class="label label-primary"><?=($job['service_type']==1)?$this->lang->line('Offer'):$this->lang->line('Project')?></span>&nbsp;&nbsp;    <?php if($job['cancellation_status'] != 'NULL'): ?>
                                <span class="label label-warning"><?= ucwords(str_replace('_', ' ', $job['cancellation_status'])) ?></span>
                              <?php endif; ?>
                            </h4>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <form action="<?=base_url('user-panel-services/customer-job-offer-workroom')?>" method="post">
                              <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                              <button type="submit" class="btn btn-sm btn-outline btn-success btn-block" id="btnWorkroom"><i class="fa fa-briefcase"></i> <?= $this->lang->line('work_room'); ?></button>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <i class="fa fa-calendar"></i> <?= $this->lang->line('Purchased on'); ?>: <?=date('D, d M y', strtotime($job['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-sort-numeric-asc"></i> <?= $this->lang->line('quantity'); ?>: <?=$job['offer_qty']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-asterisk"></i> <?= $this->lang->line('Quick delivery'); ?>: <?=($job['offer_quick_delivery_charges']>0)?$this->lang->line('yes'):$this->lang->line('no')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-list-ol"></i> <?= $this->lang->line('No. of Add-ons'); ?>: <?=$job['no_of_addons']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-user"></i> <?= $this->lang->line('Offered by'); ?>: <?=$job['free_company_name']?>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <div class="btn-group" style="width: 100%">
                              <button type="button" class="btn btn-sm btn-outline btn-warning dropdown-toggle btn-block" data-toggle="dropdown">
                               <i class="fa fa-wrench"></i> <?= $this->lang->line('Actions'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span> 
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li>
                                  <form action="<?=base_url('user-panel-services/customer-job-details')?>" method="post">
                                    <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                    <input type="hidden" name="redirect_url" value="pending-job-proposals" />
                                    <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align: -webkit-left;"><i class="fa fa-eye"></i> <?= $this->lang->line('view_details'); ?></button>
                                  </form>
                                </li>
                                <?php if($job['complete_paid'] == 0 && $job['cancellation_status'] == 'NULL') : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/customer-offer-payment')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnPayment" style="text-align: -webkit-left;"><i class="fa fa-money"></i> <?= $this->lang->line('complete_payment'); ?></button>
                                    </form>
                                  </li>
                                <?php endif; ?>
                                <?php if($job['cancellation_status'] == 'cancel_request') : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/customer-withdraw-cancel-request')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="pending-job-proposals" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnWithdrawCancel" style="text-align: -webkit-left;"><i class="fa fa-recycle"></i> <?= $this->lang->line('Withdraw request'); ?></button>
                                    </form>
                                  </li>
                                <?php endif; ?>
                                <?php if($job['cancellation_status'] == 'NULL') : ?>
                                  <li>
                                    <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnCancel" data-toggle="modal" data-target="#myModal_<?=$job['job_id']?>" style="text-align: -webkit-left;"><i class="fa fa-times"></i> <?= $this->lang->line('Cancel Purchase'); ?></button>
                                    <div id="myModal_<?=$job['job_id']?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"><?=$this->lang->line('What is the reason for cancelling?')?></h4>
                                          </div>
                                            <form id="frmCancel_<?=$job['job_id']?>" action="<?=base_url('user-panel-services/customer-offer-cancel')?>" method="post">
                                              <div class="modal-body">
                                                <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                                <input type="hidden" name="redirect_url" value="pending-job-proposals" />
                                                <div class="row">
                                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
                                                    <?php for ($i=0; $i < sizeof($cancellation_reasons); $i++) { ?>
                                                      <div class="radio radio-success">
                                                        <input type="radio" name="reason" id="<?=$i?>" value="<?=$cancellation_reasons[$i]['title']?>" />
                                                        <label for="<?=$i?>"><?=$cancellation_reasons[$i]['title']?></label>
                                                      </div>
                                                    <?php } ?>
                                                    <div class="radio radio-success">
                                                      <input type="radio" name="reason" value="other" id="other" />
                                                      <label for="other"><?=$this->lang->line('other')?></label>
                                                    </div>
                                                  </div>
                                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <label class="label-control"><?=$this->lang->line('Provide a reason')?></label>
                                                    <textarea name="other_reason" class="form-control" style="resize: none;"></textarea>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-default"><?=$this->lang->line('btn_submit')?></button>
                                              <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                                            </div>
                                          </form>
                                          <script>
                                            $("#frmCancel_<?=$job['job_id']?>").validate({
                                              ignore: [], 
                                              rules: { reason: { required : true } },
                                              errorPlacement: function(error, element) {
                                                if(element.is(":radio")) { error.insertAfter(element.parent().parent());
                                                } else { error.insertAfter(element); }
                                              },
                                              messages: { reason: { required : <?=json_encode($this->lang->line('Select cancellation reason!'))?> } },
                                            });
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                <?php endif; ?>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } else { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: -5px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding-left: 0px;">
                            <p style="margin: 0px;"><img align="middle" src="<?=base_url($job['icon_url'])?>" /> <?=$job['cat_name']?></p>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
                            <h6><i class="fa fa-tag"></i> Ref #GS-<?=$job['job_id']?></h6>
                          </div>
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12 text-right" style="padding-right: 0px;">
                            <?php if($job['job_type']==0){ ?>
                            <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=$job['budget']?> <?=$job['currency_code']?> <small> <?=($job['work_type'] == "fixed price")?$this->lang->line('Fixed Price'):$job['work_type']?> </small></strong></h4>
                            <?php }else{ ?>
                              <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=(!$job['immediate'])?$this->lang->line('Immediate'):$this->lang->line('On Date')?><small> <?=($job['immediate'])?date('d M Y' , strtotime($job['complete_date'])):'';?> </small></strong></h4>
                            


                            <?php } ?>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <h4 style="margin-top: 5px;"><?=$job['job_title']?>&nbsp;&nbsp;<span class="label label-primary"><?=($job['service_type']==1 && $job['job_type']==0)?$this->lang->line('Offer'):($job['service_type']==0 && $job['job_type']==0)?$this->lang->line('Project'):$this->lang->line('Troubleshoot')?></span>&nbsp;&nbsp;
                              

                              <?php if($job['proposal_status'] == 'open'){?>
                              <span class="label label-success"><?=$this->lang->line('open')?></span> &nbsp;&nbsp;
                              <?php }elseif($job['proposal_status'] == 'reject'){ ?>
                              <span class="label label-danger"><?=$this->lang->line('rejected')?></span> &nbsp;&nbsp;
                              <?php } ?>
                              

                              <?php if($job['cancellation_status'] != 'NULL'): ?>
                                <span class="label label-warning"><?= ucwords(str_replace('_', ' ', $job['cancellation_status'])) ?></span>
                              <?php endif; ?>
                            </h4>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <form action="<?=base_url('user-panel-services/provider-job-workroom')?>" method="post">
                              <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                              <input type="hidden" name="cust_id" value="<?=$job['cust_id']?>"/>
                              <input type="hidden" name="redirect_url" value="<?=$redirect_url?>" />
                              <button type="submit" class="btn btn-sm btn-outline btn-success btn-block" id="btnWorkroom"><i class="fa fa-briefcase"></i> <?= $this->lang->line('work_room'); ?></button>
                            </form>
                          </div>
                        </div>
                      </div> 
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <i class="fa fa-clock-o"></i> <?= $this->lang->line('posted on'); ?>: <?=date('D, d M y', strtotime($job['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <i class="fa fa-map-marker"></i>&nbsp;<?=$job['location_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-dot-circle-o"></i> <?=$this->lang->line('Proposals').": ".$job['proposal_counts']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php if($job['contract_expired_on'] == "NULL"){ ?>
                              <i class="fa fa-info"></i>&nbsp;<?=$this->lang->line('Job type')?>:&nbsp;<?=$this->lang->line('Unlimited')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php }else{ ?>
                              <i class="fa fa-clock-o"></i> <?= $this->lang->line('Job expire on'); ?>: <?=date('D, d M y', strtotime($job['contract_expired_on']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php } ?>


                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <div class="btn-group" style="width: 100%">
                              <button type="button" class="btn btn-sm btn-outline btn-warning dropdown-toggle btn-block" data-toggle="dropdown">
                               <i class="fa fa-wrench"></i> <?= $this->lang->line('Actions'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span> 
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li>
                                  <form action="<?=base_url('user-panel-services/customer-job-details-view')?>" method="post">
                                    <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                    <input type="hidden" name="redirect_url" value="pending-job-proposals" />
                                    <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align: -webkit-left;"><i class="fa fa-eye"></i> <?= $this->lang->line('view_details'); ?></button>
                                  </form>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php } else { ?>
    <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: 0px; margin-top: 20px; margin-left: -7px; margin-right: -7px;">
      <div class="panel-body" style="border-radius: 10px; padding: 15px;">
        <h5><?= $this->lang->line('No jobs found.'); ?></h5>
      </div>
    </div>
  <?php } ?>
</div>

<script>
  $('#advanceBtn').click(function() {
    if($('#advanceSearch').css('display') == 'none') {
      $('#basicSearch').hide("slow");
      $('#advanceSearch').show("slow");
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Hide Filters'))?>);
      //$('#searchbox').hide("slow");
      //$('#searchlabel').hide("slow");
    } else {
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Show Filters'))?>);
      $('#advanceSearch').hide("slow");
      $('#basicSearch').show("slow");
      //$('#searchbox').show("slow");
      //$('#searchlabel').show("slow");
    }
    return false;
  });
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-categories-by-type", 
      data: { type_id: cat_type_id },
      dataType: "json",
      success: function(res) {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('select_category')?></option>");
        $.each( res, function() {
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function() {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('loading')?></option>");
      },
      error: function() {
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('no_options')?></option>");
      }
    })
  });
</script>