<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/provider-profile'); ?>"><?= $this->lang->line('Provider Profile'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Add'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-files-o fa-2x text-muted"></i> <?= $this->lang->line('documents'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Add business documents'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="hpanel hblue">
      <div class="panel-body">
        <?php if($this->session->flashdata('error')):  ?>
          <div class="form-group">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
              <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
            </div>
          </div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="form-group">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            </div>
          </div>
        <?php endif; ?>

        <form action="<?= base_url('user-panel-services/add-provider-document'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="uploadPhoto">
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label class=""><?= $this->lang->line('document_type'); ?></label>
              <?php 
                $arrType = array(); 
                for ($i=0; $i < sizeof($document); $i++) { 
                  array_push($arrType, $document[$i]['doc_type']); } 
              ?>
              <select class="js-source-states" style="width: 100%" name="doc_type">
                <?php if(!in_array('business', $arrType)) { ?>
                        <option value="business"><?= $this->lang->line('business'); ?></option>
                <?php } ?>
                <?php if(!in_array('insurance', $arrType)) { ?>
                        <option value="insurance"><?= $this->lang->line('insurance'); ?></option>
                <?php } ?>
                <?php if(!in_array('other', $arrType)) { ?>
                        <option value="other"><?= $this->lang->line('other'); ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label class=""><?= $this->lang->line('select_document'); ?></label>
              <input type="file" class="form-control" name="attachment" id="fileAttachment" accept=".doc,.docx,.pdf">
              <span id="errorMsg" class="text-danger hidden"><strong><?= $this->lang->line('error'); ?></strong> <?= $this->lang->line('please_select_document'); ?></span>
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="panel-footer"> 
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                <button type="submit" class="btn btn-info" data-style="zoom-in" id="upload"><?= $this->lang->line('upload_document'); ?></button>  
                <a href="<?= base_url('user-panel-services/provider-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>
              </div>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#uploadPhoto").submit(function(e){ e.preventDefault(); });
  $("#upload").on('click', function(e) { e.preventDefault(); 
    var file = $('#fileAttachment').val();
    if(file !="") { $("#uploadPhoto").get(0).submit();  } 
    else { $('#errorMsg').removeClass('hidden'); }
  });
</script>