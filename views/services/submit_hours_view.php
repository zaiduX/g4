</style>
	<div class="normalheader small-header">
		<div class="hpanel">
			<div class="panel-body">
				<a class="small-header-action" href="">
					<div class="clip-header">
						<i class="fa fa-arrow-up"></i>
					</div>
				</a>

				<div id="hbreadcrumb" class="pull-right">
					<ol class="hbreadcrumb breadcrumb">
						<li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
						<li><a href="<?= $this->config->item('base_url') . 'user-panel-services/provider-inprogress-jobs'; ?>"><?= $this->lang->line('in_progress'); ?></a></li>
						<li class="active"><span><?= $this->lang->line('Submit work hours'); ?></span></li>
					</ol>
				</div>
				<h2 class="font-light m-b-xs">
				   <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Submit work hours'); ?>
				</h2>
				<small class="m-t-md"><?= $this->lang->line('Send request to customer to pay milestone price as per hour basis'); ?>.</small>
			</div>
		</div>
	</div>

	<div class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="hpanel">
					<div class="panel-body">
						<form method="post" class="form-horizontal" action="<?=base_url('user-panel-services/provider-submit-hours')?>" id="MilestoneRequest">
							<input type="hidden" name="job_id" value="<?= $job_details['job_id']?>">
							<div class="form-group">
								<div class="col-md-12 text-center">
									<?php if($this->session->flashdata('error')):  ?>
									<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
									<?php endif; ?>
									<?php if($this->session->flashdata('success')):  ?>
									<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="col-md-10 col-md-offset-1">
										<div class="col-md-4">
		                  <label><?=strtoupper($this->lang->line('from'))?> </label>
		                  <div class="input-group date">
		                    <input type="text" class="form-control" required="true" id="from_date" name="from_date" autocomplete="none" value="" />
		                    <div class="input-group-addon">
		                      <span class="glyphicon glyphicon-th"></span>
		                    </div>
		                  </div>                      
										</div>
										<div class="col-md-4">
		                  <label><?= strtoupper($this->lang->line('to'))?> </label>
		                  <div class="input-group date">
		                    <input type="text" class="form-control" id="to_date" name="to_date" required="true" autocomplete="none" value="" />
		                    <div class="input-group-addon">
		                      <span class="glyphicon glyphicon-th"></span>
		                    </div>
		                  </div>                      
										</div>
										<div class="col-md-4">
											<label class=""><?= strtoupper($this->lang->line('Hours worked')); ?> </label>
                  		<input type="number" min="1" id="hours" required="true" name="hours" class="form-control" autocomplete="none">
										</div>
										<br>
										<div class="col-md-12">
											<label style="margin-top: 10px;" class=""><?= strtoupper($this->lang->line('desc')); ?> </label>
                  		<textarea style="resize:none" required="true" rows="4" id="description" name="description" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Provide a more detailed in description'); ?>" autocomplete="none"></textarea> <br/>
										</div>
										<br>
										<div class="col-md-6 col-md-offset-6 text-right">
											<button class="btn btn-info" type="submit"><?= $this->lang->line('Submit work hours'); ?></button>
											<a href="<?= $this->config->item('base_url') . 'user-panel-services/provider-inprogress-jobs'; ?>" class="btn btn-primary" type="submit"><?= $this->lang->line('cancel');?></a>
										</div>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>