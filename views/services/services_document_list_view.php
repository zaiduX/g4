<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/user-profile'); ?>"><?= $this->lang->line('profile'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('documents'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-photo fa-2x text-muted"></i> <?= $this->lang->line('documents'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('list_of_added_documents'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="hpanel hblue">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <h4>
              <strong> <?= $this->lang->line('documents'); ?> </strong> &nbsp;
              <a href="<?= base_url('user-panel-services/document/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
            </h4>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
            <a href="<?= base_url('user-panel-services/user-profile'); ?>" class="btn btn-primary"><i class="fa fa-left-arrow"></i> <?= $this->lang->line('go_to_profile'); ?></a> 
          </div>
        </div>
      </div>
      
      <?php foreach($documents as $doc ): ?>
        <div class="panel-body" style="padding-bottom: 0px; padding-top: 10px; margin-bottom: 5px;">
          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
            <div class="lightBoxGallery">
              <?php if($doc['attachement_url'] != "NULL"): ?>
                <a href="<?= base_url($doc['attachement_url']); ?>" title="Image from Unsplash" data-gallery="">
                  <img src="<?= base_url($doc['attachement_url']); ?>" class="img-thumbnail" style="height: 72px; width: auto;">
                </a>
              <?php else: ?>
                <?= $this->lang->line('not_found'); ?>
              <?php endif; ?>
            </div> 
          </div>
          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-6 col-xs-6">
            <div class="row">
              <div class="col-md-3"> <p><strong><?= $this->lang->line('name'); ?></strong></p> </div>
              <div class="col-md-7"> <p style="word-wrap: break-word;"><?= $doc['doc_title']; ?></p> </div>  
              <div class="col-md-2 text-right">
                <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-services/document/').$doc['doc_id'];?>">
                  <i class="fa fa-pencil"></i>
                </a>
                <a class="btn btn-danger btn-circle btn-delete" id="<?=$doc['doc_id'];?>">
                  <i class="fa fa-trash"></i>
                </a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3"> <p><strong><?= $this->lang->line('description'); ?></strong></p> </div>
              <div class="col-md-8"> <p style="word-wrap: break-word;">
                <?php if( $doc['doc_desc'] != "NULL"){ echo nl2br($doc['doc_desc']); } else { echo $this->lang->line('not_defined'); } ?>
              </p></div>  
            </div>
          </div>
        </div>
      <?php endforeach; ?> 
    </div>
  </div>
</div>

<script>
  $('.btn-delete').click(function () {            
    var id = this.id;            
    swal({                
      title: <?= json_encode($this->lang->line('are_you_sure'))?>,                
      text: <?= json_encode($this->lang->line('you_will_not_be_able_to_recover_this_record'))?>,                
      type: "warning",                
      showCancelButton: true,                
      confirmButtonColor: "#DD6B55",                
      confirmButtonText: <?= json_encode($this->lang->line('yes'))?>,                
      cancelButtonText: <?= json_encode($this->lang->line('no'))?>,                
      closeOnConfirm: false,                
      closeOnCancel: false, 
    },                
    function (isConfirm) {                    
      if (isConfirm) {                        
        $.post("<?=base_url('user-panel-services/delete-document')?>", {id: id}, 
        function(res){ console.log(res);
          if(res == "success") {
            swal("<?=$this->lang->line('deleted')?>", "<?=$this->lang->line('Deleted successfully.')?>", "success");
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal("<?=$this->lang->line('error')?>", "<?=$this->lang->line('While deleting record!')?>", "error"); }
        }); 
      } else {  swal("<?=$this->lang->line('canceled')?>", "<?=$this->lang->line('Record is safe!')?>", "error");  }  
    });            
  });
</script>