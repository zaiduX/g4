<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
   display: none;
  }
  .hoverme {
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
  }
  .btnMenu:hover {
    text-decoration: none;
    background-color: #ffb606;
    color: #FFF;
  }

  .fa-heart-o: {
    content: "\f08a";
    color: red;
    text-shadow: 1px 1px 1px #ccc;
    font-size: 0.7em;
  }

  .fa-heart:{
    content: "\f08a";
    color: red;
    text-shadow: 1px 1px 1px #ccc;
    font-size: 0.7em;
  } 
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Freelance projects'); ?></span></li>
        </ol>
      </div>
      <h3 class="">
        <i class="fa fa-search text-muted"></i> <strong><?= $this->lang->line('Freelance projects'); ?> - <?= sizeof($jobs)." ".$this->lang->line('Found'); ?></strong>&nbsp;&nbsp;&nbsp;
        <a class="btn btn-outline btn-info hidden" href="<?=base_url('user-panel-services/my-favorite-offers'); ?>"><i class="fa fa-heart"></i> <?= $this->lang->line('Favorite offers'); ?></a>
      </h3>
    </div>
  </div>
</div>
<div class="content" style="padding-top: 45px;">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="hpanel hoverme" style="margin: 25px -7px 0px -7px; background-color:white; border-radius: 10px;">
        <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
          <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-9 col-sm-9 col-xs-12">
              <input type="text" id="searchbox" class="form-control" placeholder="<?= $this->lang->line('Type here to search for quick search...'); ?>" />
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-xs-12">
              <a class="btn btn-link btn-block advanceBtn" id="advanceBtn"><i class="fa fa-filter"></i> <?= $filter == 'advance' ? $this->lang->line('Hide Filters') : $this->lang->line('Show Filters') ?></a>
            </div>
          </div>
          <div class="row" id="advanceSearch" style="<?= ($filter == 'basic') ? 'display:none' : '' ?>; margin-top: 10px;">
            <form action="<?= base_url('user-panel-services/browse-project') ?>" method="post" id="offerFilter">
              <input type="hidden" name="filter" value="advance" />
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-angle-double-right"></i> <?= $this->lang->line('category_type'); ?></label>
                <select id="cat_type_id" name="cat_type_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category_type'); ?>">
                  <option value="0"><?= $this->lang->line('select_category_type'); ?></option>
                  <?php foreach ($category_types as $ctypes): ?>
                    <option value="<?= $ctypes['cat_type_id'] ?>" <?php if(isset($cat_type_id) && $cat_type_id == $ctypes['cat_type_id']) { echo 'selected'; } ?>><?= $ctypes['cat_type']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-angle-right"></i> <?= $this->lang->line('category'); ?></label>
                <select id="cat_id" name="cat_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category'); ?>">
                  <?php if(isset($cat_type_id)) { 
                    if(!isset($cat_id) || $cat_id <= 0) { ?>
                      <option value="0"><?= $this->lang->line('select_category'); ?></option>
                    <?php } ?>
                    <?php foreach ($categories as $category): ?>
                      <option value="<?= $category['cat_id'] ?>"<?php if($cat_id == $category['cat_id']) { echo 'selected'; } ?>><?= $category['cat_name']; ?></option>
                    <?php endforeach ?> 
                  <?php } else { ?>
                    <option value="0"><?= $this->lang->line('select_category'); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-calendar"></i> <?= $this->lang->line('From date'); ?></label>
                <div class="input-group date" data-provide="datepicker" data-date-start-date="" data-date-end-date="" style="">
                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php if(isset($start_date) && $start_date != 'NULL'){echo $start_date;} ?>" autocomplete="off" />
                  <div class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </div>
                </div>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-calendar"></i> <?= $this->lang->line('To date'); ?></label>
                <div class="input-group date" data-provide="datepicker" data-date-start-date="" data-date-end-date="" style="">
                  <input type="text" class="form-control" id="end_date" name="end_date" value="<?php if(isset($end_date) && $end_date != 'NULL'){echo $end_date;} ?>" autocomplete="off"/>
                  <div class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </div>
                </div>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right" style="display: inline-flex; margin-top: 23px;">
                <button class="btn btn-success btn-sm" type="submit" style="height: 34px;"><i class="fa fa-filter"></i> <?= $this->lang->line('apply.'); ?></button>&nbsp;
                <a class="btn btn-warning btn-sm" title="<?= $this->lang->line('reset'); ?>" href="<?= base_url('user-panel-services/browse-project') ?>" style="height: 34px;"><i class="fa fa-refresh" style="padding-top: 5px;"></i></a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php if($this->session->flashdata('error')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:red; color:#FFF; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('error'); ?></strong></div>
    </div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:green; color:white; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('success'); ?></strong></div>
    </div>
  <?php endif; ?>

  <?php if(sizeof($jobs) > 0) { ?>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
        <table id="addressTableData" class="table">
          <thead><tr class="hidden"><th><?=$job['job_id']?></th><th></th></tr></thead>
          <tbody>
            <?php foreach ($jobs as $job) { if($job['service_type'] == 0) { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: -5px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding-left: 0px;">
                            <p style="margin: 0px;"><img align="middle" src="<?=base_url($job['icon_url'])?>" /> <?=$job['cat_name']?></p>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
                            <h6><i class="fa fa-tag"></i> Ref #GS-<?=$job['job_id']?></h6>
                          </div>
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12 text-right" style="padding-right: 0px;">
                            <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=($job['work_type']!="Per Hour")?$job['currency_code']." ".$job['budget']:$job['currency_code']." ".$job['budget']."<small>/hr</small>"?></strong></h4>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <h4 style="margin-top: 5px;"><?=$job['job_title']?>&nbsp;&nbsp;<span class="label label-primary"><?=($job['service_type']==1)?$this->lang->line('Offer'):$this->lang->line('Project')?></span>&nbsp;&nbsp;  <!--   <?php if($job['cancellation_status'] != 'NULL'): ?>
                                <span class="label label-warning"><?= ucwords(str_replace('_', ' ', $job['cancellation_status'])) ?></span> 
                               <?php endif; ?> -->
                            </h4>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                           <?=$job['work_type']?>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <i class="fa fa- fa-clock-o"></i> <?= $this->lang->line('posted on'); ?>: <?=date('D, d M y', strtotime($job['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-map-marker"></i>&nbsp;<?=$job['location_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-dot-circle-o"></i> <?=$this->lang->line('Proposals').": ".$job['proposal_counts']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button id="btn_add_<?=$job['job_id']?>" style=" height:5px;width:5px;padding: 0px;display:<?=(in_array($job['job_id'], $fav_projects)?"inline-block":"none")?>" title="<?=$this->lang->line('Remove from favorite')?>" class="btn-link remove_favorite_<?=$job['job_id']?>" data-id="<?=$job['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job['job_id']?>" class="fa fa-heart"></i></font></button>
                            <button id="btn_remove_<?=$job['job_id']?>" style="height:5px;width:5px;padding: 0px;display:<?=(in_array($job['job_id'], $fav_projects)?"none":"inline-block")?> " title="<?=$this->lang->line('Add to favorite')?>" class="btn-link add_favorite_<?=$job['job_id']?>" data-id="<?=$job['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job['job_id']?>" class="fa fa-heart-o"></i></font></button>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <?php if($job['work_type'] == "per hour" || $job['work_type'] == "Daily" || $job['work_type'] == "Weekly" || $job['work_type'] == "Monthly"){ ?>
                            <form action="<?=base_url('user-panel-services/send-proposal-view-per-hour')?>" method="post">
                            <?php }elseif($job['work_type'] == "fixed price"){ ?>
                            <form action="<?=base_url('user-panel-services/send-proposal-view')?>" method="post">
                            <?php } ?>
                              <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                              <input type="hidden" name="redirect_url" value="browse-project" />
                              <button type="submit" class="btn btn-sm btn-outline btn-success btn-block" id="btnWorkroom"><i class="fa fa-paper-plane"></i> <?= $this->lang->line('Send Proposal'); ?></button>
                            </form>
                            <script>
                              $('.add_favorite_<?=$job['job_id']?>').click(function () { 
                                var id = $(this).attr("data-id");
                                $.post('<?=base_url("user-panel-services/make-job-favourite")?>', {id: id}, 
                                function(res) { 
                                  console.log(res);
                                  if($.trim(res) == "success") { 
                                    swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Job added to your favorite list.')); ?>, "success");
                                    $("#btn_remove_<?=$job['job_id']?>").css("display", "none");
                                    $("#btn_add_<?=$job['job_id']?>").css("display", "inline-block");
                                  } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to add. Try again...')); ?>, "error");  } 
                                });
                              });
                              $('.remove_favorite_<?=$job['job_id']?>').click(function () { 
                                var id = $(this).attr("data-id");
                                $.post('<?=base_url("user-panel-services/job-remove-from-favorite")?>', {id: id}, 
                                function(res) { 
                                  //console.log(res);
                                  if($.trim(res) == "success") { 
                                    swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Job removed from favorite list.')); ?>, "success"); 
                                    $("#btn_remove_<?=$job['job_id']?>").css("display", "inline-block");
                                    $("#btn_add_<?=$job['job_id']?>").css("display", "none");
                                  } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to remove. Try again...')); ?>, "error");  } 
                                });
                              });
                            </script>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } else { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: 0px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          Job - <?=$job['job_title']?>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php } else { ?>
    <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: 0px; margin-top: 20px; margin-left: -7px; margin-right: -7px;">
      <div class="panel-body" style="border-radius: 10px; padding: 15px;">
        <h5><?= $this->lang->line('No jobs found.'); ?></h5>
      </div>
    </div>
  <?php } ?>
</div>

<script>
  $('#advanceBtn').click(function(){
    if($('#advanceSearch').css('display') == 'none') {
      $('#basicSearch').hide("slow");
      $('#advanceSearch').show("slow");
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Hide Filters'))?>);
      //$('#searchbox').hide("slow");
      //$('#searchlabel').hide("slow");
    } else {
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Show Filters'))?>);
      $('#advanceSearch').hide("slow");
      $('#basicSearch').show("slow");
      //$('#searchbox').show("slow");
      //$('#searchlabel').show("slow");
    }
    return false;
  });
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-sub-category", 
      data: { cat_type_id: cat_type_id },
      dataType: "json",
      success: function(res) {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('select_category')?></option>");
        $.each( res, function() {
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function() {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('loading')?></option>");
      },
      error: function() {
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('no_options')?></option>");
      }
    })
  });
</script>