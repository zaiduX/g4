<?php
  // $provider_id = $keys["provider_id"];
  // $cust_id = $keys["cust_id"];
  // $job_id = $keys["job_id"];
?> 
<style>
  #attachment, .attachment { 
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px; 
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0); 
  }
  .entry:not(:first-of-type) {
    margin-top: 10px;
  }

  .gonagoo_custom {
    margin-top: 8px;
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-family: auto;
  }


</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 15px;">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/my-jobs'); ?>"><?= $this->lang->line('Search Jobs'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Send Proposal'); ?></span></li>
        </ol>
      </div>
      <h4 class="font-light m-b-xs"><i class="fa fa-paper-plane"></i> <?= $this->lang->line('Send Proposal')?><font color="#3498db"><strong></strong></font></h4> 
    </div>
  </div>
</div>

<div class="content" style="padding-top: 50px;">
  <div class="hpanel hblue">
    <div class="panel-body" style="padding: 0px;">
      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display: block;">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h2 style="margin-top: 15px; margin-bottom: 5px;" class="font-uppercase"><i class="fa fa-tag"></i> Ref #GS-<?=$job_details['job_id']?>&nbsp;&nbsp;&nbsp;<?=$job_details['job_title']?></h2>
          <hr style="margin-bottom: 10px;margin-top: 10px;">
          <div class="row" style="padding-left: 0px;margin-bottom: 10px;">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;">
                <i class="fa fa- fa-clock-o"></i> <?= $this->lang->line('posted on'); ?>: <?=date('D, d M y', strtotime($job_details['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-map-marker"></i>&nbsp;<?=$job_details['location_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-dot-circle-o"></i> <?=$this->lang->line('Proposals')." ".$job_details['proposal_counts']?>&nbsp;&nbsp;&nbsp;&nbsp;
                <strong style="color: #3498db;"><i class="fa fa-gears"></i> <?= $this->lang->line('status'); ?>: <?= strtoupper(str_replace('_', ' ', $job_details['job_status']))?></strong>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <div class="hpanel horange" style="margin-bottom: 15px;">
            <div class="panel-body no-padding">
              <div class="row">
              </div>
            </div>
            <div class="panel-footer borders" style="padding:0px 0px 0px 0px">
              <div class="row form-group" style="margin-bottom: 2px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                  <?php $flag = 0;
                  for($i=0; $i<sizeof($proposals); $i++){ 
                   echo "<div class='col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1' style='padding-left: 5px;padding-right: 5px;width: 45px;'><img title='".$proposals[$i]['provider_details']['firstname']." ".$proposals[$i]['provider_details']['lastname']."' class='img-circle m-b-xs img-responsive' src='".base_url($proposals[$i]['provider_details']['avatar_url'])."'></div>";
                   if($i==4){$flag = 1;$count = sizeof($proposals) - 5;break;}else{$flag = 0;}
                  }?>
                  <?php if($flag==1){ ?>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-xs-7"><h5 style="margin-top: 5px;">+ <?=$count." ".$this->lang->line('others have already sent a proposal.')?> 
                      <button id="btn_add_<?=$job_details['job_id']?>" style=" height:5px;width:5px;padding: 0px;display:<?=(in_array($job_details['job_id'], $fav_projects)?"inline-block":"none")?>" title="<?=$this->lang->line('Remove from favorite')?>" class="btn-link remove_favorite_<?=$job_details['job_id']?>" data-id="<?=$job_details['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job_details['job_id']?>" class="fa fa-heart"></i></font></button>
                      <button id="btn_remove_<?=$job_details['job_id']?>" style="height:5px;width:5px;padding: 0px;display:<?=(in_array($job_details['job_id'], $fav_projects)?"none":"inline-block")?> " title="<?=$this->lang->line('Add to favorite')?>" class="btn-link add_favorite_<?=$job_details['job_id']?>" data-id="<?=$job_details['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job_details['job_id']?>" class="fa fa-heart-o"></i></font></button></h5>
                    </div>
                  <?php } else{ ?>
                      <?php  if(sizeof($proposals) == 1){ ?>
                        <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10"><h5 style="margin-top: 5px;"> <?=$this->lang->line('has already sent a proposal.')?>
                          <button id="btn_add_<?=$job_details['job_id']?>" style=" height:5px;width:5px;padding: 0px;display:<?=(in_array($job_details['job_id'], $fav_projects)?"inline-block":"none")?>" title="<?=$this->lang->line('Remove from favorite')?>" class="btn-link remove_favorite_<?=$job_details['job_id']?>" data-id="<?=$job_details['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job_details['job_id']?>" class="fa fa-heart"></i></font></button>
                          <button id="btn_remove_<?=$job_details['job_id']?>" style="height:5px;width:5px;padding: 0px;display:<?=(in_array($job_details['job_id'], $fav_projects)?"none":"inline-block")?> " title="<?=$this->lang->line('Add to favorite')?>" class="btn-link add_favorite_<?=$job_details['job_id']?>" data-id="<?=$job_details['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job_details['job_id']?>" class="fa fa-heart-o"></i></font></button></h5>
                        </div>
                      <?php }else{ ?>
                        <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-xs-7"><h5 style="margin-top: 5px;"> <?php echo $this->lang->line('have already sent a proposal.'); ?>
                          <button id="btn_add_<?=$job_details['job_id']?>" style=" height:5px;width:5px;padding: 0px;display:<?=(in_array($job_details['job_id'], $fav_projects)?"inline-block":"none")?>" title="<?=$this->lang->line('Remove from favorite')?>" class="btn-link remove_favorite_<?=$job_details['job_id']?>" data-id="<?=$job_details['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job_details['job_id']?>" class="fa fa-heart"></i></font></button>
                          <button id="btn_remove_<?=$job_details['job_id']?>" style="height:5px;width:5px;padding: 0px;display:<?=(in_array($job_details['job_id'], $fav_projects)?"none":"inline-block")?> " title="<?=$this->lang->line('Add to favorite')?>" class="btn-link add_favorite_<?=$job_details['job_id']?>" data-id="<?=$job_details['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job_details['job_id']?>" class="fa fa-heart-o"></i></font></button></h5>
                        </div>
                      <?php }?>
                  <?php } ?>
                </div>
                  
              </div>
            </div>
          </div>
          <h3><?=$this->lang->line('description')?></h3>
          <lable><?=$job_details['job_desc']?></lable><br/>
          <h6><strong><?=$this->lang->line('Attachments')?></strong></h6>
          <form action="<?= base_url('user-panel-services/send-proposal'); ?>" method="post" class="form-horizontal" id="provider_job_proposal_form" enctype="multipart/form-data">            
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; display: inline-flex;">
              
              <?php if($job_details["attachment_1"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 1"?>" target="_blank" href="<?=base_url($job_details["attachment_1"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_2"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 2"?>" target="_blank" href="<?=base_url($job_details["attachment_2"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_3"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 3"?>" target="_blank" href="<?=base_url($job_details["attachment_3"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_4"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 4"?>" target="_blank" href="<?=base_url($job_details["attachment_4"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_5"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 5"?>" target="_blank" href="<?=base_url($job_details["attachment_5"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_6"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 6"?>" target="_blank" href="<?=base_url($job_details["attachment_6"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
            </div>
            <br/>
            <h3><?=$this->lang->line('New Proposal')?></h3>
            <div class="hpanel horange" style="margin-bottom: 15px;">
              <div class="panel-footer borders" style="padding:15px 15px 0px 15px;margin-top: 10px;">
                <div class="row form-group">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                      <label class=""><?= strtoupper($this->lang->line('desc')); ?> </label>
                      <textarea style="resize:none"  rows="5" id="description" name="description" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Provide a more detailed description to help you get better proposal'); ?>" autocomplete="none"></textarea> <br/>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                      <div class="control-group" id="upload_sample">
                        <div class="controls">
                          <div class="entry input-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4"style="width:unset;">
                            <input class="btn btn-primary image_url" name="image_url[]" type="file" accept="image/png,image/gif,image/jpeg" style="font-size: 12px;">
                            <span class="input-group-btn">
                              <button class="btn btn-success btn-add" type="button" style="margin-left: 2px !important;">
                                <span class="fa fa-plus"></span>
                              </button>
                              <input type="hidden" id="count" value="2">
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                      &nbsp;
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                      <h3><?=$this->lang->line('Interview Questions')?></h3>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                      <?php if($job_details["question_1"] != ""){ ?>
                        <label class=""><?=$job_details["question_1"]?> </label>
                        <textarea style="resize:none"  rows="2" id="answer_1" name="answer_1" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Answer'); ?>" autocomplete="none"></textarea><br/>
                      <?php } ?>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                      <?php if($job_details["question_2"] != ""){ ?>
                        <label class=""><?=$job_details["question_2"]?> </label>
                        <textarea style="resize:none"  rows="2" id="answer_2" name="answer_2" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Answer'); ?>" autocomplete="none"></textarea><br/>
                      <?php } ?>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                      <?php if($job_details["question_3"] != ""){ ?>
                        <label class=""><?=$job_details["question_3"]?> </label>
                        <textarea style="resize:none"  rows="2" id="answer_3" name="answer_3" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Answer'); ?>" autocomplete="none"></textarea><br/>
                      <?php } ?>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                      <?php if($job_details["question_4"] != ""){ ?>
                        <label class=""><?=$job_details["question_4"]?> </label>
                        <textarea style="resize:none"  rows="2" id="answer_4" name="answer_4" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Answer'); ?>" autocomplete="none"></textarea><br/>
                      <?php } ?>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                      <?php if($job_details["question_5"] != ""){ ?>
                        <label class=""><?=$job_details["question_5"]?> </label>
                        <textarea style="resize:none"  rows="2" id="answer_5" name="answer_5" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Answer'); ?>" autocomplete="none"></textarea><br/>
                      <?php } ?>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px">
                      <h3><?=$this->lang->line('Proposal Breakdown')?></h3>
                    </div> 
                    <div id="q1">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part1" name="part1" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q1" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a> -->  
                      <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price1" name="price1" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <!-- <button class="btn btn-danger btn-delete2" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button> -->
                      </div>
                    </div>
                    <div id="q2" style="display:none;">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part2" name="part2" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q2" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a> -->  
                        <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price2" name="price2" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <button class="btn btn-danger btn-delete2" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button>
                      </div>
                    </div>
                    <div id="q3" style="display:none;">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part3" name="part3" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q3" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a> -->  
                        <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price3" name="price3" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <button class="btn btn-danger btn-delete3" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button>
                      </div>
                    </div>
                    <div id="q4" style="display:none;">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part4" name="part4" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q4" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a> -->  
                        <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price4" name="price4" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <button class="btn btn-danger btn-delete4" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button>
                      </div>
                    </div>
                    <div id="q5" style="display:none;">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part5" name="part5" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q5" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a> -->  
                        <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price5" name="price5" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <button class="btn btn-danger btn-delete5" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button>
                      </div>
                    </div>
                    <div id="q6" style="display:none;">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part6" name="part6" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q6" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a> -->  
                        <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price6" name="price6" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <button class="btn btn-danger btn-delete6" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button>
                      </div>
                    </div>
                    <div id="q7" style="display:none;">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part7" name="part7" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q7" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a>  
                        <br/> -->
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price7" name="price7" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <button class="btn btn-danger btn-delete7" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button>
                      </div>
                    </div>
                    <div id="q8" style="display:none;">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part8" name="part8" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q8" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a> -->  
                        <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price8" name="price8" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <button class="btn btn-danger btn-delete8" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button>
                      </div>
                    </div>
                    <div id="q9" style="display:none;">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part9" name="part9" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q9" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a> -->  
                        <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price9" name="price9" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <button class="btn btn-danger btn-delete9" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button>
                      </div>
                    </div>
                    <div id="q10" style="display:none;">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label><?=$this->lang->line('item')?></label>
                        <input type="text" placeholder="<?= $this->lang->line('please specify'); ?>" class="form-control" id="part10" name="part10" autocomplete="none">
                        <!-- <a class="btn btn-link btn-add-q10" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a>   -->
                        <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                        <label><?=$this->lang->line('amount')?></label>
                        <div class="input-group">
                          <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                          <input type="number" class="form-control" id="price10" name="price10" onkeypress="return event.keyCode != 13;"  min="0"> 
                        </div>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                        <label>&nbsp;</label>
                        <button class="btn btn-danger btn-delete10" type="button" style="height: 32px;padding-top: 5px;"><span class="fa fa-trash-o"></span>
                        </button>
                      </div>
                    </div>
                    <div id="">
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <a class="btn btn-link btn-add" style="padding-left:0px;color: #225595"><i class="fa fa-plus"></i> <?=$this->lang->line("Add new item")?></a>
                        <br/>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px">
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px">
                      </div>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 text-right">
                      <h5 style="margin-top: 10px;"><?=$this->lang->line('total')?></h5>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px;padding-bottom:15px;">
                      <div class="input-group">
                        <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                        <input type="number" class="form-control" id="total" disabled min="0"> 
                        <input type="hidden" id="h_total" name="total"> 
                        <input type="hidden" value="<?=$job_details['job_id']?>" name="job_id"> 
                        <input type="hidden" value="<?=(int)$payment_percent['country_id']?>" name="country_id"> 
                      </div>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 text-right">
                      <h5 style="margin-top: 10px;"><?=$this->lang->line("You'll earn")?></h5>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px;padding-bottom:15px;">
                      <div class="input-group">
                        <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                        <input type="number" class="form-control" id="earn" disabled min="0"> 
                        <input type="hidden" class="form-control" id="h_earn" name="earn" > 
                      </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                      <label class="gonagoo_custom">
                        <input type="checkbox" class="i-checks" name="notif_me" id="notif_me"  style="position: absolute; opacity: 0;">
                        &nbsp; <?=$this->lang->line('Notify me if the project is awarded to someone else')?>
                      </label>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-right">
                      <h5 style="margin-top: 10px;"><?=$this->lang->line("Deposit")?></h5>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px;padding-bottom:15px;">
                      <div class="input-group">
                        <span class="input-group-addon"> <?=$job_details['currency_code']?> </span>
                        <input type="number" class="form-control" id="deposit" name="deposit" min="0"> 
                        <input type="hidden" class="form-control" id="min_deposit"> 
                      </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9" style="padding-bottom: 15px;">
                      <?=$this->lang->line('You have')." ".$freelancers_profile['proposal_credits']." <strong>".$this->lang->line('Proposal credits')."</strong> ".$this->lang->line('available but you can always')." <a style='text-decoration: underline;color:#5dcdf7;' href='www.google.com'>".$this->lang->line('Buy More')."</a>"?>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                      <button class="btn btn-info btn-outline" type="submit" id="btn_send" style="width: 100%;"><?=$this->lang->line('Send Proposal')?></button>
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 panel-footer borders" style="display: block;padding-left: 0px; padding-right: 15px; height:100%">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right:0px;padding-left:0px">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
              <?=$this->lang->line('Ending in days')?> <br/>
              <h4><strong>
                <?=$ending?>
              </strong></h4>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
              <?=$job_details['work_type']?><br/>
              <h4><strong>
                <?=($job_details['work_type']!='Per Hour')?$job_details['currency_code'].' '.$job_details['budget']:$job_details['currency_code'].' '.$job_details['budget']."<small>/hr</small>"?>
              </strong></h4>
            </div>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:25px;">
            <button class="btn btn-info" id="btn_send_1" style="width: 100%;"><?=$this->lang->line('Send Proposal')?></button>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0;padding-right: 0px;padding-top: 20px;padding-bottom: 20px;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($job_customer_profile['avatar_url'])?>">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
              <h4 style="color: #3498db"><strong><?=$job_customer_profile['firstname'].' '.$job_customer_profile['lastname']?></strong></h4>
              <h5><i class="fa fa-map-marker"></i> <?=$this->api->get_country_name_by_id($job_customer_profile['country_id'])?></h5>
              <h5 style="color:#3ceb3f">
                <?php
                    $to_time = strtotime($job_customer_profile['last_login_datetime']);
                    date_default_timezone_set($_SESSION['default_timezone']);
                    $from_time = strtotime(date('Y-m-d h:i:s'));
                  $min = round(((abs($to_time - $from_time) / 60) - 270),2);
                  if( $min > 10 ) { echo '<span class="text-danger"><i class="fa fa-circle"></i> '.$this->lang->line('Offline').'</span>'; }
                  else { echo '<span class="text-success" style="color: #3ceb3f;"><i class="fa fa-circle"></i> '.$this->lang->line('Online').'</span>'; }
                ?>
              </h5>
            </div>
          </div><br/>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-body" style="padding-left: 0px;padding-top: 0px;padding-bottom: 0px;padding-right: 0px;">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="height: 75px;padding-left: 5px;padding-right: 5px;">
              <?=$this->lang->line('Project completed')?><br/><br/>
              <h5><strong><?=$completed_job?></strong></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="height: 75px;padding-left: 5px;padding-right: 5px;">
              <?=$this->lang->line('Freelancers worked with')?><br/><br/>
              <h5><strong>-</strong></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="height: 75px;padding-left: 5px;padding-right: 5px;">
              <?=$this->lang->line('Project Awarded')?><br/><br/>
              <h5><strong>0%</strong></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="height: 75px;padding-left: 5px;padding-right: 5px;">
              <?=$this->lang->line('Last project')?><br/><br/>
              <h5><strong>
                <?php
                  $yrdata= strtotime($last_job['cre_datetime']);
                  echo date('d M Y', $yrdata);
                ?>
              </strong></h5>
            </div> 
          </div>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 15px 0px;">
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <div class="row">
        <div class="col-md-12">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <a href="<?= $_SERVER['HTTP_REFERER'] ?>" class="btn btn-info btn-outline"><?= $this->lang->line('back'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(function() {
    $("#send_option").on('change', function(event) {  event.preventDefault();
      var option = $(this).val();
      if(option =="rating") { $("#rating_div").removeClass('hidden'); $("#chat_div").addClass('hidden'); $(".chat_div").addClass('hidden'); }
      else { $("#chat_div").removeClass('hidden'); $(".chat_div").removeClass('hidden'); $("#rating_div").addClass('hidden'); }
    });
    $("#ratingForm").submit(function(e) { e.preventDefault();
      var form = $(this).get(0);
      var rating = $("#rating").val();
      if(rating == 0 ) {
        $("#error_rating").removeClass('hidden').html('Give some rating.');
      } else { form.submit(); }
    });
  });
</script>
<script>
  $('.add_favorite_<?=$job_details['job_id']?>').click(function () { 
    var id = $(this).attr("data-id");
    $.post('<?=base_url("user-panel-services/make-job-favourite")?>', {id: id}, 
    function(res) { 
      console.log(res);
      if($.trim(res) == "success") { 
        swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Job added to your favorite list.')); ?>, "success");
        $("#btn_remove_<?=$job_details['job_id']?>").css("display", "none");
        $("#btn_add_<?=$job_details['job_id']?>").css("display", "inline-block");
      } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to add. Try again...')); ?>, "error");  } 
    });
  });
  $('.remove_favorite_<?=$job_details['job_id']?>').click(function () { 
    var id = $(this).attr("data-id");
    $.post('<?=base_url("user-panel-services/job-remove-from-favorite")?>', {id: id}, 
    function(res) { 
      //console.log(res);
      if($.trim(res) == "success") { 
        swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Job removed from favorite list.')); ?>, "success"); 
        $("#btn_remove_<?=$job_details['job_id']?>").css("display", "inline-block");
        $("#btn_add_<?=$job_details['job_id']?>").css("display", "none");
      } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to remove. Try again...')); ?>, "error");  } 
    });
  });
</script>
<script>
  $(function() {
    $(document).on('click', '.btn-add', function(e) {
      e.preventDefault();
      var numItems = $('.entry').length;
      if(numItems <= 4) {
        var controlForm = $('.controls:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
        .removeClass('btn-add').addClass('btn-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="fa fa-minus"></span>');
      } else { swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('You can upload upto 5 files only.')?>","warning"); }
      }).on('click', '.btn-remove', function(e) {
        $(this).parents('.entry:first').remove();
      e.preventDefault();
      return false;
    });
  });
</script>
<script>
  $(".btn-add-q1").click(function(){
    $('#q2').css('display', 'block');
    $('.btn-add-q1').hide();
    //$('.btn-delete1').hide();
  });

  $(".btn-delete2").click(function(){
    $('#q2').css('display', 'none');
    $('.btn-add-q1').show();
    $('.btn-delete1').show();
    $('#price2').val("");
    $('#part2').val("");
    calculat_total();
    var count = $('#count').val();
    count = parseInt(count)-1;
    $('#count').val(count);
  });

  $(".btn-add-q2").click(function(){
    $('#q3').css('display', 'block');
    $('.btn-add-q2').hide();
    //$('.btn-delete2').hide();
  });

  $(".btn-delete3").click(function(){
    $('#q3').css('display', 'none');
    $('.btn-add-q2').show();
    $('.btn-delete2').show();
    $('#price3').val("");
    $('#part3').val("");
    calculat_total();
    var count = $('#count').val();
    count = parseInt(count)-1;
    $('#count').val(count);
  });

  $(".btn-add-q3").click(function(){
    $('#q4').css('display', 'block');
    $('.btn-add-q3').hide();
    //$('.btn-delete3').hide();
  });

  $(".btn-delete4").click(function(){
    $('#q4').css('display', 'none');
    $('.btn-add-q3').show();
    $('.btn-delete3').show();
    $('#price4').val("");
    $('#part4').val("");
    calculat_total();
    var count = $('#count').val();
    count = parseInt(count)-1;
    $('#count').val(count);
  });

  $(".btn-add-q4").click(function(){
    $('#q5').css('display', 'block');
    $('.btn-add-q4').hide();
    //$('.btn-delete4').hide();
  });

  $(".btn-delete5").click(function(){
    $('#q5').css('display', 'none');
    $('.btn-add-q4').show();
    $('.btn-delete4').show();
    $('#price5').val("");
    $('#part5').val("");
    calculat_total();
    var count = $('#count').val();
    count = parseInt(count)-1;
    $('#count').val(count);
  });

  $(".btn-add-q5").click(function(){
    $('#q6').css('display', 'block');
    $('.btn-add-q5').hide();
    //$('.btn-delete5').hide();
  });

  $(".btn-delete6").click(function(){
    $('#q6').css('display', 'none');
    $('.btn-add-q5').show();
    $('.btn-delete5').show();
    $('#price6').val("");
    $('#part6').val("");
    calculat_total();
    var count = $('#count').val();
    count = parseInt(count)-1;
    $('#count').val(count);
  });

  $(".btn-add-q6").click(function(){
    $('#q7').css('display', 'block');
    $('.btn-add-q6').hide();
    //$('.btn-delete6').hide();

  });

  $(".btn-delete7").click(function(){
    $('#q7').css('display', 'none');
    $('.btn-add-q6').show();
    $('.btn-delete6').show();
    $('#price7').val("");
    $('#part7').val("");
    calculat_total();
    var count = $('#count').val();
    count = parseInt(count)-1;
    $('#count').val(count);
  });

  $(".btn-add-q7").click(function(){
    $('#q8').css('display', 'block');
    $('.btn-add-q7').hide();
    //$('.btn-delete7').hide();
  });

  $(".btn-delete8").click(function(){
    $('#q8').css('display', 'none');
    $('.btn-add-q7').show();
    $('.btn-delete7').show();
    $('#price8').val("");
    $('#part8').val("");
    calculat_total();
    var count = $('#count').val();
    count = parseInt(count)-1;
    $('#count').val(count);
  });

  $(".btn-add-q8").click(function(){
    $('#q9').css('display', 'block');
    $('.btn-add-q8').hide();
    //$('.btn-delete8').hide();
    
  });

  $(".btn-delete9").click(function(){
    $('#q9').css('display', 'none');
    $('.btn-add-q8').show();
    $('.btn-delete8').show();
    $('#price9').val("");
    $('#part9').val("");
    calculat_total();
    var count = $('#count').val();
    count = parseInt(count)-1;
    $('#count').val(count);
  });

  $(".btn-add-q9").click(function(){
    $('#q10').css('display', 'block');
    $('.btn-add-q9').hide();
    //$('.btn-delete9').hide();
  });

  $(".btn-delete10").click(function(){
    $('#q10').css('display', 'none');
    $('.btn-add-q9').show();
    $('.btn-delete9').show();
    $('#price10').val("");
    $('#part10').val("");
    calculat_total();
    var count = $('#count').val();
    count = parseInt(count)-1;
    $('#count').val(count);
  });
  

  $(".btn-add").click(function(){
    var count = $('#count').val();
    $('#q'+count).css('display', 'block');
    count = parseInt(count)+1;
    $('#count').val(count);
  });
  
  $("#price1,#price2,#price3,#price4,#price5,#price6,#price7,#price8,#price9,#price10").on('change', function(event) {  event.preventDefault();
    var percent = "<?=(int)$payment_percent['gonagoo_commission']?>";
    var advance_percent = "<?=(int)$payment_percent['advance_payment']?>";
    if($("#price1").val() == ""){
      var price1 = 0;
    }else{
      var price1 =  parseInt($("#price1").val());
    }
    if($("#price2").val() == ""){
      var price2 = 0;
    }else{
      var price2 =  parseInt($("#price2").val());
    }
    if($("#price3").val() == ""){
      var price3 = 0;
    }else{
      var price3 =  parseInt($("#price3").val());
    }
    if($("#price4").val() == ""){
      var price4 = 0;
    }else{
      var price4 =  parseInt($("#price4").val());
    }
    if($("#price5").val() == ""){
      var price5 = 0;
    }else{
      var price5 =  parseInt($("#price5").val());
    }
    if($("#price6").val() == ""){
      var price6 = 0;
    }else{
      var price6 =  parseInt($("#price6").val());
    }
    if($("#price7").val() == ""){
      var price7 = 0;
    }else{
      var price7 =  parseInt($("#price7").val());
    }
    if($("#price8").val() == ""){
      var price8 = 0;
    }else{
      var price8 =  parseInt($("#price8").val());
    }
    if($("#price9").val() == ""){
      var price9 = 0;
    }else{
      var price9 =  parseInt($("#price9").val());
    }
    if($("#price10").val() == ""){
      var price10 = 0;
    }else{
      var price10 =  parseInt($("#price10").val());
    }
    
    var total = price1+price2+price3+price4+price5+price6+price7+price8+price9+price10;
    var you_earn = total-((total/100)*percent);
    var desposit_money =  (total/100)*advance_percent;
    $("#deposit").val(desposit_money);
    $("#min_deposit").val(desposit_money);
    $("#total").val(total);
    $("#earn").val(you_earn);
    $("#h_total").val(total);
    $("#h_earn").val(you_earn);
    // $("#deposit").attr("max",total);
  });

  
  function calculat_total() {
    var percent = "<?=(int)$payment_percent['gonagoo_commission']?>";
    var advance_percent = "<?=(int)$payment_percent['advance_payment']?>";
    if($("#price1").val() == ""){
      var price1 = 0;
    }else{
      var price1 =  parseInt($("#price1").val());
    }
    if($("#price2").val() == ""){
      var price2 = 0;
    }else{
      var price2 =  parseInt($("#price2").val());
    }
    if($("#price3").val() == ""){
      var price3 = 0;
    }else{
      var price3 =  parseInt($("#price3").val());
    }
    if($("#price4").val() == ""){
      var price4 = 0;
    }else{
      var price4 =  parseInt($("#price4").val());
    }
    if($("#price5").val() == ""){
      var price5 = 0;
    }else{
      var price5 =  parseInt($("#price5").val());
    }
    if($("#price6").val() == ""){
      var price6 = 0;
    }else{
      var price6 =  parseInt($("#price6").val());
    }
    if($("#price7").val() == ""){
      var price7 = 0;
    }else{
      var price7 =  parseInt($("#price7").val());
    }
    if($("#price8").val() == ""){
      var price8 = 0;
    }else{
      var price8 =  parseInt($("#price8").val());
    }
    if($("#price9").val() == ""){
      var price9 = 0;
    }else{
      var price9 =  parseInt($("#price9").val());
    }
    if($("#price10").val() == ""){
      var price10 = 0;
    }else{
      var price10 =  parseInt($("#price10").val());
    }
    var total = price1+price2+price3+price4+price5+price6+price7+price8+price9+price10;
    var you_earn = total-((total/100)*percent);
    var desposit_money =  (total/100)*advance_percent;
    $("#deposit").val(desposit_money);
    $("#min_deposit").val(desposit_money);
    $("#total").val(total);
    $("#h_total").val(total);
    $("#earn").val(you_earn);
    $("#h_earn").val(you_earn);
    // $("#deposit").attr("max",total);
  }

  $("#btn_send").on('click', function(event) { event.preventDefault();
    // var notify_me = $("#notif_me").is(":checked");
    var min_deposit = parseInt($("#min_deposit").val());
    var description = $("#description").val();
    var deposit = parseInt($("#deposit").val());
    var total = parseInt($("#total").val());
    var percent = "<?=(int)$payment_percent['gonagoo_commission']?>";
    var budget = "<?=$job_details['budget']?>";
   
    var minimum = ((budget/100)*percent);

    var part1 = $("#part1").val();
    var price1 = $("#price1").val();

    var part2 = $("#part2").val();
    var price2 = $("#price2").val();

    var part3 = $("#part3").val();
    var price3 = $("#price3").val();

    var part4 = $("#part4").val();
    var price4 = $("#price4").val();

    var part5 = $("#part5").val();
    var price5 = $("#price5").val();

    var part6 = $("#part6").val();
    var price6 = $("#price6").val();

    var part7 = $("#part7").val();
    var price7 = $("#price7").val();

    var part8 = $("#part8").val();
    var price8 = $("#price8").val();

    var part9 = $("#part9").val();
    var price9 = $("#price9").val();

    var part10 = $("#part10").val();
    var price10 = $("#price10").val();

    if(description == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal"); ?>', 'warning', false, "#DD6B55");}
    else if(part1 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part1').css('background-color', '#5dcdf7');}
    else if(price1 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price1').css('background-color', '#5dcdf7');}
    
    else if(part2 == "" && price2 != ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part2').css('background-color', '#5dcdf7');}
    else if(part2 != "" && price2 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price2').css('background-color', '#5dcdf7');}
    
    else if(part3 == "" && price3 != ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part3').css('background-color', '#5dcdf7');}
    else if(part3 != "" && price3 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price3').css('background-color', '#5dcdf7');}
    
    else if(part4 == "" && price4 != ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part4').css('background-color', '#5dcdf7');}
    else if(part4 != "" && price4 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price4').css('background-color', '#5dcdf7');}

    else if(part5 == "" && price5 != ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part5').css('background-color', '#5dcdf7');}
    else if(part5 != "" && price5 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price5').css('background-color', '#5dcdf7');}

    else if(part6 == "" && price6 != ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part6').css('background-color', '#5dcdf7');}
    else if(part6 != "" && price6 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price6').css('background-color', '#5dcdf7');}

    else if(part7 == "" && price7 != ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part7').css('background-color', '#5dcdf7');}
    else if(part7 != "" && price7 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price7').css('background-color', '#5dcdf7');}

    else if(part8 == "" && price8 != ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part8').css('background-color', '#5dcdf7');}
    else if(part8 != "" && price8 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price8').css('background-color', '#5dcdf7');}

    else if(part9 == "" && price9 != ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part9').css('background-color', '#5dcdf7');}
    else if(part9 != "" && price9 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price9').css('background-color', '#5dcdf7');}

    else if(part10 == "" && price10 != ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Description of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#part10').css('background-color', '#5dcdf7');}
    else if(part10 != "" && price10 == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter price of proposal breakdown part."); ?>', 'warning', false, "#DD6B55");
      $('#price10').css('background-color', '#5dcdf7');}
    else if(deposit == "" ){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Deposit Amount"); ?>', 'warning', false, "#DD6B55");
      $('#deposit').css('background-color', '#5dcdf7');}
    else if(deposit > total){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Deposit Amount Less than or equal to total Amount"); ?>', 'warning', false, "#DD6B55");
      $('#deposit').css('background-color', '#5dcdf7');}
    else if(deposit < min_deposit){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Minimum deposit is"); ?>'+" "+min_deposit, 'warning', false, "#DD6B55");
      $('#deposit').css('background-color', '#5dcdf7');}
    else if(minimum > total){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Minimum proposal amount should be greater than");?>'+" "+minimum, 'warning', false, "#DD6B55");}
    else {
      //alert(minimum);
      $("#provider_job_proposal_form").submit();
    }
  });
  
  $("#btn_send_1").on('click', function(event) { event.preventDefault();
    $("#btn_send").click();
  });
  $("#part1").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#part2").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#part3").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#part4").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#part5").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#part6").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#part7").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#part8").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#part9").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#part10").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price1").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price2").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price3").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price4").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price5").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price6").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price7").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price8").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price9").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#price10").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  $("#deposit").keypress(function() {
    $(this).css('background-color', 'rgb(255, 255, 255)');
  });
  
</script>


<script>
  var btn_counter = 2; 
  $(function(){
    var wrapper = $(".wrapperField");
    var add_button = $("#btn_add_point");

    $(add_button).click(function(e) { 
      e.preventDefault();
      $("#no_of_points").val(btn_counter);

      $(wrapper).append();
      btn_counter++;
    });

    $(wrapper).on("click",".remove_field", function(e) { e.preventDefault(); 
      $(this).parent().parent().remove(); 
      var pointCounter = $("#no_of_points").val();
      pointCounter -= 1;
      $("#no_of_points").val(pointCounter);
      if(btn_counter > 1 ) { } else { btn_counter = 0; } 
    });
  }); 
</script>
