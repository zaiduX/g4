<style>
  #attachment, .attachment { 
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px; 
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0); 
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/provider-profile'); ?>"><span><?= $this->lang->line('Provider Profile'); ?></span></a></li>
          <li class="active"><span><?= $this->lang->line('edit'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-wrench fa-2x text-muted"></i> <?= $this->lang->line('Provider Profile'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Edit your profile settings'); ?></small>    
    </div>
  </div>
</div>

<div class="content content-boxed" style="max-width: 1200px !important;">
  <div class="row">
    <div class="hpanel hblue">
      <div class="panel-body">

        <div class="panel panel-primary" style="margin-bottom: 10px !important;">
          <div class="panel-heading" style="height: 48px;">
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10"><h5 style="margin-top: 5px;"><i class="fa fa-clock-o"></i> <?= $this->lang->line('Working Days'); ?> - [ <?= $operator['working_days'] != "NULL" ? $operator['working_days'] : $this->lang->line('not_provided') ?> ]</h5></div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;"><a class="btn btn-sm btn-default" id="btn_working_days"><i class="fa fa-pencil"></i> <?= $this->lang->line('edit'); ?></a>
            </div>
          </div>
          <div style="border-top: 0px; padding-top: 0px;" class="panel-body <?=($panel_active==1)?'':'hidden'?>" id="panel_working_days">
            <div class="row">
              <form action="<?=base_url('user-panel-services/update-working-days')?>" method="POST" id="frm_working_days">
                <input type="hidden" name="cust_id" value="<?=$operator['cust_id']?>">
                <input type="hidden" name="panel_active" value="1">
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <h4><?= $this->lang->line('Select your working days'); ?></h4>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <label> <input type="checkbox" class="i-checks" name="working_days[]" value="Sunday" <?=(in_array('Sunday', explode(', ', $operator['working_days'])))?'checked':''?> >&nbsp;&nbsp;&nbsp;<?= $this->lang->line('sunday'); ?> </label><br />
                  <label> <input type="checkbox" class="i-checks" name="working_days[]" value="Monday" <?=(in_array('Monday', explode(', ', $operator['working_days'])))?'checked':''?> >&nbsp;&nbsp;&nbsp;<?= $this->lang->line('monday'); ?> </label><br />
                  <label> <input type="checkbox" class="i-checks" name="working_days[]" value="Tuesday" <?=(in_array('Tuesday', explode(', ', $operator['working_days'])))?'checked':''?> >&nbsp;&nbsp;&nbsp;<?= $this->lang->line('tuesday'); ?> </label><br />
                  <label> <input type="checkbox" class="i-checks" name="working_days[]" value="Wednesday" <?=(in_array('Wednesday', explode(', ', $operator['working_days'])))?'checked':''?> >&nbsp;&nbsp;&nbsp;<?= $this->lang->line('wednesday'); ?> </label><br />
                  <label> <input type="checkbox" class="i-checks" name="working_days[]" value="Thursday" <?=(in_array('Thursday', explode(', ', $operator['working_days'])))?'checked':''?> >&nbsp;&nbsp;&nbsp;<?= $this->lang->line('thursday'); ?> </label><br />
                  <label> <input type="checkbox" class="i-checks" name="working_days[]" value="Friday" <?=(in_array('Friday', explode(', ', $operator['working_days'])))?'checked':''?> >&nbsp;&nbsp;&nbsp;<?= $this->lang->line('friday'); ?> </label><br />
                  <label> <input type="checkbox" class="i-checks" name="working_days[]" value="Saturday" <?=(in_array('Saturday', explode(', ', $operator['working_days'])))?'checked':''?> >&nbsp;&nbsp;&nbsp;<?= $this->lang->line('saturday'); ?> </label>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <p style="color: gray">* <?= $this->lang->line('We strongly recommend to set your working days as late or non delivery could severely affect your rankings and your chances to win more work'); ?></p>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="display: inline-flex;">
                  <button class="btn btn-info btn-outline" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-floppy-o"></i> <?=$this->lang->line('save')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
                  <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('error'); ?></div>
                  <?php endif; ?>
                  <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('success'); ?></div>
                  <?php endif; ?>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="panel panel-success" style="margin-bottom: 10px !important;">
          <div class="panel-heading" style="height: 48px;">
            <?php if(!empty($off_days)){ ?>
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10"><h5 style="margin-top: 5px;"><i class="fa fa-calendar-o"></i> <?= $this->lang->line('Off Days'); ?> - [ <?php if($operator['off_days'] != 0) { echo $off_days[0]['from_date'].' - '.$off_days[0]['to_date']; } else { echo $this->lang->line('not_provided'); } ?> ]</h5></div>
            <?php }else{ ?>
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10"><h5 style="margin-top: 5px;"><i class="fa fa-calendar-o"></i> <?= $this->lang->line('Off Days'); ?> - [ <?php echo $this->lang->line('not_provided');  ?> ]</h5></div>
            <?php } ?>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;"><a class="btn btn-sm btn-default" id="btn_off_days"><i class="fa fa-pencil"></i> <?= $this->lang->line('edit'); ?></a></div>
          </div>
          <div style="border-top: 0px; padding-top: 0px;" class="panel-body <?=($panel_active==2)?'':'hidden'?>" id="panel_off_days">
            <div class="row">
              <form action="<?=base_url('user-panel-services/update-off-days')?>" method="POST" id="frm_off_days">
                <input type="hidden" name="cust_id" value="<?=$operator['cust_id']?>">
                <input type="hidden" name="panel_active" value="2">
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <h4><?= $this->lang->line('Select off days'); ?></h4>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <div class="input-daterange input-group" id="datepicker" data-date-start-date="0d" data-date-format="yyyy/mm/dd">
                    <input type="text" class="form-control" name="from_date" autocomplete="off" />
                    <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                    <input type="text" class="form-control" name="to_date" autocomplete="off" />
                  </div>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <p style="color: gray">* <?= $this->lang->line('We strongly recommend to set your off days as late or non delivery could severely affect your rankings and your chances to win more work'); ?></p>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="display: inline-flex;">
                  <button class="btn btn-info btn-outline" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-floppy-o"></i> <?=$this->lang->line('save')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
                  <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('error'); ?></div>
                  <?php endif; ?>
                  <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('success'); ?></div>
                  <?php endif; ?>
                </div>
              </form>

              <script>
                $("#frm_off_days").validate({
                  rules: {
                    from_date: "required",
                    to_date: "required",
                  },
                  messages: {
                    from_date: "<?=$this->lang->line('Enter from date')?>",
                    to_date: "<?=$this->lang->line('Enter to date')?>",
                  }
                });
              </script>
              
              <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="margin-top: 15px;">
                <h4><?= $this->lang->line('Your Off Days'); ?></h4>
              </div>
              <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <table class="table">
                  <thead>
                    <th><?= $this->lang->line('From'); ?></th>
                    <th><?= $this->lang->line('To'); ?></th>
                    <th class="text-center"><?= $this->lang->line('action'); ?></th>
                  </thead>
                  <tbody>
                    <?php if($operator['off_days'] != 0) {
                      foreach ($off_days as $value) { ?>
                        <tr>
                          <td style="padding-top: 13px;"><?=$value['from_date']?></td>
                          <td style="padding-top: 13px;"><?=$value['to_date']?></td>
                          <td class="text-center">
                            <form action="<?=base_url('user-panel-services/delete-off-days')?>" method="POST" id="frm_off_days">
                              <input type="hidden" name="cust_id" value="<?=$operator['cust_id']?>">
                              <input type="hidden" name="off_id" value="<?=$value['off_id']?>">
                              <input type="hidden" name="panel_active" value="2">
                              <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                            </td>
                        </tr>                      
                    <?php  }
                    } else { echo '<td colspan="3">'.$this->lang->line('not_provided').'</td>'; } ?> 
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-info" style="margin-bottom: 10px !important;">
          <div class="panel-heading" style="height: 48px;">
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10"><h5 style="margin-top: 5px;"><i class="fa fa-envelope-open-o"></i> <?= $this->lang->line('Max. Open Projects'); ?> - [ <?=$operator['max_open_order']?> ]</h5></div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;"><a class="btn btn-sm btn-default" id="btn_max_pro"><i class="fa fa-pencil"></i> <?= $this->lang->line('edit'); ?></a></div>
          </div>
          <div style="border-top: 0px; padding-top: 0px;" class="panel-body <?=($panel_active==3)?'':'hidden'?>" id="panel_max_pro">
            <div class="row">
              <form action="<?=base_url('user-panel-services/update-max-open-order')?>" method="POST" id="frm_max_pro">
                <input type="hidden" name="cust_id" value="<?=$operator['cust_id']?>">
                <input type="hidden" name="panel_active" value="3">
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <h4><?= $this->lang->line('How many simultaneous Offer/orders can you handle?'); ?></h4>
                </div>
                <div class="col-xl-2 col-xl-offset-2 col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-6 col-xs-6">
                  <input type="number" class="form-control" name="max_open_order" id="max_open_order" value="<?=$operator['max_open_order']?>" min="1" />
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left" style="display: inline-flex;">
                  <button class="btn btn-info btn-outline" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-floppy-o"></i> <?=$this->lang->line('save')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;
                  <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('error'); ?></div>
                  <?php endif; ?>
                  <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('success'); ?></div>
                  <?php endif; ?>
                </div>
              </form>
              <script>
                $("#frm_max_pro").validate({
                  rules: {
                    max_open_order: {
                      required: true,
                      number: true,
                      min: 1,
                    },
                  },
                  messages: {
                    max_open_order: { 
                      required: "<?=$this->lang->line('Enter maximum number of offer/orders!')?>",
                      number: "<?=$this->lang->line('numbers_only')?>",
                      min: "<?=$this->lang->line('Minimum 1 required.')?>",
                    },
                  }
                });
              </script>
              <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <p style="color: gray">* <?= $this->lang->line('We strongly recommend to set it to something you can really handle as late or non delivery could severely affect your rankings and your chances to win more work'); ?></p>
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-warning" style="margin-bottom: 10px !important;">
          <div class="panel-heading" style="height: 48px;">
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10"><h5 style="margin-top: 5px;"><i class="fa fa-handshake-o"></i> <?= $this->lang->line('Proposal Credits'); ?> - [ <?=( strtotime($operator['current_subscription_expiry']) < strtotime(date('Y-m-d')) )?0:$operator['proposal_credits']?> - <?= $this->lang->line('expiry_date'); ?> <?=$operator['current_subscription_expiry']?> ]</h5></div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;"><a class="btn btn-sm btn-default" id="btn_proposal"><i class="fa fa-pencil"></i> <?= $this->lang->line('edit'); ?></a></div>
          </div>
          <div style="border-top: 0px; padding-top: 0px;" class="panel-body <?=($panel_active==4)?'':'hidden'?>" id="panel_proposal">
            <div class="row">
              <form action="<?=base_url('user-panel-services/proposal-credit-payment')?>" method="POST" id="frm_max_pro">
                <input type="hidden" name="cust_id" value="<?=$operator['cust_id']?>">
                <input type="hidden" name="panel_active" value="3">
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <h4><?= $this->lang->line('Available proposal credits'); ?></h4>
                </div>
                <div class="col-xl-2 col-xl-offset-2 col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-6 col-xs-6">
                  <h3><strong><i class="fa fa-handshake-o"></i> <?=( strtotime($operator['current_subscription_expiry']) < strtotime(date('Y-m-d')) )?0:$operator['proposal_credits']?></strong></h3>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left">
                  <select class="select2 form-control" name="subscription_id">
                    <option value=""><?= $this->lang->line('Select subscription to buy'); ?></option>
                    <?php foreach ($subscriptions as $subscription) { ?>
                      <option value="<?=$subscription['subscription_id']?>"><?=$subscription['subscription_title']?> [<?= $this->lang->line('Proposal credits'); ?> - <?=$subscription['proposal_credit']?>] [<?=$subscription['currency_code']?> <?=$subscription['price']?>]</option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left">
                  <button class="btn btn-info btn-outline" type="submit">&nbsp;&nbsp;&nbsp;<i class="fa fa-cart-plus"></i> <?=$this->lang->line('Buy Now')?>&nbsp;&nbsp;&nbsp;</button>
                </div>
                <?php if($this->session->flashdata('error')):  ?>
                  <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                    <div class="alert alert-danger" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                <?php endif; ?>
                <?php if($this->session->flashdata('success')):  ?>
                  <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                    <div class="alert alert-success" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                <?php endif; ?>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <p style="color: red"><?= $this->lang->line('expiry_date'); ?> <?=$operator['current_subscription_expiry']?></p>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <p style="color: gray">* <?= $this->lang->line('You can buy more proposal credits, send proposals to even more projects and increase your chances to get awarded one!'); ?></p>
                </div>
              </form>
            </div>
          </div>
        </div>
        
        <div class="panel panel-danger" style="margin-bottom: 10px !important;">
          <div class="panel-heading" style="height: 48px;">
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10"><h5 style="margin-top: 5px;"><i class="fa fa-money"></i> <?= $this->lang->line('Hourly Rates'); ?> - [ <?= $operator['rate_per_hour'] != 0 ? $operator['rate_per_hour'] . ' ' . $operator['currency_code'] : $this->lang->line('not_provided') ?> ]</h5></div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;"><a class="btn btn-sm btn-default" id="btn_rate_hour"><i class="fa fa-pencil"></i> <?= $this->lang->line('edit'); ?></a></div>
          </div>
          <div style="border-top: 0px; padding-top: 0px;" class="panel-body <?=($panel_active==5)?'':'hidden'?>" id="panel_rate_hour">
            <div class="row">

           
              <form action="<?=base_url('user-panel-services/update-rate-hour')?>" method="POST" id="frm_rate_hour">
                <input type="hidden" name="cust_id" value="<?=$operator['cust_id']?>">
                <input type="hidden" name="panel_active" value="5">
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <h4><?= $this->lang->line('Define your hourly work rate to ease your customer to pick you.'); ?></h4>
                </div>
                <div class="col-xl-4 col-xl-offset-2 col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-6 col-xs-6">
                  <label class=""><?= $this->lang->line('Daily rate'); ?> </label>
                  <input type="text" class="form-control" name="rate_per_hour" id="rate_per_hour" value="<?=($operator['rate_per_hour']=='NULL')?0:$operator['rate_per_hour']?>" />
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6">
                  <label class=""><?= $this->lang->line('Daily hours'); ?> </label>
                  <input type="text" class="form-control" name="daily_hours" id="daily_hours" value="<?=($operator['daily_hours']=='NULL')?0:$operator['daily_hours']?>" />
                </div>

                <div class="col-xl-4 col-xl-offset-2 col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-6 col-xs-6" style="padding-top: 10px;">
                  <label class=""><?= $this->lang->line('Weekly rate'); ?> </label>
                  <input type="text" class="form-control" name="rate_per_hour_weekly" id="rate_per_hour_weekly" value="<?=($operator['rate_per_hour_weekly']=='NULL')?0:$operator['rate_per_hour_weekly']?>" />
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding-top: 10px;">
                  <label class=""><?= $this->lang->line('Weekly hours'); ?> </label>
                  <input type="text" class="form-control" name="weekly_hours" id="weekly_hours" value="<?=($operator['weekly_hours']=='NULL')?0:$operator['weekly_hours']?>" />
                </div>

                <div class="col-xl-4 col-xl-offset-2 col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-6 col-xs-6" style="padding-top: 10px;">
                  <label class=""><?= $this->lang->line('Monthly rate'); ?> </label>
                  <input type="text" class="form-control" name="rate_per_hour_monthly" id="rate_per_hour_monthly" value="<?=($operator['rate_per_hour_monthly']=='NULL')?0:$operator['rate_per_hour_monthly']?>" />
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6" style="padding-top: 10px;">
                  <label class=""><?= $this->lang->line('Monthly hours'); ?> </label>
                  <input type="text" class="form-control" name="monthly_hours" id="monthly_hours" value="<?=($operator['monthly_hours']=='NULL')?0:$operator['monthly_hours']?>" />
                </div>
                

                <div class="col-xl-4 col-xl-offset-2 col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-6 col-xs-6" style="padding-top: 10px;">
                  <select class="select2 form-control" name="currency_code">
                    <option value="" <?=($operator['currency_code']=='NULL')?'selected':''?>><?= $this->lang->line('Select Currency'); ?></option>
                    <?php foreach ($currencies as $currency) { ?>
                      <option value="<?=$currency['currency_sign']?>" <?=($currency['currency_sign']==$operator['currency_code'])?'selected':''?> ><?=$currency['currency_title']?> [<?=$currency['currency_sign']?>]</option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 " style="padding-top: 10px;">
                <button class="btn btn-info btn-outline" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-floppy-o"></i> <?=$this->lang->line('save')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;
                </div>               
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <p style="color: gray">* <?= $this->lang->line('We strongly recommend to set hourly work rate to win more work'); ?></p>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="display: inline-flex;">
                  
                  <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('error'); ?></div>
                  <?php endif; ?>
                  <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('success'); ?></div>
                  <?php endif; ?>
                </div>
              </form>
              <script>
                $("#frm_rate_hour").validate({
                  rules: {
                    currency_code: {
                      required: true,
                    },
                    rate_per_hour: {
                      required: true,
                      number: true,
                      min: 1,
                    },
                    daily_hours: {
                      required: true,
                      number: true,
                      min: 1,
                    },
                    rate_per_hour_weekly: {
                      required: true,
                      number: true,
                      min: 1,
                    },
                    weekly_hours: {
                      required: true,
                      number: true,
                      min: 1,
                    },
                    rate_per_hour_monthly: {
                      required: true,
                      number: true,
                      min: 1,
                    },
                    monthly_hours: {
                      required: true,
                      number: true,
                      min: 1,
                    },
                  },
                  messages: {
                    currency_code: { 
                      required: "<?=$this->lang->line('Select Currency')?>",
                    },
                    rate_per_hour: { 
                      required: "<?=$this->lang->line('Enter daily rate')?>",
                      number: "<?=$this->lang->line('numbers_only')?>",
                      min: "<?=$this->lang->line('Minimum 1 required.')?>",
                    },

                    daily_hours: { 
                      required: "<?=$this->lang->line('Enter daily hours')?>",
                      number: "<?=$this->lang->line('numbers_only')?>",
                      min: "<?=$this->lang->line('Minimum 1 required.')?>",
                    },
                    rate_per_hour_weekly: { 
                      required: "<?=$this->lang->line('Enter weekly rate')?>",
                      number: "<?=$this->lang->line('numbers_only')?>",
                      min: "<?=$this->lang->line('Minimum 1 required.')?>",
                    },
                    weekly_hours: { 
                      required: "<?=$this->lang->line('Enter weekly hours')?>",
                      number: "<?=$this->lang->line('numbers_only')?>",
                      min: "<?=$this->lang->line('Minimum 1 required.')?>",
                    },
                    rate_per_hour_monthly: { 
                      required: "<?=$this->lang->line('Enter monthly rate')?>",
                      number: "<?=$this->lang->line('numbers_only')?>",
                      min: "<?=$this->lang->line('Minimum 1 required.')?>",
                    },
                    monthly_hours: { 
                      required: "<?=$this->lang->line('Enter monthly hours')?>",
                      number: "<?=$this->lang->line('numbers_only')?>",
                      min: "<?=$this->lang->line('Minimum 1 required.')?>",
                    },
                  }
                });
              </script>
            </div>
          </div>
        </div>

        <div class="panel panel-default" style="margin-bottom: 10px !important;">
          <div class="panel-heading" style="height: 48px;">
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10"><h5 style="margin-top: 5px;"><i class="fa fa-globe"></i> <?= $this->lang->line('Working Location'); ?> - [ <?= $operator['work_location'] != 'NULL' ? ucwords($operator['work_location']) : $this->lang->line('not_provided') ?> ]</h5></div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;"><a class="btn btn-sm btn-default" id="btn_work_location"><i class="fa fa-pencil"></i> <?= $this->lang->line('edit'); ?></a></div>
          </div>
          <div style="border-top: 0px; padding-top: 0px;" class="panel-body <?=($panel_active==6)?'':'hidden'?>" id="panel_work_location">
            <div class="row">
              <form action="<?=base_url('user-panel-services/update-work-location')?>" method="POST" id="frm_work_location">
                <input type="hidden" name="cust_id" value="<?=$operator['cust_id']?>">
                <input type="hidden" name="panel_active" value="6">
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <h4><?= $this->lang->line('Define your work location?'); ?></h4>
                </div>
                <div class="col-xl-3 col-xl-offset-2 col-lg-3 col-lg-offset-2 col-md-3 col-md-offset-2 col-sm-6 col-xs-6">
                  <select class="select2 form-control" name="work_location">
                    <option value="" <?=($operator['work_location']=='NULL')?'selected':''?>><?=$this->lang->line('Select location type.')?></option>
                    <option value="remotely" <?=($operator['work_location']=='remotely')?'selected':''?> ><?=$this->lang->line('Remotely')?></option>
                    <option value="on_site" <?=($operator['work_location']=='on_site')?'selected':''?> ><?=$this->lang->line('On Site')?></option>
                    <option value="both" <?=($operator['work_location']=='both')?'selected':''?> ><?=$this->lang->line('Remotely and On site')?></option>
                  </select>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-6 col-xs-6 text-left" style="display: inline-flex;">
                  <button class="btn btn-info btn-outline" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-floppy-o"></i> <?=$this->lang->line('save')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;
                  <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('error'); ?></div>
                  <?php endif; ?>
                  <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('success'); ?></div>
                  <?php endif; ?>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <p style="color: gray">* <?= $this->lang->line('We strongly recommend to set work location that could severely affect your rankings and your chances to win more work'); ?></p>
                </div>
              </form>
              <script>
                $("#frm_work_location").validate({
                  rules: {
                    work_location: {
                      required: true,
                    },
                  },
                  messages: {
                    work_location: { 
                      required: "<?=$this->lang->line('Select location type.')?>",
                    },
                  }
                });
              </script>
            </div>
          </div>
        </div>

        <div class="panel panel-primary" style="margin-bottom: 10px !important;">
          <div class="panel-heading" style="height: 48px;">
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10"><h5 style="margin-top: 5px;"><i class="fa fa-share-square-o"></i> <?= $this->lang->line('Social Media Profiles'); ?> - [<?= $this->lang->line('Facebook'); ?> <?= $this->lang->line('Linkedin'); ?> <?= $this->lang->line('Twitter'); ?>]</h5></div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;"><a class="btn btn-sm btn-default" id="btn_social_media"><i class="fa fa-pencil"></i> <?= $this->lang->line('edit'); ?></a></div>
          </div>
          <div style="border-top: 0px; padding-top: 0px;" class="panel-body <?=($panel_active==7)?'':'hidden'?>" id="panel_social_media">
            <div class="row">
              <form action="<?=base_url('user-panel-services/update-social-media')?>" method="POST" id="frm_social_media">
                <input type="hidden" name="cust_id" value="<?=$operator['cust_id']?>">
                <input type="hidden" name="panel_active" value="7">
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                  <h4><?= $this->lang->line('Update your social media profiles.'); ?></h4>
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12 form-group">
                  <input type="text" class="form-control" name="linkedin_url" id="linkedin_url" value="<?=($operator['linkedin_url']=='NULL')?'':$operator['linkedin_url']?>" placeholder="<?= $this->lang->line('Linkedin profile URL'); ?> <?= $this->lang->line('Example'); ?> - https://linkedin.com/in/gonagoo-contact" autocomplete="none" />
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12 form-group">
                  <input type="text" class="form-control" name="twitter_url" id="twitter_url" value="<?=($operator['twitter_url']=='NULL')?'':$operator['twitter_url']?>" placeholder="<?= $this->lang->line('Twitter profile URL'); ?> <?= $this->lang->line('Example'); ?> - https://twitter.com/Gonagoocm" autocomplete="none" />
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12 form-group">
                  <input type="text" class="form-control" name="fb_url" id="fb_url" value="<?=($operator['fb_url']=='NULL')?'':$operator['fb_url']?>" placeholder="<?= $this->lang->line('Facebook profile URL'); ?> <?= $this->lang->line('Example'); ?> - https://www.facebook.com/gonagooFR" autocomplete="none" />
                </div>
                <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12 form-group" style="display: inline-flex;">
                  <button class="btn btn-info btn-outline" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-floppy-o"></i> <?=$this->lang->line('save')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;
                  <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('error'); ?></div>
                  <?php endif; ?>
                  <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success" style="height: 32px; padding-top: 5px;"><?= $this->session->flashdata('success'); ?></div>
                  <?php endif; ?>
                </div>
              </form>
              <script>
                $().ready(function() {
                  $.validator.addMethod("URLVALIDATOR", function(value, element) {
                      return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
                  }, "<?= $this->lang->line('Please enter valid URL.'); ?>");

                  // Validate signup form on keyup and submit
                  $("#frm_social_media").validate({
                      rules: {
                          linkedin_url: "URLVALIDATOR",
                          twitter_url: "URLVALIDATOR",
                          fb_url: "URLVALIDATOR",
                      },
                  });
              });
              </script>
              <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <p style="color: gray">* <?= $this->lang->line('We strongly recommend to set your social media profile to let your customer more about you that could severely affect your rankings and your chances to win more work'); ?></p>
              </div>
            </div>
          </div>
        </div>

        <script>
          $("#btn_working_days").click(function() {
            $("#panel_working_days").removeClass("hidden");
            $("#panel_off_days").addClass("hidden");
            $("#panel_max_pro").addClass("hidden");
            $("#panel_proposal").addClass("hidden");
            $("#panel_rate_hour").addClass("hidden");
            $("#panel_work_location").addClass("hidden");
            $("#panel_social_media").addClass("hidden");
          });
          $("#btn_off_days").click(function() {
            $("#panel_off_days").removeClass("hidden");
            $("#panel_working_days").addClass("hidden");
            $("#panel_max_pro").addClass("hidden");
            $("#panel_proposal").addClass("hidden");
            $("#panel_rate_hour").addClass("hidden");
            $("#panel_work_location").addClass("hidden");
            $("#panel_social_media").addClass("hidden");
          });
          $("#btn_max_pro").click(function() {
            $("#panel_max_pro").removeClass("hidden");
            $("#panel_working_days").addClass("hidden");
            $("#panel_off_days").addClass("hidden");
            $("#panel_proposal").addClass("hidden");
            $("#panel_rate_hour").addClass("hidden");
            $("#panel_work_location").addClass("hidden");
            $("#panel_social_media").addClass("hidden");
          });
          $("#btn_proposal").click(function() {
            $("#panel_proposal").removeClass("hidden");
            $("#panel_working_days").addClass("hidden");
            $("#panel_off_days").addClass("hidden");
            $("#panel_max_pro").addClass("hidden");
            $("#panel_rate_hour").addClass("hidden");
            $("#panel_work_location").addClass("hidden");
            $("#panel_social_media").addClass("hidden");
          });
          $("#btn_rate_hour").click(function() {
            $("#panel_rate_hour").removeClass("hidden");
            $("#panel_working_days").addClass("hidden");
            $("#panel_off_days").addClass("hidden");
            $("#panel_max_pro").addClass("hidden");
            $("#panel_proposal").addClass("hidden");
            $("#panel_work_location").addClass("hidden");
            $("#panel_social_media").addClass("hidden");
          });
          $("#btn_work_location").click(function() {
            $("#panel_work_location").removeClass("hidden");
            $("#panel_rate_hour").addClass("hidden");
            $("#panel_working_days").addClass("hidden");
            $("#panel_off_days").addClass("hidden");
            $("#panel_max_pro").addClass("hidden");
            $("#panel_proposal").addClass("hidden");
            $("#panel_social_media").addClass("hidden");
          });
          $("#btn_social_media").click(function() {
            $("#panel_social_media").removeClass("hidden");
            $("#panel_work_location").addClass("hidden");
            $("#panel_rate_hour").addClass("hidden");
            $("#panel_working_days").addClass("hidden");
            $("#panel_off_days").addClass("hidden");
            $("#panel_max_pro").addClass("hidden");
            $("#panel_proposal").addClass("hidden");
          });
        </script>

    </div>
  </div>
</div>

<script>
  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $(this).val();
    $('#city_id').attr('disabled', true);

    $.ajax({
      url: "<?= base_url('user-panel-services/get-state-by-country-id')?>",
      type: "POST",
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){ 
        $('#state_id').attr('disabled', false);
        $('#state_id').empty(); 
        $('#state_id').append('<option value=""><?= json_encode($this->lang->line('select_state')); ?></option>');
        $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
        $('#state_id').focus();
      },
      beforeSend: function(){
        $('#state_id').empty();
        $('#state_id').append('<option value=""><?= json_encode($this->lang->line('loading')); ?></option>');
      },
      error: function(){
        $('#state_id').attr('disabled', true);
        $('#state_id').empty();
        $('#state_id').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?></option>');
      }
    })
  });

  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    $.ajax({
      type: "POST",
      url: "<?= base_url('user-panel-services/get-cities-by-state-id')?>",
      data: { state_id: state_id },
      dataType: "json",
      success: function(res){ 
        $('#city_id').attr('disabled', false);
        $('#city_id').empty(); 
        $('#city_id').append('<option value=""><?= json_encode($this->lang->line('select_city')); ?></option>');
        $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
        $('#city_id').focus();
      },
      beforeSend: function(){
        $('#city_id').empty();
        $('#city_id').append('<option value=""><?= json_encode($this->lang->line('loading')); ?></option>');
      },
      error: function(){
        $('#city_id').attr('disabled', true);
        $('#city_id').empty();
        $('#city_id').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?></option>');
      }
    })
  });
</script>
<script>
  $(function(){
    $("#updateProfile").validate({
      ignore: [],
      rules: {
        avatar: {  accept:"jpg,png,jpeg,gif" },
        cover: {  accept:"jpg,png,jpeg,gif" },
        firstname: { required: true, },
        lastname: { required: true, },
        company_name: { required: true, },
        contact_no: { required: true, },
        country_id: { required: true, },
        state_id: { required: true, },
        city_id: { required: true, },
        zipcode: { required: true, },
        address: { required: true, },
        email_id: { required: true, },
        introduction: { required: true, },
        street_name: { required: true, },
      }, 
      messages: {
        avatar: {  accept: <?= json_encode($this->lang->line('only_image_allowed')); ?>,   },
        cover: {  accept: <?= json_encode($this->lang->line('only_image_allowed')); ?>,   },
        firstname: { required: <?= json_encode($this->lang->line('enter_first_name')); ?>,   },
        lastname: { required: <?= json_encode($this->lang->line('enter_last_name')); ?>,  },
        company_name: { required: <?= json_encode($this->lang->line('enter_company_name')); ?>,  },
        contact_no: { required: <?= json_encode($this->lang->line('enter_mobile_number')); ?>, },
        country_id: { required: <?= json_encode($this->lang->line('select_country')); ?>,   },
        state_id: { required: <?= json_encode($this->lang->line('select_state')); ?>,   },
        city_id: { required: <?= json_encode($this->lang->line('select_city')); ?>,  },
        zipcode: { required: <?= json_encode($this->lang->line('enter_zipcode')); ?>,   },
        address: { required: <?= json_encode($this->lang->line('enter_address')); ?>,   },
        email_id: { required: <?= json_encode($this->lang->line('enter_email_address'));?>, },
        introduction: { required: <?= json_encode($this->lang->line('enter_some_introduction'));?>, },
        street_name: { required: <?= json_encode($this->lang->line('street_name'));?>, },
      }
    });

    $("#upload_avatar").on('click', function(e) { e.preventDefault(); $("#avatar").trigger('click'); });
    $("#upload_cover").on('click', function(e) { e.preventDefault(); $("#cover").trigger('click'); });
  });
  function avatar_name(e){ if(e.target.files[0].name !="") { $("#avatar_name").removeClass('hidden'); }}
  function cover_name(e){ if(e.target.files[0].name !="") { $("#cover_name").removeClass('hidden'); }}
</script>
<script>
  /*
  $(function() {
        var geocoder;
        geocoder = new google.maps.Geocoder();
        var lat = '';
        var lng = '';
        var address = document.getElementById("zipcode").value;
        //alert(address);
        geocoder.geocode({
            'address': address
        }, function(results, status) {
          alert(results[0].geometry.location.lat());
          alert(results[0].geometry.location.lng());
            if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        //alert('Latitude: ' + lat + ' Logitude: ' + lng);
  });
  */
  $('#us3').locationpicker({
    location: {
      latitude: <?= $operator['latitude'] ?>,
      longitude: <?= $operator['longitude'] ?>
    },
    radius: 300,
    inputBinding: {
      latitudeInput: $('#us3-lat'),
      longitudeInput: $('#us3-lon'),
      radiusInput: $('#us3-radius'),
      locationNameInput: $('#us3-address')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
      //Uncomment line below to show alert on each Location Changed event
      //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
  });
</script>