<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
   display: none;
  }
  .hoverme {
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
  }
  .btnMenu:hover {
    text-decoration: none;
    background-color: #ffb606;
    color: #FFF;
  }

  .fa-heart-o: {
    content: "\f08a";
    color: red;
    text-shadow: 1px 1px 1px #ccc;
    font-size: 0.7em;
  }

  .fa-heart:{
    content: "\f08a";
    color: red;
    text-shadow: 1px 1px 1px #ccc;
    font-size: 0.7em;
  } 
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Favorite projects'); ?></span></li>
        </ol>
      </div>
      <h3 class="">
        <i class="fa fa-search text-muted"></i> <strong><?= $this->lang->line('Favorite projects'); ?> - <?= sizeof($jobs)." ".$this->lang->line('Found'); ?></strong>&nbsp;&nbsp;&nbsp;
        <a class="btn btn-outline btn-info" href="<?=base_url('user-panel-services/browse-project'); ?>"><i class="fa fa-heart"></i> <?= $this->lang->line('More projects'); ?></a>
      </h3>
    </div>
  </div>
</div>
<div class="content" style="padding-top: 45px;">
  

  <?php if($this->session->flashdata('error')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:red; color:#FFF; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('error'); ?></strong></div>
    </div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:green; color:white; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('success'); ?></strong></div>
    </div>
  <?php endif; ?>

  <?php if(sizeof($jobs) > 0) { ?>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-left: 0px;padding-right: 0px;">
        <table id="addressTableData" class="table">
          <thead><tr class="hidden"><th><?=$job['job_id']?></th><th></th></tr></thead>
          <tbody>
            <?php foreach ($jobs as $job) { if($job['service_type'] == 0) { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: -5px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding-left: 0px;">
                            <p style="margin: 0px;"><img align="middle" src="<?=base_url($job['icon_url'])?>" /> <?=$job['cat_name']?></p>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
                            <h6><i class="fa fa-tag"></i> Ref #GS-<?=$job['job_id']?></h6>
                          </div>
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12 text-right" style="padding-right: 0px;">
                            <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=($job['work_type']!="Per Hour")?$job['currency_code']." ".$job['budget']:$job['currency_code']." ".$job['budget']."<small>/hr</small>"?></strong></h4>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <h4 style="margin-top: 5px;"><?=$job['job_title']?>&nbsp;&nbsp;<span class="label label-primary"><?=($job['service_type']==1)?$this->lang->line('Offer'):$this->lang->line('Project')?></span>&nbsp;&nbsp;  <!--   <?php if($job['cancellation_status'] != 'NULL'): ?>
                                <span class="label label-warning"><?= ucwords(str_replace('_', ' ', $job['cancellation_status'])) ?></span> 
                               <?php endif; ?> -->
                            </h4>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                           <?=$job['work_type']?>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <i class="fa fa- fa-clock-o"></i> <?= $this->lang->line('posted on'); ?>: <?=date('D, d M y', strtotime($job['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-map-marker"></i>&nbsp;<?=$job['location_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-dot-circle-o"></i> <?=$this->lang->line('Proposals').": ".$job['proposal_counts']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button id="btn_add_<?=$job['job_id']?>" style=" height:5px;width:5px;padding: 0px;display:<?=(in_array($job['job_id'], $fav_projects)?"inline-block":"none")?>" title="<?=$this->lang->line('Remove from favorite')?>" class="btn-link remove_favorite_<?=$job['job_id']?>" data-id="<?=$job['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job['job_id']?>" class="fa fa-heart"></i></font></button>
                            <button id="btn_remove_<?=$job['job_id']?>" style="height:5px;width:5px;padding: 0px;display:<?=(in_array($job['job_id'], $fav_projects)?"none":"inline-block")?> " title="<?=$this->lang->line('Add to favorite')?>" class="btn-link add_favorite_<?=$job['job_id']?>" data-id="<?=$job['job_id']?>"><font size="5px" style="color: red;font-size: 1.4em;"><i id="add_favorite_<?=$job['job_id']?>" class="fa fa-heart-o"></i></font></button>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <script>
                              $('.add_favorite_<?=$job['job_id']?>').click(function () { 
                                var id = $(this).attr("data-id");
                                $.post('<?=base_url("user-panel-services/make-job-favourite")?>', {id: id}, 
                                function(res) { 
                                  console.log(res);
                                  if($.trim(res) == "success") { 
                                    swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Job added to your favorite list.')); ?>, "success");
                                    $("#btn_remove_<?=$job['job_id']?>").css("display", "none");
                                    $("#btn_add_<?=$job['job_id']?>").css("display", "inline-block");
                                  } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to add. Try again...')); ?>, "error");  } 
                                });
                              });
                              $('.remove_favorite_<?=$job['job_id']?>').click(function () { 
                                var id = $(this).attr("data-id");
                                $.post('<?=base_url("user-panel-services/job-remove-from-favorite")?>', {id: id}, 
                                function(res) { 
                                  //console.log(res);
                                  if($.trim(res) == "success") { 
                                    swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Job removed from favorite list.')); ?>, "success"); 
                                    $("#btn_remove_<?=$job['job_id']?>").css("display", "inline-block");
                                    $("#btn_add_<?=$job['job_id']?>").css("display", "none");
                                  } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to remove. Try again...')); ?>, "error");  } 
                                });
                              });
                            </script>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } else { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: 0px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          Job - <?=$job['job_title']?>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php } else { ?>
    <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: 0px; margin-top: 20px; margin-left: -7px; margin-right: -7px;">
      <div class="panel-body" style="border-radius: 10px; padding: 15px;">
        <h5><?= $this->lang->line('No jobs found.'); ?></h5>
      </div>
    </div>
  <?php } ?>
</div>

<script>
  $('#advanceBtn').click(function(){
    if($('#advanceSearch').css('display') == 'none') {
      $('#basicSearch').hide("slow");
      $('#advanceSearch').show("slow");
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Hide Filters'))?>);
      //$('#searchbox').hide("slow");
      //$('#searchlabel').hide("slow");
    } else {
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Show Filters'))?>);
      $('#advanceSearch').hide("slow");
      $('#basicSearch').show("slow");
      //$('#searchbox').show("slow");
      //$('#searchlabel').show("slow");
    }
    return false;
  });
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-sub-category", 
      data: { cat_type_id: cat_type_id },
      dataType: "json",
      success: function(res) {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('select_category')?></option>");
        $.each( res, function() {
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function() {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('loading')?></option>");
      },
      error: function() {
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('no_options')?></option>");
      }
    })
  });
</script>