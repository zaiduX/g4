<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/user-profile'); ?>"><?= $this->lang->line('profile'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('edit_skill'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-photo fa-2x text-muted"></i> <?= $this->lang->line('skill'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('edit_skill_details'); ?></small>    
    </div>
  </div>
</div>
 
<div class="content">
  <div class="row">
    <div class="hpanel hblue">
      <form action="<?= base_url('user-panel-services/edit-skill'); ?>" method="post" class="form-horizontal" id="addskillForm" >
        <div class="panel-body">              
          <div class="col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

            <?php if($this->session->flashdata('error')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                </div>
              </div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                </div>
              </div>
            <?php endif; ?>
            <div class="row">
              <div class="form-group">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label class=""><?= $this->lang->line('skills'); ?></label>
                  <select class="js-source-states" style="width: 100%" name="skills[]" id="skills" multiple="multiple">
                    <?php 
                      $old_sub_cat = explode(',', $old_sub_cat);
                      foreach ($sub_category as $sub_cat) { ?>
                      <option value="<?= $sub_cat['cat_id'] ?>" <?=(in_array($sub_cat['cat_name'], $old_sub_cat))?'selected':''?>><?= $sub_cat['cat_name'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
          </div>

        </div>        
        <div class="panel-footer"> 
          <div class="row">
             <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
                <a href="<?= base_url('user-panel-services/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
             </div>
             <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('update_detail'); ?></button>               
             </div>
           </div>         
        </div>
      </form>
    </div>
  </div>
</div>