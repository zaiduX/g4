<style>
	.avatar1 { width: 12.3%; max-height: 110px;  }   
	.cover { 
		<?php if($operator['cover_url'] == "NULL"): ?> 
			background-image: url(<?= $this->config->item('resource_url'). 'bg.jpg'; ?>) !important;
		<?php else: ?>
			background-image: url(<?= base_url($operator['cover_url']); ?>) !important;
		<?php endif; ?>
			background-position: center !important; background-repeat: no-repeat !important; background-size: cover !important;    height: 150px;  
	}
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Provider Profile'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('Provider Profile'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Manage Provider Profile Details'); ?></small>    
    </div>
  </div>
</div>

<div class="content content-boxed" style="max-width: 1200px !important;">
  <div class="row">
    <div class="hpanel hgreen" style="margin-bottom: 0px;">
      <div class="panel-body cover">
        <?php if($operator['avatar_url'] == "NULL"): ?>
          <img src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>" class="img-thumbnail m-b avatar1" alt="avatar" />
        <?php else: ?>
          <img src="<?= base_url($operator['avatar_url']); ?>" class="img-thumbnail m-b avatar1" alt="avatar" />
        <?php endif; ?>
      </div>
    </div>
  </div>

  <?php if($this->session->flashdata('error')):  ?>
    <div class="row"><div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div></div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
    <div class="row"><div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div></div>
  <?php endif; ?>

  <div class="row">
    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12" style="padding-left: 0px; margin-bottom: -15px;">
      <div class="hpanel hblue">
      	<div class="panel-heading" style="margin-bottom: -15px; margin-top: -15px;">
          <h4><strong><?= $this->lang->line('Profile Details'); ?></strong></h4>
        </div>
        <div class="panel-body" style="padding-bottom: 15px;">
          <div class="row">
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"> 
            	<i class="fa fa-user"></i>
            </div>   
            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-8 col-xs-8" style="padding-left: 5px;">    
              <p><?= $operator['firstname'] != "NULL" ? $operator['firstname'] . ' ' . $operator['lastname'] : $this->lang->line('not_provided') ?></p>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 text-right">
              <a title="<?=$this->lang->line('edit')?>" href="<?= base_url('user-panel-services/edit-provider-profile'); ?>" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></a>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"> 
            	<i class="fa fa-building-o"></i>
            </div>
            <div class="col-xl-11 col-lg-11 col-md-10 col-sm-10 col-xs-10" style="padding-left: 5px;">    
              <p><?= $operator['company_name'] != "NULL" ? $operator['company_name'] : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
          <div class="row">
          	<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"> 
            	<i class="fa fa-envelope-o"></i>
            </div>
            <div class="col-xl-11 col-lg-11 col-md-10 col-sm-10 col-xs-10" style="padding-left: 5px;">    
              <p><?= $operator['email_id'] != "NULL" ? $operator['email_id'] : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
          <div class="row">
          	<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"> 
            	<i class="fa fa-phone"></i>
            </div>
            <div class="col-xl-11 col-lg-11 col-md-10 col-sm-10 col-xs-10" style="padding-left: 5px;">    
              <p><?= $operator['contact_no'] != "NULL" ? $operator['contact_no'] : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
          <div class="row">
          	<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"> 
            	<i class="fa fa-map-marker"></i>
            </div>
          	<div class="col-xl-11 col-lg-11 col-md-10 col-sm-10 col-xs-10" style="padding-left: 5px;"> 
              <p>
                <?= $operator['city_id'] != "NULL" ? $this->api->get_city_name_by_id($operator['city_id']) : $this->lang->line('not_provided') ?> / 
                <?= $operator['state_id'] != "NULL" ? $this->api->get_state_name_by_id($operator['state_id']) : $this->lang->line('not_provided') ?> / 
                <?= $operator['country_id'] != "NULL" ? $this->api->get_country_name_by_id($operator['country_id']) : $this->lang->line('not_provided') ?>
              </p>
            </div>
          </div>
          <div class="row">
          	<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"> 
            	<i class="fa fa-address-card-o"></i>
            </div>
            <div class="col-xl-11 col-lg-11 col-md-10 col-sm-10 col-xs-10" style="padding-left: 5px;">     
              <p><?= $operator['address'] != "NULL" ? $operator['street_name'].', '.$operator['address'] : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
          <div class="row">
          	<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"> 
            	<i class="fa fa-map-pin"></i>
            </div>
            <div class="col-xl-11 col-lg-11 col-md-10 col-sm-10 col-xs-10" style="padding-left: 5px;">    
              <p><?= $operator['zipcode'] != "NULL" ? $operator['zipcode'] : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
          <div class="row">
          	<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"> 
            	<i class="fa fa-info"></i>
            </div>  
            <div class="col-xl-11 col-lg-11 col-md-10 col-sm-10 col-xs-10" style="padding-left: 5px;">    
              <p><?= $operator['introduction'] != "NULL" ? $operator['introduction'] : $this->lang->line('not_provided') ?></p>
            </div>  
          </div>
          <div class="row"> 
          	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">  
            	<h5><?=$this->lang->line('Gonagoo Profile')?></h5>
            </div> 
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">  
            	<input type="text" class="form-control" name="profile_url_login" id="profile_url_login" value="<?=$operator['profile_url_login']?>" readonly="readonly">
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">  
            	<button class="btn btn-info btn-sm" onclick="myFunction()"><?=$this->lang->line('Copy URL')?></button>
            </div>
            <script>
							function myFunction() {
							  var copyText = document.getElementById("profile_url_login");
							  copyText.select();
							  document.execCommand("copy");
							  swal("<?=$this->lang->line('success')?>", "<?=$this->lang->line('Profile URL copied to clipboard.')?>", "success");
							}
						</script>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-xs-12" style="padding-right: 0px; margin-bottom: -15px;">
    	<div class="hpanel hblue">
      	<div class="panel-heading" style="margin-bottom: -15px; margin-top: -15px;">
          <h4><strong><?= $this->lang->line('Settings'); ?></strong></h4>
        </div>
        <div class="panel-body" style="padding-bottom: 15px;">
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid; min-height: 45px;">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-clock-o"></i> <?=$this->lang->line('Working Days')?></p>
            </div>  
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-11" style="border-bottom: 1px solid; min-height: 45px;">
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><?= $operator['working_days'] != "NULL" ? $operator['working_days'] : $this->lang->line('not_provided') ?></p>
            </div>
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 text-right" style="padding-left: 8px;">
              <a title="<?=$this->lang->line('edit')?>" href="<?= base_url('user-panel-services/edit-provider-profile-settings'); ?>" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></a>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-calendar-o"></i> <?=$this->lang->line('Off Days')?></p>
              <?php if($operator['off_days'] < 0) { echo str_repeat('<br />', (sizeof($off_days) - 1) ); } ?>
            </div>  
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border-bottom: 1px solid">    
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;">
              	<?php if($operator['off_days'] > 0) {
              		foreach ($off_days as $value) { echo $value['from_date'] . ' - ' . $value['to_date'] . ' <br />'; }
              	} else { echo $this->lang->line('not_provided'); } ?> 
            	</p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-envelope-open-o"></i> <?=$this->lang->line('Max. Open Projects')?></p>
            </div>  
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border-bottom: 1px solid">    
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><?= $operator['max_open_order'] ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-handshake-o"></i> <?=$this->lang->line('Proposal Credits')?></p>
            </div>  
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border-bottom: 1px solid">    
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><?= $operator['proposal_credits'] ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-money"></i> <?=$this->lang->line('Hourly Rate')?></p>
            </div>  
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border-bottom: 1px solid">    
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><?= $operator['rate_per_hour'] != 0 ? $operator['rate_per_hour'] . ' ' . $operator['currency_code'] : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-clock-o"></i> <?=$this->lang->line('Response Time')?></p>
            </div>  
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border-bottom: 1px solid">    
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><?= $operator['response_time'] ?> <?=$this->lang->line('hours')?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-globe"></i> <?=$this->lang->line('Working Location')?></p>
            </div>  
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><?= $operator['work_location'] != 'NULL' ? ucwords($operator['work_location']) : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-facebook-square"></i> <?=$this->lang->line('Facebook Profile')?></p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><?= $operator['fb_url'] != 'NULL' ? $operator['fb_url'] : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-twitter-square"></i> <?=$this->lang->line('Twitter Profile')?></p>
            </div>  
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><?= $operator['twitter_url'] != 'NULL' ? $operator['twitter_url'] : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><i class="fa fa-linkedin-square"></i> <?=$this->lang->line('Linkedin Profile')?></p>
            </div>  
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border-bottom: 1px solid">  
              <p style="margin-bottom: 5px !important; margin-top: 3px !important;"><?= $operator['linkedin_url'] != 'NULL' ? $operator['linkedin_url'] : $this->lang->line('not_provided') ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden">
      <div class="hpanel hred hidden">
        <div class="panel-heading">
          <h4><strong><?= $this->lang->line('payment_informations'); ?></strong></h4>
        </div>
        <div class="panel-body">
          <div class="row hidden">
            <div class="col-md-4">    
              <p><strong><?= $this->lang->line('shipping_accepting_mode'); ?></strong></p>
            </div>
            <div class="col-md-6">    
              <p><?= $operator['shipping_mode'] != "0" ? 'Manual' : 'Automatic' ?></p>
            </div>  
            <div class="col-md-2 text-right">
              <form action="edit-deliverer-bank" method="post" style="display: inline-block;">
                <input type="hidden" name="cust_id" value="<?= $operator['cust_id'] ?>">
                <button type="submit" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></button>
              </form>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">    
              <p><strong><?= $this->lang->line('iban'); ?></strong></p>
            </div>
            <div class="col-md-6">    
              <p><?= ($operator['iban'] != "NULL") ? $operator['iban'] : $this->lang->line('not_provided') ?></p>
            </div>  
            <div class="col-md-2 text-right">
              <form action="edit-operator-bank" method="post" style="display: inline-block;">
                <input type="hidden" name="cust_id" value="<?= $operator['cust_id'] ?>">
                <button type="submit" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></button>
            	</form>
            </div>  
          </div>
          <div class="row">
            <div class="col-md-4">    
              <p><strong><?= $this->lang->line('1_Mobile_Money_Account'); ?></strong></p>
            </div>
            <div class="col-md-8">    
              <p><?= ($operator['mobile_money_acc1'] != "NULL") ? $operator['mobile_money_acc1'] : $this->lang->line('not_provided') ?></p>
            </div>  
          </div> 
          <div class="row">
            <div class="col-md-4">    
              <p><strong><?= $this->lang->line('2_Mobile_Money_Account'); ?></strong></p>
            </div>
            <div class="col-md-8">    
              <p><?= ($operator['mobile_money_acc2'] != "NULL") ? $operator['mobile_money_acc2'] : $this->lang->line('not_provided') ?></p>
            </div>  
          </div>    
        </div>
      </div>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
      <div class="hpanel hblue">
        <div class="panel-heading" style="margin-bottom: -15px; margin-top: -15px;">
          <h4><strong><?= $this->lang->line('Operator Documents'); ?></strong></h4>
        </div>
        <div class="panel-body">
          <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11 col-xs-11">
            <?php for($i=0; $i < sizeof($documents); $i++) { ?>
              <div class="col-md-4">
                <div class="hpanel">
                  <div class="panel-body file-body">
                    <i class="fa fa-file-pdf-o text-info"></i>
                  </div>
                  <div class="panel-footer text-center">
                    <div class="row">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h5><a href="<?= $this->config->item('base_url') . $documents[$i]['attachement_url']; ?>"><?= strtoupper($documents[$i]['doc_type']) ?></a></h5>
                        <button title="<?= $this->lang->line('Delete Document'); ?>" style="display: inline-block;" class="btn btn-danger btn-sm docdelete" id="<?= $documents[$i]['doc_id'] ?>"><i class="fa fa-trash"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
          <?php if(sizeof($documents) < 3) { ?>
	          <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 text-right">
	            <a title="<?= $this->lang->line('Add business documents'); ?>" href="<?= base_url('user-panel-services/edit-provider-document'); ?>" class="btn btn-default btn-circle"><i class="fa fa-plus"></i></a>
	          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $('.docdelete').click(function () {
    var id = this.id;
    swal({
      title: "<?= $this->lang->line('are_you_sure'); ?>",
      text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
      cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
      closeOnConfirm: false,
      closeOnCancel: false },
      function (isConfirm) {
        if (isConfirm) {
          $.post("<?=base_url('user-panel-services/provider-document-delete')?>", {id: id}, function(res) {
            swal("<?= $this->lang->line('deleted'); ?>", "<?= $this->lang->line('Document has been deleted.'); ?>", "success");
            setTimeout(function() { window.location.reload(); }, 2000);
          });
        } else {
          swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Document is safe.'); ?>", "error");
        }
      });
  });
</script>