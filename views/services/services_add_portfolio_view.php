<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/user-profile'); ?>"><?= $this->lang->line('profile'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/portfolios'); ?>"><?= $this->lang->line('portfolio'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Add'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-photo fa-2x text-muted"></i> <?= $this->lang->line('portfolio'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('add_portfolio_details'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="hpanel hblue">
      <form action="<?= base_url('user-panel-services/add-portfolio'); ?>" method="post" class="form-horizontal" id="addPortfolioForm" enctype="multipart/form-data">    
      
        <div class="panel-body">              
          <div class="col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
            <?php if($this->session->flashdata('error')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                </div>
              </div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                </div>
              </div>
            <?php endif; ?>
            <div class="row">
              <div class="form-group">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-6">
                  <label class=""><?= $this->lang->line('title'); ?></label>
                  <input name="title" type="text" class="form-control" id="" placeholder="<?= $this->lang->line('title'); ?>" required />
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6">                                      
                  <label class=""><?= $this->lang->line('portfolio_image'); ?></label>
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button id="portfolio_image" class="btn btn-green col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12"><i class="fa fa-user"></i>&nbsp; <?= $this->lang->line('select_portfolio_image'); ?></button> 
                    </span> 
                  </div>
                  <span id="portfolio_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('portfolio_image_attached'); ?></span>
                  <input type="file" id="portfolio" name="portfolio" class="upload attachment" accept="image/*" onchange="portfolio_name(event)"/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <label class=""><?= $this->lang->line('category_type'); ?></label>
                  <select id="cat_type_id" name="cat_type_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category_type'); ?>">
                    <option value=""><?= $this->lang->line('select_category_type'); ?></option>
                    <?php foreach ($category_types as $cat_type): ?>
                      <option value="<?= $cat_type['cat_type_id'] ?>"> <?= $cat_type['cat_type']; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <label class=""><?= $this->lang->line('category'); ?></label>
                  <select id="category_id" name="category_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category'); ?>" disabled>                      
                    <option value=""><?= $this->lang->line('select_category'); ?></option>                      
                  </select>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <label class=""><?= $this->lang->line('sub_categories'); ?></label>
                  <select id="subcat_ids" name="subcat_ids[]" class="form-control select2" data-allow-clear="true" data-placeholder="Select Sub-categories" disabled multiple>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label class=""><?= $this->lang->line('description'); ?></label>
                  <textarea name="description"  class="form-control" id="description" rows="5" placeholder="<?= $this->lang->line('description'); ?>" style="resize: none;"></textarea>
                </div> 
              </div> 
            </div> 
          </div>
        </div>        
        <div class="panel-footer"> 
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
              <a href="<?= base_url('user-panel-services/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
              <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('submit_detail'); ?></button>               
            </div>
          </div>         
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $.trim($(this).val());
    $('#subcat_id').attr('disabled', true);
    $('#category_id').empty();
    if(cat_type_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "<?=base_url('user-panel-services/portfolio/get-category-by-cat-type-id')?>", 
        data: { cat_type_id: cat_type_id },
        dataType: "json",
        success: function(res){ 
          $('#category_id').attr('disabled', false);
          $('#category_id').empty(); 
          $('#category_id').append("<option value=''><?= $this->lang->line('sel_category'); ?></option>");
          $.each( res, function(){$('#category_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');});
          $('#category_id').focus();
        },
        beforeSend: function(){
          $('#category_id').empty();
          $('#category_id').append("<option value=''><?= $this->lang->line('loading'); ?></option>");
        },
        error: function(){
          $('#category_id').attr('disabled', true);
          $('#category_id').empty();
          $('#category_id').append("<option value=''><?= $this->lang->line('no_options'); ?></option>");
        }
      });
    } else { 
      $('#category_id').empty(); 
      $('#category_id').append("<option value=''><?= $this->lang->line('sel_category'); ?></option>");
      $('#category_id, #subcat_id').attr('disabled', true); 
    }
  });

  $("#category_id").on('change', function(event) {  event.preventDefault();
    var category_id = $(this).val();
    if(category_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "<?=base_url('user-panel-services/portfolio/get-subcategories-by-category-id')?>", 
        data: { category_id: category_id },
        dataType: "json",
        success: function(res){ 
          $('#subcat_ids').attr('disabled', false);
          $('#subcat_ids').empty(); 
          $.each( res, function(){ $('#subcat_ids').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>'); });
          $('#subcat_ids').focus();
        },
        beforeSend: function(){
          $('#subcat_ids').empty();
          $('#subcat_ids').append("<option value=''><?= $this->lang->line('loading'); ?></option>");
        },
        error: function(){
          $('#subcat_ids').attr('disabled', true);
          $('#subcat_ids').empty();
          $('#subcat_ids').append("<option value=''><?= $this->lang->line('no_options'); ?></option>");
        }
      });
    } else { 
      $('#subcat_ids').empty(); 
      $('#subcat_ids').attr('disabled', true); 
    }
  });
 
 $(function(){
    $("#addPortfolioForm").validate({
      ignore: [],
      rules: {
        portfolio: { required: true, accept:"jpg,png,jpeg,gif" },
        title: { required: true, },
        cat_type_id: { required: true, },
        category_id: { required: true, },
      }, 
      messages: {
        portfolio: { required: "<?= $this->lang->line('select_portfolio_image'); ?>", accept: "<?= $this->lang->line('only_image_allowed'); ?>"   },
        title: { required: "<?= $this->lang->line('title'); ?>",   },
        cat_type_id: { required: "<?= $this->lang->line('select_category_type'); ?>",  },
        category_id: { required: "<?= $this->lang->line('select_category'); ?>",  },
      }
    });
    $("#portfolio_image").on('click', function(e) { e.preventDefault(); $("#portfolio").trigger('click'); });
  });

  function portfolio_name(e){ if(e.target.files[0].name !="") { $("#portfolio_name").removeClass('hidden'); }}
</script>