<div class="text-center" style="margin-top:0px !important; margin-bottom:0px !important; max-width: 100% !important;">
    <div class="row" style="padding: 45px 15px 45px 15px; ">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 hidden-xs">
        <h1 class="" style="-webkit-text-stroke: 1px black; font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Live your work dream'); ?></span></h1>
        <p class="lead" style="color: white !important; padding-bottom: 0px;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Our community of expert freelancers gives you the power to find the right person for any project in minutes.'); ?></span></p>
      </div>
      
      <div class="col-xs-12 hidden-xl hidden-lg hidden-md hidden-sm">
        <h6 class="" style="font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Live your work dream'); ?></span></h6>
        <h6 style="color: white !important; margin-bottom: 30px !important;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Our community of expert freelancers gives you the power to find the right person for any project in minutes.'); ?></span></h6>
      </div>

      <div class="col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12" style="margin-top: 180px;">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-sx-12" style="padding-left: 0px; padding-right: 0px">
          <form method="get" action="browse_provider" class="form-inline">
            <input class="form-control mr-sm-2" type="text" name="skill" placeholder="<?= $this->lang->line('Search for any skill'); ?>" aria-label="<?= $this->lang->line('Search for any skill'); ?>" style="margin-right: -35px; border: 0px solid #1e5394; border-radius: 25px; border-right: 0px; height: 38px; width: 60%; background-color: #FFF; padding-right: 30px;">
            <button class="btn btn-outline btn-default" type="submit" style="color: #fff; background-color: #1e5394 !important; border-color: #1e5394; font-weight: 600; border-radius: 25px; border: 3px double #1e5394; opacity: unset; width: 180px;"><i class="fa fa-search"></i> <?=$this->lang->line('Freelancer')?></button>
          </form>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-sx-12" style="padding-left: 0px; padding-right: 0px">
          <form method="get" action="browse-troubleshooter" class="form-inline">
            <input class="form-control mr-sm-2" type="text" name="skill" placeholder="<?= $this->lang->line('Search for any skill'); ?>" aria-label="<?= $this->lang->line('Search for any skill'); ?>" style="margin-right: -35px; border: 0px solid #1e5394; border-radius: 25px; border-right: 0px; height: 38px; width: 60%; background-color: #FFF; padding-right: 30px;">
            <button class="btn btn-outline btn-default" type="submit" style="color: #fff; background-color: #1e5394 !important; border-color: #1e5394; font-weight: 600; border-radius: 25px; border: 3px double #1e5394; opacity: unset; width: 180px;"><i class="fa fa-search"></i> <?=$this->lang->line('Troubleshooter')?></button>
          </form>
        </div>
      </div>

      <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="margin-top: 30px;">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center">
          <a href="browse-project" class="btn btn-success form-control" style="border-radius: 18px;"><i class="fa fa-briefcase"></i> <?=$this->lang->line('Browse Jobs')?></a>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center">
          <a href="available-offers-list-view" class="btn btn-success form-control" style="border-radius: 18px;"><i class="fa fa-archive"></i> <?=$this->lang->line('Browse Offers')?></a>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center">
          <a href="post-customer-job" class="btn btn-warning form-control" style="border-radius: 18px;"><i class="fa fa-paper-plane-o"></i> <?=$this->lang->line('Post a Project')?></a>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center">
          <a href="post-troubleshoot-job" class="btn btn-warning form-control" style="border-radius: 18px;"><i class="fa fa-paper-plane-o"></i> <?=$this->lang->line('Troubleshoot a Job')?> </a>
        </div>
      </div>
    </div>
</div>