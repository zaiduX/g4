  <style>
	.avatar1 { width: 12.3%; max-height: 110px;  }   
	.cover { 
		<?php if($operator['cover_url'] == "NULL"): ?> 
			background-image: url(<?= $this->config->item('resource_url'). 'bg.jpg'; ?>) !important;
		<?php else: ?>
			background-image: url(<?= base_url($operator['cover_url']); ?>) !important;
		<?php endif; ?>
			background-position: center !important; background-repeat: no-repeat !important; background-size: cover !important; height: 150px;
	}
  .nav-tabs > li > a { border: none; }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Provider Profile'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('Provider Profile'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Manage Provider Profile Details'); ?></small>    
    </div>
  </div>
</div>

<div class="content content-boxed" style="max-width: 1200px !important;">
  <div class="row">
    <div class="hpanel hgreen" style="margin-bottom: 0px;">
      <div class="panel-body cover">
        <?php if($operator['avatar_url'] == "NULL"): ?>
          <img src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>" class="img-thumbnail m-b avatar1" alt="avatar" />
        <?php else: ?>
          <img src="<?= base_url($operator['avatar_url']); ?>" class="img-thumbnail m-b avatar1" alt="avatar" />
        <?php endif; ?>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-left: 0px; margin-bottom: -15px;">
      <div class="hpanel hblue">
        <div class="panel-body" style="padding-top: 0px;">
          <div class="row text-center">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">    
              <h4><?= ($operator['firstname'] != "NULL") ? $operator['firstname'] . ' ' . $operator['lastname'] : $this->lang->line('not_provided') ?></h4>
            </div>
          
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">    
              <h5><?= ($operator['company_name'] != "NULL") ? $operator['company_name'] : $this->lang->line('not_provided') ?></h5>
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">     
              <p><?= ($operator['address'] != "NULL") ? $operator['address'] : $this->lang->line('not_provided') ?></p>
            </div>

            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12 text-left">     
              <p><strong>
                <?php for($i=0; $i<(int)$user['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                <?php for($i=0; $i<(5-$user['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                <?=$user['ratings']?></strong> <font color="gray">(<?=$user['no_of_ratings']?>)</font>
              </p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 text-left">     
              <p>
                <?php if($operator['rate_per_hour'] != 'NULL') { ?>
                  <strong><?=$operator['currency_code']?> <?=$operator['rate_per_hour']?></strong> / hr
                <?php } ?>
              </p>
            </div>

            <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">     
              <button type="submit" class="btn btn-primary btn-outline form-control"><?=$this->lang->line('Contact')?></button>
              <p>
                <?php 
                $to_time = strtotime($user['last_login_datetime']);
                $from_time = strtotime(date('Y-m-d H:i:s'));
                  $min = round(abs($to_time - $from_time) / 60,2);
                  if($min > 10 ) { echo '<span class="text-danger"><i class="fa fa-circle-o"></i> '.$this->lang->line('Offline').'</span>'; }
                  else { echo '<span class="text-success"><i class="fa fa-circle"></i> '.$this->lang->line('Online').'</span>'; }
                ?>
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">    
              <h4><?=$this->lang->line('skills')?></h4>
            </div>
          	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <p>
                <?php if($skills['expert_skills'] != 'NULL') {
                    $sk = explode(',', $skills['expert_skills']);
                    for($i=0; $i < sizeof($sk); $i++) { ?>
                      <code style="margin: 5px; padding: 0px 5px; font-size: 100%;">
                        <?=trim($this->api->get_skill_master_details($sk[$i])['skill_name'])?>
                      </code>
                <?php } } ?>
                <?php if($skills['senior_skills'] != 'NULL') {
                    $sk = explode(',', $skills['senior_skills']);
                    for($i=0; $i < sizeof($sk); $i++) { ?>
                      <code style="margin: 5px; padding: 0px 5px; font-size: 100%;">
                        <?=trim($this->api->get_skill_master_details($sk[$i])['skill_name'])?>
                      </code>
                <?php } } ?>
                <?php if($skills['confirmed_skills'] != 'NULL') {
                    $sk = explode(',', $skills['confirmed_skills']);
                    for($i=0; $i < sizeof($sk); $i++) { ?>
                      <code style="margin: 5px; padding: 0px 5px; font-size: 100%;">
                        <?=trim($this->api->get_skill_master_details($sk[$i])['skill_name'])?>
                      </code>
                <?php } } ?>
                <?php if($skills['junior_skills'] != 'NULL') {
                    $sk = explode(',', $skills['junior_skills']);
                    for($i=0; $i < sizeof($sk); $i++) { ?>
                      <code style="margin: 5px; padding: 0px 5px; font-size: 100%;">
                        <?=trim($this->api->get_skill_master_details($sk[$i])['skill_name'])?>
                      </code>
                <?php } } ?>
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">    
              <h4><?=$this->lang->line('Insight')?></h4>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1"style="padding-top: 5px; padding-bottom: 5px;"><?=$this->lang->line('provider')?></a></li>
                <li class=""><a data-toggle="tab" href="#tab-2"style="padding-top: 5px; padding-bottom: 5px;"><?=$this->lang->line('buyer')?></a></li>
              </ul>
              <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                  <div class="panel-body" style="padding: 5px 0px 5px 0px; margin-bottom: 10px;">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                      <?=$this->lang->line('Projects worked on')?>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <?=$this->api->get_provider_job_counts($user['cust_id'])['total_job']?>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                      <?=$this->lang->line('Buyers worked with')?>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <?=$this->api->get_provider_customer_counts($user['cust_id'])['total_customers']?>
                    </div>
                  </div>
                </div>
                <div id="tab-2" class="tab-pane">
                  <div class="panel-body" style="padding: 5px 0px 5px 0px; margin-bottom: 10px;">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                      <?=$this->lang->line('Projects listed')?>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <?=$this->api->get_provider_my_job_counts($user['cust_id'])['total_job']?>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                      <?=$this->lang->line('Provider worked with')?>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <?=$this->api->get_customers_provider_counts($user['cust_id'])['total_providers']?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">    
              <?=$this->lang->line('Last active')?>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
              <?=$user['last_login_datetime']?>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">    
              <?=$this->lang->line('Response Time')?>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
              <?=$operator['response_time']?> hr.
            </div>
          </div>
          <div class="row" style="margin-top: 15px;">
            <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12 text-center">
              <?php if($operator['fb_url'] != 'NULL') { ?>
                <a title="<?=$this->lang->line('View on Facebook')?>" href="<?=$operator['fb_url']?>" target="_blank"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/facebook.png'; ?>" class="sc-icons" style="height: 36px; width: 36px;margin-right:10px;"></a>
              <?php } ?>
              <?php if($operator['twitter_url'] != 'NULL') { ?>
                <a title="<?=$this->lang->line('View on Twitter')?>" href="<?=$operator['twitter_url']?>" target="_blank"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/twitter.png'; ?>" class="sc-icons" style="height: 36px; width: 36px;margin-right:10px;"></a>
              <?php } ?>
              <?php if($operator['linkedin_url'] != 'NULL') { ?>
                <a title="<?=$this->lang->line('View on Linkedin')?>" href="<?=$operator['linkedin_url']?>" target="_blank"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/linkedin.png'; ?>" class="sc-icons" style="height: 36px; width: 36px;margin-right:10px;"></a>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding-right: 0px; margin-bottom: -15px;">
    	<div class="hpanel hblue" style="">
        <div class="panel-body" style="padding: 0px; border: none;">
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#portfolio"><i class="fa fa-image"></i> <?=$this->lang->line('Portfolios')?> (<?=sizeof($portfolios)?>)</a></li>
                <li class=""><a data-toggle="tab" href="#offers"><i class="fa fa-archive"></i> <?=$this->lang->line('Offers')?></a></li>
              </ul>
              <div class="tab-content">
                <div id="portfolio" class="tab-pane active">
                  <div class="panel-body" style="padding: 5px 0px 5px 0px; margin-bottom: 10px; border: none;">
                    <div class="lightBoxGallery">
                      <?php if(sizeof($portfolios) < 8) $size = sizeof($portfolios); else $size = 8; ?>
                      <?php for ($i=0; $i < $size ; $i++) { ?>
                        <a style="max-width: 150px; height: 150px;" class="thumbnail" href="<?=base_url($portfolios[$i]['attachement_url'])?>" title="<?=$portfolios[$i]['title']?>" data-gallery=""><img src="<?=base_url($portfolios[$i]['attachement_url'])?>"></a>
                      <?php } ?>
                    </div>
                    <?php if(sizeof($portfolios) > 8) { ?>
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                        <a class="btn btn-link"><?=$this->lang->line('View All')?> (<?=sizeof($portfolios)?>)</a>
                      </div>
                    <?php } ?>
                  </div>
                </div>
                <div id="offers" class="tab-pane">
                  <?php if(sizeof($offers) < 6) $size = sizeof($offers); else $size = 6; ?>
                  <?php for ($i=0; $i < $size ; $i++) { ?>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                      <div class="hpanel hgreen contact-panel">
                        <div class="panel-body text-center" style="padding: 10px; margin-top: 10px;">
                          <span class="pull-right">
                            <button title="<?= $this->lang->line('Add to favorite'); ?>" class="btn-link add_favorite" data-id="<?= $offers[$i]['offer_id'];?>"><i class="fa fa-heart-o"></i></button>
                          </span>
                          <img style="margin-left: 25px" alt="logo" class="img-circle m-b-xs" src="<?=base_url($operator['avatar_url'])?>">
                          <h5 class="m-b-xs"><a href=""> <?=$operator['company_name']?> </a></h5>
                          <div class="text-muted m-b-xs"><i class="fa fa-map-marker"></i> <?=$operator['address']?></div>
                          <h5 class="" style="overflow-wrap: break-word; line-height: 1.5; height: 4.5em; max-height: 4.5em; overflow: hidden; color: #3498db"><?=$offers[$i]['offer_title']?></h5>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left" style="padding-left: 5px; padding-right: 0px;">
                            <h6 class="text-muted font-bold m-b-xs" label="<?=$this->lang->line('User Rating and Reviews')?>"><i class="fa fa-thumbs-o-up"></i> <?=$offers[$i]['rating_count']?> [<?=$offers[$i]['review_count']?>] 
                            </h6>
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left" style="padding-left: 5px; padding-right: 0px;">
                            <div class="text-muted font-bold m-b-xs"><i class="fa fa-shopping-basket"></i> <?=$this->lang->line('Sales')?> <?=$offers[$i]['sales_count']?>
                            </div>
                          </div>
                        </div>

                        <div class="panel-footer contact-footer" style="padding-top: 0px; padding-bottom: 0px;">
                          <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 border-right" style="padding-left: 5px; padding-right: 0px;">
                              <div class="contact-stat">
                                <span><?=$offers[$i]['currency_code']?></span>
                                <strong><?=$offers[$i]['offer_price']?></strong>
                              </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-left: 5px; padding-right: 0px;">
                              <div class="contact-stat">
                                <button class="btn btn-sm btn-primary btn-outline"><?=$this->lang->line('View')?></button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                  <?php if(sizeof($offers) > 6) { ?>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                      <a class="btn btn-link"><?=$this->lang->line('View All')?> (<?=sizeof($offers)?>)</a>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="hpanel hblue">
        <div class="panel-body" style="padding: 0px; border: none;">
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#review"><i class="fa fa-image"></i> <?=$this->lang->line('reviews')?> (<?=sizeof($reviews)?>)</a></li>
                <li class=""><a data-toggle="tab" href="#purchases"><i class="fa fa-archive"></i> <?=$this->lang->line('Purchases')?></a></li>
              </ul>
              <div class="tab-content">
                <div id="review" class="tab-pane active">
                  <div class="panel-body no-padding" style="border: none;">
                    <div class="chat-discussion" style="height: auto">
                      <?php if(sizeof($reviews) < 8) $size = sizeof($reviews); else $size = 8; ?>
                      <?php for ($i=0; $i < $size ; $i++) { ?>
                        <div class="chat-message">
                          <img class="message-avatar" src="images/a1.jpg" alt="" >
                          <div class="message">
                            <a class="message-author" href="#"> Michael Smith </a>
                            <span class="message-date"> Mon Jan 26 2015 - 18:39:23 </span>
                            <span class="message-content">
                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                            </span>
                            <div class="m-t-md">
                              <a class="btn btn-xs btn-default"><i class="fa fa-thumbs-up"></i> Like </a>
                              <a class="btn btn-xs btn-success"><i class="fa fa-heart"></i> Love</a>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <div id="purchases" class="tab-pane">
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(".add_favorite").on('click', function(event) {   event.preventDefault();
      var id = $(this).data('id');
      swal({ 
        title: <?= json_encode($this->lang->line('are_you_sure'))?>, 
        text: <?= json_encode($this->lang->line('Offer will added to your favorite list.'))?>,
        type: "warning", 
        showCancelButton: true, 
        confirmButtonColor: "#DD6B55", 
        confirmButtonText: <?= json_encode($this->lang->line('yes'))?>, 
        cancelButtonText: <?= json_encode($this->lang->line('no'))?>, 
        closeOnConfirm: false, 
        closeOnCancel: false, 
      }, 
      function (isConfirm) { 
        if (isConfirm) { 
          $.post("<?=base_url('user-panel-services/add-offer-favorite')?>", {id: id}, 
          function(res){
            //console.log(res);
            if($.trim(res) == "success") {
              swal(<?=json_encode($this->lang->line('success'))?>, <?=json_encode($this->lang->line('Offer added in favorite list.'))?>, "success"); 
              setTimeout(function() { window.location.reload(); }, 2000); 
            } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, <?=json_encode($this->lang->line('Unable to add or offer already into your favorite list.'))?>, "error");  }  
          }); 
        } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, <?=json_encode($this->lang->line('Offer not added to your favorite list.'))?>, "error");  }  
      }); 
  });
</script>