<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel-services/dashboard-services'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
          <li><a href="<?= $this->config->item('base_url') . 'user-panel-services/services-wallet'; ?>"><span><?= $this->lang->line('e_wallet'); ?></span></a></li>
          <li class="active"><span><?= $this->lang->line('Available milestones'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"> <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Available milestones'); ?>  &nbsp;&nbsp;&nbsp;
      <small class="m-t-md"><?= $this->lang->line('User available milestone details'); ?></small> 
    </div>
  </div>
</div>
    
<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel">
        <div class="panel-body">
          <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
            <thead>
              <tr>
                <th><?= $this->lang->line('Milestone id'); ?></th>
                <th><?= $this->lang->line('From date'); ?></th>
                <th><?= $this->lang->line('To date'); ?></th>
                <th><?= $this->lang->line('Work hours'); ?></th>
                <th><?= $this->lang->line('amount'); ?></th>
                <th><?= $this->lang->line('withdraw'); ?></th>
              </tr>
            </thead>
            <tbody class="panel-body">
              <?php foreach ($milestone as $m) { ?>
                <tr>
                  <td><?=$m['milestone_id']?></td>
                  <td><?=$m['from_date']?></td>
                  <td><?=$m['to_date']?></td>
                  <td><?=$m['work_hours']?></td>
                  <!-- <td><?=$m['milestone_price']?></td> -->
                  <td> 
                    <?php
                      $gonagoo_commission_amount = round((($m['milestone_price']/100) * $advance_payment['gonagoo_commission']),2);
                      echo $m['milestone_price']-$gonagoo_commission_amount;
                    ?> 
                  </td>
                  <td>
                    <?php if($m['security_days_date'] == "NULL"){ ?>
                      <?php if($m['withdraw_status'] == "NULL"){ ?>
                        <form action="<?= $this->config->item('base_url') . 'user-panel-services/create-services-withdraw-request'; ?>" method="post">
                        <input type="hidden" name="account_id" value="<?=$account_id?>" />
                        <input type="hidden" name="currency_code" value="<?=$currency_code?>" />
                        <input type="hidden" name="job_id" value="<?=$m['job_id']?>" />
                        <input type="hidden" name="milestone_id" value="<?=$m['milestone_id']?>" />
                        <input type="hidden" name="withdraw_amount" value="<?=$m['milestone_price']-$gonagoo_commission_amount?>" />
                        <button type="submit" class="btn btn-outline btn-info" ><i class="fa fa-retweet"></i> <?= $this->lang->line('withdraw'); ?> </button>
                        </form>
                      <?php }elseif($m['withdraw_status'] != "NULL"){ ?>
                        <button type="button" class="btn btn-outline btn-warning" ><i class="fa fa-check"></i> <?= strtoupper($m['withdraw_status']); ?> </button>
                      <?php } ?>
                    <?php }elseif(strtotime($current_date) > strtotime($m['security_days_date'])){ ?>
                      <?php if($m['withdraw_status'] == "NULL"){ ?>
                        <form action="<?= $this->config->item('base_url') . 'user-panel-services/create-services-withdraw-request'; ?>" method="post">
                        <input type="hidden" name="account_id" value="<?=$account_id?>" />
                        <input type="hidden" name="currency_code" value="<?=$currency_code?>" />
                        <input type="hidden" name="job_id" value="<?=$m['job_id']?>" />
                        <input type="hidden" name="milestone_id" value="<?=$m['milestone_id']?>" />
                        <input type="hidden" name="withdraw_amount" value="<?=$m['milestone_price']-$gonagoo_commission_amount?>" />
                        <button type="submit" class="btn btn-outline btn-info" ><i class="fa fa-retweet"></i> <?= $this->lang->line('withdraw'); ?> </button>
                        </form>
                      <?php }elseif($m['withdraw_status'] != "NULL"){ ?>
                        <button type="button" class="btn btn-outline btn-warning" ><i class="fa fa-check"></i> <?= strtoupper($m['withdraw_status']); ?> </button>
                      <?php } ?>
                    <?php }elseif(strtotime($current_date)< strtotime($m['security_days_date'])){ ?>
                      <label style="color:red">
                        <?=$this->lang->line('Available on')." ".date('D, d M y H:m:s', strtotime($m['security_days_date']))?>                          
                      </label>
                    <?php } ?> 
                  </td>    
                </tr>
              <?php } ?>
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
</div>