<style>
  .custom_label { color: #175091; display: inline-block; margin-top: 0px; margin-bottom: 2px; font-size: 13px; }
  .error { margin-top: 0px !important; }
  /* Tabs panel */
  .tabbable-panel { padding: 10px; }
  /* Default mode */
  .tabbable-line > .nav-tabs { border: none; margin: 0px; }
  .tabbable-line > .nav-tabs > li { margin-right: 2px; }
  .tabbable-line > .nav-tabs > li > a { border: 0; margin-right: 0; color: #737373; }
  .tabbable-line > .nav-tabs > li > a > i { color: #a6a6a6; }
  .tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover { border-bottom: 4px solid #fbcdcf; }
  .tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a { border: 0; background: none !important; color: #333333; }
  .tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i { color: #a6a6a6; }
  .tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu { margin-top: 0px; }
  .tabbable-line > .nav-tabs > li.active { border-bottom: 4px solid #64ccf5; position: relative; }
  .tabbable-line > .nav-tabs > li.active > a { border: 0; color: #333333; }
  .tabbable-line > .nav-tabs > li.active > a > i { color: #404040; }
  .tabbable-line > .tab-content { margin-top: -3px; background-color: #fff; border: 0; border-top: 1px solid #eee; padding: 15px 0; }
  .portlet .tabbable-line > .tab-content { padding-bottom: 0; }
  /* Below tabs mode */
  .tabbable-line.tabs-below > .nav-tabs > li { border-top: 4px solid transparent; }
  .tabbable-line.tabs-below > .nav-tabs > li > a { margin-top: 0; }
  .tabbable-line.tabs-below > .nav-tabs > li:hover { border-bottom: 0; border-top: 4px solid #fbcdcf; }
  .tabbable-line.tabs-below > .nav-tabs > li.active { margin-bottom: -2px; border-bottom: 0; border-top: 4px solid #64ccf5; }
  .tabbable-line.tabs-below > .tab-content { margin-top: -10px; border-top: 0; border-bottom: 1px solid #eee; padding-bottom: 15px; }
  .nav-tabs > li, .nav-pills > li { float:none; display:inline-block; *display:inline; zoom:1; }
  .nav-tabs, .nav-pills { text-align:center; }
  @media (max-width: 991px){
  .details{
    margin-top: 20px;
    padding: 0px !important;
  }
}
</style>

<section class="p-t-30 m-t-0 background-grey">
  <?php if($this->session->flashdata()): ?>
    <section class="p-t-20 p-b-0 m-t-0 ">
      <div class="container">
        <div class="row">
          <?php if($this->session->flashdata('success')): ?>
          <div role="alert" class="alert alert-success text-center col-md-6 col-md-offset-3">
            <strong><span class="ai-warning">Success</span>!</strong> <?= $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('warning')):  ?>
          <div role="alert" class="alert alert-warning text-center col-md-6 col-md-offset-3">
            <strong><span class="ai-warning">Done</span>!</strong> <?= $this->session->flashdata('warning'); ?>
          </div>
        <?php else: ?>
          <div role="alert" class="alert alert-danger text-center col-md-6 col-md-offset-3">
            <strong><span class="ai-warning">Failed</span>!</strong> <?= $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        </div>
      </div>
    </section>
  <?php endif; ?>

  <div class="container">
  <div class="row">
    <div class="col-md-7" style="background-color: #FFF">
      <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#loginTab"><?= $this->lang->line('Existing user - Login'); ?></a></li>
            <li><a data-toggle="tab" href="#registerTab"><?= $this->lang->line('New user - Sign up'); ?></a></li>
          </ul>
        </div>
      </div>

      <div class="tab-content">
        <div id="loginTab" class="tab-pane fade in active">
          	<div class="col-md-10 col-md-offset-1">
          	<br />  
            <div class="row">
              <div class="col-md-12 text-center">
                <div class="row">
                  <div class="col-md-6 form-group">
                    <button id="btn_login" style="font-size: 12px" class="btn-social loginBtn loginBtn--facebook" onclick="fbLogin();"> <?= $this->lang->line('btn_Login with Facebook'); ?> </button>  
                  </div>
                  <div class="col-md-6 form-group">
                    <button id="btn_linkedIn" style="font-size: 12px" class="btn-social loginBtn loginBtn--linkedin" onclick="onLinkedInLoad_login()"> <?= $this->lang->line('btn_Login with LinkedIn'); ?> </button> 
                  </div>
                </div>
              </div>
            </div>

            <div class="seperator"  style="margin: 0px 0px 15px 0px !important"><span><?= $this->lang->line('or_separator'); ?></span></div>

            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <form method="post" action="<?= base_url().'confirm-login-provider' ?>" id="login_form" autocomplete="off" >
                  <input type="hidden" id="login_type_login" name="login_type" />
                  <input type="hidden" name="job_id" value="<?=$job_details['job_id'];?>" />
                  <input type="hidden" id="timezone_login" name="timezone" />
                  <div class="form-group">
                    <label class="sr-only"> Email</label>
                    <input type="email" class="form-control" placeholder="<?= $this->lang->line('enter_email_address'); ?> ..." id="email_login" name="email" />
                  </div>
                  <div class="form-group">
                    <label class="sr-only">Password</label>
                    <input type="password" class="form-control" placeholder="<?= $this->lang->line('enter_password'); ?> ..." id="password_login" name="password" />
                  </div>
                  <div class="form-inline form-group">
                    <div class="col-md-4 text-left">
                      <button class="button blue-dark" type="submit"> <?= $this->lang->line('btn_login'); ?> <i class="fa fa-unlock"></i></button>
                    </div>
                    <div class="col-md-8 text-right">
                      <div class="checkbox">
                          <label style="">
                            <input type="checkbox" name="remember_me"><?= $this->lang->line('remember_me'); ?>
                          </label>
                      </div>
                      <br />
                      <a href="<?= base_url('recovery/password'); ?>"><small><?= $this->lang->line('lost_pass'); ?></small></a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <br />
            </div>
        </div>
        <div id="registerTab" class="tab-pane fade">
          <div class="col-md-12">
            <br />
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="row">
                  <div class="col-md-6 text-center">
                    <button name="btn_social" class="btn-social loginBtn loginBtn--facebook" onclick="fbLogin_signup();"> <?= $this->lang->line('btn_fb'); ?> </button>                    
                  </div>
                  <div class="col-md-6 text-center">
                    <button name="btn_social" class="btn-social loginBtn loginBtn--linkedin" onclick="onLinkedInLoad()"> <?= $this->lang->line('btn_li'); ?> </button>                  
                  </div>
                </div>
              </div>
            </div>
            <div class="seperator"  style="margin: 10px 0px 10px 0px !important"><span><?= $this->lang->line('or_separator'); ?></span></div>
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <form action="<?= base_url();?>register-new-provider" method="post" id="register_form" autocomplete="off" style="line-height: 0.9;">
                  <input type="hidden" id="login_type" name="login_type" />
                  <input type="hidden" id="avatar_url" name="avatar_url" />
                  <input type="hidden" id="cover_url" name="cover_url" />
                  <input type="hidden" id="gender" name="gender" />
                  <input type="hidden" id="timezone" name="timezone" />
                  <input type="hidden" name="job_id" value="<?=$job_details['job_id'];?>" />
                  <input type="hidden" id="cpt" name="cpt" value="<?= $this->session->userdata('captcha_code');?>" />
                  <div class="row"> 
                    <div class="col-md-6 custom_label">
                      <label class=""><?= $this->lang->line('first_name'); ?></label>
                      <input type="text" class="form-control" placeholder="<?= $this->lang->line('first_name'); ?>" name="firstname" id="firstname" />
                      <label class="text-center" id="error_firstname"></label>
                    </div>  
                    <div class="col-md-6 custom_label">
                      <label class=""><?= $this->lang->line('last_name'); ?></label>
                      <input type="text" class="form-control" placeholder="<?= $this->lang->line('last_name'); ?>" name="lastname" id="lastname" />
                      <label class="text-center" id="error_lastname"></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 custom_label">
                      <label class=""><?= $this->lang->line('email'); ?></label>
                      <input type="email" class="form-control" placeholder="<?= $this->lang->line('email'); ?>" name="email" id="email" />
                      <label class="text-center" id="error_email" style="color: red;"></label>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-6 custom_label">
                      <label class=""><?= $this->lang->line('password'); ?></label>
                      <input type="password" class="form-control" placeholder="<?= $this->lang->line('password'); ?>" name="password" id="password" />
                      <label class="text-center" id="error_password"></label>
                    </div>  
                    <div class="col-md-6 custom_label">
                      <label class=""><?= $this->lang->line('confirm_password'); ?></label>
                      <input type="password" class="form-control" placeholder="<?= $this->lang->line('confirm_password'); ?>" name="repeatpassword" id="repeatpassword" />
                      <label class="text-center" id="error_repeatpassword"></label>
                    </div>  
                  </div> 
                  <div class="row"> 
                    <div class="col-md-4 custom_label">
                      <label class=""><?= $this->lang->line('select_country'); ?></label>
                      <select name="country_id" id="country_id" class="selectpicker" data-live-search="true" data-live-search-style="begins">
                        <option value=""><?= $this->lang->line('select_country'); ?></option>
                        <?php foreach ($countries as $c) :?>
                        <option value="<?= $c['country_id'];?>"><?=$c['country_name'];?></option>
                        <?php endforeach; ?>
                      </select>
                      <label class="text-center" id="error_country_id"></label>
                    </div>
                    <div class="col-md-4 custom_label">
                      <label class=""><?= $this->lang->line('country_code'); ?> <i class="fa fa-plus"></i></label>
                      <div class="left-inner-addon">
                        <input type="text" class="form-control" id="country_code" name="country_code" readonly />
                      	<label class="text-center" id="error_country_code"></label>
                      </div>
                    </div>
                    <div class="col-md-4 custom_label">
                      <label class=""><?= $this->lang->line('mobile_no'); ?></label>
                      <input type="text" class="form-control" placeholder="<?= $this->lang->line('mobile_no'); ?>" name="mobile_no" id="mobile_no">
                      <label class="text-center" id="error_mobile_no"></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4"><hr/></div>
                    <div class="col-md-4 text-center"><h6 class="text-info" style="margin-bottom: 0px;"><?= $this->lang->line('register_as'); ?></h6></div>
                    <div class="col-md-4"><hr/></div>
                  </div>
                  <div class="row">
                    <div class="col-md-5 col-md-offset-1 custom_label">
                      <input type="radio" name="register_as" id="as_individual" autocomplete="off" checked value="1" class="col-md-1 register_as">  
                      <label for="as_individual" class="control-label"> &nbsp; 
                        <i class="fa fa-user"></i> <span class=""><?= $this->lang->line('as_individual'); ?> </span>
                      </label>
                    </div>  
                    <div class="col-md-6 custom_label">
                      <input type="radio" name="register_as" id="as_company" autocomplete="off" value="0" class="col-md-1 register_as ">
                      <label for="as_company" class="control-label"> &nbsp; 
                        <i class="fa fa-users"></i> <span class=""><?=$this->lang->line('as_company');?> </span>
                      </label>
                    </div>   
                  </div>
                  <div class="row"> 
                    <div class="col-md-12">
                      <div class="company_div hidden">
                        <label for="company_name" class="custom_label"><?= $this->lang->line('compnay_name'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('compnay_name'); ?>" name="company_name" id="company_name" />
                      </div>
                    </div>                            
                  </div>
                  <div class="row">
                    <div class="col-md-4"> <hr/></div>
                    <div class="col-md-4 text-center"><h6 class="text-info"><?= $this->lang->line('account_type'); ?></h6></div>
                    <div class="col-md-4"> <hr/></div>
                  </div>
									<style>
										.box-buyer{
										  margin-top: 0px;
										  height: 80px;
										  border: 1px solid #999;
										  width: 100px;
										  cursor: pointer;
										  width: 100%;
										  text-align: center;
										  background-image: url("<?=base_url('resources/web/front_site/images/buyer.png')?>");
										  background-repeat: no-repeat;
										  background-size: 70px;
										  background-position-x: center;
										  background-position-y: center;
										}
										.box-seller{
										  margin-top: 0px;
										  height: 80px;
										  border: 1px solid #999;
										  width: 100px;
										  cursor: pointer;
										  width: 100%;
										  text-align: center;
										  background-image: url("<?=base_url('resources/web/front_site/images/seller.png')?>");
										  background-repeat: no-repeat;
										  background-size: 70px;
										  background-position-x: center;
										  background-position-y: center;
										}
										.box-both{
										  margin-top: 0px;
										  height: 80px;
										  border: 1px solid #999;
										  width: 100px;
										  cursor: pointer;
										  width: 100%;
										  text-align: center;
										  background-image: url("<?=base_url('resources/web/front_site/images/both.png')?>");
										  background-repeat: no-repeat;
										  background-size: 70px;
										  background-position-x: center;
										  background-position-y: center;
										}
										.icon {
										  display: block;
										  font-size: 60px;
										  font-size: 6rem;
										  height: 78px;
										  line-height: 80px;
										  background-color: transparent;
										  margin-bottom: 15px;
										  text-align: center;
										  -webkit-border-radius: 3px;
										  -moz-border-radius: 3px;
										  border-radius: 3px;
										  -webkit-background-clip: padding-box;
										  -moz-background-clip: 'padding';
										  background-clip: padding-box;
										  width: 100%;
										}
									</style>
                  <div class="row"> 
                    <div class="col-md-4 custom_label text-center">
                    	<div class="box-buyer" id="icon_buyer"></div>
                    	<label id="lbl_buyer" style="margin-top: 10px;"><?= $this->lang->line('buyer'); ?></label>
                    	<input type="hidden" name="buyer" id="buyer" value="0" />
                    </div>                      
                    <div class="col-md-4 custom_label text-center">
                    	<div class="box-seller" id="icon_seller"></div>
											<label id="lbl_seller" style="margin-top: 10px;"><?= $this->lang->line('seller'); ?></label>
											<input type="hidden" name="seller" id="seller" value="0" />
                    </div>
                    <div class="col-md-4 custom_label text-center">
	                    <div class="box-both" id="icon_both"></div>
	                    <label id="lbl_both" style="margin-top: 10px;"><?= $this->lang->line('both'); ?></label>
	                    <input type="hidden" name="both" id="both" value="0" />
	                	</div>
                    <label class="text-center" id="error_account_type"></label>
                    <font class="text-mute" style="padding-left: 8px;margin-top: -20px; font-size: 12px;">
	                    <span class="fa fa-angle-double-right"></span><?= $this->lang->line('account_type_line1'); ?><br/>
	                    <span class="fa fa-angle-double-right" style="padding-left: 15px; margin-bottom:10px;"></span><?= $this->lang->line('account_type_line2'); ?>
                    </font>                          
                  </div>
                  <div class="row">
                      <div class="col-md-4 custom_label">
                        <label class=""><?= $this->lang->line('are_you_human'); ?></label>                          
                        <img src="<?= $this->config->item('resource_url').'captcha/'.$data['image']; ?>" alt="<?= $data['image']; ?>" />
                      </div>
                      <div class="col-md-8 custom_label">
                        <label class=""><?= $this->lang->line('txt_captcha'); ?></label>                          
                        <input type="text" name="captcha" id="captcha" class="form-control" placeholder="<?= $this->lang->line('txt_captcha'); ?>" />
                          <label class="text-center" id="capcha_error"></label>
                      </div>                          
                  </div>
                  <div class="row">
                      <div class="col-md-6 col-md-offset-3 custom_label">
                        <button id="btn_submit" class="button blue-dark form-control" type="submit" style="border-radius: 3px;"><?= $this->lang->line('btn_signup'); ?></button>
                      </div>
                  </div> 
                </form>
              </div>
            </div>
            <br />
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12 details" style="display: block;padding-left: 20px;">


        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #FFF">
         
          <h3 style="text-align: center;"><?=ucfirst($job_details['job_title']);?></h3>
          <hr style="margin-bottom: 10px;margin-top: 10px;">

          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right:0px;padding-left:0px">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
              <?=$this->lang->line('Ending proposal date')?> <br/>
              <h4><strong>
                <?=date("d  F" , strtotime("+1 month" , strtotime( $job_details['cre_datetime'])));?>
              </strong></h4>
            </div>
            <?php if($job_details['job_type']==0){  ?> 
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
              <?=$job_details['work_type']?><br/>
              <h4><strong>
                <?=($job_details['work_type']!='Per Hour')?$job_details['currency_code'].' '.$job_details['budget']:$job_details['currency_code'].' '.$job_details['budget']."<small>/hr</small>"?>
              </strong></h4>
            </div>
          <?php }else{ ?>
             <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center"><?=$this->lang->line('Troubleshoot');?>
              <?php if($job_details['immediate']){
                echo $this->lang->line('On Date');}else{
                   echo $this->lang->line('On');} ?><br/>
              <h4><strong>
                <?=($job_details['immediate'])? date("d M Y" , strtotime($job_details['complete_date'])) :$this->lang->line('Immediate');?>
              </strong></h4>
            </div>

          <?php } ?>
          </div>
          <hr style="margin-bottom: 10px;margin-top: 10px;">




          <div class="row" style="padding-left: 0px;margin-bottom: 10px;">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;">
                <i class="fa fa- fa-clock-o"></i> <?= $this->lang->line('posted on'); ?>: <?=date('D, d M y', strtotime($job_details['cre_datetime']))?><br>
                <i class="fa fa-map-marker"></i>&nbsp;<?=$job_details['location_type']?><br>
                <i class="fa fa-dot-circle-o"></i> <?=$this->lang->line('Proposals')." ".$job_details['proposal_counts']?><br>
                <strong style="color: #3498db;"><i class="fa fa-gears"></i> <?= $this->lang->line('status'); ?>: <?= strtoupper(str_replace('_', ' ', $job_details['job_status']))?></strong>
              </div>
            </div>
          </div>
        </div>


        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #FFF">
                    <hr style="margin-bottom: 10px;">
          

          <h3><?=$this->lang->line('description')?></h3>
          <lable><?=$job_details['job_desc']?></lable><br/>
          <h6><strong><?=$this->lang->line('Attachments')?></strong></h6>
                 
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0;margin-bottom: 20px; display: inline-flex;">
              
              <?php if($job_details["attachment_1"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 1"?>" target="_blank" href="<?=base_url($job_details["attachment_1"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_2"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 2"?>" target="_blank" href="<?=base_url($job_details["attachment_2"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_3"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 3"?>" target="_blank" href="<?=base_url($job_details["attachment_3"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_4"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 4"?>" target="_blank" href="<?=base_url($job_details["attachment_4"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_5"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 5"?>" target="_blank" href="<?=base_url($job_details["attachment_5"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_6"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 6"?>" target="_blank" href="<?=base_url($job_details["attachment_6"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
            </div>
            <hr style="margin-bottom: 10px;">

 
        



        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0;padding-right: 0px;padding-top: 20px;padding-bottom: 20px;background-color: #FFF">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($job_customer_profile['avatar_url'])?>">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
              <h4 style="color: #3498db"><strong><?=$job_customer_profile['firstname'].' '.$job_customer_profile['lastname']?></strong></h4>
              <h5><i class="fa fa-map-marker"></i> <?=$this->api->get_country_name_by_id($job_customer_profile['country_id'])?></h5>
              <h5 style="color:#3ceb3f">
                <?php
                    $to_time = strtotime($job_customer_profile['last_login_datetime']);
                    date_default_timezone_set(ini_get('date.timezone'));
                    $from_time = strtotime(date('Y-m-d h:i:s'));
                  $min = round(((abs($to_time - $from_time) / 60) - 270),2);
                  if( $min > 10 ) { echo '<span class="text-danger"><i class="fa fa-circle"></i> '.$this->lang->line('Offline').'</span>'; }
                  else { echo '<span class="text-success" style="color: #3ceb3f;"><i class="fa fa-circle"></i> '.$this->lang->line('Online').'</span>'; }
                ?>
              </h5>
            </div>
          </div>
          </div>
      </div>   
    </div>  
  </div>

</section>


<!-- Login script -->
<script>
  var social_media = 0;
  var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  $("#timezone_login").val(timezone);
  
  $( document ).ready(function() {
    $("#icon_buyer").addClass('text-info').css("background-color","#dfe0e3");
    $("#buyer").val('1'); $("#seller, #both").val('0');
    $("#icon_seller, #icon_both").removeClass('text-info').css("background-color","transparent");
  });

  function onLinkedInLoad_login() {
    LinkedINAuth();
    IN.Event.on(IN, "auth", function () { getProfileData_login(); });
    /*IN.Event.on(IN, "auth", getProfileData_login());*/
    // IN.Event.on(IN, "logout", function () { onLinkedInLogout(); });
  }

  function onSuccess_login(data) {
    // console.log(data);
    $("#email_login").val(data["emailAddress"]);
    $("#password_login").val(data["id"]);
    $("#login_type_login").val("linkedin");
    $("#login_form").get(0).submit();
  }
  function onError(error) { /*console.log(error);*/ }
</script>

<!-- facebook -->
<script>
  function fbLogin() {

    FB.login(function (response) {      
      if (response.authResponse) {  setFbUserData(); } 
    },{scope: 'email'});        
  }

  function setFbUserData() {
    FB.api('/me', {locale: 'en_US', fields: 'id,email'}, function (data) {    
    console.log(data['email']);
      $("#email_login").val(data["email"]);
      $("#password_login").val(data["id"]);
    
      $("#login_type_login").val("facebook");
      $("#login_form").get(0).submit();
    });
  }
</script>
<!-- End - Facebook -->
<!-- Login script -->


<!-- sign-up script -->
<script>
  $(function(){
    $("#country_id").on('change', function(event) { event.preventDefault();
      var id = $.trim($(this).val());
      var countries = <?= json_encode($countries); ?>;
      if(id != "" ) {
        $.each(countries, function(i, v){
          if(id == v['country_id']){  $("#country_code").val((v['country_phonecode'])); }
        });
      } else { $("#country_code").val(''); }
    });
    $(".register_as").on('change', function(event) {  event.preventDefault();
      var as = $(this).val();
      if(as == 0){  $(".company_div").removeClass('hidden');  }
      else {  $(".company_div").addClass('hidden'); $("#company_name").val(''); }
    });
    $("#icon_buyer, #lbl_buyer").on('click', function(event) {  event.preventDefault();
      $(this).addClass('text-info').css("background-color","#dfe0e3");
      $("#buyer").val('1'); $("#seller, #both").val('0');
      $("#icon_seller, #icon_both").removeClass('text-info').css("background-color","transparent");
    });
    $("#icon_seller, #lbl_seller").on('click', function(event) {  event.preventDefault();
      $(this).addClass('text-info').css("background-color","#dfe0e3");
      $("#seller").val('1'); $("#buyer, #both").val('0');
      $("#icon_buyer, #icon_both").removeClass('text-info').css("background-color","transparent");
    });
    $("#icon_both, #lbl_both").on('click', function(event) {  event.preventDefault();
      $(this).addClass('text-info').css("background-color","#dfe0e3");
      $("#both").val('1'); $("#seller, #buyer").val('0');
      $("#icon_seller, #icon_buyer").removeClass('text-info').css("background-color","transparent");
    });
    $("#btn_submit").on('click', function(event) {event.preventDefault();
      var buyer = $("#buyer").val();
      var seller = $("#seller").val();
      var both = $("#both").val();
      var firstname = $("#firstname").val();
      var lastname = $("#lastname").val();
      var email = $("#email").val();
      var password = $("#password").val();
      var repeatpassword = $("#repeatpassword").val();
      var country_id = $("#country_id").val();
      var country_code = $("#country_code").val();
      var mobile_no = $("#mobile_no").val();
      var captcha = $("#captcha").val();
      var err = <?= json_encode($this->lang->line('error_account_type'));?>;
      var err_firstname = <?= json_encode($this->lang->line('enter_first_name'));?>;
      var err_lastname = <?= json_encode($this->lang->line('enter_last_name'));?>;
      var err_email = <?= json_encode($this->lang->line('enter_email_address'));?>;
      var err_password = <?= json_encode($this->lang->line('enter_new_password'));?>;
      var err_password_alpha_numeric = <?= json_encode($this->lang->line('A password should have one alphabet and a number.'));?>;
      var err_password_length = <?= json_encode($this->lang->line('A password should have 6 characters.'));?>;
      var err_repeatpassword = <?= json_encode($this->lang->line('enter_rewrite_password'));?>;
      var err_country_id = <?= json_encode($this->lang->line('select_country'));?>;
      var err_country_code = <?= json_encode($this->lang->line('country_code'));?>;
      var err_mobile_no = <?= json_encode($this->lang->line('enter_mobile_number'));?>;
      var err_captcha = <?= json_encode($this->lang->line('enter_captcha'));?>;

      //remove previous errors
      $("#error_firstname").css({"display":"inline-block"}).html('&nbsp;');
      $("#error_lastname").css({"display":"inline-block"}).html('&nbsp;');
      $("#error_password").css({"display":"inline-block"}).html('&nbsp;');
      $("#error_repeatpassword").css({"display":"inline-block"}).html('&nbsp;');
      $("#error_country_id").css({"display":"inline-block"}).html('&nbsp;');
      $("#error_mobile_no").css({"display":"inline-block"}).html('&nbsp;');

      var pass_digits = password.replace(/\D/g, '').length;
      var rx = /[a-zA-Z]/gi;
      var m = password.match(rx);
      if (m) { pass_alpha = m.length; } else { pass_alpha = 0; }
      
      if(social_media != 0){
        if (firstname =='') {
          $("#error_firstname").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_firstname) +"<br/>";
          $( "#firstname" ).focus();
        } else if (lastname =='') {
          $("#error_lastname").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_lastname) +"<br/>";
          $( "#lastname" ).focus();
        } else if (email =='') {
          $("#error_email").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_email) +"<br/>";
          $( "#email" ).focus();
        } else if (password =='') {
          $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password) +"<br/>";
          $( "#password" ).focus();
        } else if ( false ) {
          $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password_alpha_numeric) +"<br/>";
          $( "#password" ).focus();
        } else if ( password.length < 6 ) {
          $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password_length) +"<br/>";
          $( "#password" ).focus();
        } else if (repeatpassword =='') {
          $("#error_repeatpassword").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_repeatpassword) +"<br/>";
          $( "#repeatpassword" ).focus();
        } else if (country_id =='') {
          $("#error_country_id").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_country_id) +"<br/>";
          $( "#country_id" ).focus();
        } else if (country_code =='') {
          $("#error_country_code").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_country_code) +"<br/>";
          $( "#country_code" ).focus();
        } else if (mobile_no =='') {
          $("#error_mobile_no").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_mobile_no) +"<br/>";
          $( "#mobile_no" ).focus();
        } else if (captcha =='') {
          $("#capcha_error").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_captcha) +"<br/>";
          $( "#captcha" ).focus();
        } else if(buyer =='0' && seller == '0' && both == '0'){
          $("#error_account_type").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err) +"<br/>";
        } else { 
          $("#error_account_type").html("");  
          $("#register_form").get(0).submit();
        }
      } else {
        if (firstname =='') {
          $("#error_firstname").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_firstname) +"<br/>";
          $( "#firstname" ).focus();
        } else if (lastname =='') {
          $("#error_lastname").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_lastname) +"<br/>";
          $( "#lastname" ).focus();
        } else if (email =='') {
          $("#error_email").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_email) +"<br/>";
          $( "#email" ).focus();
        } else if (password =='') {
          $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password) +"<br/>";
          $( "#password" ).focus();
        } else if ( pass_digits < 1 || pass_alpha < 1 ) {
          $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password_alpha_numeric) +"<br/>";
          $( "#password" ).focus();
        } else if ( password.length < 6 ) {
          $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password_length) +"<br/>";
          $( "#password" ).focus();
        } else if (repeatpassword =='') {
          $("#error_repeatpassword").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_repeatpassword) +"<br/>";
          $( "#repeatpassword" ).focus();
        } else if (country_id =='') {
          $("#error_country_id").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_country_id) +"<br/>";
          $( "#country_id" ).focus();
        } else if (country_code =='') {
          $("#error_country_code").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_country_code) +"<br/>";
          $( "#country_code" ).focus();
        } else if (mobile_no =='') {
          $("#error_mobile_no").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_mobile_no) +"<br/>";
          $( "#mobile_no" ).focus();
        } else if (captcha =='') {
          $("#capcha_error").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_captcha) +"<br/>";
          $( "#captcha" ).focus();
        } else if(buyer =='0' && seller == '0' && both == '0'){
          $("#error_account_type").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err) +"<br/>";
        } else { 
          $("#error_account_type").html("");  
          $("#register_form").get(0).submit();
        }
      }
    });
  });
</script>

<!-- LinkedIn -->
<script>
  var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  $("#timezone").val(timezone);
  
  function onLinkedInLoad() {  
    LinkedINAuth();
    IN.Event.on(IN, "auth", function () { getProfileData(); });
  }
  
  function onSuccess(data) {
    $("#email").val(btoa(data["id"])+'@linkedin.com').attr('readonly',true);
    $("#password,#repeatpassword").val(data["id"]).attr('readonly',true);
    $("#login_type").val("linkedin");
    $("#avatar_url").val(data["pictureUrl"]);
    $("#firstname").val(data["firstName"]);
    $("#lastname").val(data["lastName"]);
    social_media = 1;
  }
  function onError(error) { /*console.log(error);*/ }
</script>
<!-- End - LinkedIn -->

<!-- facebook -->
<script>
  function fbLogin_signup() {
    FB.login(function (response) {      
      if (response.authResponse) {  setFbUserData_signup(); } 
    },{scope: 'email'});        
  }

  function setFbUserData_signup() {
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,gender,picture,cover'}, function (data) {    
      console.log(data);      
      $("#email").val(btoa(data["id"])+'@facebook.com').attr('readonly',true);
      $("#password,#repeatpassword").val(data["id"]).attr('readonly',true);
      $("#login_type").val("facebook");
      $("#avatar_url").val(data['picture']['data']['url']);
      //$("#cover_url").val(data['cover']['source']);
      $("#cover_url").val("NULL");
      $("#firstname").val(data["first_name"]);
      $("#lastname").val(data["last_name"]);
      $("#gender").val(data["gender"]);
      social_media = 1;   
    });
  }
</script>
<!-- End - Facebook -->

<script>
  $('#email').focusout(function() {
    var email = $('#email').val();
    $.ajax({
        type: "POST", 
        url: "check-email-exists", 
        data: { user_email:email },
        dataType: "json",
        success: function(res){   
           console.log(res);
           if(res == 1) { $('#error_email').text("<?=$this->lang->line('email_exists')?>"); $('#error_email').focus(); }
           else { $('#error_email').text(""); }
        },
        beforeSend: function(){ },
        error: function(){ }
    });
  });
</script>
<!-- sign-up script -->