<section class="p-t-30 m-t-0 background-grey" style="background: url(<?= $this->config->item('resource_url') . 'web/front_site/images/bg.jpg'; ?>);">

	<?php if($this->session->flashdata('error') != NULL): ?>
	<section class="p-t-20 p-b-0 m-t-0 ">
		<div class="container">
			<div class="row">
				<div role="alert" class="alert alert-danger text-center col-md-6 col-md-offset-3">
					<strong><span class="ai-warning">Failed</span>!</strong> <?= $this->session->flashdata('error'); ?>
				</div>
			</div>
		</div>
	</section>
	<?php elseif($this->session->flashdata('success') != NULL): ?>
		<section class="p-t-20 p-b-0 m-t-0 ">
		<div class="container">
			<div class="row">
				<div role="alert" class="alert alert-success text-center col-md-6 col-md-offset-3">
					<strong><span class="ai-warning">Success</span>!</strong> <?= $this->session->flashdata('success'); ?>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="panel panel-register-box"><br />
						<div class="hr-title hr-long center"><abbr>Password Recovery</abbr> </div>
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<form id="resend_pass" action="<?= base_url('resend-password'); ?>" method="post" autocomplete="off">
									<div class=" col-md-12">
										<div class="form-group">
											<label class="sr-only">Email</label>
											<input type="email" class="form-control" placeholder="Enter Registered Email Address" name="email"/>
										</div>													
										<div class="pull-left">
											<button id="btn_recovery" class="button blue-dark" type="submit">&nbsp; Send &nbsp;</button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<hr class="space">
					</div>
				</div>
			</div>			
		</div>
	</section>

