<style type="text/css">
.container {
  width: 60%;
  margin:0 auto;
  padding: 20px 0
}
.drop-shadow {
    position:relative;
    width:100%;
    padding:3em 1em;
    margin:2em 10px 4em;
    background:#fff;
    -webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
       -moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
      box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
}

.drop-shadow:before,
.drop-shadow:after {
    content:"";
    position:absolute;
    z-index:-2;
}

.drop-shadow p {
    font-size:1.5em;
    font-weight:300;
    text-align:center;
}

.lifted {
    -moz-border-radius:4px;
   border-radius:4px;
}

.lifted:before,
.lifted:after {
    bottom:15px;
    left:10px;
    width:50%;
    height:20%;
    max-width:300px;
    max-height:100px;
    -webkit-box-shadow:0 15px 10px rgba(0, 0, 0, 0.7);
       -moz-box-shadow:0 15px 10px rgba(0, 0, 0, 0.7);
      box-shadow:0 15px 10px rgba(0, 0, 0, 0.7);
    -webkit-transform:rotate(-3deg);
       -moz-transform:rotate(-3deg);
  -ms-transform:rotate(-3deg);
   -o-transform:rotate(-3deg);
      transform:rotate(-3deg);
}

.lifted:after {
    right:10px;
    left:auto;
    -webkit-transform:rotate(3deg);
       -moz-transform:rotate(3deg);
  -ms-transform:rotate(3deg);
   -o-transform:rotate(3deg);
      transform:rotate(3deg);
}


#attachment, .attachment { 
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px; 
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0); 
  }
  .entry:not(:first-of-type) {
    margin-top: 10px;
  }
</style>


<div class="normalheader small-header" style="background-color: #f1f3f6;">
<span ><h6 style="color: #f1f3f6; font-size: 15px;"> &nbsp; </h6><span>
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px; background-color: white;">
    <h3 class="">  <i class="fa fa-wpforms fa-2x"></i> <strong><?= $this->lang->line('Post A Job'); ?> </strong> </h3>
    </div>
    <span ><h6 style="color: #f1f3f6; font-size: 15px;"> &nbsp; </h6><span>

  </div>
</div>
 
<div class="content">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
    <div class="hpanel hblue">
      <form action="<?= base_url('post-project-login'); ?>" method="get" class="form-horizontal" id="cutomer_job_post_form" enctype="multipart/form-data">
        <div class="panel-body">
          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <label class=""><?= $this->lang->line('WHAT DO YOU NEED TO GET DONE?'); ?> </label>
              <input id="job_title" name="job_title" type="text" class="form-control" placeholder="<?= $this->lang->line('e.g I need a professional website design'); ?>" autocomplete="none"> <br/>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding: 0px;">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <label class=""><?= strtoupper($this->lang->line('Category')); ?> </label>
                <select class="select2 form-control" name="category" id="category">
                    <option value=""><?= $this->lang->line('select'); ?></option>
                    <?php foreach ($category_types as $c ): ?>
                      <option value="<?= $c['cat_type_id']?>"><?= $c['cat_type']?></option>
                    <?php endforeach; ?>
                </select><br/>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <label class=""><?= strtoupper($this->lang->line('Sub Category')); ?> </label>
                <select class="select2 form-control" name="sub_category" id="sub_category" disabled>
                    <option value=""><?= $this->lang->line('select'); ?></option>
                </select><br/>
              </div>
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <label class=""><?= strtoupper($this->lang->line('desc')); ?> </label>
              <textarea style="resize:none"  rows="5" id="description" name="description" type="textarea" class="form-control" placeholder="<?= $this->lang->line('Provide a more detailed description to help you get better proposal'); ?>" autocomplete="none"></textarea> <br/>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-right:0px; padding-left: 0px;">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-right:0px; padding-left: 0px;">
             <!--    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <label class=""><?= strtoupper($this->lang->line('Currency')); ?> </label>
                  <input type="text" id="currency_signz" name="currency_signz" value="<?=$currency_details['currency_sign']?>" class="form-control" disabled="true" autocomplete="none"> <br/>
                  <input type="hidden" id="currency_sign" name="currency_sign" value="<?=$currency_details['currency_sign']?>" class="form-control">
                  <input type="hidden" id="currency_id" name="currency_id" value="<?=$currency_details['currency_id']?>" class="form-control">
                </div> -->
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                  <label class=""><?= strtoupper($this->lang->line('work type')); ?> </label>
                  <select class="select2 form-control" name="work_type" id="work_type">
                      <option value="fixed price"><?= $this->lang->line('Fixed Price'); ?></option>
                      <option value="per hour"><?= $this->lang->line('Per Hour'); ?></option>
                  </select><br/>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                  <label class="bgtfxd"><?= strtoupper($this->lang->line('budget')); ?> </label>
                  <label class="bgtpr hidden"><?= strtoupper($this->lang->line('budget per hour')); ?> </label>
                  <input id="budget" name="budget" type="text" class="form-control" autocomplete="none"> <br/>
                </div>
              </div>
             <!--  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px; padding-left: 0px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <label class=""><?= strtoupper($this->lang->line('Upload samples'))." (".$this->lang->line('Optional').")"; ?> </label>
                  <div class="control-group" id="upload_sample">
                    <div class="controls">
                      <div class="entry input-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4"style="width:unset;">
                        <input class="btn btn-primary image_url" name="image_url[]" type="file" accept="image/png,image/gif,image/jpeg" style="font-size: 12px;">
                        <span class="input-group-btn">
                          <button class="btn btn-success btn-add" type="button" style="margin-left: 2px !important;">
                            <span class="fa fa-plus"></span>
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
            <div class="exprdt col-xl-12 col-lg-12 col-md-12 col-sm-12 hidden" style="padding-right:0px; padding-left: 0px;">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px; padding-left: 0px;">
                <div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-bottom: 18px;">
                  <label><?= strtoupper($this->lang->line('expiry_date'))." (".$this->lang->line('Optional').")";?> </label>
                  <div class="input-group date" data-provide="datepicker" data-date-start-date="<?=date("m/d/Y")?>" data-date-end-date="">
                    <input type="text" class="form-control" id="expiry_date" name="expiry_date" autocomplete="none" value="" />
                    <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                    </div>
                  </div>                      
                </div>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px; padding-left: 0px;">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-right:0px ">
                  <label><?= strtoupper($this->lang->line('Billing period'))?> </label>
                  <select class="select2 form-control" name="billing_period" id="billing_period">
                    <option value=""><?= $this->lang->line('select'); ?></option>
                    <option value="Daily"><?= $this->lang->line('Daily'); ?></option>
                    <option value="Weekly"><?= $this->lang->line('Weekly'); ?></option>
                    <option value="Monthly"><?= $this->lang->line('Monthly'); ?></option>
                    <option value="Other"><?= $this->lang->line('other'); ?></option>
                  </select><br/>
                </div>
              </div>
            </div>
           
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
             <br/> <input type="submit" id="post_project" value="<?=strtoupper($this->lang->line('Post project'))?>" class="btn btn-success">
            </div>
          </div>
        
          <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
            <div class="drop-shadow lifted">
              <h3 class="text-center" style="margin-top:-20px;"><?=$this->lang->line('USEFUL TIPS')?></h3>
              <ul> 
                <li><?=$this->lang->line('useful tips 1')?></li><br>
                <li><?=$this->lang->line('useful tips 2')?></li><br>
                <li><?=$this->lang->line('useful tips 3')?></li><br>
              </ul>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  </div>
</div>


              

<script>
  $(function() {
    $(document).on('click', '.btn-add', function(e) {
      e.preventDefault();
      var numItems = $('.entry').length;
      if(numItems <= 5) {
        var controlForm = $('.controls:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
        .removeClass('btn-add').addClass('btn-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="fa fa-minus"></span>');
      } else { swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('You can upload upto 6 files only.')?>","warning"); }
      }).on('click', '.btn-remove', function(e) {
        $(this).parents('.entry:first').remove();
      e.preventDefault();
      return false;
    });
  });
</script>
<script>
  $(".btn_show").click(function(){
    $("#advance_option").slideToggle();
    $(".btn_show").addClass("hidden");
    $(".btn_hide").removeClass("hidden");
  });
  $(".btn_hide").click(function(){
    $("#advance_option").slideToggle();
    $(".btn_show").removeClass("hidden");
    $(".btn_hide").addClass("hidden");
  });
</script>
<script>
  $("#category").on('change', function(event) { event.preventDefault();
    $('#sub_category').empty();
    $('#question1,#question2,#question3,#question4,#question5').empty();
    var d = <?= json_encode($this->lang->line("no sub category found"))?>;
    var cat_type_id = $(this).val();

    $.post('get-categories-by-type', {type_id: cat_type_id}, function(sub_category) {
      //console.log(sub_category);
      sub_category = $.parseJSON(sub_category);
      if(sub_category.length > 0 ){
        $('#sub_category').attr('disabled', false);
          $('#sub_category').empty(); 
          $('#sub_category').append('<option value=""><?= $this->lang->line("select")?></option>');
          $('#sub_category').append('<option value=""><?= $this->lang->line("select")?></option>');
          $.each(sub_category, function(){    
            $('#sub_category').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');              
          });
          $('#sub_category').focus();
      }else{
          $('#sub_category').attr('disabled', true);
          $('#sub_category').empty();
          $('#sub_category').append('<option value="0">'+d+'</option>');
        }
    });

    $.post('get-questions-freelancer', {cat_type_id: cat_type_id}, function(questions) {
      //console.log(questions);
      questions = $.parseJSON(questions);
      if(questions.length > 0 ){
        $('#question1,#question2,#question3,#question4,#question5').attr('disabled', false);
          $('#question1,#question2,#question3,#question4,#question5').empty(); 
          $('#question1,#question2,#question3,#question4,#question5').append('<option value=""><?= $this->lang->line("select")?></option>');
          $.each(questions, function(){    
              $('#question1,#question2,#question3,#question4,#question5').append('<option value="'+$(this).attr('question_title')+'">'+$(this).attr('question_title')+'</option>');              
          });
          $('#question1,#question2,#question3,#question4,#question5').append('<option value="Create your own question"><?= $this->lang->line("Create your own question"); ?></option>');
          //$('#question1').focus();
      }else{
          $('#question1,#question2,#question3,#question4,#question5').attr('disabled', true);
          $('#question1,#question2,#question3,#question4,#question5').empty();
          $('#question1,#question2,#question3,#question4,#question5').append('<option value="0">'+d+'</option>');
        }
    });
  });
</script>
<script>
  $("#work_from").on('change', function(event) {
    event.preventDefault();
    var w_f = $(this).val();
    if(w_f == "Remotely (Preferred Country)"){
      $('#preffered_country').css('display', 'block');
      $('#on_site').css('display', 'none');
    }
    if(w_f == "On-site (specific location)"){
      $('#preffered_country').css('display', 'none');
      $('#on_site').css('display', 'block');
    }
    if(w_f == "Remotely (anywhere)"){
      $('#preffered_country').css('display', 'none');
      $('#on_site').css('display', 'none');
    }
  }); 
</script>

<script>
  $(".btn-add-q1").click(function(){
    $('#q2').css('display', 'block');
    $('.btn-add-q1').hide();
    $('.btn-delete1').hide();
  });

  $(".btn-delete2").click(function(){
    $('#q2,#q2_other').css('display', 'none');
    $('.btn-add-q1').show();
    $('.btn-delete1').show();
  });

  $(".btn-add-q2").click(function(){
    $('#q3').css('display', 'block');
    $('.btn-add-q2').hide();
    $('.btn-delete2').hide();
  });

  $(".btn-delete3").click(function(){
    $('#q3,#q3_other').css('display', 'none');
    $('.btn-add-q2').show();
    $('.btn-delete2').show();
  });

  $(".btn-add-q3").click(function(){
    $('#q4').css('display', 'block');
    $('.btn-add-q3').hide();
    $('.btn-delete3').hide();
  });

  $(".btn-delete4").click(function(){
    $('#q4,#q4_other').css('display', 'none');
    $('.btn-add-q3').show();
    $('.btn-delete3').show();
  });

  $(".btn-add-q4").click(function(){
    $('#q5').css('display', 'block');
    $('.btn-add-q4').hide();
    $('.btn-delete4').hide();
  });

  $(".btn-delete5").click(function(){
    $('#q5,#q5_other').css('display', 'none');
    $('.btn-add-q4').show();
    $('.btn-delete4').show();
  });

  $("#project_duration").on('change', function(event) {
    event.preventDefault();
    //alert($(this).val());
    if($(this).val() == "other" ){
      $('.project_duration_other').show();
    }else{
      $('.project_duration_other').hide();
      $('#project_duration_other').val("");
    }
  });

  $("#question1").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q1_other').show();
    }else{
      $('#q1_other').hide();
      $('#question1_other').val("");
    }
  });

  $("#question2").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q2_other').show();
    }else{
      $('#q2_other').hide();
      $('#question2_other').val("");
    }
  });

  $("#question3").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q3_other').show();
    }else{
      $('#q3_other').hide();
      $('#question3_other').val("");
    }
  });

  $("#question4").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q4_other').show();
    }else{
      $('#q4_other').hide();
      $('#question4_other').val("");
    }
  });

  $("#question5").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "Create your own question" ){
      $('#q5_other').show();
    }else{
      $('#q5_other').hide();
      $('#question5_other').val("");
    }
  });

  $("#city_name").geocomplete({
    map:".map_canvas_location",
    location: "",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: false, },
    details: "form",
    detailsAttribute: "data-geo-location", 
    types: ["geocode", "establishment"],
  });

  $("#post_project").on('click', function(event) { event.preventDefault();
    var job_title = $("#job_title").val();
    var category = $("#category").val();
    var sub_category = $("#sub_category").val();
    var description = $("#description").val();
    var work_type = $("#work_type").val();
    var billing_period = $("#billing_period").val();
    var budget = $("#budget").val();
    var work_from = $("#work_from").val();
    var country_id = $("#country_id").val();
    var city_name = $("#city_name").val();
    var starting_date = $("#starting_date").val();
    var starting_time = $("#starting_time").val();
    var hours = $("#hours").val();
    var hours_per = $("#hours_per").val();
    var visibility = $("#visibility").is(":checked");
    var question1 = $("#question1").val();
    var question2 = $("#question2").val();
    var question3 = $("#question3").val();
    var question4 = $("#question4").val();
    var question5 = $("#question5").val();
    var question1_other = $("#question1_other").val();
    var question2_other = $("#question2_other").val();
    var question3_other = $("#question3_other").val();
    var question4_other = $("#question4_other").val();
    var question5_other = $("#question5_other").val();
    var project_duration = $("#project_duration").val();
    var project_duration_other = $("#project_duration_other").val();
    


    if(job_title == "") { $("#job_title").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Job Title"); ?>', 'warning', false, "#DD6B55");}
    else if(category == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("sel_category"); ?>', 'warning', false, "#DD6B55"); }
    else if(sub_category == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("sel_subcategory"); ?>', 'warning', false, "#DD6B55"); }
    else if(description == "") {$("#description").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("please_enter_description"); ?>', 'warning', false, "#DD6B55"); }
    else if(work_type == "") { $("#work_type").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Please select work type"); ?>', 'warning', false, "#DD6B55"); }
    else if(budget == "") { $("#budget").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter budget"); ?>', 'warning', false, "#DD6B55"); }
    else if( parseInt(budget) <= 0) { $("#budget").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("budget should be greater than 0"); ?>', 'warning', false, "#DD6B55"); }
    else if(work_from == "Remotely (Preferred Country)" && country_id == ""){ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("sel_count"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(work_type == "per hour" && billing_period == ""){ $("#billing_period").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Select billing period"); ?>', 'warning', false, "#DD6B55");}
    else if(work_from == "On-site (specific location)" && city_name == ""){ $("#city_name").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter Location Name"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question1 == "Create your own question" && question1_other == "" && $("#q1").is(":visible")){ $("#question1_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question2 == "Create your own question" && question2_other == "" && $("#q2").is(":visible")){ $("#question2_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question3 == "Create your own question" && question3_other == "" && $("#q3").is(":visible")){ $("#question3_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question4 == "Create your own question" && question4_other == "" && $("#q4").is(":visible")){ $("#question4_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(question5 == "Create your own question" && question5_other == "" && $("#q5").is(":visible")){ $("#question5_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter your own question"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else if(project_duration == "other" && project_duration_other == ""){ $("#project_duration_other").focus(); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Enter project duration"); ?>', 'warning', false, "#DD6B55");if($("#advance_option").is(":hidden")){$("#advance_option").slideToggle();$(".btn_show").addClass("hidden"); $(".btn_hide").removeClass("hidden");}}
    else{
      //alert("submit");
      $("#cutomer_job_post_form").submit(); 
    }
  });

  $("#work_type").on('change', function(event) {
    event.preventDefault();
    if($(this).val() == "per hour" ){
      $(".bgtpr").removeClass("hidden");
      $(".exprdt").removeClass("hidden");
      $('.bgtfxd').hide();
    }else{
      $('.bgtfxd').show();
      $(".bgtpr").addClass("hidden");
      $(".exprdt").addClass("hidden");
    }
  });

  $("#budget").keypress(function(event) {
    return /\d/.test(String.fromCharCode(event.keyCode));
  });

  $("#budget").keyup(function(){
    var value = $(this).val();
    value = value.replace(/^(0*)/,"");
    $(this).val(value);
  });

</script>
