<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
   display: none;
  }
  .hoverme {
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
  }
  .btnMenu:hover {
    text-decoration: none;
    background-color: #ffb606;
    color: #FFF;
  }

  .fa-heart-o: {
    content: "\f08a";
    color: red;
    text-shadow: 1px 1px 1px #ccc;
    font-size: 0.7em;
  }

  .fa-heart:{
    content: "\f08a";
    color: red;
    text-shadow: 1px 1px 1px #ccc;
    font-size: 0.7em;
  } 
</style>
 
<div class="normalheader small-header" style="background-color: #f1f3f6;">
<span ><h6 style="color: #f1f3f6; font-size: 15px;">hii</h6><span>
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px; background-color: white;">
    <h3 class="">  <i class="fa fa-search text-muted"></i> <strong><?= ($type==0)?$this->lang->line('Freelance projects'):$this->lang->line('Troubleshoot job'); ?> - <?= sizeof($jobs)." ".$this->lang->line('Found'); ?></strong> </h3>
    </div>
  </div>
</div>
 
<div class="content" style="background-color: #f1f3f6;">
<div><div>
 <div class="hpanel hblue" style="margin-top: -10px; margin-left: -15px; margin-right: -15px;">
  <div class="panel-body" >
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="hpanel" style="margin-bottom: 0px; margin-left: -8px; margin-right: -7px; margin-top: 15px;">
        <div class="panel-body" style="background-color:white">
          <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-xs-12 text-center">
              <h5 style="margin:10px; display: <?=($filter == 'advance')?'none':''?>" id="searchlabel"><i class="fa fa-search"></i> <?= $this->lang->line('search'); ?></h5>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 col-xs-12">
              <input type="text" id="searchbox" class="form-control" placeholder="<?= $this->lang->line('Type here to search for social media, sales, seo, marketing, web design...'); ?>" style="display: <?=($filter == 'advance')?'none':''?>" />
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-xs-12 text-right">
              <a class="btn btn-link btn-block advanceBtn" id="advanceBtn"><i class="fa fa-search"></i> <?= $filter == 'advance' ? $this->lang->line('basic_search') : $this->lang->line('advance_search') ?></a>
            </div>
          </div>
          <div class="" id="advanceSearch" style="<?= ($filter == 'basic') ? 'display:none' : '' ?>;">
            <?php if($type==0){ ?>
            <form action="<?= base_url('projects') ?>" method="get" id="offerFilter">
            <?php }else{ ?>
            <form action="<?= base_url('troubleshooter') ?>" method="get" id="offerFilter">

            <?php } ?>
              <input type="hidden" name="filter" value="advance" />
         <div class="row">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-globe"></i> <?= $this->lang->line('Country'); ?></label>
                <select id="country_id" name="country_id" class="form-control select2" autocomplete="none">
                  <option value="0"><?= $this->lang->line('Select Country'); ?></option>
                  <?php foreach ($countries as $country): ?>
                    <option value="<?= $country['country_id'] ?>"<?php if(isset($country_id) && $country_id == $country['country_id']) { echo 'selected'; } ?>><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
          <!--     <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-right: 15px; padding-left: 15px;" hidden>
                <label class="control-label"><i class="fa fa-map-marker"></i> <?= $this->lang->line('state'); ?></label>
                <select id="state_id" name="state_id" class="form-control select2" autocomplete="none">
                  <?php if(isset($country_id)) { 
                    if(!isset($state_id) || $state_id <= 0) { ?>
                      <option value="0"><?= $this->lang->line('select_state'); ?></option>
                    <?php } ?>
                    <?php foreach ($states as $state): ?>
                      <option value="<?= $state['state_id'] ?>"<?php if($state_id == $state['state_id']) { echo 'selected'; } ?>><?= $state['state_name']; ?></option>
                    <?php endforeach ?> 
                  <?php } else { ?>
                    <option value="0"><?= $this->lang->line('select_state'); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-right: 15px; padding-left: 15px;" hidden>
                <label class="control-label"><i class="fa fa-address-book-o"></i> <?= $this->lang->line('city'); ?></label>
                <select id="city_id" name="city_id" class="form-control select2" autocomplete="none">
                  <?php if(isset($city_id) || $city_id > 0) { 
                    if($city_id == 0) { ?>
                      <option value="0"><?= $this->lang->line('select_city'); ?></option>
                    <?php } ?>
                    <?php foreach ($cities as $city): ?>
                      <option value="<?= $city['city_id'] ?>" <?php if($city_id == $city['city_id']) { echo 'selected'; } ?>><?= $city['city_name']; ?></option>
                    <?php endforeach ?> 
                  <?php } else { ?>
                    <option value="0"><?= $this->lang->line('select_city'); ?></option>
                  <?php } ?>
                </select>
              </div> -->
             

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 15px; padding-left: 15px;" >
                <label class="control-label"><i class="fa fa-angle-double-right"></i> <?= $this->lang->line('category_type'); ?></label>
                <select id="cat_type_id" name="cat_type_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category_type'); ?>">
                  <option value="0"><?= $this->lang->line('select_category_type'); ?></option>
                  <?php foreach ($category_types as $ctypes): ?>
                    <option value="<?= $ctypes['cat_type_id'] ?>" <?php if(isset($cat_type_id) && $cat_type_id == $ctypes['cat_type_id']) { echo 'selected'; } ?>><?= $ctypes['cat_type']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-angle-right"></i> <?= $this->lang->line('category'); ?></label>
                <select id="cat_id" name="cat_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category'); ?>">
                  <?php if(isset($cat_type_id)) { 
                    if(!isset($cat_id) || $cat_id <= 0) { ?>
                      <option value="0"><?= $this->lang->line('select_category'); ?></option>
                    <?php } ?>
                    <?php foreach ($categories as $category): ?>
                      <option value="<?= $category['cat_id'] ?>"<?php if($cat_id == $category['cat_id']) { echo 'selected'; } ?>><?= $category['cat_name']; ?></option>
                    <?php endforeach ?> 
                  <?php } else { ?>
                    <option value="0"><?= $this->lang->line('select_category'); ?></option>
                  <?php } ?>
                </select>
              </div>
          </div>
         <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-sort-amount-asc"></i> <?=$this->lang->line('Sort'); ?></label>
                <select class="form-control m-b select2" name="sort_by"  id="sort_by">
                  <option value=""><?=$this->lang->line('by'); ?></option>
                  
                  <option value="low" <?=(isset($sort_by) && $sort_by == 'low')?'selected':''?>><?=$this->lang->line('Price (low to high)'); ?></option>
                  <option value="high" <?=(isset($sort_by) && $sort_by == 'high')?'selected':''?>><?=$this->lang->line('Price (high to low)'); ?></option>
                </select>
            </div>
             <!--  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-clock-o"></i> <?= $this->lang->line('Delivery Time'); ?></label>
                <select class="form-control m-b select2" name="delivered_in"  id="delivered_in">
                  <option value=""> <?= $this->lang->line('Select day'); ?> </option>
                  <?php for ($i=1; $i <= 5 ; $i++) : ?>
                    <option value="<?=$i?>" <?php if(isset($delivered_in) && $delivered_in == $i) { echo 'selected'; } ?>><?php echo ($i==1)?$i.' '.$this->lang->line('Day'):$i.' '.$this->lang->line('Days'); ?></option>
                  <?php endfor; ?>
                </select>
              </div> -->
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-money"></i> <?= $this->lang->line('Price between'); ?></label>
                <div class="input-range input-group" style="height: 34px;">
                  <input type="number" class="input-sm form-control" name="start_price" min="0" value="<?php if(isset($start_price)) { echo $start_price; } ?>" style="height: 34px;" />
                  <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                  <input type="number" class="input-sm form-control" name="end_price" min="0" value="<?php if(isset($end_price)) { echo $end_price; } ?>" style="height: 34px;" />
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 text-left" style="padding-right: 15px; padding-left: 15px;">
                <br />

                    
                <?php $reset_url=($type==0)?'projects':'troubleshooter'; ?>
                <button class="btn btn-success btn-sm" type="submit"><i class="fa fa-search"></i> <?= $this->lang->line('apply_filter'); ?></button>
                <a class="btn btn-warning btn-sm" href="<?= base_url($reset_url) ?>"><i class="fa fa-ban"></i> <?= $this->lang->line('reset'); ?></a>
              </div>
              </div>
<!--          <div class="row">
              <div class="col-xl-9 col-lg-9 col-md-8 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px; padding-top: 15px;">
                <div class="checkbox checkbox-default checkbox-inline" style="margin-left: 0px; margin-right: 15px;">
                  <input type="checkbox" style=" margin-left: -100px;" id="latest" name="latest" <?= ( isset($latest) && $latest == "on")? "checked" : '' ;?> >
                  <label for="latest"> <?= $this->lang->line('Show latest offers'); ?> </label>
                </div>
              </div> -->
          <!--     <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12 text-right" style="padding-right: 15px; padding-left: 15px;">
                <br />
                <button class="btn btn-success btn-sm" type="submit"><i class="fa fa-search"></i> <?= $this->lang->line('apply_filter'); ?></button>
                <a class="btn btn-warning btn-sm" href="<?= base_url('offers') ?>"><i class="fa fa-ban"></i> <?= $this->lang->line('reset'); ?></a>
              </div> -->
              <!-- </div> -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

      <?php if(sizeof($jobs) > 0) { ?>
        <div class="hpanel">

          <?php if($this->session->flashdata('error')):  ?>
            <div class="row">
              <div class="col-xl-offset-1 col-xl-10 col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 text-center">
                <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
              </div>
            </div>
          <?php endif; ?>
          <?php if($this->session->flashdata('success')):  ?>
            <div class="row">
              <div class="col-xl-offset-1 col-xl-10 col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 text-center">
                <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
              </div>
            </div>
          <?php endif; ?>

          <table id="addressTableData" class="table hpanel" >
            <thead>
              <tr>
                <th class="hidden"></th>
              </tr>
            </thead>
             <tbody>
            <?php foreach ($jobs as $job) { if($job['service_type'] == 0) { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: -5px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 0px 15px 10px 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding: 0px">
                            <h6><img align="middle" src="<?=base_url($job['icon_url'])?>" /> <?=$job['cat_name']?></h6>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
                            <h6><i class="fa fa-tag"></i> Ref #GS-<?=$job['job_id']?></h6>
                          </div>
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12 text-right" style="padding-right: 0px;">
                            <?php if($job['job_type']==0){ ?>
                            <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=$job['budget']?> <?=$job['currency_code']?></strong></h4>
                            <?php }else{ ?>
                              <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=(!$job['immediate'])?$this->lang->line('Immediate'):$this->lang->line('On Date')?><small> <?=($job['immediate'])?date('d M Y' , strtotime($job['complete_date'])):'';?> </small></strong></h4>
                            <?php } ?>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <h4 style="margin-top: 5px;"><?=$job['job_title']?>&nbsp;&nbsp;<span class="label label-primary"><?=($job['service_type']==1 && $job['job_type']==0)?$this->lang->line('Offer'):($job['service_type']==0 && $job['job_type']==0)?$this->lang->line('Project'):$this->lang->line('Troubleshoot')?></span>&nbsp;&nbsp; 
                            </h4>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                           <?php if($job['job_type']==0){
                            echo ($job['work_type'] == "per hour")?$this->lang->line('Per Hour'):$this->lang->line('Fixed Price');
                           } else{
                            echo $this->lang->line('Troubleshoot');
                           }
                           ?>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <i class="fa fa- fa-clock-o"></i> <?= $this->lang->line('posted on'); ?>: <?=date('D, d M y', strtotime($job['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-map-marker"></i>&nbsp;<?=$job['location_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-dot-circle-o"></i> <?=$this->lang->line('Proposals').": ".$job['proposal_counts']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <form action="<?=base_url('view-proposal-detail')?>" method="get">
                              <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                              <!-- <input type="hidden" name="redirect_url" value="browse-project" /> -->
                              <button type="submit" class="btn btn-sm btn-outline btn-success btn-block" id="btnWorkroom"><i class="fa fa-paper-plane"></i> <?= $this->lang->line('view job'); ?></button>
                            </form>
                            <script>
                              $('.add_favorite_<?=$job['job_id']?>').click(function () { 
                                var id = $(this).attr("data-id");
                                $.post('<?=base_url("user-panel-services/make-job-favourite")?>', {id: id}, 
                                function(res) { 
                                  console.log(res);
                                  if($.trim(res) == "success") { 
                                    swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Job added to your favorite list.')); ?>, "success");
                                    $("#btn_remove_<?=$job['job_id']?>").css("display", "none");
                                    $("#btn_add_<?=$job['job_id']?>").css("display", "inline-block");
                                  } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to add. Try again...')); ?>, "error");  } 
                                });
                              });
                              $('.remove_favorite_<?=$job['job_id']?>').click(function () { 
                                var id = $(this).attr("data-id");
                                $.post('<?=base_url("user-panel-services/job-remove-from-favorite")?>', {id: id}, 
                                function(res) { 
                                  //console.log(res);
                                  if($.trim(res) == "success") { 
                                    swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Job removed from favorite list.')); ?>, "success"); 
                                    $("#btn_remove_<?=$job['job_id']?>").css("display", "inline-block");
                                    $("#btn_add_<?=$job['job_id']?>").css("display", "none");
                                  } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to remove. Try again...')); ?>, "error");  } 
                                });
                              });
                            </script>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } else { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: 0px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          Job - <?=$job['job_title']?>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } } ?>
          </tbody>
          </table>
        </div>
      <?php } else { ?>
        <div class="hpanel horange" style="margin-left: -8px; margin-right: -7px; margin-top: 10px;">
          <div class="panel-body text-center">
            <h5><?=$this->lang->line('No offers found.')?></h5>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
</div>

<script>
  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $(this).val();
    //$('#city_id').attr('disabled', true);

    $.ajax({
      url: "get-state-by-country-id",
      type: "POST",
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        console.log(res);
        $('#state_id').attr('disabled', false);
        $('#state_id').empty();
        $('#city_id').empty();
        $('#state_id').append("<option value='0'><?=$this->lang->line('select_state')?></option>");
        $('#state_id').select2("val",$('#state_id option:nth-child(1)').val());
        $('#city_id').append("<option value='0'><?=$this->lang->line('select_city')?></option>");
        $('#city_id').select2("val",$('#city_id option:nth-child(1)').val());
        $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
        $('#state_id').focus();
      },
      beforeSend: function(){
        $('#state_id').empty();
        $('#state_id').append("<option value='0'><?=$this->lang->line('loading')?></option>");
      },
      error: function(){
        //$('#state_id').attr('disabled', true);
        $('#state_id').empty();
        $('#state_id').append("<option value='0'><?=$this->lang->line('no_option')?></option>");
      }
    })
  });
  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    $.ajax({
      type: "POST",
      url: "get-cities-by-state-id",
      data: { state_id: state_id },
      dataType: "json",
      success: function(res){
        $('#city_id').attr('disabled', false);
        $('#city_id').empty();
        $('#city_id').append("<option value='0' selected='selected'><?=$this->lang->line('select_city')?></option>");
        $('#city_id').select2("val",$('#city_id option:nth-child(1)').val());
        $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
        $('#city_id').focus();
      },
      beforeSend: function(){
        $('#city_id').empty();
        $('#city_id').append("<option value='0'><?=$this->lang->line('loading')?></option>");
      },
      error: function(){
        //$('#city_id').attr('disabled', true);
        $('#city_id').empty();
        $('#city_id').append("<option value='0'><?=$this->lang->line('no_option')?></option>");
      }
    })
  });
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-categories-by-type", 
      data: { type_id: cat_type_id },
      dataType: "json",
      success: function(res) {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('select_category')?></option>");
        $.each( res, function() {
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function() {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('loading')?></option>");
      },
      error: function() {
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('no_options')?></option>");
      }
    })
  });
  $("#offerFilter").validate({
    ignore: [], 
    rules: {
      price: { number : true },
    },
    messages: {
      price: { number : <?= json_encode($this->lang->line('number_only')); ?> },
    },
  });
  $('#advanceBtn').click(function() {
    if($('#advanceSearch').css('display') == 'none') {
      $('#basicSearch').hide("slow");
      $('#advanceSearch').show("slow");
      $('#advanceBtn').html("<i class='fa fa-search'></i>" + <?= json_encode($this->lang->line('basic_search'))?>);
      $('#searchbox').hide("slow");
      $('#searchlabel').hide("slow");
    } else {
      $('#advanceBtn').html("<i class='fa fa-search'></i>" + <?= json_encode($this->lang->line('advance_search'))?>);
      $('#advanceSearch').hide("slow");
      $('#basicSearch').show("slow");
      $('#searchbox').show("slow");
      $('#searchlabel').show("slow");
    }
    return false;
  });
</script>