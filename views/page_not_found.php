<!-- 404 PAGE -->
<section class="m-t-80 p-b-150">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="page-error-404 text-center">404</div>
			</div>
			<div class="col-md-6">
				<div class="text-left">
					<h1 class="text-medium"><?= $this->lang->line('page_under_construction'); ?></h1>
					<p class="lead"><?= $this->lang->line("we_sorry"); ?></p><div class="seperator m-t-20 m-b-20"></div><a href="<?= base_url() ?>" class="btn btn-primary"><?= $this->lang->line('back_to_home'); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- END:  404 PAGE -->
