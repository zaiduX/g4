<style type="text/css">
    .table > tbody > tr > td {
        border-top: none;
    }
    .dataTables_filter {
        display: none;
    }
</style>
<section class="p-b-10" style="margin-top: -50px">
	<div class="container">
		<div class="row col-md-12">
      <div class="hr-title hr-long center"><abbr><?= $this->lang->line('lbl_job'); ?></abbr> </div>
      <?php if(empty($orders)) { echo '<h4 class="text-center" style="padding-bottom: 100px">'.$this->lang->line("no_jobs_available").'<br /><br /><a href="'.base_url().'" class="btn btn-info">Gonagoo Home</a></h4>'; } else { ?>
			<table id="orderTableData" class="table">
                                    <thead><tr class="hidden"><th></th></tr></thead>
                                    <tbody>
                                        <?php foreach ($orders as $order) { ?>
                                        <tr>
                                            <td>
                                                <div class="hpanel filter-item">
                                                    <a href="<?= $this->config->item('base_url') . 'user-panel/open-order-details/'.$order['order_id']; ?>">
                                                    <div class="panel-body">
                                                        <div class="col-lg-12">
                                                        <div class="col-lg-2"> 
                                                            <h5 class="text-left"><?= $this->lang->line('ref_G'); ?><?= $order['order_id'] ?></h5>
                                                        </div>
                                                        <div class="col-lg-6 text-center"> 
                                                            <h4>
                                                                <?= $this->lang->line('courier'); ?>
                                                            </h4>
                                                            <h6 class=""><?= ($order['order_description'] !="NULL")?$order['order_description']:""; ?></h6>    
                                                        </div>
                                                        <div class="col-lg-4"> 
                                                            <h5 class="text-right"><?= $this->lang->line('order_date'); ?><?= date('l, d M Y',strtotime($order['cre_datetime'])); ?></h5>
                                                            <h6 class="text-right"><?= $this->lang->line('expiry_date'); ?><?= date('l, d M Y',strtotime($order['expiry_date'])); ?></h6>
                                                        </div>
                                                        <br/>
                                                    </div>
                                                        <h4 class="m-b-xs"><?= $this->lang->line('from_addr'); ?><?php 
                                                                                    echo $order['from_address'];
                                                                                    if($order['from_addr_type'] == 1) {
                                                                                        echo ', ' . $this->api->get_country_name_by_id($order['from_country_id']) . ', ' . $this->api->get_city_name_by_id($order['from_city_id']); } ?></h4>
                                                        <h4 class="m-b-xs"><?= $this->lang->line('to_addr'); ?><?php 
                                                                                    echo $order['to_address'];
                                                                                        if($order['to_addr_type'] == 1) {
                                                                                        echo ', ' . $this->api->get_country_name_by_id($order['to_country_id']) . ', ' . $this->api->get_city_name_by_id($order['to_city_id']); } ?></h4>
                                                        <p class="small">
                                                            <div class="col-md-6"><?= $this->lang->line('weight'); ?><?= $order['total_weight'] ?> <?= strtoupper($this->user->get_unit_name($order['unit_id'])); ?></div>
                                                            <div class="col-md-6"><?= $this->lang->line('price'); ?><?= $order['currency_sign'] ?> <?= $order['order_price'] ?></div>
                                                        </p>
                                                        <p class="small">
                                                            <div class="col-md-6"><?= $this->lang->line('advance'); ?><?= $order['currency_sign'] ?> <?= $order['advance_payment'] ?></div>
                                                            <div class="col-md-6"><?= $this->lang->line('balance'); ?><?= $order['pickup_payment']+$order['deliver_payment'] ?> <?= $order['currency_sign'] ?></div>
                                                        </p>
                                                        <p class="small">
                                                            <div class="col-md-6"><?= $this->lang->line('by'); ?><?= $this->user->get_vehicle_type_name($order['vehical_type_id']); ?></div>
                                                            <div class="col-md-6"><?= $this->lang->line('dimension'); ?><?= strtoupper($this->user->get_dimension_type_name($order['dimension_id'])); ?> [<?= $order['width'] ?>*<?= $order['height'] ?>*<?= $order['length'] ?>]</div>
                                                        </p>
                                                        <p class="small">
                                                            <div class="col-md-6"><?= $this->lang->line('pickup_date_time'); ?><?= $order['pickup_datetime'] ?></div>
                                                            <div class="col-md-6"><?= $this->lang->line('deliver_date_time'); ?><?= $order['delivery_datetime'] ?></div>
                                                        </p>
                                                        <p class="small">
                                                            <div class="col-md-6"><?= $this->lang->line('sender_phone'); ?><?= $order['from_address_contact'] == 'NULL' ? 'Not Provided...' : $order['from_address_contact'] ?></div>
                                                            <div class="col-md-6"><?= $this->lang->line('reciever_phone'); ?><?= $order['to_address_contact'] == 'NULL' ? 'Not Provided...' : $order['to_address_contact'] ?></div>
                                                        </p>
                                                    </div>
                                                    </a>
                                                    <div class="panel-footer">
                                                        <div class="row">
                                                            <div class="col-md-3 col-md-offset-9 text-right">
                                                                <a href="base_url('log-in')" class="btn btn-outline btn-info" ><i class="fa fa-check"></i> <?= $this->lang->line('accept'); ?> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php } ?>
</div>
</div>
</section>