<!DOCTYPE html>
<html lang="<?= $this->config->item('lang'); ?>">
<?php
	$currentURL = current_url();
	$currentURL = substr($currentURL,30);
?>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Servicesgoo" />
	<?php
		if($currentURL == ""){
			echo '<meta name="description" content="main page">';
		}elseif($currentURL == "careers") {
			echo '<meta name="description" content="careers">';
		}elseif($currentURL == "courier") {
			echo '<meta name="description" content="courier">';
		}elseif($currentURL == "laundry") {
			echo '<meta name="description" content="laundry">';
		}elseif($currentURL == "log-in") {
			echo '<meta name="description" content="login page">';
		}elseif($currentURL == "page-not-found") {
			echo '<meta name="description" content="not found">';
		}elseif($currentURL == "recovery/password") {
			echo '<meta name="description" content="recovery/password">';
		}elseif($currentURL == "terms-conditions") {
			echo '<meta name="description" content="terms-conditions">';
		}elseif($currentURL == "ticket") {
			echo '<meta name="description" content="ticket booking">';
		}elseif($currentURL == "sign-up") {
			echo '<meta name="description" content="register">';
		}elseif($currentURL == "about-us") {
			echo '<meta name="description" content="about us">';
		}elseif($currentURL == "transport") {
			echo '<meta name="description" content="transport">';
		} 
	?>
	

	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="no-cache">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="Cache-Control" content="no-cache">
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/images/favicon.png';?>" rel="shortcut icon" />
	<title><?= $this->config->item('site_title'); ?></title>

	<?php
		// if($currentURL == ""){
		// 	echo '<title>'.$this->lang->line('gonagoo').'</title>';
		// }elseif($currentURL == "careers") {
		// 	echo '<title>'.$this->lang->line('careers').'</title>';
		// }elseif($currentURL == "courier") {
		// 	echo '<title>'.$this->lang->line('lbl_courier').'</title>';
		// }elseif($currentURL == "laundry") {
		// 	echo '<title>'.$this->lang->line('Laundry').'</title>';
		// }elseif($currentURL == "log-in") {
		// 	echo '<title>'.$this->lang->line('login').'</title>';
		// }elseif($currentURL == "page-not-found") {
		// 	echo '<title>'.$this->lang->line('Page not found').'</title>';
		// }elseif($currentURL == "recovery/password") {
		// 	echo '<title>'.$this->lang->line('pass_recover').'</title>';
		// }elseif($currentURL == "terms-conditions") {
		// 	echo '<title>'.$this->lang->line('terms_and').' '.$this->lang->line('conditions').'</title>';
		// }elseif($currentURL == "ticket") {
		// 	echo '<title>'.$this->lang->line('Book Ticket').'</title>';
		// }elseif($currentURL == "sign-up") {
		// 	echo '<title>'.$this->lang->line('sign_up').'</title>';
		// }elseif($currentURL == "about-us") {
		// 	echo '<title>'.$this->lang->line('about_us').'</title>';
		// }elseif($currentURL == "transport") {
		// 	echo '<title>'.$this->lang->line('transportation').'</title>';
		// }elseif($currentURL == "site-map") {
		// 	echo '<title>'.$this->lang->line('Sitemap').'</title>';
		// } 
	?>

	<!-- CDN -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<link href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" rel="stylesheet"  />
	<link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" rel="stylesheet" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
	<link href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" rel="stylesheet" />
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css">
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/clockpicker/dist/bootstrap-clockpicker.min.css'; ?>" />
    <script src="//cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="//cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/animateit/animate.min.css'; ?>"  rel="stylesheet">
	<!-- Vendor css -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/owlcarousel/owl.carousel.css'; ?>"  rel="stylesheet">
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/magnific-popup/magnific-popup.css'; ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css'; ?>" >
	<link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/sweetalert/lib/sweet-alert.css'; ?>" />
	<!-- Template base -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/css/theme-base.css'; ?>" rel="stylesheet">
	<!-- Template elements -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/css/theme-elements.css'; ?>" href="" rel="stylesheet">  
	<!-- Responsive classes -->
	<!--<link href="<?= $this->config->item('resource_url') . 'web/front_site/css/responsive.css'; ?>" rel="stylesheet">
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/css/res.css'; ?>" rel="stylesheet">-->
	<!--[if lt IE 9]>
	<script src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->  
	<!-- Template color -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/css/blue.css'; ?>" rel="stylesheet" type="text/css" media="screen" title="blue">
	<!-- LOAD GOOGLE FONTS -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
	<!-- <link rel="stylesheet" type="text/css" href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/rs-plugin/css/settings.css'; ?>"  media="all" property="stylesheet" /> -->
	<link rel="stylesheet" type="text/css" href="<?= $this->config->item('resource_url') . 'web/front_site/css/rs-plugin-styles.css'; ?>" />
	<!-- CSS CUSTOM STYLE -->
	<link rel="stylesheet" type="text/css" href="<?= $this->config->item('resource_url') . 'web/front_site/css/custom.css'; ?>"  media="screen" />
	<link rel="stylesheet" type="text/css" href="<?= $this->config->item('resource_url') . 'web/front_site/css/sufi_custom.css'; ?>"  media="screen" />
	<link rel="stylesheet" href="//www.w3schools.com/w3css/4/w3.css">
	<!--VENDOR SCRIPT-->
	<script src="<?= $this->config->item('resource_url') . 'web/front_site/vendor/jquery/jquery-1.11.2.min.js'; ?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'web/front_site/vendor/plugins-compressed.js'; ?>"></script>
	<link href="//cdn.jsdelivr.net/sweetalert2/latest/sweetalert2.css" rel="stylesheet"/>
	<script src="//cdn.jsdelivr.net/sweetalert2/latest/sweetalert2.js"></script>
	<script src="<?= $this->config->item('resource_url') . 'web/front_site/js/dragscroll.js'; ?>"></script>
	<link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css'; ?>" />

	<script src="//maps.googleapis.com/maps/api/js?libraries=places&language=en&key=AIzaSyBSaP8CCJqjgZ8viUxwpMOdswi6Y04Ch0k"></script>
    <script src="<?= $this->config->item('resource_url') . 'js/jquery.geocomplete.min.js'; ?>"></script>
    <script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/moment/moment.js'; ?>"></script>



	<style>
		.select2-container--default .select2-selection--single .select2-selection__arrow {
		    height: 26px;
		    position: absolute;
		    top: 10px;
		    right: 1px;
		    width: 20px;
		}
		.select2-container--default .select2-selection--single {
		    border-radius: 0px;
		}
		.select2-container .select2-selection--single {
		    height: 45px;
		    user-select: none;
		    -webkit-user-select: none;
		    border: 2px solid #ebebeb;
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered {
			line-height: 1.333333;
		  padding: 12px 18px;
		}


		/* width */
	    ::-webkit-scrollbar {
	        height: 10px;
	    }

	    /* Track */
	    ::-webkit-scrollbar-track {
	        background: #fff; 
	    }
	     
	    /* Handle */
	    ::-webkit-scrollbar-thumb {
	        border-radius: 10px;
	        background: #64ccf5; 
	    }

	    /* Handle on hover */
	    ::-webkit-scrollbar-thumb:hover {
	        background: #1E5394; 
	    }

	   	.booknowBtn {
	   		border-radius: 25px;
	   	}

	</style>
</head>
<body class="wide page-loader" data-animation-icon ="<?= $this->config->item('resource_url') . 'web/front_site/images/svg-loaders/spin.svg'; ?>" >
  
  <div class="wrapper"> 
  
    <!-- HEADER -->
    <header id="header" class="header header-transparent header-navigation-light">
      <div id="header-wrap">
        <div class="container-fluid">

          <!--LOGO-->
          <div id="logo">
            <a href="<?= base_url(); ?>" class="Gonagoo logo">
              <img src="<?= $this->config->item('resource_url') . 'web/front_site/images/logo-new.png'; ?>" alt="<?=base_url('Servicesgoo logo')?>" style="height:55px; margin-top: 5px;">
            </a>
          </div>
          <!--END: LOGO-->

          <!--MOBILE MENU -->
          <div class="nav-main-menu-responsive">
            <button class="lines-button x" type="button" data-toggle="collapse" data-target=".main-menu-collapse">
              <span class="lines"></span>
            </button>
          </div>
          <!--END: MOBILE MENU -->

          <!--NAVIGATION-->
          <div class="navbar-collapse collapse main-menu-collapse navigation-wrap">
			<div class="container">
				<ul class="nav navbar-nav bottom-menu left" style="padding-right: 0px !important">
					<!-- <li class="nav-item" style="<?=(($this->router->fetch_method()=='courier_index') || ($this->router->fetch_method()=='log_in_booking' && isset($_GET['cat_id']) && $_GET['cat_id'] == 7))?'background-color: #5dccf7':''?>"><a class="nav-link" href="<?=base_url('courier')?>" style="padding-left: 8px !important; padding-right: 8px !important;"><img src="<?=base_url('resources/images/front_courier.png')?>" style="max-height: 18px"> <?= $this->lang->line('Courier'); ?></a></li>
					<li class="nav-item"  style="<?=($this->router->fetch_method()=='transport_index' || ($this->router->fetch_method()=='log_in_booking' && isset($_GET['cat_id']) && $_GET['cat_id'] == 6))?'background-color: #5dccf7':''?>"><a class="nav-link" href="<?=base_url('transport')?>" style="padding-left: 8px !important; padding-right: 8px !important;"><img src="<?=base_url('resources/images/front_truck.png')?>" style="max-height: 18px"> <?= $this->lang->line('transport'); ?></a></li>
					<li class="nav-item"  style="<?=($this->router->fetch_method()=='ticket_index' || $this->router->fetch_method()=='ticket_search' || $this->router->fetch_method()=='ticket_booking_process' || $this->router->fetch_method()=='more_passengers_details' || $this->router->fetch_method()=='bus_ticket_payment_view' || $this->router->fetch_method()=='ticket_payment_confirmation')?'background-color: #5dccf7':''?>"><a class="nav-link" href="<?=base_url('ticket')?>" style="padding-left: 8px !important; padding-right: 8px !important;"><img src="<?=base_url('resources/images/front_bus.png')?>" style="max-height: 18px"> <?= $this->lang->line('Ticket'); ?></a></li>
					<li class="nav-item"  style="<?=($this->router->fetch_method()=='laundry_index' || $this->router->fetch_method()=='laundry_providers' || $this->router->fetch_method()=='laundry_booking_details')?'background-color: #5dccf7':''?>"><a class="nav-link" href="<?=base_url('laundry')?>" style="padding-left: 8px !important; padding-right: 8px !important;"><img src="<?=base_url('resources/images/front_laundry.png')?>" style="max-height: 18px"> <?= $this->lang->line('Laundry'); ?></a></li>
					<li class="nav-item"  style="<?=($this->router->fetch_method()=='services_index' || $this->router->fetch_method()=='services_providers' || $this->router->fetch_method()=='services_booking_details')?'background-color: #5dccf7':''?>"><a class="nav-link" href="<?=base_url('services')?>" style="padding-left: 8px !important; padding-right: 8px !important;"><img src="<?=base_url('resources/images/front_services.png')?>" style="max-height: 18px"> <?= $this->lang->line('Services'); ?></a></li> -->
					<!-- <li class="nav-item"  style="<?=($this->router->fetch_method()=='gas_index')?'background-color: #5dccf7':''?>"><a class="nav-link" href="<?=base_url('gas')?>"><img src="<?=base_url('resources/images/front_gas.png')?>" style="max-height: 18px" > <?= $this->lang->line('Gas'); ?></a></li> -->
				</ul>
				<nav id="mainMenu" class="main-menu mega-menu">
				  <ul class="main-menu nav nav-pills">
                  <!-- <li class="nav-li" style="margin-right: -18px;"><a href="<?= base_url().'sign-up'; ?>"><i class="fa fa-pencil-square-o"></i><?= $this->lang->line('sign_up'); ?></a></li> -->
                  <li class="nav-li" style=""><a href="<?= base_url().'log-in'; ?>"><i class="fa fa-sign-in"></i><?= $this->lang->line('login'); ?></a></li>
                  <li class="dropdown nav-li" style=""> 
                    <a href="#" aria-expanded="true" data-toggle="dropdown" class="dropdown-toggle"><?= ucfirst($this->config->item('language')); ?><i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a class="list-entry btn_lang" style="cursor: pointer;" data-id="french">French</a></li>
                      <li><a class="list-entry btn_lang" style="cursor: pointer;" data-id="english">English</a></li>
                    </ul>
                  </li>  
                  <li class="nav-li" style=""><a href="<?= base_url().'sign-up'; ?>" style="color: #fa6b6b; padding-left: 10px !important;" title="<?= $this->lang->line('Become A Provider'); ?>"><i class="fa fa-user-circle-o"></i><?= $this->lang->line('PROVIDER?'); ?></a></li>
                  <!-- <li>
                    <button class="button blue-dark" onclick="window.location.href='<?= base_url().'sign-up'; ?>';" style="margin-top: 2px; text-transform: capitalize; border-radius:5px;"><?= $this->lang->line('become_provider'); ?></button>
                  </li> -->
                </ul>
              </nav>
            </div>
          </div>
          <!--END: NAVIGATION-->
        </div>
      </div>
    </header>
    <!-- END: HEADER -->
  
  
    <script>
      $(".btn_lang").on('click', function(event) {    event.preventDefault();
        var lang = $(this).data('id');
        $.post("<?=base_url('set-lang')?>", {lang: lang}, function(res) {
          window.location.reload();         
        });
        return false;       
      });
    </script>