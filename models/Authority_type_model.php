<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authority_type_model extends CI_Model {

	private $authority_types = "tbl_authority_type_master";

	public function get_authority_types()
	{
		$this->db->where('auth_type_id !=', 1);
		return $this->db->get($this->authority_types)->result_array();
	}

	public function get_active_authority_types()
	{
		$this->db->where('auth_type_status', 1);
		return $this->db->get($this->authority_types)->result_array();
	}

	public function get_authority_type_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('auth_type_id', $id);
			// $this->db->where('auth_type_status', 1);
			return $this->db->get($this->authority_types)->row_array();
		}
		return false;
	}

	public function get_authority_type_by_type($type=null)
	{
		if($type !=  null) {
			$this->db->where('auth_type', trim($type));
			return $this->db->get($this->authority_types)->row_array();
		}
		return false;
	}

	public function update_authority_type(array $data)
	{
		if(is_array($data)){
			$this->db->where('auth_type_id', (int)$data['auth_type_id']);
			$this->db->where('auth_type !=', trim($data['auth_type']));
			$this->db->update($this->authority_types, ['auth_type' => trim($data['auth_type'])]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function activate_authority_type($id=0)
	{
		if($id > 0){
			$this->db->where('auth_type_id', (int) $id);
			$this->db->where('auth_type_status', 0);
			$this->db->update($this->authority_types, ['auth_type_status' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_authority_type($id=0)
	{
		if($id > 0){
			$this->db->where('auth_type_id', (int) $id);
			$this->db->where('auth_type_status', 1);
			$this->db->update($this->authority_types, ['auth_type_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function register_authority_type($type=null)
	{
		if($type!=null){
			$today = date('Y-m-d H:i:s');
			$insert_data = array(
				'auth_type' => trim($type),
				'auth_type_status' => 1, 
				'cre_datetime' => $today,
			);
			$this->db->insert($this->authority_types, $insert_data);
			return $this->db->insert_id();
		}
		return false;
	}
}

/* End of file Authority_type_model.php */
/* Location: ./application/models/Authority_type_model.php */