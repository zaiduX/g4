<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model {

	private $categories = "tbl_category_master"; // db table declaration

	public function get_categories()
	{
		$this->db->select('cat.*,type.cat_type_id, type.cat_type');
		$this->db->from("tbl_category_master as cat");
		$this->db->join('tbl_category_type_master as type', 'cat.cat_type_id = type.cat_type_id');

		return $this->db->get()->result_array();
	}
	
	public function get_category_detail($id=0)
	{
		if($id > 0 ) {
			$this->db->where('cat_id', (int) $id);
			$this->db->where('cat_status', 1);
			return $this->db->get($this->categories)->row_array();
		}
		return false;
	}

	public function activate_category($id=0)
	{
		if($id > 0){
			$this->db->where('cat_id', (int) $id);
			$this->db->where('cat_status', 0);
			$this->db->update($this->categories, ['cat_status' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_category($id=0)
	{
		if($id > 0){
			$this->db->where('cat_id', (int) $id);
			$this->db->where('cat_status', 1);
			$this->db->update($this->categories, ['cat_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function check_category($cat_name=null, $type_id = 0, $parent_cat_id=0){
		if($cat_name != null AND $type_id != 0){
			$this->db->where('cat_name', trim($cat_name));
			$this->db->where('cat_type_id', (int) $type_id);
			if($parent_cat_id > 0 ) {		$this->db->where('parent_cat_id', (int) $parent_cat_id); }
			return $this->db->get($this->categories)->row_array();
		}
		return false;
	}

	public function check_category_update($cat_name=null, $type_id = 0, $parent_cat_id=0, $icon_url=null){
		if($cat_name != null && $type_id != 0 && $icon_url != null){
			$this->db->where('cat_name', trim($cat_name));
			$this->db->where('cat_type_id', (int) $type_id);
			$this->db->where('icon_url', (int) $icon_url);
			if($parent_cat_id > 0 ) {		$this->db->where('parent_cat_id', (int) $parent_cat_id); }
			return $this->db->get($this->categories)->row_array();
		}
		return false;
	}

	public function register_category(array $data)
	{
		if(is_array($data)){
			$today = date('Y-m-d H:i:s');
			$insert_data = array(
				'cat_name' => $data['cat_name'],
				'parent_cat_id' => $data['parent_cat_id'],
				'cat_type_id' => $data['type_id'],
				'icon_url' => $data['icon_url'],
				'cat_status' => 1, 
				'cre_datetime' => $today,
			);
			$this->db->insert($this->categories, $insert_data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_categories_by_type($id=0)
	{
		if($id > 0 ){
			$this->db->where('parent_cat_id', 0);
			$this->db->where('cat_type_id', (int) $id);
			return $this->db->get($this->categories)->result_array();
		}
		return false;
	}

	public function update_category(array $data)
	{
		if(is_array($data)){
			$update_data = array(
				'cat_name' => $data['cat_name'],
				'cat_type_id' => (int) $data['type_id'],
				'parent_cat_id' => (int) $data['parent_cat_id'],
				'icon_url' => $data['icon_url'],
			);
			$this->db->where('cat_id', (int)$data['cat_id']);
			$this->db->update($this->categories, $update_data);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file Categories_model.php */
/* Location: ./application/models/Categories_model.php */