<?php
defined('BASEPATH') OR exit('No direct script access allowed.');

class M4_model extends CI_Model {

  private $_category_type_master = 'tbl_category_type_master';
  private $_smp_freelancer_profile = 'tbl_smp_freelancer_profile';
  

  //--------------------------------------------------
    /*private $_app_parameter = 'app_param_master';
    private $_consumer_device = 'tbl_customer_device';
    private $_customer_skills = 'tbl_customer_skills';
    private $_skills_master = 'tbl_skills_master';
    private $_category_master = 'tbl_category_master';
    private $_language_master = 'tbl_language_master';
    private $_customers_master = 'tbl_customers_master';
    private $_customer_education = 'tbl_customer_education';
    private $_customer_experience = 'tbl_customer_experience';
    private $_skilled = 'tbl_customer_skills';
    private $_portfolio = 'tbl_consumer_portfolio';
    private $_deliverer = 'tbl_deliverer_profile';
    private $_business_photos = 'tbl_business_photos';
    private $_transport_details = 'tbl_transport_details';
    private $_transport_master = 'tbl_transport_vehicle_master';
    private $_unit_master = 'tbl_unit_master';
    private $_orders = 'tbl_consumer_orders';
    private $_standard_capacities = 'tbl_standard_capacities';
    private $_permitted_category = 'tbl_permitted_category';
    private $_customer_drivers = 'tbl_customer_drivers';
    private $_customer_service_areas = 'tbl_customer_service_areas';
    private $_addressbook = 'tbl_address_books';
    private $_favourite_deliverers = 'tbl_favourite_deliverers';
    private $_standard_dimension = 'tbl_standard_dimension';
    private $_standard_rates = 'tbl_standard_rates';
    private $_payment_details = 'tbl_consumer_payment_details';
    private $_courier_orders = 'tbl_courier_orders';
    private $_documents = 'tbl_consumers_documents';
    private $_courier_order_deliverer_request = 'tbl_courier_order_deliverer_request';
    private $_courier_order_status = 'tbl_courier_order_status';
    private $_courier_workroom = 'tbl_courier_workroom';
    private $_driver_device = 'tbl_driver_device';
    private $_driver_chats = 'tbl_driver_chats';
    private $_courier_order_review = 'tbl_courier_order_review';
    private $_insurance_master = 'tbl_insurance_master';
    private $_deliverer_documents = 'tbl_deliverer_documents';
    private $_relay_points = 'tbl_relay_points';
    private $_notifications = 'tbl_notifications';
    private $_read_notifications = 'tbl_read_notifications';
    private $_read_workroom_notification = 'tbl_read_workroom_notification';
    private $_read_chat_notification = 'tbl_read_chat_notification';
    private $_user_account_master = 'tbl_user_account_master';
    private $_deliverer_scrow_master = 'tbl_deliverer_scrow_master';
    private $_deliverer_scrow_history = 'tbl_deliverer_scrow_history';
    private $_gonagoo_account_master = 'tbl_gonagoo_account_master';
    private $_user_account_history = 'tbl_user_account_history';
    private $_gonagoo_account_history = 'tbl_gonagoo_account_history';
    private $_countries = 'tbl_countries';
    private $_states = 'tbl_states';
    private $_cities = 'tbl_cities';
    private $_account_withdraw_request = 'tbl_account_withdraw_request';
    private $_relay_account_master = 'tbl_relay_account_master';
    private $_customer_support_queries = "tbl_customer_support_queries";
    private $_carrier_type = "tbl_carrier_type";
    private $_custom_percentage = "tbl_custom_percentage";
    private $_point_to_point_rates = " tbl_point_to_point_rates";
    private $_order_packages = "tbl_order_packages";
    private $_dangerous_goods_master = "tbl_dangerous_goods_master";
    private $_customer_recent_relay_point = "tbl_customer_recent_relay_point";
    private $_rejected_orders = "tbl_rejected_orders";
    private $_cust_login_stats = "tbl_cust_login_stats";
    private $_gonagoo_address = "tbl_gonagoo_address";
    private $rates = "tbl_customer_standard_rates";
    private $units = "tbl_unit_master"; 
    private $currency = "tbl_currency_master";
    private $countries = "tbl_countries";
    private $country_currencies = "tbl_country_currency";
    private $categories = "tbl_category_master";
    private $p2p_rates = "tbl_customer_point_to_point_rates";
    private $states = "tbl_states";
    private $cities = "tbl_cities";
    private $standard_dimension = "tbl_standard_dimension";
    private $_dedicated_customer_gonagoo_commission = "tbl_dedicated_customer_gonagoo_commission";
    private $_deliverer_ratings = "tbl_deliverer_ratings";
    private $_global_workroom = 'tbl_global_workroom';
    private $_bus_booking_master = 'tbl_bus_booking_master';
    private $_bus_operator_profile = 'tbl_bus_operator_profile';
    private $_bus_master = 'tbl_bus_master';
    private $_bus_trip_master = 'tbl_bus_trip_master';
    private $_bus_locations_pickup_drop_points = 'tbl_bus_locations_pickup_drop_points';
    private $_laundry_booking = 'tbl_laundry_booking';
    private $_career_enquiry = 'tbl_career_enquiry';
    private $_promo_code = 'tbl_promo_code';
    private $_promocode_used = 'tbl_promocode_used';*/


  public function get_m4_category_type() {
    $this->db->where('cat_type_status', 1);
    return $this->db->get($this->_category_type_master)->result_array();
  }
  public function get_service_providers_list() {
    return $this->db->get($this->_smp_freelancer_profile)->result_array();
  }


  /*public function get_application_parameters()
  {
    return $this->db->get($this->_app_parameter)->row();
  }

  public function is_deviceId_exists($deviceId=null, $cust_id=null)
  {
    if($deviceId != null) {
      $this->db->where('device_id', trim($deviceId));
      $this->db->where('cust_id', trim($cust_id));
      return $this->db->get($this->_consumer_device)->row();
    }
    return false;
  }

  public function register_new_device(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_consumer_device, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function register_user_login_history(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_cust_login_stats, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_device_detail($device_id=null, array $data, $cust_id=null)
  {
    if($device_id != null AND is_array($data)) {
      $this->db->where('device_id', trim($device_id));
      $this->db->where('cust_id', trim($cust_id));
      $this->db->update($this->_consumer_device, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function is_emailId_exists($emailId=null, $all=false)
  {
    if($emailId != null) {
      if(!$all){
        $this->db->select('cust_id, firstname, lastname, email1, mobile1, password, cust_dob, gender, city_id, state_id, country_id, avatar_url, cover_url,  email_verified, mobile_verified, cre_datetime, last_login_datetime, profession, overview,  cust_status, social_login, social_type, user_type, lang_known, is_deliverer, ratings, total_ratings, no_of_ratings, cust_otp, acc_type, is_dedicated, allow_payment_delay, commission_type');
      }
      $this->db->where('email1', trim($emailId));
      return $this->db->get($this->_customers_master)->row_array();
    }
    return false;
  }

  public function is_user_active_or_exists($cust_id=0)
  {
    if($cust_id > 0 ) {     
      $this->db->where('cust_id', (int)($cust_id));
      $this->db->where('cust_status', 1);
      if($this->db->get($this->_customers_master)->row_array()){  return true;  }
    }
    return false;
  }

  public function get_consumer_datails($cust_id=0, $all=false)
  {
    if($cust_id > 0 ) {
      if(!$all) {
        $this->db->select('cust_id, firstname, lastname, email1, mobile1, cust_dob, gender, city_id, state_id, country_id, avatar_url, cover_url,  email_verified,  mobile_verified, cre_datetime, last_login_datetime, profession, overview,  cust_status, social_login, social_type, user_type, lang_known, is_deliverer, ratings, total_ratings, no_of_ratings, cust_otp, acc_type, is_dedicated, allow_payment_delay, commission_type');
      }
      $this->db->where('cust_id', (int)($cust_id));
      $this->db->where('cust_status', 1);
      return $this->db->get($this->_customers_master)->row_array();
    }
    return false;
  }
  
  public function validate_old_password($cust_id=0, $old_pass=null)
  {
    if($cust_id > 0 AND $old_pass != null) {
      $this->db->select('cust_id');
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('password', trim($old_pass));
      if( $return = $this->db->get($this->_customers_master)->row()){ return $return->cust_id;  }
    }
    return false;
  }

  public function update_new_password(array $data)
  {
    if($data['password'] != null && !empty($data['password'])) {
      $today = date("Y-m-d H:i:s");
      $this->db->where('cust_id', (int) $data['cust_id']);
      $this->db->update($this->_customers_master, ['password' => $data['password'], 'last_login_datetime' => $today]);
      return $this->db->affected_rows();
    }
    return false;
  }
  
  public function validate_login($email=null, $password=null)
  {
    if($email !=null AND $password != null) {
      $this->db->select('cust_id');
      $this->db->where('email1', trim($email));
      $this->db->where('password', trim($password));
      if( $return = $this->db->get($this->_customers_master)->row()){ return $return->cust_id;  }
    }
    return false;
  } 

  public function register_consumer(array $data)
  { //echo json_encode($data); die();
    if(is_array($data)){
      $today = date("Y-m-d H:i:s");
      $isnert_data = array(
        "firstname" => trim($data['firstname']), 
        "lastname" => trim($data['lastname']), 
        "email1" => trim($data['email_id']),   
        "password" => trim($data['password']),    
        "mobile1" => trim($data['mobile_no']),
        "gender" => trim($data['gender']),  
        "city_id" => (int) $data['city_id'],  
        "state_id" => (int) $data['state_id'], 
        "country_id" => (int) $data['country_id'],
        "user_type" => (int) $data['usertype'], 
        "avatar_url" => trim($data['avatar_url']), 
        "cover_url" => trim($data['cover_url']), 
        "social_login" => (int) $data['social_login'], 
        "activation_hash" => $data['activation_hash'], 
        "cust_otp" => $data['otp'], 
        "email_verified" => (int) $data['email_verified'], 
        "company_name" => $data['company_name'],            
        "acc_type" => $data['acc_type'],            
        "cre_datetime" => $today, 
        "last_login_datetime" => $today, 
        "cust_status" => 1, 
        "overview" => "NULL", 
        "social_type" => $data['social_type'], 
        "lang_known" => "NULL", 
        "is_deliverer" => $data['is_deliverer'],            
      );
//echo json_encode($isnert_data); die();
      $this->db->insert($this->_customers_master, $isnert_data);
      return $this->db->insert_id();
    }
    return false;
  }   

  public function make_consumer_deliverer($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->where('is_deliverer', 0);
      $this->db->update($this->_customers_master, array("is_deliverer" => 1));
    }
    return false;
  }

  public function register_nulled_skilled($cust_id=0)
  {
    if($cust_id > 0 ){
      $nulled_data = array(
        "cust_id" => (int) $cust_id,
        "junior_skills" => "NULL",
        "confirmed_skills" => "NULL",
        "senior_skills" => "NULL",
        "expert_skills" => "NULL",
        "ext1" => "NULL",
        "ext2" => "NULL",
        "cre_datetime" => "NULL",
      );
      $this->db->insert($this->_skilled, $nulled_data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function sendEmail($user_name, $user_email, $subject=null, $message=null) 
  {

      $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
      $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
      
      $messageBody .="</head><body bgcolor='#FFFFFF'>";
      $messageBody .="<table class='body-wrap'><tr><td></td><td class='container'><div class='content'>";
      $messageBody .="<table><tr><td align='center'><h3>".$subject."</h3><p class='lead'>Please verify email address as provided.</p></td></tr>";
      $messageBody .="<tr><td align='center'><h3>Dear <small>".$user_name."</small>,</h3>";
      $messageBody .= $message;
      $messageBody .="</td></tr>";
      $messageBody .="<tr><td align='center'><br /><br /><h5>Unauthorized Access</h5>";
      $messageBody .="<p>Email ignorance message.</p></td></tr>";
      $messageBody .="</table></div></td><td></td></tr></table>";

      //Email Signature
      $messageBody .="<table class='head-wrap'>";
      $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
      $messageBody .="<tr><td>Your Gonagoo team !</td></tr>";
      $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />Deliver, ship cheaper easily</td></tr>";
      $messageBody .="<tr><td>Support : support@gonagoo.com</td></tr>";
      $messageBody .="<tr><td>Website : www.gonagoo.com</td></tr>";
      //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
      $messageBody .="<tr><td>Join us on : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='G+'></a>";
      $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='Twitter'></a>";
      $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='LinkedIn'></a>";
      $messageBody .="<a href='https://www.facebook.com/gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/facebook.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='FB'></a></td></tr>";
      //Download the App <icon for iOS App download><icon for Android App download>
      $messageBody .="<tr><td>Download the App : <br /><a href='https://apple.co/2z5N4jA' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='iOS'></a>";
      $messageBody .="<a href='https://tinyurl.com/ya458s6u' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='Android'></a></td></tr>";
      $messageBody .="</table></div></td><td></td></tr></table>";
      //Email Signature End
      $messageBody .="</body></html>";

      $email_from = $this->config->item('from_email');
      $email_subject = $subject;
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= 'From: Gonagoo <'.$email_from.'>' . "\r\n";
      mail($user_email, $email_subject, $messageBody, $headers);  
      return 1;
  }

  public function update_email_verification($cust_id=null)
  {
    if($cust_id != null) {
      $this->db->where('cust_id', $cust_id);
      $this->db->update($this->_customers_master, array("email_verified" => 1));
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_last_login($cust_id=0)
  {
    if($cust_id>0){
      $today = date('Y-m-d H:i:s');
      $this->db->where('cust_id', $cust_id);
      $this->db->update($this->_customers_master, array("last_login_datetime" => $today));
      return $this->db->affected_rows();
    } 
    return false;
  }

  public function update_driver_count($driver_count=0,$cust_id=0)
  {
    if($cust_id > 0){
      $today = date('Y-m-d H:i:s');
      $this->db->where('cust_id', $cust_id);
      $this->db->update($this->_customers_master, array("last_login_datetime" => $today, "driver_count" => $driver_count));
      return $this->db->affected_rows();
    } 
    return false;
  }

  public function update_user_basic_info($cust_id=0, array $data)
  {
    if($cust_id > 0 && is_array($data)){
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->update($this->_customers_master, $data);
      return $this->db->affected_rows();
    } 
    return false;
  }

  public function update_overview($cust_id=0, array $data)
  {
    if($cust_id > 0 AND is_array($data)){     
      $today = date('Y-m-d H:i:s');
      $this->db->where('cust_id', $cust_id);
      $this->db->update($this->_customers_master, $data);
      return $this->db->affected_rows();
    } 
    return false;
  }

  public function get_old_profile_image($cust_id=0)
  {
    if($cust_id > 0 ){
      $this->db->select('avatar_url');
      $this->db->where('cust_id', (int) $cust_id);
      if( $result = $this->db->get($this->_customers_master)->row_array()){
        return (trim($result['avatar_url']));
      }     
    }
    return false;
  }

  public function get_old_cover_image($cust_id=0)
  {
    if($cust_id > 0 ){
      $this->db->select('cover_url');
      $this->db->where('cust_id', (int) $cust_id);
      if( $result = $this->db->get($this->_customers_master)->row_array()){
        return (trim($result['cover_url']));
      }     
    }
    return false;
  }

  public function update_profile_image($avatar_url=null, $cust_id=0)
  {
    if($avatar_url != null AND $cust_id > 0){
      $this->db->where('cust_id', $cust_id);
      $this->db->update($this->_customers_master, array("avatar_url" => trim($avatar_url)));
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_cover_image($cover_url=null, $cust_id=0)
  {
    if($cover_url != null AND $cust_id > 0){
      $this->db->where('cust_id', $cust_id);
      $this->db->update($this->_customers_master, array("cover_url" => trim($cover_url)));
      return $this->db->affected_rows();
    }
    return false;
  }

  public function register_portfolio($data=null)
  {
    if(is_array($data)){
      $this->db->insert($this->_portfolio, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_portfolio_detail($id=0)
  {
    if($id > 0 ){
      $this->db->where('portfolio_id', (int)$id);
      return $this->db->get($this->_portfolio)->row_array();
    }
    return false;
  }

  public function get_customers_portfolio($cust_id=0)
  {
    if($cust_id > 0 ){
      $this->db->where('cust_id', (int)$cust_id);
      return $this->db->get($this->_portfolio)->result_array();
    }
    return false;
  }

  public function update_portfolio($portfolio_id=0, array $update_data)
  {
    if($portfolio_id > 0 AND  is_array($update_data)){
      $this->db->where('portfolio_id', (int)$portfolio_id);
      $this->db->update($this->_portfolio, $update_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_portfolio($portfolio_id=0)
  {
    if($portfolio_id > 0 ){
      $this->db->where('portfolio_id', $portfolio_id);
      $this->db->delete($this->_portfolio);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_customer_skills($cust_id=0) 
  {
    $this->db->where('cust_id', $cust_id);    
    return $this->db->get($this->_customer_skills)->row_array();  
  }

  public function update_user_skill(array $data)
  {
    if(is_array($data)){
      $this->db->where('cs_id', (int)$data['cs_id']);
      $this->db->update($this->_customer_skills, ['junior_skills' => trim($data['junior_skills']), 'confirmed_skills' => trim($data['confirmed_skills']), 'senior_skills' => trim($data['senior_skills']), 'expert_skills' => trim($data['expert_skills']), 'ext1' => trim($data['ext1']), 'ext2' => trim($data['ext2'])]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_skill_master()
  {
    $this->db->where('skill_status', 1);
    return $this->db->get($this->_skills_master)->result_array();
  }

  public function get_category_type()
  {
    $this->db->where('cat_type_status', 1);
    $this->db->where('cat_type_id', 3);
    return $this->db->get($this->_category_type_master)->result_array();
  }
  
  public function get_category()
  {
    $this->db->where('cat_status', 1);
    return $this->db->get($this->_category_master)->result_array();
  }
  
  public function get_category_by_id($cat_type_id=0)
  {
    if($cat_type_id > 0){
    $this->db->where('cat_type_id', (int)$cat_type_id);
    $this->db->where('cat_status', 1);
    return $this->db->get($this->_category_master)->result_array();
    }
    return false;
  }

  public function get_category_type_with_category()
  {
    $this->db->select('c.*, t.cat_type'); 
    $this->db->from($this->_category_master.' as c');
    $this->db->join($this->_category_type_master.' as t', 'c.cat_type_id = t.cat_type_id');
    $this->db->where('c.cat_status', 1);
    $this->db->where('c.cat_id', 6);
    $this->db->where('c.cat_id', 7);
    $this->db->group_by('c.cat_id');
    return $this->db->get()->result_array();
  }

  public function get_language_master()
  {
    return $this->db->get($this->_language_master)->result_array();
  }

  public function update_user_language(array $data)
  {
    if(is_array($data)){
      $this->db->where('cust_id', (int)$data['cust_id']);
      $today = date('Y-m-d H:i:s');
      $this->db->update($this->_customers_master, ['lang_known' => trim($data['lang_known']), 'last_login_datetime' => $today]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_customer_education($cust_id=0)
  {
    $this->db->where('cust_id', $cust_id);
    return $this->db->get($this->_customer_education)->result_array();
  }

  public function add_user_education(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_customer_education, ['cust_id' => trim($data['cust_id']), 'activities' => trim($data['activities']), 'description' => trim($data['description']), 'start_date' => trim($data['start_date']), 'end_date' => trim($data['end_date']), 'specialization' => trim($data['specialization']), 'qualification' => trim($data['qualification']), 'qualify_year' => trim($data['qualify_year']), 'institute_name' => trim($data['institute_name']), 'institute_address' => trim($data['institute_address']), 'certificate_title' => trim($data['certificate_title']), 'grade' => trim($data['grade']), 'remark' => trim($data['remark']), 'cre_datetime' => trim($data['cre_datetime']), 'mod_datetime' => trim($data['mod_datetime'])]);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_user_education(array $data)
  {
    if(is_array($data)){
      $this->db->where('edu_id', (int)$data['edu_id']);
      $this->db->update($this->_customer_education, ['activities' => trim($data['activities']), 'description' => trim($data['description']), 'start_date' => trim($data['start_date']), 'end_date' => trim($data['end_date']), 'specialization' => trim($data['specialization']), 'qualification' => trim($data['qualification']), 'qualify_year' => trim($data['qualify_year']), 'institute_name' => trim($data['institute_name']), 'institute_address' => trim($data['institute_address']), 'certificate_title' => trim($data['certificate_title']), 'grade' => trim($data['grade']), 'remark' => trim($data['remark']), 'mod_datetime' => trim($data['mod_datetime'])]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_user_education($edu_id=0)
  {
    if($edu_id>0){
      $this->db->where('edu_id', $edu_id);
      $this->db->delete($this->_customer_education);
      return $this->db->affected_rows();
    }
    return false;
  }
  
  public function get_customer_experience($cust_id=0)
  {
    $this->db->where('cust_id', $cust_id);
    return $this->db->get($this->_customer_experience)->result_array();
  }

  public function add_user_experience(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_customer_experience, ['cust_id' => trim($data['cust_id']), 'org_name' => trim($data['org_name']), 'job_title' => trim($data['job_title']), 'job_location' => trim($data['job_location']), 'job_desc' => trim($data['job_desc']), 'start_date' => trim($data['start_date']), 'end_date' => trim($data['end_date']), 'remark' => trim($data['remark']), 'grade' => trim($data['grade']), 'other' => trim($data['other']), 'cre_datetime' => trim($data['cre_datetime']), 'mod_datetime' => trim($data['mod_datetime']), 'currently_working' => $data['currently_working']]);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_user_experience(array $data)
  {
    if(is_array($data)){
      $this->db->where('exp_id', (int)$data['exp_id']);
      $this->db->update($this->_customer_experience, ['org_name' => trim($data['org_name']), 'job_title' => trim($data['job_title']), 'job_location' => trim($data['job_location']), 'job_desc' => trim($data['job_desc']), 'start_date' => trim($data['start_date']), 'end_date' => trim($data['end_date']), 'remark' => trim($data['remark']), 'grade' => trim($data['grade']), 'other' => trim($data['other']), 'mod_datetime' => trim($data['mod_datetime']), 'currently_working' => $data['currently_working']]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_user_experience($exp_id=0)
  {
    if($exp_id>0){
      $this->db->where('exp_id', $exp_id);
      $this->db->delete($this->_customer_experience);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_permitted_cat()
  {
    return $this->db->get($this->_permitted_category)->result_array();
  }

  public function get_customer_drivers($cust_id=0)
  {
    $this->db->where('cust_id', $cust_id);
    $this->db->where('status', '1');
    return $this->db->get($this->_customer_drivers)->result_array();
  }

  public function is_driver_exists($emailId=null)
  {
    if($emailId != null) {
      $this->db->where('email', ($emailId));
      if($this->db->get($this->_customer_drivers)->row()){
        return true;
      }
    }
    return false;
  }

  public function is_driver_exists_excluding($emailId=null, $cd_id=0)
  {
    if($emailId != null || $cd_id > 0 ) {
      $cd_id = (int)$cd_id;
      $this->db->where('cd_id !=', $cd_id );
      $this->db->where('email', $emailId );
      if($this->db->get($this->_customer_drivers)->row()){
        return true;
      }
    }
    return false;
  }

  public function register_driver(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_customer_drivers, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function sendEmailDriver($user_name, $user_email, $subject=null, $message=null) 
  {
    $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
    $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />"; 
    $messageBody .="</head><body bgcolor='#FFFFFF'>";
   
    $messageBody .="<table class='body-wrap'><tr><td></td><td class='container'><div class='content'>";
    $messageBody .="<table><tr><td align='center'><h3>".$subject."</h3></td></tr>";
    $messageBody .="<tr><td align='center'><h3>Dear <small>".$user_name."</small>,</h3>";
    $messageBody .= $message;
    $messageBody .="</td></tr></table></div></td><tr/></table>";
    //Email Signature
    $messageBody .="<table class='head-wrap'>";
    $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
    $messageBody .="<tr><td>Your Gonagoo team !</td></tr>";
    $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />Deliver, ship cheaper easily</td></tr>";
    $messageBody .="<tr><td>Support : support@gonagoo.com</td></tr>";
    $messageBody .="<tr><td>Website : www.gonagoo.com</td></tr>";
    //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
    $messageBody .="<tr><td>Join us on : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='G+'></a>";
    $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='Twitter'></a>";
    $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='LinkedIn'></a>";
    $messageBody .="<a href='https://www.facebook.com/gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/facebook.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='FB'></a></td></tr>";
    //Download the App <icon for iOS App download><icon for Android App download>
    $messageBody .="<tr><td>Download the App : <br /><a href='https://apple.co/2z5N4jA' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='iOS'></a>";
      $messageBody .="<a href='https://tinyurl.com/ya458s6u' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='Android'></a></td></tr>";
      $messageBody .="</table></div></td><td></td></tr></table>";
    //Email Signature End
    $messageBody .="</body></html>";

    $email_from = $this->config->item('from_email');
    $email_subject = $subject;
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: Gonagoo <'.$email_from.'>' . "\r\n";
    mail($user_email, $email_subject, $messageBody, $headers);  
    return 1;
  }

  public function sendSMS_old($user_mobile, $message=null)
  {
    if($message!=null) {
      $ch = curl_init("http://103.16.101.52:8080/sendsms/bulksms?"); // url to send sms
      curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
      curl_setopt($ch, CURLOPT_POST, 1); // method to call url
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
      curl_setopt($ch, CURLOPT_POSTFIELDS,"username=kotp-noorisys&password=kaptrans&type=0&dlr=1&destination=$user_mobile&source=EZAMNA&message=$message"); 
      $outputSMS = curl_exec($ch); // execute url and save response
      curl_close($ch); // close url connection
      return 1;
    }
  }

  public function sendSMS_twilio($user_mobile=null, $message=null)
  {
    if($message!=null && $user_mobile!=null) {
      $message .= ' - Your Team Gonagoo!';
      $uri = 'https://api.twilio.com/2010-04-01/Accounts/ACcd68595e7c0ea2401b0da3764c0e8aaf/SMS/Messages';
      $auth = 'ACcd68595e7c0ea2401b0da3764c0e8aaf:30f962c9cf51541b3ef14822bdcf1d63';
      $fields = 
        '&To=' .  urlencode( '+'.$user_mobile ) . 
        '&From=' . urlencode( '+33757905310' ) . 
        '&Body=' . urlencode( $message );
      $res = curl_init();
      curl_setopt( $res, CURLOPT_URL, $uri );
      curl_setopt( $res, CURLOPT_POST, 3 ); // number of fields
      curl_setopt( $res, CURLOPT_POSTFIELDS, $fields );
      curl_setopt( $res, CURLOPT_USERPWD, $auth ); // authenticate
      curl_setopt( $res, CURLOPT_RETURNTRANSFER, true ); // don't echo
      //$result = curl_exec( $res );
      //var_dump($result); die();
      return true;
    }
  }

  public function sendSMS($user_mobile=null, $message=null)
  {
    if($message!=null && $user_mobile!=null) {
      $message .= ' - Your Team Gonagoo!';
      if(substr($user_mobile, 0, 1) == 0) { $user_mobile = ltrim($user_mobile, 0); }
      $ch = curl_init("http://smsc.txtnation.com:8091/sms/send_sms.php?"); // url to send sms
      curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
      curl_setopt($ch, CURLOPT_POST, 1); // method to call url
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
      curl_setopt($ch, CURLOPT_POSTFIELDS,"src=330679328525&dst=$user_mobile&dr=1&user=nkengne&password=P9cKxW&type=0&msg=$message"); 
      $outputSMS = curl_exec($ch); // execute url and save response
      //var_dump($outputSMS); die();
      curl_close($ch); // close url connection
      return true;
    }
  }

  public function activate_driver($id=0)
  {
    if($id > 0){
      $this->db->where('cd_id', (int) $id);
      $this->db->update($this->_customer_drivers, ['status' => 1, 'mod_datetime' => date('Y-m-d H:i:s')]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function inactivate_driver($id=0)
  {
    if($id > 0){
      $this->db->where('cd_id', (int) $id);
      $this->db->update($this->_customer_drivers, ['status' => 0, 'mod_datetime' => date('Y-m-d H:i:s')]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_customer_drivers_inactive($cust_id=0)
  {
    $this->db->where('cust_id', $cust_id);
    $this->db->where('status', 0);
    return $this->db->get($this->_customer_drivers)->result_array();
  }

  public function update_driver(array $data, $cd_id=0)
  {
    if(is_array($data) || $cd_id > 0 ){
      $cd_id = (int)$cd_id;
      $this->db->where('cd_id', $cd_id);
      $this->db->update($this->_customer_drivers, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_old_avatar_driver($cd_id=0)
  {
    if($cd_id > 0 ){
      $this->db->select('avatar');
      $this->db->where('cd_id', (int) $cd_id);
      if( $result = $this->db->get($this->_customer_drivers)->row_array()){
        if($result['avatar'] != 'NULL') { unlink(trim($result['avatar'])); }
      }     
    }
    return false;
  }

  public function update_avatar_driver($avatar=null, $cd_id=0)
  {
    if($avatar != null AND $cd_id > 0){
      $this->db->where('cd_id', $cd_id);
      $this->db->update($this->_customer_drivers, array("avatar" => trim($avatar)));
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_driver_datails($cd_id=0)
  {
    $this->db->where('cd_id', $cd_id);
    return $this->db->get($this->_customer_drivers)->result_array();
  }

  public function get_customer_service_area($cust_id=0)
  {
    $this->db->where('cust_id', $cust_id);
    return $this->db->get($this->_customer_service_areas)->result_array();
  }

  public function register_service_area(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_customer_service_areas, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function delete_service_area($sa_id=0)
  {
    if($sa_id > 0){
      $this->db->where('sa_id', $sa_id);
      return $this->db->delete($this->_customer_service_areas);
    }
    return false;
  }

  public function register_deliverer(array $insert_data)
  {
    if(is_array($insert_data)){     
      $this->db->insert($this->_deliverer, $insert_data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_deliverer_profile($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      return $this->db->get($this->_deliverer)->row_array();
    }
    return false;
  }

  public function is_deliverer_email_exists($email_id=null, $cust_id=0)
  {
    if($email_id != null ) {
      if($cust_id > 0){ $this->db->where('cust_id !=', (int)$cust_id); }
      $this->db->where('email_id', trim($email_id));
      return $this->db->get($this->_deliverer)->row_array();
    }
    return false;
  }

  public function update_deliverer(array $update_data, $cust_id=0)
  {
    if(is_array($update_data) && $cust_id > 0 ){
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->update($this->_deliverer, $update_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_business_datails($id=0)
  {
    if($id>0){
      $this->db->where('cust_id', (int) $id);
      return $this->db->get($this->_deliverer)->row_array();
    }
    return false;
  }

  public function delete_old_business_avatar($cust_id=0)
  {
    if($cust_id > 0 ){
      $this->db->select('avatar_url');
      $this->db->where('cust_id', (int) $cust_id);
      if( $result = $this->db->get($this->_deliverer)->row_array()){
        unlink(trim($result['avatar_url']));
      }     
    }
    return false;
  }

  public function delete_old_business_cover($cust_id=0)
  {
    if($cust_id > 0 ){
      $this->db->select('cover_url');
      $this->db->where('cust_id', (int) $cust_id);
      if( $result = $this->db->get($this->_deliverer)->row_array()){
        unlink(trim($result['cover_url']));
      }     
    }
    return false;
  }

  public function update_business_avatar($avatar_url=null, $cust_id=0)
  {
    if($avatar_url != null AND $cust_id > 0){
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->update($this->_deliverer, array("avatar_url" => trim($avatar_url)));
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_business_cover($cover_url=null, $cust_id=0)
  {
    if($cover_url != null ANd $cust_id > 0){
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->update($this->_deliverer, array("cover_url" => trim($cover_url)));
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_business_info(array $data, $cust_id=0)
  {
    if( is_array($data) AND $cust_id > 0) {
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->update($this->_deliverer, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function add_business_photo($photo_url=null, $cust_id=0)
  {
    if($photo_url != null AND  $cust_id > 0){
      $this->db->insert($this->_business_photos, array("photo_url" => trim($photo_url), "cust_id" => (int)$cust_id));
      return $this->db->insert_id();
    }
    return false;
  }

  public function delete_business_photo($photo_id=0)
  {
    if($photo_id > 0 ){
      $this->db->select('photo_url');
      $this->db->where('photo_id', (int) $photo_id);
      if( $result = $this->db->get($this->_business_photos)->row_array()){
        if(unlink(trim($result['photo_url']))){ 
          $this->db->where('photo_id', $photo_id);
          $this->db->delete($this->_business_photos);
          return true; 
        }
      }     
    }
    return false;
  }

  public function get_business_photos($cust_id=0)
  {
    if($cust_id > 0 ){
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('photo_id', 'desc');
      return $this->db->get($this->_business_photos)->result_array();
    }
    return false;
  }
  
  public function add_transport_details(array $insert_data)
  {
    if(is_array($insert_data)){
      $this->db->insert($this->_transport_details, $insert_data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_transport_details(array $update_data, $cust_id=0, $transport_id=0)
  {
    if(is_array($update_data) AND $cust_id > 0 AND $transport_id > 0){
      $this->db->where('transport_id', (int)$transport_id);
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->update($this->_transport_details, $update_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_transport_details($transport_id=0)
  {
    if($transport_id > 0 ){
      $this->db->where('transport_id', (int) $transport_id);
      $this->db->delete($this->_transport_details);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_consumers_transport_list($cust_id=0)
  {
    if($cust_id > 0 ){
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('transport_id', 'desc');
      return $this->db->get($this->_transport_details)->result_array();
    }
    return false;
  }
 
  public function get_vehicle_type_name_by_id($vehical_type_id=0)
  {
    if($vehical_type_id > 0 ){
      $this->db->where('vehical_type_id', (int) $vehical_type_id);
      $row = $this->db->get($this->_transport_master)->row_array();
      return $row['vehicle_type'];
    }
    return false;
  }

  public function get_transport_master($active=false)
  {
    if($active==true) { $this->db->where('vehicle_status', 1); }
    $this->db->order_by('vehical_type_id', 'ASC');
    return $this->db->get($this->_transport_master)->result_array();    
  }

  public function get_unit_master($active=false)
  {
    if($active==true) { $this->db->where('unit_status', 1); }
    $this->db->order_by('unit_id', 'desc');
    return $this->db->get($this->_unit_master)->result_array();
  } 

  public function get_consumers_addressbook($cust_id=0, array $data)
  {
    if($cust_id > 0 ){
        if(!empty($data)) {
            if($data['country_id']) { $this->db->where('country_id', $data['country_id']);  }
            if($data['state_id']) { $this->db->where('state_id', $data['state_id']);  }
            if($data['city_id']) { $this->db->where('city_id', $data['city_id']);  }
        }
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('addr_id', 'desc');
        return $this->db->get($this->_addressbook)->result_array();
    }
    return false;
  }

  public function delete_old_address_image($image_url=null, $addr_id=0)
  {
    if($addr_id > 0){
      $this->db->select('image_url');
      $this->db->where('addr_id', (int) $addr_id);
      if( $result = $this->db->get($this->_addressbook)->row_array()){
        unlink(trim($result['image_url']));
      } 
    }
    return false;
  }

  public function update_address_image($image_url=null, $addr_id=0)
  {
    if($image_url != null AND $addr_id > 0){
      $this->db->where('addr_id', (int) $addr_id);
      $this->db->update($this->_addressbook, array("image_url" => trim($image_url)));
      return $this->db->affected_rows();
    }
    return false;
  }

  public function register_new_address(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_addressbook, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_existing_address(array $data, $id=0)
  {
    if(is_array($data) && $id > 0 ){
      $this->db->where('addr_id', (int) $id);
      $this->db->update($this->_addressbook, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_address_from_addressbook($addr_id=0)
  {
    if($addr_id > 0) {
      $this->db->where('addr_id', (int) $addr_id);
      $this->db->delete($this->_addressbook);
      return $this->db->affected_rows();
    }
    return false;
  }
  
  public function get_advance_payment($category_id=0, $country_id=0)
  {
    if($category_id > 0 AND $country_id > 0){
      // $this->db->select('advance_percent, commission_percent');
      $this->db->where('country_id', (int)$country_id);
      $this->db->where('category_id', (int) $category_id);
      return $this->db->get('tbl_courier_advance_payment')->row_array();
      // return $return['advance_percent'];
    }
    return false;
  }

  public function get_custom_percentage($category_id=0, $country_id=0)
  {
    if($category_id > 0 AND $country_id > 0){
      $this->db->select('EICCPNG, SICCPNG, AICCPNG, EICCPOG, SICCPOG, AICCPOG');
      $this->db->where('country_id', (int)$country_id);
      $this->db->where('category_id', (int) $category_id);
      return $this->db->get($this->_custom_percentage)->row_array();
    }
    return false;
  }

  public function get_standard_dimension_masters()
  {
    $this->db->where('status', 1);
    $this->db->order_by('dimension_id', 'ASC');
    return $this->db->get($this->_standard_dimension)->result_array();
  }

  public function get_standard_dimension_masters_cat_id($id=0, $transport_type=null)
  {
    if($id > 0 && !is_null($transport_type)){
      if($transport_type == 'earth') { $this->db->where('on_earth', 1); }
      if($transport_type == 'air') { $this->db->where('on_air', 1); }
      if($transport_type == 'sea') { $this->db->where('on_sea', 1); }
      $this->db->where('status', 1);
      $this->db->where('category_id', (int)$id);
      $this->db->order_by('dimension_id', 'ASC');
      return $this->db->get($this->_standard_dimension)->result_array();
    }
    return false;
  }

  public function get_standard_dimension_details($dimension_id=0, $category_id=0, $transport_type=null)
  {
    if($dimension_id  > 0 && $category_id  > 0 && !is_null($transport_type)){
      if($transport_type == 'earth') { $this->db->where('on_earth', 1); }
      if($transport_type == 'air') { $this->db->where('on_air', 1); }
      if($transport_type == 'sea') { $this->db->where('on_sea', 1); }
      $this->db->where('status', 1);
      $this->db->where('dimension_id ', (int)$dimension_id );
      $this->db->where('category_id ', (int)$category_id );
      return $this->db->get($this->_standard_dimension)->row_array();
    } return false;
  }

  public function calculate_weight_based_price(array $data)
  {
    if(is_array($data)){
      $total_weight = $data['total_weight'];
      $distance = ROUND($data['distance_in_km']);
      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title"; 
      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('country_id', (int)$data['country_id']);
      $this->db->where('unit_id', (int)$data['unit_id']);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("'".$total_weight."' BETWEEN min_weight AND max_weight");
      $this->db->where("ROUND($distance) BETWEEN min_distance AND max_distance");

      return $this->db->get($this->_standard_rates)->row_array();     
    } 
    return false;
  }
  public function calculate_weight_based_p_to_p_price(array $data)
  {
    //echo json_encode($data); die();
    if(is_array($data)){
     $total_weight = (int) $data['total_weight']; 
      
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];

      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title"; 

      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('from_country_id', (int)$data['from_country_id']);
      $this->db->where('to_country_id', (int)$data['to_country_id']);      
      $this->db->where('from_state_id', (int)$data['from_state_id']);
      $this->db->where('to_state_id', (int)$data['to_state_id']);
      $this->db->where('from_city_id', (int)$data['from_city_id']);
      $this->db->where('to_city_id', (int)$data['to_city_id']);
      $this->db->where('unit_id', (int)$data['unit_id']);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("'".$total_weight."' BETWEEN min_weight AND max_weight");
      $return = $this->db->get($this->_point_to_point_rates)->row_array(); 
      if($return){ return $return;  }    
      else{
        $this->db->select($query);
        $this->db->where('category_id', (int)$data['category_id']);
        $this->db->where('from_country_id', (int)$data['from_country_id']);
        $this->db->where('to_country_id', (int)$data['to_country_id']);      
        $this->db->where('from_state_id', (int)$data['from_state_id']);
        $this->db->where('to_state_id', (int)$data['to_state_id']);
        $this->db->where('unit_id', (int)$data['unit_id']);
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where("'".$total_weight."' BETWEEN min_weight AND max_weight");
        $return_without_city = $this->db->get($this->_point_to_point_rates)->row_array();
        if($return_without_city){ return $return_without_city;  }    
        else{
          $this->db->select($query);
          $this->db->where('category_id', (int)$data['category_id']);
          $this->db->where('from_country_id', (int)$data['from_country_id']);
          $this->db->where('to_country_id', (int)$data['to_country_id']);      
          $this->db->where('unit_id', (int)$data['unit_id']);
          $this->db->where('is_formula_volume_rate', 0);
          $this->db->where('is_formula_weight_rate', 0);
          $this->db->where("'".$total_weight."' BETWEEN min_weight AND max_weight");
          $return_without_state_city = $this->db->get($this->_point_to_point_rates)->row_array();
          if($return_without_state_city){ return $return_without_state_city;  }    
          return false;
        }        
      }
    } 
    return false;
  }
  public function calculate_volume_based_price(array $data)
  {
    if(is_array($data)){
      $volume = $data['volume'];
      $distance = ROUND($data['distance_in_km']);
      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type'];

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title";   
      $this->db->select($query);  
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('country_id', (int)$data['country_id']);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("$volume BETWEEN min_volume AND max_volume");
      $this->db->where("ROUND($distance) BETWEEN min_distance AND max_distance");

      return $this->db->get($this->_standard_rates)->row_array();     
    } 
    return false;
  }
  public function calculate_volume_based_p_to_p_price(array $data)
  {
    if(is_array($data)){
      $volume = $data['volume'];
      $from_country_id = $data['from_country_id'];
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];

      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type'];

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title";   

      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('from_country_id', (int)$data['from_country_id']);
      $this->db->where('to_country_id', (int)$data['to_country_id']);      
      $this->db->where('from_state_id', (int)$data['from_state_id']);
      $this->db->where('to_state_id', (int)$data['to_state_id']);
      $this->db->where('from_city_id', (int)$data['from_city_id']);
      $this->db->where('to_city_id', (int)$data['to_city_id']);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("$volume BETWEEN min_volume AND max_volume");
      $return = $this->db->get($this->_point_to_point_rates)->row_array(); 
      if($return){ return $return;  }    
      else{
        $this->db->select($query);
        $this->db->where('category_id', (int)$data['category_id']);
        $this->db->where('from_country_id', (int)$data['from_country_id']);
        $this->db->where('to_country_id', (int)$data['to_country_id']);      
        $this->db->where('from_state_id', (int)$data['from_state_id']);
        $this->db->where('to_state_id', (int)$data['to_state_id']);
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where("$volume BETWEEN min_volume AND max_volume");
        $return_without_city = $this->db->get($this->_point_to_point_rates)->row_array();
        if($return_without_city){ return $return_without_city;  }    
        else{
          $this->db->select($query);
          $this->db->where('category_id', (int)$data['category_id']);
          $this->db->where('from_country_id', (int)$data['from_country_id']);
          $this->db->where('to_country_id', (int)$data['to_country_id']);
          $this->db->where('is_formula_volume_rate', 0);
          $this->db->where('is_formula_weight_rate', 0);
          $this->db->where("$volume BETWEEN min_volume AND max_volume");
          $return_without_state_city = $this->db->get($this->_point_to_point_rates)->row_array();
          if($return_without_state_city){ return $return_without_state_city;  }    
          return false;
        }        
      }
    } 
    return false;
  }
  public function calculate_formula_weight_based_price(array $data)
  {
    if(is_array($data)){
      $total_weight = $data['total_weight'];
      $distance = ROUND($data['distance_in_km']);
      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title"; 
      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('country_id', (int)$data['country_id']);
      $this->db->where('unit_id', (int)$data['unit_id']);
      $this->db->where('is_formula_weight_rate', 1);
      $this->db->where("ROUND($distance) BETWEEN min_distance AND max_distance");

      return $this->db->get($this->_standard_rates)->row_array();     
    } 
    return false;
  }
  public function calculate_formula_weight_based_p_to_p_price(array $data)
  {
    //echo json_encode($data); die();
    if(is_array($data)){
     $total_weight = (int) $data['total_weight']; 
      
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];

      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title"; 

      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('from_country_id', (int)$data['from_country_id']);
      $this->db->where('to_country_id', (int)$data['to_country_id']);      
      $this->db->where('from_state_id', (int)$data['from_state_id']);
      $this->db->where('to_state_id', (int)$data['to_state_id']);
      $this->db->where('from_city_id', (int)$data['from_city_id']);
      $this->db->where('to_city_id', (int)$data['to_city_id']);
      $this->db->where('unit_id', (int)$data['unit_id']);
      $this->db->where('is_formula_weight_rate', 1);
      $return = $this->db->get($this->_point_to_point_rates)->row_array(); 
      if($return){ return $return;  }    
      else{
        $this->db->select($query);
        $this->db->where('category_id', (int)$data['category_id']);
        $this->db->where('from_country_id', (int)$data['from_country_id']);
        $this->db->where('to_country_id', (int)$data['to_country_id']);      
        $this->db->where('from_state_id', (int)$data['from_state_id']);
        $this->db->where('to_state_id', (int)$data['to_state_id']);
        $this->db->where('unit_id', (int)$data['unit_id']);
        $this->db->where('is_formula_weight_rate', 1);
        $return_without_city = $this->db->get($this->_point_to_point_rates)->row_array();
        if($return_without_city){ return $return_without_city;  }    
        else {
          $this->db->select($query);
          $this->db->where('category_id', (int)$data['category_id']);
          $this->db->where('from_country_id', (int)$data['from_country_id']);
          $this->db->where('to_country_id', (int)$data['to_country_id']);      
          $this->db->where('unit_id', (int)$data['unit_id']);
          $this->db->where('is_formula_weight_rate', 1);
          $return_without_state_city = $this->db->get($this->_point_to_point_rates)->row_array();
          if($return_without_state_city){ return $return_without_state_city;  }    
          return false;
        }        
      }
    } 
    return false;
  }
  public function calculate_formula_volume_based_price(array $data)
  {
    if(is_array($data)){
      $volume = $data['volume'];
      $distance = ROUND($data['distance_in_km']);
      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type'];

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title";   
      $this->db->select($query);  
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('country_id', (int)$data['country_id']);
      $this->db->where('is_formula_volume_rate', 1);
      $this->db->where("ROUND($distance) BETWEEN min_distance AND max_distance");

      return $this->db->get($this->_standard_rates)->row_array();     
    } 
    return false;
  }
  public function calculate_formula_volume_based_p_to_p_price(array $data)
  {
    if(is_array($data)){
      $volume = $data['volume'];
      $from_country_id = $data['from_country_id'];
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];

      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type'];

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title";   

      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('from_country_id', (int)$data['from_country_id']);
      $this->db->where('to_country_id', (int)$data['to_country_id']);      
      $this->db->where('from_state_id', (int)$data['from_state_id']);
      $this->db->where('to_state_id', (int)$data['to_state_id']);
      $this->db->where('from_city_id', (int)$data['from_city_id']);
      $this->db->where('to_city_id', (int)$data['to_city_id']);
      $this->db->where('is_formula_volume_rate', 1);
      $return = $this->db->get($this->_point_to_point_rates)->row_array(); 
      if($return){ return $return;  }    
      else{
        $this->db->select($query);
        $this->db->where('category_id', (int)$data['category_id']);
        $this->db->where('from_country_id', (int)$data['from_country_id']);
        $this->db->where('to_country_id', (int)$data['to_country_id']);      
        $this->db->where('from_state_id', (int)$data['from_state_id']);
        $this->db->where('to_state_id', (int)$data['to_state_id']);
        $this->db->where('is_formula_volume_rate', 1);
        $return_without_city = $this->db->get($this->_point_to_point_rates)->row_array();
        if($return_without_city){ return $return_without_city;  }    
        else{
          $this->db->select($query);
          $this->db->where('category_id', (int)$data['category_id']);
          $this->db->where('from_country_id', (int)$data['from_country_id']);
          $this->db->where('to_country_id', (int)$data['to_country_id']);
          $this->db->where('is_formula_volume_rate', 1);
          $return_without_state_city = $this->db->get($this->_point_to_point_rates)->row_array();
          if($return_without_state_city){ return $return_without_state_city;  }
          return false;
        }        
      }
    } 
    return false;
  }

  public function calculate_customer_weight_based_price(array $data)
  {
    if(is_array($data)){
      $total_weight = $data['total_weight'];
      $distance = ROUND($data['distance_in_km']);
      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type']; 
      $cust_id = $data['cust_id']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title"; 
      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('country_id', (int)$data['country_id']);
      $this->db->where('unit_id', (int)$data['unit_id']);
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("'".$total_weight."' BETWEEN min_weight AND max_weight");
      $this->db->where("ROUND($distance) BETWEEN min_distance AND max_distance");

      return $this->db->get($this->rates)->row_array();     
    } 
    return false;
  }
  public function calculate_customer_weight_based_p_to_p_price(array $data)
  {
    //echo json_encode($data); die();
    if(is_array($data)){
     $total_weight = $data['total_weight']; 
      
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];

      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type']; 
      $cust_id = $data['cust_id']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title"; 

      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('from_country_id', (int)$data['from_country_id']);
      $this->db->where('to_country_id', (int)$data['to_country_id']);      
      $this->db->where('from_state_id', (int)$data['from_state_id']);
      $this->db->where('to_state_id', (int)$data['to_state_id']);
      $this->db->where('from_city_id', (int)$data['from_city_id']);
      $this->db->where('to_city_id', (int)$data['to_city_id']);
      $this->db->where('unit_id', (int)$data['unit_id']);
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("'".$total_weight."' BETWEEN min_weight AND max_weight");
      $return = $this->db->get($this->p2p_rates)->row_array(); 
      if($return){ return $return;  }    
      else{
        $this->db->select($query);
        $this->db->where('category_id', (int)$data['category_id']);
        $this->db->where('from_country_id', (int)$data['from_country_id']);
        $this->db->where('to_country_id', (int)$data['to_country_id']);      
        $this->db->where('from_state_id', (int)$data['from_state_id']);
        $this->db->where('to_state_id', (int)$data['to_state_id']);
        $this->db->where('unit_id', (int)$data['unit_id']);
        $this->db->where('cust_id', (int)$cust_id);
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where("'".$total_weight."' BETWEEN min_weight AND max_weight");
        $return_without_city = $this->db->get($this->p2p_rates)->row_array();
        if($return_without_city){ return $return_without_city;  }    
        else{
          $this->db->select($query);
          $this->db->where('category_id', (int)$data['category_id']);
          $this->db->where('from_country_id', (int)$data['from_country_id']);
          $this->db->where('to_country_id', (int)$data['to_country_id']);      
          $this->db->where('unit_id', (int)$data['unit_id']);
          $this->db->where('cust_id', (int)$cust_id);
          $this->db->where('is_formula_volume_rate', 0);
          $this->db->where('is_formula_weight_rate', 0);
          $this->db->where("'".$total_weight."' BETWEEN min_weight AND max_weight");
          $return_without_state_city = $this->db->get($this->p2p_rates)->row_array();
          if($return_without_state_city){ return $return_without_state_city;  }    
          return false;
        }        
      }
    } 
    return false;
  }
  public function calculate_customer_volume_based_price(array $data)
  {
    if(is_array($data)){
      $volume = $data['volume'];
      $distance = ROUND($data['distance_in_km']);
      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type'];
      $cust_id = $data['cust_id']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title";   
      $this->db->select($query);  
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('country_id', (int)$data['country_id']);
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("$volume BETWEEN min_volume AND max_volume");
      $this->db->where("ROUND($distance) BETWEEN min_distance AND max_distance");

      return $this->db->get($this->rates)->row_array();     
    } 
    return false;
  }
  public function calculate_customer_volume_based_p_to_p_price(array $data)
  {
    if(is_array($data)){
      $volume = $data['volume'];
      $from_country_id = $data['from_country_id'];
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];

      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type'];
      $cust_id = $data['cust_id']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title";   

      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('from_country_id', (int)$data['from_country_id']);
      $this->db->where('to_country_id', (int)$data['to_country_id']);      
      $this->db->where('from_state_id', (int)$data['from_state_id']);
      $this->db->where('to_state_id', (int)$data['to_state_id']);
      $this->db->where('from_city_id', (int)$data['from_city_id']);
      $this->db->where('to_city_id', (int)$data['to_city_id']);
      $this->db->where('cust_id', (int)$cust_id);  
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);  
      $this->db->where("$volume BETWEEN min_volume AND max_volume");
      $return = $this->db->get($this->p2p_rates)->row_array(); 
      if($return){ return $return;  }    
      else{
        $this->db->select($query);
        $this->db->where('category_id', (int)$data['category_id']);
        $this->db->where('from_country_id', (int)$data['from_country_id']);
        $this->db->where('to_country_id', (int)$data['to_country_id']);      
        $this->db->where('from_state_id', (int)$data['from_state_id']);
        $this->db->where('to_state_id', (int)$data['to_state_id']);
        $this->db->where('cust_id', (int)$cust_id);
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where("$volume BETWEEN min_volume AND max_volume");
        $return_without_city = $this->db->get($this->p2p_rates)->row_array();
        if($return_without_city){ return $return_without_city;  }    
        else{
          $this->db->select($query);
          $this->db->where('category_id', (int)$data['category_id']);
          $this->db->where('from_country_id', (int)$data['from_country_id']);
          $this->db->where('to_country_id', (int)$data['to_country_id']);
          $this->db->where('cust_id', (int)$cust_id);
          $this->db->where('is_formula_volume_rate', 0);
          $this->db->where('is_formula_weight_rate', 0);     
          $this->db->where("$volume BETWEEN min_volume AND max_volume");
          $return_without_state_city = $this->db->get($this->p2p_rates)->row_array();
          if($return_without_state_city){ return $return_without_state_city;  }    
          return false;
        }        
      }
    } 
    return false;
  }
  public function calculate_customer_formula_weight_based_price(array $data)
  {
    if(is_array($data)){
      $total_weight = $data['total_weight'];
      $distance = ROUND($data['distance_in_km']);
      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type']; 
      $cust_id = $data['cust_id']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title"; 
      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('country_id', (int)$data['country_id']);
      $this->db->where('unit_id', (int)$data['unit_id']);
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('is_formula_weight_rate', 1);
      $this->db->where("ROUND($distance) BETWEEN min_distance AND max_distance");

      return $this->db->get($this->rates)->row_array();     
    } 
    return false;
  }
  public function calculate_customer_formula_weight_based_p_to_p_price(array $data)
  {
    //echo json_encode($data); die();
    if(is_array($data)){
     $total_weight = $data['total_weight']; 
      
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];

      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type']; 
      $cust_id = $data['cust_id']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title"; 

      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('from_country_id', (int)$data['from_country_id']);
      $this->db->where('to_country_id', (int)$data['to_country_id']);      
      $this->db->where('from_state_id', (int)$data['from_state_id']);
      $this->db->where('to_state_id', (int)$data['to_state_id']);
      $this->db->where('from_city_id', (int)$data['from_city_id']);
      $this->db->where('to_city_id', (int)$data['to_city_id']);
      $this->db->where('unit_id', (int)$data['unit_id']);
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('is_formula_weight_rate', 1);
      $return = $this->db->get($this->p2p_rates)->row_array(); 
      if($return){ return $return;  }    
      else{
        $this->db->select($query);
        $this->db->where('category_id', (int)$data['category_id']);
        $this->db->where('from_country_id', (int)$data['from_country_id']);
        $this->db->where('to_country_id', (int)$data['to_country_id']);      
        $this->db->where('from_state_id', (int)$data['from_state_id']);
        $this->db->where('to_state_id', (int)$data['to_state_id']);
        $this->db->where('unit_id', (int)$data['unit_id']);
        $this->db->where('cust_id', (int)$cust_id);
        $this->db->where('is_formula_weight_rate', 1);
        $return_without_city = $this->db->get($this->p2p_rates)->row_array();
        if($return_without_city){ return $return_without_city;  }    
        else{
          $this->db->select($query);
          $this->db->where('category_id', (int)$data['category_id']);
          $this->db->where('from_country_id', (int)$data['from_country_id']);
          $this->db->where('to_country_id', (int)$data['to_country_id']);      
          $this->db->where('unit_id', (int)$data['unit_id']);
          $this->db->where('cust_id', (int)$cust_id);
          $this->db->where('is_formula_weight_rate', 1);
          $return_without_state_city = $this->db->get($this->p2p_rates)->row_array();
          if($return_without_state_city){ return $return_without_state_city;  }    
          return false;
        }        
      }
    } 
    return false;
  }
  public function calculate_customer_formula_volume_based_price(array $data)
  {
    if(is_array($data)){
      $volume = $data['volume'];
      $distance = ROUND($data['distance_in_km']);
      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type'];
      $cust_id = $data['cust_id']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title";   
      $this->db->select($query);  
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('country_id', (int)$data['country_id']);
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('is_formula_volume_rate', 1);
      $this->db->where("ROUND($distance) BETWEEN min_distance AND max_distance");

      return $this->db->get($this->rates)->row_array();     
    } 
    return false;
  }
  public function calculate_customer_formula_volume_based_p_to_p_price(array $data)
  {
    if(is_array($data)){
      $volume = $data['volume'];
      $from_country_id = $data['from_country_id'];
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];

      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $service_area_type = $data['service_area_type'];
      $transport_type = $data['transport_type'];
      $cust_id = $data['cust_id']; 

      $query ="";

      if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
        $query .= 'earth_local_rate as service_rate, earth_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
        $query .= 'earth_national_rate as service_rate, earth_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
        $query .= 'earth_international_rate as service_rate, earth_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
        $query .= 'air_local_rate as service_rate, air_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
        $query .= 'air_national_rate as service_rate, air_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
        $query .= 'air_international_rate as service_rate, air_international_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
        $query .= 'sea_local_rate as service_rate, sea_local_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
        $query .= 'sea_national_rate as service_rate, sea_national_duration as max_duration,'; 
      }
      elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
        $query .= 'sea_international_rate as service_rate, sea_international_duration as max_duration,'; 
      }
      $query .= "currency_id, currency_sign, currency_title";   

      $this->db->select($query);
      $this->db->where('category_id', (int)$data['category_id']);
      $this->db->where('from_country_id', (int)$data['from_country_id']);
      $this->db->where('to_country_id', (int)$data['to_country_id']);      
      $this->db->where('from_state_id', (int)$data['from_state_id']);
      $this->db->where('to_state_id', (int)$data['to_state_id']);
      $this->db->where('from_city_id', (int)$data['from_city_id']);
      $this->db->where('to_city_id', (int)$data['to_city_id']);
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('is_formula_volume_rate', 1);
      $return = $this->db->get($this->p2p_rates)->row_array(); 
      if($return){ return $return;  }    
      else{
        $this->db->select($query);
        $this->db->where('category_id', (int)$data['category_id']);
        $this->db->where('from_country_id', (int)$data['from_country_id']);
        $this->db->where('to_country_id', (int)$data['to_country_id']);      
        $this->db->where('from_state_id', (int)$data['from_state_id']);
        $this->db->where('to_state_id', (int)$data['to_state_id']);
        $this->db->where('cust_id', (int)$cust_id);
        $this->db->where('is_formula_volume_rate', 1);
        $return_without_city = $this->db->get($this->p2p_rates)->row_array();
        if($return_without_city){ return $return_without_city;  }    
        else{
          $this->db->select($query);
          $this->db->where('category_id', (int)$data['category_id']);
          $this->db->where('from_country_id', (int)$data['from_country_id']);
          $this->db->where('to_country_id', (int)$data['to_country_id']);
          $this->db->where('cust_id', (int)$cust_id);
          $this->db->where('is_formula_volume_rate', 1);
          $return_without_state_city = $this->db->get($this->p2p_rates)->row_array();
          if($return_without_state_city){ return $return_without_state_city;  }
          return false;
        }        
      }
    } 
    return false;
  }

  public function calculate_order_price(array $data)
  {
    //echo json_encode($data); die();
    $transport_type = $data['transport_type'];
    $service_area_type = $data['service_area_type'];
    $custom_clearance_by = $data['custom_clearance_by'];
    $custom_package_value = $data['custom_package_value'];
    $old_new_goods = $data['old_new_goods'];
    $category_id= $data['category_id'];
    $loading_time = $data['loading_time'];
    
    if(array_key_exists("laundry",$data)) {
      $laundry = $data['laundry'];
    } else { $laundry = 0; }

    $cust_id = $data['cust_id'];
    $user_details = $this->get_user_details((int)$cust_id);
    //echo json_encode($user_details); die();

    $loading_unloading_charge = 0;  $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;

    $delivery_hours = ROUND((strtotime($data['delivery_datetime']) - strtotime($data['pickup_datetime']))/3600);
    $delivery_days = ROUND($delivery_hours/24); 
    
    $distance_in_km=$this->GetDrivingDistance($data['from_latitude'],$data['from_longitude'], $data['to_latitude'],$data['to_longitude']);

    if($user_details['is_dedicated'] == "1" && $laundry == 0) { //if user is dedicated apply user prices
      // Point to Point / Standard - Weight Based Price
      $p_to_p_weight_based = array(
        "category_id" => $data['category_id'],
        "total_weight" => $data['total_weight'],
        "unit_id" => $data['unit_id'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
        "cust_id" => $data['cust_id'],
      );
      if(!$weight_based_price_data = $this->calculate_customer_weight_based_p_to_p_price($p_to_p_weight_based)) {
        $weight_based = array(
          "category_id" => $data['category_id'],
          "total_weight" => $data['total_weight'],
          "unit_id" => $data['unit_id'],
          "country_id" => $data['from_country_id'],
          "state_id" => $data['from_state_id'],
          "city_id" => $data['from_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
          "cust_id" => $data['cust_id'],
        );
        $weight_based_price_data = $this->calculate_customer_weight_based_price($weight_based);      
      }
      // Point to Point / Standard - Formula Weight Based Price
      $p_to_p_formula_weight_based = array(
        "category_id" => $data['category_id'],
        "total_weight" => $data['total_weight'],
        "unit_id" => $data['unit_id'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
        "cust_id" => $data['cust_id'],
      );
      if(!$formula_weight_based_price_data = $this->calculate_customer_formula_weight_based_p_to_p_price($p_to_p_formula_weight_based)) {
        $weight_based = array(
          "category_id" => $data['category_id'],
          "total_weight" => $data['total_weight'],
          "unit_id" => $data['unit_id'],
          "country_id" => $data['from_country_id'],
          "state_id" => $data['from_state_id'],
          "city_id" => $data['from_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
          "cust_id" => $data['cust_id'],
        );
        $formula_weight_based_price_data = $this->calculate_customer_formula_weight_based_price($weight_based);      
      }

      // Point to Point / Standard - Volume Based Price
      $p_to_p_volume_based = array(
        "category_id" => $data['category_id'],
        "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
        "cust_id" => $data['cust_id'],
      );
      if(!$volume_based_price_data = $this->calculate_customer_volume_based_p_to_p_price($p_to_p_volume_based) ){
        $volume_based = array(
          "category_id" => $data['category_id'],
          "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
          "country_id" => $data['from_country_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
          "cust_id" => $data['cust_id'],
        );
        $volume_based_price_data = $this->calculate_customer_volume_based_price($volume_based);
      }
      // Point to Point / Standard - Formula Volume Based Price
      $p_to_p_formula_volume_based = array(
        "category_id" => $data['category_id'],
        "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
        "cust_id" => $data['cust_id'],
      );
      if(!$formula_volume_based_price_data = $this->calculate_customer_formula_volume_based_p_to_p_price($p_to_p_formula_volume_based) ){
        $volume_based = array(
          "category_id" => $data['category_id'],
          "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
          "country_id" => $data['from_country_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
          "cust_id" => $data['cust_id'],
        );
        $formula_volume_based_price_data = $this->calculate_customer_formula_volume_based_price($volume_based);
      }

      if (is_array($weight_based_price_data) || is_array($volume_based_price_data) || is_array($formula_weight_based_price_data) || is_array($formula_volume_based_price_data)){
        if($formula_weight_based_price_data!=null && $formula_volume_based_price_data!=null) {
          if ( (($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km) <= (((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km) ) { 
            //Volume*Rate*Distance  
            $core_price = ((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km;
            $currency_id = $formula_volume_based_price_data['currency_id'];
            $currency_sign = $formula_volume_based_price_data['currency_sign'];
            $currency_title = $formula_volume_based_price_data['currency_title'];
            $max_duration = $formula_volume_based_price_data['max_duration'];
          } else { 
            //Total Weight*Rate*Distance  
            $core_price = ($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km;
            $currency_id = $formula_weight_based_price_data['currency_id'];
            $currency_sign = $formula_weight_based_price_data['currency_sign'];
            $currency_title = $formula_weight_based_price_data['currency_title'];
            $max_duration = $formula_weight_based_price_data['max_duration'];
          }
        } else if($formula_weight_based_price_data!=null) {
          //Total Weight*Rate*Distance  
          $core_price = ($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km;
          $currency_id = $formula_weight_based_price_data['currency_id'];
          $currency_sign = $formula_weight_based_price_data['currency_sign'];
          $currency_title = $formula_weight_based_price_data['currency_title'];
          $max_duration = $formula_weight_based_price_data['max_duration'];
        } else if($formula_volume_based_price_data!=null){
          //Volume*Rate*Distance  
          $core_price = ((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km;
          $currency_id = $formula_volume_based_price_data['currency_id'];
          $currency_sign = $formula_volume_based_price_data['currency_sign'];
          $currency_title = $formula_volume_based_price_data['currency_title'];
          $max_duration = $formula_volume_based_price_data['max_duration'];
        } else {
          if($weight_based_price_data!=null && $volume_based_price_data!=null) {
            if ($weight_based_price_data['service_rate'] <= $volume_based_price_data['service_rate']){ 
              $core_price = $volume_based_price_data['service_rate'];
              $currency_id = $volume_based_price_data['currency_id'];
              $currency_sign = $volume_based_price_data['currency_sign'];
              $currency_title = $volume_based_price_data['currency_title'];
              $max_duration = $volume_based_price_data['max_duration'];
            } else { 
              $core_price = $weight_based_price_data['service_rate'];
              $currency_id = $weight_based_price_data['currency_id'];
              $currency_sign = $weight_based_price_data['currency_sign'];
              $currency_title = $weight_based_price_data['currency_title'];
              $max_duration = $weight_based_price_data['max_duration'];
            }
          } else if($weight_based_price_data!=null) {
            $core_price = $weight_based_price_data['service_rate'];
            $currency_id = $weight_based_price_data['currency_id'];
            $currency_sign = $weight_based_price_data['currency_sign'];
            $currency_title = $weight_based_price_data['currency_title'];
            $max_duration = $weight_based_price_data['max_duration'];
          } else if($volume_based_price_data!=null) {
            $core_price = $volume_based_price_data['service_rate'];
            $currency_id = $volume_based_price_data['currency_id'];
            $currency_sign = $volume_based_price_data['currency_sign'];
            $currency_title = $volume_based_price_data['currency_title'];
            $max_duration = $volume_based_price_data['max_duration'];
          } else { 
            return array(
              "total_price" => 0,
              "standard_price" => 0,
              "urgent_fee" => 0,
              "ins_fee" => 0,
              "handling_fee" => 0,
              "dedicated_vehicle_fee" => 0,
              "currency_id" => 0,
              "currency_sign" => "",
              "currency_title" => "",
              "distance_in_km" => $distance_in_km,
              "advance_percent" => 0,       
              "advance_payment" => 0,
              "commission_percent" => 0,        
              "old_goods_custom_commission_percent" => 0,        
              "old_goods_custom_commission_fee" => 0,       
              "new_goods_custom_commission_percent" => 0,        
              "new_goods_custom_commission_fee" => 0,    
              "loading_unloading_charge" => 0,    
            );
          }
        }
    
        // ScheduleTimeUntilDelivery = ("Delivery_date_time" - "Creation_date_time")
        $ScheduleTimeUntilDelivery = $delivery_hours;
        // OperatingTimeUntilDelivery = ("Max_duration" - "ScheduleTimeUntilDelivery)
        $OperatingTimeUntilDelivery = ($max_duration - $ScheduleTimeUntilDelivery);
        //echo json_encode($OperatingTimeUntilDelivery);
        // UrgenceRate = Ur = (ScheduleTimeUntilDelivery / Max_duration)
        $UrgenceRate = ($ScheduleTimeUntilDelivery / $max_duration);
        //  payment predefine constants and percentages
        $payment_constants = $this->get_advance_payment((int)$data['category_id'], $data['from_country_id']);   

        $loading_free_hours = $payment_constants['loading_free_hours'];
        $charget_per_hours = $payment_constants['loading_hours_charge'];

        if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
          $advance_payment_percentage = $payment_constants['ELAPP'] ; $gonagoo_commission_percentage = $payment_constants['ELGCP'];
          $delivery_delay_constant = $payment_constants['ELDDC'] ; $delivery_delay_percentage = $payment_constants['ELDDP'];
          $handling_fee_constant = $payment_constants['ELHFC'] ; $handling_fee_percentage = $payment_constants['ELHFP'];
          $dedicated_vehicle_constant = $payment_constants['ELDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ELDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
          $advance_payment_percentage = $payment_constants['ENAPP'] ; $gonagoo_commission_percentage = $payment_constants['ENGCP'];
          $delivery_delay_constant = $payment_constants['ENDDC'] ; $delivery_delay_percentage = $payment_constants['ENDDP'];
          $handling_fee_constant = $payment_constants['ENHFC'] ; $handling_fee_percentage = $payment_constants['ENHFP'];
          $dedicated_vehicle_constant = $payment_constants['ENDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ENDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
          $advance_payment_percentage = $payment_constants['EIAPP'] ; $gonagoo_commission_percentage = $payment_constants['EIGCP'];
          $delivery_delay_constant = $payment_constants['EIDDC'] ; $delivery_delay_percentage = $payment_constants['EIDDP'];
          $handling_fee_constant = $payment_constants['EIHFC'] ; $handling_fee_percentage = $payment_constants['EIHFP'];
          $dedicated_vehicle_constant = $payment_constants['EIDVC'] ; $dedicated_vehicle_percentage = $payment_constants['EIDVP'];
          // get custom commission percentage for to_country_id
          if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
            if( $custom_percent = $this->get_custom_percentage((int)$data['category_id'], $data['to_country_id'])){              
              $new_goods_custom_percentage = $custom_percent['EICCPNG'];   $old_goods_custom_percentage = $custom_percent['EICCPOG'];          
            } else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
          }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
        }     
        elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
          $advance_payment_percentage = $payment_constants['ALAPP'] ; $gonagoo_commission_percentage = $payment_constants['ALGCP'];
          $delivery_delay_constant = $payment_constants['ALDDC'] ; $delivery_delay_percentage = $payment_constants['ALDDP'];
          $handling_fee_constant = $payment_constants['ALHFC'] ; $handling_fee_percentage = $payment_constants['ALHFP'];
          $dedicated_vehicle_constant = $payment_constants['ALDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ALDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
          $advance_payment_percentage = $payment_constants['ANAPP'] ; $gonagoo_commission_percentage = $payment_constants['ANGCP'];
          $delivery_delay_constant = $payment_constants['ANDDC'] ; $delivery_delay_percentage = $payment_constants['ANDDP'];
          $handling_fee_constant = $payment_constants['ANHFC'] ; $handling_fee_percentage = $payment_constants['ANHFP'];
          $dedicated_vehicle_constant = $payment_constants['ANDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ANDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
          $advance_payment_percentage = $payment_constants['AIAPP'] ; $gonagoo_commission_percentage = $payment_constants['AIGCP'];
          $delivery_delay_constant = $payment_constants['AIDDC'] ; $delivery_delay_percentage = $payment_constants['AIDDP'];
          $handling_fee_constant = $payment_constants['AIHFC'] ; $handling_fee_percentage = $payment_constants['AIHFP'];
          $dedicated_vehicle_constant = $payment_constants['AIDVC'] ; $dedicated_vehicle_percentage = $payment_constants['AIDVP'];        
          // get custom commission percentage for to_country_id
          if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
            if( $custom_percent = $this->get_custom_percentage((int)$data['category_id'], $data['to_country_id'])){            
              $new_goods_custom_percentage = $custom_percent['AICCPNG']; $old_goods_custom_percentage = $custom_percent['AICCPOG'];
            }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
          }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
        }
        elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
          $advance_payment_percentage = $payment_constants['SLAPP'] ; $gonagoo_commission_percentage = $payment_constants['SLGCP'];
          $delivery_delay_constant = $payment_constants['SLDDC'] ; $delivery_delay_percentage = $payment_constants['SLDDP'];
          $handling_fee_constant = $payment_constants['SLHFC'] ; $handling_fee_percentage = $payment_constants['SLHFP'];
          $dedicated_vehicle_constant = $payment_constants['SLDVC'] ; $dedicated_vehicle_percentage = $payment_constants['SLDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
          $advance_payment_percentage = $payment_constants['SNAPP'] ; $gonagoo_commission_percentage = $payment_constants['SNGCP'];
          $delivery_delay_constant = $payment_constants['SNDDC'] ; $delivery_delay_percentage = $payment_constants['SNDDP'];
          $handling_fee_constant = $payment_constants['SNHFC'] ; $handling_fee_percentage = $payment_constants['SNHFP'];
          $dedicated_vehicle_constant = $payment_constants['SNDVC'] ; $dedicated_vehicle_percentage = $payment_constants['SNDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
          $advance_payment_percentage = $payment_constants['SIAPP'] ; $gonagoo_commission_percentage = $payment_constants['SIGCP'];
          $delivery_delay_constant = $payment_constants['SIDDC'] ; $delivery_delay_percentage = $payment_constants['SIDDP'];
          $handling_fee_constant = $payment_constants['SIHFC'] ; $handling_fee_percentage = $payment_constants['SIHFP'];
          $dedicated_vehicle_constant = $payment_constants['SIDVC'] ; $dedicated_vehicle_percentage = $payment_constants['SIDVP'];
          // get custom commission percentage for to_country_id
          if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
            if( $custom_percent = $this->get_custom_percentage((int)$data['category_id'], $data['to_country_id'])){            
              $new_goods_custom_percentage = $custom_percent['SICCPNG']; $old_goods_custom_percentage = $custom_percent['SICCPOG'];          
            }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
          }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
        }
        //echo json_encode($OperatingTimeUntilDelivery);
        // IF "OperatingTimeUntilDelivery > 0" then ft = fee01 + (1-Ur)*k1*Cp;  else ft = 0;
        if($OperatingTimeUntilDelivery >  0 ) {
          $delivery_delay_fee = $delivery_delay_constant + (( 1 - $UrgenceRate ) * ( $delivery_delay_percentage / 100 ) * $core_price );
        } else { $delivery_delay_fee = 0; }
    
        // Insurance 
        if($data['package_value'] > 0 ){
          $insurance = $this->get_insurance_fees((int) $data['from_country_id'], $data['package_value'],(int)$data['category_id']);
          $insurance_fee = $insurance['ins_fee'];
        } else { $insurance_fee = 0; }
    
        // Handling Fee
        if($data['handling_by'] == 1) {
          // fh = fee02 + k2*Cp
          $handling_fee = $handling_fee_constant + ($handling_fee_percentage / 100 ) * $core_price;
        } else { $handling_fee = 0; }
    
        // Dedicated Vehicle Fee
        if($data['dedicated_vehicle'] == 1){
          // c = fee03 + k3*Cp
          $dedicated_vehicle_fee = $dedicated_vehicle_constant + ($dedicated_vehicle_percentage / 100) * $core_price; 
        } else { $dedicated_vehicle_fee = 0; }
        
        if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
          $old_goods_custom_commission_fee = number_format((( $custom_package_value * $old_goods_custom_percentage ) / 100),2,".","");
          $new_goods_custom_commission_fee =  number_format((( $custom_package_value * $new_goods_custom_percentage ) / 100),2,".","");
        } else {  $old_goods_custom_commission_fee = 0; $new_goods_custom_commission_fee = 0; }

        //echo json_encode((strtolower($service_area_type)=="international") && $old_new_goods =="Old"); die();
        if((strtolower($service_area_type)=="international") && $old_new_goods =="Old") {
          $total_price = $core_price + $delivery_delay_fee + $insurance_fee + $handling_fee + $dedicated_vehicle_fee + $old_goods_custom_commission_fee;
        }
        else if((strtolower($service_area_type)=="international") && $old_new_goods =="New"){
          $total_price = $core_price + $delivery_delay_fee + $insurance_fee + $handling_fee + $dedicated_vehicle_fee + $new_goods_custom_commission_fee;
        }
        else{ $total_price = $core_price + $delivery_delay_fee + $insurance_fee + $handling_fee + $dedicated_vehicle_fee; }

        if($loading_time > 0 && $loading_time > $loading_free_hours ){
          $panelty_hrs = $loading_time - (int) $loading_free_hours;
          $loading_unloading_charge = $charget_per_hours * $panelty_hrs;
          $total_price += $loading_unloading_charge;
        } else { $loading_unloading_charge = 0; }

        return array(
          "total_price" =>($core_price > 0 ) ? number_format($total_price,2,".","") : 0,
          "standard_price" => ($core_price > 0 ) ? number_format($core_price,2,".","") : 0,
          "urgent_fee" => ($core_price > 0 ) ? number_format($delivery_delay_fee,2,".",""):0,
          "ins_fee" => ($core_price > 0 ) ? number_format($insurance_fee,2,".",""):0,
          "handling_fee" => ($core_price > 0 ) ? number_format($handling_fee,2,".",""):0,
          "dedicated_vehicle_fee" => ($core_price > 0 ) ? number_format($dedicated_vehicle_fee,2,".",""):0,
          "currency_id" => $currency_id,
          "currency_sign" => $currency_sign,
          "currency_title" => $currency_title,
          "distance_in_km" => $distance_in_km,
          "advance_percent" => ($core_price > 0 ) ? $advance_payment_percentage : 0,       
          "advance_payment" => ($core_price > 0 ) ? number_format((( $core_price * $advance_payment_percentage ) / 100),2,".","") : 0,       
          "commission_percent" => ($core_price > 0 ) ? $gonagoo_commission_percentage : 0,        
          "old_goods_custom_commission_percent" => ($core_price > 0 ) ? $old_goods_custom_percentage : 0,
          "old_goods_custom_commission_fee" => ($core_price > 0 ) ? $old_goods_custom_commission_fee :0,       
          "new_goods_custom_commission_percent" => ($core_price > 0 ) ? $new_goods_custom_percentage : 0,        
          "new_goods_custom_commission_fee" => ($core_price > 0 ) ? $new_goods_custom_commission_fee :0,        
          "loading_unloading_charge" => $loading_unloading_charge,        
          "loading_free_hours" => $loading_free_hours,        
        );
      } 
      else { //if user dedicated but no price found then apply Gonagoo prices
        // Point to Point / Standard - Weight Based Price
        $p_to_p_weight_based = array(
          "category_id" => $data['category_id'],
          "total_weight" => $data['total_weight'],
          "unit_id" => $data['unit_id'],
          "from_country_id" => $data['from_country_id'],
          "from_state_id" => $data['from_state_id'],
          "from_city_id" => $data['from_city_id'],
          "to_country_id" => $data['to_country_id'],
          "to_state_id" => $data['to_state_id'],
          "to_city_id" => $data['to_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
        );
        if(!$weight_based_price_data = $this->calculate_weight_based_p_to_p_price($p_to_p_weight_based)) {
          $weight_based = array(
            "category_id" => $data['category_id'],
            "total_weight" => $data['total_weight'],
            "unit_id" => $data['unit_id'],
            "country_id" => $data['from_country_id'],
            "state_id" => $data['from_state_id'],
            "city_id" => $data['from_city_id'],
            "service_area_type" => $data['service_area_type'],
            "transport_type" => $data['transport_type'],
            "distance_in_km" => (string) $distance_in_km,
          );
          $weight_based_price_data = $this->calculate_weight_based_price($weight_based);      
        }
        // Point to Point / Standard - Formula Weight Based Price
        $p_to_p_formula_weight_based = array(
          "category_id" => $data['category_id'],
          "total_weight" => $data['total_weight'],
          "unit_id" => $data['unit_id'],
          "from_country_id" => $data['from_country_id'],
          "from_state_id" => $data['from_state_id'],
          "from_city_id" => $data['from_city_id'],
          "to_country_id" => $data['to_country_id'],
          "to_state_id" => $data['to_state_id'],
          "to_city_id" => $data['to_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
        );
        if(!$formula_weight_based_price_data = $this->calculate_formula_weight_based_p_to_p_price($p_to_p_formula_weight_based)) {
          $weight_based = array(
            "category_id" => $data['category_id'],
            "total_weight" => $data['total_weight'],
            "unit_id" => $data['unit_id'],
            "country_id" => $data['from_country_id'],
            "state_id" => $data['from_state_id'],
            "city_id" => $data['from_city_id'],
            "service_area_type" => $data['service_area_type'],
            "transport_type" => $data['transport_type'],
            "distance_in_km" => (string) $distance_in_km,
          );
          $formula_weight_based_price_data = $this->calculate_formula_weight_based_price($weight_based);      
        }

        // Point to Point / Standard - Volume Based Price
        $p_to_p_volume_based = array(
          "category_id" => $data['category_id'],
          "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
          "from_country_id" => $data['from_country_id'],
          "from_state_id" => $data['from_state_id'],
          "from_city_id" => $data['from_city_id'],
          "to_country_id" => $data['to_country_id'],
          "to_state_id" => $data['to_state_id'],
          "to_city_id" => $data['to_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
        );
        if(!$volume_based_price_data = $this->calculate_volume_based_p_to_p_price($p_to_p_volume_based) ){
          $volume_based = array(
            "category_id" => $data['category_id'],
            "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
            "country_id" => $data['from_country_id'],
            "service_area_type" => $data['service_area_type'],
            "transport_type" => $data['transport_type'],
            "distance_in_km" => (string) $distance_in_km,
          );
          $volume_based_price_data = $this->calculate_volume_based_price($volume_based);
        }
        // Point to Point / Standard - Formula Volume Based Price
        $p_to_p_formula_volume_based = array(
          "category_id" => $data['category_id'],
          "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
          "from_country_id" => $data['from_country_id'],
          "from_state_id" => $data['from_state_id'],
          "from_city_id" => $data['from_city_id'],
          "to_country_id" => $data['to_country_id'],
          "to_state_id" => $data['to_state_id'],
          "to_city_id" => $data['to_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
        );
        if(!$formula_volume_based_price_data = $this->calculate_formula_volume_based_p_to_p_price($p_to_p_formula_volume_based) ){
          $volume_based = array(
            "category_id" => $data['category_id'],
            "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
            "country_id" => $data['from_country_id'],
            "service_area_type" => $data['service_area_type'],
            "transport_type" => $data['transport_type'],
            "distance_in_km" => (string) $distance_in_km,
          );
          $formula_volume_based_price_data = $this->calculate_formula_volume_based_price($volume_based);
        }

        if (is_array($weight_based_price_data) || is_array($volume_based_price_data) || is_array($formula_weight_based_price_data) || is_array($formula_volume_based_price_data)){
          if($formula_weight_based_price_data!=null && $formula_volume_based_price_data!=null) {
            if ( (($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km) <= (((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km) ) { 
              //Volume*Rate*Distance  
              $core_price = ((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km;
              $currency_id = $formula_volume_based_price_data['currency_id'];
              $currency_sign = $formula_volume_based_price_data['currency_sign'];
              $currency_title = $formula_volume_based_price_data['currency_title'];
              $max_duration = $formula_volume_based_price_data['max_duration'];
            } else { 
              //Total Weight*Rate*Distance  
              $core_price = ($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km;
              $currency_id = $formula_weight_based_price_data['currency_id'];
              $currency_sign = $formula_weight_based_price_data['currency_sign'];
              $currency_title = $formula_weight_based_price_data['currency_title'];
              $max_duration = $formula_weight_based_price_data['max_duration'];
            }
          } else if($formula_weight_based_price_data!=null) {
            //Total Weight*Rate*Distance  
            $core_price = ($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km;
            $currency_id = $formula_weight_based_price_data['currency_id'];
            $currency_sign = $formula_weight_based_price_data['currency_sign'];
            $currency_title = $formula_weight_based_price_data['currency_title'];
            $max_duration = $formula_weight_based_price_data['max_duration'];
          } else if($formula_volume_based_price_data!=null){
            //Volume*Rate*Distance  
            $core_price = ((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km;
            $currency_id = $formula_volume_based_price_data['currency_id'];
            $currency_sign = $formula_volume_based_price_data['currency_sign'];
            $currency_title = $formula_volume_based_price_data['currency_title'];
            $max_duration = $formula_volume_based_price_data['max_duration'];
          } else {
            if($weight_based_price_data!=null && $volume_based_price_data!=null) {
              if ($weight_based_price_data['service_rate'] <= $volume_based_price_data['service_rate']){ 
                $core_price = $volume_based_price_data['service_rate'];
                $currency_id = $volume_based_price_data['currency_id'];
                $currency_sign = $volume_based_price_data['currency_sign'];
                $currency_title = $volume_based_price_data['currency_title'];
                $max_duration = $volume_based_price_data['max_duration'];
              } else { 
                $core_price = $weight_based_price_data['service_rate'];
                $currency_id = $weight_based_price_data['currency_id'];
                $currency_sign = $weight_based_price_data['currency_sign'];
                $currency_title = $weight_based_price_data['currency_title'];
                $max_duration = $weight_based_price_data['max_duration'];
              }
            } else if($weight_based_price_data!=null) {
              $core_price = $weight_based_price_data['service_rate'];
              $currency_id = $weight_based_price_data['currency_id'];
              $currency_sign = $weight_based_price_data['currency_sign'];
              $currency_title = $weight_based_price_data['currency_title'];
              $max_duration = $weight_based_price_data['max_duration'];
            } else if($volume_based_price_data!=null) {
              $core_price = $volume_based_price_data['service_rate'];
              $currency_id = $volume_based_price_data['currency_id'];
              $currency_sign = $volume_based_price_data['currency_sign'];
              $currency_title = $volume_based_price_data['currency_title'];
              $max_duration = $volume_based_price_data['max_duration'];
            } else { 
              return array(
                "total_price" => 0,
                "standard_price" => 0,
                "urgent_fee" => 0,
                "ins_fee" => 0,
                "handling_fee" => 0,
                "dedicated_vehicle_fee" => 0,
                "currency_id" => 0,
                "currency_sign" => "",
                "currency_title" => "",
                "distance_in_km" => $distance_in_km,
                "advance_percent" => 0,       
                "advance_payment" => 0,
                "commission_percent" => 0,        
                "old_goods_custom_commission_percent" => 0,        
                "old_goods_custom_commission_fee" => 0,       
                "new_goods_custom_commission_percent" => 0,        
                "new_goods_custom_commission_fee" => 0,    
                "loading_unloading_charge" => 0,    
              );
            }
          }

          // ScheduleTimeUntilDelivery = ("Delivery_date_time" - "Creation_date_time")
          $ScheduleTimeUntilDelivery = $delivery_hours;
          // OperatingTimeUntilDelivery = ("Max_duration" - "ScheduleTimeUntilDelivery)
          $OperatingTimeUntilDelivery = ($max_duration - $ScheduleTimeUntilDelivery);
          //echo json_encode($OperatingTimeUntilDelivery);
          // UrgenceRate = Ur = (ScheduleTimeUntilDelivery / Max_duration)
          $UrgenceRate = ($ScheduleTimeUntilDelivery / $max_duration);
          //  payment predefine constants and percentages
          $payment_constants = $this->get_advance_payment((int)$data['category_id'], $data['from_country_id']);   

          $loading_free_hours = $payment_constants['loading_free_hours'];
          $charget_per_hours = $payment_constants['loading_hours_charge'];

          if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
            $advance_payment_percentage = $payment_constants['ELAPP'] ; $gonagoo_commission_percentage = $payment_constants['ELGCP'];
            $delivery_delay_constant = $payment_constants['ELDDC'] ; $delivery_delay_percentage = $payment_constants['ELDDP'];
            $handling_fee_constant = $payment_constants['ELHFC'] ; $handling_fee_percentage = $payment_constants['ELHFP'];
            $dedicated_vehicle_constant = $payment_constants['ELDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ELDVP'];
            $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
          }
          elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
            $advance_payment_percentage = $payment_constants['ENAPP'] ; $gonagoo_commission_percentage = $payment_constants['ENGCP'];
            $delivery_delay_constant = $payment_constants['ENDDC'] ; $delivery_delay_percentage = $payment_constants['ENDDP'];
            $handling_fee_constant = $payment_constants['ENHFC'] ; $handling_fee_percentage = $payment_constants['ENHFP'];
            $dedicated_vehicle_constant = $payment_constants['ENDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ENDVP'];
            $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
          }
          elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
            $advance_payment_percentage = $payment_constants['EIAPP'] ; $gonagoo_commission_percentage = $payment_constants['EIGCP'];
            $delivery_delay_constant = $payment_constants['EIDDC'] ; $delivery_delay_percentage = $payment_constants['EIDDP'];
            $handling_fee_constant = $payment_constants['EIHFC'] ; $handling_fee_percentage = $payment_constants['EIHFP'];
            $dedicated_vehicle_constant = $payment_constants['EIDVC'] ; $dedicated_vehicle_percentage = $payment_constants['EIDVP'];
            // get custom commission percentage for to_country_id
            if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
              if( $custom_percent = $this->get_custom_percentage((int)$data['category_id'], $data['to_country_id'])){              
                $new_goods_custom_percentage = $custom_percent['EICCPNG'];   $old_goods_custom_percentage = $custom_percent['EICCPOG'];          
              } else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
            }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
          }     
          elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
            $advance_payment_percentage = $payment_constants['ALAPP'] ; $gonagoo_commission_percentage = $payment_constants['ALGCP'];
            $delivery_delay_constant = $payment_constants['ALDDC'] ; $delivery_delay_percentage = $payment_constants['ALDDP'];
            $handling_fee_constant = $payment_constants['ALHFC'] ; $handling_fee_percentage = $payment_constants['ALHFP'];
            $dedicated_vehicle_constant = $payment_constants['ALDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ALDVP'];
            $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
          }
          elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
            $advance_payment_percentage = $payment_constants['ANAPP'] ; $gonagoo_commission_percentage = $payment_constants['ANGCP'];
            $delivery_delay_constant = $payment_constants['ANDDC'] ; $delivery_delay_percentage = $payment_constants['ANDDP'];
            $handling_fee_constant = $payment_constants['ANHFC'] ; $handling_fee_percentage = $payment_constants['ANHFP'];
            $dedicated_vehicle_constant = $payment_constants['ANDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ANDVP'];
            $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
          }
          elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
            $advance_payment_percentage = $payment_constants['AIAPP'] ; $gonagoo_commission_percentage = $payment_constants['AIGCP'];
            $delivery_delay_constant = $payment_constants['AIDDC'] ; $delivery_delay_percentage = $payment_constants['AIDDP'];
            $handling_fee_constant = $payment_constants['AIHFC'] ; $handling_fee_percentage = $payment_constants['AIHFP'];
            $dedicated_vehicle_constant = $payment_constants['AIDVC'] ; $dedicated_vehicle_percentage = $payment_constants['AIDVP'];        
            // get custom commission percentage for to_country_id
            if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
              if( $custom_percent = $this->get_custom_percentage((int)$data['category_id'], $data['to_country_id'])){            
                $new_goods_custom_percentage = $custom_percent['AICCPNG']; $old_goods_custom_percentage = $custom_percent['AICCPOG'];
              }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
            }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
          }
          elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
            $advance_payment_percentage = $payment_constants['SLAPP'] ; $gonagoo_commission_percentage = $payment_constants['SLGCP'];
            $delivery_delay_constant = $payment_constants['SLDDC'] ; $delivery_delay_percentage = $payment_constants['SLDDP'];
            $handling_fee_constant = $payment_constants['SLHFC'] ; $handling_fee_percentage = $payment_constants['SLHFP'];
            $dedicated_vehicle_constant = $payment_constants['SLDVC'] ; $dedicated_vehicle_percentage = $payment_constants['SLDVP'];
            $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
          }
          elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
            $advance_payment_percentage = $payment_constants['SNAPP'] ; $gonagoo_commission_percentage = $payment_constants['SNGCP'];
            $delivery_delay_constant = $payment_constants['SNDDC'] ; $delivery_delay_percentage = $payment_constants['SNDDP'];
            $handling_fee_constant = $payment_constants['SNHFC'] ; $handling_fee_percentage = $payment_constants['SNHFP'];
            $dedicated_vehicle_constant = $payment_constants['SNDVC'] ; $dedicated_vehicle_percentage = $payment_constants['SNDVP'];
            $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
          }
          elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
            $advance_payment_percentage = $payment_constants['SIAPP'] ; $gonagoo_commission_percentage = $payment_constants['SIGCP'];
            $delivery_delay_constant = $payment_constants['SIDDC'] ; $delivery_delay_percentage = $payment_constants['SIDDP'];
            $handling_fee_constant = $payment_constants['SIHFC'] ; $handling_fee_percentage = $payment_constants['SIHFP'];
            $dedicated_vehicle_constant = $payment_constants['SIDVC'] ; $dedicated_vehicle_percentage = $payment_constants['SIDVP'];
            // get custom commission percentage for to_country_id
            if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
              if( $custom_percent = $this->get_custom_percentage((int)$data['category_id'], $data['to_country_id'])){            
                $new_goods_custom_percentage = $custom_percent['SICCPNG']; $old_goods_custom_percentage = $custom_percent['SICCPOG'];          
              }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
            }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
          }
          //echo json_encode($OperatingTimeUntilDelivery);
          // IF "OperatingTimeUntilDelivery > 0" then ft = fee01 + (1-Ur)*k1*Cp;  else ft = 0;
          if($OperatingTimeUntilDelivery >  0 ) {
            $delivery_delay_fee = $delivery_delay_constant + (( 1 - $UrgenceRate ) * ( $delivery_delay_percentage / 100 ) * $core_price );
          } else { $delivery_delay_fee = 0; }
      
          // Insurance 
          if($data['package_value'] > 0 ){
            $insurance = $this->get_insurance_fees((int) $data['from_country_id'], $data['package_value'],(int)$data['category_id']);
            $insurance_fee = $insurance['ins_fee'];
          } else { $insurance_fee = 0; }
      
          // Handling Fee
          if($data['handling_by'] == 1) {
            // fh = fee02 + k2*Cp
            $handling_fee = $handling_fee_constant + ($handling_fee_percentage / 100 ) * $core_price;
          } else { $handling_fee = 0; }
      
          // Dedicated Vehicle Fee
          if($data['dedicated_vehicle'] == 1){
            // c = fee03 + k3*Cp
            $dedicated_vehicle_fee = $dedicated_vehicle_constant + ($dedicated_vehicle_percentage / 100) * $core_price; 
          } else { $dedicated_vehicle_fee = 0; }
          
          if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
            $old_goods_custom_commission_fee = number_format((( $custom_package_value * $old_goods_custom_percentage ) / 100),2,".","");
            $new_goods_custom_commission_fee =  number_format((( $custom_package_value * $new_goods_custom_percentage ) / 100),2,".","");
          } else {  $old_goods_custom_commission_fee = 0; $new_goods_custom_commission_fee = 0; }

          //echo json_encode((strtolower($service_area_type)=="international") && $old_new_goods =="Old"); die();
          if((strtolower($service_area_type)=="international") && $old_new_goods =="Old") {
            $total_price = $core_price + $delivery_delay_fee + $insurance_fee + $handling_fee + $dedicated_vehicle_fee + $old_goods_custom_commission_fee;
          }
          else if((strtolower($service_area_type)=="international") && $old_new_goods =="New"){
            $total_price = $core_price + $delivery_delay_fee + $insurance_fee + $handling_fee + $dedicated_vehicle_fee + $new_goods_custom_commission_fee;
          }
          else{ $total_price = $core_price + $delivery_delay_fee + $insurance_fee + $handling_fee + $dedicated_vehicle_fee; }

          if($loading_time > 0 && $loading_time > $loading_free_hours ){
            $panelty_hrs = $loading_time - (int) $loading_free_hours;
            $loading_unloading_charge = $charget_per_hours * $panelty_hrs;
            $total_price += $loading_unloading_charge;
          } else { $loading_unloading_charge = 0; }

          return array(
            "total_price" =>($core_price > 0 ) ? number_format($total_price,2,".","") : 0,
            "standard_price" => ($core_price > 0 ) ? number_format($core_price,2,".","") : 0,
            "urgent_fee" => ($core_price > 0 ) ? number_format($delivery_delay_fee,2,".",""):0,
            "ins_fee" => ($core_price > 0 ) ? number_format($insurance_fee,2,".",""):0,
            "handling_fee" => ($core_price > 0 ) ? number_format($handling_fee,2,".",""):0,
            "dedicated_vehicle_fee" => ($core_price > 0 ) ? number_format($dedicated_vehicle_fee,2,".",""):0,
            "currency_id" => $currency_id,
            "currency_sign" => $currency_sign,
            "currency_title" => $currency_title,
            "distance_in_km" => $distance_in_km,
            "advance_percent" => ($core_price > 0 ) ? $advance_payment_percentage : 0,       
            "advance_payment" => ($core_price > 0 ) ? number_format((( $core_price * $advance_payment_percentage ) / 100),2,".","") : 0,       
            "commission_percent" => ($core_price > 0 ) ? $gonagoo_commission_percentage : 0,        
            "old_goods_custom_commission_percent" => ($core_price > 0 ) ? $old_goods_custom_percentage : 0,
            "old_goods_custom_commission_fee" => ($core_price > 0 ) ? $old_goods_custom_commission_fee :0,       
            "new_goods_custom_commission_percent" => ($core_price > 0 ) ? $new_goods_custom_percentage : 0,        
            "new_goods_custom_commission_fee" => ($core_price > 0 ) ? $new_goods_custom_commission_fee :0,        
            "loading_unloading_charge" => $loading_unloading_charge,        
            "loading_free_hours" => $loading_free_hours,        
          );
        } 
        else { 
          return array(
            "total_price" => 0,
            "standard_price" => 0,
            "urgent_fee" => 0,
            "ins_fee" => 0,
            "handling_fee" => 0,
            "dedicated_vehicle_fee" => 0,
            "currency_id" => 0,
            "currency_sign" => "",
            "currency_title" => "",
            "distance_in_km" => $distance_in_km,
            "advance_percent" => 0,       
            "advance_payment" => 0,
            "commission_percent" => 0,        
            "old_goods_custom_commission_percent" => 0,        
            "old_goods_custom_commission_fee" => 0,       
            "new_goods_custom_commission_percent" => 0,        
            "new_goods_custom_commission_fee" => 0,    
            "loading_unloading_charge" => 0,    
          );
        }
      }
    } else { //if user not dedicated apply Gonagoo prices
      // Point to Point / Standard - Weight Based Price
      $p_to_p_weight_based = array(
        "category_id" => $data['category_id'],
        "total_weight" => $data['total_weight'],
        "unit_id" => $data['unit_id'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
      );
      if(!$weight_based_price_data = $this->calculate_weight_based_p_to_p_price($p_to_p_weight_based)) {
        $weight_based = array(
          "category_id" => $data['category_id'],
          "total_weight" => $data['total_weight'],
          "unit_id" => $data['unit_id'],
          "country_id" => $data['from_country_id'],
          "state_id" => $data['from_state_id'],
          "city_id" => $data['from_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
        );
        $weight_based_price_data = $this->calculate_weight_based_price($weight_based);      
      }
      // Point to Point / Standard - Formula Weight Based Price
      $p_to_p_formula_weight_based = array(
        "category_id" => $data['category_id'],
        "total_weight" => $data['total_weight'],
        "unit_id" => $data['unit_id'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
      );
      if(!$formula_weight_based_price_data = $this->calculate_formula_weight_based_p_to_p_price($p_to_p_formula_weight_based)) {
        $weight_based = array(
          "category_id" => $data['category_id'],
          "total_weight" => $data['total_weight'],
          "unit_id" => $data['unit_id'],
          "country_id" => $data['from_country_id'],
          "state_id" => $data['from_state_id'],
          "city_id" => $data['from_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
        );
        $formula_weight_based_price_data = $this->calculate_formula_weight_based_price($weight_based);      
      }

      // Point to Point / Standard - Volume Based Price
      $p_to_p_volume_based = array(
        "category_id" => $data['category_id'],
        "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
      );
      if(!$volume_based_price_data = $this->calculate_volume_based_p_to_p_price($p_to_p_volume_based) ){
        $volume_based = array(
          "category_id" => $data['category_id'],
          "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
          "country_id" => $data['from_country_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
        );
        $volume_based_price_data = $this->calculate_volume_based_price($volume_based);
      }
      // Point to Point / Standard - Formula Volume Based Price
      $p_to_p_formula_volume_based = array(
        "category_id" => $data['category_id'],
        "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
      );
      if(!$formula_volume_based_price_data = $this->calculate_formula_volume_based_p_to_p_price($p_to_p_formula_volume_based) ){
        $volume_based = array(
          "category_id" => $data['category_id'],
          "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
          "country_id" => $data['from_country_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
        );
        $formula_volume_based_price_data = $this->calculate_formula_volume_based_price($volume_based);
      }

      if (is_array($weight_based_price_data) || is_array($volume_based_price_data) || is_array($formula_weight_based_price_data) || is_array($formula_volume_based_price_data)){ 
        if($formula_weight_based_price_data!=null && $formula_volume_based_price_data!=null) {
          if ( (($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km) <= (((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km) ) { 
            //Volume*Rate*Distance  
            $core_price = ((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km;
            $currency_id = $formula_volume_based_price_data['currency_id'];
            $currency_sign = $formula_volume_based_price_data['currency_sign'];
            $currency_title = $formula_volume_based_price_data['currency_title'];
            $max_duration = $formula_volume_based_price_data['max_duration'];
          } else { 
            //Total Weight*Rate*Distance  
            $core_price = ($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km;
            $currency_id = $formula_weight_based_price_data['currency_id'];
            $currency_sign = $formula_weight_based_price_data['currency_sign'];
            $currency_title = $formula_weight_based_price_data['currency_title'];
            $max_duration = $formula_weight_based_price_data['max_duration'];
          }
        } else if($formula_weight_based_price_data!=null) {
          //Total Weight*Rate*Distance  
          $core_price = ($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km;
          $currency_id = $formula_weight_based_price_data['currency_id'];
          $currency_sign = $formula_weight_based_price_data['currency_sign'];
          $currency_title = $formula_weight_based_price_data['currency_title'];
          $max_duration = $formula_weight_based_price_data['max_duration'];
        } else if($formula_volume_based_price_data!=null){
          //Volume*Rate*Distance  
          $core_price = ((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km;
          $currency_id = $formula_volume_based_price_data['currency_id'];
          $currency_sign = $formula_volume_based_price_data['currency_sign'];
          $currency_title = $formula_volume_based_price_data['currency_title'];
          $max_duration = $formula_volume_based_price_data['max_duration'];
        } else {
          if($weight_based_price_data!=null && $volume_based_price_data!=null) {
            if ($weight_based_price_data['service_rate'] <= $volume_based_price_data['service_rate']){ 
              $core_price = $volume_based_price_data['service_rate'];
              $currency_id = $volume_based_price_data['currency_id'];
              $currency_sign = $volume_based_price_data['currency_sign'];
              $currency_title = $volume_based_price_data['currency_title'];
              $max_duration = $volume_based_price_data['max_duration'];
            } else { 
              $core_price = $weight_based_price_data['service_rate'];
              $currency_id = $weight_based_price_data['currency_id'];
              $currency_sign = $weight_based_price_data['currency_sign'];
              $currency_title = $weight_based_price_data['currency_title'];
              $max_duration = $weight_based_price_data['max_duration'];
            }
          } else if($weight_based_price_data!=null) {
            $core_price = $weight_based_price_data['service_rate'];
            $currency_id = $weight_based_price_data['currency_id'];
            $currency_sign = $weight_based_price_data['currency_sign'];
            $currency_title = $weight_based_price_data['currency_title'];
            $max_duration = $weight_based_price_data['max_duration'];
          } else if($volume_based_price_data!=null) {
            $core_price = $volume_based_price_data['service_rate'];
            $currency_id = $volume_based_price_data['currency_id'];
            $currency_sign = $volume_based_price_data['currency_sign'];
            $currency_title = $volume_based_price_data['currency_title'];
            $max_duration = $volume_based_price_data['max_duration'];
          } else { 
            return array(
              "total_price" => 0,
              "standard_price" => 0,
              "urgent_fee" => 0,
              "ins_fee" => 0,
              "handling_fee" => 0,
              "dedicated_vehicle_fee" => 0,
              "currency_id" => 0,
              "currency_sign" => "",
              "currency_title" => "",
              "distance_in_km" => $distance_in_km,
              "advance_percent" => 0,       
              "advance_payment" => 0,
              "commission_percent" => 0,        
              "old_goods_custom_commission_percent" => 0,        
              "old_goods_custom_commission_fee" => 0,       
              "new_goods_custom_commission_percent" => 0,        
              "new_goods_custom_commission_fee" => 0,    
              "loading_unloading_charge" => 0,    
            );
          }
        }
    
        // ScheduleTimeUntilDelivery = ("Delivery_date_time" - "Creation_date_time")
        $ScheduleTimeUntilDelivery = $delivery_hours;
        // OperatingTimeUntilDelivery = ("Max_duration" - "ScheduleTimeUntilDelivery)
        $OperatingTimeUntilDelivery = ($max_duration - $ScheduleTimeUntilDelivery);
        //echo json_encode($OperatingTimeUntilDelivery);
        // UrgenceRate = Ur = (ScheduleTimeUntilDelivery / Max_duration)
        $UrgenceRate = ($ScheduleTimeUntilDelivery / $max_duration);
        //  payment predefine constants and percentages
        $payment_constants = $this->get_advance_payment((int)$data['category_id'], $data['from_country_id']);   

        $loading_free_hours = $payment_constants['loading_free_hours'];
        $charget_per_hours = $payment_constants['loading_hours_charge'];

        if(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="local"){ 
          $advance_payment_percentage = $payment_constants['ELAPP'] ; $gonagoo_commission_percentage = $payment_constants['ELGCP'];
          $delivery_delay_constant = $payment_constants['ELDDC'] ; $delivery_delay_percentage = $payment_constants['ELDDP'];
          $handling_fee_constant = $payment_constants['ELHFC'] ; $handling_fee_percentage = $payment_constants['ELHFP'];
          $dedicated_vehicle_constant = $payment_constants['ELDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ELDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="national"){ 
          $advance_payment_percentage = $payment_constants['ENAPP'] ; $gonagoo_commission_percentage = $payment_constants['ENGCP'];
          $delivery_delay_constant = $payment_constants['ENDDC'] ; $delivery_delay_percentage = $payment_constants['ENDDP'];
          $handling_fee_constant = $payment_constants['ENHFC'] ; $handling_fee_percentage = $payment_constants['ENHFP'];
          $dedicated_vehicle_constant = $payment_constants['ENDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ENDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="earth" && strtolower($service_area_type)=="international"){ 
          $advance_payment_percentage = $payment_constants['EIAPP'] ; $gonagoo_commission_percentage = $payment_constants['EIGCP'];
          $delivery_delay_constant = $payment_constants['EIDDC'] ; $delivery_delay_percentage = $payment_constants['EIDDP'];
          $handling_fee_constant = $payment_constants['EIHFC'] ; $handling_fee_percentage = $payment_constants['EIHFP'];
          $dedicated_vehicle_constant = $payment_constants['EIDVC'] ; $dedicated_vehicle_percentage = $payment_constants['EIDVP'];
          // get custom commission percentage for to_country_id
          if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
            if( $custom_percent = $this->get_custom_percentage((int)$data['category_id'], $data['to_country_id'])){              
              $new_goods_custom_percentage = $custom_percent['EICCPNG'];   $old_goods_custom_percentage = $custom_percent['EICCPOG'];          
            } else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
          }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
        }     
        elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="local"){ 
          $advance_payment_percentage = $payment_constants['ALAPP'] ; $gonagoo_commission_percentage = $payment_constants['ALGCP'];
          $delivery_delay_constant = $payment_constants['ALDDC'] ; $delivery_delay_percentage = $payment_constants['ALDDP'];
          $handling_fee_constant = $payment_constants['ALHFC'] ; $handling_fee_percentage = $payment_constants['ALHFP'];
          $dedicated_vehicle_constant = $payment_constants['ALDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ALDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="national"){ 
          $advance_payment_percentage = $payment_constants['ANAPP'] ; $gonagoo_commission_percentage = $payment_constants['ANGCP'];
          $delivery_delay_constant = $payment_constants['ANDDC'] ; $delivery_delay_percentage = $payment_constants['ANDDP'];
          $handling_fee_constant = $payment_constants['ANHFC'] ; $handling_fee_percentage = $payment_constants['ANHFP'];
          $dedicated_vehicle_constant = $payment_constants['ANDVC'] ; $dedicated_vehicle_percentage = $payment_constants['ANDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="air" && strtolower($service_area_type)=="international"){ 
          $advance_payment_percentage = $payment_constants['AIAPP'] ; $gonagoo_commission_percentage = $payment_constants['AIGCP'];
          $delivery_delay_constant = $payment_constants['AIDDC'] ; $delivery_delay_percentage = $payment_constants['AIDDP'];
          $handling_fee_constant = $payment_constants['AIHFC'] ; $handling_fee_percentage = $payment_constants['AIHFP'];
          $dedicated_vehicle_constant = $payment_constants['AIDVC'] ; $dedicated_vehicle_percentage = $payment_constants['AIDVP'];        
          // get custom commission percentage for to_country_id
          if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
            if( $custom_percent = $this->get_custom_percentage((int)$data['category_id'], $data['to_country_id'])){            
              $new_goods_custom_percentage = $custom_percent['AICCPNG']; $old_goods_custom_percentage = $custom_percent['AICCPOG'];
            }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
          }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
        }
        elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="local"){ 
          $advance_payment_percentage = $payment_constants['SLAPP'] ; $gonagoo_commission_percentage = $payment_constants['SLGCP'];
          $delivery_delay_constant = $payment_constants['SLDDC'] ; $delivery_delay_percentage = $payment_constants['SLDDP'];
          $handling_fee_constant = $payment_constants['SLHFC'] ; $handling_fee_percentage = $payment_constants['SLHFP'];
          $dedicated_vehicle_constant = $payment_constants['SLDVC'] ; $dedicated_vehicle_percentage = $payment_constants['SLDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="national"){ 
          $advance_payment_percentage = $payment_constants['SNAPP'] ; $gonagoo_commission_percentage = $payment_constants['SNGCP'];
          $delivery_delay_constant = $payment_constants['SNDDC'] ; $delivery_delay_percentage = $payment_constants['SNDDP'];
          $handling_fee_constant = $payment_constants['SNHFC'] ; $handling_fee_percentage = $payment_constants['SNHFP'];
          $dedicated_vehicle_constant = $payment_constants['SNDVC'] ; $dedicated_vehicle_percentage = $payment_constants['SNDVP'];
          $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0;
        }
        elseif(strtolower($transport_type)=="sea" && strtolower($service_area_type)=="international"){ 
          $advance_payment_percentage = $payment_constants['SIAPP'] ; $gonagoo_commission_percentage = $payment_constants['SIGCP'];
          $delivery_delay_constant = $payment_constants['SIDDC'] ; $delivery_delay_percentage = $payment_constants['SIDDP'];
          $handling_fee_constant = $payment_constants['SIHFC'] ; $handling_fee_percentage = $payment_constants['SIHFP'];
          $dedicated_vehicle_constant = $payment_constants['SIDVC'] ; $dedicated_vehicle_percentage = $payment_constants['SIDVP'];
          // get custom commission percentage for to_country_id
          if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
            if( $custom_percent = $this->get_custom_percentage((int)$data['category_id'], $data['to_country_id'])){            
              $new_goods_custom_percentage = $custom_percent['SICCPNG']; $old_goods_custom_percentage = $custom_percent['SICCPOG'];          
            }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
          }  else{ $new_goods_custom_percentage = 0; $old_goods_custom_percentage = 0; }
        }
        //echo json_encode($OperatingTimeUntilDelivery);
        // IF "OperatingTimeUntilDelivery > 0" then ft = fee01 + (1-Ur)*k1*Cp;  else ft = 0;
        if($OperatingTimeUntilDelivery >  0 ) {
          $delivery_delay_fee = $delivery_delay_constant + (( 1 - $UrgenceRate ) * ( $delivery_delay_percentage / 100 ) * $core_price );
        } else { $delivery_delay_fee = 0; }
    
        // Insurance 
        if($data['package_value'] > 0 ){
          $insurance = $this->get_insurance_fees((int) $data['from_country_id'], $data['package_value'],(int)$data['category_id']);
          $insurance_fee = $insurance['ins_fee'];
        } else { $insurance_fee = 0; }
    
        // Handling Fee
        if($data['handling_by'] == 1) {
          // fh = fee02 + k2*Cp
          $handling_fee = $handling_fee_constant + ($handling_fee_percentage / 100 ) * $core_price;
        } else { $handling_fee = 0; }
    
        // Dedicated Vehicle Fee
        if($data['dedicated_vehicle'] == 1){
          // c = fee03 + k3*Cp
          $dedicated_vehicle_fee = $dedicated_vehicle_constant + ($dedicated_vehicle_percentage / 100) * $core_price; 
        } else { $dedicated_vehicle_fee = 0; }
        
        if($custom_clearance_by == "Provider" && $custom_package_value > 0 ){
          $old_goods_custom_commission_fee = number_format((( $custom_package_value * $old_goods_custom_percentage ) / 100),2,".","");
          $new_goods_custom_commission_fee =  number_format((( $custom_package_value * $new_goods_custom_percentage ) / 100),2,".","");
        } else {  $old_goods_custom_commission_fee = 0; $new_goods_custom_commission_fee = 0; }

        //echo json_encode((strtolower($service_area_type)=="international") && $old_new_goods =="Old"); die();
        if((strtolower($service_area_type)=="international") && $old_new_goods =="Old") {
          $total_price = $core_price + $delivery_delay_fee + $insurance_fee + $handling_fee + $dedicated_vehicle_fee + $old_goods_custom_commission_fee;
        }
        else if((strtolower($service_area_type)=="international") && $old_new_goods =="New"){
          $total_price = $core_price + $delivery_delay_fee + $insurance_fee + $handling_fee + $dedicated_vehicle_fee + $new_goods_custom_commission_fee;
        }
        else{ $total_price = $core_price + $delivery_delay_fee + $insurance_fee + $handling_fee + $dedicated_vehicle_fee; }

        if($loading_time > 0 && $loading_time > $loading_free_hours ){
          $panelty_hrs = $loading_time - (int) $loading_free_hours;
          $loading_unloading_charge = $charget_per_hours * $panelty_hrs;
          $total_price += $loading_unloading_charge;
        } else { $loading_unloading_charge = 0; }

        return array(
          "total_price" =>($core_price > 0 ) ? number_format($total_price,2,".","") : 0,
          "standard_price" => ($core_price > 0 ) ? number_format($core_price,2,".","") : 0,
          "urgent_fee" => ($core_price > 0 ) ? number_format($delivery_delay_fee,2,".",""):0,
          "ins_fee" => ($core_price > 0 ) ? number_format($insurance_fee,2,".",""):0,
          "handling_fee" => ($core_price > 0 ) ? number_format($handling_fee,2,".",""):0,
          "dedicated_vehicle_fee" => ($core_price > 0 ) ? number_format($dedicated_vehicle_fee,2,".",""):0,
          "currency_id" => $currency_id,
          "currency_sign" => $currency_sign,
          "currency_title" => $currency_title,
          "distance_in_km" => $distance_in_km,
          "advance_percent" => ($core_price > 0 ) ? $advance_payment_percentage : 0,       
          "advance_payment" => ($core_price > 0 ) ? number_format((( $core_price * $advance_payment_percentage ) / 100),2,".","") : 0,       
          "commission_percent" => ($core_price > 0 ) ? $gonagoo_commission_percentage : 0,        
          "old_goods_custom_commission_percent" => ($core_price > 0 ) ? $old_goods_custom_percentage : 0,
          "old_goods_custom_commission_fee" => ($core_price > 0 ) ? $old_goods_custom_commission_fee :0,       
          "new_goods_custom_commission_percent" => ($core_price > 0 ) ? $new_goods_custom_percentage : 0,        
          "new_goods_custom_commission_fee" => ($core_price > 0 ) ? $new_goods_custom_commission_fee :0,        
          "loading_unloading_charge" => $loading_unloading_charge,        
          "loading_free_hours" => $loading_free_hours,        
          "max_duration" => $max_duration,        
        );
      } 
      else { 
        return array(
          "total_price" => 0,
          "standard_price" => 0,
          "urgent_fee" => 0,
          "ins_fee" => 0,
          "handling_fee" => 0,
          "dedicated_vehicle_fee" => 0,
          "currency_id" => 0,
          "currency_sign" => "",
          "currency_title" => "",
          "distance_in_km" => $distance_in_km,
          "advance_percent" => 0,       
          "advance_payment" => 0,
          "commission_percent" => 0,        
          "old_goods_custom_commission_percent" => 0,        
          "old_goods_custom_commission_fee" => 0,       
          "new_goods_custom_commission_percent" => 0,        
          "new_goods_custom_commission_fee" => 0,    
          "loading_unloading_charge" => 0,    
          "max_duration" => 0,    
        );
      }
    }
  }

  public function get_favourite_deliverers($cust_id=0)
  {
    if($cust_id > 0 ) {
      return $this->db->query('SELECT d.*, c.ratings FROM tbl_deliverer_profile as d JOIN tbl_customers_master AS c ON d.cust_id = c.cust_id WHERE d.cust_id IN ( SELECT deliverer_id FROM tbl_favourite_deliverers WHERE cust_id = '.$cust_id.' )')->result_array();
    }
    return false;
  }

  public function register_new_favourite_deliverer($cust_id=0, $deliverer_id=0)
  {
    if($cust_id > 0 AND $deliverer_id > 0) {
      $insert_data = array(
        "cust_id" => (int) $cust_id,
        "deliverer_id" => (int) $deliverer_id,
        "cre_datetime" => date('Y-m-d H:i:s'),
      );
      return $this->db->insert($this->_favourite_deliverers, $insert_data);
    }
    return false;
  }

  public function remove_favourite_deliverer($cust_id=0, $deliverer_id=0)
  {
    if($cust_id > 0 AND $deliverer_id > 0) {
      
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->where('deliverer_id', (int) $deliverer_id);
      $this->db->delete($this->_favourite_deliverers);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function register_new_courier_orders(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_courier_orders, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function distance($latitudeFrom=null, $longitudeFrom=null, $latitudeTo=null, $longitudeTo=null)
  {
    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;

    // return $distance = ($miles * 1.609344).' km';
    return $distance = ($miles * 1.609344);
  }

  public function GetDrivingDistance($lat1, $long1, $lat2, $long2)
  {
      $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&key=".$this->config->item("google_key");
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $response = curl_exec($ch);
      curl_close($ch);
      $response_a = json_decode($response, true);
  

    if(isset($response_a['rows'][0]['elements'][0]["status"]) && $response_a['rows'][0]['elements'][0]["status"] != "ZERO_RESULTS"){
      $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
      $dist = explode('km', $dist);
      // $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
      // return array('distance' => $dist, 'time' => $time);

      return round(trim($dist[0]),2);
    }
    else { return round((string)$this->distance($lat1, $long1, $lat2, $long2),2);  }
  }

  public function get_documents($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('doc_id', 'desc');
      return $this->db->get($this->_documents)->result_array();
    } 
    return false;
  } 

  public function register_document(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_documents, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_old_document_url($cust_id=0, $doc_id=0)
  {
    if( $cust_id > 0 AND $doc_id > 0 ) {
      $this->db->select('attachement_url');
      $this->db->where('doc_id', (int) $doc_id); 
      $this->db->where('cust_id', (int) $cust_id);
      $result = $this->db->get($this->_documents)->row_array();     
      return $result['attachement_url'];
    } 
    return false;
  }

  public function update_document($doc_id=0, array $data)
  {
    if(is_array($data) AND $doc_id > 0) {
      $this->db->where('doc_id', (int)$doc_id);
      $this->db->update($this->_documents, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_document($cust_id=0, $doc_id=0)
  {
    if($cust_id > 0 AND $doc_id > 0) {      
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->where('doc_id', (int) $doc_id);
      $this->db->delete($this->_documents);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_payment_details($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('pay_id', 'desc');
      return $this->db->get($this->_payment_details)->result_array();
    } 
    return false;
  }

  public function register_payment_details(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_payment_details, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_payment_details($pay_id=0, array $data)
  {
    if(is_array($data) AND $pay_id > 0) {
      $this->db->where('pay_id', (int)$pay_id);
      $this->db->update($this->_payment_details, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_payment_detail($cust_id=0, $pay_id=0)
  {
    if($cust_id > 0 AND $pay_id > 0) {
      $this->db->where('pay_id', (int)$pay_id);
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->delete($this->_payment_details);
      return $this->db->affected_rows();
    }
    return false;
  }
  
  public function get_deliverer_for_booking($cust_id=0, $id=0, $from_country_id=0, $from_state_id=0, $from_city_id=0, $rating=0)
  {
    if($cust_id > 0 ) {
      //$this->db->select('cust_id, firstname, lastname, email1, mobile1, cust_dob, gender, city_id, state_id, country_id, avatar_url, cover_url, last_login_datetime, overview, profession, cust_status, user_type, lang_known, ratings, total_ratings, no_of_ratings, email_verified, mobile_verified');


      $this->db->select('d.*, c.ratings, c.total_ratings, c.no_of_ratings');  
      $this->db->from($this->_deliverer.' as d');
      $this->db->join($this->_customers_master.' as c', 'd.cust_id = c.cust_id');

      $this->db->where('c.cust_id !=', (int) $cust_id);
      if($from_country_id > 0) {
        $this->db->where('d.country_id', $from_country_id);
      }
      if($from_state_id > 0) {
        $this->db->where('d.state_id', $from_state_id);
      }
      if($from_city_id > 0) {
        $this->db->where('d.city_id', $from_city_id);
      }

      if($rating > 0) {
        $this->db->where('c.ratings', $rating);
      }

      $this->db->where('c.is_deliverer', 1);
      if($id > 0) {
        $this->db->where('d.deliverer_id >', $id);
      }
      $this->db->where('c.cust_status', 1);
      $this->db->order_by('d.deliverer_id', 'ASC');
      $this->db->group_by('c.cust_id');
      $this->db->limit(20);

      //$this->db->get()->result_array();
      return $this->db->get()->result_array();
      //var_dump($this->db->last_query()); die();
    } 
    return false;
  }

  public function get_booking_list($cust_id=0, $last_id=0, $order_status=null)
  {
    if($cust_id > 0 && !is_null($order_status)) {
      $last_id = (int) $last_id;
      if($order_status == 'open') {
        $this->db->where('cust_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        if($last_id > 0) { $this->db->where('order_id <', $last_id); }
        $this->db->or_where('order_status', 'reject');
        $this->db->where('cust_id', (int)($cust_id));
        if($last_id > 0) { $this->db->where('order_id <', $last_id); }
      }
      if($order_status == 'in_progress') {
        $this->db->where('cust_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        if($last_id > 0) { $this->db->where('order_id <', $last_id); }
        $this->db->or_where('order_status', 'accept');
        $this->db->where('cust_id', (int)($cust_id));
        if($last_id > 0) { $this->db->where('order_id <', $last_id); }
      }
      if($order_status == 'delivered') {
        $this->db->where('cust_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        if($last_id > 0) { $this->db->where('order_id <', $last_id); }
      }
      
      $this->db->order_by('order_id', 'desc');
      $this->db->limit(20);
  
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }

  public function get_order_packages($order_id=0)
  {
    if($order_id > 0 ){
      $this->db->where('order_id', $order_id);
      return $this->db->get($this->_order_packages)->result_array();
    }
    return false;
  }

  public function booking_delivery_request(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_courier_order_deliverer_request, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_user_device_details($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->select('reg_id, device_type');
      $this->db->where('cust_id', (int)($cust_id));
      return $this->db->get($this->_consumer_device)->result_array();
    }
    return false;
  }

  public function get_deliverer_shipping_mode($cust_id=0)
  {
    if($cust_id > 0 ) { 
      $this->db->select('shipping_mode');   
      $this->db->where('cust_id', (int)($cust_id));
      return $this->db->get($this->_deliverer)->row_array();
    }
    return false;
  }

  public function sendFCM(array $msg, array $reg_id, $api_key=null)
  {
    if(is_array($msg) && is_array($reg_id) && !is_null($api_key)) {
      $header = array('Authorization: key='.$api_key,'Content-Type: application/json');
      $fields = array('registration_ids' => $reg_id, 'data' => $msg);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
      $result_fcm = curl_exec($ch);
      curl_close($ch);
      return 1;
    } return 0;
  } 

  public function sendAPN(array $msg, array $reg_id, $pem_file=null)
  {
    foreach ($reg_id as $key) {
      $deviceToken = $key;
      $ctx = stream_context_create();
      stream_context_set_option($ctx, 'ssl', 'local_cert', $pem_file);
      $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
      //if (!$fp) exit("Failed to connect: $err $errstr" . PHP_EOL);
      // Create the payload body
      $body['aps'] = array(
           'alert' => array(
              'title' => $msg['title'],
              'body' => $msg['desc'],
              'datetime' => $msg['notice_date'],
              'type' => $msg['type']
             ),
            'sound' => 'default'
            );
      // Encode the payload as JSON
      $payload = json_encode($body);
      // Build the binary notification
      $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
      // Send it to the server
      $result = fwrite($fp, $msg, strlen($msg));
      // Close the connection to the server
      fclose($fp);
    }
    return 1;
  }

  public function update_order_status($deliverer_id=0, $order_id=0, $order_status=null, $delivery_code=null, $deliverer_name=null, $deliverer_company_name=null, $deliverer_contact_name=null)
  {
    if($deliverer_id != null && $order_id != null && !is_null($order_status) && !is_null($deliverer_name)) {
      $this->db->where('order_id', trim($order_id));
      $this->db->update($this->_courier_orders, ['order_status' => $order_status, 'status_updated_by' => 'deliverer', 'deliverer_id' => $deliverer_id, 'accept_datetime' => date('Y-m-d H:i:s'), 'status_update_datetime' => date('Y-m-d H:i:s'), 'delivery_code' => $delivery_code, 'deliverer_name' => $deliverer_name, 'deliverer_contact_name' => $deliverer_contact_name, 'deliverer_company_name' => $deliverer_company_name ]);
      return $this->db->affected_rows();
    }
    return false;
  }
  
  public function insert_order_status($deliverer_id=0, $order_id=0, $order_status=null)
  {
    if($deliverer_id > 0 && $order_id > 0 && !is_null($order_status)) {
      if($order_status == 'open') {
        $this->db->insert($this->_courier_order_status, ['order_id' => $order_id, 'user_id' => $deliverer_id, 'user_type' => 'customer', 'status' => $order_status, 'mod_datetime' => date('Y-m-d H:i:s')]);
      } else {
        $this->db->insert($this->_courier_order_status, ['order_id' => $order_id, 'user_id' => $deliverer_id, 'user_type' => 'deliverer', 'status' => $order_status, 'mod_datetime' => date('Y-m-d H:i:s')]);
      }
      return $this->db->affected_rows();
    }
    return false;
  }

  public function remove_order_request($order_id=0, $deliverer_id=0)
  {
    if( $order_id > 0) {
      $this->db->where('order_id', (int)$order_id);
      $this->db->delete($this->_courier_order_deliverer_request);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_user_details($cust_id=0)
  {
    if($cust_id > 0 ) {
      //$this->db->select('firstname, lastname, email1, mobile1');
      $this->db->where('cust_id', (int)($cust_id));
      return $this->db->get($this->_customers_master)->row_array();
    }
    return false;
  }

  public function sendEmailOrderStatus($user_name, $user_email, $subject=null, $message=null) 
  {
      $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
      $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
      $messageBody .="</head><body bgcolor='#FFFFFF'>";
      
      $messageBody .="<table class='body-wrap'><tr><td></td><td class='container'><div class='content'>";
      $messageBody .="<table><tr><td align='center'><h3>".$subject."</h3></td></tr>";
      $messageBody .="<tr><td align='center'>";
      $messageBody .= $message;

      $messageBody .="</td></tr></table></div></td><tr/></table>";
      //Email Signature
      $messageBody .="<table class='head-wrap'>";
      $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
      $messageBody .="<tr><td>Your Gonagoo team !</td></tr>";
      $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />Deliver, ship cheaper easily</td></tr>";
      $messageBody .="<tr><td>Support : support@gonagoo.com</td></tr>";
      $messageBody .="<tr><td>Website : www.gonagoo.com</td></tr>";
      //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
      $messageBody .="<tr><td>Join us on : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='G+'></a>";
      $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='Twitter'></a>";
      $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='LinkedIn'></a>";
      $messageBody .="<a href='https://www.facebook.com/gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/facebook.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='FB'></a></td></tr>";
      //Download the App <icon for iOS App download><icon for Android App download>
      $messageBody .="<tr><td>Download the App : <br /><a href='https://apple.co/2z5N4jA' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='iOS'></a>";
      $messageBody .="<a href='https://tinyurl.com/ya458s6u' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='Android'></a></td></tr>";
      $messageBody .="</table></div></td><td></td></tr></table>";
      //Email Signature End
      $messageBody .="</body></html>";

      $email_from = $this->config->item('from_email');
      $email_subject = $subject;
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= 'From: Gonagoo <'.$email_from.'>' . "\r\n";
      mail($user_email, $email_subject, $messageBody, $headers);  
      return 1;
  }

  public function post_to_workroom($order_id=0, $cust_id=0, $deliverer_id=0, $text_msg = null, $sender_type = null, $type = null, $attachment_url = null, $cust_name = null, $deliverer_name = null, $file_type = null, $thumbnail = null, $rating = "NULL")
  {
    if($order_id > 0 && $cust_id > 0 && $deliverer_id > 0 && !is_null($text_msg) && !is_null($sender_type) && !is_null($type) && !is_null($cust_name) && !is_null($deliverer_name) && !is_null($file_type)) {
    
      if($sender_type == 'sp') { $sender_id = $deliverer_id; } else { $sender_id = $cust_id; }
      $this->db->insert($this->_courier_workroom, ['order_id' => $order_id, 'cust_id' => $cust_id, 'deliverer_id' => $deliverer_id, 'text_msg' => $text_msg, 'attachment_url' => $attachment_url, 'cre_datetime' => date('Y-m-d H:i:s'), 'sender_id' => $sender_id, 'type' => $type, 'cust_name' => $cust_name, 'deliverer_name' => $deliverer_name, 'file_type' => $file_type, 'thumbnail' => $thumbnail, 'ratings' => $rating, 'cat_id' => 0]);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_deliverer_request_status($cust_id=0, $deliverer_id=0, $order_id=0)
  {
    if($cust_id > 0 && $deliverer_id > 0 && $order_id > 0 ) { 
      $this->db->where('cust_id', (int)($cust_id));
      $this->db->where('deliverer_id', (int)($deliverer_id));
      $this->db->where('order_id', (int)($order_id));
      return $this->db->get($this->_courier_order_deliverer_request)->num_rows();
    }
    return false;
  }

  public function deliverer_order_request_list_old($cust_id=0, $last_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->select($this->_courier_order_deliverer_request.'.request_id, '.$this->_courier_orders.'.*');
      //$this->db->from($this->_courier_order_deliverer_request);

      $this->db->join($this->_courier_orders, $this->_courier_orders . '.order_id = ' . $this->_courier_order_deliverer_request . '.order_id', 'right outer');

      $this->db->where($this->_courier_order_deliverer_request.'.deliverer_id', (int)($cust_id));
      $this->db->order_by($this->_courier_order_deliverer_request.'.request_id', 'desc');
      $this->db->limit(20);
      $last_id = (int) $last_id;
      if($last_id > 0) {
        $this->db->where($this->_courier_order_deliverer_request.'.request_id <', $last_id);
      }
      //return $this->db->get($this->_courier_order_deliverer_request)->result_array();
      return $this->db->get($this->_courier_order_deliverer_request)->result_array();
      //var_dump($this->db->last_query()); die();
    }
    return false;
  }
  
  public function deliverer_order_request_list(array $data) //$cust_id=0, $last_id=0
  {
    //echo json_encode($data); die();
    $cust_id = $data['cust_id'];
    $last_id = $data['last_id'];
    if($cust_id > 0 ) {
      $this->db->select($this->_courier_order_deliverer_request.'.request_id, '.$this->_courier_orders.'.*');
      $this->db->join($this->_courier_orders, $this->_courier_orders . '.order_id = ' . $this->_courier_order_deliverer_request . '.order_id', 'right outer');

      $this->db->where($this->_courier_order_deliverer_request.'.deliverer_id', $cust_id);
      //$this->db->where($this->_courier_order_deliverer_request.'.deliverer_id', (int)($cust_id));

      if($data['filter_type'] == 'advance'){
          // From country, state, city filter
          if($data['from_country_id'] > 0 ) { $this->db->where($this->_courier_orders.'.from_country_id', (int) $data['from_country_id']);  }
          if($data['from_state_id'] > 0 ) { $this->db->where($this->_courier_orders.'.from_state_id', (int) $data['from_state_id']);  }
          if($data['from_city_id'] > 0 ) {  $this->db->where($this->_courier_orders.'.from_city_id', (int) $data['from_city_id']);  }
          // To Country, state, city filter
          if($data['to_country_id'] > 0 ) { $this->db->where($this->_courier_orders.'.to_country_id', (int) $data['to_country_id']);  }
          if($data['to_state_id'] > 0 ) { $this->db->where($this->_courier_orders.'.to_state_id', (int) $data['to_state_id']);  }
          if($data['to_city_id'] > 0 ) {  $this->db->where($this->_courier_orders.'.to_city_id', (int) $data['to_city_id']);  }
          // From address
          if($data['from_address'] !="NULL" ) { $this->db->like($this->_courier_orders.'.from_address', trim($data['from_address']), 'BOTH'); }
          // To address
          if($data['to_address'] !="NULL" ) { $this->db->like($this->_courier_orders.'.to_address', trim($data['to_address']), 'BOTH'); }
          // Date filter
          if(trim($data['delivery_start_date']) !="NULL" AND trim($data['delivery_start_date']) !="" AND trim($data['delivery_end_date']) !="NULL" AND trim($data['delivery_end_date']) !="" ) {  $this->db->where($this->_courier_orders.'.delivery_datetime BETWEEN "'. $data['delivery_start_date']. '" AND "'. $data['delivery_end_date'].'"'); }
          if(trim($data['pickup_start_date']) !="NULL" AND trim($data['pickup_start_date']) !="" AND trim($data['pickup_end_date']) !="NULL" AND trim($data['pickup_end_date']) !="" ) {  $this->db->where($this->_courier_orders.'.delivery_datetime BETWEEN "'. $data['pickup_start_date']. '" AND "'. $data['pickup_end_date'].'"'); }
          // Price Filter
          if($data['price'] > 0) { $this->db->where($this->_courier_orders.'.order_price <='. $data['price']); }
          //Dimension Filter
          if($data['dimension_id'] > 0) { $this->db->where($this->_courier_orders.'.dimension_id='. $data['dimension_id']); }
          //Order Type Filter
          if(trim($data['order_type']) != "NULL") { $this->db->where($this->_courier_orders.'.order_type ', $data['order_type']); }
          //Creation Date Filter
          if(trim($data['creation_start_date']) !="NULL" AND trim($data['creation_start_date']) !="" AND trim($data['creation_end_date']) !="NULL" AND trim($data['creation_end_date']) !="" ) {  $this->db->where($this->_courier_orders.'.cre_datetime BETWEEN "'. $data['creation_start_date']. '" AND "'. $data['creation_end_date'].'"');  }
          //Expirty Date  Filter
          if(trim($data['expiry_start_date']) !="NULL" AND trim($data['expiry_start_date']) !="" AND trim($data['expiry_end_date']) !="NULL" AND trim($data['expiry_end_date']) !="" ) {  $this->db->where($this->_courier_orders.'.expiry_date  BETWEEN "'. $data['expiry_start_date']. '" AND "'. $data['expiry_end_date'].'"');  }
          //Distance Filter
          if($data['distance_start'] > 0 AND $data['distance_end'] > 0 ) {  $this->db->where($this->_courier_orders.'.distance_in_km BETWEEN "'. $data['distance_start']. '" AND "'. $data['distance_end'].'"');  }
          // Transport type Filter
          if($data['transport_type'] != "NULL") { $this->db->where($this->_courier_orders.'.transport_type', $data['transport_type']); }
          // Vehicle Filter
          if($data['vehicle_id'] > 0) { $this->db->where($this->_courier_orders.'.vehical_type_id', $data['vehicle_id']); }
          // Vehicle Filter
          if($data['max_weight'] > 0) { $this->db->where($this->_courier_orders.'.total_weight <='. $data['max_weight']); }
          if($data['unit_id'] > 0) { $this->db->where($this->_courier_orders.'.unit_id', $data['unit_id']); }
          if($data['cat_id'] > 0) { $this->db->where($this->_courier_orders.'.category_id', $data['cat_id']); }
         
      } // advance filter ends here

      $this->db->order_by($this->_courier_order_deliverer_request.'.request_id', 'desc');
      $this->db->limit(20);
      $last_id = (int) $last_id;
      if($last_id > 0) {
        $this->db->where($this->_courier_order_deliverer_request.'.request_id <', $last_id);
      }
      return $this->db->get($this->_courier_order_deliverer_request)->result_array();
    }
    return false;
  }

  public function deliverer_order_list($cust_id=0, $last_id=0, $order_status=null)
  {
    if($cust_id > 0 && !is_null($order_status)) {
      $last_id = (int) $last_id;
      if ($order_status == 'accept') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        if($last_id > 0) {  $this->db->where('order_id <', $last_id); }
      } 
      if ($order_status == 'assign' || $order_status == 'in_progress') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        if($last_id > 0) {  $this->db->where('order_id <', $last_id); }
        $this->db->or_where('order_status', 'assign');
        $this->db->where('deliverer_id', (int)($cust_id));
        if($last_id > 0) {  $this->db->where('order_id <', $last_id); }
      }
      if ($order_status == 'delivered') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        if($last_id > 0) {  $this->db->where('order_id <', $last_id); }
      }
      if ($order_status == 'rejected') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        if($last_id > 0) {  $this->db->where('order_id <', $last_id); }
      }
      
      $this->db->order_by('order_id', 'desc');
      $this->db->limit(10);
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }

  public function update_order_status_deliverer($cd_id=0, $order_id=0, $cd_name=null)
  {
    if($cd_id != null && $order_id != null && !is_null($cd_name)) {
      $this->db->where('order_id', trim($order_id));
      $this->db->update($this->_courier_orders, ['order_status' => 'accept', 'status_updated_by' => 'deliverer', 'driver_id' => $cd_id, 'cd_name' => $cd_name, 'assigned_datetime' => date('Y-m-d H:i:s'), 'status_update_datetime' => date('Y-m-d H:i:s'), 'driver_status_update' => 'assign']);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_driver_details($cd_id=0)
  {
    if($cd_id > 0 ) {
      //$this->db->select('first_name, last_name, email, mobile1');
      $this->db->where('cd_id', (int)($cd_id));
      return $this->db->get($this->_customer_drivers)->row_array();
    }
    return false;
  }

  public function get_driver_device_details($cd_id=0)
  {
    if($cd_id > 0 ) {
      $this->db->select('reg_id, device_type');
      $this->db->where('cd_id', (int)($cd_id));
      return $this->db->get($this->_driver_device)->result_array();
    }
    return false;
  }
  
  public function get_delivery_code($order_id=0)
  {
    if($order_id > 0 ) {
      $this->db->select('delivery_code');
      $this->db->where('order_id', (int)($order_id));
      return $this->db->get($this->_courier_orders)->row_array();
    }
    return false;
  }

  public function update_review(array $data, $order_id=0)
  {
    if(is_array($data) && $order_id != null) {
      $this->db->where('order_id', trim($order_id));
      $this->db->update($this->_courier_orders, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_user_review_details($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->select('ratings, total_ratings, no_of_ratings');
      $this->db->where('cust_id', (int)($cust_id));
      return $this->db->get($this->_customers_master)->row_array();
    }
    return false;
  }

  public function update_deliverer_rating(array $deliverer_review, $deliverer_id=0)
  {
    if(is_array($deliverer_review) && $deliverer_id != null) {
      $this->db->where('cust_id', trim($deliverer_id));
      $this->db->update($this->_customers_master, $deliverer_review);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_courier_rating($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->select('rating_id, ratings, no_of_ratings, total_ratings');
      $this->db->where('deliverer_id', (int)($cust_id));
      return $this->db->get($this->_deliverer_ratings)->row_array();
    }
    return false;
  }

  public function manage_courier_ratings(array $deliverer_review, $key=null, $rating_id=0)
  {
    if(is_array($deliverer_review) && $key != null) {
      if($key == "update" && $rating_id > 0) { 
        $this->db->where('rating_id', trim($rating_id));
        $this->db->update($this->_deliverer_ratings, $deliverer_review);
      } else { 
        $this->db->insert($this->_deliverer_ratings, $deliverer_review);
      } 
      return $this->db->affected_rows();
    } 
    return false;
  }

  public function get_driver_location($cd_id=0)
  {
    if($cd_id > 0 ) {
      $this->db->select('latitude, longitude, loc_update_datetime');
      $this->db->where('cd_id', (int)($cd_id));
      return $this->db->get($this->_customer_drivers)->row_array();
    }
    return false;
  }

  public function get_order_detail($order_id=0)
  {
    if($order_id > 0 ) {
      $this->db->where('order_id', (int)($order_id));
      return $this->db->get($this->_courier_orders)->row_array();
    }
    return false;
  }

  public function get_chat_details($id=0)
  {
    if($id > 0 ){
      $this->db->where('ow_id', (int)$id);
      return $this->db->get($this->_courier_workroom)->row_array();
    }
    return false;
  }

  public function get_workroom_chat__old($order_id=0, $ow_id=0)
  {
    if( $order_id > 0 ) {
      $ow_id = (int) $ow_id;
      if($ow_id > 0) {
        $this->db->where('ow_id <', $ow_id);
      }
      $this->db->where('cat_id', 0);
      $this->db->order_by('ow_id', 'desc');
      $this->db->limit(20);
      return $this->db->get($this->_courier_workroom)->result_array();
    }
    return false;
  }
  
  public function get_workroom_chat($order_id=0)
  {
    if( $order_id > 0 ) {     
      $this->db->where('order_id ', $order_id);
      $this->db->where('cat_id', 0);
      $this->db->order_by('ow_id', 'desc');
      return $this->db->get($this->_courier_workroom)->result_array();
    }
    return false;
  }
  
  public function get_order_chat($order_id=0)
  {
    if( $order_id > 0 ) {     
      $this->db->where('order_id ', $order_id);
      $this->db->order_by('chat_id', 'ASC');
      return $this->db->get($this->_driver_chats)->result_array();
    }
    return false;
  }
  
  public function customer_chat_post($data=null)
  {
    if(is_array($data)){
      $this->db->insert($this->_driver_chats, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_customer_chat_details($id=0)
  {
    if($id > 0 ){
      $this->db->where('chat_id', (int)$id);
      return $this->db->get($this->_driver_chats)->row_array();
    }
    return false;
  }

  public function customer_chat_history($cd_id=0, $cust_id=0, $chat_id=0, $order_id=0)
  {
    if($cd_id > 0 && $cust_id > 0 ) {
      $chat_id = (int) $chat_id;
      $order_id = (int) $order_id;
      if($chat_id > 0) {
        $this->db->where('chat_id <', $chat_id);
      }
      $this->db->where('cust_id = '.$cust_id.' AND sender_id = '.$cust_id.' AND receiver_id = '.$cd_id.' AND order_id = '.$order_id.'');

      $this->db->or_where('cd_id = '.$cd_id.' AND sender_id = '.$cd_id.' AND receiver_id = '.$cust_id.' AND order_id = '.$order_id.'');
      if($chat_id > 0) {
        $this->db->where('chat_id <', $chat_id);
      }

      $this->db->order_by('chat_id', 'desc');
      $this->db->limit(20);
      return $this->db->get($this->_driver_chats)->result_array();
    }
    return false;
  }

  public function courier_order_payment_update($id=0, array $data)
  {
    if(is_array($data) AND $id > 0 ){
      $this->db->where('order_id', (int) $id);
      $this->db->update($this->_courier_orders, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_courier_orders(array $update_data, $order_id=0)
  {
    if(is_array($update_data) AND $order_id > 0) {
      $this->db->where('order_id', (int)$order_id);
      $this->db->update($this->_courier_orders, $update_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function cancel_courier_order($order_id=0)
  {
    if($order_id > 0) {
      $this->db->where('order_id', (int)$order_id);
      $this->db->update($this->_courier_orders, ['order_status' => 'cancel']);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function insert_order_status_customer($cust_id=0, $order_id=0, $order_status=null)
  {
    if($cust_id != null && $order_id != null && !is_null($order_status)) {
      $this->db->insert($this->_courier_order_status, ['order_id' => $order_id, 'user_id' => $cust_id, 'user_type' => 'customer', 'status' => $order_status, 'mod_datetime' => date('Y-m-d H:i:s')]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_deliverer_company_name($cust_id=0)
  {
    if($cust_id > 0 ) { 
      $this->db->select('company_name, deliverer_id');    
      $this->db->where('cust_id', (int)($cust_id));
      return $this->db->get($this->_deliverer)->row_array();
    }
    return false;
  }

  public function post_to_review(array $insert_data)
  {
    if(is_array($insert_data)) {
      $this->db->insert($this->_courier_order_review, $insert_data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_reviews($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('deliverer_id', (int)($cust_id));
      return $this->db->get($this->_courier_order_review)->result_array();
    }
    return false;
  }

  public function get_insurance_fees($country_id=0, $package_value=0,$cat_id=0)
  {
    if($country_id > 0 && $country_id > 0 && $cat_id > 0 ) {
      $this->db->where('country_id', (int)$country_id);
      $this->db->where('category_id', (int)$cat_id);
      $this->db->where("$package_value BETWEEN min_value AND max_value");
      return $this->db->get($this->_insurance_master)->row_array();
    }
    return false;
  }

  public function get_deliverer_details($deliverer_id=0)
  {
    if($deliverer_id > 0 ) {
      $this->db->where('deliverer_id', (int) $deliverer_id);
      return $this->db->get($this->_deliverer)->row_array();
    }
    return null;
  }

  public function order_status_list($order_id=0)
  {
    if($order_id > 0 ) {
      $this->db->where('order_id', (int)($order_id));
      $this->db->where('status !=', 'reject');
      return $this->db->get($this->_courier_order_status)->result_array();
    }
    return false;
  }

  public function get_deliverer_documents($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('doc_id', 'desc');
      return $this->db->get($this->_deliverer_documents)->result_array();
    } 
    return false;
  }

  public function register_deliverer_document(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_deliverer_documents, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_deliverer_old_document_url($doc_id=0)
  {
    if( $doc_id > 0 ) {
      $this->db->select('attachement_url');
      $this->db->where('doc_id', (int) $doc_id); 
      $result = $this->db->get($this->_deliverer_documents)->row_array();     
      return $result['attachement_url'];
    } 
    return false;
  }

  public function update_deliverer_document($doc_id=0, array $data)
  {
    if(is_array($data) && $doc_id > 0) {
      $this->db->where('doc_id', (int)$doc_id);
      $this->db->update($this->_deliverer_documents, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_deliverer_document($doc_id=0)
  {
    if( $doc_id > 0) {      
      $this->db->where('doc_id', (int) $doc_id);
      $this->db->delete($this->_deliverer_documents);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_order_workroom_count($order_id=0)
  {
    if( $order_id > 0 ) {
      $this->db->where('order_id', (int) $order_id); 
      $this->db->where('cat_id', 0);
      return $this->db->get($this->_courier_workroom)->num_rows();
    } 
    return false;
  }

  public function get_open_booking_list($last_id=0, $cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->where('cust_id !=', $cust_id);     
      $this->db->where('order_status', 'open');     
      $this->db->order_by('order_id', 'desc');
      $this->db->limit(20);
      $last_id = (int) $last_id;
      if($last_id > 0) {
        $this->db->where('order_id <', $last_id);
      }
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }

  public function get_relay_point_list(array $data)
  {
                if($data['country_id'] > 0) { $this->db->where('country_id', $data['country_id']);  }
    if($data['state_id'] > 0) { $this->db->where('state_id', $data['state_id']);  }
    if($data['city_id'] > 0) { $this->db->where('city_id', $data['city_id']);  }
    $this->db->where('status', 1); 
    return $this->db->get($this->_relay_points)->result_array();
  }
  
  public function verify_favorite($did=0, $cust_id=0)
  {
    if($did > 0 && $cust_id > 0) {
      $this->db->where('deliverer_id', (int) $did);
      $this->db->where('cust_id', (int) $cust_id);
      return $this->db->get($this->_favourite_deliverers)->row_array();
    }
    return false;
  } 

  public function update_verified_email_or_otp($cust_id=0, $type=null)
  {
    if( $cust_id > 0 AND $type != null ) {
      
      if($type == "otp") {  $data = array( "mobile_verified" => 1); }
      else if($type == "email") { $data = array( "email_verified" => 1); }
      else { return false; }

      $this->db->where('cust_id', (int) $cust_id);
      $this->db->update($this->_customers_master, $data);
      return $this->db->affected_rows();
    }
    return false;
  }


  public function get_total_unread_notifications_count($cust_id=0, $type=null)
  {
    $qry = "SELECT id FROM `tbl_notifications` WHERE id NOT IN ( SELECT notification_id FROM tbl_read_notifications WHERE cust_id = $cust_id ) AND ( `consumer_id` = $cust_id OR `consumer_id` = 0 ) AND (`consumer_filter` = 'All' OR `consumer_filter` = '".$type."' )";
    return $this->db->query($qry)->num_rows();    
  }

  public function get_total_unread_notifications($cust_id=0,$type=null, $limit=0)
  {
    $qry = "SELECT id, notify_title, notify_text, attachement_url, cre_datetime FROM `tbl_notifications` WHERE id NOT IN ( SELECT notification_id FROM tbl_read_notifications WHERE cust_id = $cust_id ) AND ( `consumer_id` = $cust_id OR `consumer_id` = 0 ) AND (`consumer_filter` = 'All' OR `consumer_filter` = '".$type."' ) ORDER BY `cre_datetime` DESC "; 
                if($limit > 0 ) { $qry .= " LIMIT $limit"; }
    return  $this->db->query($qry)->result_array();
  }

  public function get_notifications($user_id=0, $type=null, $limit=50, $last_id = 0)
  { 
    $qry = "SELECT * FROM `tbl_notifications` WHERE ( `consumer_id` = $user_id OR `consumer_id` = 0 ) AND (`consumer_filter` = 'All' OR `consumer_filter` = '".$type."' )";
    if($last_id > 0 ){ $qry .= " AND id < ". $last_id; }
    $qry .= " ORDER BY `cre_datetime` DESC ";
                if($limit > 0 ) { $qry .= " LIMIT $limit"; }

    return $this->db->query($qry)->result_array();
  }

  public function get_notification($id=0)
  {
    if($id>0){
      $this->db->where('id', $id);
      return $this->db->get($this->_notifications)->row_array();
    }
    return false;
  }

  public function make_notification_read($id=0, $cust_id=0)
  {
    if($id > 0 AND $cust_id > 0) {
      $this->db->insert($this->_read_notifications, ['cust_id' => $cust_id, 'notification_id' => $id, 'read_datetime' => date('Y-m-d H:i:s')]);
      return true;
    }
    return false;
  }

  public function get_words($sentence, $count=10) {
    preg_match("/(?:\w+(?:\W+|$)){0,$count}/", $sentence, $matches);
    return $matches[0];
  }

  public function get_total_unread_workroom_notifications_count($cust_id=0)
  {
    $qry = "SELECT ow_id FROM tbl_courier_workroom WHERE ow_id NOT IN ( SELECT notification_id FROM tbl_read_workroom_notification WHERE cust_id = $cust_id ) AND  sender_id != $cust_id AND (cust_id = $cust_id OR deliverer_id = $cust_id)"; 
    return $this->db->query($qry)->num_rows();    
  }

  public function get_total_unread_workroom_notifications($cust_id=0, $limit=5)
  {
    $qry = "SELECT * FROM tbl_courier_workroom WHERE ow_id NOT IN ( SELECT notification_id FROM tbl_read_workroom_notification WHERE cust_id = $cust_id ) AND  sender_id != $cust_id AND (cust_id = $cust_id OR deliverer_id = $cust_id) ORDER BY `cre_datetime` DESC LIMIT $limit";
    return $this->db->query($qry)->result_array();    
  }

  public function get_total_unread_order_workroom_notifications($order_id=0, $cust_id=0)
  {
    $qry = "SELECT ow_id FROM tbl_courier_workroom WHERE ow_id NOT IN ( SELECT notification_id FROM tbl_read_workroom_notification WHERE cust_id = $cust_id AND notification_id != ow_id ) AND  sender_id != $cust_id AND order_id >= $order_id ";
    return $this->db->query($qry)->result_array();  

  }

  public function get_workroom_previous_ids($id=0,$order_id=0)
  {
    if($id > 0 AND $order_id > 0){

    }
    return false;
  }
  
  public function make_workroom_notification_read($id=0, $cust_id=0)
  {
    if($id > 0 AND $cust_id > 0) {
      $data = ['cust_id' => $cust_id, 'notification_id' => $id, 'read_datetime' => date('Y-m-d H:i:s')];
      $this->db->insert($this->_read_workroom_notification, $data);
      return true;
    }
    return false;
  }

  public function make_all_order_workroom_notification_read($id=0, $cust_id=0)
  {
    if($id > 0 AND $cust_id > 0) {
      $data = ['cust_id' => $cust_id, 'notification_id' => $id, 'read_datetime' => date('Y-m-d H:i:s')];
      $this->db->insert($this->_read_workroom_notification, $data);
      return true;
    }
    return false;
  }

  public function get_workorder_list($cust_id=0, $order_id=0)
  {
    if($cust_id > 0 ){
      if($order_id > 0) { $this->db->where('order_id', $order_id); }
      $this->db->where("order_status !='open'");
      $this->db->where("order_status !='reject'");
      $this->db->where('cust_id', $cust_id);
      $this->db->or_where('deliverer_id', $cust_id);
      $this->db->order_by('status_update_datetime', 'desc');
      if($order_id > 0) {
        return $this->db->get($this->_courier_orders)->row_array();
      } else {
        return $this->db->get($this->_courier_orders)->result_array();
      }
    }
    return false;
  }

  public function get_booking_workorder_list($cust_id=0, $ticket_id=0)
  {
    if($cust_id > 0 ){
      if($ticket_id > 0) {
        $this->db->where('ticket_id', $ticket_id);
        return $this->db->get($this->_bus_booking_master)->row_array();
      } else {
        $this->db->where('complete_paid', 1);
        $this->db->where('cust_id', $cust_id);
        $this->db->or_where('operator_id', $cust_id);
        $this->db->order_by('status_update_datetime', 'desc');
        return $this->db->get($this->_bus_booking_master)->result_array();
      }
    }
    return false;
  }

  public function get_global_workroom_list($cust_id=0, $last_id=0)
  {
    if( $cust_id > 0 ) {     
      $this->db->where('cust_id', $cust_id);
      $this->db->or_where('deliverer_id', $cust_id);
      if($last_id > 0) { $this->db->where('gw_id >', $last_id); }
      $this->db->order_by('gw_id', 'desc');
      return $this->db->get($this->_global_workroom)->result_array();
    }
    return false;
  }

  public function get_bus_operator_profile($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      return $this->db->get($this->_bus_operator_profile)->row_array();
    }
    return false;
  }

  public function get_operator_bus_details($bus_id=0)
  {
    if($bus_id > 0 ){
      $this->db->where('bus_id', (int) $bus_id);
      $this->db->where('bus_status', 1);
      return $this->db->get($this->_bus_master)->row_array();
    }
    return false;
  }

  public function get_trip_master_details($trip_id=0)
  {
    if($trip_id > 0 ){
      $this->db->where('trip_id', (int) $trip_id);
      $this->db->where('trip_status', 1);
      return $this->db->get($this->_bus_trip_master)->result_array();
    }
    return false;
  }

  public function get_trip_booking_details($ticket_id=0)
  {
    if($ticket_id > 0) {
      $this->db->where('ticket_id', (int)$ticket_id);
      return $this->db->get($this->_bus_booking_master)->row_array();
    } else return false;
  }
  
  public function get_loc_lat_long_by_id($point_id=0)
  { 
    if($point_id > 0) {
      $this->db->where('point_id', (int)$point_id);
      $data = $this->db->get($this->_bus_locations_pickup_drop_points)->row_array();
      return $data['loc_lat_long'];
    }
    return false;
  }

  public function get_courier_workorder_list($cust_id=0, $order_id=0)
  {
    if($cust_id > 0 ){
      if($order_id > 0) { $this->db->where('order_id', $order_id); }
      $this->db->where("order_status !='open'");
      $this->db->where("order_status !='reject'");
      $this->db->where('cust_id', $cust_id);
      $this->db->or_where('deliverer_id', $cust_id);
      $this->db->order_by('status_update_datetime', 'desc');
      if($order_id > 0) {
        return $this->db->get($this->_courier_orders)->row_array();
      } else {
        return $this->db->get($this->_courier_orders)->result_array();
      }
    }
    return false;
  }

  public function get_total_unread_chatroom_notifications_count($cust_id=0)
  {
    $qry = "SELECT chat_id FROM tbl_driver_chats WHERE chat_id NOT IN ( SELECT notification_id FROM tbl_read_chat_notification WHERE cust_id = $cust_id ) AND ( receiver_id = $cust_id )";
    return $this->db->query($qry)->num_rows();    
  }

  public function get_total_unread_chatroom_notifications($cust_id=0, $limit=5)
  {
    $qry = "SELECT * FROM tbl_driver_chats WHERE chat_id NOT IN ( SELECT notification_id FROM tbl_read_chat_notification WHERE cust_id = $cust_id ) AND ( receiver_id = $cust_id )  ORDER BY chat_id DESC LIMIT $limit";
    return $this->db->query($qry)->result_array();    
  }

  public function get_total_unread_order_chatroom_notifications($order_id=0, $cust_id=0)
  {
    $qry = "SELECT chat_id FROM tbl_driver_chats WHERE chat_id NOT IN ( SELECT notification_id FROM tbl_read_chat_notification WHERE cust_id = $cust_id AND notification_id != chat_id ) AND ( receiver_id = $cust_id ) AND ( order_id >= $order_id )";
    return $this->db->query($qry)->result_array();  
  }

  public function make_chatrooom_notification_read($id=0, $cust_id=0)
  {
    if($id > 0 AND $cust_id > 0) {
      $data = ['cust_id' => $cust_id, 'notification_id' => $id, 'read_datetime' => date('Y-m-d H:i:s')];
      $this->db->insert($this->_read_chat_notification, $data);
      return true;
    }
    return false;
  }

  public function register_nulled_account_master($cust_id=0)
  {
    if($cust_id > 0 ){
      $nulled_data = array(
        "user_id" => (int) $cust_id,
        "account_balance" => 0,
        "update_datetime" => date('Y-m-d h:i:s'),
      );
      $this->db->insert($this->_user_account_master, $nulled_data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function register_nulled_scrow_master($cust_id=0)
  {
    if($cust_id > 0 ){
      $nulled_data = array(
        "deliverer_id" => (int) $cust_id,
        "scrow_balance" => 0,
        "update_datetime" => date('Y-m-d h:i:s'),
      );
      $this->db->insert($this->_deliverer_scrow_master, $nulled_data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_account_id($account_id=0, $cust_id=0)
  {
    if($account_id > 0 && $cust_id > 0) {
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->update($this->_customers_master, ["account_id" => $account_id]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_scrow_id($scrow_id=0, $cust_id=0)
  {
    if($scrow_id > 0 && $cust_id > 0) {
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->update($this->_customers_master, ["scrow_id" => $scrow_id]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_payment_details_in_order(array $update_data, $order_id=0)
  {
    if(is_array($update_data) && $order_id > 0) {
      $this->db->where('order_id', (int)$order_id);
      $this->db->update($this->_courier_orders, $update_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function gonagoo_master_details($currency_sign=null)
  {
    if(!is_null($currency_sign)) {
      $this->db->where('currency_code', trim($currency_sign));
      return $this->db->get($this->_gonagoo_account_master)->row_array();
    }
    return false;
  }

  public function insert_gonagoo_master_record(array $insert_data_gonagoo_master)
  {
    if(is_array($insert_data_gonagoo_master)) {
      $this->db->insert($this->_gonagoo_account_master, $insert_data_gonagoo_master);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_payment_in_gonagoo_master($gonagoo_id=0, $gonagoo_balance=null)
  {
    if(!is_null($gonagoo_balance)) {
      $this->db->where('gonagoo_id', (int)$gonagoo_id);
      $this->db->update($this->_gonagoo_account_master, ['gonagoo_balance' => $gonagoo_balance]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function insert_payment_in_gonagoo_history(array $insert_data)
  {
    if(is_array($insert_data)) {
      $this->db->insert($this->_gonagoo_account_history, $insert_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function customer_account_master_details($cust_id=0, $currency_sign=null)
  {
    if($cust_id > 0 && !is_null($currency_sign)) {
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('currency_code', trim($currency_sign));
      return $this->db->get($this->_user_account_master)->row_array();
    }
    return false;
  }

  public function customer_account_master_list($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->where('user_id', (int)$cust_id);
      return $this->db->get($this->_user_account_master)->result_array();
    }
    return false;
  }

  public function insert_gonagoo_customer_record(array $insert_data_customer_master)
  {
    if(is_array($insert_data_customer_master)) {
      $this->db->insert($this->_user_account_master, $insert_data_customer_master);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_payment_in_customer_master($account_id=0, $cust_id=0, $account_balance=0)
  {
    if($cust_id > 0 && $account_id > 0) {
      $this->db->where('account_id', (int)$account_id);
      $this->db->where('user_id', (int)$cust_id);
      $this->db->update($this->_user_account_master, ['account_balance' => $account_balance]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function insert_payment_in_account_history(array $insert_data)
  {
    if(is_array($insert_data)) {
      $this->db->insert($this->_user_account_history, $insert_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function insert_gonagoo_customer_scrow_record(array $insert_data_customer_scrow)
  {
    if(is_array($insert_data_customer_scrow)) {
      $this->db->insert($this->_deliverer_scrow_master, $insert_data_customer_scrow);
      return $this->db->insert_id();
    }
    return false;
  }

  public function deliverer_scrow_master_details($deliverer_id=0, $currency_sign=null)
  {
    if($deliverer_id > 0 && !is_null($currency_sign)) {
      $this->db->where('deliverer_id', (int)$deliverer_id);
      $this->db->where('currency_code', $currency_sign);
      return $this->db->get($this->_deliverer_scrow_master)->row_array();
    }
    return false;
  }

  public function deliverer_scrow_master_list($deliverer_id=0)
  {
    if($deliverer_id > 0) {
      $this->db->where('deliverer_id', (int)$deliverer_id);
      return $this->db->get($this->_deliverer_scrow_master)->result_array();
    }
    return false;
  }

  public function update_payment_in_deliverer_scrow($scrow_id=0, $deliverer_id=0, $scrow_balance=0)
  {
    if($deliverer_id > 0 && $scrow_id > 0) {
      $this->db->where('deliverer_id', (int)$deliverer_id);
      $this->db->where('scrow_id', (int)$scrow_id);
      $this->db->update($this->_deliverer_scrow_master, ['scrow_balance' => $scrow_balance]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function insert_payment_in_scrow_history(array $insert_data)
  {
    if(is_array($insert_data)) {
      $this->db->insert($this->_deliverer_scrow_history, $insert_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_country_details($country_id=0)
  {
    if($country_id > 0 ) {
      $this->db->where('country_id', (int)($country_id));
      return $this->db->get($this->_countries)->row_array();
    }
    return false;
  }

  public function get_state_details($state_id=0)
  {
    if($state_id > 0 ) {
      $this->db->where('state_id', (int)($state_id));
      return $this->db->get($this->_states)->row_array();
    }
    return false;
  }
  
  public function get_city_details($city_id=0)
  {
    if($city_id > 0 ) {
      $this->db->where('city_id', (int)($city_id));
      return $this->db->get($this->_cities)->row_array();
    }
    return false;
  }
  
  public function update_deliverer_invoice_url($order_id=0, $invoice_url=null)
  {
    if( $order_id != null && !is_null($invoice_url) ) {
      $this->db->where('order_id', trim($order_id));
      $this->db->update($this->_courier_orders, ['deliverer_invoice_url' => $invoice_url]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_country_code_by_id($country_id=0)
  {
    if($country_id > 0){
      $this->db->select('country_phonecode');
      $this->db->where('country_id', (int)$country_id);
      $this->db->limit(1);
      $return = $this->db->get($this->_countries)->row_array();
      return $return['country_phonecode'];
    }
    return false;
  }

  public function user_account_withdraw_request(array $insert_data)
  {
    if(is_array($insert_data)) {
      $this->db->insert($this->_account_withdraw_request, $insert_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function check_advance_transfer_in_deliverer_account_master($order_id=0)
  {
    if($order_id > 0) {
      $this->db->where('order_id', (int) $order_id);
      $this->db->where('transaction_type', 'advance_payment');
      $this->db->where('type', 1);
      return $this->db->get($this->_user_account_history)->num_rows();
    }
    return false;
  }

  public function get_assign_status($order_id=0)
  {
    if($order_id > 0) {
      $this->db->where('order_id', (int) $order_id);
      $this->db->where('user_type', 'deliverer');
      $this->db->where('status', 'assign');
      return $this->db->get($this->_courier_order_status)->num_rows();
    }
    return false;
  }
  
  public function get_order_details_driver($driver_code=null, $cust_id=0)
  {
    if(!is_null($driver_code) && $cust_id > 0) {
      $cust_id = (int) $cust_id;
      $this->db->where('driver_code', $driver_code);
      $this->db->where('cust_id', $cust_id);
      return $this->db->get($this->_courier_orders)->row_array();
    }
    return false;
  }

  public function check_user_account_balance($cust_id=0, $currency_code=null)
  {
    if($cust_id > 0 && !is_null($currency_code)) {
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('currency_code', $currency_code);
      $row = $this->db->get($this->_user_account_master)->row_array();
      return $row['account_balance'];
    }
    return false;
  }

  public function check_relay_account_balance($relay_id=0, $currency_code=null)
  {
    if($relay_id > 0 && !is_null($currency_code)) {
      $this->db->where('user_id', (int)$relay_id);
      $this->db->where('currency_code', $currency_code);
      $row = $this->db->get($this->_relay_account_master)->row_array();
      return $row['account_balance'];
    }
    return false;
  }

  public function get_customer_account_history($cust_id=0, $last_id=0)
  {
    if($cust_id > 0) {
      $this->db->where('user_id', (int)$cust_id);
      if($last_id > 0) { $this->db->where('ah_id <', $last_id); }
      $this->db->limit(10);
      $this->db->order_by('ah_id', 'desc');
      return $this->db->get($this->_user_account_history)->result_array();
    }
    return false;
  }

  public function get_customer_scrow_history($deliverer_id=0, $last_id=0)
  {
    if($deliverer_id > 0) {
      $this->db->where('deliverer_id', (int)$deliverer_id);
      if($last_id > 0) { $this->db->where('scrow_id <', $last_id); }
      $this->db->limit(10);
      $this->db->order_by('scrow_id', 'desc');
      return $this->db->get($this->_deliverer_scrow_history)->result_array();
    }
    return false;
  }

  public function get_customer_account_withdraw_request_list($cust_id=0, $last_id=0)
  {
    if($cust_id > 0) {
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('user_type', 0);
      if($last_id > 0) { $this->db->where('req_id <', $last_id); }
      $this->db->limit(10);
      $this->db->order_by('req_id', 'desc');
      return $this->db->get($this->_account_withdraw_request)->result_array();
    }
    return false;
  }

  public function get_customer_account_withdraw_request_sum($user_type=0, $cust_id=0, $response=null)
  {
    if($cust_id > 0 && !is_null($response)) {
      $this->db->select('SUM(amount) AS amount');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('response', trim($response));
      $this->db->where('user_type', (int)$user_type);
      $row = $this->db->get($this->_account_withdraw_request)->row_array();
      return $row['amount'];
    }
    return false;
  }

  public function customer_paid_this_month($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->select('SUM(amount) AS amount, currency_code');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('type', 0);
      $this->db->like('datetime', date('Y-m'));
      $this->db->where('transaction_type', 'payment');
      $this->db->group_by('currency_code');
      return $row = $this->db->get($this->_user_account_history)->result_array();
    }
    return false;
  }

  public function customer_earned_this_month($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->select('SUM(amount) AS amount, currency_code');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('type', 1);
      $this->db->like('datetime', date('Y-m'));
      $this->db->where('transaction_type', 'advance_payment');

      $this->db->or_where('transaction_type', 'pickup_payment');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('type', 1);
      $this->db->like('datetime', date('Y-m'));

      $this->db->or_where('transaction_type', 'deliver_payment');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('type', 1);
      $this->db->like('datetime', date('Y-m'));


      $this->db->group_by('currency_code');
      return $row = $this->db->get($this->_user_account_history)->result_array();
    }
    return false;
  }
  
  public function customer_paid_to_date($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->select('SUM(amount) AS amount, currency_code');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('type', 0);
      $this->db->where('transaction_type', 'payment');
      $this->db->group_by('currency_code');
      return $row = $this->db->get($this->_user_account_history)->result_array();
    }
    return false;
  }

  public function customer_earned_to_date($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->select('SUM(amount) AS amount, currency_code');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('type', 1);
      $this->db->where('transaction_type', 'advance_payment');

      $this->db->or_where('transaction_type', 'pickup_payment');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('type', 1);

      $this->db->or_where('transaction_type', 'deliver_payment');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('type', 1);

      $this->db->group_by('currency_code');
      return $row = $this->db->get($this->_user_account_history)->result_array();
    }
    return false;
  }

  public function get_user_order_invoices($cust_id=0, $last_id=0)
  {
    if($cust_id > 0) {
      $this->db->select('order_id, invoice_url, cust_id, deliverer_id, from_address, to_address, distance_in_km, pickup_datetime, order_price, order_picture_url, delivered_datetime, rating, currency_sign, cust_name, deliverer_name, deliverer_company_name, deliverer_contact_name');

      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('invoice_url !=', 'NULL');
      if($last_id > 0) { $this->db->where('order_id <', $last_id); }

      $this->db->or_where('deliverer_id', (int)$cust_id);
      $this->db->where('invoice_url !=', 'NULL');
      if($last_id > 0) { $this->db->where('order_id <', $last_id); }

      $this->db->limit(10);
      $this->db->order_by('order_id', 'desc');
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }

  public function get_deliverer_comission_invoices($cust_id=0, $last_id=0)
  {
    if($cust_id > 0) {
      $this->db->select('order_id, deliverer_invoice_url, cust_id, deliverer_id, from_address, to_address, distance_in_km, pickup_datetime, order_price, order_picture_url, delivered_datetime, rating, currency_sign, cust_name, deliverer_name, deliverer_company_name, deliverer_contact_name');

      $this->db->where('deliverer_id', (int)$cust_id);
      $this->db->where('deliverer_invoice_url !=', 'NULL');
      if($last_id > 0) { $this->db->where('order_id <', $last_id); }

      $this->db->limit(10);
      $this->db->order_by('order_id', 'desc');
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }
  
  public function check_verified($code=null)
  {
    if($code != null) {
      $this->db->select('email_verified, mobile_verified');
      $this->db->where('activation_hash', $code);
      return $this->db->get($this->_customers_master)->row_array();     
    }
    return false;
  }

  public function verify_email($code=null)
  {
    if($code != null) {
      $this->db->where('activation_hash', $code);
      $this->db->where('email_verified', 0);
      $this->db->update($this->_customers_master, ['email_verified' => 1]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function customer_payment_methods($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->select('iban, mobile_money_acc1, mobile_money_acc2');
      $this->db->where('cust_id', (int) $cust_id);
      return $this->db->get($this->_deliverer)->row_array();
    }
    return false;
  }

  public function get_consumers_transport_details($transport_id=0)
  {
    if($transport_id > 0 ){
      $this->db->where('transport_id', (int) $transport_id);
      return $this->db->get($this->_transport_details)->row_array();
    }
    return false;
  }

  public function get_customer_account_withdraw_request_sum_currencywise($cust_id=0, $response=null, $currency_code=null)
  {
    if($cust_id > 0 && !is_null($response) && !is_null($currency_code)) {
      $this->db->select('SUM(amount) AS amount');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('response', trim($response));
      $this->db->where('currency_code', trim($currency_code));
      return $this->db->get($this->_account_withdraw_request)->row_array();
    }
    return false;
  }

  public function get_driver_order_list($cd_id=0, $last_id=0, $order_status=null)
  {
    if($cd_id > 0 && !is_null($order_status)) {
      $this->db->where('driver_id', (int)($cd_id));
      $this->db->where('driver_status_update', $order_status);
      $this->db->order_by('order_id', 'desc');
      $this->db->limit(10);
      $last_id = (int) $last_id;
      if($last_id > 0) {
        $this->db->where('order_id <', $last_id);
      }
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }
  
  public function get_city_name_by_id($id=0)    {        
    if($id>0){            
      $this->db->where('city_id', (int) $id);            
      $r = $this->db->get($this->_cities)->row_array();            
      return $r['city_name'];        
    }        
    return false;    
  }
  
  public function get_country_name_by_id($id=0){        
    if($id>0){            
      $this->db->where('country_id', (int) $id);            
      $r = $this->db->get($this->_countries)->row_array();            
      return $r['country_name'];        
    }        
    return false;    
  }

  public function get_state_name_by_id($id=0)    {        
    if($id>0){            
      $this->db->where('state_id', (int) $id);            
      $r = $this->db->get($this->_states)->row_array();            
      return $r['state_name'];        
    }        
    return false;    
  } 

  public function get_open_booking_list_front($cat_name=null, $location=null)
  {
    if(!is_null($cat_name) || !is_null($location)) {

      $address_array = explode(', ', $location);
      
      //$address_string = implode("', '", $address_array);
      //$this->db->where("from_address IN ('".$address_string."')");

      $address_string = implode("|", $address_array);
      $this->db->where("from_address REGEXP '".$address_string."'");

      $this->db->where('order_status', 'open');
      $this->db->where('complete_paid', 1);
      //$this->db->like('from_address', $location, 'BOTH');     
      $this->db->order_by('order_id', 'desc');
      $this->db->limit(300);
      if(preg_match('/Courier/',$cat_name)) {
        return $this->db->get($this->_courier_orders)->result_array();
        //$this->db->get($this->_courier_orders)->result_array();
        //var_dump($this->db->last_query()); die();
      }
      if(preg_match('/Transport/',$cat_name)) {
        return $this->db->get($this->_courier_orders)->result_array();
      }
    } else return false;
  }

  public function register_new_support(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_customer_support_queries, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_support_ticket($id=0, $ticket_id=null)
  {
    if($id > 0 && !is_null($ticket_id)){
      $this->db->where('query_id', (int)$id );
      $this->db->update($this->_customer_support_queries, ['ticket_id' => trim($ticket_id)]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_support_list($id=0, $last_id=0)
  {
    if($id > 0) {
      $this->db->where('cust_id', (int)($id));
      $this->db->order_by('query_id', 'desc');
      $this->db->limit(10);
      $last_id = (int) $last_id;
      if($last_id > 0) {
        $this->db->where('query_id<', $last_id);
      }
      return $this->db->get($this->_customer_support_queries)->result_array();
    } return false;
  }

  public function get_carrier_type()
  {
    return $this->db->get($this->_carrier_type)->result_array();
  }

  public function requested_order_count($cust_id=0, $last_login_date_time=null)
  {
    if($cust_id > 0 && !is_null($last_login_date_time)) {
      $this->db->select('COUNT(order_id) as requested_order_count');
      $this->db->where('deliverer_id', (int)($cust_id));
      $this->db->where('cre_datetime >', trim($last_login_date_time));
      $row = $this->db->get($this->_courier_order_deliverer_request)->row_array();
      return $row['requested_order_count'];
    }
    return false;
  }
  public function get_category_details_by_id($cat_id=0)
  {
    if($cat_id > 0){
    $this->db->where('cat_id', (int)$cat_id);
    return $this->db->get($this->_category_master)->row_array();
    }
    return false;
  }

  public function get_customer_email_name_by_order_id($id=0)
  {
    if($id > 0) {
      $this->db->select('email1');
      $this->db->where('cust_id', (int)$id);
      $row = $this->db->get($this->_customers_master)->row_array();
      $email_cut = explode('@', $row['email1']);
      return ucwords($email_cut[0]);
    }
    return 'User';
  }

  public function get_dangerous_goods($id=0)
  {
    if($id > 0 ){
      $this->db->where('id', $id);
      return $this->db->get($this->_dangerous_goods_master)->row_array();
    }
    return false;
  }

  public function register_state($country_id=0, $state_name=null)
  {
    if($country_id > 0 && !is_null($state_name)) {
      $this->db->insert($this->_states, ['state_name' => trim($state_name), 'country_id' => (int)$country_id ]);
      return $this->db->insert_id();
    }
    return false;
  }

  public function register_city($state_id=0, $city_name=null)
  {
    if($state_id > 0 && !is_null($city_name)) {
      $this->db->insert($this->_cities, ['city_name' => trim($city_name), 'state_id' => (int)$state_id ]);
      return $this->db->insert_id();
    }
    return false;
  }
	
	public function convert_big_int($int = 0){
	    if( $int > 1000 && $int < 1000000){ $int = ($int/1000); return number_format($int ,1,'.',',') . 'K'; }         
	    else if( $int >= 1000000 ){ $int = ($int / 1000000 ); return number_format($int ,1,'.',',') . 'M'; }
	    else{ return $int; }
    }

  public function register_user_new_recent_relay(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_customer_recent_relay_point, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_recent_relay_list($id=0)
  {
    if($id > 0 ){
      $this->db->select('f.relay_id, f.relay_status, r.*'); 
      $this->db->from($this->_customer_recent_relay_point.' as f');
      $this->db->join($this->_relay_points.' as r', 'f.relay_id = r.relay_id');
      $this->db->where('r.status', 1);
      $this->db->group_by('f.relay_id');
      $this->db->where('f.cust_id', $id);
      return $this->db->get($this->_customer_recent_relay_point)->result_array();
    }
    return false;
  }

  public function get_relay_point_by_lat_long($frm_relay_latitude=null, $frm_relay_longitude=null, $cust_id=0)
  {
    if($frm_relay_latitude != null && $frm_relay_longitude != null && $cust_id > 0) {
      $this->db->select("*, SQRT(
      POW(69.1 * (latitude - $frm_relay_latitude), 2) +
      POW(69.1 * ($frm_relay_longitude - longitude) * COS(latitude / 57.3), 2)) AS distance FROM tbl_relay_points WHERE relay_id NOT IN (SELECT relay_id from tbl_customer_recent_relay_point WHERE cust_id = $cust_id) ORDER BY distance LIMIT 10");
      return $this->db->get()->result_array();
      //echo $this->db->last_query();
    } else return false;
  }

  public function register_rejected_order_history(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_rejected_orders, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_gonagoo_address($country_id=0)
  {
    if($country_id > 0 ) {
      $this->db->where('country_id', $country_id);
      $row = $this->db->get($this->_gonagoo_address)->row_array();
      if(!isset($row)) {
        $this->db->where('country_id', 75);
        return $this->db->get($this->_gonagoo_address)->row_array();
      } else {
        return $row;
      }
    }
    return false;
  }

  public function update_payment_type_of_order(array $data, $order_id=null)
  {
    if(is_array($data) && $order_id > 0){
      $this->db->where('order_id', (int)$order_id );
      $this->db->update($this->_courier_orders, $data);
      return $this->db->affected_rows();
    }
    return false;
  }
  
  public function get_addresses($country_id=0)
  { 
    $this->db->select('a.*, c.country_name');
    $this->db->from($this->_gonagoo_address. ' as a');
    $this->db->join($this->_countries.' as c', 'c.country_id = a.country_id');
    $this->db->where('a.country_id IN ('.$country_id.', 75)');
    return $this->db->get()->result_array();      
  }
  
  public function get_unit_master_details($unit_id=0)
  {
    if($unit_id>0) {
      $this->db->where('unit_id', $unit_id);
      $details = $this->db->get($this->_unit_master)->row_array();
      return $details['shortname'];
    } else return false;
  } 

  //Start Standard Rates-----------------------------------------
    public function get_standard_rates($id=0, $cust_id=0)
    {   
      // $this->db->select('r.*, c.country_name, u.shortname, cr.currency_sign,cr.currency_title, c.country_name');   
      $this->db->select('r.*, c.country_name, cr.currency_sign,cr.currency_title, c.country_name, cat.cat_name');   
      $this->db->from('tbl_customer_standard_rates as r');
      // $this->db->join('tbl_unit_master as u', 'r.unit_id = u.unit_id');
      $this->db->join('tbl_currency_master as cr', 'cr.currency_id = r.currency_id');
      $this->db->join('tbl_countries as c', 'c.country_id = r.country_id');
      $this->db->join($this->categories.' as cat', 'cat.cat_id = r.category_id');
      $this->db->where('r.cust_id', $cust_id);
      
      if($id > 0) { 
        $this->db->where('rate_id', $id);
        return $this->db->get()->row_array();
      } else {
        $this->db->order_by('rate_id', 'desc');
        return $this->db->get()->result_array();
      }
    } 
    public function get_unit_detail($id=0)
    {
      if($id > 0) {
        $this->db->where('unit_id', (int) $id);
        return $this->db->get($this->units)->row_array();
      }
      return false;
    }
    public function get_countries()
    {
      return $this->db->get($this->countries)->result_array();
    }
    public function get_country_currencies($country_id=0)
    {
      if($country_id > 0 ) {
        $this->db->select('cc.currency_id, cr.currency_sign, cr.currency_title');
        $this->db->from('tbl_country_currency as cc');
        $this->db->join('tbl_currency_master as cr', 'cr.currency_id = cc.currency_id');
        $this->db->where('cc.country_id', (int) $country_id);
        return $this->db->get()->result_array();
      }
      return false;
    }
    public function get_currency_detail($id=0)
    {
      if($id > 0) {
        $this->db->where('currency_id', (int) $id);
        return $this->db->get($this->currency)->row_array();
      }
      return false;
    }
    public function check_min_volume_based_standard_rates(array $data)
    {
      // echo json_encode($data); die();
      if(is_array($data)){
        $min_volume = $data['min_volume'];
        $min_distance = $data['min_distance'];

        $this->db->select('rate_id');
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where("$min_volume BETWEEN min_volume AND max_volume");
        $this->db->where("ROUND($min_distance) BETWEEN min_distance AND max_distance");
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_max_volume_based_standard_rates(array $data)
    {
      // echo json_encode($data); die();
      if(is_array($data)){
        $max_volume = $data['max_volume'];
        $max_distance = $data['max_distance'];

        $this->db->select('rate_id');
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where("$max_volume BETWEEN min_volume AND max_volume");
        $this->db->where("ROUND($max_distance) BETWEEN min_distance AND max_distance");
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
    public function register_standard_rates(array $data)
    {
      if(is_array($data)){      
        $this->db->insert($this->rates, $data);
        return $this->db->insert_id();
      }
      return false;
    }
    public function update_standard_rates(array $data, $id=0)
    {
      if(is_array($data) AND $id > 0){
        $this->db->where('rate_id', (int)$id);
        $this->db->update($this->rates, $data);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function delete_standard_rates($id=0)
    {
      if($id > 0 ){
        $this->db->where('rate_id', (int) $id);
        $this->db->delete($this->rates);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function check_min_weight_based_standard_rates(array $data)
    {
      // echo json_encode($data); die();
      if(is_array($data)){      
        $min_distance = $data['min_distance'];
        $min_weight = $data['min_weight'];
        $unit_id = $data['unit_id'];

        $this->db->select('rate_id');
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('unit_id', (int) $data['unit_id']);
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where("ROUND($min_distance) BETWEEN min_distance AND max_distance");
        $this->db->where("$min_weight BETWEEN min_weight AND max_weight");
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_max_weight_based_standard_rates(array $data)
    {
      // echo json_encode($data); die();
      if(is_array($data)){      
        $max_distance = $data['max_distance'];
        $max_weight = $data['max_weight'];
        $unit_id = $data['unit_id'];

        $this->db->select('rate_id');
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('unit_id', (int) $data['unit_id']);
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where("ROUND($max_distance) BETWEEN min_distance AND max_distance");
        $this->db->where("$max_weight BETWEEN min_weight AND max_weight");
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_formula_volume_based_standard_rates(array $data)
    {
      //echo json_encode($data); die();
      if(is_array($data)){
        $this->db->select('rate_id');
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('is_formula_volume_rate', 1);
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_formula_weight_based_standard_rates(array $data)
    {
      //echo json_encode($data); die();
      if(is_array($data)){
        $this->db->select('rate_id');
        $this->db->where('unit_id', (int) $data['unit_id']);
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('is_formula_weight_rate', 1);
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_formula_min_volume_based_standard_rates(array $data)
    {
      // echo json_encode($data); die();
      if(is_array($data)){
        $min_distance = $data['min_distance'];
        $this->db->select('rate_id');
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('is_formula_volume_rate', 1);
        $this->db->where("ROUND($min_distance) BETWEEN min_distance AND max_distance");
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_formula_max_volume_based_standard_rates(array $data)
    {
      // echo json_encode($data); die();
      if(is_array($data)){
        $max_distance = $data['max_distance'];
        $this->db->select('rate_id');
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('is_formula_volume_rate', 1);
        $this->db->where("ROUND($max_distance) BETWEEN min_distance AND max_distance");
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_formula_min_weight_based_standard_rates(array $data)
    {
      // echo json_encode($data); die();
      if(is_array($data)){      
        $min_distance = $data['min_distance'];
        $unit_id = $data['unit_id'];

        $this->db->select('rate_id');
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('unit_id', (int) $data['unit_id']);
        $this->db->where('is_formula_weight_rate', 1);
        $this->db->where("ROUND($min_distance) BETWEEN min_distance AND max_distance");
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_formula_max_weight_based_standard_rates(array $data)
    {
      // echo json_encode($data); die();
      if(is_array($data)){      
        $max_distance = $data['max_distance'];
        $unit_id = $data['unit_id'];
        $this->db->select('rate_id');
        $this->db->where('country_id', (int) $data['country_id']);
        $this->db->where('category_id', (int) $data['category_id']);
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->where('unit_id', (int) $data['unit_id']);
        $this->db->where('is_formula_weight_rate', 1);
        $this->db->where("ROUND($max_distance) BETWEEN min_distance AND max_distance");
        if($res = $this->db->get($this->rates)->row_array()){ return true; }
      }
      return false;
    }
  //End Standard Rates-----------------------------------------

  //Start Point to Point Rates---------------------------------
    public function get_pp_rates($id=0, $cust_id=0)
    {
      $this->db->select('r.*, cf.country_name as from_country_name, ct.country_name as to_country_name, cr.currency_sign, cr.currency_title,cat.cat_name');
      $this->db->from($this->p2p_rates.' as r');
      $this->db->join($this->currency.' as cr', 'cr.currency_id = r.currency_id');
      $this->db->join($this->countries.' as cf', 'cf.country_id = r.from_country_id');
      $this->db->join($this->countries.' as ct', 'ct.country_id = r.to_country_id');
      $this->db->join($this->categories.' as cat', 'cat.cat_id = r.category_id');
      if($cust_id > 0) { $this->db->where('r.cust_id', $cust_id); }
      
      if($id > 0) { 
        $this->db->where('rate_id', $id);
        return $this->db->get()->row_array();
      } else {
        $this->db->order_by('rate_id', 'desc');
        return $this->db->get()->result_array();
      }
    }
    public function get_state_detail_by_id($id=0)
    {
      if($id > 0 ) {
        $this->db->where('state_id', $id);
        return $this->db->get($this->states)->row_array();
      }
      return false;
    }
    public function get_city_detail_by_id($id=0)
    {
      if($id > 0 ) {
        $this->db->where('city_id', $id);
        return $this->db->get($this->cities)->row_array();
      }
      return false;
    }
    public function get_states($country_id=0)
    {
      if($country_id > 0 ) {
        $this->db->where('country_id', $country_id);
        return $this->db->get($this->states)->result_array();
      }
      return false;
    }
    public function get_cities($state_id=0)
    {
      if($state_id > 0 ) {
        $this->db->where('state_id', $state_id);
        return $this->db->get($this->cities)->result_array();
      }
      return false;
    }
    public function register_p_to_p_rates(array $data)
    {
      if(is_array($data)){      
        $this->db->insert($this->p2p_rates, $data);
        return $this->db->insert_id();
      }
      return false;
    }
    public function check_volume_based_p_to_p_rates(array $data, $flag=null)
    {
      if(is_array($data) && $flag != null && !empty($flag)){
        $category_id = $data['category_id'];
        $from_country_id = $data['from_country_id'];
        $from_state_id = $data['from_state_id'];
        $from_city_id = $data['from_city_id'];
        $to_country_id = $data['to_country_id'];
        $to_state_id = $data['to_state_id'];
        $to_city_id = $data['to_city_id'];
        $cust_id = $data['cust_id'];

        $this->db->select('rate_id');
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('category_id', (int) $category_id);
        $this->db->where('from_country_id', (int) $from_country_id);
        if($from_state_id > 0 ){ $this->db->where('from_state_id', (int) $from_state_id); }
        if($from_city_id > 0 ){ $this->db->where('from_city_id', (int) $from_city_id); }

        $this->db->where('to_country_id', (int) $to_country_id);
        if($to_state_id > 0 ){ $this->db->where('to_state_id', (int) $to_state_id); }
        if($to_city_id > 0 ){ $this->db->where('to_city_id', (int) $to_city_id); }

        if($flag == 'min') { $min_volume = $data['min_volume']; $this->db->where("$min_volume BETWEEN min_volume AND max_volume"); }
        else if($flag == 'max') { $max_volume = $data['max_volume'];  $this->db->where("$max_volume BETWEEN min_volume AND max_volume"); }

        if($res = $this->db->get($this->p2p_rates)->row_array()){ return true; }
      }
      return false;
    }
    public function delete_pp_rates($id=0)
    {
      if($id > 0 ){
        $this->db->where('rate_id', (int) $id);
        $this->db->delete($this->p2p_rates);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function get_dimension_detail_by_id($id=0)
    {
      if($id > 0 ) {
        $this->db->where('dimension_id', $id);
        return $this->db->get($this->standard_dimension)->row_array();
      }
      return false;
    }
    public function update_pp_rates(array $data, $id=0)
    {
      if(is_array($data) AND $id > 0){
        $this->db->where('rate_id', (int)$id);
        $this->db->update($this->p2p_rates, $data);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function check_weight_based_p_to_p_rates(array $data, $flag=null)
    {
      if(is_array($data) && $flag != null && !empty($flag)){
        $category_id = $data['category_id'];
        $from_country_id = $data['from_country_id'];
        $from_state_id = $data['from_state_id'];
        $from_city_id = $data['from_city_id'];
        $to_country_id = $data['to_country_id'];
        $to_state_id = $data['to_state_id'];
        $to_city_id = $data['to_city_id'];
        $unit_id = $data['unit_id'];
        $cust_id = $data['cust_id'];

        $this->db->select('rate_id');
        $this->db->where('is_formula_volume_rate', 0);
        $this->db->where('is_formula_weight_rate', 0);
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('category_id', (int) $category_id);
        $this->db->where('from_country_id', (int) $from_country_id);
        if($from_state_id > 0 ){ $this->db->where('from_state_id', (int) $from_state_id); }
        if($from_city_id > 0 ){ $this->db->where('from_city_id', (int) $from_city_id); }

        $this->db->where('to_country_id', (int) $to_country_id);
        if($to_state_id > 0 ){ $this->db->where('to_state_id', (int) $to_state_id); }
        if($to_city_id > 0 ){ $this->db->where('to_city_id', (int) $to_city_id); }
        
        if($flag == "min"){ $min_weight = $data['min_weight']; $this->db->where("$min_weight BETWEEN min_weight AND max_weight"); }
        else if($flag == "max"){ $max_weight = $data['max_weight']; $this->db->where("$max_weight BETWEEN min_weight AND max_weight"); }

        $this->db->where('unit_id', (int) $unit_id);
        if($res = $this->db->get($this->p2p_rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_formula_volume_based_p_to_p_rates(array $data)
    {
      if(is_array($data)){
        $category_id = $data['category_id'];
        $from_country_id = $data['from_country_id'];
        $from_state_id = $data['from_state_id'];
        $from_city_id = $data['from_city_id'];
        $to_country_id = $data['to_country_id'];
        $to_state_id = $data['to_state_id'];
        $to_city_id = $data['to_city_id'];
        $cust_id = $data['cust_id'];

        $this->db->select('rate_id');
        $this->db->where('is_formula_volume_rate', 1);
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('category_id', (int) $category_id);
        $this->db->where('from_country_id', (int) $from_country_id);
        if($from_state_id > 0 ){ $this->db->where('from_state_id', (int) $from_state_id); }
        if($from_city_id > 0 ){ $this->db->where('from_city_id', (int) $from_city_id); }

        $this->db->where('to_country_id', (int) $to_country_id);
        if($to_state_id > 0 ){ $this->db->where('to_state_id', (int) $to_state_id); }
        if($to_city_id > 0 ){ $this->db->where('to_city_id', (int) $to_city_id); }

        if($res = $this->db->get($this->p2p_rates)->row_array()){ return true; }
      }
      return false;
    }
    public function check_formula_weight_based_p_to_p_rates(array $data)
    {
      if(is_array($data)){
        $category_id = $data['category_id'];
        $from_country_id = $data['from_country_id'];
        $from_state_id = $data['from_state_id'];
        $from_city_id = $data['from_city_id'];
        $to_country_id = $data['to_country_id'];
        $to_state_id = $data['to_state_id'];
        $to_city_id = $data['to_city_id'];
        $unit_id = $data['unit_id'];
        $cust_id = $data['cust_id'];

        $this->db->select('rate_id');
        $this->db->where('is_formula_weight_rate', 1);
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('category_id', (int) $category_id);
        $this->db->where('from_country_id', (int) $from_country_id);
        if($from_state_id > 0 ){ $this->db->where('from_state_id', (int) $from_state_id); }
        if($from_city_id > 0 ){ $this->db->where('from_city_id', (int) $from_city_id); }

        $this->db->where('to_country_id', (int) $to_country_id);
        if($to_state_id > 0 ){ $this->db->where('to_state_id', (int) $to_state_id); }
        if($to_city_id > 0 ){ $this->db->where('to_city_id', (int) $to_city_id); }

        $this->db->where('unit_id', (int) $unit_id);
        if($res = $this->db->get($this->p2p_rates)->row_array()){ return true; }
      }
      return false;
    }
  //End Point to Point Rates-----------------------------------

    public function get_favourite_deliverers_as_customer($order_cust_id=0, $cust_id=0)
    {
      if($order_cust_id > 0 && $cust_id > 0) {
        $this->db->where('cust_id', (int)$order_cust_id);
        $this->db->where('deliverer_id', (int)$cust_id);
        return $this->db->get($this->_favourite_deliverers)->result_array();
      }
      return false;
    }

    public function get_user_gonagoo_commission_details(array $data)
    {
      if(is_array($data)){
        $this->db->where('cust_id', (int)$data['cust_id']);
        $this->db->where('rate_type', (int)$data['rate_type']);
        $this->db->where('category_id', (int)$data['category_id']);
        $this->db->where('country_id', (int)$data['country_id']);
        return $this->db->get($this->_dedicated_customer_gonagoo_commission)->row_array();
      }
      return false;
    }

    public function courier_order_cleanup($id=0)
    {
      $this->db->empty_table('tbl_courier_orders');
      $this->db->empty_table('tbl_order_packages');
      $this->db->empty_table('tbl_courier_order_deliverer_request');
      $this->db->empty_table('tbl_courier_order_review');
      $this->db->empty_table('tbl_courier_order_status');
      $this->db->empty_table('tbl_courier_workroom');
      $this->db->empty_table('tbl_gonagoo_account_history');
      $this->db->empty_table('tbl_gonagoo_account_master');
      $this->db->empty_table('tbl_user_account_history');
      $this->db->empty_table('tbl_user_account_master');
      $this->db->empty_table('tbl_deliverer_scrow_history');
      $this->db->empty_table('tbl_deliverer_scrow_master');
      $this->db->empty_table('tbl_driver_chats');
      $this->db->empty_table('tbl_read_chat_notification');
      $this->db->empty_table('tbl_read_notifications');
      $this->db->empty_table('tbl_read_workroom_notification');
      $this->db->empty_table('tbl_rejected_orders');
      $this->db->empty_table('tbl_relay_account_history');
      $this->db->empty_table('tbl_relay_account_master');
      $this->db->empty_table('tbl_relay_warehouse_details');
      return true;
    }

    public function get_user_pending_invoices_history($cust_id=0, $last_id=0)
    {
      if($cust_id > 0) {
        $this->db->where('deliverer_id', (int)$cust_id);
        if($last_id > 0) { $this->db->where('order_id <', $last_id); $this->db->limit(10); }
        $this->db->where('order_status', 'delivered');
        $this->db->where('complete_paid', 0);
        $this->db->where('payment_mode', 'bank');
        $this->db->order_by('order_id', 'desc');
        return $this->db->get($this->_courier_orders)->result_array();
      }
      return false;
    }

    public function create_global_workroom(array $data)
    {
      if(!empty($data)){
        $this->db->insert($this->_global_workroom, $data);
        return $this->db->insert_id();
      }
      return false;
    }

    public function get_max_duration(array $data, $distance_in_km=0) 
    {
      //echo json_encode($data); die();
      // Point to Point / Standard - Weight Based Price
      $p_to_p_weight_based = array(
        "category_id" => $data['category_id'], //7
        "total_weight" => $data['total_weight'], 
        "unit_id" => $data['unit_id'], //1
        "from_country_id" => $data['from_country_id'], 
        "from_state_id" => $data['from_state_id'], 
        "from_city_id" => $data['from_city_id'], 
        "to_country_id" => $data['to_country_id'], 
        "to_state_id" => $data['to_state_id'], 
        "to_city_id" => $data['to_city_id'], 
        "service_area_type" => $data['service_area_type'], //local
        "transport_type" => $data['transport_type'], // earth
      );
      if(!$weight_based_price_data = $this->calculate_weight_based_p_to_p_price($p_to_p_weight_based)) {
        $weight_based = array(
          "category_id" => $data['category_id'],
          "total_weight" => $data['total_weight'],
          "unit_id" => $data['unit_id'],
          "country_id" => $data['from_country_id'],
          "state_id" => $data['from_state_id'],
          "city_id" => $data['from_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
        );
        $weight_based_price_data = $this->calculate_weight_based_price($weight_based);      
      }
      // Point to Point / Standard - Formula Weight Based Price
      $p_to_p_formula_weight_based = array(
        "category_id" => $data['category_id'],
        "total_weight" => $data['total_weight'],
        "unit_id" => $data['unit_id'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
      );
      if(!$formula_weight_based_price_data = $this->calculate_formula_weight_based_p_to_p_price($p_to_p_formula_weight_based)) {
        $weight_based = array(
          "category_id" => $data['category_id'],
          "total_weight" => $data['total_weight'],
          "unit_id" => $data['unit_id'],
          "country_id" => $data['from_country_id'],
          "state_id" => $data['from_state_id'],
          "city_id" => $data['from_city_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
        );
        $formula_weight_based_price_data = $this->calculate_formula_weight_based_price($weight_based);      
      }

      // Point to Point / Standard - Volume Based Price
      $p_to_p_volume_based = array(
        "category_id" => $data['category_id'],
        "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
      );
      if(!$volume_based_price_data = $this->calculate_volume_based_p_to_p_price($p_to_p_volume_based) ){
        $volume_based = array(
          "category_id" => $data['category_id'],
          "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
          "country_id" => $data['from_country_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
        );
        $volume_based_price_data = $this->calculate_volume_based_price($volume_based);
      }
      // Point to Point / Standard - Formula Volume Based Price
      $p_to_p_formula_volume_based = array(
        "category_id" => $data['category_id'],
        "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
        "from_country_id" => $data['from_country_id'],
        "from_state_id" => $data['from_state_id'],
        "from_city_id" => $data['from_city_id'],
        "to_country_id" => $data['to_country_id'],
        "to_state_id" => $data['to_state_id'],
        "to_city_id" => $data['to_city_id'],
        "service_area_type" => $data['service_area_type'],
        "transport_type" => $data['transport_type'],
      );
      if(!$formula_volume_based_price_data = $this->calculate_formula_volume_based_p_to_p_price($p_to_p_formula_volume_based) ){
        $volume_based = array(
          "category_id" => $data['category_id'],
          "volume" => ($data['width'] * $data['height'] * $data['length']) * $data['total_quantity'],
          "country_id" => $data['from_country_id'],
          "service_area_type" => $data['service_area_type'],
          "transport_type" => $data['transport_type'],
          "distance_in_km" => (string) $distance_in_km,
        );
        $formula_volume_based_price_data = $this->calculate_formula_volume_based_price($volume_based);
      }

      if (is_array($weight_based_price_data) || is_array($volume_based_price_data) || is_array($formula_weight_based_price_data) || is_array($formula_volume_based_price_data)) {
        if($formula_weight_based_price_data!=null && $formula_volume_based_price_data!=null) {
          if ( (($data['total_weight'] * $formula_weight_based_price_data['service_rate']) * (int) $distance_in_km) <= (((($data['width'] * $data['height'] * $data['length']) * $data['total_quantity']) * $formula_volume_based_price_data['service_rate']) * (int) $distance_in_km) ) { 
            $max_duration = $formula_volume_based_price_data['max_duration'];
          } else { 
            $max_duration = $formula_weight_based_price_data['max_duration'];
          }
        } else if($formula_weight_based_price_data!=null) {
          $max_duration = $formula_weight_based_price_data['max_duration'];
        } else if($formula_volume_based_price_data!=null){
          $max_duration = $formula_volume_based_price_data['max_duration'];
        } else {
          if($weight_based_price_data!=null && $volume_based_price_data!=null) {
            if ($weight_based_price_data['service_rate'] <= $volume_based_price_data['service_rate']){ 
              $max_duration = $volume_based_price_data['max_duration'];
            } else { 
              $max_duration = $weight_based_price_data['max_duration'];
            }
          } else if($weight_based_price_data!=null) {
            $max_duration = $weight_based_price_data['max_duration'];
          } else if($volume_based_price_data!=null) {
            $max_duration = $volume_based_price_data['max_duration'];
          } else { 
            $max_duration = 0;
          }
        } 
      } else { $max_duration = 0; }
      
      return array(
        "max_duration" => $max_duration,    
      );
    }

    public function laundry_booking_details($booking_id=0)
    {
      if($booking_id > 0) {
        $this->db->where('booking_id', (int) $booking_id);
        return $this->db->get($this->_laundry_booking)->row_array();
      } return false;
    }
    public function laundry_booking_update($data=0, $booking_id=0)
    {
      if(is_array($data) && $booking_id > 0){
        $this->db->where('booking_id', (int) $booking_id);
        $this->db->update($this->_laundry_booking, $data);
        return $this->db->affected_rows();
      } return false;
    }


    public function get_laundry_workorder_list($cust_id=0, $order_id=0)
    {
      if($cust_id > 0 ){
        if($order_id > 0) { $this->db->where('booking_id', $order_id); }
        $this->db->where("booking_status !='cancelled'");
        $this->db->where('cust_id', $cust_id);
        $this->db->or_where('provider_id', $cust_id);
        $this->db->order_by('status_update_datetime', 'desc');
        if($order_id > 0) {
          return $this->db->get($this->_laundry_booking)->row_array();
        } else {
          return $this->db->get($this->_laundry_booking)->result_array();
        }
      }
      return false;
    }

    public function laundry_booking_details_list($booking_id=0)
    {
      if($booking_id > 0) {
        $this->db->select('*');
        $this->db->from('tbl_laundry_booking_details dtls'); 
        $this->db->join('tbl_laundry_category cat', 'cat.cat_id=dtls.cat_id', 'left');
        $this->db->join('tbl_laundry_sub_category sub', 'sub.sub_cat_id=dtls.sub_cat_id', 'left');
        $this->db->where('dtls.booking_id', (int) $booking_id);
        return $this->db->get()->result_array();
      } return false;
    }

    public function register_career_enquiry(array $data)
    {
      if(is_array($data)) {
        $this->db->insert($this->_career_enquiry, $data);
        return $this->db->insert_id();
      } return false;
    }

  //Promo used for courier and transport-------------------
    public function get_promocodes($service_id = 0)
    {
      //if($service_id != 0){$this->db->where('service_id',$service_id);}
      if($service_id != 0){$this->db->where("FIND_IN_SET('$service_id',service_id) !=", 0);}
      $this->db->where('is_delete', 0);
      $this->db->where('code_status', 1);
      $this->db->where('code_expire_date >=' , date('m-d-Y'));
      return $this->db->get($this->_promo_code)->result_array();
    }
    public function get_promocode_by_name($promo_code = null)
    {
      if($promo_code != null){
      $this->db->where('promo_code',$promo_code);
      $this->db->where('code_status', 1);
      $this->db->where('is_delete', 0);
      return $this->db->get($this->_promo_code)->row_array();
      } return false;
    }
    public function check_used_promo_code($promo_id = 0 , $promo_version = 0 , $cust_id = 0 , $cust_email = null)
    {
      if($promo_id>0 && $promo_version>0){
      $this->db->where('promo_code_id',$promo_id);
      $this->db->where('promo_code_version',$promo_version);
      if($cust_id > 0){ $this->db->where('cust_id' , $cust_id); }
      if($cust_email != null){ $this->db->where('customer_email' , $cust_email); }
      return $this->db->get($this->_promocode_used)->row_array();
      }
        return false;
    }
    
    public function get_used_promo_code($promo_id = 0 , $promo_version = 0)
    {
      if($promo_id>0 && $promo_version>0){
      $this->db->where('promo_code_id',$promo_id);
      $this->db->where('promo_code_version',$promo_version);
      return $this->db->get($this->_promocode_used)->result_array();
      }
        return false;
    }
    
    public function register_user_used_prmocode(array $update)
    {
      if(is_array($update)) {
      $this->db->insert($this->_promocode_used, $update);
      return $this->db->insert_id();
      }
      return false;
    }
  //Promo used for courier and transport-------------------
    
    
    public function get_address_by_id($from_id=0, $address_type=null, $cust_id=0)
  { 
    if($from_id > 0 && $cust_id > 0 && !is_null($address_type) && $address_type == 'address') {
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('addr_id', (int)$from_id);
      return $this->db->get($this->_addressbook)->row_array();
    } return false;
  }
  
  public function get_currency_by_country_relay_id($from_id=0, $address_type=null, $cust_id=0)
  { 
    if($from_id > 0 && $cust_id > 0 && !is_null($address_type) && $address_type == 'address') {
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('addr_id', (int)$from_id);
      return $this->db->get($this->_addressbook)->row_array();
    } else {
      $this->db->where('relay_id', (int)$from_id);
      return $this->db->get($this->_relay_points)->row_array();
    }
  }*/
    
}

/* End of file M4_model.php */
/* Location: ./application/models/M4_model.php */