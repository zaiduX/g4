<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_dispute_model extends CI_Model {
	private $_smp_job_dispute = "tbl_smp_job_dispute";

	public function get_job_dispute() {
		$this->db->where('dispute_status', "open");
		$this->db->order_by('dispute_id', 'desc');
		return $this->db->get($this->_smp_job_dispute)->result_array();
	}
	
	// public function get_sub_categories() {
	// 	$this->db->select('c.dispute_cat_title, subc.*');
	// 	$this->db->from($this->_smp_dispute_category_master.' as c');
	// 	$this->db->join($this->_smp_dispute_sub_category_master.' as subc', 'subc.dispute_cat_id = c.dispute_cat_id');
	// 	$this->db->group_by('subc.dispute_sub_cat_id');
	// 	$this->db->order_by('subc.dispute_sub_cat_id', 'asc');
	// 	return $this->db->get($this->_smp_dispute_sub_category_master)->result_array();
	// }
	// public function register_sub_cateogry(array $data) {
	// 	if(is_array($data)){			
	// 		$this->db->insert($this->_smp_dispute_sub_category_master, $data);
	// 		return $this->db->insert_id();
	// 	} return false;
	// }
	// public function update_sub_cateogry(array $data, $dispute_sub_cat_id=0) {
	// 	if(is_array($data) AND $dispute_sub_cat_id > 0){
	// 		$this->db->where('dispute_sub_cat_id', (int)$dispute_sub_cat_id);
	// 		$this->db->update($this->_smp_dispute_sub_category_master, $data);
	// 		return $this->db->affected_rows();
	// 	} return false;
	// }
}

/* End of file Dispute_sub_category_master_model.php */
/* Location: ./application/models/Dispute_sub_category_master_model.php */