<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advance_payment_services_model extends CI_Model {

	private $_smp_advance_payment = "tbl_smp_advance_payment";
	private $_countries = "tbl_countries"; 
	private $_category_master = "tbl_category_master";
	private $_category_type_master = "tbl_category_type_master";
	
	public function get_payments() {
		$this->db->select('c.cat_name, ct.cat_type, country.country_name, pay.*');
		$this->db->from($this->_category_master.' as c');
		$this->db->from($this->_category_type_master.' as ct');
		$this->db->from($this->_countries.' as country');
		$this->db->join($this->_smp_advance_payment.' as pay', 'pay.cat_id = c.cat_id AND pay.cat_type_id = ct.cat_type_id AND pay.country_id = country.country_id');
		$this->db->group_by('pay.smp_ap_id');
		$this->db->order_by('pay.smp_ap_id', 'asc');
		return $this->db->get($this->_smp_advance_payment)->result_array();
	}
	public function get_countries() {
		return $this->db->get($this->_countries)->result_array();
	}
	public function get_country_currencies($country_id=0) {
      if($country_id > 0 ) {
        $this->db->select('cc.currency_id, cr.currency_sign, cr.currency_title');
        $this->db->from('tbl_country_currency as cc');
        $this->db->join('tbl_currency_master as cr', 'cr.currency_id = cc.currency_id');
        $this->db->where('cc.country_id', (int) $country_id);
        return $this->db->get()->result_array();
      } return false;
    }
    public function get_category_type() {
		$this->db->where('cat_type_status', 1);
		$this->db->where('cat_type_id IN (5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,25)');
		return $this->db->get($this->_category_type_master)->result_array();
	}
	public function get_categories_by_type($type_id=0) {
		if($type_id > 0){
			$this->db->where('cat_type_id', (int)$type_id);
			return $this->db->get($this->_category_master)->result_array();
		}
	}
	public function check_payment(array $data) {
		if(is_array($data)) {
			$this->db->select('smp_ap_id');
			$this->db->where('country_id', trim($data['country_id']));
			$this->db->where('cat_type_id', trim($data['cat_type_id']));
			$this->db->where('cat_id', trim($data['cat_id']));
			if($res = $this->db->get($this->_smp_advance_payment)->row_array()) { return $res['smp_ap_id']; }
		} return false;
	}
	public function register_payment(array $data) {
		if(is_array($data)){			
			$this->db->insert($this->_smp_advance_payment, $data);
			return $this->db->insert_id();
		} return false;
	}
	public function get_payment_detail($smp_ap_id=0) {
		if($smp_ap_id > 0) {
			$this->db->select('c.cat_name, ct.cat_type, country.country_name, pay.*');
			$this->db->from($this->_category_master.' as c');
			$this->db->from($this->_category_type_master.' as ct');
			$this->db->from($this->_countries.' as country');
			$this->db->join($this->_smp_advance_payment.' as pay', 'pay.cat_id = c.cat_id AND pay.cat_type_id = ct.cat_type_id AND pay.country_id = country.country_id AND pay.smp_ap_id = '.$smp_ap_id);
			return $this->db->get()->row_array();
		} return false;
	}
	public function update_payment(array $data, $smp_ap_id=0) {
		if(is_array($data) AND $smp_ap_id > 0){
			$this->db->where('smp_ap_id', (int)$smp_ap_id);
			$this->db->update($this->_smp_advance_payment, $data);
			return $this->db->affected_rows();
		} return false;
	}
	public function delete_payment($smp_ap_id=0) {
		if($smp_ap_id > 0 ) {
			$this->db->where('smp_ap_id', (int) $smp_ap_id);
			$this->db->delete($this->_smp_advance_payment);
			return $this->db->affected_rows();
		} return false;
	}
}

/* End of file Advance_payment_services_model.php */
/* Location: ./application/models/Advance_payment_services_model.php */