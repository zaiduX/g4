<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_followups_sales_model extends CI_Model {

	private $customers_master = "tbl_customers_master";
	private $authorities_master = "tbl_authorities_master";
    
	public function total_accounts($followup_status=0, $is_deliverer=null)
	{
	    if ($followup_status > 0 ) { $this->db->where('followup_status',$followup_status); }
	    if (!is_null($is_deliverer)) { $this->db->where('is_deliverer',$is_deliverer); }
$this->db->where('followup_taken_authority_id',$this->session->userdata('userid'));
	    return $this->db->get($this->customers_master)->num_rows();
	}
    
	public function get_users($id=0)
	{
	    if($id > 0 ) { $this->db->where('followup_taken_authority_id', (int)$id);
	    $this->db->where('followup_status', 2);
	    return $this->db->get($this->customers_master)->result_array(); } 
	    return false;
	}

	public function post_followup_remark($cust_id=0, $followup_remark=null, $followup_status=0)
	{
		if( $cust_id > 0 && $followup_status > 0 && !is_null($followup_remark)){
			$this->db->where('cust_id', (int)$cust_id);
			$this->db->update($this->customers_master, ['followup_status' => (int)$followup_status, 'followup_remark' => trim($followup_remark), 'followup_taken_datetime' => date('Y-m-d h:i:s') ]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_follow_up_taken_users($id=0)
	{
	    if($id > 0 ) { $this->db->where('followup_taken_authority_id', (int)$id);
		$this->db->where('followup_status IN (3,4)');
		return $this->db->get($this->customers_master)->result_array();
	    }
	    return false;
	}
	
	public function convert_big_int($int = 0){
	    if( $int > 1000 && $int < 1000000){ $int = ($int/1000); return number_format($int ,1,'.',',') . 'K'; }         
	    else if( $int >= 1000000 ){ $int = ($int / 1000000 ); return number_format($int ,1,'.',',') . 'M'; }
	    else{ return $int; }
    }

}

/* End of file Users_followups_sales_model.php */
/* Location: ./application/models/Users_followups_sales_model.php */