<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_dangerous_goods_model extends CI_Model {

  private $countries = "tbl_countries"; 
  private $country_dangerous_goods = "tbl_country_dangerous_goods"; 

  public function get_country_dangerous_goods($id=0)
  {
    $this->db->select('cg.*, c.country_name');
    $this->db->from($this->country_dangerous_goods.' as cg');
    $this->db->join($this->countries.' as c', 'c.country_id = cg.country_id');
    if($id > 0 ){  
      $this->db->where('id', $id);
      return $this->db->get()->row_array();  
    }
    else{ return $this->db->get()->result_array(); }
  }

  public function check_country_existance($id=0) 
  {
    if($id>0){
      $this->db->where('country_id', $id);
      return $this->db->get($this->country_dangerous_goods)->row_array();
    }
    return false;
  }

  public function register(array $data)
  {
    if(!empty($data) && is_array($data)){
      $this->db->insert($this->country_dangerous_goods, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update(array $data,$id=0)
  {
    if(!empty($data) && is_array($data) && $id > 0 ){
      $this->db->where('id', $id);
      $this->db->update($this->country_dangerous_goods, $data);
      return $this->db->affected_rows();
    }
    return false;
  }




}

/* End of file country_dangerous_goods_model.php */
/* Location: ./application/models/country_dangerous_goods_model.php */