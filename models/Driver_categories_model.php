<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver_categories_model extends CI_Model {

	private $category = "tbl_permitted_category"; // db table declaration
	
	public function get_categories()
	{
		$this->db->order_by('pc_id', 'desc');
		return $this->db->get($this->category)->result_array();
	}	

	public function check_category(array $data)
	{
		if(is_array($data)){
			$this->db->select('pc_id');
			$this->db->where('short_name', trim($data['short_name']));
			$this->db->where('full_name', trim($data['full_name']));
			if($res = $this->db->get($this->category)->row_array()){ return $res['pc_id']; }
		}
		return false;
	}

	public function register_category(array $data)
	{
		if(is_array($data)){			
			$this->db->insert($this->category, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_category_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('pc_id', (int) $id);
			return $this->db->get($this->category)->row_array();
		}
		return false;
	}

	public function update_category(array $data, $id=0)
	{
		if(is_array($data) AND $id > 0){
			$this->db->where('pc_id', (int)$id);
			$this->db->update($this->category, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function activate_category($id=0)
	{
		if($id > 0){
			$this->db->where('pc_id', (int) $id);
			$this->db->where('pc_status', 0);
			$this->db->update($this->category, ['pc_status' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_category($id=0)
	{
		if($id > 0){
			$this->db->where('pc_id', (int) $id);
			$this->db->where('pc_status', 1);
			$this->db->update($this->category, ['pc_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file Driver_categories_model.php */
/* Location: ./application/models/Driver_categories_model.php */