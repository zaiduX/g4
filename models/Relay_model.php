<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relay_model extends CI_Model {

  private $_customer_drivers = "tbl_customer_drivers";
  private $_relay_points = "tbl_relay_points";
  private $_relay_points_warehouse = "tbl_relay_points_warehouse";
  private $_countries = "tbl_countries";
  private $_states = "tbl_states";
  private $_cities = "tbl_cities";
  private $_courier_orders = "tbl_courier_orders";


  public function get_relay_points__old()
  {
    $this->db->where('cust_id', 0);
    return $this->db->get($this->_customer_drivers)->result_array();
  }

  public function get_relay_points($admin_country_id=0)
  {
    $this->db->select('r.*, rm.cd_id, rm.first_name,rm.last_name,rm.status as manager_status');
    $this->db->from($this->_relay_points.' as r');
    $this->db->join($this->_customer_drivers.' as rm', 'r.relay_mgr_id = rm.cd_id');
    $this->db->where('rm.cust_id', 0);
    if($admin_country_id > 0 ) { $this->db->where('r.country_id', $admin_country_id); }
    return $this->db->get()->result_array();
  }

  public function total_orders_at_relay($r_id=0)
  {
    if($r_id > 0 ){
      $this->db->where('from_relay_id', $r_id);
      $this->db->or_where('to_relay_id', $r_id);
      return $this->db->get($this->_courier_orders)->num_rows();
    }
    return false;
  }

  public function activate_relay_point($id=0)
  {
    if($id > 0){
      $this->db->where('cd_id', (int) $id);
      $this->db->update($this->_customer_drivers, ['status' => 1]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function inactivate_relay_point($id=0)
  {
    if($id > 0){
      $this->db->where('cd_id', (int) $id);
      $this->db->update($this->_customer_drivers, ['status' => 0]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_countries()
  {
    return $this->db->get($this->_countries)->result_array();
  }

  public function get_country_name_by_id($id=0)
  {
      if($id>0){
          $this->db->where('country_id', (int) $id);
          $r = $this->db->get($this->_countries)->row_array();
          return $r['country_name'];
      }
      return false;
  }

  public function get_city_name_by_id($id=0)
  {
      if($id>0){
          $this->db->where('city_id', (int) $id);
          $r = $this->db->get($this->_cities)->row_array();
          return $r['city_name'];
      }
      return false;
  }

  public function get_state_name_by_id($id=0)
  {
      if($id>0){
          $this->db->where('state_id', (int) $id);
          $r = $this->db->get($this->_states)->row_array();
          return $r['state_name'];
      }
      return false;
  }

  public function get_state_by_country_id($id=0)
  {
      if( $id> 0 ) {
          $this->db->where('country_id', (int) $id);
          return $this->db->get($this->_states)->result_array();
      }
      return false;
  }

  public function get_city_by_state_id($id=0)
  {
      if( $id> 0 ) {
          $this->db->where('state_id', (int) $id);
          return $this->db->get($this->_cities)->result_array();
      }
      return false;
  }

  public function relay_email_exists($email=null)
  {
    if($email != null){
      $this->db->where('email', $email);
      $row = $this->db->get($this->_customer_drivers)->num_rows();
      if($row > 0) { return true; } else { return false; }
    }
  }

  public function register_relay_manager(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_customer_drivers, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function register_relay_point(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_relay_points, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function register_relay_warehouse(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_relay_points_warehouse, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_manager_details($id=0)
  {
    if($id > 0) {
      $this->db->where('cd_id', $id);
      return $this->db->get($this->_customer_drivers)->row_array();
    }
    return false;
  }

  public function get_relay_point_details($cd_id=0)
  {
    if($cd_id > 0 ) {
      $this->db->where('relay_mgr_id', (int)($cd_id));
      return $this->db->get($this->_relay_points)->row_array();
    }
    return false;
  }

  public function update_manager($cd_id=0, array $data)
  {
    if(is_array($data) && $cd_id > 0 ){     
      $this->db->where('cd_id', (int)$cd_id);
      $this->db->update($this->_customer_drivers, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_relay_point_details($cd_id=0, array $data)
  {
    if(is_array($data) && $cd_id > 0 ){     
      $this->db->where('relay_mgr_id', (int)$cd_id);
      $this->db->update($this->_relay_points, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function sendEmail($user_name, $user_email, $subject=null, $message=null) 
  {

    $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";   
    $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";    
    $messageBody .="</head><body bgcolor='#FFFFFF'>";
    $messageBody .="<table class='body-wrap'><tr><td></td><td class='container'><div class='content'>";
    $messageBody .="<table><tr><td align='center'><h3>".$subject."</h3></td></tr>";
    $messageBody .="<tr><td align='center'><h3>Dear <small>".$user_name."</small>,</h3>";

    $messageBody .= $message;

    $messageBody .="</td></tr></table></div></td><tr/></table>";
    //Email Signature
    $messageBody .="<table class='head-wrap'>";
    $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
    $messageBody .="<tr><td>Your Gonagoo team !</td></tr>";
    $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />Deliver, ship cheaper easily</td></tr>";
    $messageBody .="<tr><td>Support : support@gonagoo.com</td></tr>";
    $messageBody .="<tr><td>Website : www.gonagoo.com</td></tr>";
    //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
    $messageBody .="<tr><td>Join us on : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='G+'></a>";
    $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='Twitter'></a>";
    $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='LinkedIn'></a>";
    $messageBody .="<a href='https://www.facebook.com/gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/facebook.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='FB'></a></td></tr>";
    //Download the App <icon for iOS App download><icon for Android App download>
    $messageBody .="<tr><td>Download the App : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='iOS'></a>";
    $messageBody .="<a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='Android'></a></td></tr>";
    $messageBody .="</table></div></td><td></td></tr></table>";
    //Email Signature End
    $messageBody .="</body></html>";

    $email_from = $this->config->item('from_email');
    $email_subject = $subject;
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: Gonagoo <'.$email_from.'>' . "\r\n";
    mail($user_email, $email_subject, $messageBody, $headers);  
    return 1;
  }

  public function sendSMS_old($user_mobile, $message=null)
  {
    if($message!=null) {
        $ch = curl_init("http://103.16.101.52:8080/sendsms/bulksms?"); // url to send sms
        curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
        curl_setopt($ch, CURLOPT_POST, 1); // method to call url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
        curl_setopt($ch, CURLOPT_POSTFIELDS,"username=kotp-noorisys&password=kaptrans&type=0&dlr=1&destination=$user_mobile&source=EZAMNA&message=$message"); 
        $outputSMS = curl_exec($ch); // execute url and save response
        curl_close($ch); // close url connection
        return 1;
    }
  }

  public function sendSMS($user_mobile=null, $message=null)
  {
    if($message!=null && $user_mobile!=null) {
      $message .= ' - Your Team Gonagoo!';
      $uri = 'https://api.twilio.com/2010-04-01/Accounts/ACcd68595e7c0ea2401b0da3764c0e8aaf/SMS/Messages';
      $auth = 'ACcd68595e7c0ea2401b0da3764c0e8aaf:30f962c9cf51541b3ef14822bdcf1d63';
      $fields = 
          '&To=' .  urlencode( '+'.$user_mobile ) . 
          '&From=' . urlencode( '+33757905310' ) . 
          '&Body=' . urlencode( $message );
      $res = curl_init();
      curl_setopt( $res, CURLOPT_URL, $uri );
      curl_setopt( $res, CURLOPT_POST, 3 ); // number of fields
      curl_setopt( $res, CURLOPT_POSTFIELDS, $fields );
      curl_setopt( $res, CURLOPT_USERPWD, $auth ); // authenticate
      curl_setopt( $res, CURLOPT_RETURNTRANSFER, true ); // don't echo
      $result = curl_exec( $res );
    //var_dump($result); die();
      return true;
    }
  }
    
}

/* End of file Relay_model.php */
/* Location: ./application/models/Relay_model.php */