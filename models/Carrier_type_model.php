<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrier_type_model extends CI_Model {
	
	private $_carrier_type = "tbl_carrier_type";

	public function get_carrier_type_list()
	{
		return $this->db->get($this->_carrier_type)->result_array();
	}

	public function get_carrier_type_details($carrier_id=0)
	{
		$this->db->where('carrier_id', (int) $carrier_id);
		return $this->db->get($this->_carrier_type)->row_array();
	}

	public function delete_carrier_type($carrier_id=0)
	{
		if($carrier_id > 0){
			$this->db->where('carrier_id', (int) $carrier_id);
			$this->db->delete($this->_carrier_type);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function check_carrier_type_existance($carrier_title=null)
	{
		if(!is_null($carrier_title)) { 
			$this->db->where('carrier_title', (strtolower($carrier_title)));
			return $this->db->get($this->_carrier_type)->row_array();
		}
		return false;
	}

	public function add_carrier_type_details($carrier_title=null, $carrier_desc=null)
	{
		if(!is_null($carrier_title)){
			$today = date('Y-m-d H:i:s');
			$insert_data = array(
				'carrier_title' => $carrier_title,
				'carrier_desc' => $carrier_desc, 
				'cre_datetime' => $today,
			);
			$this->db->insert($this->_carrier_type, $insert_data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function update_carrier_type_details($carrier_id=0, array $data)
	{
		if(is_array($data) && $carrier_id > 0 ){			
			$this->db->where('carrier_id', (int)$carrier_id);
			$this->db->update($this->_carrier_type, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file Carrier_type_model.php */
/* Location: ./application/models/Carrier_type_model.php */