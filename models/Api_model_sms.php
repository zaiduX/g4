<?php
defined('BASEPATH') OR exit('No direct script access allowed.');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Api_model_sms extends CI_Model {

  private $_sms_url = 'https://eu21.chat-api.com/instance17374/sendMessage?token=csaz6siepcze3eyl';
  private $_file_url = 'https://eu21.chat-api.com/instance17374/sendfile?token=csaz6siepcze3eyl';
    
  public function send_pdf_whatsapp($phone_number="NULL",$sms_body="NULL" , $filename="NULL") {
    if($phone_number != "NULL" && $sms_body != "NULL" && $filename != "NULL") {
      $sms_body .= ' - '.$this->lang->line('Powered by Gonagoo');
      $jason_data = json_encode(array(
        'phone' => $phone_number,
        "body" => $sms_body,
        "filename" => $filename,
      ));
      //$url = 'https://eu22.chat-api.com/instance17971/sendfile?token=vv16hxv7z6cmhp3v';
      $url = $this->_file_url;
      $options = stream_context_create(['http' => [
        'method' => 'POST',
        'header' => 'Content-type: application/json',
        'content' => $jason_data]
      ]);
      //$result = file_get_contents($url, false, $options);
    } return true;
  } 
  public function send_sms_whatsapp($phone_number="NULL",$sms_body="NULL") {
    if($phone_number != "NULL" && $sms_body != "NULL") {
      $sms_body .= ' - '.$this->lang->line('Powered by Gonagoo');
      $jason_data = json_encode(array(
        'phone' => $phone_number,
        "body" => $sms_body,
      ));
      $url = $this->_sms_url;
      $options = stream_context_create(['http' => [
        'method' => 'POST',
        'header' => 'Content-type: application/json',
        'content' => $jason_data]
      ]);
      //$result = file_get_contents($url, false, $options);
    } return true;
  }
  public function send_email($emailMessage="NULL", $gonagooAddress="NULL", $emailSubject="NULL", $emailAddress="NULL", $fileAddress="NULL", $fileName="NULL") {
    $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
    $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
    $messageBody .="</head><body bgcolor='#FFFFFF'>";
    $messageBody .= "<h4>" . $emailMessage . "</h4><br /><br /><br />";
    //Email Signature
    $messageBody .="<table class='head-wrap'>";
    $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
    $messageBody .="<tr><td>".$this->lang->line('Your Gonagoo team!')."</td></tr>";
    $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />".$this->lang->line('slider_heading1')."</td></tr>";
    $messageBody .="<tr><td>".$this->lang->line('Support')." : support@gonagoo.com</td></tr>";
    $messageBody .="<tr><td>".$this->lang->line('Website')." : www.gonagoo.com</td></tr>";
    //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
    $messageBody .="<tr><td>".$this->lang->line('Join us on')." : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='G+'></a>";
    $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='Twitter'></a>";
    $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='LinkedIn'></a>";
    $messageBody .="<a href='https://www.facebook.com/gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/facebook.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='FB'></a></td></tr>";
    //Download the App <icon for iOS App download><icon for Android App download>
    $messageBody .="<tr><td>".$this->lang->line('Download the App')." : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='iOS'></a>";
    $messageBody .="<a href='".$this->config->item('main_app_link')."' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='Android'></a></td></tr>";
    $messageBody .="</table></div></td><td></td></tr></table>";
    //Email Signature End
    $messageBody .="</body></html>";

    require_once('resources/PHPMailer-master/src/Exception.php');
    require_once('resources/PHPMailer-master/src/PHPMailer.php');
    $email = new PHPMailer();
    $email->SetFrom($this->config->item('from_email'), $gonagooAddress);
    $email->isHTML(true);
    $email->Subject   = $emailSubject;
    $email->Body      = $messageBody;
    $email->CharSet  = 'UTF-8';
    $email->AddAddress(trim($emailAddress));
    if($fileAddress != '' && $fileName != '' && $fileName != 'NULL') { $email->AddAttachment( $fileAddress , $fileName ); }
    //$email->Send();
    return true;
  }
  public function send_email_text($username="NULL", $emailAddress="NULL", $emailSubject="NULL", $emailMessage="NULL") {
    $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
    $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
    $messageBody .="</head><body bgcolor='#FFFFFF'>";
    if($username != 'NULL')  $messageBody .= "<h3>" . $this->lang->line('Dear'). " " . $username. "</h3>"; 
    $messageBody .= "<h4>" . $emailMessage . "</h4><br /><br /><br />";
    //Email Signature
    $messageBody .="<table class='head-wrap'>";
    $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
    $messageBody .="<tr><td>".$this->lang->line('Your Gonagoo team!')."</td></tr>";
    $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />".$this->lang->line('slider_heading1')."</td></tr>";
    $messageBody .="<tr><td>".$this->lang->line('Support')." : support@gonagoo.com</td></tr>";
    $messageBody .="<tr><td>".$this->lang->line('Website')." : www.gonagoo.com</td></tr>";
    //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
    $messageBody .="<tr><td>".$this->lang->line('Join us on')." : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='G+'></a>";
    $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='Twitter'></a>";
    $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='LinkedIn'></a>";
    $messageBody .="<a href='https://www.facebook.com/gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/facebook.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='FB'></a></td></tr>";
    //Download the App <icon for iOS App download><icon for Android App download>
    $messageBody .="<tr><td>".$this->lang->line('Download the App')." : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='iOS'></a>";
    $messageBody .="<a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='Android'></a></td></tr>";
    $messageBody .="</table></div></td><td></td></tr></table>";
    //Email Signature End
    $messageBody .="</body></html>";

    require_once('resources/PHPMailer-master/src/Exception.php');
    require_once('resources/PHPMailer-master/src/PHPMailer.php');
    $email = new PHPMailer();
    $email->SetFrom($this->config->item('from_email'), 'Gonagoo');
    $email->isHTML(true);
    $email->Subject   = $emailSubject;
    $email->Body      = $messageBody;
    $email->CharSet  = 'UTF-8';
    $email->AddAddress(trim($emailAddress));
    //$email->Send();
    return true;
  }
  public function sendSMS($user_mobile=null, $message=null) {
    if($message!=null && $user_mobile!=null) {
      $message .= ' - '.$this->lang->line('Powered by Gonagoo');
      if(substr($user_mobile, 0, 1) == 0) { $user_mobile = ltrim($user_mobile, 0); }
      $ch = curl_init("http://smsc.txtnation.com:8091/sms/send_sms.php?"); // url to send sms
      curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
      curl_setopt($ch, CURLOPT_POST, 1); // method to call url
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
      curl_setopt($ch, CURLOPT_POSTFIELDS,"src=330679328525&dst=$user_mobile&dr=1&user=nkengne&password=P9cKxW&type=0&msg=$message"); 
      //$outputSMS = curl_exec($ch); // execute url and save response
      //var_dump($outputSMS); die();
      curl_close($ch); // close url connection
      return true;
    }
  }
  public function sendFCM(array $msg, array $reg_id, $api_key=null) {
    if(is_array($msg) && is_array($reg_id) && !is_null($api_key)) {
      $header = array('Authorization: key='.$api_key,'Content-Type: application/json');
      $fields = array('registration_ids' => $reg_id, 'data' => $msg);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
      //$result_fcm = curl_exec($ch);
      curl_close($ch);
      return 1;
    }
  }
  public function sendPushIOS(array $data, $tokens, $api_key=null) {
    $fcmMsg = array(
      'body' => $data['text'],
      'title' => $data['title'],
      'sound' => "default",
      'color' => "#203E78" 
    );
    $fcmFields = array(
      'to' => $tokens,
      'priority' => 'high',
      'notification' => $fcmMsg
    );
    $headers = array(
      'Authorization: key=' . $api_key,
      'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    //$result = curl_exec($ch );
    curl_close( $ch );    
  }





  /*public function send_email_text($username="NULL", $emailAddress="NULL", $emailSubject="NULL", $emailMessage="NULL") {
    require_once('resources/PHPMailer-master/src/Exception.php');
    require_once('resources/PHPMailer-master/src/PHPMailer.php');
    $mail = new PHPMailer();
    $mail->SetFrom($this->config->item('email_from'), 'Papeo');
    $mail->AddAddress($emailAddress);
    $mail->Subject = $emailSubject;

    $mail->CharSet = 'UTF-8';
    $mail->isHTML(true);
    $mail->AddEmbeddedImage('http://papeo.party/apis/uploads/papeo.jpg', 'logo_2u');

    $messageBody ='<html><head><meta name="viewport" content="width=device-width" /><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
    $messageBody .='</head><body bgcolor="#AAAAAA">';
    if($username != 'NULL') $messageBody .= "<h3> Dear " . $username. "</h3>"; 
    $messageBody .= "<h4>" . $emailMessage . "</h4><br /><br /><br />";
    // Email Signature
    $messageBody .="<table class='head-wrap'>";
    $messageBody .="<tr><td class='header container'><div class='content'><table>";
    $messageBody .="<tr><td> Your Papeo terms </td></tr>";
    $messageBody .="<tr><td><img src=\"cid:logo_2u\" width='50px' height='50px' alt='Logo'></td></tr>";
    $messageBody .="<tr><td> Support  : support@papeo.com</td></tr>";
    $messageBody .="<tr><td> website : www.papeo.party</td></tr>";
    $messageBody .="</table></div></td></tr></table>";
    $messageBody .='</body></html>';

    $mail->Body = $messageBody;
    
    if($mail->Send()) { return true;
    } else { return false; }
  } */










}
/* End of file Api_model_laundry.php */
/* Location: ./application/models/Api_model_laundry.php */