<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_support_model extends CI_Model {

	private $_account_withdraw_request = "tbl_account_withdraw_request";
	private $_relay_points = "tbl_relay_points";
	private $_user_account_master = 'tbl_user_account_master';
	private $_user_account_history = 'tbl_user_account_history';
	private $_relay_account_master = 'tbl_relay_account_master';
	private $_relay_account_history = 'tbl_relay_account_history';

	private $_customer_drivers = "tbl_customer_drivers";
	private $_relay_points_warehouse = "tbl_relay_points_warehouse";
	private $_states = "tbl_states";
	private $_cities = "tbl_cities";

	
	private $_customer_support_queries = "tbl_customer_support_queries";
	private $_countries = "tbl_countries";
	private $_customers_master = "tbl_customers_master";


	public function get_support_list($admin_country_id=0)
	{
		if($admin_country_id > 0){ 	
			$qry = "SELECT `s`.*, `c`.`country_id` as country_id FROM `tbl_customer_support_queries` as `s` JOIN `tbl_customers_master` as `c` ON `s`.`cust_id` = `c`.`cust_id` AND `c`.`country_id` = ".$admin_country_id."  AND `s`.`status` IN ('open','in_progress') order by `s`.`cre_datetime` asc";
		} 
		else {	
			$qry = "SELECT `s`.*, `c`.`country_id` as country_id FROM `tbl_customer_support_queries` as `s` JOIN `tbl_customers_master` as `c` ON `s`.`cust_id` = `c`.`cust_id` AND `s`.`status` IN ('open','in_progress') order by `s`.`cre_datetime` asc";
		}
		return $this->db->query($qry)->result_array();
	}

	public function get_support_request_details($query_id=0)
	{
		if($query_id > 0){
			$this->db->where('query_id', (int)$query_id);
			return $this->db->get($this->_customer_support_queries)->row_array();
		}
		return false;
	}

	public function get_customer_details($cust_id=0)
	{
		if($cust_id > 0 ) {
			$this->db->select('firstname, lastname, mobile1, email1, country_id');
			$this->db->where('cust_id', $cust_id);
			$row = $this->db->get($this->_customers_master)->row_array();
			$firstname = $row["firstname"] == 'NULL' ? '' : $row["firstname"] ;
			$lastname = $row["lastname"] == 'NULL' ? '' : $row["lastname"];
			//get country code
			$this->db->select('country_phonecode');
			$this->db->where('country_id', (int)$row["country_id"]);
			$country_row = $this->db->get($this->_countries)->row_array();
			return $firstname.' '.$lastname.' [+'.$country_row['country_phonecode'].$row["mobile1"].' / '.$row["email1"].']';
		}
		return false;
	}

	public function update_customer_support_request($query_id=0, array $data)
	{
		if(is_array($data) && $query_id > 0 ){			
			$this->db->where('query_id', (int)$query_id);
			$this->db->update($this->_customer_support_queries, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file Customer_support_model.php */
/* Location: ./application/models/Customer_support_model.php */