<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends CI_Model {

	private $notifications = "tbl_notifications";
	private $countries = "tbl_countries";
	private $states = "tbl_states";
	private $cities = "tbl_cities";
	private $customers = "tbl_customers_master";


	public function get_notifications()
	{		
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->notifications)->result_array();
	}

	public function get_country_detail($id=0)
	{		
		if($id > 0 ){
			$this->db->where('country_id', (int)$id);
			return $this->db->get($this->countries)->row_array();
		}
		return false;
	}

	public function get_state_detail($id=0)
	{		
		if($id > 0 ){
			$this->db->where('state_id', (int)$id);
			return $this->db->get($this->states)->row_array();
		}
		return false;
	}

	public function get_city_detail($id=0)
	{		
		if($id > 0 ){
			$this->db->where('city_id', (int)$id);
			return $this->db->get($this->cities)->row_array();
		}
		return false;
	}

	public function get_notification_detail($id=0)
	{		
		if($id > 0 ){
			$this->db->where('id', (int) $id);
			return $this->db->get($this->notifications)->row_array();
		}
	}

	public function get_customer_countries()
	{
		$this->db->select('c.country_id, c.country_name, u.country_id');
		$this->db->from('tbl_customers_master as u');
		$this->db->join('tbl_countries as c', 'u.country_id = c.country_id');
		$this->db->group_by('u.country_id');
		return $this->db->get()->result_array();
	}

	public function get_customer_states($country_id=0)	
	{
		if($country_id > 0) {
			$this->db->select('s.state_id, s.state_name');
			$this->db->from('tbl_customers_master as u');
			$this->db->join('tbl_states as s', 'u.state_id = s.state_id');
			$this->db->where('u.country_id', $country_id);
			$this->db->group_by('u.state_id');
			return $this->db->get()->result_array();
		}
		return false;
	}	

	public function get_customer_cities($state_id=0)	
	{
		if($state_id > 0) {
			$this->db->select('s.city_id, s.city_name');
			$this->db->from('tbl_customers_master as u');
			$this->db->join('tbl_cities as s', 'u.city_id = s.city_id');
			$this->db->where('u.state_id', $state_id);
			$this->db->group_by('u.city_id');
			return $this->db->get()->result_array();
		}
		return false;
	}	

	public function register_notification(array $data)
	{
		if(is_array($data)){
			$this->db->insert($this->notifications, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function sendFCM($message=array(), $notifiction_regids=array())
	{
	  $fields = array();

	  $key = trim($this->config->item('delivererAppGoogleKey'));
	  $url = 'https://fcm.googleapis.com/fcm/send';
	  $headers = array('Authorization: key='.$key,'Content-Type: application/json');

	  $fields = array('registration_ids' => $notifiction_regids,'data' => $message);

	  $ch = curl_init();
	  curl_setopt($ch, CURLOPT_URL, $url);
	  curl_setopt($ch, CURLOPT_POST, true);
	  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	  $result = curl_exec($ch);

	  curl_close($ch);
	  return $result;
	}

	public function get_android_fcmids(array $data)
	{
		if(is_array($data)) {

			$android_ids = array();

			$this->db->select('d.reg_id');
			$this->db->from('tbl_customers_master as c');
			$this->db->join('tbl_customer_device as d', 'd.cust_id = c.cust_id');

			$this->db->where('c.cust_status', 1);
			$this->db->where('d.device_type', 1);

			if(strtolower($data ["consumer"]) === "individuals") { $this->db->where('c.user_type', 1);	}
			if(strtolower($data ["consumer"]) === "business") { $this->db->where('c.user_type', 0);	}

			if((int)($data ["country_id"]) > 0 ) { $this->db->where('c.country_id', (int)($data ["country_id"]));	}
			if((int)($data ["state_id"]) > 0 ) { $this->db->where('c.state_id', (int)($data ["state_id"]));	}
			if((int)($data ["city_id"]) > 0 ) { $this->db->where('c.city_id', (int)($data ["city_id"]));	}

			$this->db->group_by('c.cust_id');
			$result = $this->db->get()->result_array();
			
			foreach ( $result as $v) {	$android_ids[] = $v['reg_id'];	}
		} 
		return $android_ids;
	}

	public function get_users_android_fcmids($user_id=0)
	{
		if($user_id > 0) {

			$android_ids = array();

			$this->db->select('reg_id');
			$this->db->from('tbl_customer_device');			
			$this->db->where('cust_id', $user_id);
			//$this->db->where('device_type', 1);
			$result = $this->db->get()->result_array();
			
			foreach ( $result as $v) {	$android_ids[] = $v['reg_id'];	}
		} 
		return $android_ids;
	}

	// IOS push notification 
    public function sendPushIOS(array $data, $tokens)
	{
		$serverKey = trim($this->config->item('delivererAppGoogleKey'));

		$fcmMsg = array(
			'body' => $data['text'],
			'title' => $data['title'],
			'sound' => "default",
		    'color' => "#203E78" 
		);

		$fcmFields = array(
			'to' => $tokens,
            'priority' => 'high',
			'notification' => $fcmMsg
		);

		$headers = array(
			'Authorization: key=' . $serverKey,
			'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
		$result = curl_exec($ch );
		curl_close( $ch );		
	}

  public function get_ios_tokens(array $data)
	{
		if(is_array($data)) {

			$tokens = array();

			$this->db->select('d.reg_id');
			$this->db->from('tbl_customers_master as c');
			$this->db->join('tbl_customer_device as d', 'd.cust_id = c.cust_id');

			$this->db->where('c.cust_status', 1);
			$this->db->where('d.device_type', 0);

			if(strtolower($data ["consumer"]) === "individuals") { $this->db->where('c.user_type', 1);	}
			if(strtolower($data ["consumer"]) === "business") { $this->db->where('c.user_type', 0);	}

			if((int)($data ["country_id"]) > 0 ) { $this->db->where('c.country_id', (int)($data ["country_id"]));	}
			if((int)($data ["state_id"]) > 0 ) { $this->db->where('c.state_id', (int)($data ["state_id"]));	}
			if((int)($data ["city_id"]) > 0 ) { $this->db->where('c.city_id', (int)($data ["city_id"]));	}

			$this->db->group_by('c.cust_id');
			$result = $this->db->get()->result_array();
			
			foreach ( $result as $v) {	$tokens[] = $v['reg_id'];	}
		} 
		return $tokens;
	}

	public function get_users_ios_tokens($user_id=0)
	{
		if($user_id > 0) {

			$tokens = array();

			$this->db->select('reg_id');
			$this->db->from('tbl_customer_device');			
			$this->db->where('cust_id', $user_id);
			$this->db->where('device_type', 0);
			$result = $this->db->get()->result_array();
			
			foreach ( $result as $v) {	$tokens[] = $v['reg_id'];	}
		} 
		return $tokens;
	}

}

/* End of file Notification_model.php */
/* Location: ./application/models/Notification_model.php */