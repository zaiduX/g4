<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdraw_model extends CI_Model {

	private $_account_withdraw_request = "tbl_account_withdraw_request";
	private $_customers_master = "tbl_customers_master";
	private $_relay_points = "tbl_relay_points";
	private $_user_account_master = 'tbl_user_account_master';
	private $_user_account_history = 'tbl_user_account_history';
	private $_relay_account_master = 'tbl_relay_account_master';
	private $_relay_account_history = 'tbl_relay_account_history';

	private $_customer_drivers = "tbl_customer_drivers";
	private $_relay_points_warehouse = "tbl_relay_points_warehouse";
	private $_countries = "tbl_countries";
	private $_states = "tbl_states";
	private $_cities = "tbl_cities";
	private $_smp_user_account_master = 'tbl_smp_user_account_master';   
    private $_smp_user_account_history = 'tbl_smp_user_account_history';


	public function get_withdraw_request($admin_country_id=0)
	{
		if($admin_country_id > 0){ 	
			$qry = "SELECT `w`.*, `c`.`country_id` as country_id FROM `tbl_account_withdraw_request` as `w` JOIN `tbl_customers_master` as `c` ON `w`.`user_id` = `c`.`cust_id` AND `w`.`user_type` = 0 AND `c`.`country_id` = ".$admin_country_id." UNION SELECT `w`.*, `r`.`country_id` as country_id FROM `tbl_account_withdraw_request` as `w` JOIN `tbl_relay_points` as `r` ON `w`.`user_id` = `r`.`relay_id` AND `w`.`user_type` = 1 AND `r`.`country_id` = ".$admin_country_id;
		} 
		else {	
			$qry = "SELECT `w`.*, `c`.`country_id` as country_id FROM `tbl_account_withdraw_request` as `w` JOIN `tbl_customers_master` as `c` ON `w`.`user_id` = `c`.`cust_id` AND `w`.`user_type` = 0 UNION SELECT `w`.*, `r`.`country_id` as country_id FROM `tbl_account_withdraw_request` as `w` JOIN `tbl_relay_points` as `r` ON `w`.`user_id` = `r`.`relay_id` AND `w`.`user_type` = 1";
		}
		return $this->db->query($qry)->result_array();
	}

	public function get_customer_details($cust_id=0)
	{
		if($cust_id > 0 ) {
			$this->db->select('firstname, lastname, mobile1');
			$this->db->where('cust_id', $cust_id);
			$row = $this->db->get($this->_customers_master)->row_array();
			if($row["firstname"] == "NULL") { return 'Not Provided ['.$row["mobile1"].']'; }
			else { return $row["firstname"].' '.$row["lastname"].' ['.$row["mobile1"].']'; }
		}
		return false;
	}

	public function get_relay_details($relay_id=0)
	{
		if($relay_id > 0 ) {
			$this->db->select('firstname, lastname, mobile_no');
			$this->db->where('relay_id', $relay_id);
			$row = $this->db->get($this->_relay_points)->row_array();
			if($row["firstname"] == "NULL") { return 'Not Provided ['.$row["mobile_no"].']'; }
			else { return $row["firstname"].' '.$row["lastname"].' ['.$row["mobile_no"].']'; }
		}
		return false;
	}

	public function reject_withdraw_request($id=0)
	{
		if($id > 0){
			$this->db->where('req_id', (int) $id);
			$this->db->update($this->_account_withdraw_request, ['response' => 'reject', 'response_datetime' => date('Y-m-d h:i:s')]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_withdraw_request_details($req_id=0)
	{
		if($req_id > 0){
			$this->db->where('req_id', (int)$req_id);
			return $this->db->get($this->_account_withdraw_request)->row_array();
		}
		return false;
	}

	public function update_withdraw_transfer_amount($req_id=0, array $data)
	{
		if(is_array($data) && $req_id > 0 ){			
			$this->db->where('req_id', (int)$req_id);
			$this->db->update($this->_account_withdraw_request, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function customer_account_master_details($cust_id=0)
	{
		if($cust_id > 0) {
			$this->db->where('user_id', (int)$cust_id);
			return $this->db->get($this->_user_account_master)->row_array();
		}
		return false;
	}

	public function customer_account_master_details_currencywise($cust_id=0, $currency_sign=null)
	{
		if($cust_id > 0 && !is_null($currency_sign)) {
			$this->db->where('user_id', (int)$cust_id);
			$this->db->where('currency_code', trim($currency_sign));
			return $this->db->get($this->_user_account_master)->row_array();
		}
		return false;
	}

	public function update_payment_in_customer_master($cust_id=0, $account_balance=0)
	{
		if($cust_id > 0) {
			$this->db->where('user_id', (int)$cust_id);
			$this->db->update($this->_user_account_master, ['account_balance' => $account_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_payment_in_customer_master_by_id($account_id=0, $cust_id=0, $account_balance=0)
	{
		if($cust_id > 0 && $account_id > 0) {
			$this->db->where('user_id', (int)$cust_id);
			$this->db->where('account_id', (int)$account_id);
			$this->db->update($this->_user_account_master, ['account_balance' => $account_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function insert_payment_in_account_history(array $insert_data)
	{
		if(is_array($insert_data)) {
			$this->db->insert($this->_user_account_history, $insert_data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function relay_account_master_details($user_id=0)
	{
		if($user_id > 0) {
			$this->db->where('user_id', (int)$user_id);
			return $this->db->get($this->_relay_account_master)->row_array();
		}
		return false;
	}

	public function relay_account_master_details_currencywise($user_id=0, $currency_sign=null)
	{
		if($user_id > 0) {
			$this->db->where('user_id', (int)$user_id);
			$this->db->where('currency_sign', $currency_sign);
			return $this->db->get($this->_relay_account_master)->row_array();
		}
		return false;
	}

	public function update_payment_in_relay_master($user_id=0, $account_balance=0)
	{
		if($user_id > 0) {
			$this->db->where('user_id', (int)$user_id);
			$this->db->update($this->_relay_account_master, ['account_balance' => $account_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_payment_in_relay_master_by_id($account_id=0, $user_id=0, $account_balance=0)
	{
		if($user_id > 0 && $account_id > 0) {
			$this->db->where('user_id', (int)$user_id);
			$this->db->where('account_id', (int)$account_id);
			$this->db->update($this->_relay_account_master, ['account_balance' => $account_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function insert_payment_in_relay_account_history(array $insert_data)
	{
		if(is_array($insert_data)) {
			$this->db->insert($this->_relay_account_history, $insert_data);
			return $this->db->affected_rows();
		}
		return false;
	}

	// g4
	public function customer_account_master_details_smp($cust_id=0)
	{
		if($cust_id > 0) {
			$this->db->where('user_id', (int)$cust_id);
			return $this->db->get($this->_smp_user_account_master)->row_array();
		}
		return false;
	}

	public function customer_account_master_details_currencywise_smp($cust_id=0, $currency_sign=null)
	{
		if($cust_id > 0 && !is_null($currency_sign)) {
			$this->db->where('user_id', (int)$cust_id);
			$this->db->where('currency_code', trim($currency_sign));
			return $this->db->get($this->_smp_user_account_master)->row_array();
		}
		return false;
	}

	public function update_payment_in_customer_master_smp($cust_id=0, $account_balance=0)
	{
		if($cust_id > 0) {
			$this->db->where('user_id', (int)$cust_id);
			$this->db->update($this->_smp_user_account_master, ['account_balance' => $account_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_payment_in_customer_master_by_id_smp($account_id=0, $cust_id=0, $account_balance=0)
	{
		if($cust_id > 0 && $account_id > 0) {
			$this->db->where('user_id', (int)$cust_id);
			$this->db->where('account_id', (int)$account_id);
			$this->db->update($this->_smp_user_account_master, ['account_balance' => $account_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function insert_payment_in_account_history_smp(array $insert_data)
	{
		if(is_array($insert_data)) {
			$this->db->insert($this->_smp_user_account_history, $insert_data);
			return $this->db->affected_rows();
		}
		return false;
	}
		
}

/* End of file Withdraw_model.php */
/* Location: ./application/models/Withdraw_model.php */