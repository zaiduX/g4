<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport_model extends CI_Model {

  private $country_dangerous_goods = "tbl_country_dangerous_goods";
  private $dangerous_goods_master = "tbl_dangerous_goods_master";
  private $unit_master = "tbl_unit_master";
  private $order_packages = "tbl_order_packages";

  public function get_country_dangerous_goods($country_id=0)
  {
    if($country_id > 0 ){  
      $this->db->select('goods_ids');
      $this->db->where('status', 1);
      $this->db->where('country_id', (int) $country_id); 
      if($return = $this->db->get($this->country_dangerous_goods)->row_array()) {
        $ids = explode(',',$return['goods_ids']);      
        $dangerous_goods = array();
        foreach ($ids as $id) {        
          $dgd = $this->get_dangerous_goods($id);
          if(is_array($dgd) && !empty($dgd)){
            $dangerous_goods[] = array('id' => $dgd['id'], 'code' => $dgd['code'], 'name' => $dgd['name'],'description' => $dgd['description'],'image_url' => $dgd['image_url']);
          }
        }
        return $dangerous_goods;
      } else { return array(); }
    } else {  
      $this->db->where('status', 1);
      return $this->db->get($this->country_dangerous_goods)->result_array(); 
    }
  }

  public function get_countries_dangerous_goods($from_country_id=0, $to_country_id=0)
  {
    $this->db->select('goods_ids');
    $this->db->where('status', 1);
    $this->db->where('country_id', (int) $from_country_id); 
    $this->db->or_where('country_id', (int) $to_country_id); 
    if($return = $this->db->get($this->country_dangerous_goods)->result_array()) {
      $ids = $good_ids = array();
      foreach ($return as $v ) {  $ids[] = $v['goods_ids'];   }
      foreach ($ids as $d ) {  
        $temp = explode(',',$d); 
        foreach ($temp as $v) { $goods_ids[] = $v; }  
      }
      $goods_ids = array_unique($goods_ids);
      $dangerous_goods = array();
      foreach ($goods_ids as $id) {        
        $dgd = $this->get_dangerous_goods($id);
        if(is_array($dgd) && !empty($dgd)){
          $dangerous_goods[] = array('id' => $dgd['id'], 'code' => $dgd['code'], 'name' => $dgd['name'],'description' => $dgd['description'],'image_url' => $dgd['image_url']);
        }
      }
      return $dangerous_goods;
    } else return false;
  }

  public function get_dangerous_goods($id=0)
  {
    $this->db->where('status', 1);
    if($id > 0 ){  
      $this->db->where('id', (int) $id); 
      return $this->db->get($this->dangerous_goods_master)->row_array();
    } else {  return $this->db->get($this->dangerous_goods_master)->result_array(); } 
  }

  public function get_unit($id=0)
  {
    if($id>0){
      $this->db->where('unit_id', (int) $id);
      return $this->db->get($this->unit_master)->row_array();
    }
    else{ return $this->db->get($this->unit_master)->result_array();  }
  }

  public function insert_package(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->order_packages, $data);
      return $this->db->insert_id();
    }
    return false;
  }


}

/* End of file transport_model.php */
/* Location: ./application/models/transport_model.php */