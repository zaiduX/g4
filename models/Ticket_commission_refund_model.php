<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket_commission_refund_model extends CI_Model {

	private $_ticket_commission_refund_request = "tbl_ticket_commission_refund_request";
	private $_customers_master = "tbl_customers_master";

	public function get_commission_refund_request($admin_country_id=0)
	{
		if($admin_country_id > 0){ 	
			$qry = "SELECT `w`.*, `c`.`country_id` as country_id FROM `tbl_ticket_commission_refund_request` as `w` JOIN `tbl_customers_master` as `c` ON `w`.`operator_id` = `c`.`cust_id` AND `c`.`country_id` = ".$admin_country_id." AND req_status = 'open'";
		} else {	
			$qry = "SELECT `w`.*, `c`.`country_id` as country_id FROM `tbl_ticket_commission_refund_request` as `w` JOIN `tbl_customers_master` as `c` ON `w`.`operator_id` = `c`.`cust_id` AND req_status = 'open'";
		}
		return $this->db->query($qry)->result_array();
	}
	public function get_customer_details($cust_id=0)
	{
		if($cust_id > 0 ) {
			$this->db->select('firstname, lastname, mobile1');
			$this->db->where('cust_id', $cust_id);
			$row = $this->db->get($this->_customers_master)->row_array();
			if($row["firstname"] == "NULL") { return 'Not Provided ['.$row["mobile1"].']'; }
			else { return $row["firstname"].' '.$row["lastname"].' ['.$row["mobile1"].']'; }
		}
		return false;
	}
	public function reject_refund_request($id=0)
	{
		if($id > 0){
			$this->db->where('req_id', (int) $id);
			$this->db->update($this->_ticket_commission_refund_request, ['req_status' => 'reject', 'req_status_date' => date('Y-m-d h:i:s')]);
			return $this->db->affected_rows();
		}
		return false;
	}
	public function accept_refund_request($id=0)
	{
		if($id > 0){
			$this->db->where('req_id', (int) $id);
			$this->db->update($this->_ticket_commission_refund_request, ['req_status' => 'accept', 'req_status_date' => date('Y-m-d h:i:s')]);
			return $this->db->affected_rows();
		}
		return false;
	}
    public function get_refund_request_details($req_id = null)
    {
        if($req_id != null) {
            $this->db->where('req_id',$req_id);
            return $this->db->get($this->_ticket_commission_refund_request)->row_array();
        } return false;
    }	
}

/* End of file Ticket_commission_refund_model.php */
/* Location: ./application/models/Ticket_commission_refund_model.php */