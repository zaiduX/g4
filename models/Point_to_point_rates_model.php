<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Point_to_point_rates_model extends CI_Model {

  private $p2p_rates = "tbl_point_to_point_rates"; // db table declaration
  private $units = "tbl_unit_master"; 
  private $currency = "tbl_currency_master";
  private $countries = "tbl_countries";
  private $states = "tbl_states";
  private $cities = "tbl_cities";
  private $country_currencies = "tbl_country_currency";
  private $standard_dimension = "tbl_standard_dimension";
  private $categories = "tbl_category_master";
  
  public function get_pp_rates($id=0)
  {   
    $this->db->select('r.*, cf.country_name as from_country_name, ct.country_name as to_country_name, cr.currency_sign, cr.currency_title,cat.cat_name');
    $this->db->from($this->p2p_rates.' as r');
    $this->db->join($this->currency.' as cr', 'cr.currency_id = r.currency_id');
    $this->db->join($this->countries.' as cf', 'cf.country_id = r.from_country_id');
    $this->db->join($this->countries.' as ct', 'ct.country_id = r.to_country_id');
    $this->db->join($this->categories.' as cat', 'cat.cat_id = r.category_id');
    
    if($id > 0) { 
      $this->db->where('rate_id', $id);
      return $this->db->get()->row_array();
    } else {
      $this->db->order_by('rate_id', 'desc');
      return $this->db->get()->result_array();
    }
  }

  public function get_states($country_id=0)
  {
    if($country_id > 0 ) {
      $this->db->where('country_id', $country_id);
      return $this->db->get($this->states)->result_array();
    }
    return false;
  }

  public function get_state_detail_by_id($id=0)
  {
    if($id > 0 ) {
      $this->db->where('state_id', $id);
      return $this->db->get($this->states)->row_array();
    }
    return false;
  }

  public function get_cities($state_id=0)
  {
    if($state_id > 0 ) {
      $this->db->where('state_id', $state_id);
      return $this->db->get($this->cities)->result_array();
    }
    return false;
  }

  public function get_city_detail_by_id($id=0)
  {
    if($id > 0 ) {
      $this->db->where('city_id', $id);
      return $this->db->get($this->cities)->row_array();
    }
    return false;
  }

  public function get_dimension_detail_by_id($id=0)
  {
    if($id > 0 ) {
      $this->db->where('dimension_id', $id);
      return $this->db->get($this->standard_dimension)->row_array();
    }
    return false;
  }

  public function check_volume_based_p_to_p_rates(array $data, $flag=null)
  {
    if(is_array($data) && $flag != null && !empty($flag)){
      $category_id = $data['category_id'];
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];
      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $this->db->select('rate_id');
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where('category_id', (int) $category_id);
      $this->db->where('from_country_id', (int) $from_country_id);
      if($from_state_id > 0 ){ $this->db->where('from_state_id', (int) $from_state_id); }
      if($from_city_id > 0 ){ $this->db->where('from_city_id', (int) $from_city_id); }

      $this->db->where('to_country_id', (int) $to_country_id);
      if($to_state_id > 0 ){ $this->db->where('to_state_id', (int) $to_state_id); }
      if($to_city_id > 0 ){ $this->db->where('to_city_id', (int) $to_city_id); }

      if($flag == 'min') { $min_volume = $data['min_volume']; $this->db->where("$min_volume BETWEEN min_volume AND max_volume"); }
      else if($flag == 'max') { $max_volume = $data['max_volume'];  $this->db->where("$max_volume BETWEEN min_volume AND max_volume"); }

      if($res = $this->db->get($this->p2p_rates)->row_array()){ return true; }
    }
    return false;
  }

  public function register_p_to_p_rates(array $data)
  {
    if(is_array($data)){      
      $this->db->insert($this->p2p_rates, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_p_to_p_rates(array $data, $id=0)
  {
    if(is_array($data) AND $id > 0){
      $this->db->where('rate_id', (int)$id);
      $this->db->update($this->p2p_rates, $data);
      return $this->db->affected_rows();
    }
    return false;
  }
  
  public function update_pp_rates(array $data, $id=0)
  {
    if(is_array($data) AND $id > 0){
      $this->db->where('rate_id', (int)$id);
      $this->db->update($this->p2p_rates, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function check_weight_based_p_to_p_rates(array $data, $flag=null)
  {
    if(is_array($data) && $flag != null && !empty($flag)){
      $category_id = $data['category_id'];
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];
      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];
      $unit_id = $data['unit_id'];


      $this->db->select('rate_id');
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where('category_id', (int) $category_id);
      $this->db->where('from_country_id', (int) $from_country_id);
      if($from_state_id > 0 ){ $this->db->where('from_state_id', (int) $from_state_id); }
      if($from_city_id > 0 ){ $this->db->where('from_city_id', (int) $from_city_id); }

      $this->db->where('to_country_id', (int) $to_country_id);
      if($to_state_id > 0 ){ $this->db->where('to_state_id', (int) $to_state_id); }
      if($to_city_id > 0 ){ $this->db->where('to_city_id', (int) $to_city_id); }
      
      if($flag == "min"){ $min_weight = $data['min_weight']; $this->db->where("$min_weight BETWEEN min_weight AND max_weight"); }
      else if($flag == "max"){ $max_weight = $data['max_weight']; $this->db->where("$max_weight BETWEEN min_weight AND max_weight"); }

      $this->db->where('unit_id', (int) $unit_id);
      if($res = $this->db->get($this->p2p_rates)->row_array()){ return true; }
    }
    return false;
  }

  public function get_unit_detail($id=0)
  {
    if($id > 0) {
      $this->db->where('unit_id', (int) $id);
      return $this->db->get($this->units)->row_array();
    }
    return false;
  }

  public function delete_pp_rates($id=0)
  {
    if($id > 0 ){
      $this->db->where('rate_id', (int) $id);
      $this->db->delete($this->p2p_rates);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function check_formula_volume_based_p_to_p_rates(array $data)
  {
    if(is_array($data)){
      $category_id = $data['category_id'];
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];
      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];

      $this->db->select('rate_id');
      $this->db->where('is_formula_volume_rate', 1);
      $this->db->where('category_id', (int) $category_id);
      $this->db->where('from_country_id', (int) $from_country_id);
      if($from_state_id > 0 ){ $this->db->where('from_state_id', (int) $from_state_id); }
      if($from_city_id > 0 ){ $this->db->where('from_city_id', (int) $from_city_id); }

      $this->db->where('to_country_id', (int) $to_country_id);
      if($to_state_id > 0 ){ $this->db->where('to_state_id', (int) $to_state_id); }
      if($to_city_id > 0 ){ $this->db->where('to_city_id', (int) $to_city_id); }

      if($res = $this->db->get($this->p2p_rates)->row_array()){ return true; }
    }
    return false;
  }

  public function check_formula_weight_based_p_to_p_rates(array $data)
  {
    if(is_array($data)){
      $category_id = $data['category_id'];
      $from_country_id = $data['from_country_id'];
      $from_state_id = $data['from_state_id'];
      $from_city_id = $data['from_city_id'];
      $to_country_id = $data['to_country_id'];
      $to_state_id = $data['to_state_id'];
      $to_city_id = $data['to_city_id'];
      $unit_id = $data['unit_id'];

      $this->db->select('rate_id');
      $this->db->where('is_formula_weight_rate', 1);
      $this->db->where('category_id', (int) $category_id);
      $this->db->where('from_country_id', (int) $from_country_id);
      if($from_state_id > 0 ){ $this->db->where('from_state_id', (int) $from_state_id); }
      if($from_city_id > 0 ){ $this->db->where('from_city_id', (int) $from_city_id); }

      $this->db->where('to_country_id', (int) $to_country_id);
      if($to_state_id > 0 ){ $this->db->where('to_state_id', (int) $to_state_id); }
      if($to_city_id > 0 ){ $this->db->where('to_city_id', (int) $to_city_id); }

      $this->db->where('unit_id', (int) $unit_id);
      if($res = $this->db->get($this->p2p_rates)->row_array()){ return true; }
    }
    return false;
  }

}

/* End of file Point_to_point_rates_model.php */
/* Location: ./application/models/Point_to_point_rates_model.php */