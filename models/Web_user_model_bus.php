<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_user_model_bus extends CI_Model {

  private $authorities = "tbl_authorities_master";
  private $auth_permissions = "tbl_auth_permissions";
  private $_state_master = 'tbl_states';
  private $_city_master = 'tbl_cities';
  private $_business_photos = "tbl_business_photos";
  private $_permitted_category = "tbl_permitted_category";
  private $_customer_master = 'tbl_customers_master';
  private $_customer_education = 'tbl_customer_education';
  private $_customer_experience = 'tbl_customer_experience';
  private $_customer_skills = 'tbl_customer_skills';
  private $_customer_portfolio = 'tbl_consumer_portfolio';
  private $_customer_documents = 'tbl_consumers_documents';
  private $_customer_pay_methods = 'tbl_consumer_payment_details';
  private $_addressbook = 'tbl_address_books';
  private $_category_master = 'tbl_category_master';
  private $_language_master = 'tbl_language_master';
  private $_category_type_master = 'tbl_category_type_master';
  private $_skill_master = 'tbl_skills_master';
  private $_courier_workroom = 'tbl_courier_workroom';
  private $_driver_chats = 'tbl_driver_chats';
  private $_relay_points = 'tbl_relay_points';
  private $_courier_orders = 'tbl_courier_orders';
  private $_unit_master = "tbl_unit_master";  
  private $_transport_vehicle_master = "tbl_transport_vehicle_master";  
  private $_standard_dimension = "tbl_standard_dimension";  
  private $_customer_drivers = "tbl_customer_drivers";
  private $_courier_order_deliverer_request = 'tbl_courier_order_deliverer_request';
  private $_favourite_deliverers = 'tbl_favourite_deliverers';
  private $_deliverer_documents = 'tbl_deliverer_documents';
  private $_deliverer_scrow_master = 'tbl_deliverer_scrow_master';
  private $_deliverer_scrow_history = 'tbl_deliverer_scrow_history';
  private $_user_account_master = 'tbl_user_account_master';
  private $_user_account_history = 'tbl_user_account_history';
  private $_account_withdraw_request = 'tbl_account_withdraw_request';
  private $_deliverer_profile = "tbl_deliverer_profile";
  private $_customer_support_queries = "tbl_customer_support_queries";
  private $_carrier_type = "tbl_carrier_type";
  private $_order_packages = "tbl_order_packages";
  private $_courier_order_status = "tbl_courier_order_status";
  private $_customer_recent_relay_point = "tbl_customer_recent_relay_point";


  private $_country_master = 'tbl_countries';


  
  public function get_countries()
  {
    return $this->db->get($this->_country_master)->result_array();
  }





  public function getCountryIdByName($name=null)
  {
    $this->db->select('country_id');
    $this->db->where('country_name', $name);
    $return = $this->db->get($this->_country_master)->row_array();
    return $return['country_id'];
  }

  public function getStateIdByName($name=null,$country_id=0)
  {
    $this->db->select('state_id');
    $this->db->where('state_name', $name);
    if($return = $this->db->get($this->_state_master)->row_array()){  return $return['state_id']; }
    else {
      $data = array("state_name" => $name,  "country_id" => $country_id );
      $this->db->insert($this->_state_master, $data);
      return $this->db->insert_id();
    }
  }

  public function getCityIdByName($name=null,$state_id=0)
  {
    $this->db->select('city_id');
    $this->db->where('city_name', $name);
    if($return = $this->db->get($this->_city_master)->row_array()){ return $return['city_id'];  }
    else{
      $data = array("city_name" => $name,  "state_id" => $state_id );
      $this->db->insert($this->_city_master, $data);
      return $this->db->insert_id();
    }

  }

  public function user_authentication($email=null, $password=null)
  {
    if(!empty($email) AND !empty($password)){
      $this->db->select('auth_id');
      $this->db->where('auth_email', $email);
      $this->db->where('auth_pass', $password);
      return $this->db->get($this->authorities)->row_array();
    } 
    return false;
  }

  public function logged_in_user_details($id=0)
  {
    if($id > 0 ) {
      $this->db->select('auth_id, auth_fname as fname, auth_lname as lname, auth_email as email, auth_type_id as type_id, auth_avatar_url as avatar_url');
      $this->db->where('auth_id', $id);
      return $this->db->get($this->authorities)->row_array();
    }
    return false;
  }

  public function get_auth_permissions($id=0)
  {
    if($id > 0 ) {
      $this->db->where('auth_id', $id);
      return $this->db->get($this->auth_permissions)->result_array();
    }
    return false;
  }

  public function update_profile(array $data)
  {
    if(is_array($data) ){     
      $auth_id = $this->session->userdata('userid');
      $this->db->where('auth_id', (int)$auth_id);
      $this->db->update($this->authorities, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_profile_password($auth_pass=null)
  {
    if($auth_pass != null ){      
      $auth_id = $this->session->userdata('userid');
      $this->db->where('auth_id', (int)$auth_id);
      $this->db->update($this->authorities, ['auth_pass' => trim($auth_pass)]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_country_name_by_id($id=0)
  {
    if($id>0){
      $this->db->where('country_id', (int) $id);
      $r = $this->db->get($this->_country_master)->row_array();   
      return $r['country_name'];
    }
    return false;
  }

  public function get_city_name_by_id($id=0)
  {
    if($id>0){
      $this->db->where('city_id', (int) $id);
      $r = $this->db->get($this->_city_master)->row_array();    
      return $r['city_name'];
    }
    return false;
  }

  public function get_state_name_by_id($id=0)
  {
    if($id>0){
      $this->db->where('state_id', (int) $id);
      $r = $this->db->get($this->_state_master)->row_array();   
      return $r['state_name'];
    }
    return false;
  }

  public function get_state_by_country_id($id=0)
  {
    if( $id> 0 ) {
      $this->db->where('country_id', (int) $id);
      return $this->db->get($this->_state_master)->result_array();    
    }
    return false;
  }

  public function get_transport_vehicle_list_by_type($type=null, $cat_id=0)
  {
    if( $type != null && $type !="" ) {
      $this->db->select('vehical_type_id, vehicle_type');
      $this->db->where('transport_mode', strtolower($type));
      if($cat_id > 0) { $this->db->where('cat_id', (int)$cat_id); }
      return $this->db->get($this->_transport_vehicle_master)->result_array();    
    }
    return false;
  }

  public function get_city_by_state_id($id=0)
  {
    if( $id> 0 ) {
      $this->db->where('state_id', (int) $id);
      return $this->db->get($this->_city_master)->result_array();   
    }
    return false;
  }

  public function business_photos_delete($id=0)
  {
    if($id > 0 ){
      $this->db->where('photo_id', (int) $id);
      $this->db->delete($this->_business_photos);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_driver_order_list($cd_id=0, $last_id=0, $order_status=null)
  {
    if($cd_id > 0 && !is_null($order_status)) {
      $this->db->where('driver_id', (int)($cd_id));
      $this->db->where('driver_status_update', $order_status);
      $this->db->order_by('order_id', 'desc');
      $this->db->limit(8);
      $last_id = (int) $last_id;
      if($last_id > 0) {
        $this->db->where('order_id <', $last_id);
      }
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }

  public function get_driver_category_name($pc_id=0)
  {
    if($pc_id > 0) {
      $this->db->select('short_name');
      $this->db->where('pc_id', (int)($pc_id));
      return $this->db->get($this->_permitted_category)->row_array();
    }
    return false;
  }

  public function get_deliverer_id($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->select('deliverer_id');
      $this->db->where('cust_id', (int)($cust_id));
      $row = $this->db->get($this->_deliverer_profile)->row_array();
      return $row['deliverer_id'];
    }
    return false;
  }

  public function deliverer_order_request_list($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->select($this->_courier_order_deliverer_request.'.request_id, '.$this->_courier_orders.'.*');
      $this->db->join($this->_courier_orders, $this->_courier_orders . '.order_id = ' . $this->_courier_order_deliverer_request . '.order_id', 'right outer');
      $this->db->where($this->_courier_order_deliverer_request.'.deliverer_id', (int)($cust_id));
      $this->db->order_by($this->_courier_order_deliverer_request.'.request_id', 'desc');
      return $this->db->get($this->_courier_order_deliverer_request)->result_array();
    }
    return false;
  }

  public function deliverer_order_request_list_filtered($cust_id=0, $country_id_src=0, $state_id_src=0, $city_id_src=0, $country_id_dest=0, $state_id_dest=0, $city_id_dest=0, $from_address="NULL", $to_address="NULL", $delivery_start="NULL", $delivery_end="NULL", $pickup_start="NULL", $pickup_end="NULL", $price=0, $dimension_id=0, $order_type="NULL", $creation_start_date="NULL",$creation_end_date="NULL",$expiry_start_date="NULL",$expiry_end_date="NULL",$distance_start=0,$distance_end=0,$transport_type=0,$vehicle_id=0,$max_weight=0,$unit_id=0,$cat_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->select($this->_courier_order_deliverer_request.'.request_id, '.$this->_courier_orders.'.*');
      $this->db->join($this->_courier_orders, $this->_courier_orders . '.order_id = ' . $this->_courier_order_deliverer_request . '.order_id', 'right outer');
      $this->db->where($this->_courier_order_deliverer_request.'.deliverer_id', (int)($cust_id));
      //Source Country State City Filter
      if($country_id_src > 0) { $this->db->where($this->_courier_orders.'.from_country_id', (int)($country_id_src)); }
      if($state_id_src > 0) { $this->db->where($this->_courier_orders.'.from_state_id', (int)($state_id_src)); }
      if($city_id_src > 0) { $this->db->where($this->_courier_orders.'.from_city_id', (int)($city_id_src)); }
      //Destination Country State City Filter
      if($country_id_dest > 0) { $this->db->where($this->_courier_orders.'.to_country_id', (int)($country_id_dest)); }
      if($state_id_dest > 0) { $this->db->where($this->_courier_orders.'.to_state_id', (int)($state_id_dest)); }
      if($city_id_dest > 0) { $this->db->where($this->_courier_orders.'.to_city_id', (int)($city_id_dest)); }
      //Address Filter
      if(trim($from_address) != "NULL" && trim($from_address) != "") { $this->db->like($this->_courier_orders.'.from_address', ($from_address), 'BOTH'); }
      if(trim($to_address) != "NULL" && trim($to_address) != "") { $this->db->like($this->_courier_orders.'.to_address', ($to_address), 'BOTH'); }
      //Delivery Date Filter
      if(trim($delivery_start) != "NULL" && trim($delivery_start) != "" && trim($delivery_end) != "NULL" && trim($delivery_end) != "") { $this->db->where($this->_courier_orders.'.delivery_datetime BETWEEN "'. date('Y-m-d', strtotime($delivery_start)). '" AND "'. date('Y-m-d', strtotime($delivery_end)).'"'); }
      if(trim($pickup_start) != "NULL" && trim($pickup_start) != "" && trim($pickup_end) != "NULL" && trim($pickup_end)) { $this->db->where($this->_courier_orders.'.pickup_datetime BETWEEN "'. date('Y-m-d', strtotime($pickup_start)). '" AND "'. date('Y-m-d', strtotime($pickup_end)).'"'); }
      //Price Filter
      if($price > 0) { $this->db->where($this->_courier_orders.'.order_price <='. $price); }
      //Dimension Filter
      if($dimension_id > 0) { $this->db->where($this->_courier_orders.'.dimension_id='. $dimension_id); }
      //Order Type Filter
      if(trim($order_type) != "NULL") { $this->db->where($this->_courier_orders.'.order_type ', $order_type); }

    //Creation Date Filter
  if(trim($creation_start_date) !="NULL" AND trim($creation_start_date) !="" AND trim($creation_end_date) !="NULL" AND trim($creation_end_date) !="" ) {  $this->db->where($this->_courier_orders.'.cre_datetime BETWEEN "'. $creation_start_date. '" AND "'. $creation_end_date.'"');  }
  //Expirty Date  Filter
  if(trim($expiry_start_date) !="NULL" AND trim($expiry_start_date) !="" AND trim($expiry_end_date) !="NULL" AND trim($expiry_end_date) !="" ) {  $this->db->where($this->_courier_orders.'.expiry_date BETWEEN "'. $expiry_start_date. '" AND "'. $expiry_end_date.'"');  }
  //Distance Filter
  if($distance_start > 0 AND $distance_end > 0 ) {  $this->db->where($this->_courier_orders.'.distance_in_km BETWEEN "'. $distance_start. '" AND "'. $distance_end.'"');  }
  // Transport type Filter
  if($transport_type != "NULL") { $this->db->where($this->_courier_orders.'.transport_type', $transport_type); }
  // Vehicle Filter
  if($vehicle_id > 0) { $this->db->where($this->_courier_orders.'.vehical_type_id', $vehicle_id); }
  // Vehicle Filter
  if($max_weight > 0) { $this->db->where($this->_courier_orders.'.total_weight <='. $max_weight); }
  if($unit_id > 0) { $this->db->where($this->_courier_orders.'.unit_id', $unit_id); }
  if($cat_id > 0) { $this->db->where($this->_courier_orders.'.category_id', $cat_id); }
    
      $this->db->order_by($this->_courier_order_deliverer_request.'.request_id', 'desc');
      return $this->db->get($this->_courier_order_deliverer_request)->result_array();
      //$this->db->get($this->_courier_order_deliverer_request)->result_array();
      //var_dump($this->db->last_query()); die();
    }
    return false;
  }


  public function get_total_educations($cust_id = 0 )
  {
    if( $cust_id > 0 ) {
      $this->db->where('cust_id', (int)$cust_id);
      return $this->db->from($this->_customer_education)->count_all_results();
    }
    return false;
  }

  public function get_customer_educations($cust_id=0, $rows=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('qualify_year', 'desc');
      if($rows > 0 ) { $result = $this->db->get($this->_customer_education, $rows); }
      else {  $result = $this->db->get($this->_customer_education); }
      return $result->result_array();
    }
    return false;
  }

  public function get_total_experiences($cust_id = 0, $other=false )
  {
    if( $cust_id > 0 ) {
      if($other) { $this->db->where('other', 1); }
      else { $this->db->where('other', 0); }
      $this->db->where('cust_id', (int)$cust_id);
      return $this->db->from($this->_customer_experience)->count_all_results();
    }
    return false;
  }

  public function get_customer_experiences($cust_id=0, $other=false, $rows=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      if($other) { $this->db->where('other', 1); }
      $this->db->order_by('exp_id', 'desc');
      if($rows > 0 ) { $result = $this->db->get($this->_customer_experience, $rows);  }
      else {  $result = $this->db->get($this->_customer_experience); }
      return $result->result_array();
    }
    return false;
  }

  public function get_customer_skills($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('cs_id', 'desc');
      return $result = $this->db->get($this->_customer_skills)->row_array();
    }
    return false;
  }

  public function get_total_portfolios($cust_id = 0 )
  {
    if( $cust_id > 0 ) {
      $this->db->where('cust_id', (int)$cust_id);
      return $this->db->from($this->_customer_portfolio)->count_all_results();
    }
    return false;
  }

  public function get_customer_portfolio($cust_id=0, $rows=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('portfolio_id', 'desc');
      if($rows > 0 ) { $result = $this->db->get($this->_customer_portfolio, $rows); }
      else {  $result = $this->db->get($this->_customer_portfolio); }
      return $result->result_array();
    }
    return false;
  }

  public function get_category_name_by_id($id=0)
  {
    if( $id > 0 ) {
      $this->db->where('cat_id', (int) $id);
      $return = $this->db->get($this->_category_master)->row_array();
      return $return['cat_name'];
    }
    return false;
  }

  public function get_language_master($filter=null)
  {
    $return = array();

    if($filter != null) { $this->db->like('name_en', $filter, 'BOTH');  }
    $this->db->order_by('name_en', 'asc');
    if( $res = $this->db->get($this->_language_master)->result_array()){
      foreach ($res as $r) {
        $return [] = $r['name_en'];
      }
    }
    return $return;
  }

  public function get_total_documents($cust_id = 0 )
  {
    if( $cust_id > 0 ) {
      $this->db->where('cust_id', (int)$cust_id);
      return $this->db->from($this->_customer_documents)->count_all_results();
    }
    return false;
  }

  public function get_customer_documents($cust_id=0, $rows=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('doc_id', 'desc');
      if($rows > 0 ) { $result = $this->db->get($this->_customer_documents, $rows); }
      else {  $result = $this->db->get($this->_customer_documents); }
      return $result->result_array();
    }
    return false;
  }

  public function get_total_payment_methods($cust_id = 0 )
  {
    if( $cust_id > 0 ) {
      $this->db->where('cust_id', (int)$cust_id);
      return $this->db->from($this->_customer_pay_methods)->count_all_results();
    }
    return false;
  }

  public function get_customer_payment_methods($cust_id=0, $rows=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->order_by('pay_id', 'desc');
      if($rows > 0 ) { $result = $this->db->get($this->_customer_pay_methods, $rows); }
      else {  $result = $this->db->get($this->_customer_pay_methods); }
      return $result->result_array();
    }
    return false;
  }

  public function update_basic_profile(array $data)
  {
    if(is_array($data)){
      $this->db->where('cust_id', (int) $this->session->userdata('cust_id'));
      $this->db->update($this->_customer_master, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_customer_education_by_id($id=0)
  {
    $this->db->where('edu_id', $id);
    return $this->db->get($this->_customer_education)->row_array();
  }

  public function get_customer_experience_by_id($id=0)
  {
    $this->db->where('exp_id', $id);
    return $this->db->get($this->_customer_experience)->row_array();
  }

  public function get_category_types()
  {
    $this->db->order_by('cat_type_id', 'desc');
    $this->db->where('cat_type_status', 1);
    return $this->db->get($this->_category_type_master)->result_array();
  }
  
  public function get_category_by_cat_type_id($id=0)
  {
    if( $id> 0 ) {
      $this->db->where('cat_type_id', (int) $id);
      $this->db->where('cat_status', 1);
      return $this->db->get($this->_category_master)->result_array();   
    }
    return false;
  }


  public function get_subcategory_by_category_id($id=0)
  {
    if( $id> 0 ) {
      $this->db->where('parent_cat_id', (int) $id);
      $this->db->where('cat_status', 1);
      return $this->db->get($this->_category_master)->result_array();   
    }
    return false;
  }

  public function get_document_detail($doc_id=0)
  {
    if($doc_id > 0 ) {
      $this->db->where('doc_id', (int) $doc_id);
      return $this->db->get($this->_customer_documents)->row_array();
    } 
    return false;
  }

  public function get_payment_method_detail($id=0)
  {
    if($id > 0 ) {
      $this->db->where('pay_id', (int) $id);
      return $this->db->get($this->_customer_pay_methods)->row_array();
    } 
    return false;
  } 

  public function get_skill_by_cat_id($id=0)
  {
    if( $id> 0 ) {
      $this->db->where('cat_id', (int) $id);
      $this->db->where('skill_status', 1);
      return $this->db->get($this->_skill_master)->result_array();    
    }
    return false;
  }

  public function get_skillname_by_id($id=0)
  {
    if( $id> 0 ) {
      $this->db->select('skill_name');
      $this->db->where('skill_id', (int) $id);
      $this->db->where('skill_status', 1);
      $r = $this->db->get($this->_skill_master)->row_array();   
      return $r['skill_name'];
    }
    return false;
  }

  public function get_relay_points()
  {
    $this->db->where('status', 1);
    $this->db->where('is_active', 1);
    $this->db->order_by('relay_id', 'desc');
    return $this->db->get($this->_relay_points)->result_array();
  }

  public function get_relay_pint_details($id=0)
  {
    if($id > 0 ){
      $this->db->where('relay_id', (int) $id);
      return $this->db->get($this->_relay_points)->row_array();
    }
    return false;
  }

  public function get_address_from_book($addr_id=0)
  {
    if($addr_id > 0 ){
      $this->db->where('addr_id', (int) $addr_id);
      return $this->db->get($this->_addressbook)->row_array();
    }
    return false;
  }

  public function add_chat_to_workroom(array $data)
  {
    if(!empty($data)){
      $this->db->insert($this->_courier_workroom, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function add_chat_to_chatroom(array $data)
  {
    if(!empty($data)){
      $this->db->insert($this->_driver_chats, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_unit_name($unit_id=0)
  {
    if($unit_id > 0) {
      $this->db->where('unit_id', $unit_id);
      $row = $this->db->get($this->_unit_master)->row_array();
      return $row['shortname'];
    }
  }

  public function get_vehicle_type_name($vehical_type_id=0)
  {
    if($vehical_type_id > 0) {
      $this->db->where('vehical_type_id', $vehical_type_id);
      $row = $this->db->get($this->_transport_vehicle_master)->row_array();
      return $row['vehicle_type'];
    }
  }

  public function get_dimension_type_name($dimension_id=0)
  {
    if($dimension_id > 0) {
      $this->db->where('dimension_id', $dimension_id);
      $row = $this->db->get($this->_standard_dimension)->row_array();
      return $row['dimension_type'];
    }
  }

  public function verify_deliverer_in_order_request($order_id=0, $cust_id=0)
  {
    if($order_id > 0 && $cust_id > 0) {
      $this->db->where('order_id', (int)($order_id));
      $this->db->where('deliverer_id', (int)($cust_id));
      return $this->db->get($this->_courier_order_deliverer_request)->num_rows();
    }
    return false;
  }

  public function verify_deliverer_in_order($order_id=0, $cust_id=0)
  {
    if($order_id > 0 && $cust_id > 0) {
      $this->db->where('order_id', (int)($order_id));
      $this->db->where('deliverer_id', (int)($cust_id));
      return $this->db->get($this->_courier_orders)->num_rows();
    }
    return false;
  }

  public function get_driver_assigned_orders($cd_id=0)
  {
    if($cd_id > 0) {
      $this->db->where('driver_id', $cd_id);
      $this->db->where('driver_status_update', 'assign');
      return $this->db->get($this->_courier_orders)->num_rows();
      
    }
  }

  public function deliverer_order_list($cust_id=0, $order_status=null)
  {
    if($cust_id > 0 && !is_null($order_status)) {

      if ($order_status == 'accept') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      } 
      if ($order_status == 'assign' || $order_status == 'in_progress') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        $this->db->or_where('order_status', 'in_progress');
        $this->db->where('deliverer_id', (int)($cust_id));
      }
      if ($order_status == 'delivered') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }
      if ($order_status == 'rejected') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }
      
      $this->db->order_by('order_id', 'DESC');
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }

  public function deliverer_order_list_filtered($cust_id=0, $order_status=null, $country_id_src=0, $state_id_src=0, $city_id_src=0, $country_id_dest=0, $state_id_dest=0, $city_id_dest=0, $from_address="NULL", $to_address="NULL", $delivery_start="NULL", $delivery_end="NULL", $pickup_start="NULL", $pickup_end="NULL", $price=0, $dimension_id=0, $order_type="NULL")
  {
    if($cust_id > 0 && !is_null($order_status)) {

      if ($order_status == 'accept') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      } 
      if ($order_status == 'assign' || $order_status == 'in_progress') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        $this->db->or_where('order_status', 'in_progress');
        $this->db->where('deliverer_id', (int)($cust_id));
      }
      if ($order_status == 'delivered') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }
      if ($order_status == 'rejected') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }
      
      //Source Country State City Filter
      if($country_id_src > 0) { $this->db->where('from_country_id', (int)($country_id_src)); }
      if($state_id_src > 0) { $this->db->where('from_state_id', (int)($state_id_src)); }
      if($city_id_src > 0) { $this->db->where('from_city_id', (int)($city_id_src)); }
      //Destination Country State City Filter
      if($country_id_dest > 0) { $this->db->where('to_country_id', (int)($country_id_dest)); }
      if($state_id_dest > 0) { $this->db->where('to_state_id', (int)($state_id_dest)); }
      if($city_id_dest > 0) { $this->db->where('to_city_id', (int)($city_id_dest)); }
      //Address Filter
      if(trim($from_address) != "NULL" && trim($from_address) != "") { $this->db->like('from_address', ($from_address), 'BOTH'); }
      if(trim($to_address) != "NULL" && trim($to_address) != "") { $this->db->like('to_address', ($to_address), 'BOTH'); }
      //Delivery Date Filter
      if(trim($delivery_start) != "NULL" && trim($delivery_start) != "" && trim($delivery_end) != "NULL" && trim($delivery_end) != "") { $this->db->where('delivery_datetime BETWEEN "'. date('Y-m-d', strtotime($delivery_start)). '" AND "'. date('Y-m-d', strtotime($delivery_end)).'"'); }
      if(trim($pickup_start) != "NULL" && trim($pickup_start) != "" && trim($pickup_end) != "NULL" && trim($pickup_end)) { $this->db->where('pickup_datetime BETWEEN "'. date('Y-m-d', strtotime($pickup_start)). '" AND "'. date('Y-m-d', strtotime($pickup_end)).'"'); }
      //Price Filter
      if($price > 0) { $this->db->where('order_price <='. $price); }
      //Dimension Filter
      if($dimension_id > 0) { $this->db->where('dimension_id='. $dimension_id); }
      //Order Type Filter
      if(trim($order_type) != "NULL") { $this->db->where('order_type ', $order_type); }

      $this->db->order_by('order_id', 'DESC');
      return $this->db->get($this->_courier_orders)->result_array();
      //$this->db->get($this->_courier_orders)->result_array();
      //var_dump($this->db->last_query()); die();
    }
    return false;
  }

  public function get_booking_list($cust_id=0, $status=null)  {   
    if($cust_id > 0) {
      $this->db->where('cust_id', $cust_id);            
      if($status != null AND !empty($status)){  $this->db->where('order_status', strtolower($status));  }
      $this->db->order_by('order_id', 'desc');      
      return $this->db->get($this->_courier_orders)->result_array();    
    }   
    return false; 
  }

        public function get_order_list($cust_id=0, $status=null)  {   
    if($cust_id > 0) {
      $this->db->where('deliverer_id', $cust_id);           
      if($status != null AND !empty($status)){  $this->db->where('order_status', strtolower($status));  }
      $this->db->order_by('order_id', 'desc');      
      return $this->db->get($this->_courier_orders)->result_array();    
    }   
    return false; 
  }


  public function get_driver_delivered_orders($cd_id=0)
  {
    if($cd_id > 0) {
      $this->db->where('driver_id', $cd_id);
      $this->db->where('driver_status_update', 'delivered');
      return $this->db->get($this->_courier_orders)->num_rows();
    }
  }

  public function get_driver_name_by_id($cd_id=0)
  {
    if($cd_id > 0) {
      $this->db->where('cd_id', $cd_id);
      $row = $this->db->get($this->_customer_drivers)->row_array();
      return $row['first_name'];
    }
  }

  public function get_driver_contact_by_id($cd_id=0)
  {
    if($cd_id > 0) {
      $this->db->where('cd_id', $cd_id);
      $row = $this->db->get($this->_customer_drivers)->row_array();
      return $row['mobile1'];
    }
  }

  public function in_progress_order_list($cust_id=0, $order_status=null)
  {
    if($cust_id > 0 && !is_null($order_status)) {

      if ($order_status == 'accept') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      } 
      if ($order_status == 'assign' || $order_status == 'in_progress') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        $this->db->or_where('order_status', 'in_progress');
        $this->db->where('deliverer_id', (int)($cust_id));
      }
      if ($order_status == 'delivered') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }
      if ($order_status == 'rejected') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }
      
      $this->db->order_by('order_id', 'desc');
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }

  public function in_progress_order_list_filtered($cust_id=0, $order_status=null, $country_id_src=0, $state_id_src=0, $city_id_src=0, $country_id_dest=0, $state_id_dest=0, $city_id_dest=0, $from_address="NULL", $to_address="NULL", $delivery_start="NULL", $delivery_end="NULL", $pickup_start="NULL", $pickup_end="NULL", $price=0, $dimension_id=0, $order_type="NULL")
  {
    if($cust_id > 0 && !is_null($order_status)) {

      if ($order_status == 'accept') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      } 
      if ($order_status == 'assign' || $order_status == 'in_progress') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        //Source Country State City Filter
        if($country_id_src > 0) { $this->db->where('from_country_id', (int)($country_id_src)); }
        if($state_id_src > 0) { $this->db->where('from_state_id', (int)($state_id_src)); }
        if($city_id_src > 0) { $this->db->where('from_city_id', (int)($city_id_src)); }
        //Destination Country State City Filter
        if($country_id_dest > 0) { $this->db->where('to_country_id', (int)($country_id_dest)); }
        if($state_id_dest > 0) { $this->db->where('to_state_id', (int)($state_id_dest)); }
        if($city_id_dest > 0) { $this->db->where('to_city_id', (int)($city_id_dest)); }
        //Address Filter
        if(trim($from_address) != "NULL" && trim($from_address) != "") { $this->db->like('from_address', ($from_address), 'BOTH'); }
        if(trim($to_address) != "NULL" && trim($to_address) != "") { $this->db->like('to_address', ($to_address), 'BOTH'); }
        //Delivery Date Filter
        if(trim($delivery_start) != "NULL" && trim($delivery_start) != "" && trim($delivery_end) != "NULL" && trim($delivery_end) != "") { $this->db->where('delivery_datetime BETWEEN "'. date('Y-m-d', strtotime($delivery_start)). '" AND "'. date('Y-m-d', strtotime($delivery_end)).'"'); }
        if(trim($pickup_start) != "NULL" && trim($pickup_start) != "" && trim($pickup_end) != "NULL" && trim($pickup_end)) { $this->db->where('pickup_datetime BETWEEN "'. date('Y-m-d', strtotime($pickup_start)). '" AND "'. date('Y-m-d', strtotime($pickup_end)).'"'); }
        //Price Filter
        if($price > 0) { $this->db->where('order_price <='. $price); }
        //Dimension Filter
        if($dimension_id > 0) { $this->db->where('dimension_id='. $dimension_id); }
        //Order Type Filter
        if(trim($order_type) != "NULL") { $this->db->where('order_type ', $order_type); }

        $this->db->or_where('order_status', 'assign');
        $this->db->where('deliverer_id', (int)($cust_id));
        //Source Country State City Filter
        if($country_id_src > 0) { $this->db->where('from_country_id', (int)($country_id_src)); }
        if($state_id_src > 0) { $this->db->where('from_state_id', (int)($state_id_src)); }
        if($city_id_src > 0) { $this->db->where('from_city_id', (int)($city_id_src)); }
        //Destination Country State City Filter
        if($country_id_dest > 0) { $this->db->where('to_country_id', (int)($country_id_dest)); }
        if($state_id_dest > 0) { $this->db->where('to_state_id', (int)($state_id_dest)); }
        if($city_id_dest > 0) { $this->db->where('to_city_id', (int)($city_id_dest)); }
        //Address Filter
        if(trim($from_address) != "NULL" && trim($from_address) != "") { $this->db->like('from_address', ($from_address), 'BOTH'); }
        if(trim($to_address) != "NULL" && trim($to_address) != "") { $this->db->like('to_address', ($to_address), 'BOTH'); }
        //Delivery Date Filter
        if(trim($delivery_start) != "NULL" && trim($delivery_start) != "" && trim($delivery_end) != "NULL" && trim($delivery_end) != "") { $this->db->where('delivery_datetime BETWEEN "'. date('Y-m-d', strtotime($delivery_start)). '" AND "'. date('Y-m-d', strtotime($delivery_end)).'"'); }
        if(trim($pickup_start) != "NULL" && trim($pickup_start) != "" && trim($pickup_end) != "NULL" && trim($pickup_end)) { $this->db->where('pickup_datetime BETWEEN "'. date('Y-m-d', strtotime($pickup_start)). '" AND "'. date('Y-m-d', strtotime($pickup_end)).'"'); }
        //Price Filter
        if($price > 0) { $this->db->where('order_price <='. $price); }
        //Dimension Filter
        if($dimension_id > 0) { $this->db->where('dimension_id='. $dimension_id); }
        //Order Type Filter
        if(trim($order_type) != "NULL") { $this->db->where('order_type ', $order_type); }
      }
      if ($order_status == 'delivered') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }
      if ($order_status == 'rejected') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }

      $this->db->order_by('order_id', 'desc');
      return $this->db->get($this->_courier_orders)->result_array();
      //$this->db->get($this->_courier_orders)->result_array();
      //var_dump($this->db->last_query()); die();
    }
    return false;
  }

  public function delivered_order_list($cust_id=0, $order_status=null)
  {
    if($cust_id > 0 && !is_null($order_status)) {
      $this->db->where('deliverer_id', (int)($cust_id));
      $this->db->where('order_status', $order_status);    
      $this->db->order_by('order_id', 'desc');
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }

  public function delivered_order_list_filtered($cust_id=0, $order_status=null, $country_id_src=0, $state_id_src=0, $city_id_src=0, $country_id_dest=0, $state_id_dest=0, $city_id_dest=0, $from_address="NULL", $to_address="NULL", $delivery_start="NULL", $delivery_end="NULL", $pickup_start="NULL", $pickup_end="NULL", $price=0, $dimension_id=0, $order_type="NULL")
  {
    if($cust_id > 0 && !is_null($order_status)) {

      if ($order_status == 'accept') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      } 
      if ($order_status == 'assign' || $order_status == 'in_progress') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
        //Source Country State City Filter
        if($country_id_src > 0) { $this->db->where('from_country_id', (int)($country_id_src)); }
        if($state_id_src > 0) { $this->db->where('from_state_id', (int)($state_id_src)); }
        if($city_id_src > 0) { $this->db->where('from_city_id', (int)($city_id_src)); }
        //Destination Country State City Filter
        if($country_id_dest > 0) { $this->db->where('to_country_id', (int)($country_id_dest)); }
        if($state_id_dest > 0) { $this->db->where('to_state_id', (int)($state_id_dest)); }
        if($city_id_dest > 0) { $this->db->where('to_city_id', (int)($city_id_dest)); }
        //Address Filter
        if(trim($from_address) != "NULL" && trim($from_address) != "") { $this->db->like('from_address', ($from_address), 'BOTH'); }
        if(trim($to_address) != "NULL" && trim($to_address) != "") { $this->db->like('to_address', ($to_address), 'BOTH'); }
        //Delivery Date Filter
        if(trim($delivery_start) != "NULL" && trim($delivery_start) != "" && trim($delivery_end) != "NULL" && trim($delivery_end) != "") { $this->db->where('delivery_datetime BETWEEN "'. date('Y-m-d', strtotime($delivery_start)). '" AND "'. date('Y-m-d', strtotime($delivery_end)).'"'); }
        if(trim($pickup_start) != "NULL" && trim($pickup_start) != "" && trim($pickup_end) != "NULL" && trim($pickup_end)) { $this->db->where('pickup_datetime BETWEEN "'. date('Y-m-d', strtotime($pickup_start)). '" AND "'. date('Y-m-d', strtotime($pickup_end)).'"'); }
        //Price Filter
        if($price > 0) { $this->db->where('order_price <='. $price); }
        //Dimension Filter
        if($dimension_id > 0) { $this->db->where('dimension_id='. $dimension_id); }
        //Order Type Filter
        if(trim($order_type) != "NULL") { $this->db->where('order_type ', $order_type); }

        $this->db->or_where('order_status', 'assign');
        $this->db->where('deliverer_id', (int)($cust_id));
        //Source Country State City Filter
        if($country_id_src > 0) { $this->db->where('from_country_id', (int)($country_id_src)); }
        if($state_id_src > 0) { $this->db->where('from_state_id', (int)($state_id_src)); }
        if($city_id_src > 0) { $this->db->where('from_city_id', (int)($city_id_src)); }
        //Destination Country State City Filter
        if($country_id_dest > 0) { $this->db->where('to_country_id', (int)($country_id_dest)); }
        if($state_id_dest > 0) { $this->db->where('to_state_id', (int)($state_id_dest)); }
        if($city_id_dest > 0) { $this->db->where('to_city_id', (int)($city_id_dest)); }
        //Address Filter
        if(trim($from_address) != "NULL" && trim($from_address) != "") { $this->db->like('from_address', ($from_address), 'BOTH'); }
        if(trim($to_address) != "NULL" && trim($to_address) != "") { $this->db->like('to_address', ($to_address), 'BOTH'); }
        //Delivery Date Filter
        if(trim($delivery_start) != "NULL" && trim($delivery_start) != "" && trim($delivery_end) != "NULL" && trim($delivery_end) != "") { $this->db->where('delivery_datetime BETWEEN "'. date('Y-m-d', strtotime($delivery_start)). '" AND "'. date('Y-m-d', strtotime($delivery_end)).'"'); }
        if(trim($pickup_start) != "NULL" && trim($pickup_start) != "" && trim($pickup_end) != "NULL" && trim($pickup_end)) { $this->db->where('pickup_datetime BETWEEN "'. date('Y-m-d', strtotime($pickup_start)). '" AND "'. date('Y-m-d', strtotime($pickup_end)).'"'); }
        //Price Filter
        if($price > 0) { $this->db->where('order_price <='. $price); }
        //Dimension Filter
        if($dimension_id > 0) { $this->db->where('dimension_id='. $dimension_id); }
        //Order Type Filter
        if(trim($order_type) != "NULL") { $this->db->where('order_type ', $order_type); }
      }
      if ($order_status == 'delivered') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }
      if ($order_status == 'rejected') {
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('order_status', $order_status);
      }
      
      

      $this->db->order_by('order_id', 'desc');
      return $this->db->get($this->_courier_orders)->result_array();
      //$this->db->get($this->_courier_orders)->result_array();
      //var_dump($this->db->last_query()); die();
    }
    return false;
  }

  public function open_booking_list($cust_id=0, $order_status=null, $user_type="cust")
  {
    if($cust_id > 0 && !is_null($order_status)) {
      $this->db->where($user_type.'_id !=', (int)($cust_id));
      $this->db->where('order_status', $order_status);
      $this->db->where('payment_mode !=', 'NULL');    
      $this->db->order_by('order_id', 'desc');
      return $this->db->get($this->_courier_orders)->result_array();
      //$this->db->get($this->_courier_orders)->result_array();
      //echo $this->db->last_query(); die();
    }
    return false;
  }

  public function open_booking_list_filtered($cust_id=0, $order_status=null, $country_id_src=0, $state_id_src=0, $city_id_src=0, $country_id_dest=0, $state_id_dest=0, $city_id_dest=0, $from_address="NULL", $to_address="NULL", $delivery_start="NULL", $delivery_end="NULL", $pickup_start="NULL", $pickup_end="NULL", $price=0, $dimension_id=0, $order_type="NULL")
  {
    if($cust_id > 0 && !is_null($order_status)) {
      $this->db->where('deliverer_id', (int)($cust_id));
      $this->db->where('cust_id !=', (int)($cust_id));
      $this->db->where('order_status', $order_status);
      //Source Country State City Filter
      if($country_id_src > 0) { $this->db->where('from_country_id', (int)($country_id_src)); }
      if($state_id_src > 0) { $this->db->where('from_state_id', (int)($state_id_src)); }
      if($city_id_src > 0) { $this->db->where('from_city_id', (int)($city_id_src)); }
      //Destination Country State City Filter
      if($country_id_dest > 0) { $this->db->where('to_country_id', (int)($country_id_dest)); }
      if($state_id_dest > 0) { $this->db->where('to_state_id', (int)($state_id_dest)); }
      if($city_id_dest > 0) { $this->db->where('to_city_id', (int)($city_id_dest)); }
      //Address Filter
      if(trim($from_address) != "NULL" && trim($from_address) != "") { $this->db->like('from_address', ($from_address), 'BOTH'); }
      if(trim($to_address) != "NULL" && trim($to_address) != "") { $this->db->like('to_address', ($to_address), 'BOTH'); }
      //Delivery Date Filter
      if(trim($delivery_start) != "NULL" && trim($delivery_start) != "" && trim($delivery_end) != "NULL" && trim($delivery_end) != "") { $this->db->where('delivery_datetime BETWEEN "'. date('Y-m-d', strtotime($delivery_start)). '" AND "'. date('Y-m-d', strtotime($delivery_end)).'"'); }
      if(trim($pickup_start) != "NULL" && trim($pickup_start) != "" && trim($pickup_end) != "NULL" && trim($pickup_end)) { $this->db->where('pickup_datetime BETWEEN "'. date('Y-m-d', strtotime($pickup_start)). '" AND "'. date('Y-m-d', strtotime($pickup_end)).'"'); }
      //Price Filter
      if($price > 0) { $this->db->where('order_price <='. $price); }
      //Dimension Filter
      if($dimension_id > 0) { $this->db->where('dimension_id='. $dimension_id); }
      //Order Type Filter
      if(trim($order_type) != "NULL") { $this->db->where('order_type ', $order_type); }

      $this->db->or_where('order_status', 'assign');
      $this->db->where('deliverer_id', (int)($cust_id));
      //Source Country State City Filter
      if($country_id_src > 0) { $this->db->where('from_country_id', (int)($country_id_src)); }
      if($state_id_src > 0) { $this->db->where('from_state_id', (int)($state_id_src)); }
      if($city_id_src > 0) { $this->db->where('from_city_id', (int)($city_id_src)); }
      //Destination Country State City Filter
      if($country_id_dest > 0) { $this->db->where('to_country_id', (int)($country_id_dest)); }
      if($state_id_dest > 0) { $this->db->where('to_state_id', (int)($state_id_dest)); }
      if($city_id_dest > 0) { $this->db->where('to_city_id', (int)($city_id_dest)); }
      //Address Filter
      if(trim($from_address) != "NULL" && trim($from_address) != "") { $this->db->like('from_address', ($from_address), 'BOTH'); }
      if(trim($to_address) != "NULL" && trim($to_address) != "") { $this->db->like('to_address', ($to_address), 'BOTH'); }
      //Delivery Date Filter
      if(trim($delivery_start) != "NULL" && trim($delivery_start) != "" && trim($delivery_end) != "NULL" && trim($delivery_end) != "") { $this->db->where('delivery_datetime BETWEEN "'. date('Y-m-d', strtotime($delivery_start)). '" AND "'. date('Y-m-d', strtotime($delivery_end)).'"'); }
      if(trim($pickup_start) != "NULL" && trim($pickup_start) != "" && trim($pickup_end) != "NULL" && trim($pickup_end)) { $this->db->where('pickup_datetime BETWEEN "'. date('Y-m-d', strtotime($pickup_start)). '" AND "'. date('Y-m-d', strtotime($pickup_end)).'"'); }
      //Price Filter
      if($price > 0) { $this->db->where('order_price <='. $price); }
      //Dimension Filter
      if($dimension_id > 0) { $this->db->where('dimension_id='. $dimension_id); }
      //Order Type Filter
      if(trim($order_type) != "NULL") { $this->db->where('order_type ', $order_type); }
            
      $this->db->where('complete_paid',1);
      $this->db->order_by('order_id', 'desc');
      return $this->db->get($this->_courier_orders)->result_array();
      //$this->db->get($this->_courier_orders)->result_array();
      //var_dump($this->db->last_query()); die();
    }
    return false;
  }

  public function get_customer_account_history($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->where('user_id', (int)$cust_id);
      $this->db->limit(300);
      return $this->db->get($this->_user_account_history)->result_array();
    }
    return false;
  }

  public function get_customer_scrow_history($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->where('deliverer_id', (int)$cust_id);
      $this->db->limit(300);
      return $this->db->get($this->_deliverer_scrow_history)->result_array();
    }
    return false;
  }

  public function get_customer_account_withdraw_request_list($cust_id=0, $response=null)
  {
    if($cust_id > 0 && !is_null($response)) {
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('response', trim($response));
      $this->db->order_by('req_id', 'desc');
      $this->db->limit(300);
      return $this->db->get($this->_account_withdraw_request)->result_array();
    }
    return false;
  }

  public function get_customer_account_withdraw_list($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->where('user_id', (int)$cust_id);
      $this->db->order_by('req_id', 'desc');
      $this->db->limit(300);
      return $this->db->get($this->_account_withdraw_request)->result_array();
    }
    return false;
  }

  public function withdrawal_request_details($req_id=0)
  {
    if($req_id > 0) {
      $this->db->where('req_id', (int)$req_id);
      return $this->db->get($this->_account_withdraw_request)->row_array();
    }
    return false;
  }

  public function get_deliverer_accounts($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->select('iban, mobile_money_acc1, mobile_money_acc2');
      $this->db->where('cust_id', (int)$cust_id);
      return $this->db->get($this->_deliverer_profile)->row_array();
    }
    return false;
  }

  public function check_user_account_balance($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->where('user_id', (int)$cust_id);
      return $this->db->get($this->_user_account_master)->result_array();
    }
    return false;
  }

  public function get_customer_account_withdraw_request_sum($cust_id=0, $response=null)
  {
    if($cust_id > 0 && !is_null($response)) {
      $this->db->select('SUM(amount) AS amount, currency_code');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('response', trim($response));
      $this->db->group_by('currency_code');
      return $this->db->get($this->_account_withdraw_request)->result_array();
    }
    return false;
  }

  public function get_customer_account_withdraw_request_sum_currencywise($cust_id=0, $response=null, $currency_code=null)
  {
    if($cust_id > 0 && !is_null($response) && !is_null($currency_code)) {
      $this->db->select('SUM(amount) AS amount');
      $this->db->where('user_id', (int)$cust_id);
      $this->db->where('response', trim($response));
      $this->db->where('currency_code', trim($currency_code));
      return $this->db->get($this->_account_withdraw_request)->row_array();
    }
    return false;
  }

  public function check_user_account_balance_by_id($account_id=0)
  {
    if($account_id > 0) {
      $this->db->where('account_id', (int)$account_id);
      return $this->db->get($this->_user_account_master)->row_array();
    }
    return false;
  }

  public function filtered_list(array $data)
  { //var_dump($data); die();
    if($data['user_id'] > 0 AND !is_null($data['order_status'])) {
      // User type filter
      if($data['user_type'] == 'cust') {
        $this->db->where('cust_id', $data['user_id']);
        if($data['order_status'] != 'open') { $this->db->where('payment_mode !=', 'NULL'); }
      }
      if($data['user_type'] == 'deliverer') {
        $this->db->where('cust_id !=', $data['user_id']);
        if($data['order_status'] == 'open') { $this->db->where('payment_mode !=', 'NULL'); }
        if($data['order_status'] != 'open') { $this->db->where('deliverer_id', $data['user_id']); }
      }
      // order status filter
      $this->db->where('order_status', $data['order_status']);
      // From country, state, city filter
      if($data['from_country_id'] > 0 ) { $this->db->where('from_country_id', (int) $data['from_country_id']);  }
      if($data['from_state_id'] > 0 ) { $this->db->where('from_state_id', (int) $data['from_state_id']);  }
      if($data['from_city_id'] > 0 ) {  $this->db->where('from_city_id', (int) $data['from_city_id']);  }
      // To Country, state, city filter
      if($data['to_country_id'] > 0 ) { $this->db->where('to_country_id', (int) $data['to_country_id']);  }
      if($data['to_state_id'] > 0 ) { $this->db->where('to_state_id', (int) $data['to_state_id']);  }
      if($data['to_city_id'] > 0 ) {  $this->db->where('to_city_id', (int) $data['to_city_id']);  }
      // From address
      if($data['from_address'] !="NULL" ) { $this->db->like('from_address', trim($data['from_address']), 'BOTH'); }
      // To address
      if($data['to_address'] !="NULL" ) { $this->db->like('to_address', trim($data['to_address']), 'BOTH'); }
      // Date filter
      if(trim($data['delivery_start_date']) !="NULL" AND trim($data['delivery_start_date']) !="" AND trim($data['delivery_end_date']) !="NULL" AND trim($data['delivery_end_date']) !="" ) {  $this->db->where('delivery_datetime BETWEEN "'. $data['delivery_start_date']. '" AND "'. $data['delivery_end_date'].'"'); }
      if(trim($data['pickup_start_date']) !="NULL" AND trim($data['pickup_start_date']) !="" AND trim($data['pickup_end_date']) !="NULL" AND trim($data['pickup_end_date']) !="" ) {  $this->db->where('delivery_datetime BETWEEN "'. $data['pickup_start_date']. '" AND "'. $data['pickup_end_date'].'"'); }
      // Price Filter
      if($data['price'] > 0) { $this->db->where('order_price <='. $data['price']); }
      //Dimension Filter
      if($data['dimension_id'] > 0) { $this->db->where('dimension_id='. $data['dimension_id']); }
      //Order Type Filter
      if(trim($data['order_type']) != "NULL") { $this->db->where('order_type ', $data['order_type']); }

      //Creation Date Filter
      if(trim($data['creation_start_date']) !="NULL" AND trim($data['creation_start_date']) !="" AND trim($data['creation_end_date']) !="NULL" AND trim($data['creation_end_date']) !="" ) {  $this->db->where('cre_datetime BETWEEN "'. $data['creation_start_date']. '" AND "'. $data['creation_end_date'].'"');  }
      //Expirty Date  Filter
      if(trim($data['expiry_start_date']) !="NULL" AND trim($data['expiry_start_date']) !="" AND trim($data['expiry_end_date']) !="NULL" AND trim($data['expiry_end_date']) !="" ) {  $this->db->where('expiry_date  BETWEEN "'. $data['expiry_start_date']. '" AND "'. $data['expiry_end_date'].'"');  }
      //Distance Filter
      if($data['distance_start'] > 0 AND $data['distance_end'] > 0 ) {  $this->db->where('distance_in_km BETWEEN "'. $data['distance_start']. '" AND "'. $data['distance_end'].'"');  }
      // Transport type Filter
      if($data['transport_type'] != "NULL") { $this->db->where('transport_type', $data['transport_type']); }
      // Vehicle Filter
      if($data['vehicle_id'] > 0) { $this->db->where('vehical_type_id', $data['vehicle_id']); }
      // Vehicle Filter
      if($data['max_weight'] > 0) { $this->db->where('total_weight <='. $data['max_weight']); }
      if($data['unit_id'] > 0) { $this->db->where('unit_id', $data['unit_id']); }
      if($data['cat_id'] > 0) { $this->db->where('category_id', $data['cat_id']); }

      $this->db->order_by('order_id', 'desc'); 
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }

    public function filtered_list_market_place(array $data)
    { //var_dump($data); die();
        if($data['user_id'] > 0 AND !is_null($data['order_status'])) {
          // User type filter
    
          $this->db->where('cust_id !=', $data['user_id']);
          $this->db->where('complete_paid',1); 
          // order status filter
          $this->db->where('order_status', 'open');
          // From country, state, city filter
          if($data['from_country_id'] > 0 ) { $this->db->where('from_country_id', (int) $data['from_country_id']);  }
          if($data['from_state_id'] > 0 ) { $this->db->where('from_state_id', (int) $data['from_state_id']);  }
          if($data['from_city_id'] > 0 ) {  $this->db->where('from_city_id', (int) $data['from_city_id']);  }
          // To Country, state, city filter
          if($data['to_country_id'] > 0 ) { $this->db->where('to_country_id', (int) $data['to_country_id']);  }
          if($data['to_state_id'] > 0 ) { $this->db->where('to_state_id', (int) $data['to_state_id']);  }
          if($data['to_city_id'] > 0 ) {  $this->db->where('to_city_id', (int) $data['to_city_id']);  }
          // From address
          if($data['from_address'] !="NULL" ) { $this->db->like('from_address', trim($data['from_address']), 'BOTH'); }
          // To address
          if($data['to_address'] !="NULL" ) { $this->db->like('to_address', trim($data['to_address']), 'BOTH'); }
          // Date filter
          if(trim($data['delivery_start_date']) !="NULL" AND trim($data['delivery_start_date']) !="" AND trim($data['delivery_end_date']) !="NULL" AND trim($data['delivery_end_date']) !="" ) {  $this->db->where('delivery_datetime BETWEEN "'. $data['delivery_start_date']. '" AND "'. $data['delivery_end_date'].'"'); }
          if(trim($data['pickup_start_date']) !="NULL" AND trim($data['pickup_start_date']) !="" AND trim($data['pickup_end_date']) !="NULL" AND trim($data['pickup_end_date']) !="" ) {  $this->db->where('delivery_datetime BETWEEN "'. $data['pickup_start_date']. '" AND "'. $data['pickup_end_date'].'"'); }
          // Price Filter
          if($data['price'] > 0) { $this->db->where('order_price <='. $data['price']); }
          //Dimension Filter
          if($data['dimension_id'] > 0) { $this->db->where('dimension_id='. $data['dimension_id']); }
          //Order Type Filter
          if(trim($data['order_type']) != "NULL") { $this->db->where('order_type ', $data['order_type']); }
    
          //Creation Date Filter
          if(trim($data['creation_start_date']) !="NULL" AND trim($data['creation_start_date']) !="" AND trim($data['creation_end_date']) !="NULL" AND trim($data['creation_end_date']) !="" ) {  $this->db->where('cre_datetime BETWEEN "'. $data['creation_start_date']. '" AND "'. $data['creation_end_date'].'"');  }
          //Expirty Date  Filter
          if(trim($data['expiry_start_date']) !="NULL" AND trim($data['expiry_start_date']) !="" AND trim($data['expiry_end_date']) !="NULL" AND trim($data['expiry_end_date']) !="" ) {  $this->db->where('expiry_date  BETWEEN "'. $data['expiry_start_date']. '" AND "'. $data['expiry_end_date'].'"');  }
          //Distance Filter
          if($data['distance_start'] > 0 AND $data['distance_end'] > 0 ) {  $this->db->where('distance_in_km BETWEEN "'. $data['distance_start']. '" AND "'. $data['distance_end'].'"');  }
          // Transport type Filter
          if($data['transport_type'] != "NULL") { $this->db->where('transport_type', $data['transport_type']); }
          // Vehicle Filter
          if($data['vehicle_id'] > 0) { $this->db->where('vehical_type_id', $data['vehicle_id']); }
          // Vehicle Filter
          if($data['max_weight'] > 0) { $this->db->where('total_weight <='. $data['max_weight']); }
          if($data['unit_id'] > 0) { $this->db->where('unit_id', $data['unit_id']); }
          if($data['cat_id'] > 0) { $this->db->where('category_id', $data['cat_id']); }
    
          $this->db->order_by('order_id', 'desc'); 
          return $this->db->get($this->_courier_orders)->result_array();
        }
        return false;
    }

  public function get_requested_deliverers($cust_id=0, $order_id=0) 
  {
    if($cust_id > 0 AND $order_id > 0) {    
       $qry = "SELECT d.*, c.ratings FROM tbl_deliverer_profile AS d JOIN tbl_customers_master AS c ON d.cust_id = c.cust_id WHERE d.deliverer_id IN ( SELECT o.deliverer_id FROM tbl_courier_order_deliverer_request AS o WHERE o.order_id = ".(int) $order_id.") AND d.deliverer_id != ".(int) $cust_id; 
      return $this->db->query($qry)->result_array();
    }
    return false;
  }

  public function is_request_sent_to_deliverer(array $data)
  {
    $ids = array();
    if(is_array($data)){
      $this->db->select('request_id');
      $this->db->where('cust_id', (int)$data['cust_id']);
      $this->db->where('deliverer_id', (int)$data['deliverer_id']);
      $this->db->where('order_id', (int)$data['order_id']);
      return $this->db->get($this->_courier_order_deliverer_request)->num_rows();           
    }
    return $ids;
  }

  public function get_statuswise_order_counts($id=0, $status=null)
  {
    if($id > 0 AND $status != null) {
      $this->db->where('deliverer_id', (int) $id);
      $this->db->where('order_status', strtolower($status));
      return $this->db->get($this->_courier_orders)->num_rows();
    }
    return false;
  }

  public function get_favourite_deliverers_ids($cust_id=0)
  {
    $ids = array();
    if($cust_id > 0 ) {
      $this->db->select('deliverer_id');
      $this->db->where('cust_id', (int)$cust_id);
      $return = $this->db->get($this->_favourite_deliverers)->result_array();     
      foreach ($return as $r){ $ids[] = $r['deliverer_id']; }
    }
    return $ids;
  }

  public function find_deliverer_against_order($order_id=0, $login_cust_id=0, array $data)
  {
    //echo var_dump($order_id); die();
    if($order_id > 0 AND $login_cust_id > 0){
      $qry = "SELECT d.*, c.ratings FROM tbl_deliverer_profile AS d JOIN tbl_customers_master AS c ON d.cust_id = c.cust_id WHERE d.cust_id NOT IN ( SELECT deliverer_id FROM tbl_courier_order_deliverer_request WHERE order_id = ".(int) $order_id." ) AND  d.cust_id != ". (int) $login_cust_id." AND c.acc_type IN ('seller','both') AND c.is_deliverer = 1";
      if($data['country_id'] > 0 ) { $qry .= " AND d.country_id = ". (int) $data['country_id']; }
      if($data['state_id'] > 0 ) { $qry .= " AND d.state_id = ". (int) $data['state_id']; }
      if($data['city_id'] > 0 ) { $qry .= " AND d.city_id = ". (int) $data['city_id']; }
      if($data['rating'] > 0 ) { $qry .= " AND ROUND(c.ratings) = ". (int) $data['rating']; }
      //$this->db->where("c.acc_type IN ('seller','both')");
      //$this->db->where('c.is_deliverer', 1);
      //echo $qry; die();
      return $this->db->query($qry)->result_array();
      $this->db->query($qry)->result_array();
      var_dump($this->db->last_query()); die();
    }
    return false;
  }

  public function verify_deliverer_document($cust_id=0, $doc_type=null)
  {
    if($cust_id > 0 AND !empty($doc_type) AND $doc_type != null) {
      $this->db->select('is_verified, is_rejected');
      $this->db->where('cust_id', (int) $cust_id);
      $this->db->where('doc_type', trim($doc_type));
      
      return $this->db->get($this->_deliverer_documents)->row_array();
    } 
    return false;
  }

  public function is_deliverer_delivered($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('order_status', 'delivered');
      $this->db->where('deliverer_id', (int) $cust_id);
      return $this->db->get($this->_courier_orders)->num_rows();
    }
    return false;
  }

  public function create_deliverer_profile(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_deliverer_profile, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_order_status($cd_id=0, $order_id=0, $order_status=null)
  {
    if($cd_id != null && $order_id != null && !is_null($order_status)) {
      $this->db->where('order_id', trim($order_id));
      if(trim($order_status) == 'delivered') {
        $this->db->update($this->_courier_orders, ['order_status' => $order_status, 'status_updated_by' => 'deliverer', 'status_update_datetime' => date('Y-m-d H:i:s'), 'delivered_datetime' => date('Y-m-d H:i:s'), 'driver_status_update' => 'delivered']);
      }
      if(trim($order_status) == 'in_progress') {
        $this->db->update($this->_courier_orders, ['order_status' => 'in_progress', 'status_updated_by' => 'deliverer', 'status_update_datetime' => date('Y-m-d H:i:s'), 'driver_status_update' => 'in_progress']);
      }
      if(trim($order_status) == 'accept') {
        $this->db->update($this->_courier_orders, ['order_status' => 'in_progress', 'status_updated_by' => 'deliverer', 'status_update_datetime' => date('Y-m-d H:i:s'), 'assigned_accept_datetime' => date('Y-m-d H:i:s'), 'driver_status_update' => 'accept']); 
      }
      if(trim($order_status) == 'reject') {
        $this->db->update($this->_courier_orders, ['driver_id' => 'NULL', 'cd_name' => 'NULL', 'assigned_datetime' => 'NULL', 'assigned_accept_datetime' => 'NULL', 'driver_status_update' => 'reject']);
      }
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_user_order_invoices($cust_id=0)
  {
    if($cust_id > 0) {
      $this->db->select('order_id, invoice_url, cust_id, deliverer_id, from_address, to_address, distance_in_km, pickup_datetime, order_price, order_picture_url, delivered_datetime, rating, currency_sign, cust_name, deliverer_name, deliverer_company_name, deliverer_contact_name, from_relay_id, to_relay_id');

      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('invoice_url !=', 'NULL');

      $this->db->or_where('deliverer_id', (int)$cust_id);
      $this->db->where('invoice_url !=', 'NULL');

      $this->db->limit(300);
      $this->db->order_by('order_id', 'desc');
      return $this->db->get($this->_courier_orders)->result_array();
    }
    return false;
  }
  
  public function get_total_request($id=0)
  {
    $this->db->where('deliverer_id', (int) $id);
    return $this->db->get($this->_courier_order_deliverer_request)->num_rows();
  }
  
  public function get_current_balance($cust_id=0,$currency_code=null)
  {
    if($cust_id > 0 && !empty($currency_code)){
    
      $this->db->where('user_id', (int) $cust_id);
      $this->db->where('currency_code', trim($currency_code));
      return $this->db->get('tbl_user_account_master')->row_array();
    }
    return 0;
  }

  public function get_total_spend($cust_id=0,$currency_code=null)
  {
    if($cust_id > 0 && !empty($currency_code)){
      $this->db->select_sum('amount');
      $this->db->where('user_id', (int) $cust_id);
      $this->db->where('type', 0);
      //$this->db->where('transaction_type', 'payment');
      $this->db->where('currency_code', trim($currency_code));
      return $this->db->get('tbl_user_account_history')->row_array();
    }
    return false;
  }
  
  public function get_total_earned($cust_id=0,$currency_code=null)
  {
    if($cust_id > 0 && !empty($currency_code)){
      $this->db->select_sum('amount');
      $this->db->where('user_id', (int) $cust_id);
      $this->db->where('type', 1);
      $this->db->where('transaction_type', 'add');
      $this->db->where('currency_code', trim($currency_code));
      return $this->db->get('tbl_user_account_history')->row_array();
    }
    return 0;
  }

  public function get_relay_by_location($country_id=0, $state_id=0, $city_id=0)
  {
    if($country_id > 0 || $country_id > 0 || $city_id > 0) {
      $this->db->select('r.*,c.country_name, s.state_name, ct.city_name');
      $this->db->from($this->_relay_points.' as r');
      $this->db->join($this->_country_master.' as c', "c.country_id = r.country_id");
      $this->db->join($this->_state_master.' as s', "s.state_id = r.state_id");
      $this->db->join($this->_city_master.' as ct', "ct.city_id = r.city_id");

      if($country_id > 0) { 
        $this->db->where('r.country_id', $country_id); 
      }
      else if($state_id > 0) { $this->db->where('r.state_id', $state_id); }
      else if($city_id > 0) { $this->db->where('r.city_id', $city_id); }
      
      return $this->db->get()->result_array();
    } else return false;
  }

  public function get_relay_point_by_lat_long($frm_relay_latitude=null, $frm_relay_longitude=null)
  {
    if($frm_relay_latitude != null && $frm_relay_longitude != null) {
      $this->db->select("*, SQRT(
      POW(69.1 * (latitude - $frm_relay_latitude), 2) +
      POW(69.1 * ($frm_relay_longitude - longitude) * COS(latitude / 57.3), 2)) AS distance FROM tbl_relay_points ORDER BY distance LIMIT 10");
      return $this->db->get()->result_array();
      //return $this->db->last_query();
    } else return false;
  }

  public function get_recent_relay_list($id=0)
  {
    if($id > 0 ){
      $this->db->select('f.relay_id, f.relay_status, r.*'); 
      $this->db->from($this->_customer_recent_relay_point.' as f');
      $this->db->join($this->_relay_points.' as r', 'f.relay_id = r.relay_id');
      $this->db->where('r.status', 1);
      $this->db->group_by('f.relay_id');
      $this->db->where('f.cust_id', $id);
      return $this->db->get($this->_customer_recent_relay_point)->result_array();
    }
    return false;
  }

  public function get_order_count($id=0, $status=null)
  {
    if($id > 0 && !is_null($status)) {
      $this->db->select('COUNT(order_id) as accepted_count');
      $this->db->where('driver_id', (int)$id );
      $this->db->where('driver_status_update', trim($status));
      $row = $this->db->get($this->_courier_orders)->row_array();
      if($row['accepted_count'] > 0) return $row['accepted_count']; else return 0;
    } else return 0;
  }

  public function get_inprogress_working_distance($id=0)
  {
    if($id > 0) {
      $this->db->select('SUM(distance_in_km) as distance_in_km');
      $this->db->where('driver_id', (int)$id );
      $this->db->where('driver_status_update', 'in_progress');
      $row = $this->db->get($this->_courier_orders)->row_array();
      if($row['distance_in_km'] > 0) return $row['distance_in_km']; else return 0;
    } else return 0;
  }

  public function register_new_support(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_customer_support_queries, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_support_ticket($id=0, $ticket_id=null)
  {
    if($id > 0 && !is_null($ticket_id)){
      $this->db->where('query_id', (int)$id );
      $this->db->update($this->_customer_support_queries, ['ticket_id' => trim($ticket_id)]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_support_list($id=0)
  {
    if($id > 0) {
      $this->db->where('cust_id', (int)$id );
      return $this->db->get($this->_customer_support_queries)->result_array();
    } else return false;
  }

  public function get_support_details($id=0)
  {
    if($id > 0) {
      $this->db->where('query_id', (int)$id );
      return $this->db->get($this->_customer_support_queries)->row_array();
    } else return false;
  }

  public function carrier_type_list()
  {
    return $this->db->get($this->_carrier_type)->result_array();
  }

  public function get_recent_order_reviews($id=0)
  {
    if($id > 0) {
      $this->db->where('deliverer_id', (int)$id );
      $this->db->where('order_status', 'delivered' );
      $this->db->order_by('order_id', 'desc');
      $this->db->limit(5);
      return $this->db->get($this->_courier_orders)->result_array();
    } else return false;
  }

  public function register_deliverer_document(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_deliverer_documents, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function deliverer_scrow_master_list($deliverer_id=0)
  {
    if($deliverer_id > 0) {
      $this->db->where('deliverer_id', (int)$deliverer_id);
      return $this->db->get($this->_deliverer_scrow_master)->result_array();
    }
    return false;
  }

  public function get_order_packages($order_id=0)
  {
    if($order_id > 0) {
      $this->db->where('order_id', (int)$order_id);
      return $this->db->get($this->_order_packages)->result_array();
    }
    return false;
  }

	public function get_customer_email_name_by_order_id($id=0)
	{
		if($id > 0) {
			$this->db->select('email1');
			$this->db->where('cust_id', (int)$id);
			$row = $this->db->get($this->_customer_master)->row_array();
			$email_cut = explode('@', $row['email1']);
			return ucwords($email_cut[0]);
		}
		return false;
	}

  public function order_status_list($order_id=0)
  {
    if($order_id > 0 ) {
      $this->db->where('order_id', (int)($order_id));
      $this->db->where('status !=', 'reject');
      return $this->db->get($this->_courier_order_status)->result_array();
    }
    return false;
  }

  public function check_recent_relay($from_relay_id=0, $cust_id=0)
  {
    if($from_relay_id > 0 && $cust_id > 0){
      $this->db->where('relay_id', $from_relay_id);
      $this->db->where('cust_id', $cust_id);
      return $this->db->get($this->_customer_recent_relay_point)->num_rows();
    } return false;
  }

  public function get_total_spend_service($cust_id=0, $currency_code=null, $cat_id=0)
  {
    if($cust_id > 0 && !empty($currency_code) && $cat_id > 0) {
      $this->db->select_sum('amount');
      $this->db->where('user_id', (int) $cust_id);
      $this->db->where('cat_id', (int) $cat_id);
      $this->db->where('type', 0);
      //$this->db->where('transaction_type', 'payment');
      $this->db->where('currency_code', trim($currency_code));
      return $this->db->get('tbl_user_account_history')->row_array();
    } return 0;
  }

  public function get_total_earned_service($cust_id=0, $currency_code=null, $cat_id=0)
  {
    if($cust_id > 0 && !empty($currency_code) && $cat_id > 0){
      $this->db->select_sum('amount');
      $this->db->where('user_id', (int) $cust_id);
      $this->db->where('cat_id', (int) $cat_id);
      $this->db->where('type', 1);
      $this->db->where('transaction_type', 'add');
      $this->db->where('currency_code', trim($currency_code));
      return $this->db->get('tbl_user_account_history')->row_array();
    }
    return false;
  }

}

/* End of file Web_user_model_bus.php */
/* Location: ./application/models/Web_user_model_bus.php */