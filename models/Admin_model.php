<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

  private $authorities = "tbl_authorities_master";
  private $auth_permissions = "tbl_auth_permissions";
  private $_authority_permission_master = "tbl_authority_permission_master";
  private $_gonagoo_account_master = "tbl_gonagoo_account_master";
  private $_gonagoo_account_history = "tbl_gonagoo_account_history";
  private $_account_withdraw_request = "tbl_account_withdraw_request";
  private $_courier_orders = "tbl_courier_orders";
  private $_category_master = "tbl_category_master";
  private $_insurance_master = "tbl_insurance_master";
  private $_countries = "tbl_countries";
  private $_currency_master = "tbl_currency_master";
  private $_customers_master = "tbl_customers_master";
  private $_driver_app_param_master = "driver_app_param_master";
  private $_app_param_master = "app_param_master";
  private $_bus_booking_master = "tbl_bus_booking_master";
  private $_cancelled_trips = 'tbl_cancelled_trips';
  private $_bus_trip_master = 'tbl_bus_trip_master';
  private $_laundry_booking = 'tbl_laundry_booking';
  private $_laundry_booking_details = 'tbl_laundry_booking_details';
  private $_laundry_provider_profile = 'tbl_laundry_provider_profile';
  private $_laundry_sub_category = 'tbl_laundry_sub_category';
  private $_laundry_category = 'tbl_laundry_category';
  private $_bus_operator_profile = 'tbl_bus_operator_profile';
  private $_bus_booking_seat_details = "tbl_bus_booking_seat_details";
  private $_promo_code = "tbl_promo_code";

  public function get_main_app_version()
  {
    return $this->db->get($this->_app_param_master)->row_array();
  }

  public function get_driver_app_version()
  {
    return $this->db->get($this->_driver_app_param_master)->row_array();
  }

  public function update_main_app_version($id=0, $data=null)
  {
    if(is_array($data) && $id > 0){ 
      $this->db->where('id', (int)$id);
      $this->db->update($this->_app_param_master, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_driver_app_version($id=0, $data=null)
  {
    if(is_array($data) && $id > 0){ 
      $this->db->where('id', (int)$id);
      $this->db->update($this->_driver_app_param_master, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_currencies()
  {
    $this->db->where('currency_status', 1);
    return $this->db->get($this->_currency_master)->result_array();
  }
    
  public function user_authentication($email=null, $password=null)
  {
    if(!empty($email) AND !empty($password)){
      $this->db->select('auth_id, country_id, auth_type_id');
      $this->db->where('auth_email', $email);
      $this->db->where('auth_pass', $password);
      return $this->db->get($this->authorities)->row_array();
    } 
    return false;
  }

  public function logged_in_user_details($id=0)
  {
    if($id > 0 ) {
      $this->db->select('auth_id, auth_fname as fname, auth_lname as lname, auth_email as email, auth_type_id as type_id, auth_avatar_url as avatar_url , country_id');
      $this->db->where('auth_id', $id);
      return $this->db->get($this->authorities)->row_array();
    }
    return false;
  }

  public function get_auth_permissions($id=0)
  {
    if($id > 0 ) {
      $this->db->where('auth_type_id', $id);
      return $this->db->get($this->_authority_permission_master)->result_array();
    }
    return false;
  }

  public function update_profile(array $data)
  {
    if(is_array($data) ){     
      $auth_id = $this->session->userdata('userid');
      $this->db->where('auth_id', (int)$auth_id);
      $this->db->update($this->authorities, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function update_profile_password($auth_pass=null)
  {
    if($auth_pass != null ){      
      $auth_id = $this->session->userdata('userid');
      $this->db->where('auth_id', (int)$auth_id);
      $this->db->update($this->authorities, ['auth_pass' => trim($auth_pass)]);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_gonagoo_master_details()
  {
    $this->db->order_by('currency_code','desc');
    return $this->db->get($this->_gonagoo_account_master)->result_array();
  }

  public function get_gonagoo_account_details($transaction_type=null, $from_date=0, $to_date=0, $country_id=0, $cat_id=0)
  {
    if($transaction_type != null ){
      $this->db->select('SUM(amount) as amount, currency_code');
      if($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
      if($transaction_type ==  "my_xode" ){
      $this->db->where( 'transaction_type','commission_amount');
      $this->db->or_where('transaction_type','laundry_commission');
      $this->db->or_where('transaction_type','ticket_commission');
      }else{
        $this->db->where('transaction_type', $transaction_type);
      }
      if($cat_id > 0) { $this->db->where('cat_id', (int)$cat_id); }
      if($from_date != 0 && $to_date != 0) {
          $this->db->where("DATE_FORMAT(datetime,'%Y-%m-%d') >= ", $from_date);
          $this->db->where("DATE_FORMAT(datetime,'%Y-%m-%d') <= ", $to_date);
      }
      $this->db->group_by('currency_code');
      $this->db->order_by('currency_code','desc');
      return $this->db->get($this->_gonagoo_account_history)->result_array();
      //echo $this->db->last_query();
    } else return false;
  }

  public function get_gonagoo_refund_details()
  {
    $this->db->select('SUM(amount_transferred) as amount_transferred, currency_code');
    $this->db->where('response', 'accept');
    $this->db->group_by('currency_code');
    $this->db->order_by('currency_code','desc');
    return $this->db->get($this->_account_withdraw_request)->result_array();
  }

  public function get_gonagoo_history($limit=0)
  {
    if( $limit > 0 ){ $this->db->limit((int)$limit); }
    $this->db->order_by('gh_id','desc');
    return $this->db->get($this->_gonagoo_account_history)->result_array();
  }

  public function get_gonagoo_history_by_currency($currency_code=null)
  {
    if( $currency_code != null ){ 
      $this->db->where('currency_code', trim($currency_code));
      $this->db->order_by('gh_id','desc');
      return $this->db->get($this->_gonagoo_account_history)->result_array();
    }
  }

  public function get_history_by_currency($currency_code=null, $transaction_type=null)
  {
    if( $currency_code != null && $transaction_type != null ){ 
      $this->db->where('currency_code', trim($currency_code));
      if($transaction_type != '0') { $this->db->where('transaction_type', trim($transaction_type)); }
      $this->db->order_by('gh_id','desc');
      return $this->db->get($this->_gonagoo_account_history)->result_array();
    }
  }

  public function get_currencywise_refund_history($currency_code=null, $response=null)
  {
    if( $currency_code != null && $response != null ) {
      $this->db->where('response', trim($response));
      $this->db->where('currency_code', trim($currency_code));
      $this->db->order_by('req_id','desc');
      return $this->db->get($this->_account_withdraw_request)->result_array();
    } else return false;
  }

  public function get_category_name_by_order_id($order_id=0 , $cat_id=0)
  {
    if($cat_id > 0){
      $this->db->select('cat_name');
      $this->db->where('cat_id', (int)$cat_id);
      $row = $this->db->get($this->_category_master)->row_array();
      return $row['cat_name']; die();
    }
    if( $order_id > 0 ){
      //get category id 
      $this->db->select('category_id');
      $this->db->where('order_id', (int)$order_id);
      $row = $this->db->get($this->_courier_orders)->row_array();
      $category_id = $row['category_id'];
      //get category name
      $this->db->select('cat_name');
      $this->db->where('cat_id', (int)$category_id);
      $row = $this->db->get($this->_category_master)->row_array();
      return $row['cat_name'];
    } return false;
  }

  public function get_order_status_by_order_id($order_id=0)
  {
    if( $order_id > 0 ){
      $this->db->select('order_status');
      $this->db->where('order_id', (int)$order_id);
      $row = $this->db->get($this->_courier_orders)->row_array();
      return $row['order_status'];
    } return false;
  }

  public function get_gonagoo_insurance_commission_by_order_id($order_id=0)
  {
    if( $order_id > 0 ){
      //get category id 
      $this->db->select('from_country_id, package_value, category_id');
      $this->db->where('order_id', (int)$order_id);
      $row = $this->db->get($this->_courier_orders)->row_array();
      //get category name
      $this->db->select('commission_percent');
      $this->db->where('country_id', (int)$row['from_country_id']);
      $this->db->where('category_id', (int)$row['category_id']);
      $this->db->where('min_value <=', trim($row['package_value']));
      $this->db->where('max_value >=', trim($row['package_value']));
      $row = $this->db->get($this->_insurance_master)->row_array();
      return $row['commission_percent'];
    } return false;
  }

  public function convert_big_int($int = 0)
  {
    if( $int > 1000 && $int < 1000000){ $int = ($int/1000); return number_format($int ,2,'.',',') . 'K'; }         
    else if( $int >= 1000000 ){ $int = ($int / 1000000 ); return number_format($int ,2,'.',',') . 'M'; }
    else{ return $int; }
  }
    
  public function get_user_count_with_bookings($user_type=null, $country_id=0, $cat_id=0, $rows=0, $booking_status=null)
  {
      if($user_type != null && $user_type == 'deliverer'){
        $this->db->select('COUNT(b.deliverer_id) AS userCount, b.deliverer_id, p.company_name, p.firstname, p.lastname, p.contact_no, p.country_id, p.email_id');
        $this->db->from('tbl_courier_orders AS b');
        $this->db->join('tbl_deliverer_profile AS p', 'p.cust_id = b.deliverer_id');
        
        $this->db->where('b.order_status', trim($booking_status));
      
        if($booking_status == 'in_progress') { $this->db->where('b.expiry_date < NOW()'); }
        if($country_id > 0) { $this->db->where('b.from_country_id', (int)$country_id); }
        if($cat_id > 0) { $this->db->where('b.category_id', (int)$cat_id); }
        if($rows > 0) { $this->db->limit($rows); }
        
        $this->db->group_by('b.deliverer_id');
        $this->db->order_by('userCount','desc');
        return $this->db->get()->result_array();
      } else if($user_type != null && $user_type == 'customer'){
        $this->db->select('COUNT(b.cust_id) AS userCount, b.cust_id, c.company_name, c.firstname, c.lastname, c.mobile1, c.email1, c.country_id');
        $this->db->from('tbl_courier_orders AS b');
        $this->db->join('tbl_customers_master AS c', 'c.cust_id = b.cust_id');
        
        $this->db->where('b.order_status', trim($booking_status));
        if($booking_status == 'in_progress') { $this->db->where('b.expiry_date < NOW()'); }
        
        if($country_id > 0) { $this->db->where('b.from_country_id', (int)$country_id); }
        if($cat_id > 0) { $this->db->where('b.category_id', (int)$cat_id); }
        if($rows > 0) { $this->db->limit($rows); }
        
        $this->db->group_by('b.cust_id');
        $this->db->order_by('userCount','desc');
        return $this->db->get()->result_array();
      } else return false;
  }
    
  public function get_user_count_with_rejected_bookings($user_type=null, $country_id=0, $cat_id=0, $rows=0)
  {
    if($user_type != null && $user_type == 'deliverer'){
      $this->db->select('COUNT(r.deliverer_id) AS userCount, r.deliverer_id, p.company_name, p.firstname, p.lastname, p.contact_no, p.country_id, p.email_id');
      $this->db->from('tbl_rejected_orders AS r');
      $this->db->join('tbl_deliverer_profile AS p', 'p.cust_id = r.deliverer_id');
      
      if($country_id > 0) { $this->db->where('r.from_country_id', (int)$country_id); }
      if($cat_id > 0) { $this->db->where('r.cat_id', (int)$cat_id); }
      
      if($rows > 0) { $this->db->limit($rows); }

      $this->db->group_by('r.deliverer_id');
      $this->db->order_by('userCount','desc');
      return $this->db->get()->result_array();
    } else if($user_type != null && $user_type == 'customer'){
      $this->db->select('COUNT(r.cust_id) AS userCount, r.cust_id, c.company_name, c.firstname, c.lastname, c.mobile1, c.email1, c.country_id');
      $this->db->from('tbl_rejected_orders AS r');
      $this->db->join('tbl_customers_master AS c', 'c.cust_id = r.cust_id');
      
      if($country_id > 0) { $this->db->where('r.from_country_id', (int)$country_id); }
      if($cat_id > 0) { $this->db->where('r.cat_id', (int)$cat_id); }
      if($rows > 0) { $this->db->limit($rows); }
      
      $this->db->group_by('r.cust_id');
      $this->db->order_by('userCount','desc');
      return $this->db->get()->result_array();
    } else return false;
  }

  public function get_user_login_history($type=null, $country_id=0, $rows=0)
  {
    if($type != null && $type == 'recently'){
      $this->db->select('l.*, c.company_name, c.firstname, c.lastname, c.mobile1, c.email1, c.country_id');
      $this->db->from('tbl_cust_login_stats AS l');
      $this->db->join('tbl_customers_master AS c', 'c.cust_id = l.cust_id');
      if($country_id > 0) { $this->db->where('c.country_id', (int)$country_id); }
      if($rows > 0) { $this->db->limit($rows); }
      $this->db->order_by('l.cre_datetime','DESC');
      return $this->db->get()->result_array();
    } else if($type != null && $type == 'frequently'){
      $this->db->select('l.*, COUNT(l.cust_id) AS userCount, c.company_name, c.firstname, c.lastname, c.mobile1, c.email1, c.country_id');
      $this->db->from('tbl_cust_login_stats AS l');
      $this->db->join('tbl_customers_master AS c', 'c.cust_id = l.cust_id');
      if($country_id > 0) { $this->db->where('c.country_id', (int)$country_id); }
      if($rows > 0) { $this->db->limit($rows); }
      $this->db->group_by('l.cust_id');
      $this->db->order_by('userCount','DESC');
      return $this->db->get()->result_array();
    } else if($type != null && $type == 'never'){
      $this->db->select('cust_id, company_name, firstname, lastname, mobile1, email1, country_id, cre_datetime, social_type');
      $this->db->where('cust_id NOT IN (SELECT cust_id FROM tbl_cust_login_stats)');
      if($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
      if($rows > 0) { $this->db->limit($rows); }
      $this->db->order_by('cre_datetime','DESC');
      return $this->db->get('tbl_customers_master')->result_array();
    } else return false;
  }

  public function get_user_by_ratings($type=null, $country_id=0, $rows=0)
  {
    if($type != null){
      $this->db->select('cust_id, company_name, firstname, lastname, mobile1, email1, country_id, ratings');
      if($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
      if($rows > 0) { $this->db->limit($rows); }
      if($type == 'high') {
        $this->db->order_by('ratings','DESC');
      } else {
        $this->db->order_by('ratings','ASC');
      }
      return $this->db->get('tbl_customers_master')->result_array();
    } else return false;
  }

  public function get_user_by_currency($currency_code=null, $country_id=0, $rows=0, $type=null)
  {
    if($currency_code != null && $type != null){
      $this->db->select('a.*, CAST(a.account_balance AS SIGNED) AS acc_bal, c.cust_id, c.company_name, c.firstname, c.lastname, c.mobile1, c.email1, c.country_id');
      $this->db->from('tbl_user_account_master AS a');
      $this->db->join('tbl_customers_master AS c', 'a.user_id = c.cust_id');
      if($currency_code != 'all') { $this->db->where('a.currency_code', $currency_code); }
      if($country_id > 0) { $this->db->where('c.country_id', (int)$country_id); }
      if($rows > 0) { $this->db->limit($rows); }
      if($type == 'high') { $this->db->order_by('acc_bal','DESC'); } 
      else {  $this->db->order_by('acc_bal','ASC'); }
      return $this->db->get()->result_array();
    } else return false;
  }
  
  public function get_country_country_phonecode_by_id($id=0)
  { 
    if($id>0){            
      $this->db->where('country_id', (int) $id); 
      $r = $this->db->get($this->_countries)->row_array(); 
      return $r['country_phonecode'];
    } 
    return false; 
  }
  
  public function get_country_name_by_id($id=0)
  {        
    if($id>0){            
      $this->db->where('country_id', (int) $id);            
      $r = $this->db->get($this->_countries)->row_array();            
      return $r['country_name'];        
    }        
    return false;    
  }
  
  public function get_orders($status=null, $rows=0)
  {
    if( $rows > 0 ){ $this->db->limit($rows); }
    if($status != null) { $this->db->where('order_status', $status); }
    $this->db->order_by('cre_datetime', 'desc');
    return $this->db->get($this->_courier_orders)->result_array();
  }
    
  public function get_customer_email_by_id($id=0)
  {        
    if($id>0){            
      $this->db->where('cust_id', (int) $id);            
      $r = $this->db->get($this->_customers_master)->row_array();            
      return $r['email1'];        
    }        
    return false;    
  }
  
  public function get_users($user_type=null, $country_id=0, $record_type=null, $rows=0)
  {
      if($user_type != null && $record_type != null){
          if($record_type == 'count') {
              $this->db->select('COUNT(cust_id) AS userCount'); 
              if ($user_type == 'business') { $this->db->where('user_type', 0); }
              if ($user_type == 'individual') { $this->db->where('user_type', 1); }
              if ($user_type == 'seller') { $this->db->where('acc_type', 'seller'); }
              if ($user_type == 'buyer') { $this->db->where('acc_type', 'buyer'); }
              if ($user_type == 'both') { $this->db->where('acc_type', 'both'); }
              if ($user_type == 'active') { $this->db->where('cust_status', 1); }
              if ($user_type == 'inactive') { $this->db->where('cust_status', 0); }
              if ($user_type == 'online') { $this->db->where("last_login_datetime >= SUBTIME(NOW(), '1 0:10:0')"); }
              if ($user_type == 'offline') { $this->db->where("last_login_datetime <= SUBTIME(NOW(), '1 0:10:0')"); }
              if ($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
          }
          
          if($record_type == 'records') {
              if ($user_type == 'business') { $this->db->where('user_type', 0); }
              if ($user_type == 'individual') { $this->db->where('user_type', 1); }
              if ($user_type == 'seller') { $this->db->where('acc_type', 'seller'); }
              if ($user_type == 'buyer') { $this->db->where('acc_type', 'buyer'); }
              if ($user_type == 'both') { $this->db->where('acc_type', 'both'); }
              if ($user_type == 'active') { $this->db->where('cust_status', 1); }
              if ($user_type == 'inactive') { $this->db->where('cust_status', 0); }
              if ($user_type == 'online') { $this->db->where("last_login_datetime >= SUBTIME(NOW(), '1 0:10:0')"); }
              if ($user_type == 'offline') { $this->db->where("last_login_datetime <= SUBTIME(NOW(), '1 0:10:0')"); }
              if ($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
          }
          
          return $this->db->get($this->_customers_master)->result_array();
      } else return false;
  }
  
  public function get_jobs($order_type=null, $country_id=0, $record_type=null, $rows=0, $cat_id=0, $start_date=0, $to_date=0)
  {   
      if($order_type != null && $record_type != null){
          if($record_type == 'count') {
              $this->db->select('COUNT(order_id) AS jobCount');
              if ($order_type == 'open') { $this->db->where('order_status', 'open'); }
              if ($order_type == 'accepted') { $this->db->where('order_status', 'accept'); }
              if ($order_type == 'inprogress') { $this->db->where('order_status', 'in_progress'); }
              if ($order_type == 'delivered') { $this->db->where('order_status', 'delivered'); }
              if ($order_type == 'cancelled') { $this->db->where('order_status', 'cancel'); }
              if ($order_type == 'expired') { $this->db->where("DATE_FORMAT(expiry_date, '%Y-%m-%d') < NOW()"); }
              if ($country_id > 0) { $this->db->where('from_country_id', (int)$country_id); }
              if ($cat_id > 0) { $this->db->where('category_id', (int)$cat_id); }
              if ($start_date != 0 && $to_date != 0) { $this->db->where("DATE_FORMAT(cre_datetime, '%Y-%m-%d') >=", $start_date); $this->db->where("DATE_FORMAT(cre_datetime, '%Y-%m-%d') <=", $to_date); }
          }
          if($record_type == 'records') {
              if ($order_type == 'open') { $this->db->where('order_status', 'open'); }
              if ($order_type == 'accepted') { $this->db->where('order_status', 'accept'); }
              if ($order_type == 'inprogress') { $this->db->where('order_status', 'in_progress'); }
              if ($order_type == 'delivered') { $this->db->where('order_status', 'delivered'); }
              if ($order_type == 'cancelled') { $this->db->where('order_status', 'cancel'); }
              if ($order_type == 'expired') { $this->db->where("DATE_FORMAT(expiry_date, '%Y-%m-%d') < NOW()"); }
              if ($country_id > 0) { $this->db->where('from_country_id', (int)$country_id); }
              if ($cat_id > 0) { $this->db->where('category_id', (int)$cat_id); }
              if ($start_date != 0 && $to_date != 0) { $this->db->where("DATE_FORMAT(cre_datetime, '%Y-%m-%d') >=", $start_date); $this->db->where("DATE_FORMAT(cre_datetime, '%Y-%m-%d') <=", $to_date); }
          }
          
          return $this->db->get($this->_courier_orders)->result_array();
          //echo $this->db->last_query();
      } else return false;
  }
  
  

  public function get_customer_email_name_by_order_id($id=0)
  {
    if($id > 0) {
      $this->db->select('email1');
      $this->db->where('cust_id', (int)$id);
      $row = $this->db->get($this->_customers_master)->row_array();
      $email_cut = explode('@', $row['email1']);
      return ucwords($email_cut[0]);
    }
    return 'User';
  }

	public function get_country_detail($id=0)
	{		
		if($id > 0 ){
			$this->db->where('country_id', (int)$id);
			return $this->db->get($this->_countries)->row_array();
		}
		return false;
	}
  
  public function get_current_balance($cust_id=0,$currency_code=null)
  {
    if($cust_id > 0 && !empty($currency_code)){
    
      $this->db->where('user_id', (int) $cust_id);
      $this->db->where('currency_code', trim($currency_code));
      return $this->db->get('tbl_user_account_master')->row_array();
    }
    return false;
  }

  public function get_daily_transaction_graph( $date, $currency)
  {
    if($date == '') {
      return [];
    }
    $data[] = ['x' => $currency, 'y' => $this->daily_graph_amount_added($date, $currency), 'z' => $this->daily_graph_amount_deducted($date, $currency)];
    return $data;
  }

  public function get_weekly_transaction_graph($date, $currency)
  {
    if(empty($date)){
      return [];
    }
    //return $date;
    $datess = $this->date_range($date['to_date'], $date['from_date'],'+1 day','Y-m-d');
    //return $datess;
    $data = array();
    foreach ($datess as $date) {
      $added = 0;
      $deducted = 0;
      //$date = date('Y-m-d',strtotime($date));
      $this->db->select('sum(amount) AS added_sum');
      $this->db->where("type = 0");
      $this->db->where("currency_code = '".$currency."'");
      $this->db->where("DATE_FORMAT(datetime, '%Y-%m-%d') = '".$date."'");
      $added_res = $this->db->get($this->_gonagoo_account_history)->result_array();
      if($added_res[0]['added_sum'] != null){
       $added = $added_res[0]['added_sum'];
      }
      $this->db->select('sum(amount) AS added_sum');
      $this->db->where("type = 1");
      $this->db->where("currency_code = '".$currency."'");
      $this->db->where("DATE_FORMAT(datetime, '%Y-%m-%d') = '".$date."'");
      $added_res = $this->db->get($this->_gonagoo_account_history)->result_array();
      if($added_res[0]['added_sum'] != null){
       $deducted = $added_res[0]['added_sum'];
      }
      $data[] = ['x' => date('d M',strtotime($date)), 'y' => $added, 'z' => $deducted];
    }
    return $data;  
  }

  public function get_monthly_transaction_graph($currency, $year)
  {
    for ($m=1; $m<=12; $m++) {
      $month = date('m/Y', mktime(0,0,0,$m, 1, $year));
      $month_str = date('M', mktime(0,0,0,$m, 1, $year));
      //$data[] = $month;

      $added = 0;
      $deducted = 0;
      //$date = date('Y-m-d',strtotime($date));
      $this->db->select('sum(amount) AS added_sum');
      $this->db->where("type = 0");
      $this->db->where("currency_code = '".$currency."'");
      $this->db->where("DATE_FORMAT(datetime, '%m/%Y') = '".$month."'");
      $added_res = $this->db->get($this->_gonagoo_account_history)->result_array();
      if($added_res[0]['added_sum'] != null){
       $added = $added_res[0]['added_sum'];
      }
      $this->db->select('sum(amount) AS added_sum');
      $this->db->where("type = 1");
      $this->db->where("currency_code = '".$currency."'");
      $this->db->where("DATE_FORMAT(datetime, '%m/%Y') = '".$month."'");
      $added_res = $this->db->get($this->_gonagoo_account_history)->result_array();
      if($added_res[0]['added_sum'] != null){
       $deducted = $added_res[0]['added_sum'];      
      }
      $data[] = ['x' =>$month_str, 'y' => $added, 'z' => $deducted];
    } // foreach
    return $data;  
  }

  public function daily_graph_amount_added($date, $currency)
  {
    //return rand(0,100);
    $date = date('Y-m-d',strtotime($date));
    $this->db->select('sum(amount) AS added_sum');
    $this->db->where("type = 1");
    $this->db->where("currency_code = '".$currency."'");
    $this->db->where("DATE_FORMAT(datetime, '%Y-%m-%d') = '$date'");
    $added_res = $this->db->get($this->_gonagoo_account_history)->result_array();
    //print_r($added_res[0]['added_sum'] != null);
    if($added_res[0]['added_sum'] != null){
     return $added_res[0]['added_sum'];
    }else{
     return 0;
    }
  }

  public function daily_graph_amount_deducted($date, $currency)
  {
    $date = date('Y-m-d',strtotime($date));
    $this->db->select('sum(amount) AS added_sum');
    $this->db->where("type = 0");
    $this->db->where("currency_code = '".$currency."'");
    $this->db->where("DATE_FORMAT(datetime, '%Y-%m-%d') = '".$date."'");
    $added_res = $this->db->get($this->_gonagoo_account_history)->result_array();
    //print_r($added_res[0]['added_sum'] != null);
    if($added_res[0]['added_sum'] != null){
     return $added_res[0]['added_sum'];
    }else{
     return 0;
    }
  }

  function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) 
  {
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    
    while( $current <= $last ) {
      $dates[] = date($output_format, $current);
      $current = strtotime($step, $current);
    }
    return $dates;
  }

  public function get_daily_users_graph($date)
  {
    $this->db->select('COUNT(cust_id) AS userCount');
    $this->db->where('user_type', 0);  //business
    $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
    $business = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

    $this->db->select('COUNT(cust_id) AS userCount');
    $this->db->where('user_type', 1);  //individual
    $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
    $individual = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

    $this->db->select('COUNT(cust_id) AS userCount');
    $this->db->where('acc_type', 'buyer');  //buyer
    $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
    $buyer = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

    $this->db->select('COUNT(cust_id) AS userCount');
    $this->db->where('acc_type', 'seller');  //seller
    $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
    $seller = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

    $this->db->select('COUNT(cust_id) AS userCount');
    $this->db->where('acc_type', 'both');  //both
    $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
    $both = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];
   $data[] = [
              'x'=> date('d F Y',strtotime($date)), 
              'o'=> $business, 
              'p'=> $individual,
              'q'=> $buyer,
              'r'=> $seller,
              's'=> $both,  
            ];
    return $data;
  }

  public function get_weekly_users_graph($weekly_date)
  {
    $date_array = explode(' To ', $weekly_date);
    if(empty($date_array)){
      $data[] = [
                'x'=> $weekly_date, 
                'o'=> 0, 
                'p'=> 0,
                'q'=> 0,
                'r'=> 0,
                's'=> 0,  
              ];
      return $data;
    }

    $datess = $this->date_range($date_array[0], $date_array[1], '+1 day', 'Y-m-d');
    //print_r($datess);
    $data = array();
    foreach ($datess as $date) {
        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('user_type', 0);  //business
        $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
        $business = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('user_type', 1);  //individual
        $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
        $individual = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('acc_type', 'buyer');  //buyer
        $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
        $buyer = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('acc_type', 'seller');  //seller
        $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
        $seller = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('acc_type', 'both');  //both
        $this->db->where("DATE_FORMAT(cre_datetime,'%Y-%m-%d')",date('Y-m-d',strtotime($date)));
        $both = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];
       $data[] = [
                  'x'=> date('d M',strtotime($date)), 
                  'o'=> $business, 
                  'p'=> $individual,
                  'q'=> $buyer,
                  'r'=> $seller,
                  's'=> $both,  
                ];
    }
       return $data;
  }

  public function get_monthly_users_graph( $year)
  {
    for ($m=1; $m<=12; $m++) {
      $month = date('m/Y', mktime(0,0,0,$m, 1, $year));
      $month_str = date('M', mktime(0,0,0,$m, 1, $year));
        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('user_type', 0);  //business
        $this->db->where("DATE_FORMAT(cre_datetime,'%m/%Y')",$month);
        $business = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('user_type', 1);  //individual
        $this->db->where("DATE_FORMAT(cre_datetime,'%m/%Y')",$month);
        $individual = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('acc_type', 'buyer');  //buyer
        $this->db->where("DATE_FORMAT(cre_datetime,'%m/%Y')",$month);
        $buyer = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('acc_type', 'seller');  //seller
        $this->db->where("DATE_FORMAT(cre_datetime,'%m/%Y')",$month);
        $seller = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];

        $this->db->select('COUNT(cust_id) AS userCount');
        $this->db->where('acc_type', 'both');  //both
        $this->db->where("DATE_FORMAT(cre_datetime,'%m/%Y')",$month);
        $both = $this->db->get($this->_customers_master)->result_array()[0]['userCount'];
       $data[] = [
                  'x'=> $month_str, 
                  'o'=> $business, 
                  'p'=> $individual,
                  'q'=> $buyer,
                  'r'=> $seller,
                  's'=> $both,  
                ];
    }
    return $data;  
  }

  //@zaid - bus
  public function get_total_trips($order_type=null, $country_id=0, $record_type=null, $start_date=0, $to_date=0, $rows=0)
  {
    if($order_type != null && $record_type != null){
      if($record_type == 'count'){
        $this->db->select('COUNT(trip_id) AS jobCount');
        if($order_type=='upcoming') { $this->db->where('journey_date > ', date('m/d/Y')); }
        if($order_type=='current') { $this->db->where('journey_date', date('m/d/Y')); }
        if($order_type=='previous') { $this->db->where('journey_date < ', date('m/d/Y')); }
        if($order_type=='complete') { $this->db->where('trip_status' , "complete"); }
        if($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
        if ($start_date != 0 && $to_date != 0){
          $a = explode('-',$start_date); 
          $b = explode('-',$to_date);
          $start_date_b = $a[1]."/".$a[2]."/".$a[0];
          $to_date_b = $b[1]."/".$b[2]."/".$b[0];
          $this->db->where("journey_date >=", $start_date_b);
          $this->db->where("journey_date <=", $to_date_b); 
        }      
      }
      if($record_type == 'records') {
        $this->db->group_by('unique_id');
        if($order_type=='upcoming') { $this->db->where('journey_date > ', date('m/d/Y')); }
        if($order_type=='current') { $this->db->where('journey_date', date('m/d/Y'));}
        if($order_type=='previous') { $this->db->where('journey_date < ', date('m/d/Y')); }
        if($order_type=='complete') { $this->db->where('trip_status' , "complete"); }
        if($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
        if($start_date != 0 && $to_date != 0){ 
          $a = explode('-',$start_date); 
          $b = explode('-',$to_date);
          $start_date_b = $a[1]."/".$a[2]."/".$a[0];
          $to_date_b = $b[1]."/".$b[2]."/".$b[0];
          $this->db->where("journey_date >=", $start_date_b);
          $this->db->where("journey_date <=", $to_date_b); 
        }
      }
      if( $rows > 0 ){ $this->db->limit($rows); }
      return $this->db->get($this->_bus_booking_master)->result_array();
      // $this->db->last_query();
    } else return false;
  }

  public function get_cancelled_trips($order_type=null, $country_id=0, $record_type=null, $start_date=0, $to_date=0, $rows=0)
  {
    if($order_type != null && $record_type != null)
    {
      if($record_type == 'count'){
        $this->db->select('COUNT(trip_id) AS jobCount');
        if($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
        if($start_date != 0 && $to_date != 0){ 
          $a = explode('-',$start_date); 
          $b = explode('-',$to_date);
          $start_date_b = $a[1]."/".$a[2]."/".$a[0];
          $to_date_b = $b[1]."/".$b[2]."/".$b[0];
          $this->db->where("cancelled_date >=", $start_date_b);
          $this->db->where("cancelled_date <=", $to_date_b);
        }
      }
      if($record_type == 'records'){
        if($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
        if($start_date != 0 && $to_date != 0){ 
          $a = explode('-',$start_date); 
          $b = explode('-',$to_date);
          $start_date_b = $a[1]."/".$a[2]."/".$a[0];
          $to_date_b = $b[1]."/".$b[2]."/".$b[0];
          $this->db->where("cancelled_date >=", $start_date_b);
          $this->db->where("cancelled_date <=", $to_date_b);
        }
      }
      if( $rows > 0 ){ $this->db->limit($rows); }
      return $this->db->get($this->_cancelled_trips)->result_array();
    } else return false;
  }

  public function get_bus_trip($trip_id =0)
  {
    if($trip_id > 0){
      $this->db->where('trip_id', (int)$trip_id);
      return $this->db->get($this->_bus_trip_master)->row_array();
    }return false;
  }

  public function laundry_booking_list($booking_type=null , $country_id=0, $record_type=null, $start_date=0, $to_date=0, $rows=0)
  {

    if($booking_type != null && $record_type != null)
    {
      if($record_type == 'count'){
        $this->db->select('COUNT(booking_id) AS jobCount');
        if(trim($booking_type)!= 'all') { $this->db->where('booking_status', trim($booking_type));}
        if($country_id > 0) { $this->db->where('country_id', (int)$country_id); }
        if($start_date != 0 && $to_date != 0){ 
          $this->db->where("DATE_FORMAT(cre_datetime , '%Y-%m-%d') >=", $start_date);
          $this->db->where("DATE_FORMAT(cre_datetime , '%Y-%m-%d') <=", $to_date); 
        }
      }

      if($record_type == 'records'){
        if(trim($booking_type) != 'all'){$this->db->where('booking_status', trim($booking_type));}
        if($country_id > 0){$this->db->where('country_id', (int)$country_id);}
        if($start_date != 0 && $to_date != 0){ 
          $this->db->where("DATE_FORMAT(cre_datetime , '%Y-%m-%d') >=", $start_date);
          $this->db->where("DATE_FORMAT(cre_datetime , '%Y-%m-%d') <=", $to_date); 
        }
      }
      if( $rows > 0 ){ $this->db->limit($rows); }
      $this->db->order_by('booking_id', 'desc');
      return $this->db->get($this->_laundry_booking)->result_array();
    } return false;
  }

  public function get_user_count_with_bookings_bus($user_type=null, $country_id=0, $rows=0, $booking_status=null)
  {
      if($user_type != null && $user_type == 'deliverer'){
        $this->db->select('COUNT(b.operator_id) AS userCount, b.operator_id, p.company_name, p.firstname, p.lastname, p.contact_no, p.country_id, p.email_id');
        $this->db->from('tbl_bus_booking_master AS b');
        $this->db->join('tbl_bus_operator_profile AS p', 'p.deliverer_id = b.operator_id');
        
        if($booking_status=='upcoming') { $this->db->where('journey_date > ', date('m/d/Y')); }
        if($booking_status=='current') { $this->db->where('journey_date', date('m/d/Y')); }
        if($booking_status=='previous') { $this->db->where('journey_date < ', date('m/d/Y')); }
        if($booking_status=='complete') { $this->db->where('trip_status' , "complete"); }
        
        if($country_id > 0) { $this->db->where('p.country_id', (int)$country_id); }
        if($rows > 0) { $this->db->limit($rows); }
        
        $this->db->group_by('b.operator_id');
        $this->db->order_by('userCount','desc');
        return $this->db->get()->result_array();
      } else if($user_type != null && $user_type == 'customer'){
        $this->db->select('COUNT(b.cust_id) AS userCount, b.cust_id, c.company_name, c.firstname, c.lastname, c.mobile1, c.email1, c.country_id');
        $this->db->from('tbl_bus_booking_master AS b');
        $this->db->join('tbl_customers_master AS c', 'c.cust_id = b.cust_id');
        
        if($booking_status=='upcoming') { $this->db->where('journey_date > ', date('m/d/Y')); }
        if($booking_status=='current') { $this->db->where('journey_date', date('m/d/Y')); }
        if($booking_status=='previous') { $this->db->where('journey_date < ', date('m/d/Y')); }
        if($booking_status=='complete') { $this->db->where('trip_status' , "complete"); }
        
        if($country_id > 0) { $this->db->where('c.country_id', (int)$country_id); }
        if($rows > 0) { $this->db->limit($rows); }
        
        $this->db->group_by('b.cust_id');
        $this->db->order_by('userCount','desc');
        return $this->db->get()->result_array();
      } else return false;
  }


  public function get_user_count_with_bookings_laundry($user_type=null, $country_id=0, $rows=0, $booking_status=null)
  {
      if($user_type != null && $user_type == 'deliverer'){
        $this->db->select('COUNT(b.provider_id) AS userCount, b.provider_id, p.company_name, p.firstname, p.lastname, p.contact_no, p.country_id, p.email_id');
        $this->db->from('tbl_laundry_booking AS b');
        $this->db->join('tbl_laundry_provider_profile AS p', 'p.deliverer_id = b.provider_id');
        
        if(trim($booking_status) != 'all'){
          $this->db->where('b.booking_status', trim($booking_status));
        }
        
        if($country_id > 0) { $this->db->where('p.country_id', (int)$country_id); }
        if($rows > 0) { $this->db->limit($rows); }
        
        $this->db->group_by('b.provider_id');
        $this->db->order_by('userCount','desc');
        return $this->db->get()->result_array();
      } else if($user_type != null && $user_type == 'customer'){
        $this->db->select('COUNT(b.cust_id) AS userCount, b.cust_id, c.company_name, c.firstname, c.lastname, c.mobile1, c.email1, c.country_id');
        $this->db->from('tbl_laundry_booking AS b');
        $this->db->join('tbl_customers_master AS c', 'c.cust_id = b.cust_id');
       
        if(trim($booking_status) != 'all'){
          $this->db->where('b.booking_status', trim($booking_status));
        }

        if($country_id > 0) { $this->db->where('c.country_id', (int)$country_id); }
        if($rows > 0) { $this->db->limit($rows); }
        
        $this->db->group_by('b.cust_id');
        $this->db->order_by('userCount','desc');
        return $this->db->get()->result_array();
      } else return false;
  }


  public function laundry_booking_details($booking_id=0)
  {
    if($booking_id > 0) {
      $this->db->where('booking_id', (int) $booking_id);
      return $this->db->get($this->_laundry_booking)->row_array();
    } return false;
  }

  public function laundry_booking_details_cart($booking_id=0)
  {
    if($booking_id > 0) {
      $this->db->where('booking_id', (int) $booking_id);
      return $this->db->get($this->_laundry_booking_details)->result_array();
    } return false;
  }

  public function get_laundry_provider_profile($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      return $this->db->get($this->_laundry_provider_profile)->row_array();
    } return false;
  }
  public function get_sub_category_details($sub_cat_id=0)
  {
    if($sub_cat_id > 0){
      $this->db->where('sub_cat_id', (int) $sub_cat_id);
      return $this->db->get($this->_laundry_sub_category)->row_array();
    } return false;
  }
  public function get_laundry_category($cat_id=0)
  {
    if($cat_id>0){
      $this->db->where('cat_id',$cat_id);
      return $this->db->get($this->_laundry_category)->row_array();
    } return false;
  }

  public function get_bus_operator_profile($cust_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      return $this->db->get($this->_bus_operator_profile)->row_array();
    }
    return false;
  }

  public function get_trip_booking_details($ticket_id=0)
  {
    if($ticket_id > 0) {
      $this->db->where('ticket_id', (int)$ticket_id);
      return $this->db->get($this->_bus_booking_master)->row_array();
    } else return false;
  }

  public function get_ticket_seat_details($ticket_id=0)
  {
    if($ticket_id > 0) {
      $this->db->where('ticket_id', (int)$ticket_id);
      return $this->db->get($this->_bus_booking_seat_details)->result_array();
    } else return false;
  }


  //Manage Promo Code-----------------------------
    public function get_promocode()
    {
      $this->db->where('is_delete', 0);
      return $this->db->get($this->_promo_code)->result_array();
    }
    public function get_promocode_by_id($promo_code_id = 0)
    {
      if($promo_code_id > 0){
      $this->db->where('promo_code_id',$promo_code_id);
      $this->db->where('is_delete', 0);
      return $this->db->get($this->_promo_code)->row_array();
      }else{ return false;}
    }
    public function activate_promo_code($id=0)
    {
      if($id > 0){
        $this->db->where('promo_code_id', (int) $id);
        $this->db->where('code_status', 0);
        $this->db->update($this->_promo_code, ['code_status' => 1]);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function inactivate_promo_code($id=0)
    {
      if($id > 0){
        $this->db->where('promo_code_id', (int) $id);
        $this->db->where('code_status', 1);
        $this->db->update($this->_promo_code, ['code_status' => 0]);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function delete_promo_code($id=0)
    {
      if($id > 0){
        $this->db->where('promo_code_id', (int) $id);
        $this->db->where('is_delete', 0);
        $this->db->update($this->_promo_code, ['is_delete' => 1]);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function register_promocode(array $data)
    {
      if(is_array($data)){
        $this->db->insert($this->_promo_code, $data);
        return $this->db->insert_id();
      }
      return false;
    }
    public function update_promo_code($id=0 , $data=null)
    {
      if(is_array($data) && $id > 0){
        $this->db->where('promo_code_id', (int) $id);
        $this->db->update($this->_promo_code, $data);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function is_promocode_exists($promocode=null)
    {
      if($promocode != null) {
        $this->db->where('promo_code', trim($promocode));
        return $this->db->get($this->_promo_code)->row_array();
      }
      return false;
    }
  //Manage Promo Code-----------------------------


}//end of file

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */