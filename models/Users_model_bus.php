<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model_bus extends CI_Model {

	private $customers_master = "tbl_customers_master";
	private $customer_skills = "tbl_customer_skills";
	private $skills_master = "tbl_skills_master";
	private $skill_levels = "tbl_skill_levels";
	private $_deliverer_profile = "tbl_deliverer_profile";
    
    public function total_accounts($type = false, $acc_type=false, $active = false, $status = false )
	{   
	    $today = date('Y-m-d H:i:s');
	    
	    if($type != false){ $type = ($type == "business") ? 0 : 1;  $this->db->where('user_type',$type); }
	    else if($acc_type != false){ $this->db->where('acc_type',strtolower($acc_type)); }
	    else if($active != false){ $active = ($active == 'active') ? 1 : 0; $this->db->where('cust_status',$active);  }
	    else if($status != false && $status == "online"){ $this->db->where("last_login_datetime >= DATE_SUB('$today', INTERVAL 10 MINUTE)");  }
	    
	    
		return $this->db->get($this->customers_master)->num_rows();
	}
    
	public function get_users($country_id=0)
	{
	    if($country_id > 0 ) { $this->db->where('country_id', $country_id); }
		$this->db->where('cust_status', 1);
		return $this->db->get($this->customers_master)->result_array();
	}

	public function get_users_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', $id);
			//$this->db->where('cust_status', 1);
			return $this->db->get($this->customers_master)->row_array();
		}
		return false;
	}

	public function get_deliverer_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', $id);
			return $this->db->get($this->_deliverer_profile)->row_array();
		}
		return false;
	}

	public function activate_users($id=0)
	{
		if($id > 0){
			$this->db->where('cust_id', (int) $id);
			$this->db->where('cust_status', 0);
			$this->db->update($this->customers_master, ['cust_status' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_shipping_mode($deliverer_id=null, $shipping_mode=null)
	{
		if( !is_null($deliverer_id) && !is_null($shipping_mode) ){
			$this->db->where('deliverer_id', (int)$deliverer_id);
			$this->db->update($this->_deliverer_profile, ['shipping_mode' => (int)$shipping_mode]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_users($id=0)
	{
		if($id > 0){
			$this->db->where('cust_id', (int) $id);
			$this->db->where('cust_status', 1);
			$this->db->update($this->customers_master, ['cust_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	//inactive users
	public function get_inactive_users()
	{
		$this->db->where('cust_status', 0);
		return $this->db->get($this->customers_master)->result_array();
	}

	public function get_customer_skills($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', $id);
			return $this->db->get($this->customer_skills)->row_array();
		}
		return false;
	}

	public function get_skill_name($id=0)
	{
		if($id > 0) {
			$this->db->select('skill_name');
			$this->db->where('skill_id', $id);
			return $this->db->get($this->skills_master)->row_array();
		}
		return false;
	}

	public function get_level_name($id=0)
	{
		if($id > 0) {
			$this->db->select('level_title');
			$this->db->where('level_id', $id);
			return $this->db->get($this->skill_levels)->row_array();
		}
		return false;
	}
	
	public function convert_big_int($int = 0){
	    if( $int > 1000 && $int < 1000000){ $int = ($int/1000); return number_format($int ,1,'.',',') . 'K'; }         
	    else if( $int >= 1000000 ){ $int = ($int / 1000000 ); return number_format($int ,1,'.',',') . 'M'; }
	    else{ return $int; }
    }

}

/* End of file Users_model_bus.php */
/* Location: ./application/models/Users_model_bus.php */