<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authority_model extends CI_Model {

	private $authorities_master = "tbl_authorities_master";
	private $authority_type = "tbl_authority_type_master";
	private $auth_permissions = "tbl_auth_permissions";
	private $_authority_permission_master = "tbl_authority_permission_master";

	public function get_authorities()
	{
		//$this->db->select('a.*, at.auth_type');
		//$this->db->from('tbl_authorities_master as a');
		//$this->db->join('tbl_authority_type_master as at', 'a.auth_type_id = at.auth_type_id');
		// $this->db->where('a.auth_status', 1);
		$this->db->where('auth_id !=', 1);
		return $this->db->get($this->authorities_master)->result_array();
	}

	public function get_authority_type()
	{
		$this->db->where('auth_type_status', 1);
		$this->db->where('auth_type_id !=', 1);
		return $this->db->get($this->_authority_permission_master)->result_array();
	}

	public function get_authority_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('auth_id', $id);
			$this->db->where('auth_status', 1);
			return $this->db->get($this->authorities_master)->row_array();
		}
		return false;
	}

	public function check_auth_email_existance($email=null)
	{
		if($email !=  null) {
			$this->db->where('auth_email', trim($email));
			return $this->db->get($this->authorities_master)->row_array();
		}
		return false;
	}

	public function update_authority($auth_id=0, array $data)
	{
		if(is_array($data) && $auth_id > 0 ){			
			$this->db->where('auth_id', (int)$auth_id);
			$this->db->update($this->authorities_master, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_authority_password($auth_id=0, $auth_pass=null)
	{
		if($auth_id > 0 AND  $auth_pass != null ){			
			$this->db->where('auth_id', (int)$auth_id);
			$this->db->update($this->authorities_master, ['auth_pass' => trim($auth_pass)]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function activate_authority($id=0)
	{
		if($id > 0){
			$this->db->where('auth_id', (int) $id);
			$this->db->where('auth_status', 0);
			$this->db->update($this->authorities_master, ['auth_status' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_authority($id=0)
	{
		if($id > 0){
			$this->db->where('auth_id', (int) $id);
			$this->db->where('auth_status', 1);
			$this->db->update($this->authorities_master, ['auth_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function register_authority(array $data)
	{
		if(is_array($data)){
			$today = date('Y-m-d H:i:s');
			$insert_data = array(
				"auth_fname" => $data['fname'],
				"auth_lname" => $data['lname'],
				"auth_email" => $data['email'],
				"auth_pass" => $data['password'],
				"auth_type_id" => $data['auth_type_id'],
				"type" => $data['type'],
				"country_id" => $data['country_id'],
				"auth_avatar_url" => $data['profile_image_url'],
				"cre_datetime" => $today,
				"auth_status" => 1,
			);
			$this->db->insert($this->authorities_master, $insert_data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function update_permissions(array $data)
	{
		if(is_array($data)){
			$this->db->where('auth_id', (int)$data['auth_id']);
			$this->db->update($this->auth_permissions, ['category_type' => trim($data['category_type']),'categories' => trim($data['categories']),'currency_master' => trim($data['currency_master']),'country_currency' => trim($data['country_currency']),'authority_type' => trim($data['authority_type']),'manage_authority' => trim($data['manage_authority']),'manage_users' => trim($data['manage_users']),'notifications' => trim($data['notifications']), 'promo_code' => trim($data['promo_code']), 'promo_code_service' => trim($data['promo_code_service']), 'dimension' => trim($data['dimension']), 'vehicals' => trim($data['vehicals']), 'rates' => trim($data['rates']), 'driver_category' => trim($data['driver_category']), 'advance_payment' => trim($data['advance_payment']), 'skills_master' => trim($data['skills_master']), 'insurance_master' => trim($data['insurance_master']), 'relay_point' => trim($data['relay_point']), 'verify_doc' => trim($data['verify_doc']), 'all_orders' => trim($data['all_orders']), 'withdraw_request' => trim($data['withdraw_request'])]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_group_name_by_id($id=0)
	{
		if($id > 0){
			$this->db->where('auth_type_id', (int)$id);
			$row = $this->db->get($this->_authority_permission_master)->row_array();
			return $row['auth_type'];
		} else return false;
	}
}

/* End of file Authority_model.php */
/* Location: ./application/models/Authority_model.php */