<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dispute_category_master_model extends CI_Model {
	private $_smp_dispute_category_master = "tbl_smp_dispute_category_master";

	public function get_categories() {
		$this->db->order_by('dispute_cat_id', 'desc');
		return $this->db->get($this->_smp_dispute_category_master)->result_array();
	}
	public function register_cateogry(array $data) {
		if(is_array($data)){			
			$this->db->insert($this->_smp_dispute_category_master, $data);
			return $this->db->insert_id();
		} return false;
	}
	public function update_cateogry(array $data, $dispute_cat_id=0) {
		if(is_array($data) AND $dispute_cat_id > 0){
			$this->db->where('dispute_cat_id', (int)$dispute_cat_id);
			$this->db->update($this->_smp_dispute_category_master, $data);
			return $this->db->affected_rows();
		} return false;
	}
}

/* End of file Dispute_category_master_model.php */
/* Location: ./application/models/Dispute_category_master_model.php */