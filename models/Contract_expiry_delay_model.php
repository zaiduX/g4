<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract_expiry_delay_model extends CI_Model {
	private $_smp_contract_expiry_notification_hours = "tbl_smp_contract_expiry_notification_hours";
	private $_countries = "tbl_countries";

	public function get_contract_delay_hours() {
		$this->db->select('c.country_name, delay.*');
		$this->db->from($this->_countries.' as c');
		$this->db->join($this->_smp_contract_expiry_notification_hours.' as delay', 'delay.country_id = c.country_id');
		$this->db->group_by('delay.expiry_id');
		$this->db->order_by('delay.expiry_id', 'asc');
		return $this->db->get($this->_smp_contract_expiry_notification_hours)->result_array();
	}
	public function get_countries() {
		return $this->db->get($this->_countries)->result_array();
	}
	public function register_delay_hours(array $data) {
		if(is_array($data)){			
			$this->db->insert($this->_smp_contract_expiry_notification_hours, $data);
			return $this->db->insert_id();
		} return false;
	}
	public function update_delay_hours(array $data, $expiry_id=0) {
		if(is_array($data) AND $expiry_id > 0){
			$this->db->where('expiry_id', (int)$expiry_id);
			$this->db->update($this->_smp_contract_expiry_notification_hours, $data);
			return $this->db->affected_rows();
		} return false;
	}
	public function delete_delay_hours($expiry_id=0) {
		if($expiry_id > 0){
			$this->db->where('expiry_id', (int)$expiry_id);
			$this->db->delete($this->_smp_contract_expiry_notification_hours);
			return $this->db->affected_rows();
		} return false;
	}
}

/* End of file Contract_expiry_delay_model.php */
/* Location: ./application/models/Contract_expiry_delay_model.php */