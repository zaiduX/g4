<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standard_dimension_model extends CI_Model {

	private $standard_dimension = "tbl_standard_dimension"; // db table declaration
	private $categories = "tbl_category_master"; 
	
	public function get_categories()
	{
		$this->db->where('cat_status', 1);
		return $this->db->get($this->categories)->result_array();
	}

	public function get_category_name_by_id($id=0)
	{
		if($id>0){
			$this->db->select('cat_id, cat_name, parent_cat_id, cat_type_id');
			$this->db->where('cat_id', $id);
			return $this->db->get($this->categories)->row_array();
		}
	}

	public function get_standard_dimension()
	{
		$this->db->where('status', 1);
		return $this->db->get($this->standard_dimension)->result_array();
	}	

	public function check_standard_dimension(array $data)
	{
		if(is_array($data)){
			$this->db->select('dimension_id');
			$this->db->where('dimension_type', $data['dimension_type']);
			$this->db->where('width', $data['width']);
			$this->db->where('height', $data['height']);
			$this->db->where('length', $data['length']);
			$this->db->where('category_id', $data['category_id']);
			if($res = $this->db->get($this->standard_dimension)->row_array()){ return $res['dimension_id']; }
		}
		return false;
	}

	public function check_standard_dimension_for_update(array $data)
	{
		if(is_array($data)){
			$this->db->select('dimension_id');
			$this->db->where('dimension_type', $data['dimension_type']);
			$this->db->where('width', $data['width']);
			$this->db->where('height', $data['height']);
			$this->db->where('length', $data['length']);
			$this->db->where('on_earth', $data['on_earth']);
			$this->db->where('on_air', $data['on_air']);
			$this->db->where('on_sea', $data['on_sea']);
			$this->db->where('image_url', $data['image_url']);
			if($res = $this->db->get($this->standard_dimension)->row_array()){ return $res['dimension_id']; }
		}
		return false;
	}

	public function register_standard_dimension(array $data)
	{
		if(is_array($data)){			
			$this->db->insert($this->standard_dimension, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_standard_dimension_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('dimension_id', (int) $id);
			return $this->db->get($this->standard_dimension)->row_array();
		}
		return false;
	}

	public function update_standard_dimension(array $data, $id=0)
	{
		if(is_array($data) AND $id > 0){
			$this->db->where('dimension_id', (int)$id);
			$this->db->update($this->standard_dimension, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function delete_standard_dimension($id=0)
	{
		if($id > 0 ){
			$this->db->where('dimension_id', (int) $id);
			// $this->db->delete($this->standard_dimension);
			$this->db->update($this->standard_dimension, ['status'=>0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	

}

/* End of file Standard_dimension_model.php */
/* Location: ./application/models/Standard_dimension_model.php */