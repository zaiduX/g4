<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_type_model extends CI_Model {

	private $category_types = "tbl_category_type_master"; // db table declaration
	
	public function get_category_types()
	{
		// $this->db->where('cat_type_status', 1);
		return $this->db->get($this->category_types)->result_array();
	}

	public function get_active_category_types()
	{
		$this->db->where('cat_type_status', 1);
		return $this->db->get($this->category_types)->result_array();
	}

	public function get_category_type_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('cat_type_id', $id);
			$this->db->where('cat_type_status', 1);
			return $this->db->get($this->category_types)->row_array();
		}
		return false;
	}

	public function get_category_type_by_type($type=null)
	{
		if($type !=  null) {
			$this->db->where('cat_type', trim($type));
			return $this->db->get($this->category_types)->row_array();
		}
		return false;
	}

	public function update_category_type(array $data)
	{
		if(is_array($data)){
			$this->db->where('cat_type_id', (int)$data['cat_type_id']);
			$this->db->where('cat_type !=', trim($data['cat_type']));
			$this->db->update($this->category_types, ['cat_type' => trim($data['cat_type']), 'mod_datetime' => date('Y-m-d H:i:s')]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function activate_category_type($id=0)
	{
		if($id > 0){
			$this->db->where('cat_type_id', (int) $id);
			$this->db->where('cat_type_status', 0);
			$this->db->update($this->category_types, ['cat_type_status' => 1, 'mod_datetime' => date('Y-m-d H:i:s')]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_category_type($id=0)
	{
		if($id > 0){
			$this->db->where('cat_type_id', (int) $id);
			$this->db->where('cat_type_status', 1);
			$this->db->update($this->category_types, ['cat_type_status' => 0, 'mod_datetime' => date('Y-m-d H:i:s')]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function register_category_type($type=null)
	{
		if($type!=null){
			$today = date('Y-m-d H:i:s');
			$insert_data = array(
				'cat_type' => trim($type),
				'cat_type_status' => 1, 
				'cre_datetime' => $today,
				'mod_datetime' => $today,
			);
			$this->db->insert($this->category_types, $insert_data);
			return $this->db->insert_id();
		}
		return false;
	}
}

/* End of file Category_type_model.php */
/* Location: ./application/models/Category_type_model.php */