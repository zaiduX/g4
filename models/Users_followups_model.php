<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_followups_model extends CI_Model {

	private $customers_master = "tbl_customers_master";
	private $authorities_master = "tbl_authorities_master";
    
	public function total_accounts($followup_status=0, $is_deliverer=null)
	{
	    if ($followup_status > 0 ) { $this->db->where('followup_status',$followup_status); }
	    if (!is_null($is_deliverer)) { $this->db->where('is_deliverer',$is_deliverer); }
	    return $this->db->get($this->customers_master)->num_rows();
	}
    
	public function get_users($country_id=0)
	{
	    if($country_id > 0 ) { $this->db->where('country_id', $country_id); }
		$this->db->where('followup_status', 1);
		return $this->db->get($this->customers_master)->result_array();
	}
    
	public function get_users_assigned($country_id=0)
	{
	    if($country_id > 0 ) { $this->db->where('country_id', $country_id); }
		$this->db->where('followup_status IN (2,3,4)');
		return $this->db->get($this->customers_master)->result_array();
	}
    
	public function get_authority_list()
	{
		$this->db->where('auth_id !=', 1);
		return $this->db->get($this->authorities_master)->result_array();
	}

	public function assign_authority_to_user($cust_id=0, $auth_id=0)
	{
		if( $cust_id > 0 && $auth_id > 0 ){
			$this->db->where('cust_id', (int)$cust_id);
			$this->db->update($this->customers_master, ['followup_taken_authority_id' => (int)$auth_id, 'followup_status' => 2, 'followup_remark' => '', 'followup_taken_datetime' => 'NULL' ]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_authority_name_by_id($id=0)
	{
		if($id > 0) {
			$this->db->where('auth_id', $id);
			$row = $this->db->get($this->authorities_master)->row_array();
			return $row['auth_fname'] . ' ' . $row['auth_lname'];
		}
		return false;
	}
	
	public function convert_big_int($int = 0){
	    if( $int > 1000 && $int < 1000000){ $int = ($int/1000); return number_format($int ,1,'.',',') . 'K'; }         
	    else if( $int >= 1000000 ){ $int = ($int / 1000000 ); return number_format($int ,1,'.',',') . 'M'; }
	    else{ return $int; }
    }

}

/* End of file Users_followups_model.php */
/* Location: ./application/models/Users_followups_model.php */