<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_permissions_model extends CI_Model {

	private $authority_types = "tbl_authority_type_master";
	private $_authority_permission_master = "tbl_authority_permission_master";

	public function get_group_permission()
	{
		return $this->db->get($this->_authority_permission_master)->result_array();
	}

	public function register_group_permission($data=null)
	{
		if($data!=null){
			$this->db->insert($this->_authority_permission_master, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_group_permission_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('auth_type_id', $id);
			return $this->db->get($this->_authority_permission_master)->row_array();
		}
		return false;
	}

	public function activate_group_permission($id=0)
	{
		if($id > 0){
			$this->db->where('auth_type_id', (int) $id);
			$this->db->where('auth_type_status', 0);
			$this->db->update($this->_authority_permission_master, ['auth_type_status' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_group_permission($id=0)
	{
		if($id > 0){
			$this->db->where('auth_type_id', (int) $id);
			$this->db->where('auth_type_status', 1);
			$this->db->update($this->_authority_permission_master, ['auth_type_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_group_permission(array $data, $id=0)
	{
		if(is_array($data) && $id > 0){
			$this->db->where('auth_type_id', (int)$id);
			$this->db->update($this->_authority_permission_master, $data);
			return $this->db->affected_rows();
		}
		return false;
	}










	public function get_active_authority_types()
	{
		$this->db->where('auth_type_status', 1);
		return $this->db->get($this->authority_types)->result_array();
	}

	public function get_authority_type_by_type($type=null)
	{
		if($type !=  null) {
			$this->db->where('auth_type', trim($type));
			return $this->db->get($this->authority_types)->row_array();
		}
		return false;
	}

}

/* End of file Group_permissions_model.php */
/* Location: ./application/models/Group_permissions_model.php */