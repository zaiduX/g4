<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skill_model extends CI_Model {

	private $_category_master = "tbl_category_master"; // db table declaration
	private $_skills_master = "tbl_skills_master"; // db table declaration

	public function get_skills()
	{	
		$this->db->select('skill.*, cat.cat_name as cat_name');
		$this->db->from("tbl_skills_master as skill");
		$this->db->join('tbl_category_master as cat', 'skill.cat_id = cat.cat_id');
		$this->db->where('skill.skill_status', 1);
		$this->db->group_by('skill.skill_id');
		return $this->db->get($this->_skills_master)->result_array();
	}
	
	public function get_active_category_types()
	{
		$this->db->where('cat_status', 1);
		return $this->db->get($this->_category_master)->result_array();
	}

	public function check_skill($skill_name=null, $cat_id = 0){
		if($skill_name != null AND $cat_id != 0) {
			$this->db->where('skill_name', strtolower(trim($skill_name)));
			$this->db->where('cat_id', (int) $cat_id);
			return $this->db->get($this->_skills_master)->row_array();
		}
		return false;
	}

	public function register_skill(array $data)
	{
		if(is_array($data)){
			$this->db->insert($this->_skills_master, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function delete_skills($id=0)
	{
		if($id > 0 ){
			$this->db->where('skill_id', (int) $id);
			$this->db->delete($this->_skills_master);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_skill_details($id=0)
	{	
		if($id != 0) {
			$this->db->select('skill.*, cat.cat_name as cat_name');
			$this->db->from("tbl_skills_master as skill");
			$this->db->join('tbl_category_master as cat', 'skill.cat_id = cat.cat_id');
			$this->db->where('skill.skill_status', 1);
			$this->db->group_by('skill.skill_id');
			$this->db->where('skill.skill_id', $id);
			return $this->db->get($this->_skills_master)->row_array();
		}
		return false;
	}

	public function check_skill_excluding($skill_id = 0, $skill_name = null, $cat_id = 0) 
	{
		if($skill_name != null && $skill_id > 0 && $cat_id > 0) {
			$this->db->where('skill_name', strtolower(trim($skill_name)));
			$this->db->where('cat_id', (int) $cat_id);
			$this->db->where('skill_id !=', (int) $skill_id);
			return $this->db->get($this->_skills_master)->num_rows();
		}
		return false;
	}

	public function update_skill(array $update_data, $skill_id = 0)
	{
		if(is_array($update_data)){
			$this->db->where('skill_id', (int) $skill_id);
			$this->db->update($this->_skills_master, $update_data);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file Skill_model.php */
/* Location: ./application/models/Skill_model.php */