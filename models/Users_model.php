<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	private $customers_master = "tbl_customers_master";
	private $customer_skills = "tbl_customer_skills";
	private $skills_master = "tbl_skills_master";
	private $skill_levels = "tbl_skill_levels";
	private $_deliverer_profile = "tbl_deliverer_profile";
  	private $_dedicated_customer_gonagoo_commission = "tbl_dedicated_customer_gonagoo_commission";
	private $countries = "tbl_countries"; 
	private $categories = "tbl_category_master";
	private $_courier_orders = "tbl_courier_orders";
  	private $unit_master = "tbl_unit_master";
  	private $dangerous_goods_master = "tbl_dangerous_goods_master";
  	private $_standard_dimension = "tbl_standard_dimension";  
    
    public function total_accounts($type = false, $acc_type=false, $active = false, $status = false )
	{   
	    $today = date('Y-m-d H:i:s');
	    
	    if($type != false){ $type = ($type == "business") ? 0 : 1;  $this->db->where('user_type',$type); }
	    else if($acc_type != false){ $this->db->where('acc_type',strtolower($acc_type)); }
	    else if($active != false){ $active = ($active == 'active') ? 1 : 0; $this->db->where('cust_status',$active);  }
	    else if($status != false && $status == "online"){ $this->db->where("last_login_datetime >= DATE_SUB('$today', INTERVAL 10 MINUTE)");  }
	    
	    
		return $this->db->get($this->customers_master)->num_rows();
	}
    
	public function get_users($country_id=0)
	{
	    if($country_id > 0 ) { $this->db->where('country_id', $country_id); }
		$this->db->where('cust_status', 1);
		return $this->db->get($this->customers_master)->result_array();
	}

	public function get_users_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', $id);
			//$this->db->where('cust_status', 1);
			return $this->db->get($this->customers_master)->row_array();
		}
		return false;
	}

	public function get_deliverer_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', $id);
			return $this->db->get($this->_deliverer_profile)->row_array();
		}
		return false;
	}

	public function activate_users($id=0)
	{
		if($id > 0){
			$this->db->where('cust_id', (int) $id);
			$this->db->where('cust_status', 0);
			$this->db->update($this->customers_master, ['cust_status' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_shipping_mode($deliverer_id=null, $shipping_mode=null)
	{
		if( !is_null($deliverer_id) && !is_null($shipping_mode) ){
			$this->db->where('deliverer_id', (int)$deliverer_id);
			$this->db->update($this->_deliverer_profile, ['shipping_mode' => (int)$shipping_mode]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_users($id=0)
	{
		if($id > 0){
			$this->db->where('cust_id', (int) $id);
			$this->db->where('cust_status', 1);
			$this->db->update($this->customers_master, ['cust_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	//inactive users
	public function get_inactive_users()
	{
		$this->db->where('cust_status', 0);
		return $this->db->get($this->customers_master)->result_array();
	}

	public function get_customer_skills($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', $id);
			return $this->db->get($this->customer_skills)->row_array();
		}
		return false;
	}

	public function get_skill_name($id=0)
	{
		if($id > 0) {
			$this->db->select('skill_name');
			$this->db->where('skill_id', $id);
			return $this->db->get($this->skills_master)->row_array();
		}
		return false;
	}

	public function get_level_name($id=0)
	{
		if($id > 0) {
			$this->db->select('level_title');
			$this->db->where('level_id', $id);
			return $this->db->get($this->skill_levels)->row_array();
		}
		return false;
	}
	
	public function convert_big_int($int = 0){
	    if( $int > 1000 && $int < 1000000){ $int = ($int/1000); return number_format($int ,1,'.',',') . 'K'; }         
	    else if( $int >= 1000000 ){ $int = ($int / 1000000 ); return number_format($int ,1,'.',',') . 'M'; }
	    else{ return $int; }
    }

    public function activate_users_dedicated($id=0)
	{
		if($id > 0){
			$this->db->where('cust_id', (int) $id);
			$this->db->update($this->customers_master, ['is_dedicated' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_users_dedicated($id=0)
	{
		if($id > 0){
			$this->db->where('cust_id', (int) $id);
			$this->db->update($this->customers_master, ['is_dedicated' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function activate_users_payment_type($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', (int) $id);
			$this->db->update($this->customers_master, ['allow_payment_delay' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_users_payment_type($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', (int) $id);
			$this->db->update($this->customers_master, ['allow_payment_delay' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function set_gonagoo_commission_type($cust_id=0, $commission_type=0)
	{
		if($cust_id > 0) {
			$this->db->where('cust_id', (int) $cust_id);
			$this->db->update($this->customers_master, ['commission_type' => (int)$commission_type]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_user_gonagoo_commission_details($cust_id=0)
    {
      if($cust_id > 0) {

      	$this->db->select('p.*, c.country_name, cc.cat_name');
		$this->db->from($this->_dedicated_customer_gonagoo_commission. ' as p');
		$this->db->join($this->countries.' as c', 'c.country_id = p.country_id');
		$this->db->join($this->categories.' as cc', 'cc.cat_id = p.category_id');
        $this->db->where('p.cust_id', (int)$cust_id);
		$this->db->order_by('p.pay_id', 'desc');
		return $this->db->get()->result_array();
      }
      return false;
    }

    public function get_categories()
	{
		$this->db->where('cat_status', 1);
		return $this->db->get($this->categories)->result_array();
	}

	public function get_countries()
    {
      return $this->db->get($this->countries)->result_array();
    }

    public function check_payment(array $data)
	{
		if(is_array($data)){
			$this->db->select('pay_id');
			$this->db->where('category_id', trim($data['category_id']));
			$this->db->where('cust_id', trim($data['cust_id']));
			$this->db->where('country_id', trim($data['country_id']));
			$this->db->where('rate_type', trim($data['rate_type']));
			if($res = $this->db->get($this->_dedicated_customer_gonagoo_commission)->row_array()) { return $res['pay_id']; }
		}
		return false;
	}

	public function register_payment(array $data)
	{
		if(is_array($data)){			
			$this->db->insert($this->_dedicated_customer_gonagoo_commission, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function delete_commission_rate($pay_id=0)
	{
		if($pay_id > 0) {
			$this->db->where('pay_id', (int) $pay_id);
			$this->db->delete($this->_dedicated_customer_gonagoo_commission);
			return $this->db->affected_rows();
		} return false;
	}

	public function get_commission_details($pay_id=0)
	{
		if($pay_id > 0) {
			$this->db->where('pay_id', (int)$pay_id);
			return $this->db->get($this->_dedicated_customer_gonagoo_commission)->row_array();
		} else return false;
	}

	public function get_country_name_by_id($id=0) {        
	    if($id>0){            
	      	$this->db->where('country_id', (int) $id);            
	      	$r = $this->db->get($this->countries)->row_array();            
	      	return $r['country_name'];        
	    }        
	    return false;    
	}

	public function get_category_details_by_id($cat_id=0)
	{
		if($cat_id > 0){
			$this->db->where('cat_id', (int)$cat_id);
			return $this->db->get($this->categories)->row_array();
		} return false;
	}

	public function update_payment_details(array $data, $pay_id=0)
	{
		if(is_array($data) && $pay_id > 0) {
			$this->db->where('pay_id', (int) $pay_id);
			$this->db->update($this->_dedicated_customer_gonagoo_commission, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_dedicated_users_pending_order_list($cust_id=0)
	{
	    if($cust_id > 0 ) { 
	    	$user_details = $this->get_users_detail((int)$cust_id);
	    	//echo json_encode($user_details); die();
	    	if($user_details['allow_payment_delay'] == 1) {
		    	$this->db->where('cust_id', $cust_id); 
		    	$this->db->where('complete_paid', 0); 
		    	$this->db->where('order_status', 'delivered'); 
				return $this->db->get($this->_courier_orders)->result_array();
	    	} else { return false; }
		} return false;
	}

	public function get_dedicated_users_pending_order_currency_list($cust_id=0)
	{
	    if($cust_id > 0 ) { 
	    	$user_details = $this->get_users_detail((int)$cust_id);
	    	//echo json_encode($user_details); die();
	    	if($user_details['allow_payment_delay'] == 1) {
	    		$this->db->select('currency_sign');
		    	$this->db->where('cust_id', $cust_id); 
		    	$this->db->where('complete_paid', 0); 
		    	$this->db->group_by('currency_sign');
		    	$this->db->where('order_status', 'delivered'); 
				return $this->db->get($this->_courier_orders)->result_array();
	    	} else { return false; }
		} return false;
	}

	public function get_dedicated_users_pending_order_list_filter(array $filter_data, $cust_id=0)
	{
	    if(is_array($filter_data) && $cust_id > 0) { 
	    	$user_details = $this->get_users_detail((int)$cust_id);
	    	//echo json_encode($user_details); die();
	    	if($user_details['allow_payment_delay'] == 1) {
		    	$this->db->where('cust_id', $cust_id); 
		    	$this->db->where('complete_paid', 0); 
		    	$this->db->where('order_status', 'delivered'); 
		    	if($filter_data['currency_sign'] != '0') { $this->db->where('currency_sign', $filter_data['currency_sign']); }
		    	if($filter_data['currency_sign'] != '0') { $this->db->where('from_country_id', $filter_data['country_id']); }
		    	if($filter_data['from_date'] != '0') {
		    		$this->db->where('DATE(cre_datetime) >=', $filter_data['from_date']);
		    		$this->db->where('DATE(cre_datetime) <=', $filter_data['to_date']);
		    	}
				return $this->db->get($this->_courier_orders)->result_array();
	    	} else { return false; }
		} return false;
	}

	public function get_dedicated_users_pending_order_sum($cust_id=0)
	{
	    if($cust_id > 0) { 
	    	$user_details = $this->get_users_detail((int)$cust_id);
	    	if($user_details['allow_payment_delay'] == 1) {
	    		$this->db->select('SUM(order_price) AS pending, SUM(commission_amount) AS gcommission');
		    	$this->db->where('cust_id', $cust_id); 
		    	$this->db->where('complete_paid', 0); 
		    	$this->db->where('order_status', 'delivered');
				return $this->db->get($this->_courier_orders)->row_array();
	    	} else { return array('pending' => 0, 'gcommission' => 0); }
		} return array('pending' => 0, 'gcommission' => 0);
	}

	public function get_customer_email_name_by_order_id($id=0)
	{
		if($id > 0) {
			$this->db->select('email1');
			$this->db->where('cust_id', (int)$id);
			$row = $this->db->get($this->customers_master)->row_array();
			$email_cut = explode('@', $row['email1']);
			return ucwords($email_cut[0]);
		}
		return false;
	}

	public function get_country_details($id=0)
	{		
		if($id > 0 ){
			$this->db->where('country_id', (int)$id);
			return $this->db->get($this->countries)->row_array();
		}
		return false;
	}

	public function get_unit($id=0)
	{
		if($id > 0){
			$this->db->where('unit_id', (int) $id);
			return $this->db->get($this->unit_master)->row_array();
		} else { return $this->db->get($this->unit_master)->result_array(); }
	}

	public function get_dangerous_goods($id=0)
	{
		$this->db->where('status', 1);
		if($id > 0 ) {  
			$this->db->where('id', (int) $id); 
			return $this->db->get($this->dangerous_goods_master)->row_array();
		} else {  return $this->db->get($this->dangerous_goods_master)->result_array(); } 
	}

	public function get_dimension_type_name($dimension_id=0)
	{
		if($dimension_id > 0) {
			$this->db->where('dimension_id', $dimension_id);
			$row = $this->db->get($this->_standard_dimension)->row_array();
			return $row['dimension_type'];
		} else return false;
	}

	public function update_order_details(array $data, $order_id=0)
	{
		if($order_id > 0 && is_array($data)) {
			$this->db->where('order_id', trim($order_id));
			$this->db->update($this->_courier_orders, $data);
			return $this->db->affected_rows();
		} return false;
	}

	// IOS push notification 
    public function sendPushIOS(array $data, $tokens)
	{
		$serverKey = trim($this->config->item('delivererAppGoogleKey'));
		$fcmMsg = array(
			'body' => $data['text'],
			'title' => $data['title'],
			'sound' => "default",
		    'color' => "#203E78" 
		);
		$fcmFields = array(
			'to' => $tokens,
            'priority' => 'high',
			'notification' => $fcmMsg
		);
		$headers = array(
			'Authorization: key=' . $serverKey,
			'Content-Type: application/json'
		);
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
		$result = curl_exec($ch );
		curl_close( $ch );		
	}

}

/* End of file Users_model.php */
/* Location: ./application/models/Users_model.php */