<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_currency_model extends CI_Model {

	private $country_currency = "tbl_country_currency"; // db table declaration
	private $countries = "tbl_countries"; // db table declaration
	private $currencies = "tbl_currency_master"; // db table declaration

	public function get_country_currency()
	{
		$this->db->select('cc.cc_id,cc.cc_status, cc.cre_datetime,ct.country_name,c.currency_id, c.currency_sign,c.currency_title');
		$this->db->from('tbl_country_currency as cc');
		$this->db->join('tbl_countries as ct', 'cc.country_id = ct.country_id');
		$this->db->join('tbl_currency_master as c', 'cc.currency_id = c.currency_id');
		// $this->db->where('cc.cc_status', 1);
		$this->db->group_by('cc.cc_id');
		return $this->db->get($this->country_currency)->result_array();
	}

	public function get_currency_except_by_id($currency_id=0, $country_id=0)
	{
		if($currency_id > 0 AND $country_id > 0 ) {
			$this->db->select('currency_id, currency_title, currency_sign');
			$this->db->where('currency_status', 1);
			$this->db->where('currency_id NOT IN ( SELECT currency_id FROM tbl_country_currency WHERE country_id = '. $country_id . ')');
			return $this->db->get($this->currencies)->result_array();
		}
		return false;
	}

	public function get_country_currency_detail($id=0)
	{
		if($id > 0) {
			$this->db->select('cc.cc_id, ct.country_id, ct.country_name, cr.currency_title, cr.currency_id, cr.currency_title, cr.currency_sign');
			$this->db->from('tbl_country_currency cc'); 
			$this->db->join('tbl_countries ct', 'ct.country_id = cc.country_id');
			$this->db->join('tbl_currency_master cr', 'cr.currency_id = cc.currency_id');
			$this->db->where('cc.cc_status', 1);
			$this->db->where('cc.cc_id', $id);
 			$this->db->group_by('cc.cc_id');
			return $this->db->get($this->country_currency)->row_array();
		}
		return false;
	}

	public function check_currency_existance($currency_id=0, $country_id=0)
	{
		if($country_id > 0) {
			//$this->db->where('currency_id', (int)($currency_id));
			$this->db->where('country_id', (int)($country_id));
			return $this->db->get($this->country_currency)->row_array();
		}
		return false;
	}

	public function update_country_currency(array $data)
	{
		if(is_array($data)){
			$this->db->where('cc_id', (int)$data['cc_id']);
			$this->db->update($this->country_currency, array('currency_id' => (int) $data['currency_id']));
			return $this->db->affected_rows();
		}
		return false;
	}

	public function activate_country_currency($id=0)
	{
		if($id > 0){
			$this->db->where('cc_id', (int) $id);
			$this->db->where('cc_status', 0);
			$this->db->update($this->country_currency, ['cc_status' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate_country_currency($id=0)
	{
		if($id > 0){
			$this->db->where('cc_id', (int) $id);
			$this->db->where('cc_status', 1);
			$this->db->update($this->country_currency, ['cc_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function register_country_currency($currency_id=0, $country_id=0)
	{
		if($currency_id > 0 AND $country_id > 0){
			$today = date('Y-m-d H:i:s');
			$insert_data = array(
				'currency_id' => $currency_id,
				'country_id' => $country_id, 
				'cre_datetime' => $today,
				'cc_status' => 1
			);
			$this->db->insert($this->country_currency, $insert_data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_countries()
	{
		return $this->db->get($this->countries)->result_array();
	}

	public function get_currencies()
	{
		return $this->db->get($this->currencies)->result_array();
	}

}

/* End of file Country_currency_model.php */
/* Location: ./application/models/Country_currency_model.php */