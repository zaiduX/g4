<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dangerous_goods_model extends CI_Model {

  private $dangerous_goods_master = "tbl_dangerous_goods_master";  

  public function get_dangerous_goods($id=0)
  {
    if($id > 0 ){  
      $this->db->where('id', $id);
      return $this->db->get($this->dangerous_goods_master)->row_array();  
    }
    else{ return $this->db->get($this->dangerous_goods_master)->result_array(); }
  }

  public function check_existance(array $data) 
  {
    if(is_array($data) && !empty($data)){
      $this->db->where('code', strtoupper($data['code']));
      $this->db->where('name', $data['name']);
      return $this->db->get($this->dangerous_goods_master)->row_array();
    }
    return false;
  }

  public function check_existance_for_update(array $data,$id=0) 
  {
    if(is_array($data) && !empty($data) && $id > 0){
      $q = "SELECT * FROM `tbl_dangerous_goods_master` WHERE `id` != '".$id."' AND ( `code` = '".$data['code']."' OR `name` = '".$data['name']."')"; 
      return$this->db->query($q)->row_array();
    }
    return false;
  }

  public function register_goods(array $data)
  {
    if(is_array($data) && !empty($data)){
      $this->db->insert($this->dangerous_goods_master, $data);
      return $this->db->insert_id();
    } 
    return false;  
  }

  public function update_goods(array $data, $id=0)
  {
    if(is_array($data) && !empty($data) && $id>0){
      $this->db->where('id', $id);
      $this->db->update($this->dangerous_goods_master, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

}

/* End of file dangerous_goods_model.php */
/* Location: ./application/models/dangerous_goods_model.php */