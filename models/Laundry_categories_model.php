<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laundry_categories_model extends CI_Model {

	private $category = "tbl_laundry_category"; // db table declaration
	private $sub_category = "tbl_laundry_sub_category"; // db table declaration
	
	public function get_sub_categories()
	{
		$this->db->order_by('sub_cat_id', 'desc');
		return $this->db->get($this->sub_category)->result_array();
	}	
	
	public function get_categories()
	{
		$this->db->order_by('cat_id', 'desc');
		return $this->db->get($this->category)->result_array();
	}	

	public function get_category_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('cat_id', (int) $id);
			return $this->db->get($this->category)->row_array();
		}
		return false;
	}

	public function get_sub_category_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('sub_cat_id', (int) $id);
			return $this->db->get($this->sub_category)->row_array();
		}
		return false;
	}

	public function check_sub_category(array $data)
	{
		if(is_array($data)){
			$this->db->select('sub_cat_id');
			$this->db->where('sub_cat_name', trim($data['sub_cat_name']));
			$this->db->where('cat_id', trim($data['cat_id']));
			if($res = $this->db->get($this->sub_category)->row_array()){ return $res['sub_cat_id']; }
		}
		return false;
	}

	public function check_sub_category_excluding(array $data)
	{
		if(is_array($data)){
			$this->db->select('sub_cat_id');
			$this->db->where('sub_cat_name', trim($data['sub_cat_name']));
			$this->db->where('sub_cat_id !=', trim($data['sub_cat_id']));
			$this->db->where('cat_id', trim($data['cat_id']));
			if($res = $this->db->get($this->sub_category)->row_array()){ return $res['sub_cat_id']; }
		}
		return false;
	}

	public function register_sub_category(array $data)
	{
		if(is_array($data)){			
			$this->db->insert($this->sub_category, $data);
			return $this->db->insert_id();
		} return false;
	}

	public function update_sub_category(array $data, $id=0)
	{
		if(is_array($data) AND $id > 0){
			$this->db->where('sub_cat_id', (int)$id);
			$this->db->update($this->sub_category, $data);
			return $this->db->affected_rows();
		} return false;
	}

	public function activate_sub_category($id=0)
	{
		if($id > 0){
			$this->db->where('sub_cat_id', (int) $id);
			$this->db->where('sub_cat_status', 0);
			$this->db->update($this->sub_category, ['sub_cat_status' => 1]);
			return $this->db->affected_rows();
		} return false;
	}

	public function inactivate_sub_category($id=0)
	{
		if($id > 0){
			$this->db->where('sub_cat_id', (int) $id);
			$this->db->where('sub_cat_status', 1);
			$this->db->update($this->sub_category, ['sub_cat_status' => 0]);
			return $this->db->affected_rows();
		} return false;
	}

	public function get_category_details($id=0)
	{
		if($id > 0) {
			$this->db->where('cat_id', (int)$id);
			return $this->db->get($this->category)->row_array();
		} else false;
	}

	public function update_main_category(array $data, $id=0)
	{
		if(is_array($data) AND $id > 0){
			$this->db->where('cat_id', (int)$id);
			$this->db->update($this->category, $data);
			return $this->db->affected_rows();
		} return false;
	}

}

/* End of file Laundry_categories_model.php */
/* Location: ./application/models/Laundry_categories_model.php */