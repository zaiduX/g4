<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cancellation_rescheduling_bus_reservation_model extends CI_Model {

	private $bus_cancellation_rescheduling = "tbl_bus_cancellation_rescheduling"; 
	private $countries = "tbl_countries"; 
	private $categories = "tbl_category_master";
	private $_transport_vehicle_master = "tbl_transport_vehicle_master";
	
	public function get_countries()
	{
		return $this->db->get($this->countries)->result_array();
	}

	public function get_list()
	{
		$this->db->select('b.*, c.country_name');
		$this->db->from($this->bus_cancellation_rescheduling. ' as b');
		$this->db->join($this->countries.' as c', 'c.country_id = b.country_id');
		$this->db->order_by('b.bcr_id', 'desc');
		return $this->db->get()->result_array();
	}

    public function get_category_vehicles_master()
    {
      $this->db->where('cat_id', 281);
      return $this->db->get($this->_transport_vehicle_master)->result_array();
    } 

	public function get_vehicles_by_type_id($vehical_type_id=0)
	{
		if($vehical_type_id > 0){
		  $this->db->where('vehical_type_id', (int) $vehical_type_id);
		  return $this->db->get($this->_transport_vehicle_master)->row_array();
		}
		return false;
	}

	public function check_payment(array $data, $flag=null)
	{
		if(is_array($data) && $flag != null && !empty($flag)){
			$this->db->select('bcr_id');
			$this->db->where('country_id', trim($data['country_id']));
			$this->db->where('vehical_type_id', trim($data['vehical_type_id']));

			if($flag == 'min') { $bcr_min_hours = $data['bcr_min_hours']; $this->db->where("$bcr_min_hours BETWEEN bcr_min_hours AND bcr_max_hours"); }
      		else if($flag == 'max') { $bcr_max_hours = $data['bcr_max_hours'];  $this->db->where("$bcr_max_hours BETWEEN bcr_min_hours AND bcr_max_hours"); }

			if($res = $this->db->get($this->bus_cancellation_rescheduling)->row_array()) { return true; }
		}
		return false;
	}

	public function register_payment(array $data)
	{
		if(is_array($data)){			
			$this->db->insert($this->bus_cancellation_rescheduling, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_cancellation_detail($id=0)
	{
		if($id > 0) {
            $this->db->select('p.*, c.country_name');
            $this->db->from($this->bus_cancellation_rescheduling. ' as p');
	        $this->db->join($this->countries.' as c', 'c.country_id = p.country_id');
			$this->db->where('p.bcr_id', (int) $id);
			return $this->db->get()->row_array();
		}
		return false;
	}

	public function update_cencellation(array $data, $id=0)
	{
		if(is_array($data) AND $id > 0){
			$this->db->where('bcr_id', (int)$id);
			$this->db->update($this->bus_cancellation_rescheduling, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function check_payment_expect_self(array $data, $flag=null, $bcr_id=0)
	{
		if(is_array($data) && $flag != null && !empty($flag) && $bcr_id > 0){
			$this->db->select('bcr_id');
			$this->db->where('country_id', (int)$data['country_id']);
			$this->db->where('bcr_id !=', (int)$bcr_id);
			$this->db->where('vehical_type_id', trim($data['vehical_type_id']));

			if($flag == 'min') { $bcr_min_hours = $data['bcr_min_hours']; $this->db->where("$bcr_min_hours BETWEEN bcr_min_hours AND bcr_max_hours"); }
      		else if($flag == 'max') { $bcr_max_hours = $data['bcr_max_hours'];  $this->db->where("$bcr_max_hours BETWEEN bcr_min_hours AND bcr_max_hours"); }

			if($res = $this->db->get($this->bus_cancellation_rescheduling)->row_array()) { return true; }
		}
		return false;
	}

	public function delete_details($id=0)
	{
		if($id > 0 ){
			$this->db->where('bcr_id', (int) $id);
			$this->db->delete($this->bus_cancellation_rescheduling);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file Cancellation_rescheduling_bus_reservation_model.php */
/* Location: ./application/models/Cancellation_rescheduling_bus_reservation_model.php */