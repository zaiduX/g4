<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gonagoo_address_model extends CI_Model {

  
  private $_insurance_master = "tbl_insurance_master"; // db table declaration
  private $_country_currency = "tbl_country_currency"; // db table declaration
  private $_currency_master = "tbl_currency_master"; // db table declaration
  private $_categories = "tbl_category_master";


  private $_gonagoo_address = "tbl_gonagoo_address"; // db table declaration
  private $_countries = "tbl_countries"; // db table declaration
  private $_states = "tbl_states"; // db table declaration
  private $_cities = "tbl_cities"; // db table declaration


  public function get_country_name_by_id($id=0)
  { 
    if($id>0){
      $this->db->where('country_id', (int) $id);
      $r = $this->db->get($this->_countries)->row_array();   
      return $r['country_name'];
    }
    return false;
  }

  public function get_addresses($id=0)
  { 
    $this->db->select('a.*, c.country_name');
    $this->db->from($this->_gonagoo_address. ' as a');
    $this->db->join($this->_countries.' as c', 'c.country_id = a.country_id');
    if($id > 0 ){ 
      $this->db->where('addr_id', $id);
      return $this->db->get()->row_array();      
    } else{
      $this->db->order_by('a.addr_id', 'desc');
      return $this->db->get()->result_array();      
    }
  }

  public function check_country_address_exists($id=0)
  { 
    if($id>0){
      $this->db->where('country_id', (int) $id);  
      return $this->db->get($this->_gonagoo_address)->num_rows();
    }
    return false;
  }
  
  public function get_countries()
  {
    return $this->db->get($this->_countries)->result_array();
  }
  
  public function get_states($id=0)
  {
    if($id>0) { $this->db->where('country_id', $id); }
    return $this->db->get($this->_states)->result_array();
  }
  
  public function get_cities($id=0)
  {
    if($id>0) { $this->db->where('state_id', $id); }
    return $this->db->get($this->_cities)->result_array();
  }

  public function register_address(array $data)
  { 
    if(is_array($data)){
      $this->db->insert($this->_gonagoo_address, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_gonagoo_address(array $update_data, $addr_id = 0)
  { 
    if(is_array($update_data)){
      $this->db->where('addr_id', (int) $addr_id);
      $this->db->update($this->_gonagoo_address, $update_data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_gonagoo_address($id=0)
  { 
    if($id > 0 ){
      $this->db->where('addr_id', (int) $id);
      $this->db->delete($this->_gonagoo_address);
      return $this->db->affected_rows();
    }
    return false;
  }

}

/* End of file Gonagoo_address_model.php */
/* Location: ./application/models/Gonagoo_address_model.php */