<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider_offer_manage_model extends CI_Model {
	private $_category_master = "tbl_category_master";
	private $_category_type_master = "tbl_category_type_master";
	private $_countries = "tbl_countries";
	private $_smp_offers = "tbl_smp_offers";
	private $_customers_master = 'tbl_customers_master';
	private $_smp_freelancer_profile = 'tbl_smp_freelancer_profile';
	private $_smp_job_review = 'tbl_smp_job_review';
	private $_smp_offer_addons = 'tbl_smp_offer_addons';

	public function get_service_provider_offers($status=0) {
		if($status >= 0 && $status <= 2) {
	    $this->db->select('c.cat_name, ct.cat_type, country.country_name, pro.company_name, pro.firstname, pro.lastname, offer.*');
	    $this->db->from($this->_category_master.' as c');
	    $this->db->from($this->_category_type_master.' as ct');
	    $this->db->from($this->_countries.' as country');
	    $this->db->from($this->_smp_freelancer_profile.' as pro');
	    $this->db->join($this->_smp_offers.' as offer', 'offer.cat_id = c.cat_id AND offer.cat_type_id = ct.cat_type_id AND offer.country_id = country.country_id AND offer.cust_id = pro.cust_id');
	    $this->db->where('offer.admin_approval', (int)$status);
	    $this->db->order_by('offer.offer_id', 'desc');
	    $this->db->group_by('offer.offer_id');
	    return $this->db->get($this->_smp_offers)->result_array();
	  } else { return false; }
  }
  public function update_offer_details($offer_id=0, array $data) {
    if($offer_id > 0 AND is_array($data)){     
      $this->db->where('offer_id', $offer_id);
      $this->db->update($this->_smp_offers, $data);
      return $this->db->affected_rows();
    } return false;
  }
  public function get_service_provider_offer_details($offer_id=0) {
    if($offer_id > 0) {
      $this->db->select('c.cat_name, ct.cat_type, country.country_name, offer.*');
      $this->db->from($this->_category_master.' as c');
      $this->db->from($this->_category_type_master.' as ct');
      $this->db->from($this->_countries.' as country');
      $this->db->join($this->_smp_offers.' as offer', 'offer.cat_id = c.cat_id AND offer.cat_type_id = ct.cat_type_id AND offer.country_id = country.country_id AND offer.offer_id = ' . (int)($offer_id));
      return $this->db->get($this->_smp_offers)->row_array();
    } return false;
  }
  public function get_offer_reviews($offer_id=0) {
    if($offer_id > 0) {
      $this->db->select('cust.firstname AS cust_fname, cust.lastname AS cust_lname, cust.country_id AS cust_country_id, cust.avatar_url AS cust_avatar_url, pro.firstname AS pro_fname, pro.lastname AS pro_lname, free.firstname AS free_fname, free.lastname AS free_lname, free.avatar_url AS free_avatar_url, free.company_name AS free_company_name, review.*');
      $this->db->from($this->_customers_master.' as cust');
      $this->db->from($this->_customers_master.' as pro');
      $this->db->from($this->_smp_freelancer_profile.' as free');
      $this->db->from($this->_countries.' as contry');
      $this->db->join($this->_smp_job_review.' as review', 'review.cust_id = cust.cust_id AND review.provider_id = pro.cust_id AND review.provider_id = free.cust_id AND review.offer_id = ' . (int)($offer_id));
      $this->db->order_by('review.review_id', 'desc');
      $this->db->group_by('review.review_id');
      return $this->db->get($this->_smp_job_review)->result_array();
    } return false;    
  }
  public function get_service_provider_profile($cust_id=0) {
    if($cust_id > 0 ) {
      $this->db->where('cust_id', (int) $cust_id);
      return $this->db->get($this->_smp_freelancer_profile)->row_array();
    } return false;
  }
  public function get_country_name_by_id($id=0) {
    if($id>0){            
      $this->db->where('country_id', (int) $id);            
      $r = $this->db->get($this->_countries)->row_array();            
      return $r['country_name'];        
    } return false;
  }
  public function get_offer_add_ons($offer_id=0) {
    if($offer_id > 0) {
      $this->db->where('status', 1);
      $this->db->where('offer_id', (int)$offer_id);            
      return $this->db->get($this->_smp_offer_addons)->result_array();            
    } return false;    
  }
}

/* End of file Provider_offer_manage_model.php */
/* Location: ./application/models/Provider_offer_manage_model.php */