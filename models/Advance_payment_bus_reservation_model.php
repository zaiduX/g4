<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advance_payment_bus_reservation_model extends CI_Model {

	private $payment = "tbl_bus_advance_payment"; 
	private $countries = "tbl_countries"; 
	private $categories = "tbl_category_master";
	
	public function get_countries()
	{
		return $this->db->get($this->countries)->result_array();
	}

	public function get_payments()
	{
		$this->db->select('p.*, c.country_name');
		$this->db->from($this->payment. ' as p');
		$this->db->join($this->countries.' as c', 'c.country_id = p.country_id');
		$this->db->order_by('p.bap_id', 'desc');
		return $this->db->get()->result_array();
	}	

	public function check_payment(array $data)
	{
		if(is_array($data)){
			$this->db->select('bap_id');
			$this->db->where('country_id', trim($data['country_id']));
			if($res = $this->db->get($this->payment)->row_array()){ return $res['bap_id']; }
		}
		return false;
	}

	public function register_payment(array $data)
	{
		if(is_array($data)){			
			$this->db->insert($this->payment, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_payment_detail($id=0)
	{
		if($id > 0) {
            $this->db->select('p.*, c.country_name');
            $this->db->from($this->payment. ' as p');
	        $this->db->join($this->countries.' as c', 'c.country_id = p.country_id');
			$this->db->where('p.bap_id', (int) $id);
			return $this->db->get()->row_array();
		}
		return false;
	}

	public function update_payment(array $data, $id=0)
	{
		if(is_array($data) AND $id > 0){
			$this->db->where('bap_id', (int)$id);
			$this->db->update($this->payment, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function delete_payment($id=0)
	{
		if($id > 0 ){
			$this->db->where('bap_id', (int) $id);
			$this->db->delete($this->payment);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file Advance_payment_bus_reservation_model.php */
/* Location: ./application/models/Advance_payment_bus_reservation_model.php */