<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currency_model extends CI_Model {

	private $currency_master = "tbl_currency_master"; // db table declaration
	
	public function get_currencies()
	{
		// $this->db->where('currency_status', 1);
		return $this->db->get($this->currency_master)->result_array();
	}

	public function get_currency_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('currency_id', $id);
			// $this->db->where('currency_status', 1);
			return $this->db->get($this->currency_master)->row_array();
		}
		return false;
	}

	public function get_currency_verification($title=0, $sign=0)
	{
		if($title != '' && $sign != '') {
			$this->db->where('currency_title', $title, 'OR currency_sign', $sign);
			$result = $this->db->get($this->currency_master)->row_array();
			if($result) { return true; }
		}
		return false;
	}

	public function update_currency_details(array $data)
	{
		if(is_array($data)){
			$this->db->where('currency_id', (int)$data['currency_id']);
			$this->db->where('currency_title !=', trim($data['currency_title']));
			$this->db->update($this->currency_master, ['currency_title' => trim($data['currency_title']), 'currency_sign' => trim($data['currency_sign']), 'currency_details' => trim($data['currency_details'])]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function register_currency(array $data)
	{
		if(is_array($data)){
			//var_dump($data); exit();
			$this->db->insert($this->currency_master, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function activate($id=0)
	{
		if($id > 0){
			$this->db->where('currency_id', (int) $id);
			$this->db->where('currency_status', 0);
			$this->db->update($this->currency_master, ['currency_status' => 1]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function inactivate($id=0)
	{
		if($id > 0){
			$this->db->where('currency_id', (int) $id);
			$this->db->where('currency_status', 1);
			$this->db->update($this->currency_master, ['currency_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file Currency_model.php */
/* Location: ./application/models/Currency_model.php */