<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insurance_model extends CI_Model {

  
  private $_insurance_master = "tbl_insurance_master"; // db table declaration
  private $_countries = "tbl_countries"; // db table declaration
  private $_country_currency = "tbl_country_currency"; // db table declaration
  private $_currency_master = "tbl_currency_master"; // db table declaration
  private $_categories = "tbl_category_master";

  public function get_insurance($id=0)
  {     
    $this->db->select('p.*, c.country_name, cc.cat_name');
    $this->db->from($this->_insurance_master. ' as p');
    $this->db->join($this->_countries.' as c', 'c.country_id = p.country_id');
    $this->db->join($this->_categories.' as cc', 'cc.cat_id = p.category_id');
    if($id > 0 ){ 
      $this->db->where('ins_id', $id);
      return $this->db->get()->row_array();      
    }
    else{
      $this->db->order_by('p.ins_id', 'desc');
      return $this->db->get()->result_array();      
    }
  }
  
  public function get_countries()
  {
    return $this->db->get($this->_countries)->result_array();
  }

  public function check_min_insurance(array $data){
    if(is_array($data)) {
      $this->db->where('country_id', $data['country_id']);
      $this->db->where('category_id', $data['category_id']);
      $this->db->where($data['min_value']." BETWEEN min_value AND max_value");      
      return $this->db->get($this->_insurance_master)->num_rows();
    }
    return false;
  }

  public function check_max_insurance(array $data){
    if(is_array($data)) {
      $this->db->where('country_id', $data['country_id']);
      $this->db->where('category_id', $data['category_id']);      
      $this->db->where($data['max_value']." BETWEEN min_value AND max_value");
      return $this->db->get($this->_insurance_master)->num_rows();
    }
    return false;
  }

  public function register_insurance(array $data)
  {
    if(is_array($data)){
      $this->db->insert($this->_insurance_master, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function get_country_details($country_id=0)
  { 
    if($country_id > 0) {
      $this->db->where('country_id', (int)$country_id);
      return $this->db->get($this->_countries)->row_array();
    }
    return false;
  }

  public function get_country_currency_details($country_id=0)
  { 
    if($country_id > 0) {
      $this->db->where('country_id', (int)$country_id);
      return $this->db->get($this->_country_currency)->row_array();
    }
    return false;
  }

  public function get_currency_details($currency_id=0)
  { 
    if($currency_id > 0) {
      $this->db->where('currency_id', (int)$currency_id);
      return $this->db->get($this->_currency_master)->row_array();
    }
    return false;
  }

  public function delete_insurance($id=0)
  {
    if($id > 0 ){
      $this->db->where('ins_id', (int) $id);
      $this->db->delete($this->_insurance_master);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_insurance_details($id=0)
  { 
    if($id != 0) {
      $this->db->where('ins_id', $id);
      return $this->db->get($this->_insurance_master)->row_array();
    }
    return false;
  }

  public function check_insurance_excluding(array $data, $id=0){
    if(is_array($data)) {      
      $this->db->where('ins_id', (int) $id);
      $this->db->where('min_value', $data['min_value']);
      $this->db->where('max_value', $data['max_value']);
      $this->db->where('ins_fee', $data['ins_fee']);
      $this->db->where('commission_percent', $data['commission_percent']);
      return $this->db->get($this->_insurance_master)->num_rows();
    }
    return false;
  }

  public function update_insurance(array $update_data, $ins_id = 0)
  {
    if(is_array($update_data)){
      $this->db->where('ins_id', (int) $ins_id);
      $this->db->update($this->_insurance_master, $update_data);
      return $this->db->affected_rows();
    }
    return false;
  }

}

/* End of file Insurance_model.php */
/* Location: ./application/models/Insurance_model.php */