<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport_vehicles_model extends CI_Model {

	private $vehicles = "tbl_transport_vehicle_master"; // db table declaration
	private $_bus_seat_type_master = "tbl_bus_seat_type_master"; // db table declaration
	
	public function get_vehicles()
	{
		$this->db->order_by('vehical_type_id', 'desc');
		return $this->db->get($this->vehicles)->result_array();
	}	

	public function check_vehicles(array $data)
	{
		if(is_array($data)){
			$this->db->select('vehical_type_id');
			$this->db->where('transport_mode', trim($data['transport_mode']));
			$this->db->where('vehicle_type', trim($data['vehicle_type']));
			$this->db->where('cat_id', trim($data['cat_id']));
			if($res = $this->db->get($this->vehicles)->row_array()){ return $res['vehical_type_id']; }
		}
		return false;
	}

	public function register_vehicles(array $data)
	{
		if(is_array($data)){			
			$this->db->insert($this->vehicles, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_vehicles_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('vehical_type_id', (int) $id);
			return $this->db->get($this->vehicles)->row_array();
		}
		return false;
	}

	public function update_vehicles(array $data, $id=0)
	{
		if(is_array($data) AND $id > 0){
			$this->db->where('vehical_type_id', (int)$id);
			$this->db->update($this->vehicles, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function delete_vehicles($id=0)
	{
		if($id > 0 ){
			$this->db->where('vehical_type_id', (int) $id);
			$this->db->delete($this->vehicles);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_vehicles_seat_type_master($vehical_type_id=0)
	{
		if($vehical_type_id > 0) {
			$this->db->where('vehical_type_id', (int) $vehical_type_id);
			return $this->db->get($this->_bus_seat_type_master)->result_array();
		}
		return false;
	}

	public function register_vehicle_seat_type(array $data)
	{
		if(is_array($data)){			
			$this->db->insert($this->_bus_seat_type_master, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function delete_vehicle_seat_type($st_id=0)
	{
		if($st_id > 0 ){
			$this->db->where('st_id', (int) $st_id);
			$this->db->delete($this->_bus_seat_type_master);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file Transport_vehicles_model.php */
/* Location: ./application/models/Transport_vehicles_model.php */