<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify_docs_model extends CI_Model {

	private $_customers_master = "tbl_customers_master";
	private $_deliverer_documents = "tbl_deliverer_documents";
	private $_customer_skills = "tbl_customer_skills";
	private $_countries = "tbl_countries";
	private $_states = "tbl_states";
	private $_cities = "tbl_cities";

	public function get_user_documents($admin_country_id=0)
	{
	    $this->db->select('c.*, d.*');
	    $this->db->from($this->_customers_master.' as c');
	    $this->db->join($this->_deliverer_documents.' as d', 'c.cust_id = d.cust_id');
            if($admin_country_id > 0 ){ $this->db->where('c.country_id', $admin_country_id); }
	    //$this->db->where('d.is_verified', 0);
	    //$this->db->where('d.is_rejected', 0);
	    $this->db->group_by('d.doc_id');
	    return $this->db->get()->result_array();
	}

	public function get_user_rejected_documents()
	{
		$this->db->select('c.*, d.doc_id, d.attachement_url, d.doc_type');
	    $this->db->from($this->_customers_master.' as c');
	    $this->db->join($this->_deliverer_documents.' as d', 'c.cust_id = d.cust_id');
	    $this->db->where('d.is_rejected', 1);
	    $this->db->group_by('d.doc_id');
		return $this->db->get()->result_array();
	}

	public function get_users_detail($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', $id);
			$this->db->where('cust_status', 1);
			return $this->db->get($this->_customers_master)->row_array();
		}
		return false;
	}

	public function get_customer_skills($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', $id);
			return $this->db->get($this->_customer_skills)->row_array();
		}
		return false;
	}
	
	public function get_country_detail($id=0)
	{		
		if($id > 0 ){
			$this->db->where('country_id', (int)$id);
			return $this->db->get($this->_countries)->row_array();
		}
		return false;
	}

	public function get_state_detail($id=0)
	{		
		if($id > 0 ){
			$this->db->where('state_id', (int)$id);
			return $this->db->get($this->_states)->row_array();
		}
		return false;
	}

	public function get_city_detail($id=0)
	{		
		if($id > 0 ){
			$this->db->where('city_id', (int)$id);
			return $this->db->get($this->_cities)->row_array();
		}
		return false;
	}

	public function verify_document($id=0)
	{
		if($id > 0){
                        $today = date('Y-m-d H:i:s');
			$this->db->where('doc_id', (int) $id);
			$this->db->update($this->_deliverer_documents, ['is_verified' => 1,'verify_datetime'=>$today]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function reject_document($id=0)
	{
		if($id > 0){
                        $today = date('Y-m-d H:i:s');
			$this->db->where('doc_id', (int) $id);
			$this->db->update($this->_deliverer_documents, ['is_verified' => 0, 'is_rejected' => 1, 'verify_datetime'=>$today]);
			return $this->db->affected_rows();
		}
		return false;
	}






	public function inactivate_users($id=0)
	{
		if($id > 0){
			$this->db->where('cust_id', (int) $id);
			$this->db->where('cust_status', 1);
			$this->db->update($this->_customers_master, ['cust_status' => 0]);
			return $this->db->affected_rows();
		}
		return false;
	}

	//inactive users
	public function get_inactive_users()
	{
		$this->db->where('cust_status', 0);
		return $this->db->get($this->_customers_master)->result_array();
	}

	public function get_skill_name($id=0)
	{
		if($id > 0) {
			$this->db->select('skill_name');
			$this->db->where('skill_id', $id);
			return $this->db->get($this->skills_master)->row_array();
		}
		return false;
	}

	public function get_level_name($id=0)
	{
		if($id > 0) {
			$this->db->select('level_title');
			$this->db->where('level_id', $id);
			return $this->db->get($this->skill_levels)->row_array();
		}
		return false;
	}

}

/* End of file Verify_docs_model.php */
/* Location: ./application/models/Verify_docs_model.php */