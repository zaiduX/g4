<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_custom_model extends CI_Model {

	private $payment = "tbl_courier_advance_payment"; 
	private $countries = "tbl_countries"; 
	private $categories = "tbl_category_master";
	private $custom_percentage = "tbl_custom_percentage";
	
	public function get_custom_percentage()
	{
		$this->db->select('p.*, c.country_name, cc.cat_name');
		$this->db->from($this->custom_percentage. ' as p');
		$this->db->join($this->countries.' as c', 'c.country_id = p.country_id');
		$this->db->join($this->categories.' as cc', 'cc.cat_id = p.category_id');
		$this->db->order_by('p.custom_id', 'desc');
		return $this->db->get()->result_array();
	}	

	public function check_custom_percentage(array $data)
	{
		if(is_array($data)){
			$this->db->select('custom_id');
			$this->db->where('category_id', trim($data['category_id']));
			$this->db->where('country_id', trim($data['country_id']));
			if($res = $this->db->get($this->custom_percentage)->row_array()){ return $res['custom_id']; }
		}
		return false;
	}

	public function register_custom_percentage(array $data)
	{
		if(is_array($data)){			
			$this->db->insert($this->custom_percentage, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_custom_percentage_detail($id=0)
	{
		if($id > 0) {
      $this->db->select('p.*, c.country_name, cc.cat_name');
      $this->db->from($this->custom_percentage. ' as p');
      $this->db->join($this->countries.' as c', 'c.country_id = p.country_id');
      $this->db->join($this->categories.' as cc', 'cc.cat_id = p.category_id');
			$this->db->where('p.custom_id', (int) $id);
			return $this->db->get()->row_array();
		}
		return false;
	}

	public function update_custom_percentage(array $data, $id=0)
	{
		if(is_array($data) AND $id > 0){
			$this->db->where('custom_id', (int)$id);
			$this->db->update($this->custom_percentage, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function delete_custom_percentage($id=0)
	{
		if($id > 0 ){
			$this->db->where('custom_id', (int) $id);
			$this->db->delete($this->custom_percentage);
			return $this->db->affected_rows();
		}
		return false;
	}

}

/* End of file country_custom_model.php */
/* Location: ./application/models/country_custom_model.php */