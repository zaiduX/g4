<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class profile_subscription_model extends CI_Model {

	private $_smp_profile_subscription_master = "tbl_smp_profile_subscription_master";
	private $_countries = "tbl_countries";
	
	public function get_subscription() {
		$this->db->select('country.country_name, subs.*');
		$this->db->from($this->_countries.' as country');
		$this->db->join($this->_smp_profile_subscription_master.' as subs', 'subs.country_id = country.country_id AND subs.status = 1');
		$this->db->group_by('subs.subscription_id');
		$this->db->order_by('subs.country_id', 'asc');
		return $this->db->get($this->_smp_profile_subscription_master)->result_array();
	}
	public function get_countries() {
		return $this->db->get($this->_countries)->result_array();
	}
	public function get_country_currencies($country_id=0) {
      if($country_id > 0 ) {
        $this->db->select('cc.currency_id, cr.currency_sign, cr.currency_title');
        $this->db->from('tbl_country_currency as cc');
        $this->db->join('tbl_currency_master as cr', 'cr.currency_id = cc.currency_id');
        $this->db->where('cc.country_id', (int) $country_id);
        return $this->db->get()->result_array();
      } return false;
    }
    public function register_subscription(array $data) {
		if(is_array($data)){			
			$this->db->insert($this->_smp_profile_subscription_master, $data);
			return $this->db->insert_id();
		} return false;
	}
	public function get_subscription_detail($subscription_id=0) {
		if($subscription_id > 0) {
			$this->db->select('country.country_name, subs.*');
			$this->db->from($this->_countries.' as country');
			$this->db->join($this->_smp_profile_subscription_master.' as subs', 'subs.country_id = country.country_id AND subs.subscription_id = '.$subscription_id);
			return $this->db->get()->row_array();
		} return false;
	}
	public function update_subscription(array $data, $subscription_id=0) {
		if(is_array($data) AND $subscription_id > 0){
			$this->db->where('subscription_id', (int)$subscription_id);
			$this->db->update($this->_smp_profile_subscription_master, $data);
			return $this->db->affected_rows();
		} return false;
	}
	public function delete_subscription($subscription_id=0) {
		if($subscription_id > 0 ) {
			$this->db->where('subscription_id', (int) $subscription_id);
			$this->db->update($this->_smp_profile_subscription_master, ['status' => 0]);
			return $this->db->affected_rows();
		} return false;
	}
}

/* End of file profile_subscription_model.php */
/* Location: ./application/models/profile_subscription_model.php */