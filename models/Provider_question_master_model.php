<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider_question_master_model extends CI_Model {
	private $_smp_questions_know_freelancer_master = "tbl_smp_questions_know_freelancer_master";
	private $_category_master = "tbl_category_master";
	private $_category_type_master = "tbl_category_type_master";

	public function get_questions_group_by_cat_id() {
		$this->db->select('c.cat_name, ct.cat_type, q.*, COUNT(DISTINCT(q.question_id)) AS question_count');
		$this->db->from($this->_category_master.' as c');
		$this->db->from($this->_category_type_master.' as ct');
		$this->db->join($this->_smp_questions_know_freelancer_master.' as q', 'q.cat_id = c.cat_id AND q.cat_type_id = ct.cat_type_id');
		$this->db->group_by('q.cat_id');
		$this->db->order_by('c.cat_name', 'asc');
		return $this->db->get($this->_smp_questions_know_freelancer_master)->result_array();
	}
	public function get_questions_by_cat_id($cat_id=0) {
		if($cat_id > 0) {
			$this->db->select('c.cat_name, ct.cat_type, q.*');
			$this->db->from($this->_category_master.' as c');
			$this->db->from($this->_category_type_master.' as ct');
			$this->db->join($this->_smp_questions_know_freelancer_master.' as q', 'q.cat_id = c.cat_id AND q.cat_type_id = ct.cat_type_id AND q.cat_id ='.(int)$cat_id);
			$this->db->group_by('q.question_id');
			$this->db->order_by('q.question_id', 'asc');
			return $this->db->get($this->_smp_questions_know_freelancer_master)->result_array();
		}
	}
	public function update_question(array $data, $question_id=0) {
		if(is_array($data) AND $question_id > 0){
			$this->db->where('question_id', (int)$question_id);
			$this->db->update($this->_smp_questions_know_freelancer_master, $data);
			return $this->db->affected_rows();
		} return false;
	}
	public function delete_question($question_id=0) {
		if($question_id > 0){
			$this->db->where('question_id', (int)$question_id);
			$this->db->delete($this->_smp_questions_know_freelancer_master);
			return $this->db->affected_rows();
		} return false;
	}
	public function register_question(array $data) {
		if(is_array($data)){			
			$this->db->insert($this->_smp_questions_know_freelancer_master, $data);
			return $this->db->insert_id();
		} return false;
	}
	public function get_category_type() {
		$this->db->where('cat_type_status', 1);
		$this->db->where('cat_type_id IN (5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,25)');
		return $this->db->get($this->_category_type_master)->result_array();
	}
	public function get_categories_by_type($type_id=0) {
		if($type_id > 0){
			$this->db->where('cat_type_id', (int)$type_id);
			return $this->db->get($this->_category_master)->result_array();
		}
	}
}

/* End of file Provider_question_master_model.php */
/* Location: ./application/models/Provider_question_master_model.php */