<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_cancellation_reason_master_model extends CI_Model {
	private $_smp_job_cancellation_reason_master = "tbl_smp_job_cancellation_reason_master";

	public function get_reasons() {
		$this->db->order_by('reason_id', 'desc');
		return $this->db->get($this->_smp_job_cancellation_reason_master)->result_array();
	}
	public function register_reason(array $data) {
		if(is_array($data)){			
			$this->db->insert($this->_smp_job_cancellation_reason_master, $data);
			return $this->db->insert_id();
		} return false;
	}
	public function delete_reason($reason_id=0) {
		if($reason_id > 0){
			$this->db->where('reason_id', (int)$reason_id);
			$this->db->delete($this->_smp_job_cancellation_reason_master);
			return $this->db->affected_rows();
		} return false;
	}
	public function update_reason(array $data, $reason_id=0) {
		if(is_array($data) AND $reason_id > 0){
			$this->db->where('reason_id', (int)$reason_id);
			$this->db->update($this->_smp_job_cancellation_reason_master, $data);
			return $this->db->affected_rows();
		} return false;
	}
}

/* End of file Job_cancellation_reason_master_model.php */
/* Location: ./application/models/Job_cancellation_reason_master_model.php */