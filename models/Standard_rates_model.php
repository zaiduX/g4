<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standard_rates_model extends CI_Model {

  private $rates = "tbl_standard_rates"; // db table declaration
  private $units = "tbl_unit_master"; 
  private $currency = "tbl_currency_master";
  private $countries = "tbl_countries";
  private $country_currencies = "tbl_country_currency";
  private $categories = "tbl_category_master";
  
  public function get_standard_rates($id=0)
  {   
    // $this->db->select('r.*, c.country_name, u.shortname, cr.currency_sign,cr.currency_title, c.country_name');   
    $this->db->select('r.*, c.country_name, cr.currency_sign,cr.currency_title, c.country_name, cat.cat_name');   
    $this->db->from('tbl_standard_rates as r');
    // $this->db->join('tbl_unit_master as u', 'r.unit_id = u.unit_id');
    $this->db->join('tbl_currency_master as cr', 'cr.currency_id = r.currency_id');
    $this->db->join('tbl_countries as c', 'c.country_id = r.country_id');
    $this->db->join($this->categories.' as cat', 'cat.cat_id = r.category_id');
    
    if($id > 0) { 
      $this->db->where('rate_id', $id);
      return $this->db->get()->row_array();
    } else {
      $this->db->order_by('rate_id', 'desc');
      return $this->db->get()->result_array();
    }
  } 
  
  public function check_min_volume_based_standard_rates(array $data)
  {
    // echo json_encode($data); die();
    if(is_array($data)){
      $min_volume = $data['min_volume'];
      $min_distance = $data['min_distance'];

      $this->db->select('rate_id');
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("$min_volume BETWEEN min_volume AND max_volume");
      $this->db->where("ROUND($min_distance) BETWEEN min_distance AND max_distance");
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

  public function check_max_volume_based_standard_rates(array $data)
  {
    // echo json_encode($data); die();
    if(is_array($data)){
      $max_volume = $data['max_volume'];
      $max_distance = $data['max_distance'];

      $this->db->select('rate_id');
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("$max_volume BETWEEN min_volume AND max_volume");
      $this->db->where("ROUND($max_distance) BETWEEN min_distance AND max_distance");
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

  public function check_min_weight_based_standard_rates(array $data)
  {
    // echo json_encode($data); die();
    if(is_array($data)){      
      $min_distance = $data['min_distance'];
      $min_weight = $data['min_weight'];
      $unit_id = $data['unit_id'];

      $this->db->select('rate_id');
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('unit_id', (int) $data['unit_id']);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("ROUND($min_distance) BETWEEN min_distance AND max_distance");
      $this->db->where("$min_weight BETWEEN min_weight AND max_weight");
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

  public function check_max_weight_based_standard_rates(array $data)
  {
    // echo json_encode($data); die();
    if(is_array($data)){      
      $max_distance = $data['max_distance'];
      $max_weight = $data['max_weight'];
      $unit_id = $data['unit_id'];

      $this->db->select('rate_id');
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('unit_id', (int) $data['unit_id']);
      $this->db->where('is_formula_volume_rate', 0);
      $this->db->where('is_formula_weight_rate', 0);
      $this->db->where("ROUND($max_distance) BETWEEN min_distance AND max_distance");
      $this->db->where("$max_weight BETWEEN min_weight AND max_weight");
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

  public function register_standard_rates(array $data)
  {
    if(is_array($data)){      
      $this->db->insert($this->rates, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_standard_rates(array $data, $id=0)
  {
    if(is_array($data) AND $id > 0){
      $this->db->where('rate_id', (int)$id);
      $this->db->update($this->rates, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function delete_standard_rates($id=0)
  {
    if($id > 0 ){
      $this->db->where('rate_id', (int) $id);
      $this->db->delete($this->rates);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function get_unit_master()
  {
    $this->db->order_by('unit_id', 'desc');
    return $this->db->get($this->units)->result_array();
  }

  public function get_unit_detail($id=0)
  {
    if($id > 0) {
      $this->db->where('unit_id', (int) $id);
      return $this->db->get($this->units)->row_array();
    }
    return false;
  }

  public function get_countries()
  {
    return $this->db->get($this->countries)->result_array();
  }

  public function get_country_currencies($country_id=0)
  {
    if($country_id > 0 ) {
      $this->db->select('cc.currency_id, cr.currency_sign, cr.currency_title');
      $this->db->from('tbl_country_currency as cc');
      $this->db->join('tbl_currency_master as cr', 'cr.currency_id = cc.currency_id');
      $this->db->where('cc.country_id', (int) $country_id);
      return $this->db->get()->result_array();
    }
    return false;
  }

  public function get_currency_detail($id=0)
  {
    if($id > 0) {
      $this->db->where('currency_id', (int) $id);
      return $this->db->get($this->currency)->row_array();
    }
    return false;
  }

  public function check_formula_volume_based_standard_rates(array $data)
  {
    //echo json_encode($data); die();
    if(is_array($data)){
      $this->db->select('rate_id');
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('is_formula_volume_rate', 1);
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

  public function check_formula_weight_based_standard_rates(array $data)
  {
    //echo json_encode($data); die();
    if(is_array($data)){
      $this->db->select('rate_id');
      $this->db->where('unit_id', (int) $data['unit_id']);
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('is_formula_weight_rate', 1);
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

  public function check_formula_min_weight_based_standard_rates(array $data)
  {
    // echo json_encode($data); die();
    if(is_array($data)){
      $min_distance = $data['min_distance'];
      $this->db->select('rate_id');
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('unit_id', (int) $data['unit_id']);
      $this->db->where('is_formula_weight_rate', 1);
      $this->db->where("ROUND($min_distance) BETWEEN min_distance AND max_distance");
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

  public function check_formula_max_weight_based_standard_rates(array $data)
  {
    // echo json_encode($data); die();
    if(is_array($data)){
      $max_distance = $data['max_distance'];
      $this->db->select('rate_id');
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('unit_id', (int) $data['unit_id']);
      $this->db->where('is_formula_weight_rate', 1);
      $this->db->where("ROUND($max_distance) BETWEEN min_distance AND max_distance");
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

  public function check_formula_min_volume_based_standard_rates(array $data)
  {
    // echo json_encode($data); die();
    if(is_array($data)){
      $min_distance = $data['min_distance'];
      $this->db->select('rate_id');
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('is_formula_volume_rate', 1);
      $this->db->where("ROUND($min_distance) BETWEEN min_distance AND max_distance");
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

  public function check_formula_max_volume_based_standard_rates(array $data)
  {
    // echo json_encode($data); die();
    if(is_array($data)){
      $max_distance = $data['max_distance'];
      $this->db->select('rate_id');
      $this->db->where('country_id', (int) $data['country_id']);
      $this->db->where('category_id', (int) $data['category_id']);
      $this->db->where('is_formula_volume_rate', 1);
      $this->db->where("ROUND($max_distance) BETWEEN min_distance AND max_distance");
      if($res = $this->db->get($this->rates)->row_array()){ return true; }
    }
    return false;
  }

}

/* End of file Standard_rates_model.php */
/* Location: ./application/models/Standard_rates_model.php */