<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_orders_model extends CI_Model {

	private $_courier_orders = "tbl_courier_orders";
	private $_customers_master = "tbl_customers_master";
	private $_customer_drivers = "tbl_customer_drivers";
	private $_countries = "tbl_countries";
	private $_states = "tbl_states";
	private $_cities = "tbl_cities";
	private $_transport_vehicle_master = "tbl_transport_vehicle_master";
	private $_unit_master = "tbl_unit_master";
	private $_courier_order_deliverer_request = "tbl_courier_order_deliverer_request";
	private $_courier_order_status = "tbl_courier_order_status";
	private $_order_inactive_reasons = "tbl_order_inactive_reasons";
	private $_order_packages = "tbl_order_packages";
	private $_laundry_booking = "tbl_laundry_booking";

	public function get_orders($rows=0)
	{
		$this->db->order_by('expiry_date', 'asc');
		return $this->db->get($this->_courier_orders)->result_array();
	}

	public function get_order_details($id=0)
	{
		if($id > 0) {
			$this->db->where('order_id', $id);
			return $this->db->get($this->_courier_orders)->row_array();
		}
		return false;
	}

	public function get_order_package_details($order_id=0)
	{
		if(!empty($order_id) && $order_id > 0 ){
			$this->db->where('order_id', (int)$order_id );
			return $this->db->get($this->_order_packages)->result_array(); } else return false;
	}

	public function inactivate_order($order_id=0)
	{
		if(!empty($order_id) && $order_id > 0 ){			
			$update = [
				"order_status" => "inactive",
				"status_updated_by" => "Admin~".$this->session->userdata('userid'),
				"status_update_datetime" => date('Y-m-d H:i:s'),
			];

			$this->db->where('order_id', $order_id);
			$this->db->update($this->_courier_orders, $update);
			return $this->db->affected_rows();
		}
		return false;	
	}

	public function insert_order_inactive_reason(array $data)
	{
		if(!empty($data)){
			$this->db->insert($this->_order_inactive_reasons, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function get_customer_details($id=0)
	{
		if($id > 0) {
			$this->db->where('cust_id', $id);
			return $this->db->get($this->_customers_master)->row_array();
		}
		return false;
	}

	public function get_driver_details($id=0)
	{
		if($id > 0) {
			$this->db->where('cd_id', $id);
			return $this->db->get($this->_customer_drivers)->row_array();
		}
		return false;
	}
	
	public function get_country_details($id=0)
	{		
		if($id > 0 ){
			$this->db->where('country_id', (int)$id);
			return $this->db->get($this->_countries)->row_array();
		}
		return false;
	}

	public function get_state_details($id=0)
	{		
		if($id > 0 ){
			$this->db->where('state_id', (int)$id);
			return $this->db->get($this->_states)->row_array();
		}
		return false;
	}

	public function get_city_details($id=0)
	{		
		if($id > 0 ){
			$this->db->where('city_id', (int)$id);
			return $this->db->get($this->_cities)->row_array();
		}
		return false;
	}

	public function get_vehicle_details($id=0)
	{		
		if($id > 0 ){
			$this->db->where('vehical_type_id', (int)$id);
			return $this->db->get($this->_transport_vehicle_master)->row_array();
		}
		return false;
	}

	public function get_unit_details($id=0)
	{		
		if($id > 0 ){
			$this->db->where('unit_id', (int)$id);
			return $this->db->get($this->_unit_master)->row_array();
		}
		return false;
	}

	public function get_requested_deliverers_details($id=0)
	{		
		if($id > 0 ){
			$this->db->select('d.*, r.order_id, r.cre_datetime');
			$this->db->from($this->_courier_order_deliverer_request.' as r');
			$this->db->join($this->_customers_master.' as d', 'd.cust_id = r.deliverer_id'); 
			$this->db->group_by('r.deliverer_id');
			return $this->db->get()->result_array(); 
		}
		return false;
	}

	public function get_rejected_deliverers_details($id=0)
	{		
		if($id > 0 ){
			$this->db->select('d.*, r.order_id, r.mod_datetime');
			$this->db->from($this->_courier_order_status.' as r');
			$this->db->join($this->_customers_master.' as d', 'd.cust_id = r.user_id'); 
			$this->db->where('r.user_type', 'deliverer');
			$this->db->where('r.status', 'reject');
			$this->db->group_by('r.user_id');
			return $this->db->get()->result_array(); 
		}
		return false;
	}

	public function get_customer_email_name_by_order_id($id=0)
	{
		if($id > 0) {
			$this->db->select('email1');
			$this->db->where('cust_id', (int)$id);
			$row = $this->db->get($this->_customers_master)->row_array();
			$email_cut = explode('@', $row['email1']);
			return ucwords($email_cut[0]);
		}
		return false;
	}
  
	public function get_laundry_orders($status=null, $rows=0)
	{
		if( $rows > 0 ){ $this->db->limit($rows); }
		if($status != null) { $this->db->where('booking_status', $status); }
		$this->db->order_by('cre_datetime', 'desc');
		return $this->db->get($this->_laundry_booking)->result_array();
	}



}

/* End of file Admin_orders_model.php */
/* Location: ./application/models/Admin_orders_model.php */