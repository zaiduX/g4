<?php
defined('BASEPATH') OR exit('No direct script access allowed.');

class Api_model_services extends CI_Model {

  private $_category_type_master = 'tbl_category_type_master';
  private $_smp_freelancer_profile = 'tbl_smp_freelancer_profile';
  private $_customers_master = 'tbl_customers_master';
  private $_customer_education = 'tbl_customer_education';
  private $_customer_experience = 'tbl_customer_experience';
  private $_smp_portfolio = 'tbl_smp_portfolio';
  private $_customer_skills = 'tbl_customer_skills';
  private $_customer_documents = 'tbl_consumers_documents';
  private $_customer_pay_methods = 'tbl_consumer_payment_details';
  private $_countries = 'tbl_countries';
  private $_states = 'tbl_states';
  private $_cities = 'tbl_cities';
  private $_payment_details = 'tbl_consumer_payment_details';
  private $_skills_master = 'tbl_skills_master';
  private $_category_master = 'tbl_category_master';
  private $_portfolio = 'tbl_consumer_portfolio';
  private $_language_master = 'tbl_language_master';
  private $_smp_service_provider_documents = 'tbl_smp_service_provider_documents';
  private $_deliverer = 'tbl_deliverer_profile';
  private $_bus_operator_profile = 'tbl_bus_operator_profile';
  private $_laundry_provider_profile = 'tbl_laundry_provider_profile';
  private $_smp_freelancer_off_days = 'tbl_smp_freelancer_off_days';
  private $_smp_profile_subscription_master = 'tbl_smp_profile_subscription_master';
  private $_user_account_master = 'tbl_user_account_master';
  private $_user_account_history = 'tbl_user_account_history';
  private $_gonagoo_account_master = 'tbl_gonagoo_account_master';
  private $_gonagoo_account_history = 'tbl_gonagoo_account_history';
  private $_gonagoo_address = "tbl_gonagoo_address";
  private $_smp_freelancer_sub_purchase_history = "tbl_smp_freelancer_sub_purchase_history";
  private $_smp_freelancer_proposal_credit_history = "tbl_smp_freelancer_proposal_credit_history";
  private $_consumer_device = 'tbl_customer_device';
  private $_smp_jobs = 'tbl_smp_jobs';
  private $_smp_offers = 'tbl_smp_offers';
  private $_smp_job_review = 'tbl_smp_job_review';
  private $_smp_service_provider_photos = 'tbl_smp_service_provider_photos';
  private $_country_currency = 'tbl_country_currency';
  private $_smp_offer_tags = 'tbl_smp_offer_tags';
  private $_smp_offer_addons = 'tbl_smp_offer_addons';
  private $_smp_advance_payment = 'tbl_smp_advance_payment';
  private $_smp_favorite_offers = 'tbl_smp_favorite_offers';
  private $_smp_purchased_offer_addons = 'tbl_smp_purchased_offer_addons';
  private $_smp_workstream_master = 'tbl_smp_workstream_master';
  private $_smp_workstream_details = 'tbl_smp_workstream_details';
  private $_promo_code = 'tbl_promo_code';
  private $_promocode_used = 'tbl_promocode_used';
  private $_deliverer_scrow_master = 'tbl_deliverer_scrow_master';
  private $_deliverer_scrow_history = 'tbl_deliverer_scrow_history';
  private $_smp_job_cancellation_reason_master = 'tbl_smp_job_cancellation_reason_master';
  private $_smp_dispute_category_master = 'tbl_smp_dispute_category_master';
  private $_smp_dispute_sub_category_master = 'tbl_smp_dispute_sub_category_master';
  private $_smp_job_dispute = 'tbl_smp_job_dispute';
  private $_smp_services_ratings = 'tbl_smp_services_ratings';

  //-----------------------------------------------------------
    private $_app_parameter = 'app_param_master';
    private $_skilled = 'tbl_customer_skills';
    private $_business_photos = 'tbl_business_photos';
    private $_transport_details = 'tbl_transport_details';
    private $_transport_master = 'tbl_transport_vehicle_master';
    private $_unit_master = 'tbl_unit_master';
    private $_orders = 'tbl_consumer_orders';
    private $_standard_capacities = 'tbl_standard_capacities';
    private $_customer_service_areas = 'tbl_customer_service_areas';
    private $_addressbook = 'tbl_address_books';
    private $_favourite_deliverers = 'tbl_favourite_deliverers';
    private $_standard_dimension = 'tbl_standard_dimension';
    private $_standard_rates = 'tbl_standard_rates';
    private $_courier_orders = 'tbl_courier_orders';
    private $_documents = 'tbl_consumers_documents';
    private $_courier_order_deliverer_request = 'tbl_courier_order_deliverer_request';
    private $_courier_order_status = 'tbl_courier_order_status';
    private $_courier_workroom = 'tbl_courier_workroom';
    private $_driver_device = 'tbl_driver_device';
    private $_driver_chats = 'tbl_driver_chats';
    private $_courier_order_review = 'tbl_courier_order_review';
    private $_insurance_master = 'tbl_insurance_master';
    private $_deliverer_documents = 'tbl_deliverer_documents';
    private $_relay_points = 'tbl_relay_points';
    private $_notifications = 'tbl_notifications';
    private $_read_notifications = 'tbl_read_notifications';
    private $_read_workroom_notification = 'tbl_read_workroom_notification';
    private $_read_chat_notification = 'tbl_read_chat_notification';
    private $_account_withdraw_request = 'tbl_account_withdraw_request';
    private $_relay_account_master = 'tbl_relay_account_master';
    private $_customer_support_queries = "tbl_customer_support_queries";
    private $_carrier_type = "tbl_carrier_type";
    private $_custom_percentage = "tbl_custom_percentage";
    private $_point_to_point_rates = " tbl_point_to_point_rates";
    private $_order_packages = "tbl_order_packages";
    private $_dangerous_goods_master = "tbl_dangerous_goods_master";
    private $_customer_recent_relay_point = "tbl_customer_recent_relay_point";
    private $_rejected_orders = "tbl_rejected_orders";
    private $_cust_login_stats = "tbl_cust_login_stats";

    private $_bus_master = "tbl_bus_master";
    private $_bus_amenities_master = "tbl_bus_amenities_master";
    private $_bus_seat_layout_master = "tbl_bus_seat_layout_master";
    private $_customer_drivers = 'tbl_customer_drivers';
    private $_permitted_category = 'tbl_permitted_category';
    private $_bus_operator_documents = 'tbl_bus_operator_documents';
    private $_bus_operator_cancellation_rescheduling = 'tbl_bus_operator_cancellation_rescheduling';
    private $_bus_cancellation_rescheduling = 'tbl_bus_cancellation_rescheduling';
    private $_bus_operator_agents_group_permissions = 'tbl_bus_operator_agents_group_permissions';
    private $_operator_agents = 'tbl_operator_agents';
    private $_bus_locations = 'tbl_bus_locations';
    private $_bus_locations_pickup_drop_points = 'tbl_bus_locations_pickup_drop_points';
    private $_bus_address_book = 'tbl_bus_address_book';
    private $_bus_trip_master = 'tbl_bus_trip_master';
    private $_trip_source_destination_details = 'tbl_trip_source_destination_details';
    private $_transport_vehicle_master = "tbl_transport_vehicle_master";
    private $_bus_seat_type_master = "tbl_bus_seat_type_master";
    private $_bus_seat_type_details = "tbl_bus_seat_type_details";
    private $_bus_trip_seat_type_price = "tbl_bus_trip_seat_type_price";
    private $_bus_booking_master = "tbl_bus_booking_master";
    private $_bus_operator_business_photos = "tbl_bus_operator_business_photos";
    private $_user_bus_account_history = "tbl_user_bus_account_history";
    private $_bus_booking_seat_details = "tbl_bus_booking_seat_details";
    private $_bus_advance_payment = "tbl_bus_advance_payment";
    private $_bus_workroom = "tbl_bus_workroom";
    private $_bus_trip_review = 'tbl_bus_trip_review';
    private $_bus_ratings = 'tbl_bus_ratings';
    private $_deliverer_ratings = 'tbl_deliverer_ratings';
    private $_bus_customer_support_queries = 'tbl_bus_customer_support_queries';
    private $_cancelled_trips = 'tbl_cancelled_trips';
    private $_bus_alert_crone = 'tbl_bus_alert_crone';
    private $_bus_read_workroom_notification = 'tbl_bus_read_workroom_notification';
    private $_global_workroom = 'tbl_global_workroom';
    private $_counter_sale_payment = 'tbl_counter_sale_payment';
    private $_laundry_charges = 'tbl_laundry_charges';
    private $_laundry_category = 'tbl_laundry_category';
    private $_laundry_sub_category = 'tbl_laundry_sub_category';
    private $_laundry_provider_documents = 'tbl_laundry_provider_documents';
    private $_laundry_provider_photos = 'tbl_laundry_provider_photos';
    private $_laundry_provider_cancellation_charges = 'tbl_laundry_provider_cancellation_charges';
    private $_laundry_advance_payment = 'tbl_laundry_advance_payment';
    private $_laundry_favourite_providers = 'tbl_laundry_favourite_providers';
    private $_laundry_booking = 'tbl_laundry_booking';
    private $_laundry_booking_details = 'tbl_laundry_booking_details';
    private $_laundry_ratings = 'tbl_laundry_ratings';
    private $_laundry_review = 'tbl_laundry_review';
    private $_laundry_cloth_temp = 'tbl_laundry_count_temp';
    private $_walkin_customer = 'tbl_walkin_customer';
    private $_service_claims = 'tbl_service_claims';
    private $_service_claim_types = 'tbl_service_claim_types';
    private $_laundry_special_charges = 'tbl_laundry_special_charges';
    //zaid
    private $_smp_questions_know_freelancer_master = 'tbl_smp_questions_know_freelancer_master';
    private $_smp_favorite_providers = 'tbl_smp_favorite_providers';
    private $_smp_jobs_invite = 'tbl_smp_jobs_invite';
    private $_smp_job_proposals = 'tbl_smp_job_proposals';
    private $_smp_job_decline_reason_master = 'tbl_smp_job_decline_reason_master';
    private $_smp_favorite_jobs = 'tbl_smp_favorite_jobs';
    private $_smp_proposal_milestone_details = 'tbl_smp_proposal_milestone_details';
    private $_smp_milestone_payment_request = 'tbl_smp_milestone_payment_request';
    private $_smp_job_payment_history = 'tbl_smp_job_payment_history';
    private $_smp_user_account_master = 'tbl_smp_user_account_master';   
    private $_smp_user_account_history = 'tbl_smp_user_account_history';   
    private $_smp_customer_scrow_history = 'tbl_smp_customer_scrow_history';   
    private $_smp_customer_scrow_master = 'tbl_smp_customer_scrow_master';
    private $_smp_deliverer_scrow_history = 'tbl_smp_deliverer_scrow_history';
    private $_smp_deliverer_scrow_master = 'tbl_smp_deliverer_scrow_master';
    private $authorities = "tbl_authorities_master";



  //Front Site----------------------------------------------
    public function get_service_providers_list() {
      return $this->db->get($this->_smp_freelancer_profile)->result_array();
    }
  //Front Site---------------------------------------------- 

  //User Profile----------------------------------------------
    public function get_total_educations($cust_id = 0 ) {
      if( $cust_id > 0 ) {
        $this->db->where('cust_id', (int)$cust_id);
        return $this->db->from($this->_customer_education)->count_all_results();
      }
      return false;
    }
    public function get_customer_educations($cust_id = 0, $rows = 0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('qualify_year', 'desc');
        if($rows > 0 ) { $result = $this->db->get($this->_customer_education, $rows); }
        else {  $result = $this->db->get($this->_customer_education); }
        return $result->result_array();
      }
      return false;
    }
    public function get_total_experiences($cust_id = 0, $other = false ) {
      if( $cust_id > 0 ) {
        if($other) { $this->db->where('other', 1); }
        else { $this->db->where('other', 0); }
        $this->db->where('cust_id', (int)$cust_id);
        return $this->db->from($this->_customer_experience)->count_all_results();
      } return false;
    }
    public function get_customer_experiences($cust_id = 0, $other = false, $rows = 0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        if($other) { $this->db->where('other', 1); }
        $this->db->order_by('exp_id', 'desc');
        if($rows > 0 ) { $result = $this->db->get($this->_customer_experience, $rows);  }
        else {  $result = $this->db->get($this->_customer_experience); }
        return $result->result_array();
      } return false;
    }
    public function get_customer_skills($cust_id = 0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('cs_id', 'desc');
        return $result = $this->db->get($this->_customer_skills)->row_array();
      } return false;
    }
    public function get_total_portfolios($cust_id = 0 ) {
      if( $cust_id > 0 ) {
        $this->db->where('cust_id', (int)$cust_id);
        return $this->db->from($this->_portfolio)->count_all_results();
      } return false;
    }
    public function get_customer_portfolio($cust_id = 0, $rows = 0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('portfolio_id', 'desc');
        if($rows > 0 ) { $result = $this->db->get($this->_portfolio, $rows); }
        else {  $result = $this->db->get($this->_portfolio); }
        return $result->result_array();
      } return false;
    }
    public function get_total_documents($cust_id = 0 ) {
      if( $cust_id > 0 ) {
        $this->db->where('cust_id', (int)$cust_id);
        return $this->db->from($this->_customer_documents)->count_all_results();
      } return false;
    }
    public function get_customer_documents($cust_id = 0, $rows = 0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('doc_id', 'desc');
        if($rows > 0 ) { $result = $this->db->get($this->_customer_documents, $rows); }
        else {  $result = $this->db->get($this->_customer_documents); }
        return $result->result_array();
      } return false;
    }
    public function get_total_payment_methods($cust_id = 0 ) {
      if( $cust_id > 0 ) {
        $this->db->where('cust_id', (int)$cust_id);
        return $this->db->from($this->_customer_pay_methods)->count_all_results();
      } return false;
    }
    public function get_customer_payment_methods($cust_id=0, $rows=0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('pay_id', 'desc');
        if($rows > 0 ) { $result = $this->db->get($this->_customer_pay_methods, $rows); }
        else {  $result = $this->db->get($this->_customer_pay_methods); }
        return $result->result_array();
      } return false;
    }
    public function get_countries() {
      return $this->db->get($this->_countries)->result_array();
    }
    public function get_state_by_country_id($id = 0) {
      if( $id> 0 ) {
        $this->db->where('country_id', (int) $id);
        return $this->db->get($this->_states)->result_array();
      } return false;
    }
    public function get_city_by_state_id($id = 0) {
      if( $id> 0 ) {
        $this->db->where('state_id', (int) $id);
        return $this->db->get($this->_cities)->result_array();
      } return false;
    }
    public function update_basic_profile(array $data) {
      if(is_array($data)){
        $this->db->where('cust_id', (int) $this->session->userdata('cust_id'));
        $this->db->update($this->_customers_master, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_country_code_by_id($country_id = 0) {
      if($country_id > 0){
        $this->db->select('country_phonecode');
        $this->db->where('country_id', (int)$country_id);
        $this->db->limit(1);
        $return = $this->db->get($this->_countries)->row_array();
        return $return['country_phonecode'];
      } return false;
    }
  //User Profile----------------------------------------------

  //Start User Payment Details--------------------------------
    public function get_payment_details($cust_id = 0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('pay_id', 'desc');
        return $this->db->get($this->_payment_details)->result_array();
      } return false;
    }
    public function register_payment_details(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_payment_details, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_payment_method_detail($id = 0) {
      if($id > 0 ) {
        $this->db->where('pay_id', (int) $id);
        return $this->db->get($this->_payment_details)->row_array();
      } return false;
    }
    public function update_payment_details($pay_id = 0, array $data) {
      if(is_array($data) AND $pay_id > 0) {
        $this->db->where('pay_id', (int)$pay_id);
        $this->db->update($this->_payment_details, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function delete_payment_detail($cust_id = 0, $pay_id = 0) {
      if($cust_id > 0 AND $pay_id > 0) {
        $this->db->where('pay_id', (int)$pay_id);
        $this->db->where('cust_id', (int)$cust_id);
        $this->db->delete($this->_payment_details);
        return $this->db->affected_rows();
      } return false;
    }
  //End User Payment Details---------------------------------

  //Start Customer Education---------------------------------
    public function add_user_education(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_customer_education, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_customer_education_by_id($id=0) {
      $this->db->where('edu_id', $id);
      return $this->db->get($this->_customer_education)->row_array();
    }
    public function update_user_education(array $data) {
      if(is_array($data)){
        $this->db->where('edu_id', (int)$data['edu_id']);
        $this->db->update($this->_customer_education, ['activities' => trim($data['activities']), 'description' => trim($data['description']), 'start_date' => trim($data['start_date']), 'end_date' => trim($data['end_date']), 'specialization' => trim($data['specialization']), 'qualification' => trim($data['qualification']), 'qualify_year' => trim($data['qualify_year']), 'institute_name' => trim($data['institute_name']), 'institute_address' => trim($data['institute_address']), 'certificate_title' => trim($data['certificate_title']), 'grade' => trim($data['grade']), 'remark' => trim($data['remark']), 'mod_datetime' => trim($data['mod_datetime'])]);
        return $this->db->affected_rows();
      } return false;
    }
    public function delete_user_education($edu_id=0) {
      if($edu_id>0){
        $this->db->where('edu_id', $edu_id);
        $this->db->delete($this->_customer_education);
        return $this->db->affected_rows();
      } return false;
    }
  //End Customer Education-----------------------------------

  //Start Customer Experience--------------------------------
    public function add_user_experience(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_customer_experience, ['cust_id' => trim($data['cust_id']), 'org_name' => trim($data['org_name']), 'job_title' => trim($data['job_title']), 'job_location' => trim($data['job_location']), 'job_desc' => trim($data['job_desc']), 'start_date' => trim($data['start_date']), 'end_date' => trim($data['end_date']), 'remark' => trim($data['remark']), 'grade' => trim($data['grade']), 'other' => trim($data['other']), 'cre_datetime' => trim($data['cre_datetime']), 'mod_datetime' => trim($data['mod_datetime']), 'currently_working' => $data['currently_working'] ]);
        return $this->db->insert_id();
      } return false;
    }
    public function get_customer_experience_by_id($id=0) {
      $this->db->where('exp_id', $id);
      return $this->db->get($this->_customer_experience)->row_array();
    }
    public function update_user_experience(array $data) {
      if(is_array($data)){
        $this->db->where('exp_id', (int)$data['exp_id']);
        $this->db->update($this->_customer_experience, ['org_name' => trim($data['org_name']), 'job_title' => trim($data['job_title']), 'job_location' => trim($data['job_location']), 'job_desc' => trim($data['job_desc']), 'start_date' => trim($data['start_date']), 'end_date' => trim($data['end_date']), 'remark' => trim($data['remark']), 'grade' => trim($data['grade']), 'other' => trim($data['other']), 'mod_datetime' => trim($data['mod_datetime']), 'currently_working' => $data['currently_working']]);
        return $this->db->affected_rows();
      } return false;
    }
    public function delete_user_experience($exp_id=0) {
      if($exp_id>0){
        $this->db->where('exp_id', $exp_id);
        $this->db->delete($this->_customer_experience);
        return $this->db->affected_rows();
      } return false;
    }
  //End Customer Experience----------------------------------

  //Start Customer Skill-------------------------------------
    public function get_category() {
      $this->db->where('cat_status', 1);
      return $this->db->get($this->_category_master)->result_array();
    }
    public function update_user_skills(array $data) {
      if(is_array($data)){
        $this->db->where('cs_id', (int)$data['cs_id']);
        $this->db->update($this->_customer_skills, ['confirmed_skills' => $data['confirmed_skills'],'confirmed_skills_name' => $data['confirmed_skills_name']]);
        return $this->db->affected_rows();
      } return false;
    }
    public function update_user_skill(array $data) {
      if(is_array($data)){
        $this->db->where('cs_id', (int)$data['cs_id']);
        $this->db->update($this->_customer_skills, ['junior_skills' => trim($data['junior_skills']), 'confirmed_skills' => trim($data['confirmed_skills']), 'senior_skills' => trim($data['senior_skills']), 'expert_skills' => trim($data['expert_skills']), 'ext1' => trim($data['ext1']), 'ext2' => trim($data['ext2'])]);
        return $this->db->affected_rows();
      } return false;
    }
    public function update_user_skill_old(array $data) {
      if(is_array($data)){
        $this->db->where('cs_id', (int)$data['cs_id']);
        $this->db->update($this->_customer_skills, ['junior_skills' => trim($data['junior_skills']), 'confirmed_skills' => trim($data['confirmed_skills']), 'senior_skills' => trim($data['senior_skills']), 'expert_skills' => trim($data['expert_skills']), 'ext1' => trim($data['ext1']), 'ext2' => trim($data['ext2'])]);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_skill_master() {
      $this->db->where('skill_status', 1);
      return $this->db->get($this->_skills_master)->result_array();
    }
    public function get_skill_master_details($skill_id=0) {
      if($skill_id > 0) {
        $this->db->where('skill_id', (int)$skill_id);
        return $this->db->get($this->_skills_master)->row_array();
      } return false;
    }
    public function get_category_by_id($cat_type_id=0) {
      if($cat_type_id > 0) {
        $this->db->where('cat_type_id', (int)$cat_type_id);
        $this->db->where('cat_status', 1);
        return $this->db->get($this->_category_master)->result_array();
      } return false;
    }
    public function get_category_type_with_category() {
      $this->db->select('c.*, t.cat_type'); 
      $this->db->from($this->_category_master.' as c');
      $this->db->join($this->_category_type_master.' as t', 'c.cat_type_id = t.cat_type_id');
      $this->db->where('c.cat_status', 1);
      $this->db->where('c.cat_type_id IN (5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,25)');
      $this->db->group_by('c.cat_id');
      $this->db->order_by('c.cat_type_id', 'desc');
      return $this->db->get()->result_array();
    }
    public function get_skill_by_cat_id($id=0) {
      if( $id> 0 ) {
        $this->db->where('cat_id', (int) $id);
        $this->db->where('skill_status', 1);
        return $this->db->get($this->_skill_master)->result_array();    
      } return false;
    }
  //End Customer Skill---------------------------------------

  //Start Customer Portfolio---------------------------------
    public function get_category_types() {
      $this->db->where('cat_type_status', 1);
      $this->db->where('cat_type_id IN (5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,25)');
      $this->db->order_by('cat_type_id', 'desc');
      return $this->db->get($this->_category_type_master)->result_array();
    }
    public function register_portfolio($data=null) {
      if(is_array($data)){
        $this->db->insert($this->_portfolio, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_category_by_cat_type_id($id=0) {
      if( $id> 0 ) {
        $this->db->where('cat_type_id', (int) $id);
        $this->db->where('cat_status', 1);
        return $this->db->get($this->_category_master)->result_array();   
      } return false;
    }
    public function get_subcategory_by_category_id($id=0) {
      if( $id> 0 ) {
        $this->db->where('parent_cat_id', (int) $id);
        $this->db->where('cat_status', 1);
        return $this->db->get($this->_category_master)->result_array();   
      } return false;
    }
    public function get_portfolio_detail($id=0) {
      if($id > 0 ){
        $this->db->where('portfolio_id', (int)$id);
        return $this->db->get($this->_portfolio)->row_array();
      } return false;
    }
    public function get_customers_portfolio($cust_id=0) {
      if($cust_id > 0 ){
        $this->db->where('cust_id', (int)$cust_id);
        $this->db->order_by('portfolio_id', 'desc');
        return $this->db->get($this->_portfolio)->result_array();
      } return false;
    }
    public function update_portfolio($portfolio_id=0, array $update_data) {
      if($portfolio_id > 0 AND  is_array($update_data)){
        $this->db->where('portfolio_id', (int)$portfolio_id);
        $this->db->update($this->_portfolio, $update_data);
        return $this->db->affected_rows();
      } return false;
    }
    public function delete_portfolio($portfolio_id=0)
    {
      if($portfolio_id > 0 ){
        $this->db->where('portfolio_id', $portfolio_id);
        $this->db->delete($this->_portfolio);
        return $this->db->affected_rows();
      } return false;
    }
  //End Customer Portfolio-----------------------------------

  //Start Customer Language----------------------------------
    public function get_language_master() {
      return $this->db->get($this->_language_master)->result_array();
    }
    public function get_language_masters($filter=null) {
      $return = array();
      if($filter != null) { $this->db->like('name_en', $filter, 'BOTH');  }
      $this->db->order_by('name_en', 'asc');
      if( $res = $this->db->get($this->_language_master)->result_array()){
        foreach ($res as $r) {
          $return [] = $r['name_en'];
        }
      } return $return;
    }
    public function update_user_language(array $data) {
      if(is_array($data)){
        $this->db->where('cust_id', (int)$data['cust_id']);
        $today = date('Y-m-d H:i:s');
        $this->db->update($this->_customers_master, ['lang_known' => trim($data['lang_known']), 'last_login_datetime' => $today]);
        return $this->db->affected_rows();
      } return false;
    }
  //End Customer Language------------------------------------
  
  //Start Customer Overview----------------------------------
    public function update_overview($cust_id=0, array $data) {
      if($cust_id > 0 AND is_array($data)){     
        $today = date('Y-m-d H:i:s');
        $this->db->where('cust_id', $cust_id);
        $this->db->update($this->_customers_master, $data);
        return $this->db->affected_rows();
      } return false;
    }
  //End Customer Overview------------------------------------

  //Start Customer Document----------------------------------
    public function get_documents($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('doc_id', 'desc');
        return $this->db->get($this->_customer_documents)->result_array();
      } return false;
    } 
    public function register_document(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_customer_documents, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_document_detail($doc_id=0) {
      if($doc_id > 0 ) {
        $this->db->where('doc_id', (int) $doc_id);
        return $this->db->get($this->_customer_documents)->row_array();
      } return false;
    }
    public function update_document($doc_id=0, array $data) {
      if(is_array($data) AND $doc_id > 0) {
        $this->db->where('doc_id', (int)$doc_id);
        $this->db->update($this->_customer_documents, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function delete_document($cust_id=0, $doc_id=0) {
      if($cust_id > 0 AND $doc_id > 0) {      
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('doc_id', (int) $doc_id);
        $this->db->delete($this->_customer_documents);
        return $this->db->affected_rows();
      } return false;
    }
  //End Customer Document------------------------------------
  
  //Start Customer Change Password---------------------------
    public function validate_old_password($cust_id=0, $old_pass=null) {
      if($cust_id > 0 AND $old_pass != null) {
        $this->db->select('cust_id');
        $this->db->where('cust_id', (int)$cust_id);
        $this->db->where('password', trim($old_pass));
        if( $return = $this->db->get($this->_customers_master)->row()){ return $return->cust_id;  }
      } return false;
    }
    public function update_new_password(array $data) {
      if($data['password'] != null && !empty($data['password'])) {
        $today = date("Y-m-d H:i:s");
        $this->db->where('cust_id', (int) $data['cust_id']);
        $this->db->update($this->_customers_master, ['password' => $data['password'], 'last_login_datetime' => $today]);
        return $this->db->affected_rows();
      } return false;
    }
  //End Customer Change Password-----------------------------

  //Start Service Provider Profile---------------------------
    public function get_service_provider_profile($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        return $this->db->get($this->_smp_freelancer_profile)->row_array();
      } return false;
    }
    public function get_service_provider_profile_code($profile_code=null) {
      if($profile_code != null ) {
        $this->db->where('profile_code', trim($profile_code));
        return $this->db->get($this->_smp_freelancer_profile)->row_array();
      } return false;
    }
    public function get_service_provider_documents($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('doc_id', 'desc');
        return $this->db->get($this->_smp_service_provider_documents)->result_array();
      } return false;
    }
    public function get_service_provider_off_days($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where("from_date >= '".date('Y/m/d')."'");
        $this->db->order_by('from_date', 'asc');
        return $this->db->get($this->_smp_freelancer_off_days)->result_array();
      } return false;
    }
    public function register_service_provider_profile(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_freelancer_profile, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function make_consumer_deliverer($cust_id=0) {
      if($cust_id > 0) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('is_deliverer', 0);
        $this->db->update($this->_customers_master, array("is_deliverer" => 1));
      } return false;
    }
    public function update_provider(array $update_data, $cust_id=0) {
      if(is_array($update_data) && $cust_id > 0 ){
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->update($this->_smp_freelancer_profile, $update_data);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_deliverer_profile($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        if($profile = $this->db->get($this->_deliverer)->row_array()) {
          return $profile;
        } else {
          $this->db->where('cust_id', (int) $cust_id);
          if($profile = $this->db->get($this->_bus_operator_profile)->row_array()) {
            return $profile;
          } else {
            $this->db->where('cust_id', (int) $cust_id);
            if($profile = $this->db->get($this->_laundry_provider_profile)->row_array()) {
              return $profile;
            } return false;
          }
        }
      } return false;
    }
    public function is_user_active_or_exists($cust_id=0) {
      if($cust_id > 0 ) {     
        $this->db->where('cust_id', (int)($cust_id));
        $this->db->where('cust_status', 1);
        if($this->db->get($this->_customers_master)->row_array()){  return true;  }
      }
      return false;
    }
  //End Service Provider Profile-----------------------------

  //Start Service Provider Document--------------------------
    public function get_provider_id($cust_id=0) {
      if($cust_id > 0) {
        $this->db->select('provider_id');
        $this->db->where('cust_id', (int)($cust_id));
        $row = $this->db->get($this->_smp_freelancer_profile)->row_array();
        return $row['provider_id'];
      } return false;
    }
    public function register_operator_document(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_service_provider_documents, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_old_operator_documents($doc_id=0) {
      if($doc_id > 0 ) {
        $this->db->where('doc_id', (int) $doc_id);
        return $this->db->get($this->_smp_service_provider_documents)->result_array();
      } return false;
    }
    public function delete_operator_document($doc_id=0) {
      if( $doc_id > 0) {      
        $this->db->where('doc_id', (int) $doc_id);
        $this->db->delete($this->_smp_service_provider_documents);
        return $this->db->affected_rows();
      } return false;
    }
  //End Service Provider Document----------------------------
    
  //Start Service Provider Settings--------------------------
    public function get_all_currencies() {
      return $this->db->get('tbl_currency_master')->result_array();
    }
    public function register_off_day(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_freelancer_off_days, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function delete_off_day($off_id=0) {
      if( $off_id > 0) {      
        $this->db->where('off_id', (int) $off_id);
        $this->db->delete($this->_smp_freelancer_off_days);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_subscription_master($country_id=0) {
      if($country_id > 0) {
        $this->db->where('country_id', (int)($country_id));
        return $this->db->get($this->_smp_profile_subscription_master)->result_array();
      } return false;
    }
    public function get_subscription_master_details($subscription_id=0) {
      if($subscription_id > 0) {
        $this->db->where('subscription_id', (int)($subscription_id));
        return $this->db->get($this->_smp_profile_subscription_master)->row_array();
      } return false;
    }
    public function get_user_details($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int)$cust_id);
        return $this->db->get($this->_customers_master)->row_array();
      } return false;
    }
    public function customer_account_master_details($cust_id=0, $currency_sign=null) {
      if($cust_id > 0 && !is_null($currency_sign)) {
        $this->db->where('user_id', (int)$cust_id);
        $this->db->where('currency_code', trim($currency_sign));
        return $this->db->get($this->_user_account_master)->row_array();
      } return false;
    }
    public function insert_gonagoo_customer_record(array $insert_data_customer_master) {
      if(is_array($insert_data_customer_master)) {
        $this->db->insert($this->_user_account_master, $insert_data_customer_master);
        return $this->db->insert_id();
      } return false;
    }
    public function update_payment_in_customer_master($account_id=0, $cust_id=0, $account_balance=0) {
      if($cust_id > 0 && $account_id > 0) {
        $this->db->where('account_id', (int)$account_id);
        $this->db->where('user_id', (int)$cust_id);
        $this->db->update($this->_user_account_master, ['account_balance' => $account_balance , 'update_datetime' => date('Y-m-d H:i:s')]);
        return $this->db->affected_rows();
      } return false;
    }
    public function insert_payment_in_account_history(array $insert_data) {
      if(is_array($insert_data)) {
        $this->db->insert($this->_user_account_history, $insert_data);
        return $this->db->insert_id();
      } return 0;
    }
    public function gonagoo_master_details($currency_sign=null) {
      if(!is_null($currency_sign)) {
        $this->db->where('currency_code', trim($currency_sign));
        return $this->db->get($this->_gonagoo_account_master)->row_array();
      } return false;
    }
    public function insert_gonagoo_master_record(array $insert_data_gonagoo_master) {
      if(is_array($insert_data_gonagoo_master)) {
        $this->db->insert($this->_gonagoo_account_master, $insert_data_gonagoo_master);
        return $this->db->insert_id();
      } return false;
    }
    public function update_payment_in_gonagoo_master($gonagoo_id=0, $gonagoo_balance=null) {
      if(!is_null($gonagoo_balance)) {
        $today =date('Y-m-d h:i:s');
        $this->db->where('gonagoo_id', (int)$gonagoo_id);
        $this->db->update($this->_gonagoo_account_master, ['gonagoo_balance' => $gonagoo_balance, 'update_datetime'=>$today]);
        return $this->db->affected_rows();
      } return false;
    }
    public function insert_payment_in_gonagoo_history(array $insert_data) {
      if(is_array($insert_data)) {
        $this->db->insert($this->_gonagoo_account_history, $insert_data);
        return $this->db->affected_rows();
      } return false;
    }
    public function scrow_master_details($deliverer_id=0, $currency_sign=null) {
      if($deliverer_id > 0 && !is_null($currency_sign)) {
        $this->db->where('deliverer_id', (int)$deliverer_id);
        $this->db->where('currency_code', $currency_sign);
        return $this->db->get($this->_deliverer_scrow_master)->row_array();
      } return false;
    }
    public function insert_scrow_record(array $insert_data_customer_scrow) {
      if(is_array($insert_data_customer_scrow)) {
        $this->db->insert($this->_deliverer_scrow_master, $insert_data_customer_scrow);
        return $this->db->insert_id();
      } return false;
    }
    public function update_scrow_payment($scrow_id=0, $deliverer_id=0, $scrow_balance=0) {
      if($deliverer_id > 0 && $scrow_id > 0) {
        $this->db->where('deliverer_id', (int)$deliverer_id);
        $this->db->where('scrow_id', (int)$scrow_id);
        $this->db->update($this->_deliverer_scrow_master, ['scrow_balance' => $scrow_balance]);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function insert_scrow_history_record(array $insert_data) {
      if(is_array($insert_data)) {
        $this->db->insert($this->_deliverer_scrow_history, $insert_data);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_country_details($country_id=0) {
      if($country_id > 0 ) {
        $this->db->where('country_id', (int)($country_id));
        return $this->db->get($this->_countries)->row_array();
      } return false;
    }
    public function get_state_details($state_id=0) {
      if($state_id > 0 ) {
        $this->db->where('state_id', (int)($state_id));
        return $this->db->get($this->_states)->row_array();
      } return false;
    }
    public function get_city_details($city_id=0) {
      if($city_id > 0 ) {
        $this->db->where('city_id', (int)($city_id));
        return $this->db->get($this->_cities)->row_array();
      } return false;
    }
    public function get_gonagoo_address($country_id=0) {
      if($country_id > 0 ) {
        $this->db->where('country_id', $country_id);
        $row = $this->db->get($this->_gonagoo_address)->row_array();
        if(!isset($row)) {
          $this->db->where('country_id', 75);
          return $this->db->get($this->_gonagoo_address)->row_array();
        } else { return $row; }
      } return false;
    }
    public function insert_freelancer_sub_purchase_history(array $insert_data) {
      if(is_array($insert_data)) {
        $this->db->insert($this->_smp_freelancer_sub_purchase_history, $insert_data);
        return $this->db->insert_id();
      } return 0;
    }
    public function insert_freelancer_proposal_credit_history(array $insert_data) {
      if(is_array($insert_data)) {
        $this->db->insert($this->_smp_freelancer_proposal_credit_history, $insert_data);
        return $this->db->insert_id();
      } return 0;
    }
    public function get_user_device_details($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->select('reg_id, device_type');
        $this->db->where('cust_id', (int)($cust_id));
        return $this->db->get($this->_consumer_device)->result_array();
      } return false;
    }
  //End Service Provider Settings----------------------------

  //Start Service Provider Business Photos-------------------
    public function get_service_provider_business_photos($cust_id=0) {
      if($cust_id > 0 ){
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('photo_id', 'desc');
        return $this->db->get($this->_smp_service_provider_photos)->result_array();
      } return false;
    }
    public function add_service_provider_business_photo($photo_url=null, $cust_id=0) {
      if($photo_url != null AND  $cust_id > 0){
        $this->db->insert($this->_smp_service_provider_photos, array("photo_url" => trim($photo_url), "cust_id" => (int)$cust_id));
        return $this->db->insert_id();
      } return false;
    }
    public function delete_service_provider_business_photo($photo_id=0) {
      if($photo_id > 0 ){
        $this->db->select('photo_url');
        $this->db->where('photo_id', (int) $photo_id);
        if( $result = $this->db->get($this->_smp_service_provider_photos)->row_array()){
          if(unlink(trim($result['photo_url']))){ 
            $this->db->where('photo_id', $photo_id);
            $this->db->delete($this->_smp_service_provider_photos);
            return true; 
          }
        }     
      } return false;
    }
  //End Service Provider Business Photos---------------------  



    public function get_provider_job_counts_old($provider_id=0) {
      if($provider_id > 0 ) {
        $this->db->select('COUNT(job_id) AS total_job');
        $this->db->where('provider_id', (int)($provider_id));
        return $this->db->get($this->_smp_jobs)->row_array();
      } return array('total_job' => 0);
    }

    //zaidu
    public function get_provider_job_counts($provider_id=0 , $status=null) {
      if($provider_id > 0 ) {
        $this->db->select('COUNT(job_id) AS total_job');
        if($status == "completed"){ $this->db->where('job_status', 'completed'); }
        $this->db->where('provider_id', (int)($provider_id));
        return $this->db->get($this->_smp_jobs)->row_array();
      } return array('total_job' => 0);
    }

    public function get_provider_customer_counts($provider_id=0) {
      if($provider_id > 0 ) {
        $this->db->select('COUNT(DISTINCT(cust_id)) AS total_customers');
        $this->db->where('provider_id', (int)($provider_id));
        return $this->db->get($this->_smp_jobs)->row_array();
      } return array('total_customers' => 0);
    }
    public function get_provider_my_job_counts($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->select('COUNT(job_id) AS total_job');
        $this->db->where('cust_id', (int)($cust_id));
        return $this->db->get($this->_smp_jobs)->row_array();
      } return array('total_job' => 0);
    }
    public function get_customers_provider_counts($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->select('COUNT(DISTINCT(provider_id)) AS total_providers');
        $this->db->where('cust_id', (int)($cust_id));
        return $this->db->get($this->_smp_jobs)->row_array();
      } return array('total_providers' => 0);
    }
    public function get_service_provier_job_reviews($cust_id=0) {
      if($cust_id > 0 ) {
        $this->db->where('provider_id', (int)($cust_id));
        $this->db->where('rated_by', 'customer');
        $this->db->order_by('review_id', 'desc');
        return $this->db->get($this->_smp_job_review)->result_array();
      } return false;
    }


  //Start Service Provider Offers--------------------------
    public function get_service_provider_offers($cust_id=0) {
      $this->db->select('c.cat_name, ct.cat_type, country.country_name, pro.firstname AS pro_fname, pro.lastname AS pro_lname, free.firstname AS free_fname, free.lastname AS free_lname, free.avatar_url AS free_avatar_url, free.company_name AS free_company_name, offer.*');
      $this->db->from($this->_category_master.' as c');
      $this->db->from($this->_category_type_master.' as ct');
      $this->db->from($this->_countries.' as country');
      $this->db->from($this->_customers_master.' as pro');
      $this->db->from($this->_smp_freelancer_profile.' as free');
      $this->db->join($this->_smp_offers.' as offer', 'offer.cat_id = c.cat_id AND offer.cat_type_id = ct.cat_type_id AND offer.country_id = country.country_id AND offer.cust_id = pro.cust_id AND offer.cust_id = free.cust_id');

      if($cust_id > 0) { $this->db->where('offer.cust_id', (int)($cust_id)); }

      $this->db->where('offer.offer_status', 1);

      $this->db->order_by('offer.offer_id', 'desc');
      $this->db->group_by('offer.offer_id');
      return $this->db->get($this->_smp_offers)->result_array();
    }
    public function get_service_provider_offers_filtered(array $data) {
      if(is_array($data)) {
        $this->db->select('c.cat_name, ct.cat_type, country.country_name, pro.firstname AS pro_fname, pro.lastname AS pro_lname, free.firstname AS free_fname, free.lastname AS free_lname, free.avatar_url AS free_avatar_url, free.company_name AS free_company_name, offer.*');
        $this->db->from($this->_category_master.' as c');
        $this->db->from($this->_category_type_master.' as ct');
        $this->db->from($this->_countries.' as country');
        $this->db->from($this->_customers_master.' as pro');
        $this->db->from($this->_smp_freelancer_profile.' as free');
        $this->db->join($this->_smp_offers.' as offer', 'offer.cat_id = c.cat_id AND offer.cat_type_id = ct.cat_type_id AND offer.country_id = country.country_id AND offer.cust_id = pro.cust_id AND offer.cust_id = free.cust_id');
        
        if($data['country_id'] > 0) { $this->db->where('offer.country_id', $data['country_id']); }
        if($data['state_id'] > 0) { $this->db->where('offer.state_id', $data['state_id']); }
        if($data['city_id'] > 0) { $this->db->where('offer.city_id', $data['city_id']); }
        if($data['cat_type_id'] > 0) { $this->db->where('offer.cat_type_id', $data['cat_type_id']); }
        if($data['cat_id'] > 0) { $this->db->where('offer.cat_id', $data['cat_id']); }
        if($data['delivered_in'] > 0) { $this->db->where('offer.delivered_in', $data['delivered_in']); }
        if($data['start_price'] > 0) { $this->db->where('offer.offer_price >= ', (int)$data['start_price']); }
        if($data['end_price'] > 0) { $this->db->where('offer.offer_price <= ', (int)$data['end_price']); }

        if($data['active_status'] == 'active') { $this->db->where('offer.admin_approval', 1); }
        if($data['active_status'] == 'inactive') { $this->db->where('offer.admin_approval', 0); }

        if($data['running_status'] == 'paused') { $this->db->where('offer.offer_pause_resume', 0); }
        if($data['running_status'] == 'resumed') { $this->db->where('offer.offer_pause_resume', 1); }

        if($data['cust_id'] > 0) { $this->db->where('offer.cust_id', $data['cust_id']); }

        if($data['last_id'] > 0) {  $this->db->where('offer.offer_id > ', $data['last_id']); }

        if($data['offer_pause_resume'] != 'all') { $this->db->where('offer.offer_pause_resume', (int) $data['offer_pause_resume']); }

        $this->db->where('offer.offer_status', 1);
        
        if($data['sort_by'] == 'rating') { $this->db->order_by('offer.rating_count', 'desc');
        } else if($data['sort_by'] == 'high') { $this->db->order_by('CAST(offer.offer_price as SIGNED INTEGER)', 'desc'); 
        } else if($data['sort_by'] == 'low') { $this->db->order_by('CAST(offer.offer_price as SIGNED INTEGER)', 'asc'); 
        } else if($data['sort_by'] == 'top') { $this->db->order_by('offer.sales_count', 'desc'); 
        } else if($data['latest'] == 'on') { $this->db->order_by('offer.offer_id', 'desc');
        } else { $this->db->order_by('offer.offer_id', 'asc'); }

        $this->db->group_by('offer.offer_id');

        if($data['device'] == 'mobile') { $this->db->limit(10); }

        return $this->db->get($this->_smp_offers)->result_array();
      } return false;
    }
    public function get_service_provider_offer_details($offer_id=0) {
      if($offer_id > 0) {
        $this->db->select('c.cat_name, ct.cat_type, country.country_name, pro.firstname AS pro_fname, pro.lastname AS pro_lname, free.firstname AS free_fname, free.lastname AS free_lname, free.avatar_url AS free_avatar_url, free.company_name AS free_company_name, free.response_time AS free_response_time, free.country_id AS free_country_id, free.introduction AS free_introduction, offer.*');
        $this->db->from($this->_category_master.' as c');
        $this->db->from($this->_category_type_master.' as ct');
        $this->db->from($this->_countries.' as country');
        $this->db->from($this->_customers_master.' as pro');
        $this->db->from($this->_smp_freelancer_profile.' as free');
        $this->db->join($this->_smp_offers.' as offer', 'offer.cat_id = c.cat_id AND offer.cat_type_id = ct.cat_type_id AND offer.country_id = country.country_id AND offer.cust_id = pro.cust_id AND offer.cust_id = free.cust_id AND offer.offer_id = ' . (int)($offer_id));
        return $this->db->get($this->_smp_offers)->row_array();
      } return false;
    }
    public function get_service_provider_offer_details_by_code($offer_code=null) {
      if($offer_code != null) {
        $this->db->select('c.cat_name, ct.cat_type, country.country_name, offer.*');
        $this->db->from($this->_category_master.' as c');
        $this->db->from($this->_category_type_master.' as ct');
        $this->db->from($this->_countries.' as country');
        $this->db->join($this->_smp_offers.' as offer', 'offer.cat_id = c.cat_id AND offer.cat_type_id = ct.cat_type_id AND offer.country_id = country.country_id AND offer.offer_code = "' . $offer_code . '"');
        return $this->db->get($this->_smp_offers)->row_array();
      } return false;
    }
    public function get_state_name_by_id($id=0) {
      if($id>0) {            
        $this->db->where('state_id', (int) $id);            
        $r = $this->db->get($this->_states)->row_array();            
        return $r['state_name'];        
      } return false;    
    }
    public function get_city_name_by_id($id=0) {
      if($id>0) {            
        $this->db->where('city_id', (int) $id);            
        $r = $this->db->get($this->_cities)->row_array();            
        return $r['city_name'];        
      } return false;    
    }
    public function get_categories_by_type($type_id=0) {
      if($type_id > 0) {
        $this->db->where('cat_status', 1);
        $this->db->where('cat_type_id', (int)$type_id);
        return $this->db->get($this->_category_master)->result_array();
      } return false; 
    }
    public function get_country_states($country_id=0) {
      if($country_id>0) {            
        $this->db->where('country_id', (int)$country_id);            
        return $this->db->get($this->_states)->result_array();            
      } return false;
    }
    public function get_state_cities($state_id=0) {
      if($state_id>0) {            
        $this->db->where('state_id', (int)$state_id);            
        return $this->db->get($this->_cities)->result_array();            
      } return false;
    }
    public function update_offer_details($offer_id=0, array $data) {
      if($offer_id > 0 AND is_array($data)){     
        $this->db->where('offer_id', $offer_id);
        $this->db->update($this->_smp_offers, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_country_currency_detail($id=0) {
      if($id > 0) {
        $this->db->select('cc.cc_id, ct.country_id, ct.country_name, cr.currency_title, cr.currency_id, cr.currency_title, cr.currency_sign');
        $this->db->from('tbl_country_currency cc'); 
        $this->db->join('tbl_countries ct', 'ct.country_id = cc.country_id');
        $this->db->join('tbl_currency_master cr', 'cr.currency_id = cc.currency_id');
        $this->db->where('cc.cc_status', 1);
        $this->db->where('cc.country_id', $id);
        $this->db->group_by('cc.cc_id');
        return $this->db->get()->row_array();
      } return false;
    }
    public function get_offer_tags_masters($filter=null) {
      $return = array();
      $this->db->where('status', 1);
      if($filter != null) { $this->db->like('tag_name', $filter, 'BOTH');  }
      $this->db->order_by('tag_name', 'asc');
      if( $res = $this->db->get($this->_smp_offer_tags)->result_array()){
        foreach ($res as $r) { $return [] = $r['tag_name']; }
      } 
      return $return;
    }
    public function register_provider_offer_details(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_offers, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function register_provider_offer_add_on_details(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_offer_addons, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function check_tag_exists($tag_name=null) {
      if($tag_name != null ) {     
        $this->db->where('tag_name', $tag_name);
        $this->db->where('status', 1);
        if($this->db->get($this->_smp_offer_tags)->row_array()){  return true;  }
      } return false;
    }
    public function register_offer_tag(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_offer_tags, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_country_cat_advance_payment_details(array $data) {
      if(is_array($data)) {
        if($data['country_id'] > 0) { $this->db->where('country_id', $data['country_id']); }
        if($data['cat_type_id'] > 0) { $this->db->where('cat_type_id', $data['cat_type_id']); }
        if($data['cat_id'] > 0) { $this->db->where('cat_id', $data['cat_id']); }
        $this->db->where('status', 1);
        return $this->db->get($this->_smp_advance_payment)->row_array();
      } return false;
    }
    public function get_country_name_by_id($id=0) {
      if($id>0){            
        $this->db->where('country_id', (int) $id);            
        $r = $this->db->get($this->_countries)->row_array();            
        return $r['country_name'];        
      }        
      return false;    
    }
    public function get_offer_add_ons($offer_id=0) {
      if($offer_id > 0) {
        $this->db->where('status', 1);
        $this->db->where('offer_id', (int)$offer_id);            
        return $this->db->get($this->_smp_offer_addons)->result_array();            
      } return false;    
    }
    public function get_offer_reviews($offer_id=0) {
      if($offer_id > 0) {
        $this->db->select('cust.firstname AS cust_fname, cust.lastname AS cust_lname, cust.country_id AS cust_country_id, cust.avatar_url AS cust_avatar_url, pro.firstname AS pro_fname, pro.lastname AS pro_lname, free.firstname AS free_fname, free.lastname AS free_lname, free.avatar_url AS free_avatar_url, free.company_name AS free_company_name, review.*');
        $this->db->from($this->_customers_master.' as cust');
        $this->db->from($this->_customers_master.' as pro');
        $this->db->from($this->_smp_freelancer_profile.' as free');
        $this->db->from($this->_countries.' as contry');
        $this->db->join($this->_smp_job_review.' as review', 'review.cust_id = cust.cust_id AND review.provider_id = pro.cust_id AND review.provider_id = free.cust_id AND review.offer_id = ' . (int)($offer_id));

        $this->db->order_by('review.review_id', 'desc');
        $this->db->group_by('review.review_id');          

        return $this->db->get($this->_smp_job_review)->result_array();            
      } return false;    
    }
    public function update_provider_offer_add_on_details($addon_id=0, array $data) {
      if($addon_id > 0 AND is_array($data)){     
        $this->db->where('addon_id', $addon_id);
        $this->db->update($this->_smp_offer_addons, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_offer_add_on_details($addon_id=0) {
      if($addon_id > 0) {
        $this->db->where('status', 1);
        $this->db->where('addon_id', (int)$addon_id);            
        return $this->db->get($this->_smp_offer_addons)->row_array();            
      } return false;    
    }
  //End Service Provider Offers----------------------------

  //Start Favorite offers----------------------------------
    public function get_favorite_offers($cust_id=0) {
      if($cust_id > 0) {            
        $this->db->where('cust_id', (int) $cust_id);            
        return $this->db->get($this->_smp_favorite_offers)->result_array();
      } return false;    
    }
    public function offer_add_to_favorite(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_favorite_offers, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function offer_remove_from_favorite($offer_id=0, $cust_id=0) {
      if($offer_id > 0 && $cust_id > 0) {
        $this->db->where('offer_id', $offer_id);
        $this->db->where('cust_id', $cust_id);
        $this->db->delete($this->_smp_favorite_offers);
        return $this->db->affected_rows();
      } return false;
    }
    public function register_purchased_addons($data=null) {
      if($data != null) {
        $this->db->insert($this->_smp_purchased_offer_addons, $data);
        return $this->db->insert_id();
      } return false;
    }
  //End Favorite offers------------------------------------

  //Start Jobs---------------------------------------------
    public function register_job_details($data=null) {
      if($data != null) {
        $this->db->insert($this->_smp_jobs, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_jobs_list($data=null) {
      if($data != null) {
        $this->db->select('c.cat_name, c.icon_url, ct.cat_type, country.country_name, cust.firstname AS pro_fname, cust.lastname AS pro_lname, pro.firstname AS pro_fname, pro.lastname AS pro_lname, free.firstname AS free_fname, free.lastname AS free_lname, free.avatar_url AS free_avatar_url, free.company_name AS free_company_name, free.response_time AS free_response_time, free.country_id AS free_country_id, free.introduction AS free_introduction, job.*');
        $this->db->from($this->_category_master.' as c');
        $this->db->from($this->_category_type_master.' as ct');
        $this->db->from($this->_countries.' as country');
        $this->db->from($this->_customers_master.' as pro');
        $this->db->from($this->_customers_master.' as cust');
        $this->db->from($this->_smp_freelancer_profile.' as free');
        $this->db->join($this->_smp_jobs.' as job', 'job.sub_cat_id = c.cat_id AND job.cat_id = ct.cat_type_id AND  job.cust_id = cust.cust_id AND job.provider_id = pro.cust_id AND job.provider_id = free.cust_id');

        if($data['cust_id'] > 0) { $this->db->where('job.cust_id', $data['cust_id']); } //Customer & Both Jobs/Offers
        if($data['provider_id'] > 0) { $this->db->where('job.provider_id', $data['provider_id']); } //Provider Projects/Offers
        if($data['job_status'] != 'NULL') $this->db->where('job.job_status', $data['job_status']); // status-wise
        if($data['sub_cat_id'] > 0) { $this->db->where('job.sub_cat_id', $data['sub_cat_id']); } //categories-wise
        if($data['cat_id'] > 0) { $this->db->where('job.cat_id', $data['cat_id']); } //Category type-wise

        if($data['start_date'] != 'NULL') { $this->db->where('job.cre_datetime >=', $data['start_date']); } //From date
        if($data['end_date'] != 'NULL') { $this->db->where('job.cre_datetime <', $data['end_date']); } //To date

        if($data['last_id'] > 0) {  $this->db->where('job.job_id > ', $data['last_id']); } //pagination
        if($data['device'] == 'mobile') { $this->db->limit(10); } //No of records for mobile

        $this->db->order_by('job.job_id', 'desc'); //recent first
        $this->db->group_by('job.job_id');
        return $this->db->get($this->_smp_jobs)->result_array();
      } return false;
    }
    public function get_job_details($job_id=0) {
      if($job_id > 0) {
        $this->db->select('c.cat_name, ct.cat_type, country.country_name, cust.firstname AS pro_fname, cust.lastname AS pro_lname, pro.firstname AS pro_fname, pro.lastname AS pro_lname, free.firstname AS free_fname, free.lastname AS free_lname, free.avatar_url AS free_avatar_url, free.company_name AS free_company_name, free.response_time AS free_response_time, free.country_id AS free_country_id, free.introduction AS free_introduction, job.*');
        $this->db->from($this->_category_master.' as c');
        $this->db->from($this->_category_type_master.' as ct');
        $this->db->from($this->_countries.' as country');
        $this->db->from($this->_customers_master.' as pro');
        $this->db->from($this->_customers_master.' as cust');
        $this->db->from($this->_smp_freelancer_profile.' as free');
        $this->db->join($this->_smp_jobs.' as job', 'job.sub_cat_id = c.cat_id AND job.cat_id = ct.cat_type_id AND job.country_id = country.country_id AND job.cust_id = cust.cust_id AND job.provider_id = pro.cust_id AND job.provider_id = free.cust_id AND job.job_id = ' . (int)($job_id));
        return $this->db->get($this->_smp_jobs)->row_array();
      } return false;
    }
    public function job_details_update($job_id=0, array $data) {
      if($job_id > 0 AND is_array($data)){     
        $this->db->where('job_id', $job_id);
        $this->db->update($this->_smp_jobs, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_job_cancellation_reasons() {
      return $this->db->get($this->_smp_job_cancellation_reason_master)->result_array();
    }
    public function update_job_details($job_id=0, $data=null) {
      if($data != null && $job_id > 0) {
        $this->db->where('job_id', (int)$job_id);
        $this->db->update($this->_smp_jobs, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_job_purchased_add_ons($job_id=0) {
      if($job_id > 0) {
        $this->db->where('job_id', (int)$job_id);            
        return $this->db->get($this->_smp_purchased_offer_addons)->result_array();            
      } return false;    
    }
  //End Jobs-----------------------------------------------

  //Start Promo Code---------------------------------------
    public function get_promocode_by_name($promo_code = null) {
      if($promo_code != null){
      $this->db->where('promo_code',$promo_code);
      $this->db->where('code_status', 1);
      $this->db->where('is_delete', 0);
      return $this->db->get($this->_promo_code)->row_array();
      } return false;
    }
    public function check_used_promo_code($promo_id = 0 , $promo_version = 0 , $cust_id = 0 , $cust_email = null) {
      if($promo_id>0 && $promo_version>0){
      $this->db->where('promo_code_id',$promo_id);
      $this->db->where('promo_code_version',$promo_version);
      if($cust_id > 0){ $this->db->where('cust_id' , $cust_id); }
      if($cust_email != null){ $this->db->where('customer_email' , $cust_email); }
      return $this->db->get($this->_promocode_used)->row_array();
      } return false;
    }
    public function get_used_promo_code($promo_id = 0 , $promo_version = 0) {
      if($promo_id>0 && $promo_version>0){
      $this->db->where('promo_code_id',$promo_id);
      $this->db->where('promo_code_version',$promo_version);
      return $this->db->get($this->_promocode_used)->result_array();
      } return false;
    }
    public function register_user_used_prmocode(array $update) {
      if(is_array($update)) {
      $this->db->insert($this->_promocode_used, $update);
      return $this->db->insert_id();
      } return false;
    }
  //End Promo Code-----------------------------------------

  //Start Workstream---------------------------------------
    public function get_workstream_details_by_job_id($job_id=0) {
      if($job_id > 0 ) {
        $this->db->where('job_id', (int)($job_id));
        return $this->db->get($this->_smp_workstream_master)->row_array();
      } return false;
    }
    public function get_workstream_details_by_job_id_admin($job_id=0 , $id=0 , $type="NULL") {
      if($job_id > 0 ) {
        $this->db->where('job_id', (int)($job_id));
        $this->db->where('is_admin', 1);
        if($type == "customer"){$this->db->where('cust_id', $id);}
        if($type == "provider"){$this->db->where('provider_id', $id);}
        return $this->db->get($this->_smp_workstream_master)->row_array();
      } return false;
    }
    public function update_to_workstream_master(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_workstream_master, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function update_to_workstream_details(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_workstream_details, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_workstream_details($job_id=0) {
      if($job_id > 0 ) {
        $this->db->where('job_id', (int)($job_id));
        return $this->db->get($this->_smp_workstream_details)->result_array();
      } return false;
    }
    public function get_job_workstream_details($job_id=0 , $cust_id=0 , $provider_id=0) {
      if($job_id > 0 && $provider_id > 0 && $cust_id > 0) {
        $this->db->where('job_id', (int)($job_id));
        $this->db->where('provider_id', (int)($provider_id));
        $this->db->where('cust_id', (int)($cust_id));
        return $this->db->get($this->_smp_workstream_details)->result_array();
      } return false;
    }
    public function get_job_workstream_details_admin_group($job_id=0) {
      if($job_id > 0) {
        $this->db->where('job_id', (int)($job_id));
        $this->db->where('provider_id !=', 0);
        $this->db->where('cust_id !=', 0);
        return $this->db->get($this->_smp_workstream_details)->result_array();
      } return false;
    }

    public function get_job_workstream_details_admin($job_id=0 , $cust_id=0 , $provider_id=0 , $type=null) {
      if($job_id > 0 ) {
        $this->db->where('job_id', (int)($job_id));
        if($type == "customer"){
          $this->db->where('cust_id', (int)($cust_id));
        }elseif($type == "provider"){
          $this->db->where('provider_id', (int)($provider_id));
        }
        $this->db->where('is_admin', 1);
        return $this->db->get($this->_smp_workstream_details)->result_array();
      } return false;
    }

  //End Workstream-----------------------------------------

  //Start Cleanup------------------------------------------
    public function services_job_cleanup() {
      $this->db->empty_table('tbl_smp_jobs');
      $this->db->empty_table('tbl_smp_job_payment_history');
      $this->db->empty_table('tbl_smp_job_dispute');
      $this->db->empty_table('tbl_smp_job_review');
      $this->db->empty_table('tbl_smp_job_proposals');
      $this->db->empty_table('tbl_smp_jobs_invite');
      $this->db->empty_table('tbl_smp_milestone_payment_request');
      $this->db->empty_table('tbl_smp_proposal_milestone_details');
      $this->db->empty_table('tbl_smp_payment_transfer_cron_job');
      $this->db->empty_table('tbl_smp_purchased_offer_addons');
      $this->db->empty_table('tbl_smp_workstream_details');
      $this->db->empty_table('tbl_smp_workstream_master');
      $this->db->empty_table('tbl_user_account_history');
      $this->db->empty_table('tbl_user_account_master');
      $this->db->empty_table('tbl_smp_user_account_history');
      $this->db->empty_table('tbl_smp_user_account_master');
      $this->db->empty_table('tbl_account_withdraw_request');
      $this->db->empty_table('tbl_gonagoo_account_history');
      $this->db->empty_table('tbl_gonagoo_account_master');
      $this->db->empty_table('tbl_deliverer_scrow_history');
      $this->db->empty_table('tbl_deliverer_scrow_master');
      $this->db->empty_table('tbl_smp_deliverer_scrow_history');
      $this->db->empty_table('tbl_smp_deliverer_scrow_master');
      $this->db->empty_table('tbl_smp_customer_scrow_history');
      $this->db->empty_table('tbl_smp_customer_scrow_master');
      $this->db->empty_table('tbl_smp_services_ratings');
      $this->db->empty_table('tbl_smp_read_workstream_notification');
      $this->db->empty_table('tbl_smp_job_payment_history');
      return true;
    }
  //End Cleanup--------------------------------------------

  //Start Job Dispute---------------------------------------
    public function get_dispute_category_master() {
      return $this->db->get($this->_smp_dispute_category_master)->result_array();
    }
    public function get_dispute_sub_category_cat_id($dispute_cat_id=0) {
      if($dispute_cat_id > 0) {
        $this->db->where('dispute_cat_id', $dispute_cat_id);
        return $this->db->get($this->_smp_dispute_sub_category_master)->result_array();
      } return false;
    }
    public function get_job_dispute_details($service_id=0) {
      if($service_id > 0) {
        $this->db->where('dispute_status', 'open');
        $this->db->where('service_id', $service_id);
        return $this->db->get($this->_smp_job_dispute)->row_array();
      } return false;
    }
    public function create_job_dispute(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_job_dispute, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function update_job_dispute($dispute_id=0, array $data) {
      if(is_array($data) && $dispute_id > 0) {
        $this->db->where('dispute_id', $dispute_id);
        $this->db->update($this->_smp_job_dispute, $data);
        return $this->db->affected_rows();
      } return false;
    }
  //End Job Dispute-----------------------------------------

  //Start Job review and ratings----------------------------
    public function insert_job_review(array $data) {
      if(is_array($data)) {
      $this->db->insert($this->_smp_job_review, $data);
      return $this->db->insert_id();
      } return false;
    }
    public function update_job_review($review_id=0, array $data) {
      if(is_array($data) && $review_id > 0) {
        $this->db->where('review_id', (int)$review_id);            
        $this->db->update($this->_smp_job_review, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_provider_service_rating($provider_id=0) {
      if($provider_id > 0) {
        $this->db->where('provider_id', (int)$provider_id);            
        return $this->db->get($this->_smp_services_ratings)->row_array();            
      } return false;    
    }
    public function get_review_details($review_id=0) {
      if($review_id > 0) {
        $this->db->where('review_id', (int)$review_id);            
        return $this->db->get($this->_smp_job_review)->row_array();            
      } return false;    
    }
    public function update_provider_service_ratings($rating_id=0, array $data) {
      if(is_array($data) && $rating_id > 0) {
        $this->db->where('rating_id', (int)$rating_id);            
        $this->db->update($this->_smp_services_ratings, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function insert_provider_service_ratings(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_services_ratings, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function update_provider_rating($cust_id=0, array $data) {
      if(is_array($data) && $cust_id != null) {
        $this->db->where('cust_id', trim($cust_id));
        $this->db->update($this->_customers_master, $data);
        return $this->db->affected_rows();
      } return false;
    }
  //Start Job review and ratings----------------------------



































    public function laundry_booking_and_customer_list($cust_id=0, $booking_type=null, $user_type=null, $group_by=0, $last_id=0, $device=0)
    {
      if($cust_id > 0 && !is_null($booking_type)) {

        $this->db->select('*, COUNT(tbl_laundry_booking.booking_id) AS booking_count, SUM(tbl_laundry_booking.total_price) AS amount_spent');

        $this->db->from('tbl_laundry_booking');
        $this->db->join('tbl_customers_master', 'tbl_laundry_booking.cust_id = tbl_customers_master.cust_id');

        if($user_type == 'provider') { $this->db->where('tbl_laundry_booking.provider_id', (int)$cust_id);
        } else { $this->db->where('tbl_laundry_booking.cust_id', (int)$cust_id); }

        if(trim($booking_type) != 'all') { $this->db->where('tbl_laundry_booking.booking_status', trim($booking_type)); }
        
        if(trim($group_by) == 1) { $this->db->group_by('tbl_laundry_booking.cust_id'); }
        
        if($last_id > 0) { $this->db->where('tbl_laundry_booking.booking_id <', (int)$last_id); }
        if($device > 0) { $this->db->limit(10); }
        
        $this->db->order_by('tbl_laundry_booking.booking_id', 'desc');

        return $this->db->get()->result_array();
      } return false;
    }
    public function laundry_booking_and_customer_provider_list($cust_id=0, $booking_type=null, $user_type=null, $provider_type=null, $provider_id=null, $group_by=0, $last_id=0, $device=0, $currency_sign='null')
    {
      if($cust_id > 0 && !is_null($booking_type)) {

        $this->db->from('tbl_laundry_booking');
        $this->db->join('tbl_customers_master', 'tbl_laundry_booking.cust_id = tbl_customers_master.cust_id');

        if($user_type == 'customer') {
          $this->db->where('tbl_laundry_booking.cust_id', (int)$cust_id);
        }

        if($provider_type == 'provider') {
          $this->db->where('tbl_laundry_booking.provider_id', (int)$provider_id);
        }

        if($currency_sign != 'null') {
          $this->db->where('tbl_laundry_booking.currency_sign', $currency_sign);
        }

        /*if($user_type == 'provider') { $this->db->where('tbl_laundry_booking.provider_id', (int)$cust_id);
        } else { $this->db->where('tbl_laundry_booking.cust_id', (int)$cust_id); }*/

        if(trim($booking_type) != 'all') { $this->db->where('tbl_laundry_booking.booking_status', trim($booking_type)); }
        
        if(trim($group_by) == 1) { $this->db->group_by('tbl_laundry_booking.cust_id'); }
        
        if($last_id > 0) { $this->db->where('tbl_laundry_booking.booking_id <', (int)$last_id); }
        if($device > 0) { $this->db->limit(10); }
        
        $this->db->order_by('tbl_laundry_booking.booking_id', 'desc');

        return $this->db->get()->result_array();
      } return false;
    }
    public function get_user_login_history($type=null, $cust_ids=null, $country_id=0, $rows=0)
    {
      if($type != null && $cust_ids != null && $type == 'recently'){
        $this->db->where("cust_id IN ($cust_ids)");
        if($rows > 0) { $this->db->limit($rows); }
        $this->db->order_by('last_login_datetime','DESC');
        return $this->db->get($this->_customers_master)->result_array();
      } else if($type != null && $cust_ids != null && $type == 'frequently'){
        $this->db->select('l.*, COUNT(l.cust_id) AS userCount, c.*');
        $this->db->from('tbl_cust_login_stats AS l');
        $this->db->join('tbl_customers_master AS c', 'c.cust_id = l.cust_id');
        if($rows > 0) { $this->db->limit($rows); }

        $this->db->where("c.cust_id IN ($cust_ids)");
        
        $this->db->group_by('l.cust_id');
        $this->db->order_by('userCount','DESC');
        return $this->db->get()->result_array();

      } else return false;
    }
    public function laundry_bookings_day_wise($cust_id=0, $day_type=null, $user_type=null, $last_id=0, $device=0, $from_date='NULL', $to_date='NULL')
    {
      if($cust_id > 0 && !is_null($day_type)) {
        if($user_type == 'provider') { $this->db->where('provider_id', (int)$cust_id);
        } else { $this->db->where('cust_id', (int)$cust_id); }

        if($day_type == 'today') {
          $this->db->where('cre_datetime >=', date('Y-m-d').' 00:00:00');
          $this->db->where('cre_datetime <=', date('Y-m-d').' 23:59:59');
        }

        if($day_type == 'week') {
          $this->db->where('cre_datetime >= ', (date('D') != 'Mon') ? date('Y-m-d', strtotime('last Monday')) . ' 00:00:00' : date('Y-m-d').  ' 00:00:00');
          $this->db->where('cre_datetime <= ', (date('D') != 'Sat') ? date('Y-m-d', strtotime('next Sunday')) . ' 23:59:59' : date('Y-m-d').  ' 23:59:59');
        }

        if($day_type == 'month') {
          $this->db->where('cre_datetime >= ', date('Y-m-01') . ' 00:00:00');
          $this->db->where('cre_datetime <= ', date('Y-m-t').  ' 23:59:59');
        }

        if($from_date != 'NULL' && $to_date != 'NULL') { 
          $this->db->where('cre_datetime >= ', $from_date);
          $this->db->where('cre_datetime <= ', $to_date);
        }
        return $this->db->get($this->_laundry_booking)->result_array();
      } return false;
    }
    public function laundry_sales_day_wise($cust_id=0, $day_type=null, $last_id=0, $device=0, $from_date='NULL', $to_date='NULL', $currency_sign='NULL')
    {
      if($cust_id > 0 && !is_null($day_type)) {
        $this->db->where('user_id', (int)$cust_id);
        $this->db->where('cat_id', 9);

        if($day_type == 'today') {
          $this->db->where('datetime >=', date('Y-m-d').' 00:00:00');
          $this->db->where('datetime <=', date('Y-m-d').' 23:59:59');
        }

        if($day_type == 'week') {
          $this->db->where('datetime >= ', (date('D') != 'Mon') ? date('Y-m-d', strtotime('last Monday')) . ' 00:00:00' : date('Y-m-d').  ' 00:00:00');
          $this->db->where('datetime <= ', (date('D') != 'Sat') ? date('Y-m-d', strtotime('next Sunday')) . ' 23:59:59' : date('Y-m-d').  ' 23:59:59');
        }

        if($day_type == 'month') {
          $this->db->where('datetime >= ', date('Y-m-01') . ' 00:00:00');
          $this->db->where('datetime <= ', date('Y-m-t').  ' 23:59:59');
        }

        if($from_date != 'NULL' && $to_date != 'NULL') { 
          $this->db->where('datetime >= ', $from_date);
          $this->db->where('datetime <= ', $to_date);
        }

        if($currency_sign != 'NULL') { 
          $this->db->where('currency_code', $currency_sign);
        }
        return $this->db->get($this->_user_account_history)->result_array();
      } return false;
    }
    public function laundry_claim_day_wise($cust_id=0, $day_type=null, $user_type=null, $last_id=0, $device=0, $from_date='NULL', $to_date='NULL')
    {
      if($cust_id > 0 && !is_null($day_type)) {
        if($user_type == 'provider') { $this->db->where('provider_id', (int)$cust_id);
        } else { $this->db->where('cust_id', (int)$cust_id); }

        if($day_type == 'today') {
          $this->db->where('cre_datetime >=', date('Y-m-d').' 00:00:00');
          $this->db->where('cre_datetime <=', date('Y-m-d').' 23:59:59');
        }

        if($day_type == 'week') {
          $this->db->where('cre_datetime >= ', (date('D') != 'Mon') ? date('Y-m-d', strtotime('last Monday')) . ' 00:00:00' : date('Y-m-d').  ' 00:00:00');
          $this->db->where('cre_datetime <= ', (date('D') != 'Sat') ? date('Y-m-d', strtotime('next Sunday')) . ' 23:59:59' : date('Y-m-d').  ' 23:59:59');
        }

        if($day_type == 'month') {
          $this->db->where('cre_datetime >= ', date('Y-m-01') . ' 00:00:00');
          $this->db->where('cre_datetime <= ', date('Y-m-t').  ' 23:59:59');
        }

        if($from_date != 'NULL' && $to_date != 'NULL') { 
          $this->db->where('cre_datetime >= ', $from_date);
          $this->db->where('cre_datetime <= ', $to_date);
        }
        return $this->db->get($this->_service_claims)->result_array();
      } return false;
    }
    public function laundry_sales_day_wise_count($data)
    {
      if(is_array($data)) {
        $this->db->select('*, SUM(amount) AS total_sales');
        $this->db->where('user_id', (int)$data['cust_id']);
        $this->db->where('cat_id', (int)$data['cat_id']);

        $this->db->where('datetime >=', $data['from_date']);
        $this->db->where('datetime <=', $data['to_date']);
        
        $this->db->where('currency_code', $data['currency_code']);

        $this->db->group_by('currency_code');

        return $this->db->get($this->_user_account_history)->result_array();
      } return false;
    }
    public function get_claim_status_wise(array $data)
    {
      if(is_array($data)) {
        if($data['user_type'] == 'customer') { $this->db->where('cust_id', $data['cust_id']);
        } else { $this->db->where('provider_id', $data['cust_id']); }
        $this->db->where('service_cat_id', $data['service_cat_id']);
        $this->db->where('claim_status', $data['claim_status']);
        $this->db->order_by('claim_id', 'desc');
        return $this->db->get($this->_service_claims)->result_array();
      } return false;
    }


  

  //Start Manage Laundry charges-----------------------------
    public function get_laundry_charges_list($cust_id=0, $last_id='NULL')
    {
      if($cust_id > 0 ){
        $this->db->select('*');
        $this->db->from('tbl_laundry_charges ch'); 
        $this->db->join('tbl_laundry_category cat', 'cat.cat_id=ch.cat_id', 'left');
        $this->db->join('tbl_laundry_sub_category sub', 'sub.sub_cat_id=ch.sub_cat_id', 'left');
        $this->db->join('tbl_countries cont', 'cont.country_id=ch.country_id', 'left');
        $this->db->where('ch.cust_id', (int) $cust_id);
        if($last_id != 'NULL') {
          $this->db->where('ch.charge_id >', (int)$last_id);
          $this->db->limit(10);
          $this->db->order_by("ch.charge_id", "asc");
        }
        $this->db->order_by('ch.charge_id','desc'); 
        return $this->db->get()->result_array();
      } return false;
    }
    public function get_laundry_category_list()
    {
      return $this->db->get($this->_laundry_category)->result_array();
    }
    public function get_laundry_category_details($cat_id=0)
    {
      $this->db->where('cat_id', (int)$cat_id);
      return $this->db->get($this->_laundry_category)->row_array();
    }
    public function get_laundry_sub_category_list($cat_id=0, $all=0)
    {
      if($cat_id > 0) {
        if($all <= 0) { $this->db->where('cat_id', (int)$cat_id); }
        return $this->db->get($this->_laundry_sub_category)->result_array();
      } return false;
    }
    public function add_laundry_charge_details(array $insert_data)
    {
      if(is_array($insert_data)){
        $this->db->insert($this->_laundry_charges, $insert_data);
        return $this->db->insert_id();
      } return false;
    }
    public function check_laundry_charge_details($insert_data=null)
    {
      if(is_array($insert_data)) {
        $this->db->where('cust_id', $insert_data['cust_id']);
        $this->db->where('cat_id', $insert_data['cat_id']);
        $this->db->where('sub_cat_id', $insert_data['sub_cat_id']);
        $this->db->where('country_id', $insert_data['country_id']);
        if($this->db->get($this->_laundry_charges)->row()) { return true;
        } return false;
      } return false;
    }    
    public function delete_laundry_charge_details($charge_id=0)
    {
      if($charge_id > 0 ){
        $this->db->where('charge_id', (int) $charge_id);
        $this->db->delete($this->_laundry_charges);
        return $this->db->affected_rows();
      } return false;
    } 
    public function update_laundry_charge_amount_details($amount=0, $charge_id=0)
    {
      if($amount >= 0 && $charge_id > 0 ){
        $this->db->where('charge_id', (int) $charge_id);
        $this->db->update($this->_laundry_charges, ['charge_amount' => trim($amount)]);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_country_currencies($country_id=0)
    {
      if($country_id > 0 ) {
        $this->db->select('cc.currency_id, cr.currency_sign, cr.currency_title');
        $this->db->from('tbl_country_currency as cc');
        $this->db->join('tbl_currency_master as cr', 'cr.currency_id = cc.currency_id');
        $this->db->where('cc.country_id', (int) $country_id);
        return $this->db->get()->result_array();
      }
      return false;
    }
  //End Manage Laundry charges-------------------------------

  //Start Bus cancellation and rescheduling charges----------
    public function get_laundry_provider_cancellation_details($id=0)
    {
      if( $id > 0 ) {
        $this->db->order_by('charge_id', 'desc');
        $this->db->where('cust_id', (int) $id);
        return $this->db->get($this->_laundry_provider_cancellation_charges)->result_array();
      }
      return false;
    }
    public function get_master_cancellation_details()
    { 
      $this->db->where('status', 1);
      return $this->db->get($this->_laundry_advance_payment)->result_array();
    }
    public function check_country_cancellation_charges(array $data)
    {
      if(is_array($data)) {
        $this->db->select('charge_id');
        $this->db->where('country_id', (int)trim($data['country_id']));
        $this->db->where('cust_id', (int)trim($data['cust_id']));
        if($res = $this->db->get($this->_laundry_provider_cancellation_charges)->row_array()) { return true; }
      } return false;
    }
    public function register_laundry_provider_cancellation_details(array $data)
    {
      if(is_array($data)){
        $this->db->insert($this->_laundry_provider_cancellation_charges, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function delete_cencellation_charges_details($charge_id=0)
    {
      if($charge_id > 0 ){
        $this->db->where('charge_id', (int) $charge_id);
        $this->db->delete($this->_laundry_provider_cancellation_charges);
        return $this->db->affected_rows();
      } return false;
    }
    public function update_cancellation_percentage_details(array $update_data, $charge_id=0)
    {
      if(is_array($update_data) && $charge_id > 0){
        $this->db->where('charge_id', (int)$charge_id);
        $this->db->update($this->_laundry_provider_cancellation_charges, $update_data);
        return $this->db->affected_rows();
      } return false;
    }
  //End Bus cancellation and rescheduling charges------------

  //Start Favorite Deliverers For Laundry Providers----------
    public function get_favourite_deliverers($cust_id=0)
    {
      if($cust_id > 0 ) {
        return $this->db->query('SELECT d.*, c.ratings FROM tbl_deliverer_profile as d JOIN tbl_customers_master AS c ON d.cust_id = c.cust_id WHERE d.cust_id IN ( SELECT deliverer_id FROM tbl_favourite_deliverers WHERE cust_id = '.$cust_id.' )')->result_array();
      } return false;
    }
    public function get_courir_deliverer_profile($cust_id=0)
    {
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int) $cust_id);
        return $this->db->get($this->_deliverer)->row_array();
      } return false;
    }
    public function get_consumer_datails($cust_id=0, $all=false)
    {
      if($cust_id > 0 ) {
        if(!$all) {
          $this->db->select('cust_id, firstname, lastname, email1, mobile1, cust_dob, gender, city_id, state_id, country_id, avatar_url, cover_url,  email_verified,  mobile_verified, cre_datetime, last_login_datetime, profession, overview,  cust_status, social_login, social_type, user_type, lang_known, is_deliverer, ratings, total_ratings, no_of_ratings, cust_otp, acc_type');
        }
        $this->db->where('cust_id', (int)($cust_id));
        $this->db->where('cust_status', 1);
        return $this->db->get($this->_customers_master)->row_array();
      } return false;
    }
    public function get_reviews($cust_id=0)
    {
      if($cust_id > 0 ) {
        $this->db->where('deliverer_id', (int)($cust_id));
        return $this->db->get($this->_courier_order_review)->result_array();
      } return false;
    }
    public function register_new_favourite_deliverer($cust_id=0, $deliverer_id=0)
    {
      if($cust_id > 0 AND $deliverer_id > 0) {
        $insert_data = array(
          "cust_id" => (int) $cust_id,
          "deliverer_id" => (int) $deliverer_id,
          "cre_datetime" => date('Y-m-d H:i:s'),
        );
        return $this->db->insert($this->_favourite_deliverers, $insert_data);
      } return false;
    }
    public function remove_favourite_deliverer($cust_id=0, $deliverer_id=0)
    {
      if($cust_id > 0 AND $deliverer_id > 0) {
        
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('deliverer_id', (int) $deliverer_id);
        $this->db->delete($this->_favourite_deliverers);
        return $this->db->affected_rows();
      } return false;
    }
  //End Favorite Deliverers For Laundry Providers------------

  //Start Favorite Providers For Laundry Buyers--------------
    public function get_favourite_providers($cust_id=0)
    {
      if($cust_id > 0 ) {
        return $this->db->query('SELECT d.*, c.ratings FROM tbl_laundry_provider_profile as d JOIN tbl_customers_master AS c ON d.cust_id = c.cust_id WHERE d.cust_id IN ( SELECT deliverer_id FROM tbl_laundry_favourite_providers WHERE cust_id = '.$cust_id.' )')->result_array();
      } return false;
    }

    public function search_for_providers($login_cust_id=0, array $data)
    {
      //echo var_dump($order_id); die();
      if($login_cust_id > 0){
        $qry = "SELECT d.*, c.ratings FROM tbl_laundry_provider_profile AS d JOIN tbl_customers_master AS c ON d.cust_id = c.cust_id WHERE d.cust_id != ". (int) $login_cust_id." AND c.acc_type IN ('seller','both') AND c.is_deliverer = 1";
        if($data['country_id'] > 0 ) { $qry .= " AND d.country_id = ". (int) $data['country_id']; }
        if($data['state_id'] > 0 ) { $qry .= " AND d.state_id = ". (int) $data['state_id']; }
        if($data['city_id'] > 0 ) { $qry .= " AND d.city_id = ". (int) $data['city_id']; }
        if($data['rating'] > 0 ) { $qry .= " AND ROUND(c.ratings) = ". (int) $data['rating']; }
        return $this->db->query($qry)->result_array();
      } return false;
    }
    public function get_favourite_providers_ids($cust_id=0)
    {
      $ids = array();
      if($cust_id > 0 ) {
        $this->db->select('deliverer_id');
        $this->db->where('cust_id', (int)$cust_id);
        $return = $this->db->get($this->_laundry_favourite_providers)->result_array();     
        foreach ($return as $r){ $ids[] = $r['deliverer_id']; }
      } return $ids;
    }
    public function check_is_favourite_providers_id($cust_id=0, $provider_id=0)
    {
      $ids = array();
      if($cust_id > 0 ) {
        $this->db->where('cust_id', (int)$cust_id);
        $this->db->where('deliverer_id', (int)$provider_id);
        $return = $this->db->get($this->_laundry_favourite_providers)->result_array();     
      } return false;
    }
    public function register_new_favourite_provider($cust_id=0, $deliverer_id=0)
    {
      if($cust_id > 0 AND $deliverer_id > 0) {
        $insert_data = array(
          "cust_id" => (int) $cust_id,
          "deliverer_id" => (int) $deliverer_id,
          "cre_datetime" => date('Y-m-d H:i:s'),
        );
        return $this->db->insert($this->_laundry_favourite_providers, $insert_data);
      } return false;
    }
    public function remove_favourite_provider($cust_id=0, $deliverer_id=0)
    {
      if($cust_id > 0 AND $deliverer_id > 0) {
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('deliverer_id', (int) $deliverer_id);
        $this->db->delete($this->_laundry_favourite_providers);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_deliverer_details($deliverer_id=0)
    {
      if($deliverer_id > 0 ) {
        $this->db->where('deliverer_id', (int) $deliverer_id);
        return $this->db->get($this->_laundry_provider_profile)->row_array();
      } return null;
    }
    public function get_providers_list($cust_id=0, $id=0, $from_country_id=0, $from_state_id=0, $from_city_id=0, $rating=0)
    {
      if($cust_id > 0 ) {
        $this->db->select('d.*, c.ratings, c.total_ratings, c.no_of_ratings');  
        $this->db->from($this->_laundry_provider_profile.' as d');
        $this->db->join($this->_customers_master.' as c', 'd.cust_id = c.cust_id');
        $this->db->where('c.cust_id !=', (int) $cust_id);
        if($from_country_id > 0) { $this->db->where('d.country_id', $from_country_id); }
        if($from_state_id > 0) { $this->db->where('d.state_id', $from_state_id); }
        if($from_city_id > 0) { $this->db->where('d.city_id', $from_city_id); }
        if($rating > 0) { $this->db->where('c.ratings', $rating); }
        $this->db->where('c.is_deliverer', 1);
        if($id > 0) { $this->db->where('d.deliverer_id >', $id); }
        $this->db->where('c.cust_status', 1);
        $this->db->order_by('d.deliverer_id', 'ASC');
        $this->db->group_by('c.cust_id');
        $this->db->limit(20);
        return $this->db->get()->result_array();
      } 
      return false;
    }
    public function verify_favorite($did=0, $cust_id=0)
    {
      if($did > 0 && $cust_id > 0) {
        $this->db->where('deliverer_id', (int) $did);
        $this->db->where('cust_id', (int) $cust_id);
        return $this->db->get($this->_laundry_favourite_providers)->row_array();
      }
      return false;
    } 
  //End Favorite Providers For Laundry Buyers----------------

  //Start Admin Notifications--------------------------------
    public function get_notifications($user_id=0, $type=null, $limit=50, $last_id = 0)
    { 
      $qry = "SELECT * FROM `tbl_notifications` WHERE ( `consumer_id` = $user_id OR `consumer_id` = 0 ) AND (`consumer_filter` = 'All' OR `consumer_filter` = '".$type."' )";
      if($last_id > 0 ){ $qry .= " AND id < ". $last_id; }
      $qry .= " ORDER BY `cre_datetime` DESC ";
      if($limit > 0 ) { $qry .= " LIMIT $limit"; }
      return $this->db->query($qry)->result_array();
    }
    public function get_notification($id=0)
    {
      if($id>0){
        $this->db->where('id', $id);
        return $this->db->get($this->_notifications)->row_array();
      } return false;
    }
    public function get_total_unread_notifications_count_old($cust_id=0, $type=null)
    {
      $qry = "SELECT id FROM `tbl_notifications` WHERE id NOT IN ( SELECT notification_id FROM tbl_read_notifications WHERE cust_id = $cust_id ) AND ( `consumer_id` = $cust_id OR `consumer_id` = 0 ) AND (`consumer_filter` = 'All' OR `consumer_filter` = '".$type."' )";
      return $this->db->query($qry)->num_rows();    
    }
    public function get_total_unread_notifications_old($cust_id=0,$type=null, $limit=0)
    {
      $qry = "SELECT id, notify_title, notify_text, attachement_url, cre_datetime FROM `tbl_notifications` WHERE id NOT IN ( SELECT notification_id FROM tbl_read_notifications WHERE cust_id = $cust_id ) AND ( `consumer_id` = $cust_id OR `consumer_id` = 0 ) AND (`consumer_filter` = 'All' OR `consumer_filter` = '".$type."' ) ORDER BY `cre_datetime` DESC "; 
      if($limit > 0 ) { $qry .= " LIMIT $limit"; }
      return  $this->db->query($qry)->result_array();
    }
    public function make_notification_read_old($id=0, $cust_id=0)
    {
      if($id > 0 AND $cust_id > 0) {
        $this->db->insert($this->_read_notifications, ['cust_id' => $cust_id, 'notification_id' => $id, 'read_datetime' => date('Y-m-d H:i:s')]);
        return true;
      } return false;
    }
  //End Admin Notifications----------------------------------

  
  
  //Start Laundry Provider Orders----------------------------
    public function laundry_booking_list($cust_id=0, $booking_type=null, $user_type=null, $last_id=0, $device=0)
    {
      if($cust_id > 0 && !is_null($booking_type)) {
        if($user_type == 'provider') { $this->db->where('provider_id', (int)$cust_id);
        } else { $this->db->where('cust_id', (int)$cust_id); }
        if(trim($booking_type) != 'all') { $this->db->where('booking_status', trim($booking_type)); }
        if($last_id > 0) { $this->db->where('booking_id <', (int)$last_id); }
        if($device > 0) { $this->db->limit(10); }
        $this->db->order_by('booking_id', 'desc');
        return $this->db->get($this->_laundry_booking)->result_array();
      } return false;
    }
    public function laundry_booking_details_list($booking_id=0)
    {
      if($booking_id > 0) {
        $this->db->select('*');
        $this->db->from('tbl_laundry_booking_details dtls'); 
        $this->db->join('tbl_laundry_category cat', 'cat.cat_id=dtls.cat_id', 'left');
        $this->db->join('tbl_laundry_sub_category sub', 'sub.sub_cat_id=dtls.sub_cat_id', 'left');
        $this->db->where('dtls.booking_id', (int) $booking_id);
        return $this->db->get()->result_array();
      } return false;
    }
    public function laundry_booking_details($booking_id=0)
    {
      if($booking_id > 0) {
        $this->db->where('booking_id', (int) $booking_id);
        return $this->db->get($this->_laundry_booking)->row_array();
      } return false;
    }
    public function laundry_pickup_drop_order_details($order_id=0)
    {
      if($order_id > 0) {
        $this->db->where('order_id', (int)$order_id);
        $this->db->where('is_laundry', 1);
        return $this->db->get($this->_courier_orders)->result_array();
      } return false;
    }
    public function get_category_details_by_id($cat_id=0)
    {
      if($cat_id > 0) {
        $this->db->where('cat_id', (int)$cat_id);
        return $this->db->get($this->_category_master)->row_array();
      } return false;
    }
    public function laundry_booking_update(array $data, $booking_id=0)
    {
      if(is_array($data) && $booking_id > 0){
        $this->db->where('booking_id', (int) $booking_id);
        $this->db->update($this->_laundry_booking, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function order_status_list($order_id=0)
    {
      if($order_id > 0 ) {
        $this->db->where('order_id', (int)($order_id));
        $this->db->where('status !=', 'reject');
        return $this->db->get($this->_courier_order_status)->result_array();
      } return false;
    }
    
    public function sendSMS($user_mobile=null, $message=null)
    {
      if($message!=null && $user_mobile!=null) {
        $message .= ' - Powered by Gonagoo';
        if(substr($user_mobile, 0, 1) == 0) { $user_mobile = ltrim($user_mobile, 0); }
        $ch = curl_init("http://smsc.txtnation.com:8091/sms/send_sms.php?"); // url to send sms
        curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
        curl_setopt($ch, CURLOPT_POST, 1); // method to call url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
        curl_setopt($ch, CURLOPT_POSTFIELDS,"src=330679328525&dst=$user_mobile&dr=1&user=nkengne&password=P9cKxW&type=0&msg=$message"); 
        $outputSMS = curl_exec($ch); // execute url and save response
        curl_close($ch); // close url connection
        return true;
      }
    }
    public function sendFCM(array $msg, array $reg_id, $api_key=null)
    {
      if(is_array($msg) && is_array($reg_id) && !is_null($api_key)) {
        $header = array('Authorization: key='.$api_key,'Content-Type: application/json');
        $fields = array('registration_ids' => $reg_id, 'data' => $msg);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result_fcm = curl_exec($ch);
        curl_close($ch);
        return 1;
      }
    }
    public function sendPushIOS(array $data, $tokens, $api_key=null)
    {
      $fcmMsg = array(
        'body' => $data['text'],
        'title' => $data['title'],
        'sound' => "default",
        'color' => "#203E78" 
      );
      $fcmFields = array(
        'to' => $tokens,
        'priority' => 'high',
        'notification' => $fcmMsg
      );
      $headers = array(
        'Authorization: key=' . $api_key,
        'Content-Type: application/json'
      );
      $ch = curl_init();
      curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
      curl_setopt( $ch,CURLOPT_POST, true );
      curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
      curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
      $result = curl_exec($ch );
      curl_close( $ch );    
    }
    public function sendEmail($user_name, $user_email, $subject=null, $message=null) 
    {
      $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
      $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
      
      $messageBody .="</head><body bgcolor='#FFFFFF'>";
      $messageBody .="<table class='body-wrap'><tr><td></td><td class='container'><div class='content'>";
      $messageBody .="<table><tr><td align='center'><h3>".$subject."</h3></td></tr>";
      $messageBody .="<tr><td align='center'><h3>".$this->lang->line('Dear')." <small>".$user_name."</small>,</h3>";
      $messageBody .="<p class='lead'>". $message . "</p>";
      $messageBody .="</td></tr>";
      $messageBody .="</table></div></td><td></td></tr></table>";

      //Email Signature
      $messageBody .="<table class='head-wrap'>";
      $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
      $messageBody .="<tr><td>".$this->lang->line('Your Gonagoo team!')."</td></tr>";
      $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />".$this->lang->line('slider_heading1')."</td></tr>";
      $messageBody .="<tr><td>".$this->lang->line('Support')." : support@gonagoo.com</td></tr>";
      $messageBody .="<tr><td>".$this->lang->line('Website')." : www.gonagoo.com</td></tr>";
      //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
      $messageBody .="<tr><td>".$this->lang->line('Join us on')." : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='G+'></a>";
      $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='Twitter'></a>";
      $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='LinkedIn'></a>";
      $messageBody .="<a href='https://www.facebook.com/gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/facebook.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='FB'></a></td></tr>";
      //Download the App <icon for iOS App download><icon for Android App download>
      $messageBody .="<tr><td>Download the App : <br /><a href='https://apple.co/2z5N4jA' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='iOS'></a>";
      $messageBody .="<a href='https://tinyurl.com/ya458s6u' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='Android'></a></td></tr>";
      $messageBody .="</table></div></td><td></td></tr></table>";
      //Email Signature End
      $messageBody .="</body></html>";

      $email_from = $this->config->item('from_email');
      $email_subject = $subject;
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= 'From: Gonagoo <'.$email_from.'>' . "\r\n";
      mail($user_email, $email_subject, $messageBody, $headers);  
      return 1;
    }
    public function update_to_workroom(array $data)
    {
      if(!empty($data)){
        $this->db->insert($this->_courier_workroom, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_words($sentence, $count=10) {
      preg_match("/(?:\w+(?:\W+|$)){0,$count}/", $sentence, $matches);
      return $matches[0];
    }
    public function update_courier_order_details(array $data, $order_id=null)
    {
      if(is_array($data) && $order_id > 0){
        $this->db->where('order_id', (int)$order_id );
        $this->db->update($this->_courier_orders, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function pending_invoice_list($cust_id=0, $booking_type=null, $user_type=null, $complete_paid=0, $last_id=0, $device=0, $date_start=null, $date_end=null, $country_id=null, $currency_sign=null)
    {
      if($cust_id > 0 && !is_null($booking_type)) {
        if($user_type == 'provider') { $this->db->where('provider_id', (int)$cust_id);
        } else { $this->db->where('cust_id', (int)$cust_id); }
        if(trim($booking_type) != 'all') { $this->db->where('booking_status', trim($booking_type)); }
        
        if($date_start != null) { $this->db->where('cre_datetime >=', $date_start); }
        if($date_end != null) { $this->db->where('status_update_datetime <', $date_end); }
        if($country_id != null) { $this->db->where('country_id', $country_id); }
        if($currency_sign != null) { $this->db->where('currency_sign', $currency_sign); }
        
        $this->db->where('complete_paid', 0);
        
        if($last_id > 0) { $this->db->where('booking_id <', (int)$last_id); }
        if($device > 0) { $this->db->limit(10); }
        $this->db->order_by('booking_id', 'desc');
        return $this->db->get($this->_laundry_booking)->result_array();
      } return false;
    }
  //End Laundry Provider Orders------------------------------

  //Start Laundry Payment Related----------------------------
    public function get_gonagoo_laundry_commission_details($country_id=0)
    { 
      if($country_id > 0){ 
        $this->db->where('country_id', (int)$country_id); 
        return $this->db->get($this->_laundry_advance_payment)->row_array();
      } return false;
    }
  //End Laundry Payment Related------------------------------

  //Start Workroom-------------------------------------------
    public function get_laundry_workroom_chat($booking_id=0, $last_id=0)
    {
      if( $booking_id > 0 ) { 
        if($last_id > 0) { $this->db->where('ow_id >', $last_id); }
        $this->db->where('order_id', $booking_id);
        $this->db->where('cat_id', 9);
        $this->db->order_by('ow_id', 'desc');
        return $this->db->get($this->_courier_workroom)->result_array();
      } return false;
    }
    public function add_laundry_chat_to_workroom(array $data)
    {
      if(!empty($data)){
        $this->db->insert($this->_courier_workroom, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_laundry_review_details($cust_id=0)
    {
      if($cust_id > 0 ) {
        $this->db->select('ratings, total_ratings, no_of_ratings');
        $this->db->where('operator_id', (int)($cust_id));
        return $this->db->get($this->_laundry_ratings)->row_array();
      } return false;
    }
    public function update_laundry_ratings(array $laundry_review, $operator_id=0, $key= "update")
    {
      if(is_array($laundry_review) && $operator_id != null) {
        if($key == "update"){
          $this->db->where('operator_id', trim($operator_id));
          $this->db->update($this->_laundry_ratings, $laundry_review);
        } else { $this->db->insert($this->_laundry_ratings, $laundry_review);
        } return $this->db->affected_rows();
      } return false;
    }
    public function insert_laundry_review_details(array $data)
    {
      if(is_array($data)) {
        $this->db->insert($this->_laundry_ratings, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function update_deliverer_rating(array $deliverer_review, $deliverer_id=0)
    {
      if(is_array($deliverer_review) && $deliverer_id != null) {
        $this->db->where('cust_id', trim($deliverer_id));
        $this->db->update($this->_customers_master, $deliverer_review);
        return $this->db->affected_rows();
      } return false;
    }
    public function post_to_review_laundry(array $insert_data)
    {
      if(is_array($insert_data)) {
        $this->db->insert($this->_laundry_review, $insert_data);
        return $this->db->insert_id();
      } return false;
    }
    public function update_laundry_review(array $data, $booking_id=0)
    {
      if(is_array($data) && $booking_id != null) {
        $this->db->where('booking_id', trim($booking_id));
        $this->db->update($this->_laundry_booking, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function get_total_unread_order_workroom_notifications($booking_id=0, $cust_id=0)
    {
      $qry = "SELECT ow_id FROM tbl_courier_workroom WHERE ow_id NOT IN ( SELECT notification_id FROM tbl_read_workroom_notification WHERE cust_id = $cust_id AND notification_id != ow_id ) AND  sender_id != $cust_id AND order_id >= $booking_id ";
      return $this->db->query($qry)->result_array();  
    }
    public function make_workroom_notification_read($id=0, $cust_id=0)
    {
      if($id > 0 AND $cust_id > 0) {
        $data = ['cust_id' => $cust_id, 'notification_id' => $id, 'read_datetime' => date('Y-m-d H:i:s')];
        $this->db->insert($this->_read_workroom_notification, $data);
        return true;
      } return false;
    }
    public function get_bus_review_details($cust_id=0)
    {
      if($cust_id > 0 ) {
        $this->db->select('ratings, total_ratings, no_of_ratings');
        $this->db->where('operator_id', (int)$cust_id);
        return $this->db->get($this->_bus_ratings)->row_array();
      }
      return false;
    }
  //End Workroom---------------------------------------------

  //Start Constructor Functions------------------------------
    public function update_last_login($cust_id=0)
    {
      if($cust_id>0){
        $today = date('Y-m-d H:i:s');
        $this->db->where('cust_id', $cust_id);
        $this->db->update($this->_customers_master, array("last_login_datetime" => $today));
        return $this->db->affected_rows();
      } return false;
    }
    public function get_total_unread_order_chatroom_notifications($booking_id=0, $cust_id=0)
    {
      $qry = "SELECT chat_id FROM tbl_driver_chats WHERE chat_id NOT IN ( SELECT notification_id FROM tbl_read_chat_notification WHERE cust_id = $cust_id AND notification_id != chat_id ) AND ( receiver_id = $cust_id ) AND ( order_id >= $booking_id )";
      return $this->db->query($qry)->result_array();  
    }
    public function make_chatrooom_notification_read($id=0, $cust_id=0)
    {
      if($id > 0 AND $cust_id > 0) {
        $data = ['cust_id' => $cust_id, 'notification_id' => $id, 'read_datetime' => date('Y-m-d H:i:s')];
        $this->db->insert($this->_read_chat_notification, $data);
        return true;
      } return false;
    }
    public function get_total_unread_workroom_notifications_count_old($cust_id=0)
    {
      $qry = "SELECT ow_id FROM tbl_courier_workroom WHERE ow_id NOT IN ( SELECT notification_id FROM tbl_read_workroom_notification WHERE cust_id = $cust_id ) AND  sender_id != $cust_id AND (cust_id = $cust_id OR deliverer_id = $cust_id)"; 
      return $this->db->query($qry)->num_rows();    
    }
    public function get_total_unread_workroom_notifications_old($cust_id=0, $limit=5)
    {
      $qry = "SELECT * FROM tbl_courier_workroom WHERE ow_id NOT IN ( SELECT notification_id FROM tbl_read_workroom_notification WHERE cust_id = $cust_id ) AND  sender_id != $cust_id AND (cust_id = $cust_id OR deliverer_id = $cust_id) ORDER BY `cre_datetime` DESC LIMIT $limit";
      return $this->db->query($qry)->result_array();    
    }
    public function get_total_unread_chatroom_notifications_count($cust_id=0)
    {
      $qry = "SELECT chat_id FROM tbl_driver_chats WHERE chat_id NOT IN ( SELECT notification_id FROM tbl_read_chat_notification WHERE cust_id = $cust_id ) AND ( receiver_id = $cust_id )";
      return $this->db->query($qry)->num_rows();    
    }
    public function get_total_unread_chatroom_notifications($cust_id=0, $limit=5)
    {
      $qry = "SELECT * FROM tbl_driver_chats WHERE chat_id NOT IN ( SELECT notification_id FROM tbl_read_chat_notification WHERE cust_id = $cust_id ) AND ( receiver_id = $cust_id )  ORDER BY chat_id DESC LIMIT $limit";
      return $this->db->query($qry)->result_array();    
    }
  //End Constructor Functions--------------------------------

  //Start Driver Chat----------------------------------------
    public function get_order_detail($order_id=0)
    {
      if($order_id > 0 ) {
        $this->db->where('order_id', (int)($order_id));
        return $this->db->get($this->_courier_orders)->row_array();
      } return false;
    }
    public function get_order_chat($order_id=0)
    {
      if( $order_id > 0 ) {     
        $this->db->where('order_id ', $order_id);
        // $this->db->order_by('chat_id', 'desc');
        return $this->db->get($this->_driver_chats)->result_array();
      } return false;
    }
    public function add_chat_to_chatroom(array $data)
    {
      if(!empty($data)){
        $this->db->insert($this->_driver_chats, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_driver_device_details($cd_id=0)
    {
      if($cd_id > 0 ) {
        $this->db->select('reg_id, device_type');
        $this->db->where('cd_id', (int)($cd_id));
        return $this->db->get($this->_driver_device)->result_array();
      } return false;
    }
  //End Driver Chat------------------------------------------
  
  //Start General Modals-------------------------------------
    public function get_courier_details_by_ref_id($cust_id=0, $order_id=0)
    {
      if($order_id > 0 ) {
        $this->db->where('cust_id', (int)($cust_id));
        $this->db->where('ref_no', $order_id);
        return $this->db->get($this->_courier_orders)->row_array();
      } return false;
    }
    public function get_country_id_by_name($name=null)
    {
      if(!is_null($name)){ 
        $this->db->where('country_name', $name); 
        $r = $this->db->get($this->_countries)->row_array(); 
        return $r['country_id']; 
      }        
      return false;
    }
    public function get_state_id_by_name($name=null, $country_id=0)
    {
      if(!is_null($name) && $country_id > 0){ 
        $this->db->where('state_name', $name); 
        $this->db->where('country_id', (int)$country_id); 
        $r = $this->db->get($this->_states)->row_array(); 
        return $r['state_id']; 
      }        
      return false;
    }
    public function get_city_id_by_name($name=null, $state_id=0)
    {
      if(!is_null($name) && $state_id > 0){ 
        $this->db->where('city_name', $name);
        $this->db->where('state_id', (int)$state_id); 
        $r = $this->db->get($this->_cities)->row_array(); 
        return $r['city_id']; 
      }        
      return false;
    }
    public function distance($latitudeFrom=null, $longitudeFrom=null, $latitudeTo=null, $longitudeTo=null)
    {
      //Calculate distance from latitude and longitude
      $theta = $longitudeFrom - $longitudeTo;
      $dist = sin(deg2rad(floatval($latitudeFrom))) * sin(deg2rad(floatval($latitudeTo))) +  cos(deg2rad(floatval($latitudeFrom))) * cos(deg2rad(floatval($latitudeTo))) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      // return $distance = ($miles * 1.609344).' km';
      return $distance = ($miles * 1.609344);
    }
    public function GetDrivingDistance($lat1, $long1, $lat2, $long2)
    {
      $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving";
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $response = curl_exec($ch);
      curl_close($ch);
      $response_a = json_decode($response, true);

      if(isset($response_a['rows'][0]['elements'][0]['distance']) && $response_a['rows'][0]['elements'][0]["status"] != "ZERO_RESULTS"){
        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        $dist = explode('km', $dist);
        return trim($dist[0]);
      } else { return (string)$this->distance($lat1, $long1, $lat2, $long2);  }
    }
    
    
    
    public function requested_order_count($cust_id=0, $last_login_date_time=null)
    {
      if($cust_id > 0 && !is_null($last_login_date_time)) {
        $this->db->select('COUNT(order_id) as requested_order_count');
        $this->db->where('deliverer_id', (int)($cust_id));
        $this->db->where('cre_datetime >', trim($last_login_date_time));
        $row = $this->db->get($this->_courier_order_deliverer_request)->row_array();
        return $row['requested_order_count'];
      }
      return false;
    }
    public function convert_big_int($int = 0)
    {
      if( $int > 1000 && $int < 1000000){ $int = ($int/1000); return number_format($int ,1,'.',',') . 'K'; }         
      else if( $int >= 1000000 ){ $int = ($int / 1000000 ); return number_format($int ,1,'.',',') . 'M'; }
      else{ return $int; }
    }
    public function get_provider_review_details($cust_id=0)
    {
      if($cust_id > 0 ) {
        $this->db->select('ratings, total_ratings, no_of_ratings');
        $this->db->where('cust_id', (int)($cust_id));
        return $this->db->get($this->_customers_master)->row_array();
      } return false;
    }
  //End General Modals---------------------------------------

  //Start Create Booking Laundry-----------------------------
    public function add_cloth_coun_temp(array $insert_data)
    {
      if(is_array($insert_data)){
        $this->db->insert($this->_laundry_cloth_temp, $insert_data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_cloth_details($cust_id=0)
    {
      if($cust_id > 0){
        $this->db->where('cust_id',$cust_id);
        $this->db->order_by('cat_id', 'asc');
        return $this->db->get($this->_laundry_cloth_temp)->result_array();
      } return false;
    }
    public function get_cloth_details_by_session_id($session_id=null)
    {
      if($session_id != null){
        $this->db->where('session_id',$session_id);
        $this->db->order_by('cat_id', 'asc');
        return $this->db->get($this->_laundry_cloth_temp)->result_array();
      } return false;
    }
    public function get_cloth_count_by_cat_id($cust_id=0, $cat_id=0)
    {
      if($cust_id > 0 && $cust_id > 0){
        $this->db->select('SUM(count) AS cat_count');
        $this->db->where('cust_id', $cust_id);
        $this->db->where('cat_id', $cat_id);
        return $this->db->get($this->_laundry_cloth_temp)->result_array();
      } return false;
    }
    public function get_cloth_details_by_sub_cat_id_user_id($cust_id=0, $sub_cat_id=0)
    {
      if($cust_id > 0 && $sub_cat_id > 0){
        $this->db->where('cust_id', $cust_id);
        $this->db->where('sub_cat_id', $sub_cat_id);
        return $this->db->get($this->_laundry_cloth_temp)->row_array();
      } return false;
    }
    public function get_cloth_details_volum($cust_id=0)
    {
      if($cust_id > 0){ 
        $this->db->where('cust_id',$cust_id);
        $this->db->select_sum('height');
        $this->db->select_sum('weight');
        $this->db->select_max('width');
        $this->db->select_max('length');
        return $this->db->get($this->_laundry_cloth_temp)->row_array();
      } return false;
    }
    public function check_cloth_coun_temp(array $insert_data)
    {
      if(is_array($insert_data)){
        $this->db->where('cust_id', $insert_data['cust_id']);
        $this->db->where('cat_id', $insert_data['cat_id']);
        $this->db->where('sub_cat_id', $insert_data['sub_cat_id']);
        if($res = $this->db->get($this->_laundry_cloth_temp)->row_array()){return $res;}
        return false;
      } return false;
    }
    public function check_cloth_coun_temp_by_session_id(array $insert_data)
    {
      if(is_array($insert_data)){
        $this->db->where('session_id', $insert_data['session_id']);
        $this->db->where('cat_id', $insert_data['cat_id']);
        $this->db->where('sub_cat_id', $insert_data['sub_cat_id']);
        return $this->db->get($this->_laundry_cloth_temp)->row_array();
      } return false;
    }
    public function update_cloth_coun_temp(array $insert_data,$delete=null)
    {
      //echo json_encode($insert_data); die();
      if(is_array($insert_data)){
        $this->db->where('cust_id', $insert_data['cust_id']);
        $this->db->where('cat_id', $insert_data['cat_id']);
        $this->db->where('sub_cat_id', $insert_data['sub_cat_id']);
        if($delete==1){
          $this->db->delete($this->_laundry_cloth_temp);
        }else{ 
          $this->db->update($this->_laundry_cloth_temp, ['count' => trim($insert_data['count']) , 'height' => $insert_data['height'] , 'weight' => $insert_data['weight'] ]);
        }
      } return false;
    }
    public function update_cloth_coun_temp_by_session_id(array $insert_data, $delete=null)
    {
      //echo json_encode($insert_data); die();
      if(is_array($insert_data)){
        $this->db->where('session_id', $insert_data['session_id']);
        $this->db->where('cat_id', $insert_data['cat_id']);
        $this->db->where('sub_cat_id', $insert_data['sub_cat_id']);
        if($delete==1){
          $this->db->delete($this->_laundry_cloth_temp);
        } else { 
          $this->db->update($this->_laundry_cloth_temp, ['count' => trim($insert_data['count']) , 'height' => $insert_data['height'] , 'weight' => $insert_data['weight'] ]);
        }
      } return false;
    }
    public function update_user_id_by_session_id($session_id=null, $cust_id=0)
    {
      //echo json_encode($insert_data); die();
      if(!is_null($session_id) && $cust_id > 0){
        $this->db->where('session_id', $session_id);
        $this->db->update($this->_laundry_cloth_temp, ['cust_id' => trim($cust_id)]);
      } return false;
    }
    public function delete_cloth_coun_temp($cust_id)
    {
      $this->db->where('cust_id', $cust_id);
      $this->db->delete($this->_laundry_cloth_temp);
    }
    public function delete_cloth_coun_temp_by_session_id($session_id)
    {
      $this->db->where('session_id', $session_id);
      $this->db->delete($this->_laundry_cloth_temp);
    }
    public function get_laundry_provider_profile_list()
    {
      return $this->db->get($this->_laundry_provider_profile)->result_array();
    }
    public function get_laundry_charges_list_apend($cust_id=0, $sub_cat_id=0, $country_id=0)
    {
      if($cust_id > 0 ){
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('sub_cat_id', (int) $sub_cat_id);
        $this->db->where('country_id', (int) $country_id);
        $this->db->order_by('cat_id','asec'); 
        return $this->db->get($this->_laundry_charges)->row_array();
      } return false;
    }
    public function create_laundry_booking(array $data)
    {
      if(is_array($data)){
        $this->db->insert($this->_laundry_booking, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function create_laundry_booking_details(array $data)
    {
      if(is_array($data)){
        $this->db->insert($this->_laundry_booking_details, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_laundry_advance_payment($country_id=0)
    {
      if($country_id > 0){
        $this->db->where('country_id', (int) $country_id);
        return $this->db->get($this->_laundry_advance_payment)->row_array();
      } return false;
    }
    public function add_laundry_favourite_providers(array $data)
    {
      if(!empty($data)){
        $this->db->insert($this->_laundry_favourite_providers, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function add_bus_chat_to_workroom(array $data)
    {
      if(!empty($data)){
        $this->db->insert($this->_courier_workroom, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function create_global_workroom(array $data)
    {
      if(!empty($data)){
        $this->db->insert($this->_global_workroom, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_dimension_id_for_laundry($volume = 0)
    {
      if($volume > 0 ){
        $sql = "SELECT * FROM tbl_standard_dimension WHERE category_id = 7 AND on_earth = 1 AND volume > ".$volume." order by cast(volume as UNSIGNED) limit 1";
        return $this->db->query($sql)->row_array();
      } else false;
    }
    public function get_laundry_cherge($insert_data=null)
    {
      if(is_array($insert_data)){
        $this->db->where('cat_id', $insert_data['cat_id']);
        $this->db->where('sub_cat_id', $insert_data['sub_cat_id']);
        $this->db->where('country_id', $insert_data['country_id']);
        if(isset($insert_data['cust_id']) && $insert_data['cust_id'] > 0) { $this->db->where('cust_id', $insert_data['cust_id']); }
        return $this->db->get($this->_laundry_charges)->row_array();
      } else{ return false; }
    }
    public function get_laundry_sub_category($sub_cat_id=0)
    {
      if($sub_cat_id>0){
        $this->db->where('sub_cat_id',$sub_cat_id);
        return $this->db->get($this->_laundry_sub_category)->row_array();
      } return false;
    }
    public function get_laundry_category($cat_id=0)
    {
      if($cat_id>0){
        $this->db->where('cat_id',$cat_id);
        return $this->db->get($this->_laundry_category)->row_array();
      } return false;
    }
    
    public function get_sub_category_details($sub_cat_id=0)
    {
      if($sub_cat_id > 0){
        $this->db->where('sub_cat_id', (int) $sub_cat_id);
        return $this->db->get($this->_laundry_sub_category)->row_array();
      } return false;
    }
  //End Create Booking Laundry-------------------------------

  //Create booking Laundry By Provider-----------------------
    public function register_walkin_customer_profile(array $data)
    {
      if(is_array($data)) {
        $this->db->insert($this->_walkin_customer, $data);
        return $this->db->insert_id();
      } return false;
    }
    public function get_walkin_customer_profile($walkin_cust_id=0)
    {
      if($walkin_cust_id > 0 ) {
        $this->db->where('walkin_id', (int) $walkin_cust_id);
        return $this->db->get($this->_walkin_customer)->row_array();
      } return false;
    }
    public function get_walkin_customer_list($provider_id=0)
    {
      if($provider_id > 0 ) {
        $this->db->where('provider_id', (int) $provider_id);
        return $this->db->get($this->_walkin_customer)->result_array();
      } return false;
    }
    public function update_walkin_customer(array $data, $walkin_id=0)
    {
      if(is_array($data) && $walkin_id > 0) {
        $this->db->where('walkin_id', (int) $walkin_id);
        $this->db->update($this->_walkin_customer, $data);
      } return false;
    }
    public function check_walkin_customer_profile(array $data)
    {
      if(is_array($data)) {
        $this->db->where('phone', $data['phone']);
        $this->db->where('country_id', (int)$data['country_id']);
        return $this->db->get($this->_walkin_customer)->result_array();
      } return false;
    }
    public function get_cloth_details_volum_by_booking_id($booking_id=0)
    {
      if($booking_id > 0){ 
        $this->db->where('booking_id',$booking_id);
        $this->db->select_sum('height');
        $this->db->select_sum('weight');
        $this->db->select_max('width');
        $this->db->select_max('length');
        return $this->db->get($this->_laundry_booking_details)->row_array();
      } return false;
    }
  //Create booking Laundry By Provider-----------------------
  
  //Start Laundry claim--------------------------------------
    public function get_claim_types()
    {
      return $this->db->get($this->_service_claim_types)->result_array();
    }
    public function get_claim_list(array $data)
    {
      if(is_array($data)) {
        if($data['claim_id'] > 0) { //claim details
          $this->db->where('claim_id', $data['claim_id']);
          return $this->db->get($this->_service_claims)->row_array();
        } else if($data['service_id'] > 0) { //claim details
          $this->db->where('service_id', $data['service_id']);
          return $this->db->get($this->_service_claims)->row_array();
        } else { //claim list
          if($data['user_type'] == 'customer') { $this->db->where('cust_id', $data['cust_id']);
          } else { $this->db->where('provider_id', $data['cust_id']); }

          $this->db->where('service_cat_id', $data['service_cat_id']);

          if($data['claim_status'] == 'open') { $this->db->where('claim_status !=', 'closed');
          } else { $this->db->where('claim_status', 'closed'); } 

          if($data['device_type'] == 1) {  // mobile pagination
            if($data['last_id'] > 0) {
              $this->db->where('claim_id <', $data['last_id']); 
            }
            $this->db->limit(10);
          }
          
          $this->db->order_by('claim_id', 'desc');
          
          return $this->db->get($this->_service_claims)->result_array();
        }
      }
      return false;
    }
    public function update_service_claim_details(array $data, $claim_id = 0)
    {
      if(is_array($data) && $claim_id > 0){
        $this->db->where('claim_id', (int)$claim_id);
        $this->db->update($this->_service_claims, $data);
        return $this->db->affected_rows();
      } return false;
    }
    public function register_claim($data=null)
    {
      if(is_array($data)){
        $this->db->insert($this->_service_claims, $data);
        return $this->db->insert_id();
      } return false;
    }
  //End Laundry claim----------------------------------------

  //Start Laundry Service Cleanup----------------------------
    public function laundry_order_cleanup($id=0)
    {
      $this->db->empty_table('tbl_laundry_booking');
      $this->db->empty_table('tbl_laundry_booking_details');
      $this->db->empty_table('tbl_laundry_cancelled_bookings');
      $this->db->empty_table('tbl_laundry_count_temp');
      $this->db->empty_table('tbl_laundry_favourite_providers');
      $this->db->empty_table('tbl_laundry_ratings');
      $this->db->empty_table('tbl_laundry_review');
      return true;
    }

    public function laundry_order_cleanup2($id=0)
    {
      $this->db->empty_table('tbl_laundry_booking');
      $this->db->empty_table('tbl_laundry_booking_details');
      $this->db->empty_table('tbl_laundry_cancelled_bookings');
      $this->db->empty_table('tbl_laundry_count_temp');
      $this->db->empty_table('tbl_laundry_favourite_providers');
      $this->db->empty_table('tbl_laundry_ratings');
      $this->db->empty_table('tbl_laundry_review','tbl_laundry_charges','tbl_laundry_provider_cancellation_charges','tbl_laundry_advance_payment','tbl_laundry_favourite_providers','tbl_laundry_booking','tbl_laundry_booking_details','tbl_laundry_ratings','tbl_laundry_review','tbl_laundry_count_temp','tbl_walkin_customer','tbl_service_claims','tbl_service_claim_types','tbl_promo_code','tbl_promocode_used','tbl_laundry_special_charges');

      return true;
    } 

  //End Laundry Service Cleanup------------------------------

  
  //Start laundry chareges-----------------------------------
    public function get_laundry_special_charges_list($cust_id=0, $last_id='NULL')
    {
      if($cust_id > 0) {
        $this->db->where('cust_id', $cust_id);
        if($last_id != 'NULL') {
          $this->db->where('charge_id >', (int)$last_id);
          $this->db->limit(10);
          $this->db->order_by("charge_id", "asc");
        }
        return $this->db->get($this->_laundry_special_charges)->result_array();
      } return false;
    }

    public function get_laundry_special_charges($charge_id = 0)
    {
      if($charge_id > 0) {
        $this->db->where('charge_id', $charge_id);
        return $this->db->get($this->_laundry_special_charges)->row_array();
      } return false;
    }

    public function add_laundry_special_charges(array $data)
    {
      if(is_array($data) ) {
        $this->db->insert($this->_laundry_special_charges, $data);
        return $this->db->insert_id();
      } return false;
    }

    public function update_laundry_special_charges(array $update_data, $charge_id=0)
    {
      if(is_array($update_data) && $charge_id > 0 ){
        $this->db->where('charge_id', (int) $charge_id);
        $this->db->update($this->_laundry_special_charges, $update_data);
        return $this->db->affected_rows();
      } return false;
    }
    
    public function delete_laundry_special_charges($charge_id=0)
    {
      if($charge_id > 0 ){
        $this->db->where('charge_id', (int) $charge_id);
        $this->db->delete($this->_laundry_special_charges);
        return $this->db->affected_rows();
      }
      return false;
    }
  //End laundry chareges-------------------------------------

  //zaid
  //Start Freelancer Question--------------------------------
    public function get_questions_know_freelancer($cat_type_id = 0)
    {
      if($cat_type_id > 0) {
        $this->db->where('cat_type_id', $cat_type_id);
        return $this->db->get($this->_smp_questions_know_freelancer_master)->result_array();
      } return false;
    }
  //End Freelancer Question----------------------------------
    public function check_workstream_master($insert_data=null)
    {
      if(is_array($insert_data)) {
        $this->db->where('cust_id', $insert_data['cust_id']);
        $this->db->where('job_id', $insert_data['job_id']);
        $this->db->where('provider_id', $insert_data['provider_id']);
        if($res = $this->db->get($this->_smp_workstream_master)->row_array()) { return $res['workstream_id'];
        } return false;
      } return false;
    }
    public function check_workstream_master_admin($insert_data=null)
    {
      if(is_array($insert_data)) {
        $this->db->where('cust_id', $insert_data['cust_id']);
        $this->db->where('job_id', $insert_data['job_id']);
        $this->db->where('provider_id', $insert_data['provider_id']);
        $this->db->where('is_admin', $insert_data['is_admin']);
        if($res = $this->db->get($this->_smp_workstream_master)->row_array()) { return $res['workstream_id'];
        } return false;
      } return false;
    }

    public function get_customer_job_details($job_id=0)
    {
      // echo json_encode($job_id);die();
      if($job_id > 0) {
        $this->db->select('c.cat_name, ct.cat_type, country.country_name, cust.firstname AS pro_fname, cust.lastname AS pro_lname, job.*');
        $this->db->from($this->_category_master.' as c');
        $this->db->from($this->_category_type_master.' as ct');
        $this->db->from($this->_countries.' as country');
        $this->db->from($this->_customers_master.' as cust');
        $this->db->join($this->_smp_jobs.' as job', 'job.sub_cat_id = c.cat_id AND job.cat_id = ct.cat_type_id AND job.cust_id = cust.cust_id  AND job.job_id = ' . (int)($job_id));
        $this->db->group_by('job.job_id');
        return $this->db->get($this->_smp_jobs)->row_array();
      } return false;
    }

    public function list_of_freelancers_aa(array $data)
    {
      if(is_array($data)){
        $this->db->select('fp.*, skill.confirmed_skills as skills , skill.confirmed_skills_name as skills_name , country.country_name as country_name , city.city_name as city_name');
        $this->db->from($this->_skilled.' as skill');
        $this->db->from($this->_countries.' as country');
        $this->db->from($this->_cities.' as city');
        $this->db->from($this->_smp_jobs.' as job');
        if($data['fav'] == "no"){
          $this->db->join($this->_smp_freelancer_profile.' as fp','fp.cust_id != '.$data['cust_id'].' AND fp.cust_id = skill.cust_id AND fp.country_id = country.country_id AND fp.city_id = city.city_id ');
          $this->db->where('FIND_IN_SET("'.$data['skill'].'", skill.confirmed_skills_name)');
        }
        if($data['fav'] == "yes"){
          $this->db->from($this->_smp_favorite_providers.' as fav');
          $this->db->join($this->_smp_freelancer_profile.' as fp', 'fp.cust_id = fav.provider_id AND fp.country_id = country.country_id AND fp.city_id = city.city_id ');
        }
        if($data['country_id'] != ""){$this->db->where('fp.country_id' , $data['country_id']);}
        if($data['state_id'] != ""){$this->db->where('fp.state_id' , $data['state_id']);}
        if($data['city_id'] != ""){$this->db->where('fp.city_id' , $data['city_id']);}
        if($data['rate_from'] != ""){$this->db->where('fp.rate_per_hour >=' , $data['rate_from']);}
        if($data['rate_to'] != ""){$this->db->where('fp.rate_per_hour <=' , $data['rate_to']);}
        $this->db->group_by('fp.cust_id');
        return $this->db->get($this->_smp_freelancer_profile)->result_array();
      } return false;
    }

    public function list_of_freelancers(array $data)
    {
      if(is_array($data)){
        $this->db->select('fp.*, skill.confirmed_skills as skills , skill.confirmed_skills_name as skills_name , country.country_name as country_name , city.city_name as city_name');
        $this->db->from($this->_skilled.' as skill');
        $this->db->from($this->_countries.' as country');
        $this->db->from($this->_cities.' as city');
        $this->db->from($this->_smp_jobs.' as job');
        if($data['fav'] == "no"){
          $this->db->join($this->_smp_freelancer_profile.' as fp','fp.cust_id != '.$data['cust_id'].' AND fp.cust_id = skill.cust_id AND fp.country_id = country.country_id AND fp.city_id = city.city_id ');
          $this->db->where('FIND_IN_SET("'.$data['skill'].'", skill.confirmed_skills_name)');
        }
        if($data['fav'] == "yes"){
          $this->db->from($this->_smp_favorite_providers.' as fav');
          $this->db->join($this->_smp_freelancer_profile.' as fp', 'fp.cust_id = fav.provider_id AND fp.country_id = country.country_id AND fp.city_id = city.city_id ');
        }
        if($data['country_id'] != "NULL"){$this->db->where('fp.country_id' , $data['country_id']);}
        if($data['state_id'] != "NULL"){$this->db->where('fp.state_id' , $data['state_id']);}
        if($data['city_id'] != "NULL"){$this->db->where('fp.city_id' , $data['city_id']);}
        if($data['rate_from'] != "NULL"){$this->db->where('fp.rate_per_hour >=' , $data['rate_from']);}
        if($data['rate_to'] != "NULL"){$this->db->where('fp.rate_per_hour <=' , $data['rate_to']);}
        $this->db->group_by('fp.cust_id');
        return $this->db->get($this->_smp_freelancer_profile)->result_array();
      } return false;
    }


    public function get_favorite_freelancer($cust_id = 0)
    {
      if($cust_id > 0) {
        $this->db->where('cust_id', $cust_id);
        return $this->db->get($this->_smp_favorite_providers)->result_array();
      } return false;
    }

    public function check_favourite_freelancer($cust_id=0, $provider_id=0)
    {
      $this->db->where('cust_id', $cust_id);
      $this->db->where('provider_id', $provider_id);
      if($res = $this->db->get($this->_smp_favorite_providers)->row_array())
      {
        return $res;
      }else{
        return false;
      }
    }

    public function register_new_favourite_freelancer($cust_id=0, $provider_id=0)
    {
      if($cust_id > 0 AND $provider_id > 0) {
        $insert_data = array(
          "cust_id" => (int) $cust_id,
          "provider_id" => (int) $provider_id,
          "cre_datetime" => date('Y-m-d H:i:s'),
        );
        return $this->db->insert($this->_smp_favorite_providers, $insert_data);
      } return false;
    }

    public function remove_favourite_freelancer($cust_id=0, $provider_id=0)
    {
      if($cust_id > 0 AND $provider_id > 0) {
        
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('provider_id', (int) $provider_id);
        $this->db->delete($this->_smp_favorite_providers);
        return $this->db->affected_rows();
      } return false;
    }

    public function check_customer_invite_job($insert_data=null)
    {
      if(is_array($insert_data)) {
        $this->db->where('cust_id', $insert_data['cust_id']);
        $this->db->where('job_id', $insert_data['job_id']);
        $this->db->where('provider_id', $insert_data['provider_id']);
        if($this->db->get($this->_smp_jobs_invite)->row()) { return true;
        } return false;
      } return false;
    }

    public function register_customer_invite_job(array $data)
    {
      if(is_array($data)) {
        $this->db->insert($this->_smp_jobs_invite, $data);
        return $this->db->insert_id();
      } return false;
    }

    public function get_job_proposals_list($job_id = 0)
    {
      if($job_id > 0){
        $this->db->where('job_id', $job_id); 
        return $this->db->get($this->_smp_job_proposals)->result_array();
      }return false;
    }

    public function list_of_freelancers_invited_proposals(array $data)
    {
      if(is_array($data)){
        $this->db->select('fp.* , country.country_name as country_name , city.city_name as city_name');
        $this->db->from($this->_smp_jobs.' as job');
        $this->db->from($this->_countries.' as country');
        $this->db->from($this->_cities.' as city');
        $this->db->from($this->_smp_workstream_master.' as work');
        $this->db->from($this->_customers_master.' as customer');
        $this->db->join($this->_smp_freelancer_profile.' as fp', 'fp.cust_id = work.provider_id AND job.job_id = work.job_id AND fp.cust_id = customer.cust_id AND job.job_id = '.$data['job_id']." AND fp.country_id = country.country_id AND fp.city_id = city.city_id");    
        $this->db->group_by('fp.cust_id');
        if($data['country_id'] != "0"){$this->db->where('fp.country_id', $data['country_id']);}
        if($data['ratings'] != "0"){$this->db->where('customer.ratings >=', $data['ratings']);}
        return $this->db->get($this->_smp_freelancer_profile)->result_array();
      } return false;
    }

    public function job_decline_reason_master()
    {
      return $this->db->get($this->_smp_job_decline_reason_master)->result_array();
    }

    public function check_job_proposals($insert_data=null)
    {
      if(is_array($insert_data)) {
        $this->db->where('cust_id', $insert_data['cust_id']);
        $this->db->where('job_id', $insert_data['job_id']);
        $this->db->where('provider_id', $insert_data['provider_id']);
        //$this->db->where('proposal_status !=', 'reject');
        //$this->db->where('proposal_status !=', 'cancelled');
        if($this->db->get($this->_smp_job_proposals)->row()) { return true;
        } return false;
      } return false;
    }

    public function get_job_proposals(array $data)
    {
      if(is_array($data)){
        $this->db->where('job_id', $data['job_id']);
        $this->db->where('provider_id', $data['provider_id']);
        if($data['decline'] == "yes"){$this->db->where('proposal_status', "reject");} 
        return $this->db->get($this->_smp_job_proposals)->row_array();
      }return false;
    }


    // Change by Maaz
    public function get_job_list_old(array $data)
    {
      if(is_array($data)){
        $this->db->where('cust_id !=', $data['cust_id']);
        $this->db->where('job_status', 'open');
        $this->db->where('visibility', 'public');
        $this->db->where('open_for_proposal', '1');
        $this->db->where('job_type', '0');
        return $this->db->get($this->_smp_jobs)->result_array();
      }return false;
    }

    public function get_favorite_jobs($provider_id = 0)
    {
      if($provider_id > 0) {
        $this->db->where('provider_id', $provider_id);
        return $this->db->get($this->_smp_favorite_jobs)->result_array();
      } return false;
    }

    public function get_my_favorite_jobs($provider_id = 0)
    {
      if($provider_id > 0) {
        $this->db->select('job.* , cat.cat_name , cat.icon_url');
        $this->db->from($this->_smp_favorite_jobs.' as fav');
        $this->db->from($this->_category_master.' as cat');
        $this->db->join($this->_smp_jobs.' as job', 'job.job_id = fav.job_id AND job.sub_cat_id = cat.cat_id AND fav.provider_id = '.$provider_id."");
        $this->db->group_by('job.job_id');
        return $this->db->get($this->_smp_jobs)->result_array();
      } return false;
    }

    // $this->db->select("tp.* , (SELECT cat_name FROM `tbl_category_master` WHERE cat_id=tp.sub_cat_id) as cat_name , (SELECT icon_url FROM `tbl_category_master` WHERE cat_id=tp.sub_cat_id) as icon_url");

    public function register_new_favourite_jobs($job_id = 0, $cust_id=0, $provider_id=0)
    {
      if($cust_id > 0 AND $provider_id > 0 AND $job_id > 0) {
        $insert_data = array(
          "job_id" => (int) $job_id,
          "cust_id" => (int) $cust_id,
          "provider_id" => (int) $provider_id,
          "cre_datetime" => date('Y-m-d H:i:s'),
        );
        return $this->db->insert($this->_smp_favorite_jobs, $insert_data);
      } return false;
    }

    public function remove_favourite_job($job_id = 0, $cust_id=0, $provider_id=0)
    {
      if($cust_id > 0 AND $provider_id > 0) {   
        $this->db->where('job_id', (int) $job_id);
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->where('provider_id', (int) $provider_id);
        $this->db->delete($this->_smp_favorite_jobs);
        return $this->db->affected_rows();
      } return false;
    }

    public function get_last_job_of_customer($cust_id = 0 , $type=null)
    {
      if($cust_id > 0) {
        $this->db->where('cust_id', $cust_id);
        if($type == 'last'){
          $this->db->order_by('job_id', 'desc');
          return $this->db->get($this->_smp_jobs)->row_array();
        }
        if($type == 'completed_count'){
          $this->db->where('job_status','completed');
          $s = $this->db->get($this->_smp_jobs)->result_array();
          return sizeof($s);
        } 
      } return false;
    }

    public function get_job_advance_payment_percent($country_id = 0 , $cat_type_id = 0 , $cat_id = 0)
    {
      if($cat_id > 0 && $country_id > 0 &&  $cat_type_id > 0) {
        // echo json_encode($cat_id);die();
        $this->db->where('country_id', $country_id);
        $this->db->where('cat_type_id', $cat_type_id);
        $this->db->where('cat_id', $cat_id);
        return $this->db->get($this->_smp_advance_payment)->row_array();
      } return false;
    }

    public function get_country_id_by_currency_id($currency_id = 0)
    {
      if($currency_id > 0 ) {
        $this->db->where('currency_id', $currency_id);
        return $this->db->get($this->_country_currency)->row_array();
      } return false;
    }

    public function register_job_proposal(array $data) {
      if(is_array($data)) {
        $this->db->insert($this->_smp_job_proposals, $data);
        return $this->db->insert_id();
      } return false;
    }

    public function list_of_job_proposals_sends($provider_id = 0)
    {
      if($provider_id > 0){
        $this->db->select('j.*,c.cat_name,c.icon_url, ct.cat_type,proposal.proposal_status');
        $this->db->from($this->_smp_job_proposals.' as proposal');
        // $this->db->from($this->_smp_freelancer_profile.' as fp');
        $this->db->from($this->_smp_workstream_master.' as work');
        $this->db->from($this->_customers_master.' as customer');
        $this->db->from($this->_category_master.' as c');
        $this->db->from($this->_category_type_master.' as ct');
        $this->db->join($this->_smp_jobs.' as j', 'j.job_id = work.job_id AND j.sub_cat_id = c.cat_id AND j.cat_id = ct.cat_type_id AND j.job_id = proposal.job_id AND proposal.provider_id = '.$provider_id."");    
        $this->db->group_by('j.job_id');
        $this->db->where('j.provider_id ', 0);
        $this->db->where('proposal.proposal_status', "open");
        $this->db->or_where('proposal.proposal_status', "reject");
        return $this->db->get($this->_smp_jobs)->result_array();
      } return false;
    }

    public function list_of_job_invitation($provider_id = 0)
    {
      if($provider_id > 0){
        $this->db->select('j.*,c.cat_name,c.icon_url, ct.cat_type');
        $this->db->from($this->_smp_jobs_invite.' as invite');
        // $this->db->from($this->_smp_freelancer_profile.' as fp');
        $this->db->from($this->_smp_workstream_master.' as work');
        $this->db->from($this->_customers_master.' as customer');
        $this->db->from($this->_category_master.' as c');
        $this->db->from($this->_category_type_master.' as ct');
        $this->db->join($this->_smp_jobs.' as j', 'j.job_id = work.job_id AND j.sub_cat_id = c.cat_id AND j.cat_id = ct.cat_type_id AND j.job_id = invite.job_id AND invite.provider_id = '.$provider_id."");    
        $this->db->where('invite.invite_status', "open");
        $this->db->where('j.provider_id !=', $provider_id);
        $this->db->group_by('j.job_id');
        $this->db->order_by("j.job_id" , "desc");
        return $this->db->get($this->_smp_jobs)->result_array();
      } return false;
    }

    public function update_job_proposals(array $data , $job_id=0 , $provider_id=0) {
      if(is_array($data) && $job_id > 0 && $provider_id > 0 ){
        $this->db->where('job_id', $job_id);
        $this->db->where('provider_id', $provider_id);
        $this->db->update($this->_smp_job_proposals, $data);
        return $this->db->affected_rows();
      } return false;
    }

    public function get_job_proposals_by_proposal_id($proposal_id = 0)
    {
      if($proposal_id > 0){
        $this->db->where('proposal_id', $proposal_id); 
        return $this->db->get($this->_smp_job_proposals)->row_array();
      }return false;
    }

    public function get_job_proposals_list_who_notify($job_id = 0 , $provider_id = 0)
    {
      if($job_id > 0 && $provider_id > 0 ){
        $this->db->where('job_id', $job_id); 
        $this->db->where('notify_proposal_closed', 1); 
        $this->db->where('proposal_status', 'open'); 
        $this->db->where('provider_id !=', $provider_id); 
        return $this->db->get($this->_smp_job_proposals)->result_array();
      }return false;
    }

    public function close_job_proposals_except_accepted($job_id = 0 , $provider_id = 0)
    {
      if($job_id > 0 && $provider_id > 0 ){
        $this->db->where('job_id', $job_id); 
        $this->db->where('provider_id !=', $provider_id);  
        $this->db->where('proposal_status', 'open');
        $this->db->update($this->_smp_job_proposals, ['proposal_status' => 'closed']);
        return $this->db->affected_rows();
      }return false;
    }

    public function get_proposal_milestone_by_proposal_id($proposal_id = 0)
    {
      // echo json_encode($proposal_id);die();
      if($proposal_id > 0){
        $this->db->where('proposal_id', $proposal_id);
        $this->db->where('amount_paid', '0'); 
        return $this->db->get($this->_smp_proposal_milestone_details)->row_array();
      }return false;
    }

    public function get_proposal_milestone_by_milestone_id($milestone_id = 0)
    {
      // echo json_encode($proposal_id);die();
      if($milestone_id > 0){
        $this->db->where('milestone_id', $milestone_id);
        // $this->db->where('amount_paid', '0'); 
        return $this->db->get($this->_smp_proposal_milestone_details)->row_array();
      }return false;
    }
    
    public function register_milestone_payment_request(array $data)
    {
      if(is_array($data)) {
        $this->db->insert($this->_smp_milestone_payment_request, $data);
        return $this->db->insert_id();
      } return false;
    }

    public function get_milestone_payment_request($job_id = 0)
    {
      if($job_id > 0){
        $this->db->where('job_id', $job_id);
        $this->db->where('status', 'open'); 
        return $this->db->get($this->_smp_milestone_payment_request)->row_array();
      }return false;
    }





    //------- Maaz Ali G4 25 May --------------

    public function get_troubleshoot_list_old(array $data)
    {
      if(is_array($data)){
        $this->db->where('cust_id !=', $data['cust_id']);
        $this->db->where('job_status', 'open');
        $this->db->where('visibility', 'public');
        $this->db->where('open_for_proposal', '1');
        $this->db->where('job_type', '1');
        $this->db->where("Job_id NOT IN (SELECT job_id FROM `tbl_smp_job_proposals` WHERE provider_id=".$data['cust_id']." AND job_id=tp.job_id)");
        return $this->db->get($this->_smp_jobs." tp")->result_array();
      }return false;
    }

    public function get_troubleshoot_ques_ans($job_id)
    {
      $this->db->where("job_id" , $job_id);
      return $this->db->get("tbl_question_answer")->result_array();
    }

    public function get_job_payment_paid_history($job_id = 0)
    {
      if($job_id > 0){
        $this->db->where('job_id', $job_id);
        return $this->db->get($this->_smp_job_payment_history)->row_array();
      }return false;
    }

    public function register_job_payment_paid_history(array $data)
    {
      if(is_array($data)) {
        $this->db->insert($this->_smp_job_payment_history, $data);
        return $this->db->insert_id();
      } return false;
    }

    public function update_job_payment_paid_history($job_id = 0 , $amount = 0 , $datetime = "NULL")
    {
      if($job_id > 0 && $amount > 0 ){
        $this->db->where('job_id', $job_id);    
        $this->db->update($this->_smp_job_payment_history, ['paid_amount' => $amount , 'payment_datetime' => $datetime]);
        return $this->db->affected_rows();
      }return false;
    }

    public function update_proposal_milestone_request(array $data, $milestone_id =0)
    {
      if($milestone_id > 0 ){
        $this->db->where("milestone_id" , $milestone_id);
        $this->db->update($this->_smp_milestone_payment_request, $data);
        return $this->db->affected_rows();
      }return false;
    }

    public function update_proposal_milestone_details(array $data,$milestone_id)
    {
      if($milestone_id >0){
        $this->db->where("milestone_id" , $milestone_id);
        $this->db->update($this->_smp_proposal_milestone_details, $data);
        return $this->db->affected_rows();
      }return false;
    }

  //sgmsdfi
    
    public function insert_proposal_milestone_details(array $data)
    {
      if(is_array($data)) {
        $this->db->insert($this->_smp_proposal_milestone_details, $data);
        return $this->db->insert_id();
      } 
    }


    public function get_category_type()
  {
    $this->db->where('cat_type_status', 1);
    $this->db->where('cat_type_id', 3);
    return $this->db->get($this->_category_type_master)->result_array();
  }

  public function get_promocodes($service_id = 0)
    {
      //if($service_id != 0){$this->db->where('service_id',$service_id);}
      if($service_id != 0){$this->db->where("FIND_IN_SET('$service_id',service_id) !=", 0);}
      $this->db->where('is_delete', 0);
      $this->db->where('code_status', 1);
      $this->db->where('code_expire_date >=' , date('m-d-Y'));
      return $this->db->get($this->_promo_code)->result_array();
    }

    public function is_emailId_exists($emailId=null, $all=false)
    {
      if($emailId != null) {
        if(!$all){
          $this->db->select('cust_id, firstname, lastname, email1, mobile1, password, cust_dob, gender, city_id, state_id, country_id, avatar_url, cover_url,  email_verified, mobile_verified, cre_datetime, last_login_datetime, profession, overview,  cust_status, social_login, social_type, user_type, lang_known, is_deliverer, ratings, total_ratings, no_of_ratings, cust_otp, acc_type, is_dedicated, allow_payment_delay, commission_type');
        }
        $this->db->where('email1', trim($emailId));
        return $this->db->get($this->_customers_master)->row_array();
      }
      return false;
    }

  public function register_user_login_history(array $data)
  {
    if(is_array($data)) {
      $this->db->insert($this->_cust_login_stats, $data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function register_consumer_old(array $data)
  { //echo json_encode($data); die();
    if(is_array($data)){
      $today = date("Y-m-d H:i:s");
      $isnert_data = array(
        "firstname" => trim($data['firstname']), 
        "lastname" => trim($data['lastname']), 
        "email1" => trim($data['email_id']),   
        "password" => trim($data['password']),    
        "mobile1" => trim($data['mobile_no']),
        "gender" => trim($data['gender']),  
        "city_id" => (int) $data['city_id'],  
        "state_id" => (int) $data['state_id'], 
        "country_id" => (int) $data['country_id'],
        "user_type" => (int) $data['usertype'], 
        "avatar_url" => trim($data['avatar_url']), 
        "cover_url" => trim($data['cover_url']), 
        "social_login" => (int) $data['social_login'], 
        "activation_hash" => $data['activation_hash'], 
        "cust_otp" => $data['otp'], 
        "email_verified" => (int) $data['email_verified'], 
        "company_name" => $data['company_name'],            
        "acc_type" => $data['acc_type'],            
        "cre_datetime" => $today, 
        "last_login_datetime" => $today, 
        "cust_status" => 1, 
        "overview" => "NULL", 
        "social_type" => $data['social_type'], 
        "lang_known" => "NULL", 
        "is_deliverer" => $data['is_deliverer'],            
      );
    //echo json_encode($isnert_data); die();
      $this->db->insert($this->_customers_master, $isnert_data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function register_nulled_skilled_old($cust_id=0)
  {
    if($cust_id > 0 ){
      $nulled_data = array(
        "cust_id" => (int) $cust_id,
        "junior_skills" => "NULL",
        "confirmed_skills" => "NULL",
        "senior_skills" => "NULL",
        "expert_skills" => "NULL",
        "ext1" => "NULL",
        "ext2" => "NULL",
        "cre_datetime" => "NULL",
      );
      $this->db->insert($this->_skilled, $nulled_data);
      return $this->db->insert_id();
    }
    return false;
  }

  public function update_verified_email_or_otp_old($cust_id=0, $type=null)
  {
    if( $cust_id > 0 AND $type != null ) {
      
      if($type == "otp") {  $data = array( "mobile_verified" => 1); }
      else if($type == "email") { $data = array( "email_verified" => 1); }
      else { return false; }

      $this->db->where('cust_id', (int) $cust_id);
      $this->db->update($this->_customers_master, $data);
      return $this->db->affected_rows();
    }
    return false;
  }

  public function validate_login($email=null, $password=null)
  {
    if($email !=null AND $password != null) {
      $this->db->select('cust_id');
      $this->db->where('email1', trim($email));
      $this->db->where('password', trim($password));
      if( $return = $this->db->get($this->_customers_master)->row()){ return $return->cust_id;  }
    }
    return false;
  }

  //  public function get_job_list_with_filter(array $data)
  // {
  //   // echo json_encode($data);die();
  //   $this->db->where('job_status', 'open');
  //   $this->db->where('visibility', 'public');
  //   $this->db->where('open_for_proposal', '1');
  //   $this->db->where('job_type', '0');
  //   if($data['country_id'] > 0) { $this->db->where('sj.country_id', $data['country_id']); }
  //   if($data['cat_type_id'] > 0) { $this->db->where('sj.cat_id', $data['cat_type_id']); }
  //   if($data['cat_id'] > 0) { $this->db->where('sj.sub_cat_id', $data['cat_id']); }

  //   if($data['start_price'] > 0) { $this->db->where('sj.budget >= ', (int)$data['start_price']); }
  //   if($data['end_price'] > 0) { $this->db->where('sj.budget <= ', (int)$data['end_price']); }

  //   if($data['sort_by'] == 'high') { $this->db->order_by('CAST(sj.budget as SIGNED INTEGER)', 'desc'); 
  //   } else if($data['sort_by'] == 'low') { $this->db->order_by('CAST(sj.budget as SIGNED INTEGER)', 'asc');
  //   } else { $this->db->order_by('sj.job_id', 'asc'); }

  //   return $this->db->get($this->_smp_jobs.' as sj')->result_array();
  // }

  public function get_job_list_with_filter(array $data)
  {
    $this->db->select('sj.*,c.cat_name,c.icon_url');
    $this->db->from($this->_smp_jobs.' as sj');
    $this->db->where('job_status', 'open');
    $this->db->where('visibility', 'public');
    $this->db->where('open_for_proposal', '1');
    $this->db->where('job_type', '0');
    $this->db->join($this->_category_master.' as c', ' sj.sub_cat_id = c.cat_id AND sj.cat_id = c.cat_type_id'); 
    if($data['country_id'] > 0) { $this->db->where('sj.country_id', $data['country_id']); }
    if($data['cat_type_id'] > 0) { $this->db->where('sj.cat_id', $data['cat_type_id']); }
    if($data['cat_id'] > 0) { $this->db->where('sj.sub_cat_id', $data['cat_id']); }

    if($data['start_price'] > 0) { $this->db->where('sj.budget >= ', (int)$data['start_price']); }
    if($data['end_price'] > 0) { $this->db->where('sj.budget <= ', (int)$data['end_price']); }

    if($data['sort_by'] == 'high') { $this->db->order_by('CAST(sj.budget as SIGNED INTEGER)', 'desc'); 
    } else if($data['sort_by'] == 'low') { $this->db->order_by('CAST(sj.budget as SIGNED INTEGER)', 'asc');
    } else { $this->db->order_by('sj.job_id', 'asc'); }

    return $this->db->get()->result_array();
  }

  public function get_troubleshoot_list_with_filter(array $data)
  {
    $this->db->select('sj.*,c.cat_name,c.icon_url');
    $this->db->from($this->_smp_jobs.' as sj');
    $this->db->where('job_status', 'open');
    $this->db->where('visibility', 'public');
    $this->db->where('open_for_proposal', '1');
    $this->db->where('job_type', '1');
    $this->db->where('service_type', '0');
    $this->db->join($this->_category_master.' as c', ' sj.sub_cat_id = c.cat_id AND sj.cat_id = c.cat_type_id'); 
    if($data['country_id'] > 0) { $this->db->where('sj.country_id', $data['country_id']); }
    if($data['cat_type_id'] > 0) { $this->db->where('sj.cat_id', $data['cat_type_id']); }
    if($data['cat_id'] > 0) { $this->db->where('sj.sub_cat_id', $data['cat_id']); }

    if($data['start_price'] > 0) { $this->db->where('sj.budget >= ', (int)$data['start_price']); }
    if($data['end_price'] > 0) { $this->db->where('sj.budget <= ', (int)$data['end_price']); }

    if($data['sort_by'] == 'high') { $this->db->order_by('CAST(sj.budget as SIGNED INTEGER)', 'desc'); 
    } else if($data['sort_by'] == 'low') { $this->db->order_by('CAST(sj.budget as SIGNED INTEGER)', 'asc');
    } else { $this->db->order_by('sj.job_id', 'asc'); }

    return $this->db->get()->result_array();
  }

  public function get_total_submitted_hours($job_id = 0)
  {
    if($job_id >0){
      $this->db->select('SUM(work_hours) AS total_hours');
      $this->db->where("job_id" , $job_id);
      $this->db->where("status" , "accept");
      return $this->db->get($this->_smp_milestone_payment_request)->row_array();
    }return false;
  }
  public function get_milestone_payment_request_filter($job_id = 0 , $type = "NULL")
  {
    if($job_id > 0){
      $this->db->where('job_id', $job_id);
      if($type == "open"){ $this->db->where('status', "open"); }
      if($type == "reject"){ $this->db->where('status', "reject"); }
      if($type == "accept"){ $this->db->where('status', "accept"); }
      return $this->db->get($this->_smp_milestone_payment_request)->result_array();
    }return false;
  }
  //maaz
  public function get_master_category_by_id($id=0)
  {
    $this->db->select("cat_type");
    $this->db->where("cat_type_id" , $id);
    return $this->db->get("tbl_category_type_master")->row();
  }
  public function get_sub_category_by_id($id=0)
  {
    $this->db->select("cat_name");
    $this->db->where("cat_id" , $id);
    return $this->db->get("tbl_category_master")->row();
  }
  public function register_consumer(array $data)
  { 
    if(is_array($data)){
      $today = date("Y-m-d H:i:s");
      $isnert_data = array(
        "firstname" => trim($data['firstname']), 
        "lastname" => trim($data['lastname']), 
        "email1" => trim($data['email_id']),   
        "password" => trim($data['password']),    
        "mobile1" => trim($data['mobile_no']),
        "gender" => trim($data['gender']),  
        "city_id" => (int) $data['city_id'],  
        "state_id" => (int) $data['state_id'], 
        "country_id" => (int) $data['country_id'],
        "user_type" => (int) $data['usertype'], 
        "avatar_url" => trim($data['avatar_url']), 
        "cover_url" => trim($data['cover_url']), 
        "social_login" => (int) $data['social_login'], 
        "activation_hash" => $data['activation_hash'], 
        "cust_otp" => $data['otp'], 
        "email_verified" => (int) $data['email_verified'], 
        "company_name" => $data['company_name'],            
        "acc_type" => $data['acc_type'],            
        "cre_datetime" => $today, 
        "last_login_datetime" => $today, 
        "cust_status" => 1, 
        "overview" => "NULL", 
        "social_type" => $data['social_type'], 
        "lang_known" => "NULL", 
        "is_deliverer" => $data['is_deliverer'],            
      );
      $this->db->insert($this->_customers_master, $isnert_data);
      return $this->db->insert_id();
    }
    return false;
  }
  public function register_nulled_skilled($cust_id=0)
  {
    if($cust_id > 0 ){
      $nulled_data = array(
        "cust_id" => (int) $cust_id,
        "junior_skills" => "NULL",
        "confirmed_skills" => "NULL",
        "senior_skills" => "NULL",
        "expert_skills" => "NULL",
        "ext1" => "NULL",
        "ext2" => "NULL",
        "cre_datetime" => "NULL",
      );
      $this->db->insert($this->_skilled, $nulled_data);
      return $this->db->insert_id();
    }
    return false;
  }   
  public function update_verified_email_or_otp($cust_id=0, $type=null)
  {
    if( $cust_id > 0 AND $type != null ) {
      
      if($type == "otp") {  $data = array( "mobile_verified" => 1); }
      else if($type == "email") { $data = array( "email_verified" => 1); }
      else { return false; }

      $this->db->where('cust_id', (int) $cust_id);
      $this->db->update($this->_customers_master, $data);
      return $this->db->affected_rows();
    }
    return false;
  }   
  public function list_of_provider(array $data)
  {
    // echo  json_encode($data);die();
    $this->db->select('fp.*, skill.confirmed_skills as skills , skill.confirmed_skills_name as skills_name , country.country_name as country_name , city.city_name as city_name');
    $this->db->from($this->_skilled.' as skill');
    $this->db->from($this->_countries.' as country');
    $this->db->from($this->_cities.' as city');
    $this->db->from($this->_smp_jobs.' as job');
    if($data['fav'] == "no"){
      $this->db->join($this->_smp_freelancer_profile.' as fp','fp.cust_id != '.$data['cust_id'].' AND fp.cust_id = skill.cust_id AND fp.country_id = country.country_id AND fp.city_id = city.city_id ');
      if($data['skill'] != "NULL"){        
        $this->db->where('LOCATE("'.$data['skill'].'", skill.confirmed_skills_name)');

      }
    }
    if($data['fav'] == "yes"){
      $this->db->from($this->_smp_favorite_providers.' as fav');
      $this->db->join($this->_smp_freelancer_profile.' as fp' , 'fp.cust_id = fav.provider_id AND fp.country_id = country.country_id AND fp.city_id = city.city_id ');
      $this->db->where("fav.cust_id" , $data['cust_id']);
    }
    if($data['country_id'] != "NULL"){$this->db->where('fp.country_id' , $data['country_id']);}
    if($data['state_id'] != "NULL"){$this->db->where('fp.state_id' , $data['state_id']);}
    if($data['city_id'] != "NULL"){$this->db->where('fp.city_id' , $data['city_id']);}
    if($data['rate_from'] != "NULL"){$this->db->where('fp.rate_per_hour >=' , $data['rate_from']);}
    if($data['rate_to'] != "NULL"){$this->db->where('fp.rate_per_hour <=' , $data['rate_to']);}
    $this->db->group_by('fp.cust_id');
    return $this->db->get($this->_smp_freelancer_profile)->result_array();
  }
  public function make_workstream_notification_read($cust_id=0, $notification_id=0)
  {
    if($cust_id > 0 AND $notification_id > 0) {
      $data = ['cust_id' => $cust_id, 'notification_id' => $notification_id];
      $this->db->insert("tbl_smp_read_workstream_notification", $data);
      return true;
    } return false;
  }
  public function get_globle_workstream_details($cust_id=0 , $last_id=0)
  {
    if($cust_id > 0 ) {
      $this->db->select("job.* , (SELECT cat_name FROM `tbl_category_master` WHERE cat_id=job.sub_cat_id) as cat_name , (SELECT icon_url FROM `tbl_category_master` WHERE cat_id=job.sub_cat_id) as icon_url");
      $this->db->where("(job.cust_id=$cust_id or job.provider_id=$cust_id)");
      $this->db->order_by("job.job_id" , "desc");

      if($last_id > 0) {
        $start=($last_id-1)*10;
        $this->db->limit(10 ,$start);
      }
      return $this->db->get($this->_smp_jobs." as job")->result_array();

    } return false;
  }
  public function get_total_unread_workroom_notifications_count($cust_id=0)
  {
    $qry = "SELECT wsd_id FROM tbl_smp_workstream_details WHERE wsd_id NOT IN ( SELECT notification_id FROM tbl_smp_read_workstream_notification WHERE notification_id = wsd_id ) AND is_admin != 1 AND sender_id != $cust_id AND (cust_id = $cust_id OR provider_id = $cust_id)"; 
    return $this->db->query($qry)->num_rows();    
  }
  public function get_total_unread_workroom_notifications($cust_id=0, $limit=5)
  {
    $qry = "SELECT * FROM tbl_smp_workstream_details WHERE wsd_id NOT IN ( SELECT notification_id FROM tbl_smp_read_workstream_notification WHERE notification_id = wsd_id ) AND  is_admin != 1 AND sender_id != $cust_id AND (cust_id = $cust_id OR provider_id = $cust_id) ORDER BY `cre_datetime` DESC LIMIT $limit";
    return $this->db->query($qry)->result_array();    
  }
  public function get_total_unread_notifications_count($cust_id=0, $type=null)
  {
    $qry = "SELECT id FROM `tbl_notifications` WHERE id NOT IN ( SELECT notification_id FROM tbl_read_notifications WHERE cust_id = $cust_id ) AND ( `consumer_id` = $cust_id OR `consumer_id` = 0 ) AND (`consumer_filter` = 'All' OR `consumer_filter` = '".$type."' )";
    return $this->db->query($qry)->num_rows();    
  }
  public function get_total_unread_notifications($cust_id=0,$type=null, $limit=0)
  {
    $qry = "SELECT id, notify_title, notify_text, attachement_url, cre_datetime FROM `tbl_notifications` WHERE id NOT IN ( SELECT notification_id FROM tbl_read_notifications WHERE cust_id = $cust_id ) AND ( `consumer_id` = $cust_id OR `consumer_id` = 0 ) AND (`consumer_filter` = 'All' OR `consumer_filter` = '".$type."' ) ORDER BY `cre_datetime` DESC "; 
    if($limit > 0 ) { $qry .= " LIMIT $limit"; }
    return  $this->db->query($qry)->result_array();
  }
  public function make_notification_read($id=0, $cust_id=0)
  {
    if($id > 0 AND $cust_id > 0) {
      $this->db->insert($this->_read_notifications, ['cust_id' => $cust_id, 'notification_id' => $id, 'read_datetime' => date('Y-m-d H:i:s')]);
      return true;
    } return false;
  }
  public function get_troubleshoot_list(array $data , $last_id=0)
  {
    if(is_array($data)){
      $this->db->select("tp.* , (SELECT cat_name FROM `tbl_category_master` WHERE cat_id=tp.sub_cat_id) as cat_name , (SELECT icon_url FROM `tbl_category_master` WHERE cat_id=tp.sub_cat_id) as icon_url");
      $this->db->where('cust_id !=', $data['cust_id']);
      $this->db->where('job_status', 'open');
      $this->db->where('visibility', 'public');
      $this->db->where('open_for_proposal', '1');
      $this->db->where('job_type', '1');
      $this->db->where("Job_id NOT IN (SELECT job_id FROM `tbl_smp_job_proposals` WHERE provider_id=".$data['cust_id']." AND job_id=tp.job_id)");

      if($last_id > 0) {
        $start=($last_id-1)*10;
        $this->db->limit(10 ,$start);
      }
      if($data['skill']){
       $res=$this->db->query("SELECT GROUP_CONCAT(cat_id) as skill_cat FROM `tbl_category_master` WHERE cat_name LIKE '%".$data['skill']."%'")->row();
        $pieces = explode(",", $res->skill_cat);
       $this->db->where_in('tp.sub_cat_id' , $pieces);
      }
      if(isset($data['sub_cat_id']) && $data['sub_cat_id'] > 0) { $this->db->where('tp.sub_cat_id', $data['sub_cat_id']); } //categories-wise
      if(isset($data['cat_id']) &&  $data['cat_id'] > 0) { $this->db->where('tp.cat_id', $data['cat_id']); } //Category type-wise

      if(isset($data['start_date']) &&  $data['start_date'] != 'NULL') { $this->db->where('tp.cre_datetime >=', $data['start_date']); } //From date
      if(isset($data['end_date']) &&  $data['end_date'] != 'NULL') { $this->db->where('tp.cre_datetime <', $data['end_date']); } //To date

      $this->db->order_by("job_id" , "desc");
      return $this->db->get($this->_smp_jobs." tp")->result_array();
    }return false;
  }
  public function get_job_list(array $data , $last_id=0)
  {
    if(is_array($data)){
      $this->db->select("tp.* , (SELECT cat_name FROM `tbl_category_master` WHERE cat_id=tp.sub_cat_id) as cat_name , (SELECT icon_url FROM `tbl_category_master` WHERE cat_id=tp.sub_cat_id) as icon_url");
      $this->db->where('cust_id !=', $data['cust_id']);
      $this->db->where('job_status', 'open');
      $this->db->where('visibility', 'public');
      $this->db->where('open_for_proposal', '1');
      $this->db->where('job_type', '0');
      $this->db->where("Job_id NOT IN (SELECT job_id FROM `tbl_smp_job_proposals` WHERE provider_id=".$data['cust_id']." AND job_id=tp.job_id)");
     if($last_id > 0) {
        $start=($last_id-1)*10;
        $this->db->limit(10 ,$start);
      }
      if(isset($data['sub_cat_id']) && $data['sub_cat_id'] > 0) { $this->db->where('tp.sub_cat_id', $data['sub_cat_id']); } //categories-wise
      if(isset($data['cat_id']) &&  $data['cat_id'] > 0) { $this->db->where('tp.cat_id', $data['cat_id']); } //Category type-wise

      if(isset($data['start_date']) &&  $data['start_date'] != 'NULL') { $this->db->where('tp.cre_datetime >=', $data['start_date']); } //From date
      if(isset($data['end_date']) &&  $data['end_date'] != 'NULL') { $this->db->where('tp.cre_datetime <', $data['end_date']); } //To date

      $this->db->order_by("job_id" , "desc");
      return $this->db->get($this->_smp_jobs." tp")->result_array();
    }return false;
  }

  public function get_services_invoices_old($cust_id=0, $last_id=0, $device=0)
  {
    if($cust_id > 0) {
      $this->db->where('cust_id', (int)$cust_id);
      $this->db->where('invoice_url !=', 'NULL');
      if($last_id > 0) { $this->db->where('req_id <', $last_id); }

      $this->db->or_where('provider_id', (int)$cust_id);
      $this->db->where('invoice_url !=', 'NULL');
      if($last_id > 0) { $this->db->where('booking_id <', $last_id); }

      if($device > 0) { $this->db->limit(10); }
      
      $this->db->order_by('booking_id', 'desc');
      return $this->db->get($this->_laundry_booking)->result_array();
    }
    return false;
  }
  public function get_category_by_id_all()
  {
    $this->db->where('cat_type_id', 8);
    $this->db->or_where('cat_type_id', 8);
    $this->db->or_where('cat_type_id', 9);
    $this->db->or_where('cat_type_id', 10);
    $this->db->or_where('cat_type_id', 11);
    $this->db->or_where('cat_type_id', 12);
    $this->db->or_where('cat_type_id', 13);
    $this->db->or_where('cat_type_id', 14);
    $this->db->or_where('cat_type_id', 15);
    $this->db->or_where('cat_type_id', 16);
    $this->db->or_where('cat_type_id', 17);
    $this->db->or_where('cat_type_id', 18);
    $this->db->or_where('cat_type_id', 19);
    $this->db->or_where('cat_type_id', 20);
    $this->db->or_where('cat_type_id', 21);
    $this->db->or_where('cat_type_id', 22);
    $this->db->or_where('cat_type_id', 25);
    $this->db->or_where('cat_status', 1);
    return $this->db->get($this->_category_master)->result_array();
  }

  public function get_category_by_ids($cat_id = 0)
  {
    if ($cat_id > 0) {
      $this->db->where('cat_id', $cat_id);
      $this->db->where('cat_status', 1);
      return $this->db->get($this->_category_master)->row_array();
    }else return false;
  }

  //Start Wallet/Scrow/Withdraw------------------------------
    public function get_services_invoices($cust_id=0)
    {
      if($cust_id > 0) {
        $this->db->select('req.*,job.*,mp.milestone_price');
        $this->db->from($this->_smp_milestone_payment_request.' as req');
        $this->db->from($this->_smp_proposal_milestone_details.' as mp');
        $this->db->join($this->_smp_jobs.' as job', 'job.job_id = req.job_id AND req.milestone_id = mp.milestone_id');
        $this->db->where('job.cust_id', $cust_id);
        $this->db->or_where('job.provider_id', $cust_id);
        $this->db->group_by('req.req_id');
        return $this->db->get($this->_smp_jobs)->result_array();
      } return false;
    }

    public function get_services_account_history($cust_id=0, $last_id=0)
    {
      if($cust_id > 0) {
        $this->db->where('user_id', (int)$cust_id);
        if($last_id > 0) { $this->db->where('ah_id <', $last_id); }
        $this->db->limit(10);
        $this->db->order_by('ah_id', 'desc');
        return $this->db->get($this->_user_account_history)->result_array();
      }
      return false;
    }

    public function deliverer_scrow_master_list($deliverer_id=0)
    {
      if($deliverer_id > 0) {
        $this->db->where('deliverer_id', (int)$deliverer_id);
        return $this->db->get($this->_deliverer_scrow_master)->result_array();
      } return false;
    }
    public function get_customer_scrow_history($deliverer_id=0, $last_id=0, $device=0)
    {
      if($deliverer_id > 0) {
        $this->db->where('deliverer_id', (int)$deliverer_id);
        $this->db->or_where('cat_id !=', 281);
        $this->db->or_where('cat_id !=', 6);
        $this->db->or_where('cat_id !=', 7);
        $this->db->or_where('cat_id !=', 9);
        if($last_id > 0) { $this->db->where('scrow_id <', $last_id); }
        if($device > 0) { $this->db->limit(10); }
        $this->db->order_by('scrow_id', 'desc');
        return $this->db->get($this->_deliverer_scrow_history)->result_array();
      }
      return false;
    }
    public function get_customer_account_withdraw_request_list($cust_id=0, $last_id=0, $device=0)
    {
      if($cust_id > 0) {
        $this->db->where('user_id', (int)$cust_id);
        $this->db->where('user_type', 0);
        if($last_id > 0) { $this->db->where('req_id <', $last_id); }
        if($device > 0) { $this->db->limit(10); }
        $this->db->order_by('req_id', 'desc');
        return $this->db->get($this->_account_withdraw_request)->result_array();
      } return false;
    }
    public function get_customer_account_withdraw_request_sum_currencywise($cust_id=0, $response=null, $currency_code=null)
    {
      if($cust_id > 0 && !is_null($response) && !is_null($currency_code)) {
        $this->db->select('SUM(amount) AS amount');
        $this->db->where('user_id', (int)$cust_id);
        $this->db->where('response', trim($response));
        $this->db->where('currency_code', trim($currency_code));
        return $this->db->get($this->_account_withdraw_request)->row_array();
      } return false;
    }
    public function check_user_account_balance_by_id($account_id=0)
    {
      if($account_id > 0) {
        $this->db->where('account_id', (int)$account_id);
        return $this->db->get($this->_user_account_master)->row_array();
      } return false;
    }
    public function check_user_account_balance($cust_id=0, $currency_code=null)
    {
      if($cust_id > 0 && !is_null($currency_code)) {
        $this->db->where('user_id', (int)$cust_id);
        $this->db->where('currency_code', $currency_code);
        $row = $this->db->get($this->_user_account_master)->row_array();
        return $row['account_balance'];
      }
      return false;
    }
    public function user_account_withdraw_request(array $insert_data)
    {
      if(is_array($insert_data)) {
        $this->db->insert($this->_account_withdraw_request, $insert_data);
        return $this->db->affected_rows();
      } return false;
    }
  //End Wallet/Scrow/Withdraw--------------------------------
    public function customer_account_master_details_smp($cust_id=0, $currency_sign=null)
    {
      if($cust_id > 0 && !is_null($currency_sign)) {
        $this->db->where('user_id', (int)$cust_id);
        $this->db->where('currency_code', trim($currency_sign));
        return $this->db->get($this->_smp_user_account_master)->row_array();
      } return false;
    }
    public function insert_gonagoo_customer_record_smp(array $insert_data_customer_master)
    {
      if(is_array($insert_data_customer_master)) {
        $this->db->insert($this->_smp_user_account_master, $insert_data_customer_master);
        return $this->db->insert_id();
      } return false;
    }
    public function update_payment_in_customer_master_smp($account_id=0, $cust_id=0, $account_balance=0)
    {
      if($cust_id > 0 && $account_id > 0) {
        $this->db->where('account_id', (int)$account_id);
        $this->db->where('user_id', (int)$cust_id);
        $this->db->update($this->_smp_user_account_master, ['account_balance' => $account_balance , 'update_datetime' => date('Y-m-d H:i:s')]);
        return $this->db->affected_rows();
      } return false;
    }
    public function insert_payment_in_account_history_smp(array $insert_data)
    {
      if(is_array($insert_data)) {
        $this->db->insert($this->_smp_user_account_history, $insert_data);
        return $this->db->insert_id();
      } return 0;
    }
    public function get_services_account_history_smp($cust_id=0, $last_id=0)
    {
      if($cust_id > 0) {
        $this->db->where('user_id', (int)$cust_id);
        if($last_id > 0) { $this->db->where('ah_id <', $last_id); }
        $this->db->limit(10);
        $this->db->order_by('ah_id', 'desc');
        return $this->db->get($this->_smp_user_account_history)->result_array();
      }
      return false;
    }
    public function scrow_master_details_smp($deliverer_id=0, $currency_sign=null)
    {
      if($deliverer_id > 0 && !is_null($currency_sign)) {
        $this->db->where('deliverer_id', (int)$deliverer_id);
        $this->db->where('currency_code', $currency_sign);
        return $this->db->get($this->_smp_deliverer_scrow_master)->row_array();
      } return false;
    }
    public function insert_scrow_record_smp(array $insert_data_customer_scrow)
    {
      if(is_array($insert_data_customer_scrow)) {
        $this->db->insert($this->_smp_deliverer_scrow_master, $insert_data_customer_scrow);
        return $this->db->insert_id();
      } return false;
    }
    public function update_scrow_payment_smp($scrow_id=0, $deliverer_id=0, $scrow_balance=0)
    {
      if($deliverer_id > 0 && $scrow_id > 0) {
        $this->db->where('deliverer_id', (int)$deliverer_id);
        $this->db->where('scrow_id', (int)$scrow_id);
        $this->db->update($this->_smp_deliverer_scrow_master, ['scrow_balance' => $scrow_balance]);
        return $this->db->affected_rows();
      }
      return false;
    }
    public function insert_scrow_history_record_smp(array $insert_data)
    {
      if(is_array($insert_data)) {
        $this->db->insert($this->_smp_deliverer_scrow_history, $insert_data);
        return $this->db->affected_rows();
      } return false;
    }
    public function deliverer_scrow_master_list_smp($deliverer_id=0)
    {
      if($deliverer_id > 0) {
        $this->db->where('deliverer_id', (int)$deliverer_id);
        return $this->db->get($this->_smp_deliverer_scrow_master)->result_array();
      } return false;
    }
    public function get_customer_scrow_history_smp($cust_id=0)
    {
      if($cust_id > 0) {
        $this->db->where('deliverer_id', (int)$cust_id);
        $this->db->limit(300);
        return $this->db->get($this->_smp_deliverer_scrow_history)->result_array();
      }
      return false;
    }
    public function get_service_job_list($cust_id=0)
    {
      if($cust_id > 0) { $this->db->where('provider_id', (int)($cust_id)); }
      $this->db->where('job_status !=', "open");
      $this->db->where('withdraw_status !=', "accept");
      $this->db->group_by('job_id');
      return $this->db->get($this->_smp_jobs)->result_array();
    }
    public function get_job_completed_milestone($job_id=0)
    {
      if($job_id > 0) {
        $this->db->select('req.*, mile.milestone_price');
        $this->db->from($this->_smp_milestone_payment_request.' as req');
        $this->db->join($this->_smp_proposal_milestone_details.' as mile', 'req.milestone_id = mile.milestone_id');
        $this->db->where('req.job_id', (int)$job_id);
        $this->db->where('req.status', "accept");
        $this->db->where('mile.amount_paid', "1");
        return $this->db->get()->result_array();
      }
      return false;
    }



    public function get_customer_experience($cust_id=0)
    {
      $this->db->where('cust_id', $cust_id);
      return $this->db->get($this->_customer_experience)->result_array();
    }
    public function get_customer_education($cust_id=0)
    {
      $this->db->where('cust_id', $cust_id);
      return $this->db->get($this->_customer_education)->result_array();
    }
    public function get_business_photos($cust_id=0)
    {
      if($cust_id > 0 ){
        $this->db->where('cust_id', (int) $cust_id);
        $this->db->order_by('photo_id', 'desc');
        return $this->db->get($this->_business_photos)->result_array();
      }
      return false;
    }
    public function update_user_basic_info($cust_id=0, array $data)
    {
      if($cust_id > 0 && is_array($data)){
        $this->db->where('cust_id', (int)$cust_id);
        $this->db->update($this->_customers_master, $data);
        return $this->db->affected_rows();
      } 
      return false;
    }
    public function get_old_profile_image($cust_id=0)
    {
      if($cust_id > 0 ){
        $this->db->select('avatar_url');
        $this->db->where('cust_id', (int) $cust_id);
        if( $result = $this->db->get($this->_customers_master)->row_array()){
          return (trim($result['avatar_url']));
        }     
      }
      return false;
    }
    public function update_profile_image($avatar_url=null, $cust_id=0)
    {
      if($avatar_url != null AND $cust_id > 0){
        $this->db->where('cust_id', $cust_id);
        $this->db->update($this->_customers_master, array("avatar_url" => trim($avatar_url)));
        return $this->db->affected_rows();
      }
      return false;
    }
    public function get_old_cover_image($cust_id=0)
    {
      if($cust_id > 0 ){
        $this->db->select('cover_url');
        $this->db->where('cust_id', (int) $cust_id);
        if( $result = $this->db->get($this->_customers_master)->row_array()){
          return (trim($result['cover_url']));
        }     
      }
      return false;
    }
    public function update_cover_image($cover_url=null, $cust_id=0)
    {
      if($cover_url != null AND $cust_id > 0){
        $this->db->where('cust_id', $cust_id);
        $this->db->update($this->_customers_master, array("cover_url" => trim($cover_url)));
        return $this->db->affected_rows();
      }
      return false;
    }
    public function get_old_document_url($cust_id=0, $doc_id=0)
    {
      if( $cust_id > 0 AND $doc_id > 0 ) {
        $this->db->select('attachement_url');
        $this->db->where('doc_id', (int) $doc_id); 
        $this->db->where('cust_id', (int) $cust_id);
        $result = $this->db->get($this->_documents)->row_array();     
        return $result['attachement_url'];
      } 
      return false;
    }
    public function get_milestone_payment_request_by_milestone_id($milestone_id = 0)
    {
      if($milestone_id > 0){
        $this->db->where('milestone_id', $milestone_id);
        return $this->db->get($this->_smp_milestone_payment_request)->row_array();
      }return false;
    }

    public function get_favourite_freelancers($cust_id=0)
    {
      if($cust_id > 0 ) {
        return $this->db->query('SELECT d.*, c.ratings , country.country_name as country_name , city.city_name as city_name , skill.confirmed_skills as skills , skill.confirmed_skills_name as skills_name FROM tbl_smp_freelancer_profile as d JOIN tbl_customers_master AS c JOIN tbl_countries as country JOIN tbl_cities as city JOIN tbl_customer_skills as skill ON d.cust_id = c.cust_id AND d.city_id = city.city_id AND d.country_id = country.country_id AND d.cust_id = skill.cust_id WHERE d.cust_id IN ( SELECT provider_id FROM tbl_smp_favorite_providers WHERE cust_id = '.$cust_id.' )')->result_array();
      } return false;
    }

    public function logged_in_user_details($id=0)
    {
      if($id > 0 ) {
        $this->db->select('auth_id, auth_fname as firstname, auth_lname as lastname, auth_email as email, auth_type_id as type_id, auth_avatar_url as avatar_url');
        $this->db->where('auth_id', $id);
        return $this->db->get($this->authorities)->row_array();
      }
      return false;
    }


}
/* End of file Api_model_services.php */
/* Location: ./application/models/Api_model_services.php */