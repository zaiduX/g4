<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relay_api_model extends CI_Model {

	private $_app_parameter = 'driver_app_param_master';
	private $_customer_drivers = 'tbl_customer_drivers';
	private $_driver_device = 'tbl_driver_device';
	private $_courier_orders = 'tbl_courier_orders';
	private $_courier_order_status = 'tbl_courier_order_status';
	private $_customers_master = 'tbl_customers_master';
	private $_customer_device = 'tbl_customer_device';
	private $_driver_chats = 'tbl_driver_chats';
	private $_courier_workroom = 'tbl_courier_workroom';
	private $_countries = 'tbl_countries';
	private $_states = 'tbl_states';
	private $_cities = 'tbl_cities';
	private $_relay_points = 'tbl_relay_points';
	private $_relay_points_warehouse = 'tbl_relay_points_warehouse';
	private $_relay_warehouse_details = 'tbl_relay_warehouse_details';
	private $_deliverer_scrow_master = 'tbl_deliverer_scrow_master';
	private $_deliverer_scrow_history = 'tbl_deliverer_scrow_history';
	private $_user_account_master = 'tbl_user_account_master';
	private $_user_account_history = 'tbl_user_account_history';
	private $_gonagoo_account_master = 'tbl_gonagoo_account_master';
	private $_gonagoo_account_history = 'tbl_gonagoo_account_history';
	private $_relay_account_master = 'tbl_relay_account_master';
	private $_relay_account_history = 'tbl_relay_account_history';
	private $_account_withdraw_request = 'tbl_account_withdraw_request';
    private $_order_packages = 'tbl_order_packages';
    private $_dangerous_goods_master = "tbl_dangerous_goods_master";
    private $_gonagoo_address = "tbl_gonagoo_address";

	/*
	public function get_application_parameters()
	{
		return $this->db->get($this->_app_parameter)->row();
	}

	public function validate_login($email=null, $password=null)
 	{
 		if($email !=null AND $password != null) {
 			$this->db->select('cd_id');
 			$this->db->where('email', trim($email));
 			$this->db->where('password', trim($password));
 			if( $return = $this->db->get($this->_customer_drivers)->row()){	return $return->cd_id; 	}
 		}
 		return false;
 	}
 	*/
	
	public function is_emailId_exists($email=null, $all=false)
	{
		if($email != null) {
			if(!$all){
				$this->db->select('cd_id,cust_id,first_name,last_name,email,password,mobile1,mobile2,cat_ids,available,avatar,latitude,longitude,loc_update_datetime,cre_datetime,mod_datetime,status,vehicle_id');
			}
			$this->db->where('email', trim($email));
			return $this->db->get($this->_customer_drivers)->row_array();
		}
		return false;
	} 

	public function is_deviceId_exists($device_id=null)
	{
		if($device_id != null) {
			$this->db->where('device_id', trim($device_id));
			return $this->db->get($this->_driver_device)->row();
		}
		return false;
	}

	public function register_new_device(array $data)
	{
		if(is_array($data)) {
			$this->db->insert($this->_driver_device, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function update_device_detail($device_id=null, array $data)
	{
		if($device_id != null AND is_array($data)) {
			$this->db->where('device_id', trim($device_id));
			$this->db->update($this->_driver_device, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_last_login($cd_id=0)
	{
		if($cd_id>0){
			$today = date('Y-m-d H:i:s');
			$this->db->where('cd_id', $cd_id);
			$this->db->update($this->_customer_drivers, array("mod_datetime" => $today));
			return $this->db->affected_rows();
		}	
		return false;
	}

	public function is_driver_active($cd_id=0)
	{
		if($cd_id > 0 ) {
			$this->db->where('cd_id', (int)($cd_id));
			$this->db->where('status', 1);
			if($this->db->get($this->_customer_drivers)->row()){
				return true;
			}
		}
		return false;
	}

	public function sendEmail($user_name, $user_email, $subject=null, $message=null) 
	{
	    $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
	    $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
	   
	    $messageBody .="</head><body bgcolor='#FFFFFF'>";
	    $messageBody .="<table class='body-wrap'><tr><td></td><td class='container'><div class='content'>";
	    $messageBody .="<table><tr><td align='center'><h3>".$subject."</h3></td></tr>";
	    $messageBody .="<tr><td align='center'><h3>Dear <small>".$user_name."</small>,</h3>";
	    $messageBody .= $message;

	    $messageBody .="</td></tr></table></div></td><tr/></table>";
	    //Email Signature
	    $messageBody .="<table class='head-wrap'>";
	    $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
	    $messageBody .="<tr><td>Your Gonagoo team !</td></tr>";
	    $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />Deliver, ship cheaper easily</td></tr>";
	    $messageBody .="<tr><td>Support : support@gonagoo.com</td></tr>";
	    $messageBody .="<tr><td>Website : www.gonagoo.com</td></tr>";
	    //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
	    $messageBody .="<tr><td>Join us on : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;'></a>";
	    $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;'></a>";
	    $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;'></a></td></tr>";
	    //Download the App <icon for iOS App download><icon for Android App download>
	    $messageBody .="<tr><td>Download the App : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;'></a>";
	    $messageBody .="<a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;'></a></td></tr>";
	    $messageBody .="</table></div></td><td></td></tr></table>";
	    //Email Signature End
	    $messageBody .="</body></html>";

	    $email_from = $this->config->item('from_email');
	    $email_subject = $subject;
	    $headers = "MIME-Version: 1.0" . "\r\n";
	    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	    $headers .= 'From: '.$email_from.'' . "\r\n";
	    mail($user_email, $email_subject, $messageBody, $headers);  
	    return 1;
	}

	public function sendSMS_old($user_mobile, $message=null)
	{
		if($message!=null) {
		    $ch = curl_init("http://103.16.101.52:8080/sendsms/bulksms?"); // url to send sms
		    curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
		    curl_setopt($ch, CURLOPT_POST, 1); // method to call url
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
		    curl_setopt($ch, CURLOPT_POSTFIELDS,"username=kotp-noorisys&password=kaptrans&type=0&dlr=1&destination=$user_mobile&source=EZAMNA&message=$message"); 
		    $outputSMS = curl_exec($ch); // execute url and save response
			//var_dump($outputSMS); die();
		    curl_close($ch); // close url connection
		    //$res = "http://103.16.101.52:8080/sendsms/bulksms?"."username=kotp-noorisys&password=kaptrans&type=0&dlr=1&destination=$user_mobile&source=EZAMNA&message=$message";
			//var_dump($res);
		    return true;
		}
		return false;
	}

	public function sendSMS_twilio($user_mobile=null, $message=null)
  {
    if($message!=null && $user_mobile!=null) {
        $message .= ' - Your Team Gonagoo!';
        $uri = 'https://api.twilio.com/2010-04-01/Accounts/ACcd68595e7c0ea2401b0da3764c0e8aaf/SMS/Messages';
        $auth = 'ACcd68595e7c0ea2401b0da3764c0e8aaf:30f962c9cf51541b3ef14822bdcf1d63';
        $fields = 
            '&To=' .  urlencode( '+'.$user_mobile ) . 
            '&From=' . urlencode( '+33757905310' ) . 
            '&Body=' . urlencode( $message );
        $res = curl_init();
        curl_setopt( $res, CURLOPT_URL, $uri );
        curl_setopt( $res, CURLOPT_POST, 3 ); // number of fields
        curl_setopt( $res, CURLOPT_POSTFIELDS, $fields );
        curl_setopt( $res, CURLOPT_USERPWD, $auth ); // authenticate
        curl_setopt( $res, CURLOPT_RETURNTRANSFER, true ); // don't echo
        $result = curl_exec( $res );
    //var_dump($result); die();
        return true;
    }
  }

  public function sendSMS($user_mobile=null, $message=null)
  {
    if($message!=null && $user_mobile!=null) {
        $message .= ' - Your Team Gonagoo!';
        if(substr($user_mobile, 0, 1) == 0) { $user_mobile = ltrim($user_mobile, 0); }
        $ch = curl_init("http://smsc.txtnation.com:8091/sms/send_sms.php?"); // url to send sms
        curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
        curl_setopt($ch, CURLOPT_POST, 1); // method to call url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
        curl_setopt($ch, CURLOPT_POSTFIELDS,"src=330679328525&dst=$user_mobile&dr=1&user=nkengne&password=P9cKxW&type=0&msg=$message"); 
        $outputSMS = curl_exec($ch); // execute url and save response
        //var_dump($outputSMS);
        curl_close($ch); // close url connection
        return true;
    }
  }

	public function is_driver_password_match($cd_id=0, $password=null)
	{
		if($cd_id > 0 AND !is_null($password)){ 
			$this->db->where('cd_id', $cd_id);
			$this->db->where('password', $password);
			if($this->db->get($this->_customer_drivers)->row()){
				return true;
			}
		}	
		return false;
	}

	public function update_password($cd_id=0, array $data)
	{
		if($cd_id != null AND is_array($data)) {
			$this->db->where('cd_id', trim($cd_id));
			$this->db->update($this->_customer_drivers, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_profile_details($cd_id=0)
	{
		if($cd_id > 0 ) {
			$this->db->where('cd_id', (int)($cd_id));
			$this->db->where('status', 1);
			return $this->db->get($this->_customer_drivers)->row_array();
		}
		return false;
	}

	public function update_profile_details($cd_id=0, array $data)
	{
		if($cd_id != null AND is_array($data)) {
			$this->db->where('cd_id', trim($cd_id));
			$this->db->update($this->_customer_drivers, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function delete_old_profile_image($cd_id=0)
	{
		if($cd_id > 0 ){
			$this->db->select('avatar');
			$this->db->where('cd_id', (int) $cd_id);
			if( $result = $this->db->get($this->_customer_drivers)->row_array()){
				unlink(trim($result['avatar']));
			}			
		}
		return false;
	}
	
	public function update_profile_image($avatar=null, $cd_id=0)
	{
		if($avatar != null AND $cd_id > 0){
			$this->db->where('cd_id', $cd_id);
			$this->db->update($this->_customer_drivers, array("avatar" => trim($avatar)));
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_online_status($cd_id=0, array $data)
	{
		if($cd_id != null AND is_array($data)) {
			$this->db->where('cd_id', trim($cd_id));
			$this->db->update($this->_customer_drivers, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_driver_location($cd_id=0, array $data)
	{
		if($cd_id != null AND is_array($data)) {
			$this->db->where('cd_id', trim($cd_id));
			$this->db->update($this->_customer_drivers, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_relay_point_details($cd_id=0)
	{
		if($cd_id > 0 ) {
			$this->db->where('relay_mgr_id', (int)($cd_id));
			return $this->db->get($this->_relay_points)->row_array();
		}
		return false;
	}

	public function update_relay_details($relay_id=0, array $data)
	{
		if($relay_id != null AND is_array($data)) {
			$this->db->where('relay_id', trim($relay_id));
			$this->db->update($this->_relay_points, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function delete_old_relay_image($relay_id=0)
	{
		if($relay_id > 0 ){
			$this->db->select('image_url');
			$this->db->where('relay_id', (int) $relay_id);
			if( $result = $this->db->get($this->_relay_points)->row_array()){
				unlink(trim($result['image_url']));
			}			
		}
		return false;
	}
	
	public function update_relay_image($avatar=null, $relay_id=0)
	{
		if($avatar != null AND $relay_id > 0){
			$this->db->where('relay_id', $relay_id);
			$this->db->update($this->_relay_points, array("image_url" => trim($avatar)));
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_relay_status($relay_id=null, $status=null)
	{
		if($relay_id != null AND $status != null) {
			$this->db->where('relay_id', (int)trim($relay_id));
			$this->db->update($this->_relay_points, ['status' => (int)$status]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_relay_warehouse_count($relay_id=0, $count=0)
	{
		if($relay_id > 0 && $count > 0) {
			$this->db->where('relay_id', (int)trim($relay_id));
			$this->db->update($this->_relay_points, ['wh_count' => (int)$count]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_relay_warehouse($relay_id=0)
	{
		if($relay_id > 0 ) {
			$this->db->where('relay_id', (int)($relay_id));
			$this->db->where('status', 1);
			return $this->db->get($this->_relay_points_warehouse)->result_array();
		}
		return false;
	}

	public function get_warehouse_details($rpw_id=0)
	{
		if($rpw_id > 0 ) {
			$this->db->where('rpw_id', (int)($rpw_id));
			return $this->db->get($this->_relay_points_warehouse)->row_array();
		}
		return false;
	}

	public function register_warehouse(array $data)
	{
		if(is_array($data)){
			$this->db->insert($this->_relay_points_warehouse, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function update_relay_warehouse($rpw_id=0, array $data)
	{
		if(is_array($data) && $rpw_id > 0){
			$this->db->where('rpw_id', (int)($rpw_id));
			$this->db->update($this->_relay_points_warehouse, $data);
			return $this->db->affected_rows();
		}
		return false;
	}
	
	public function get_order_list($last_id=0, $relay_id=0)
	{
		if($relay_id > 0 ) {
			$last_id = (int) $last_id;

			$this->db->where('to_relay_id', (int)($relay_id));
			$this->db->where('deliverer_id != ', 0);
			if($last_id > 0) { $this->db->where('order_id <', $last_id); }
			
			$this->db->or_where('from_relay_id', (int)($relay_id));
			$this->db->where('deliverer_id != ', 0);
			if($last_id > 0) { $this->db->where('order_id <', $last_id); }

			$this->db->order_by('order_id', 'desc');
			$this->db->limit(10);

			return $this->db->get($this->_courier_orders)->result_array();
		}
		return false;
	}
	
	public function get_order_details_customer($delivery_code=null, $relay_id=0)
	{
		if(!is_null($delivery_code) && $relay_id > 0) {
			$relay_id = (int) $relay_id;
			$this->db->where('delivery_code', $delivery_code);
			$this->db->where('to_relay_id', $relay_id);
			
			$this->db->or_where('delivery_code', $delivery_code);
			$this->db->where('from_relay_id', $relay_id);
			return $this->db->get($this->_courier_orders)->row_array();
		}
		return false;
	}
	
	public function get_order_details_driver($driver_code=null, $relay_id=0)
	{
		if(!is_null($driver_code) && $relay_id > 0) {
			$relay_id = (int) $relay_id;
			$this->db->where('driver_code', $driver_code);
			$this->db->where('to_relay_id', $relay_id);
			
			$this->db->or_where('driver_code', $driver_code);
			$this->db->where('from_relay_id', $relay_id);
			return $this->db->get($this->_courier_orders)->row_array();
		}
		return false;
	}

	public function update_order_status($cd_id=0, $order_id=0, $order_status=null) //src_relay_in/src_relay_out/dest_relay_in/dest_relay_out
	{
		if($cd_id > 0 && $order_id > 0 && !is_null($order_status)) {
			$this->db->where('order_id', trim($order_id));
			// When courier recived at source relay point from customer
			if(trim($order_status) == 'src_relay_in') {
				$this->db->update($this->_courier_orders, ['from_relay_status' => $order_status, 'from_relay_status_datetime' => date('Y-m-d H:i:s')]);
			}
			// When courier recived at destination relay point from driver
			if(trim($order_status) == 'dest_relay_in') {
				$this->db->update($this->_courier_orders, ['delivered_datetime' => date('Y-m-d H:i:s'), 'driver_status_update' => 'delivered', 'to_relay_status' => $order_status, 'to_relay_status_datetime' => date('Y-m-d H:i:s')]);
			}
			// When courier out from src relay point to driver
			if(trim($order_status) == 'src_relay_out') {
				$this->db->update($this->_courier_orders, ['order_status' => 'in_progress', 'status_updated_by' => 'deliverer', 'status_update_datetime' => date('Y-m-d H:i:s'), 'driver_status_update' => 'in_progress', 'from_relay_status' => $order_status, 'from_relay_status_datetime' => date('Y-m-d H:i:s')]);
			}
			// When courier out from destination relay point to customer
			if(trim($order_status) == 'dest_relay_out') {
				$this->db->update($this->_courier_orders, ['order_status' => 'delivered', 'status_updated_by' => 'deliverer', 'status_update_datetime' => date('Y-m-d H:i:s'), 'to_relay_status' => $order_status, 'to_relay_status_datetime' => date('Y-m-d H:i:s')]);
			}
			return $this->db->affected_rows();
		}
		return false;
	}

	public function insert_order_status($cd_id=0, $order_id=0, $order_status=null)
	{
		if($cd_id > 0 && $order_id > 0 && !is_null($order_status)) {
			$this->db->insert($this->_courier_order_status, ['order_id' => $order_id, 'user_id' => $cd_id, 'user_type' => 'relay', 'status' => $order_status, 'mod_datetime' => date('Y-m-d H:i:s')]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function insert_order_status_deliverer($deliverer_id=0, $order_id=0, $order_status=null)
	{
		if($deliverer_id > 0 && $order_id > 0 && !is_null($order_status)) {
			$this->db->insert($this->_courier_order_status, ['order_id' => $order_id, 'user_id' => $deliverer_id, 'user_type' => 'deliverer', 'status' => $order_status, 'mod_datetime' => date('Y-m-d H:i:s')]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_user_details($cust_id=0)
	{
		if($cust_id > 0 ) {
			$this->db->where('cust_id', (int)($cust_id));
			return $this->db->get($this->_customers_master)->row_array();
		}
		return false;
	}

	public function get_user_device_details($cust_id=0)
	{
		if($cust_id > 0 ) {
			$this->db->select('reg_id, device_type');
			$this->db->where('cust_id', (int)($cust_id));
			return $this->db->get($this->_customer_device)->result_array();
		}
		return false;
	}

	public function post_to_workroom($order_id=0, $cust_id=0, $deliverer_id=0, $text_msg = null, $sender_type = null, $type = null, $attachment_url = null, $cust_name = null, $deliverer_name = null, $file_type = null, $thumbnail = "NULL", $ratings = "NULL")
	{
		if($order_id > 0 && $cust_id > 0 && $deliverer_id > 0 && !is_null($text_msg) && !is_null($sender_type)) {
			if($sender_type == 'sp') { $sender_id = $deliverer_id; } else { $sender_id = $cust_id; }
			$this->db->insert($this->_courier_workroom, ['order_id' => $order_id, 'cust_id' => $cust_id, 'deliverer_id' => $deliverer_id, 'text_msg' => $text_msg, 'attachment_url' => $attachment_url, 'cre_datetime' => date('Y-m-d H:i:s'), 'sender_id' => $sender_id, 'type' => $type, 'cust_name' => $cust_name, 'deliverer_name' => $deliverer_name, 'file_type' => $file_type, 'thumbnail' => $thumbnail, 'ratings' => $ratings]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function sendEmailOrderStatus($user_name, $user_email, $subject=null, $message=null) 
	{
	    $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
	    $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
	    
	    $messageBody .="</head><body bgcolor='#FFFFFF'>";
	    $messageBody .="<table class='body-wrap'><tr><td></td><td class='container'><div class='content'>";
	    $messageBody .="<table><tr><td align='center'><h3>".$subject."</h3></td></tr>";
	    $messageBody .="<tr><td align='center'>";
	    $messageBody .= $message;
	    $messageBody .="</td></tr></table></div></td><tr/></table>";
	    //Email Signature
	    $messageBody .="<table class='head-wrap'>";
	    $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
	    $messageBody .="<tr><td>Your Gonagoo team !</td></tr>";
	    $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />Deliver, ship cheaper easily</td></tr>";
	    $messageBody .="<tr><td>Support : support@gonagoo.com</td></tr>";
	    $messageBody .="<tr><td>Website : www.gonagoo.com</td></tr>";
	    //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
	    $messageBody .="<tr><td>Join us on : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;'></a>";
	    $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;'></a>";
	    $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;'></a></td></tr>";
	    //Download the App <icon for iOS App download><icon for Android App download>
	    $messageBody .="<tr><td>Download the App : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;'></a>";
	    $messageBody .="<a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;'></a></td></tr>";
	    $messageBody .="</table></div></td><td></td></tr></table>";
	    //Email Signature End
	    $messageBody .="</body></html>";

	    $email_from = $this->config->item('from_email');
	    $email_subject = $subject;
	    $headers = "MIME-Version: 1.0" . "\r\n";
	    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	    $headers .= 'From: '.$email_from.'' . "\r\n";
	    mail($user_email, $email_subject, $messageBody, $headers);  
	    return 1;
	}

	public function sendFCM(array $msg, array $reg_id, $api_key=null)
	{
		if(is_array($msg) && is_array($reg_id) && !is_null($api_key)) {
	        $header = array('Authorization: key='.$api_key,'Content-Type: application/json');
	        $fields = array('registration_ids' => $reg_id, 'data' => $msg);
	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	        $result_fcm = curl_exec($ch);
	        curl_close($ch);
	        return 1;
	    }
	}

	public function sendAPN(array $msg, array $reg_id, $pem_file=null)
	{
		foreach ($reg_id as $key) {
			$deviceToken = $key;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pem_file);
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			//if (!$fp) exit("Failed to connect: $err $errstr" . PHP_EOL);
			// Create the payload body
			$body['aps'] = array(
			     'alert' => array(
			        'title' => $msg['title'],
			        'body' => $msg['desc'],
			        'datetime' => $msg['notice_date'],
			        'type' => $msg['type']
			       ),
			      'sound' => 'default'
			      );
			// Encode the payload as JSON
			$payload = json_encode($body);
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			// Close the connection to the server
			fclose($fp);
		}
		return 1;
	}

	public function get_order_detail($order_id=0)
	{
		if($order_id > 0 ) {
			$this->db->where('order_id', (int)($order_id));
			return $this->db->get($this->_courier_orders)->row_array();
		}
		return false;
	}

	public function assign_warehouse(array $data)
	{
		if(is_array($data)){
			$this->db->insert($this->_relay_warehouse_details, $data);
			return $this->db->insert_id();
		}
		return false;
	}

	public function update_warehouse_volume_used($rpw_id=0, $updated_warehouse_volume=null)
	{
		if($rpw_id > 0 && !is_null($updated_warehouse_volume)){
			$this->db->where('rpw_id', $rpw_id);
			$this->db->update($this->_relay_points_warehouse, ["volume_used" => $updated_warehouse_volume]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function out_from_warehouse($relay_id=0, $order_id=0)
	{
		if($order_id > 0 && $relay_id > 0){
			$this->db->where('order_id', (int) $order_id);
			$this->db->where('relay_id', (int) $relay_id);
			$this->db->update($this->_relay_warehouse_details, ["out_datetime" => date('Y-m-d h:i:s')]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_relay_warehouse_details($rpw_id=0, $last_id=0)
	{
		if($rpw_id > 0 ) {
			$this->db->where('rpw_id', (int)($rpw_id));
			$this->db->where('out_datetime', "NULL");
			if($last_id > 0) { $this->db->where('rpwd_id <', $last_id); }
			$this->db->order_by('rpwd_id', 'desc');
			$this->db->limit(10);
			return $this->db->get($this->_relay_warehouse_details)->result_array();
		}
		return false;
	}

	public function get_relay_warehouse_history($rpw_id=0, $last_id=0)
	{
		if($rpw_id > 0 ) {
			$this->db->where('rpw_id', (int)($rpw_id));
			$this->db->where('out_datetime != ', "NULL");
			if($last_id > 0) { $this->db->where('rpwd_id <', $last_id); }
			$this->db->order_by('rpwd_id', 'desc');
			$this->db->limit(10);
			return $this->db->get($this->_relay_warehouse_details)->result_array();
		}
		return false;
	}

	public function get_relay_point_warehouse_id($order_id=0, $relay_id=0)
	{
		if($order_id > 0 && $relay_id > 0) {
			$this->db->where('order_id', (int)($order_id));
			$this->db->where('relay_id', (int)($relay_id));
			$this->db->limit(1);
			$row = $this->db->get($this->_relay_warehouse_details)->row_array();
			return $row['rpw_id'];
		}
		return false;
	}

	public function get_country_details($country_id=0)
	{
		if($country_id > 0 ) {
			$this->db->where('country_id', (int)($country_id));
			return $this->db->get($this->_countries)->row_array();
		}
		return false;
	}

	public function get_state_details($state_id=0)
	{
		if($state_id > 0 ) {
			$this->db->where('state_id', (int)($state_id));
			return $this->db->get($this->_states)->row_array();
		}
		return false;
	}
	
	public function get_city_details($city_id=0)
	{
		if($city_id > 0 ) {
			$this->db->where('city_id', (int)($city_id));
			return $this->db->get($this->_cities)->row_array();
		}
		return false;
	}

	public function update_order_invoice_url($order_id=0, $invoice_url=null)
	{
		if( $order_id != null && !is_null($invoice_url) ) {
			$this->db->where('order_id', trim($order_id));
			$this->db->update($this->_courier_orders, ['invoice_url' => $invoice_url]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function gonagoo_master_details($currency_sign=null)
	{
		if(!is_null($currency_sign)) {
			$this->db->where('currency_code', trim($currency_sign));
			return $this->db->get($this->_gonagoo_account_master)->row_array();
		}
		return false;
	}

	public function update_payment_in_gonagoo_master($gonagoo_id=0, $gonagoo_balance=null)
	{
		if(!is_null($gonagoo_balance)) {
			$this->db->where('gonagoo_id', (int)$gonagoo_id);
			$this->db->update($this->_gonagoo_account_master, ['gonagoo_balance' => $gonagoo_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function insert_payment_in_gonagoo_history(array $insert_data)
	{
		if(is_array($insert_data)) {
			$this->db->insert($this->_gonagoo_account_history, $insert_data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function customer_account_master_details($cust_id=0, $currency_sign=null)
	{
		if($cust_id > 0 && !is_null($currency_sign)) {
			$this->db->where('user_id', (int)$cust_id);
			$this->db->where('currency_code', trim($currency_sign));
			return $this->db->get($this->_user_account_master)->row_array();
		}
		return false;
	}

	public function update_payment_in_customer_master($account_id=0, $cust_id=0, $account_balance=0)
	{
		if($cust_id > 0 && $account_id > 0) {
			$this->db->where('account_id', (int)$account_id);
			$this->db->where('user_id', (int)$cust_id);
			$this->db->update($this->_user_account_master, ['account_balance' => $account_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function insert_payment_in_account_history(array $insert_data)
	{
		if(is_array($insert_data)) {
			$this->db->insert($this->_user_account_history, $insert_data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function deliverer_scrow_master_details($deliverer_id=0, $currency_sign=null)
	{
		if($deliverer_id > 0 && !is_null($currency_sign)) {
			$this->db->where('deliverer_id', (int)$deliverer_id);
			$this->db->where('currency_code', $currency_sign);
			return $this->db->get($this->_deliverer_scrow_master)->row_array();
		}
		return false;
	}

	public function update_payment_in_deliverer_scrow($scrow_id=0, $deliverer_id=0, $scrow_balance=0)
	{
		if($deliverer_id > 0 && $scrow_id > 0) {
			$this->db->where('deliverer_id', (int)$deliverer_id);
			$this->db->where('scrow_id', (int)$scrow_id);
			$this->db->update($this->_deliverer_scrow_master, ['scrow_balance' => $scrow_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function insert_payment_in_scrow_history(array $insert_data)
	{
		if(is_array($insert_data)) {
			$this->db->insert($this->_deliverer_scrow_history, $insert_data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_country_code_by_id($country_id=0)
	{
		if($country_id > 0){
			$this->db->select('country_phonecode');
			$this->db->where('country_id', (int)$country_id);
			$this->db->limit(1);
			$return = $this->db->get($this->_countries)->row_array();
			return $return['country_phonecode'];
		}
		return false;
	}

	public function relay_account_master_details($cust_id=0, $currency_sign=null)
	{
		if($cust_id > 0) {
			$this->db->where('user_id', (int)$cust_id);
			$this->db->where('currency_code', trim($currency_sign));
			return $this->db->get($this->_relay_account_master)->row_array();
		}
		return false;
	}

	public function relay_account_master_details_currency($currency_sign=null)
	{
		if($cust_id > 0) {
			$this->db->where('currency_code', trim($currency_sign));
			return $this->db->get($this->_relay_account_master)->row_array();
		}
		return false;
	}

	public function insert_relay_master_record(array $insert_data)
	{
		if(is_array($insert_data)) {
			$this->db->insert($this->_relay_account_master, $insert_data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function relay_account_master_list($cust_id=0)
	{
		if($cust_id > 0) {
			$this->db->where('user_id', (int)$cust_id);
			return $this->db->get($this->_relay_account_master)->result_array();
		}
		return false;
	}

	public function update_payment_in_relay_master($account_id=0, $cust_id=0, $account_balance=0)
	{
		if($cust_id > 0) {
			$this->db->where('account_id', (int)$account_id);
			$this->db->where('user_id', (int)$cust_id);
			$this->db->update($this->_relay_account_master, ['account_balance' => $account_balance]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function insert_payment_in_relay_account_history(array $insert_data)
	{
		if(is_array($insert_data)) {
			$this->db->insert($this->_relay_account_history, $insert_data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_src_relay_commission_invoice_url($order_id=0, $invoice_url=null)
	{
		if( $order_id != null && !is_null($invoice_url) ) {
			$this->db->where('order_id', trim($order_id));
			$this->db->update($this->_courier_orders, ['src_relay_commission_invoice_url' => $invoice_url]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function update_dest_relay_commission_invoice_url($order_id=0, $invoice_url=null)
	{
		if( $order_id != null && !is_null($invoice_url) ) {
			$this->db->where('order_id', trim($order_id));
			$this->db->update($this->_courier_orders, ['dest_relay_commission_invoice_url' => $invoice_url]);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function get_relay_account_history($relay_id=0, $last_id=0)
	{
		if($relay_id > 0) {
			$this->db->where('user_id', (int)$relay_id);
			if($last_id > 0) { $this->db->where('ah_id <', $last_id); }
			$this->db->limit(10);
			$this->db->order_by('ah_id', 'desc');
			return $this->db->get($this->_relay_account_history)->result_array();
		}
		return false;
	}

	public function get_relay_account_withdraw_request_list($relay_id=0, $last_id=0)
	{
		if($relay_id > 0) {
			$this->db->where('user_id', (int)$relay_id);
			$this->db->where('user_type', 1);
			if($last_id > 0) { $this->db->where('req_id <', $last_id); }
			$this->db->limit(10);
			$this->db->order_by('req_id', 'desc');
			return $this->db->get($this->_account_withdraw_request)->result_array();
		}
		return false;
	}

	public function update_relay_account_details($relay_id=0, array $data)
	{
		if($relay_id>0 && is_array($data)){
			$this->db->update($this->_relay_points, $data);
			return $this->db->affected_rows();
		}
		return false;
	}

	public function relay_earned_this_month($relay_id=0)
	{
		if($relay_id > 0) {
			$this->db->select('SUM(amount) AS amount, currency_code');
			$this->db->where('user_id', (int)$relay_id);
			$this->db->where('type', 1);
			$this->db->like('datetime', date('Y-m'));
			$this->db->group_by('currency_code');
			return $row = $this->db->get($this->_relay_account_history)->result_array();
		}
		return false;
	}

	public function relay_earned_to_date($relay_id=0)
	{
		if($relay_id > 0) {
			$this->db->select('SUM(amount) AS amount, currency_code');
			$this->db->where('user_id', (int)$relay_id);
			$this->db->where('type', 1);
			$this->db->group_by('currency_code');
			return $row = $this->db->get($this->_relay_account_history)->result_array();
		}
		return false;
	}

	public function relay_commission_invoices($cust_id=0, $last_id=0)
	{
		if($relay_id > 0) {
			$this->db->select('order_id, invoice_url, cust_id, deliverer_id, from_address, to_address, distance_in_km, pickup_datetime, order_price, order_picture_url, delivered_datetime, rating, currency_sign, cust_name, deliverer_name, deliverer_company_name, deliverer_contact_name, from_relay_id, to_relay_id');

			$this->db->where('from_relay_id', (int)$relay_id);
			if($last_id > 0) { $this->db->where('order_id <', $last_id); }

			$this->db->or_where('to_relay_id', (int)$relay_id);
			if($last_id > 0) { $this->db->where('order_id <', $last_id); }

			$this->db->limit(10);
			$this->db->order_by('order_id', 'desc');
			return $this->db->get($this->_courier_orders)->result_array();
		}
		return false;
	}

	public function relay_payment_methods($relay_id=0)
	{
		if($relay_id > 0 ) {
			$this->db->select('iban, mobile_money_acc1, mobile_money_acc2');
			$this->db->where('relay_id', (int)($relay_id));
			return $this->db->get($this->_relay_points)->row_array();
		}
		return false;
	}
	
	public function get_order_packages($order_id=0)
    {
        if($order_id > 0 ){
            $this->db->where('order_id', $order_id);
            return $this->db->get($this->_order_packages)->result_array();
        }
        return false;
    }

    public function get_dangerous_goods($id=0)
    {
        if($id > 0 ){
            $this->db->where('id', $id);
            return $this->db->get($this->_dangerous_goods_master)->row_array();
        }
        return false;
    }

	public function get_gonagoo_address($country_id=0)
	{
		if($country_id > 0 ) {
		  $this->db->where('country_id', $country_id);
		  $row = $this->db->get($this->_gonagoo_address)->row_array();
		  if(!isset($row)) {
		    $this->db->where('country_id', 75);
		    return $this->db->get($this->_gonagoo_address)->row_array();
		  } else {
		    return $row;
		  }
		}
		return false;
	}

  	public function get_relay_point_details_by_id($relay_id=0)
	{
		if($relay_id > 0 ) {
			$this->db->where('relay_id', (int)($relay_id));
			return $this->db->get($this->_relay_points)->row_array();
		}
		return false;
	}

}

/* End of file Relay_api_model.php */
/* Location: ./application/models/Relay_api_model.php */